$(document).ready(function(){
	//Enable Ajax loading animation
	ajaxloader('show');
	//Check All checkbox
	$("input.check-all").on("click", function(){
		if($(this).prop("checked") == true)
		{
			$("input[name='checked[]']").prop("checked", true);
		}else{
			$("input[name='checked[]']").prop("checked", false);
		}
	});

	// sidebar toggle state
	$(".sidebar-toggle").on('click', function () {
		if($('body').hasClass('sidebar-collapse')){
			var status = false;
		}else{
			var status = true;
		}
		
		ajaxloader('hide');
		$.ajax({
			url	 : siteurl + "dashboard/save_menu_state", 
			type : "get",
			dataType : "json", 
			data : {state : status}, 
			success : function (msg){

			}
		});
		ajaxloader('show');
	});
	// Activate Notifikasi
	notif();
});
// ajaxloader
function ajaxloader(param){
	if(param == 'show'){
		$(document).ajaxStart(function () {
	        $(".ajax_loader").show();
	    }).ajaxStop(function () {
	        $(".ajax_loader").hide();
	    });
	}else{
		$(document).ajaxStart(function () {
	        $(".ajax_loader").hide();
	    }).ajaxStop(function () {
	        $(".ajax_loader").hide();
	    });
	}
}

//Check Number if INT or Float
function isInt(val){
	return Number(val) === val && val%1 === 0;
}

function isFloat(val){
	return Number(val) === val && val%1 !== 0;
}

function updateQueryStringParameter(uri, key, value) {
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)) {
		return uri.replace(re, '$1' + key + "=" + value + '$2');
	}else{
		return uri + separator + key + "=" + value;
	}
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

//Get Notifikasi
var ajaxStatus = new window.XMLHttpRequest();
var notif = function get_notif(){
	ajaxloader('hide');

	if(ajaxStatus && ajaxStatus.readyState != 4){
        ajaxStatus.abort();
        console.log('Request Aborted');
    }

	ajaxStatus = $.ajax({
		url : siteurl + "dashboard/get_notif", 
		type : "get", 
		dataType : "json",
		success : function (data){
			if(data){
				// SPK
				if(parseInt(data['total_spk']) > 0){
					$("#mn-lbl-total-spk").text(data['total_spk']);	
				}else{
					$("#mn-lbl-total-spk").text('');	
				}
				
				$.each(data['spk'], function(i, n){
					if(parseInt(n) > 0){
						$("#mn-lbl-spk-"+i).text(n);	
					}else{
						$("#mn-lbl-spk-"+i).text('');
					}
				});
				// Produksi
				if(parseInt(data['total_produksi']) > 0){
					$("#mn-lbl-total-produksi").text(data['total_produksi']);	
				}else{
					$("#mn-lbl-total-produksi").text('');
				}
				
				$.each(data['produksi'], function(i, n){
					if(parseInt(n) > 0){
						$("#mn-lbl-produksi-"+i).text(n);	
					}else{
						$("#mn-lbl-produksi-"+i).text('');
					}
				});
				//Play Audio
				if(parseInt(data['total_spk']) > 0 || parseInt(data['total_produksi']) > 0){
					playAudio('fail');
				}
			}
			// Get notifikasi setiap 10 detik
			setTimeout(notif, 30000);
		}
	});

	ajaxloader('show');
}
/* PLAY SOUND FUNCTION */
var playAudio = function(file){
    if(file === 'alert'){
        document.getElementById('audio-alert').play();
    }

    if(file === 'fail'){
        document.getElementById('audio-fail').play();    
    }
}
// load photo
var loadfoto = function(input,idimgtag) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function (e) {
			$('#'+idimgtag).attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);

		$delbtn = $("<a id='del-photo' href='#_' style='text-decoration:none;position: absolute;top: 31px;left: 15px;' title='Hapus'><span class='label label-danger'>X</span></a>");

		$delbtn.on("click", function(){
			$(input).val("");
			$('#'+idimgtag).attr('src','');
			$(this).remove();
		});

		$('#'+idimgtag).closest("div").append($delbtn);
	}
}