<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= isset($idt->nm_perusahaan) ? $idt->nm_perusahaan : 'not-set' ?><?= isset($template['title']) ? " | ".$template['title'] : '' ?></title>
  <!-- Custom CSS -->
  <link rel="stylesheet" href="<?= base_url('assets/css/styles.css') ?>">
  <!-- jQuery 3.2.1 -->
  <script src="<?= base_url('assets/plugins/jQuery/dist/jquery.min.js') ?>"></script>
  <script type="text/javascript">
    var baseurl = "<?= base_url() ?>";
    var siteurl = "<?= site_url() ?>";
  </script>
</head>
<body>
<div class="wrapper">
    