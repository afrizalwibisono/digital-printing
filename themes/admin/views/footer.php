</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer hidden-print">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      <?= isset($idt->nm_perusahaan) ? $idt->nm_perusahaan : 'not-set' ?> - Page rendered in <strong>{elapsed_time}</strong> seconds
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <?= gmdate('Y') ?> <a href="<?= site_url() ?>"><?= isset($idt->nm_perusahaan) ? $idt->nm_perusahaan : 'not-set' ?></a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
<!-- Bootstrap 3.3.6 -->
<script src="<?= base_url('assets/adminlte/bootstrap/dist/js/bootstrap.min.js').'?v='.APP_VER ?>"></script>
<!-- iCheck -->
<script src="<?= base_url('assets/plugins/iCheck/icheck.min.js').'?v='.APP_VER ?>"></script>
<!-- Jquery UI -->
<script src="<?= base_url('assets/plugins/jquery-ui/jquery-ui.min.js').'?v='.APP_VER ?>"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('assets/adminlte/dist/js/adminlte.min.js').'?v='.APP_VER ?>"></script>
<!-- Custom Scripts -->
<script src="<?= base_url('assets/js/scripts.js').'?v='.APP_VER ?>" type="text/javascript"></script>
<!-- Audio -->
<audio id="audio-alert" src="<?= base_url('assets/audio/alert.mp3') ?>" preload="auto"></audio>
<audio id="audio-fail" src="<?= base_url('assets/audio/fail.mp3') ?>" preload="auto"></audio>
<!-- Assets -->
<?= get_scripts(); ?>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>