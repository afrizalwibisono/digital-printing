<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['estimasi_bb_title_view'] 	= 'Detail Penggunaan Bahan Baku By Sistem';

// form/table
$lang['estimasi_bb_range_tanggal']	= 'Range Tanggal';
$lang['estimasi_bb_barcode']			= 'Barcode';
$lang['estimasi_bb_nm_barang']		= 'Nama Barang';
$lang['estimasi_bb_kategori']			= 'Kategori Bahan Baku';
$lang['estimasi_bb_jenis']			= 'Janis Bahan Baku';
$lang['estimasi_bb_merk']				= 'Merk Bahan Baku';
$lang['estimasi_bb_satuan']			= 'Satuan';
$lang['estimasi_bb_other_keyword']	= 'Keyword Lain';

$lang['estimasi_bb_jml_pakai_produksi'] = 'JML Pakai Produksi';


$lang['estimasi_bb_records_found']	= 'Data tidak ditemukan';