<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_validasi','name'=>'frm_validasi','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="col-sm-3">
				<div class="form-group reduce_form_control">
					<label class="control-label nomargin_bottom_label" for="no_order" ><?=  lang('vc_noorder') ?></label>
					<span class="form-control input-sm" id="no_order"><?= $dt->no_transaksi ?></span>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group reduce_form_control">
					<input type="hidden" name="id_konsumen" value="<?= $dt->id_konsumen ?>">
					<label class="control-label nomargin_bottom_label" for="konsumen" ><?=  lang('vc_konsumen') ?></label>
					<div class="input-group">
						<span class="form-control input-sm" id="konsumen"><?= ucwords($dt->panggilan." ".$dt->nama) ?></span>
						<span class="input-group-addon" id="st_konsumen">
							<?php
								switch ($dt->st) {
									case 0:
										echo "Konsumen";
										break;
									case 1:
										echo "Reseller";
										break;
									case 2:
										echo "Instansi";
										break;	
								}
							?>
						</span>
					</div>
									
				</div>	
			</div>
			<div class="col-sm-3">
				<div class="form-group reduce_form_control">
					<label class="control-label nomargin_bottom_label" for="tgl_order" ><?=  lang('vc_tgl_order') ?></label>
					<span class="form-control input-sm" id="tgl_order"><?= date('d/m/Y', strtotime($dt->tgl_order)) ?></span>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group reduce_form_control <?= form_error('tgl_cetak') ? ' has-error' : ''; ?>">
					<label class="control-label nomargin_bottom_label" for="tgl_cetak" ><?=  lang('vc_tgl_cetak') ?></label>	
					<input class="form-control input-sm" type="text" id="tgl_cetak" name="tgl_cetak" value="<?= set_value('tgl_cetak', isset($dt_input['tgl_cetak'])) ? date('d/m/Y', strtotime($dt_input['tgl_cetak'])) : '' ?>" placeholder="dd/mm/yyyy" required="" />
				</div>
			</div>

			<div class="clearfix"></div>

		  	<div class="table-responsive">
		  		<table class="table table-bordered">
		  			<thead>
		  				<tr class="success">
			  				<th width="50">#</th>
			  				<th colspan="2"><?= lang('vc_description') ?></th>
			  				<th><?= lang('vc_kuota') ?></th>
			  				<th><?= lang('vc_jml_cetak') ?></th>
			  				<th><?= lang('vc_keterangan') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($dt->detail) : 
		  					foreach($dt->detail as $key => $det):
		  						$warna_urgent 	= "";
								if($det->st_urgent == 1){
									$warna_urgent = "class='bg-gray color-palette'";
								}

								$kuota_cetak = $det->jml_cetak - $det->jml_tercetak;
		  				?>
		  				<tr>
		  					<td>
		  						<?= $key+1 ?>
		  						<input type="hidden" name="id_m_order_detail[]" value="<?= $det->id_detail_produk_order ?>">
		  					</td>
		  					<td class="text-center thumb-daftar"><img src="<?= base_url($det->thumbnail) ?>" class="img-rounded" alt="<?= lang('vc_image_null') ?>"></td>
		  					<td class="deskripsi" style="width: 20%">
		  						<p class="nama-pekerjaan"><?= $det->nama_pekerjaan ?></p>
								<p><?= $det->nmkategori." - ".$det->nm_produk ?></p>
								<p>
									<?php 
										if($det->st_urgent == 1){
											echo lang('vc_tgl_selesai')." : <span class='text-danger'>".(date('d/m/Y', strtotime($det->tgl_selesai)))."</span>";
										}
									?>
								</p>
								<p>
									<?php
										if($det->st_tipe_produk == 1){
											echo "Uk : ".$det->p."x".$det->l."(".$det->tampil2.")";
										}
									?>
								</p>
								<p><?= $det->st_finishing ?></p>
		  					</td>
		  					<td><?= $kuota_cetak ?></td>
		  					<td style="min-width: 100px">
		  						<?php if(($kuota_cetak) > 0): ?>
		  						<input type="number" min="0" max="<?= $kuota_cetak ?>" step="1" class="form-control input-sm" name="jml_cetak[]" value="<?= isset($dt_input['det_input'][$det->id_detail_produk_order]) ? $dt_input['det_input'][$det->id_detail_produk_order] : '' ?>" />
		  						<?php else: ?>
		  						<input type="hidden" name="jml_cetak[]" value="0" />
		  						<?php endif; ?>
		  					</td>
		  					<td>
		  						<?php if(($kuota_cetak) > 0): ?>
		  						<textarea class="form-control" name="keterangan[]" maxlength="255" placeholder="catatan tambahan"><?= isset($dt_input['ket'][$det->id_detail_produk_order]) ? $dt_input['ket'][$det->id_detail_produk_order] : '' ?></textarea>
		  						<?php else: ?>
		  						<input type="hidden" name="keterangan[]" value="" />
		  						<?php endif; ?>
		  					</td>
		  				</tr>
		  				<?php endforeach;endif; ?>
		  			</tbody>
		  		</table>
		  	</div>
		  	<div class="form-group">
			    <div class="col-md-12">
			      	<button type="submit" name="save" class="btn btn-primary"><?= lang('btn_save') ?></button>
			    	<?php
	                	echo lang('bf_or') . ' ' . anchor('validasi_cetak/cancel', lang('btn_back'), array("onclick" => "return confirm('".lang('vc_cancel_confirm')."')"));
	                ?>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->