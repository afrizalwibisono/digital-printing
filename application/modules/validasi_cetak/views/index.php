<?php
	$ENABLE_ADD     = has_permission('Validasi Cetak.Add');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('name'=>'frm_index','id'=>'frm_index','role'=>'form','class'=>'form-inline')) ?>
	<div class="box-header">
		<div class="pull-left">
			<?php if($ENABLE_ADD): ?>
			<a class="btn btn-success" href="<?= site_url('validasi_cetak/open_order') ?>"><?= lang('btn_create') ?></a>
			<?php endif; ?>
		</div>
		<div class="pull-right">
			<div class="form-group">
				<div class="input-group input-daterange">
					<input type="text" name="tgl1" id="tgl1" class="form-control" readonly placeholder="<?= lang('vc_tgl1') ?>" value = "<?= set_value('tgl1',isset($tgl1) ? $tgl1 : '') ?>" style = "width: 120px" >
					<span class="input-group-addon">to</span>
					<input type="text" name="tgl2" id="tgl2" class="form-control" readonly placeholder="<?= lang('vc_tgl2') ?>" value = "<?= set_value('tgl2',isset($tgl2) ? $tgl2 : '') ?>" style = "width: 120px" >
				</div>
			</div>
			<div class="form-group">
				<select class="form-control" name="konsumen" id="konsumen" style="min-width: 230px">
					<option></option>
					<?php
						if(isset($dt_konsumen) && is_array($dt_konsumen) && count($dt_konsumen)):
							foreach ($dt_konsumen as $key => $val) :
									
					?>
					<option value="<?= $val->id ?>" <?= set_select('konsumen',$val->id, isset($konsumen) && $konsumen == $val->id) ?> ><?= ucwords($val->nm) ?></option>
					<?php
							endforeach;
						endif;
					?>
				</select>
			</div>
			<div class="form-group">
				<input type="text" name="no_order" id="no_order" class="form-control" placeholder="<?= lang('vc_noorder') ?>" value = "<?= set_value('no_order',isset($no_order) ? $no_order : '') ?>" >
			</div>
			<div class="form-group">
				<button class="btn btn-default" name="btn_cari" id="btn_cari">
				<span class="fa fa-search"></span>
				</button>
			</div>
		</div>
	</div>
	
	<?php if(isset($data) && is_array($data) && count($data)):  ?>
	<div class="box-body table-responsive">
		
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th width="50">#</th>
					<th><?= lang('vc_noorder') ?></th>
					<th><?= lang('vc_an') ?></th>
					<th class="text-center" ><?= lang('vc_status') ?></th>
					<th class="text-center" ><?= lang('vc_tgl_order') ?></th>
					<th><?= lang('vc_total') ?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($data as $key => $val) :
						
						$label 	= "";
						switch ($val->st) {
							case 0:
								$label 	= "<span class='label label-success'>$val->st_konsumen</span>";
								break;
							case 1:
								$label 	= "<span class='label label-primary'>$val->st_konsumen</span>";
								break;
							case 2:
								$label 	= "<span class='label label-danger'>$val->st_konsumen</span>";
								break;
						}
				?>
				<tr>
					<td><?= $numb; ?></td>
					<td><?= $val->no_transaksi ?></td>
					<td>
						<strong><?= ucwords($val->panggilan)." ". ucwords($val->nama) ?></strong>
					</td>
					<td class="text-center" ><?= $label ?></td>
					<td class="text-center" ><?= date('d/m/Y', strtotime($val->tgl_order)) ?></td>
					<td><?= number_format($val->total_value_order) ?></td>
					<td><a href="<?= site_url('validasi_cetak/detail/'.$val->order_produk_id) ?>" data-toggle="tooltip" data-placement="left" title="Lihat Detail"><i class="fa fa-folder"></i></a></td>
				</tr>
				<?php
					$numb++;
					endforeach;
				?>
			</tbody>
		</table>
	</div>
	<div class="box-footer clearfix">
		<?= $this->pagination->create_links(); ?>
	</div>
	<?php else: ?>
	<div class="alert alert-info" role="alert">
		<p><i class="fa fa-warning"></i> &nbsp; <?= lang('vc_no_records_found') ?></p>
	</div>
	<?php endif;?>
	<?= form_close() ?>
</div>