<?php
	$ENABLE_DELETE = has_permission('Validasi Cetak.Delete');
?>
<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_validasi','name'=>'frm_validasi','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="col-sm-3">
				<div class="form-group reduce_form_control">
					<label class="control-label nomargin_bottom_label" for="no_order" ><?=  lang('vc_noorder') ?></label>
					<span class="form-control input-sm" id="no_order"><?= $dt->no_transaksi ?></span>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group reduce_form_control">
					<label class="control-label nomargin_bottom_label" for="konsumen" ><?=  lang('vc_konsumen') ?></label>
					<div class="input-group">
						<span class="form-control input-sm" id="konsumen"><?= ucwords($dt->panggilan." ".$dt->nama) ?></span>
						<span class="input-group-addon" id="st_konsumen">
							<?php
								switch ($dt->st) {
									case 0:
										echo "Konsumen";
										break;
									case 1:
										echo "Reseller";
										break;
									case 2:
										echo "Instansi";
										break;	
								}
							?>
						</span>
					</div>
									
				</div>	
			</div>
			<div class="col-sm-3">
				<div class="form-group reduce_form_control">
					<label class="control-label nomargin_bottom_label" for="tgl_order" ><?=  lang('vc_tgl_order') ?></label>
					<span class="form-control input-sm" id="tgl_order"><?= date('d/m/Y', strtotime($dt->tgl_order)) ?></span>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-12" style="padding-left: 0">
				<div class="table-responsive">
					<table class="table table-bordered">
						<tr>
							<th colspan="2"><?= lang('vc_description') ?></th>
						</tr>
						<tr>
							<?php
								$det = $dt->deskripsi;
							?>
		  					<td class="deskripsi" style="width: 50%">
		  						<p class="nama-pekerjaan"><?= $det->nama_pekerjaan ?></p>
								<p><?= $det->nmkategori." - ".$det->nm_produk ?></p>
								<p>
									<?php 
										if($det->st_urgent == 1){
											echo lang('vc_tgl_selesai')." : <span class='text-danger'>".(date('d/m/Y', strtotime($det->tgl_selesai)))."</span>";
										}
									?>
								</p>
								<p>
									<?php
										if($det->st_tipe_produk == 1){
											echo "Uk : ".$det->p."x".$det->l."(".$det->tampil2.")";
										}
									?>
								</p>
								<p><?= $det->st_finishing ?></p>
		  					</td>
		  					<td class="text-center thumb-daftar" style="width: 50%"><img src="<?= base_url($det->thumbnail) ?>" class="img-rounded" alt="<?= lang('vc_image_null') ?>"></td>
						</tr>
					</table>
				</div>
			</div>

			<div class="clearfix"></div>

		  	<div class="table-responsive">
		  		<table class="table table-bordered">
		  			<thead>
		  				<tr class="success">
			  				<th width="50">#</th>
			  				<th><?= lang('vc_kasir') ?></th>
			  				<th><?= lang('vc_tgl_cetak') ?></th>
			  				<th><?= lang('vc_jml_cetak') ?></th>
			  				<th><?= lang('vc_keterangan') ?></th>
			  				<th width="10"></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($dt->history) : 
		  					foreach($dt->history as $key => $det):
		  				?>
		  				<tr>
		  					<td>
		  						<?= $key+1 ?>
		  					</td>
		  					<td><?= ucwords($det->kasir) ?></td>
		  					<td><?= date('d/m/Y', strtotime($det->tgl_cetak)) ?></td>
		  					<td><?= $det->jml_cetak ?></td>
		  					<td style="white-space: pre"><?= $det->ket ?></td>
		  					<?php if($ENABLE_DELETE): ?>
		  					<td><a href="<?= site_url('validasi_cetak/delete/'.$det->id_kasir_order_po_detail) ?>" data-toggle="tooltip" data-placement="left" title="Hapus" onclick="return confirm('<?= (lang('vc_delete_confirm')); ?>')"><i class="fa fa-trash text-red"></i></a></td>
		  					<?php endif; ?>
		  				</tr>
		  				<?php endforeach;endif; ?>
		  			</tbody>
		  		</table>
		  	</div>
		  	<div class="form-group">
			    <div class="col-md-12">
			    	<a href="<?= site_url("validasi_cetak") ?>" class="btn btn-primary">
						<span class="fa fa-long-arrow-left"></span> <?= lang("btn_back") ?>
					</a>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->