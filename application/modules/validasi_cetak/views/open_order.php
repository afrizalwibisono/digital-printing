<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('name'=>'frm_index','id'=>'frm_index','role'=>'form','class'=>'form-inline')) ?>
	<div class="box-header">
		
		<a href="<?= site_url("validasi_cetak") ?>" class="btn btn-primary">
			<span class="fa fa-long-arrow-left"></span> <?= lang("btn_back") ?>
		</a>
		<div class="pull-right">
			<div class="form-group">
				<div class="input-group input-daterange">
					<input type="text" name="tgl1" id="tgl1" class="form-control" readonly placeholder="<?= lang('vc_tgl1') ?>" value = "<?= set_value('tgl1',isset($tgl1) ? $tgl1 : '') ?>" style = "width: 120px" >
					<span class="input-group-addon">to</span>
					<input type="text" name="tgl2" id="tgl2" class="form-control" readonly placeholder="<?= lang('vc_tgl2') ?>" value = "<?= set_value('tgl2',isset($tgl2) ? $tgl2 : '') ?>" style = "width: 120px" >
				</div>
			</div>
			<div class="form-group">
				<select class="form-control" name="konsumen" id="konsumen" style="min-width: 230px">
					<option></option>
					<?php
							if(isset($dt_konsumen) && is_array($dt_konsumen) && count($dt_konsumen)):
								foreach ($dt_konsumen as $key => $isi) :
									
					?>
					<option value="<?= $isi->id ?>" <?= set_select('konsumen',$isi->id, isset($konsumen) && $konsumen == $isi->id) ?> ><?= ucwords($isi->nm) ?></option>
					<?php
								endforeach;
							endif;
					?>
				</select>
			</div>
			<div class="form-group">
				<input type="text" name="no_order" id="no_order" class="form-control" placeholder="<?= lang('vc_noorder') ?>" value = "<?= set_value('no_order',isset($no_order) ? $no_order : '') ?>" >
			</div>
			<div class="form-group">
				<button class="btn btn-default" name="btn_cari" id="btn_cari">
				<span class="fa fa-search"></span>
				</button>
			</div>
		</div>
	</div>
	
	<?php if(isset($data) && is_array($data) && count($data)):  ?>
	<div class="box-body table-responsive">
		
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th width="50">#</th>
					<th><?= lang('vc_noorder') ?></th>
					<th><?= lang('vc_an') ?></th>
					<th class="text-center" ><?= lang('vc_status') ?></th>
					<th class="text-center" ><?= lang('vc_tgl_order') ?></th>
					<th><?= lang('vc_total') ?></th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($data as $key => $isi) :
						
							$label 	= "";
						switch ($isi->st) {
							case 0:
									$label 	= "<span class='label label-success'>$isi->st_konsumen</span>";
								break;
								case 1:
									$label 	= "<span class='label label-primary'>$isi->st_konsumen</span>";
								break;
							case 2:
									$label 	= "<span class='label label-danger'>$isi->st_konsumen</span>";
								break;
						}
				?>
				<tr>
					<td><?= $numb; ?></td>
					<td><?= $isi->no_transaksi ?></td>
					<td>
						<a class="text-blue" href="<?= site_url('validasi_cetak/create/' . $isi->id_order); ?>" data-toggle="tooltip" data-placement="left" title="Buat Validasi Cetak">
							<u><strong><?= ucwords($isi->panggilan)." ". ucwords($isi->nama) ?></strong></u>
						</a>
					</td>
					<td class="text-center" ><?= $label ?></td>
					<td class="text-center" ><?= date('d/m/Y', strtotime($isi->tgl_order)) ?></td>
					<td class="text-right" >
						<strong class="text-primary">
						<?= number_format($isi->total_value_order) ?>
						</strong>
					</td>
				</tr>
				<?php
					$numb++;
					endforeach;
				?>
			</tbody>
		</table>
	</div>
	<div class="box-footer clearfix">
		<?= $this->pagination->create_links(); ?>
	</div>
	<?php else: ?>
	<div class="alert alert-info" role="alert">
		<p><i class="fa fa-warning"></i> &nbsp; <?= lang('vc_no_records_found') ?></p>
	</div>
	<?php endif ;?>
	<?= form_close() ?>
</div>