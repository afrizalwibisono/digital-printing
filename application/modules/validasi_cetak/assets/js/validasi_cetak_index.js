$(function(){
	$("#konsumen").select2({
        placeholder :"-- Konsumen --",
        allowClear  : true
  	});
  	
	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});
});