<?php defined('BASEPATH')|| exit('No direct script access allowed');

$lang['vc_title']		= 'Validasi Cetak PO';
$lang['vc_open_order']	= 'Daftar Order PO';
$lang['vc_new_order']	= 'Validasi Cetak Baru';
$lang['vc_view_order']	= 'Detail Validasi Cetak PO';
$lang['vc_history_cetak']	= 'History Validasi Cetak PO';

$lang['vc_noorder']		= 'No. Order';
$lang['vc_an']			= 'Atas Nama';
$lang['vc_konsumen']	= 'Konsumen';
$lang['vc_status']		= 'Status';
$lang['vc_tgl_order'] 	= 'Tgl. Order';
$lang['vc_last_cetak']  = 'Terakhir Cetak';
$lang['vc_jml_order']	= 'Jml. Order';
$lang['vc_telah_cetak'] = 'Telah dicetak';
$lang['vc_total'] 		= 'Total Order';
$lang['vc_description'] = 'Deskripsi';
$lang['vc_kuota'] 		= 'Kuota Cetak';
$lang['vc_jml_cetak'] 	= 'Jml. Cetak';
$lang['vc_tgl_cetak'] 	= 'Tgl. Req. Cetak';
$lang['vc_kasir'] 		= 'Kasir';
$lang['vc_keterangan'] 	= 'Keterangan';

$lang['vc_tgl1']		= 'Tanggal Awal';
$lang['vc_tgl2']		= 'Tanggal Akhir';

//==============================
$lang['btn_create']				= 'Baru';
$lang['btn_delete']				= 'Hapus';
$lang['btn_filter']				= 'Filter';
$lang['btn_back']				= 'Batal / Kembali';
$lang['btn_save']				= 'Simpan';
$lang['bf_or']					= "atau";

// =============================
$lang['label_simpan_sukses'] = "Simpan Data Validasi Cetak baru sukses";
$lang['label_simpan_gagal']  = "Simpan Data Validasi Cetak baru gagal";
$lang['label_cancel_sukses'] = "Data Validasi Cetak telah berhasil dibatalkan";
$lang['vc_image_null']		 = "Gambar tidak ditemukan";
$lang['vc_tgl_selesai']		 = "Tgl. Selesai";

// Messages
$lang['vc_del_error']		= 'Anda belum memilih Validasi Cetak yang akan dihapus.';
$lang['vc_del_failure']		= 'Tidak dapat menghapus Validasi Cetak: ';
$lang['vc_delete_confirm']	= 'Apakah anda yakin akan menghapus Validasi Cetak terpilih ?';
$lang['vc_cancel_confirm']	= 'Apakah anda yakin akan membatalkan Validasi Cetak ?';
$lang['vc_deleted']		  	= 'Data Validasi Cetak berhasil dihapus';
$lang['vc_delete_failure']	= 'Data Validasi Cetak gagal dihapus';
$lang['vc_no_records_found']= 'Data tidak ditemukan.';
$lang['vc_invalid_id'] 		= 'ID tidak valid';
$lang['vc_no_detail'] 		= 'Tidak ada detail order';
$lang['vc_no_jml_cetak'] 	= 'Anda belum memasukkan Jml. Cetak';
$lang['vc_over_cetak'] 		= 'Jml. cetak pada baris ke : %s melebihi kuota cetak';