<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2018, Cokeshome
 * 
 * This is controller for print po validation

*/
class Validasi_cetak extends Admin_Controller {

	protected $viewPermission   		= "Validasi Cetak.View";
    protected $addPermission    		= "Validasi Cetak.Add";
    protected $deletePermission 		= "Validasi Cetak.Delete";

    public function __construct(){
    	parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('validasi_cetak');
        $this->load->model([
        					'validasi_cetak/order_model',
        					'validasi_cetak/kasir_order_po_model',
        					'validasi_cetak/kasir_order_po_detail_model',
    						]);

        $this->template->title(lang('vc_title'));
		$this->template->page_icon('fa fa-list');
    }

    public function index(){
    	$this->auth->restrict($this->viewPermission);
        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
            $tgl_awal  = $this->input->post('tgl_awal');
            $tgl_akhir = $this->input->post('tgl_akhir');
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
            $tgl_awal  = $this->input->get('tgl_awal');
            $tgl_akhir = $this->input->get('tgl_akhir');
        }

        $filter = "?search=".urlencode($search);
        $search2 = $this->db->escape_str($search);

        $add_where = "";
        if($tgl_awal !='' && $tgl_akhir !='')
        {
            $filter    .= "&tgl_awal=".urlencode(date_ymd($tgl_awal))."&tgl_akhir=".urlencode(date_ymd($tgl_akhir));
            $add_where .= " AND ((kasir_order_po.tgl_order >='".date_ymd($tgl_awal)."' AND kasir_order_po.tgl_order <='".date_ymd($tgl_akhir)."') OR (kasir_order_po.tgl_cetak >='".date_ymd($tgl_awal)."' AND kasir_order_po.tgl_cetak <='".date_ymd($tgl_akhir)."'))";
        }
        
        $where = "`kasir_order_po.deleted` = 0 $add_where
                AND (`konsumen`.`nama` LIKE '%$search2%' ESCAPE '!'
                OR `total_value_order` LIKE '%$search2%' ESCAPE '!')";
        
        $total = $this->order_model
                    ->join("(select order_produk_id, deleted from kasir_order_po group by order_produk_id) as kasir_order_po","order_produk.id_order=kasir_order_po.order_produk_id")
                    ->join("konsumen", "order_produk.id_konsumen=konsumen.idkonsumen")
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->order_model
                    ->select("kasir_order_po.*, 
                    			order_produk.total_value_order, 
                    			order_produk.no_transaksi,
                    			konsumen.panggilan,
								konsumen.nama,
                    			konsumen.st, 
                    			IF(`konsumen`.`st` = 0,
						        'Konsumen',
						        IF(`konsumen`.`st` = 1,
						            'Reseller',
						            'Instansi')) AS st_konsumen,
						        order_produk.tgl_order")
                   	->join("(select order_produk_id, deleted from kasir_order_po group by order_produk_id) as kasir_order_po","order_produk.id_order=kasir_order_po.order_produk_id")
                    ->join("konsumen", "order_produk.id_konsumen=konsumen.idkonsumen")
                    ->where($where)
                    ->order_by(['order_produk.created_on' => 'DESC', 'tgl_order' => 'DESC'])
                    ->limit($limit, $offset)->find_all();

        $dt_konsumen 	= $this->konsumen_model
                                    ->select("`idkonsumen` as id, 
                                        CONCAT(IF(panggilan IS NULL, '', panggilan),
                                        ' ',
                                        `nama`,
                                        ' | ',
                                        IF(st = 0,
                                            'konsumen',
                                            IF(st = 1, 'reseller', 'instansi'))) AS nm")
									->where("deleted=0 and st < 2")
									->order_by("nama","asc")
									->find_all();

        $assets 	= 	array(
    						"plugins/select2/js/select2.js",
                            "plugins/select2/css/select2.css",
                            'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        	'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                            'validasi_cetak/assets/js/validasi_cetak_index.js',
                            "validasi_cetak/assets/css/css_list.css"
    				);

        add_assets($assets);

        $this->template->set('data', $data);
        $this->template->set('search', $search);
        $this->template->set('tgl1', $tgl_awal);
        $this->template->set('tgl2', $tgl_akhir);
        $this->template->set("dt_konsumen", $dt_konsumen);

        $this->template->set("toolbar_title", lang('vc_title'));
        $this->template->set("page_title", lang('vc_title'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index');
    }

    private function cek_hapus($id_kasir_order_po_detail = NULL){
    	$cek = $this->kasir_order_po_detail_model
    				->where(['id_kasir_order_po_detail' => $id_kasir_order_po_detail])
    				->where('st_produksi', 1)
    				->count_all();

    	if($cek > 0){
    		return false;
    	}

    	return true;
    }

    public function open_order(){
		$this->auth->restrict($this->addPermission);

		$where 				= "order_produk.deleted=0 AND order_produk.st_simpan=1 AND order_produk.st_kasir=1 AND order_produk.st_po=1";
		$add_where 	= "";

		if(isset($_POST['btn_cari'])){

			$tgl1 			= isset($_POST['tgl1']) ? $this->input->post('tgl1') : '';
			$tgl2 			= isset($_POST['tgl2']) ? $this->input->post('tgl2') : '';

			$nm_konsumen 	= isset($_POST['konsumen']) ? $this->input->post('konsumen') : '';
			$no_order 		= isset($_POST['no_order']) ? $this->input->post('no_order') : '';

		}else{

			$tgl1 			= isset($_GET['tgl1']) ? $this->input->get('tgl1') : '';
			$tgl2 			= isset($_GET['tgl2']) ? $this->input->get('tgl2') : '';

			$nm_konsumen 	= isset($_GET['konsumen']) ? $this->input->get('konsumen') : '';
			$no_order 		= isset($_GET['no_order']) ? $this->input->get('no_order') : '';

		}    	

		$filter 	= "?order";

		if($tgl1 != '' && $tgl2 != ''){

			$tgl1 	= date_ymd($tgl1);
			$tgl2 	= date_ymd($tgl2);

			$add_where 	.= " and order_produk.tgl_order >= '$tgl1' and order_produk.tgl_order <= '$tgl2'";

		}

		if($nm_konsumen != ''){

			$add_where 	.= " and order_produk.id_konsumen = $nm_konsumen";

		}

		if($no_order != ''){

			$add_where 	= " and order_produk.no_transaksi = '$no_order'";

		}

		$where .= $add_where;

    	$offset = $this->input->get('per_page');
        $limit = $this->config->item('list_limit');
        $total 	= $this->order_model->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
        							->where($where)
        							->count_all();

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->load->library('pagination');

        $this->pagination->initialize($this->pager);

    	$data 	= $this->order_model->select("order_produk.id_order,
											    order_produk.no_transaksi,
											    tgl_order,
											    order_produk.tgl_permintaan_selesai AS tgl_selesai,
											    order_produk.total_value_order,
											    konsumen.panggilan,
											    konsumen.nama,
											    konsumen.st,
											    IF(konsumen.st = 0,
											        'Konsumen',
											        IF(konsumen.st = 1,
											            'Reseller',
											            'Instansi')) AS st_konsumen")
    								->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
    								->where($where)
    								->order_by("order_produk.tgl_order","desc")
    								->limit($limit, $offset)
    								->find_all();

    	$dt_konsumen 	= $this->konsumen_model
                                    ->select("`idkonsumen` as id, 
                                        CONCAT(IF(panggilan IS NULL, '', panggilan),
                                        ' ',
                                        `nama`,
                                        ' | ',
                                        IF(st = 0,
                                            'konsumen',
                                            IF(st = 1, 'reseller', 'instansi'))) AS nm")
									->where("deleted=0 and st < 2")
									->order_by("nama","asc")
									->find_all();

    	$asset 	= 	array(
						"plugins/select2/js/select2.js",
                        "plugins/select2/css/select2.css",
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                    	'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        "validasi_cetak/assets/js/validasi_cetak_open.js",
    				);

    	add_assets($asset);

    	$this->template->set("tgl1",$tgl1);
    	$this->template->set("tgl2",$tgl2);
    	$this->template->set("konsumen",$nm_konsumen);
    	$this->template->set("no_order",$no_order);

    	$this->template->set("dt_konsumen",$dt_konsumen);

    	$this->template->set("numb", $offset+1);
    	$this->template->set("data", $data);
		$this->template->set("toolbar_title", lang('vc_open_order'));
        $this->template->set("page_title", lang('vc_open_order'));
        $this->template->title(lang('vc_open_order'));
        $this->template->render('open_order');     	

    }

    public function create($id = NULL){
    	if(!$id){
    		redirect('validasi_cetak');
    	}

    	if(isset($_POST['save']) && has_permission($this->addPermission)){
            $simpan  = $this->save($id);

    		if($simpan){
    			$this->template->set_message(lang('label_simpan_sukses'),"success");
    			redirect('validasi_cetak');
    		}

    	}

    	$dt_input 	= $this->session->userdata('frm_validasi_cetak');
    	if(!$dt_input){
    		$this->save_session();
    		$dt_input 	= $this->session->userdata('frm_validasi_cetak');
    	}

    	$data = $this->get_data($id);

    	$asset 	= 	array(
    						"plugins/select2/js/select2.js",
                            "plugins/select2/css/select2.css",
                            "plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
                        	"plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
                        	"plugins/number/jquery.number.js",
                            "validasi_cetak/assets/css/css_list.css",
                            "validasi_cetak/assets/js/validasi_cetak_new.js"
    				);

    	add_assets($asset);

    	$this->template->set("dt", $data);
    	$this->template->set("dt_input", $dt_input);

		$this->template->set("toolbar_title", lang('vc_new_order'));
        $this->template->set("page_title", lang('vc_new_order'));
        $this->template->title(lang('vc_new_order'));
        $this->template->page_icon('fa fa-print');
        $this->template->render('form_validasi');     	    	

    }

    public function detail($id = NULL){
    	if(!$id){
    		redirect('validasi_cetak');
    	}

    	$data = $this->get_data($id);

    	$asset 	= 	array(
                            "validasi_cetak/assets/css/css_list.css",
    				);

    	add_assets($asset);

    	$this->template->set("dt", $data);

		$this->template->set("toolbar_title", lang('vc_view_order'));
        $this->template->set("page_title", lang('vc_view_order'));
        $this->template->title(lang('vc_view_order'));
        $this->template->page_icon('fa fa-folder');
        $this->template->render('detail');     	    	

    }

    public function history($id_order = NULL, $id_m_order = NULL){
    	if(!$id_order || !$id_m_order){
    		redirect('validasi_cetak');
    	}

    	$data 	= $this->order_model->select("
										    `order_produk`.`no_transaksi`,
										    `konsumen`.`panggilan`,
										    `konsumen`.`nama`,
										    `konsumen`.`st`,
										    `order_produk`.`tgl_order`")
								->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
								->find_by([
											'order_produk.deleted' => 0, 
											'order_produk.id_order' => $id_order
										]);

		if(!$data){
			$this->template->set_message(lang('vc_invalid_id'), 'error');
			redirect('validasi_cetak');
		}

    	$history = $this->kasir_order_po_detail_model
    						->select('kasir_order_po_detail.*, kasir_order_po.tgl_cetak, users.nm_lengkap as kasir')
    						->join('kasir_order_po', 'kasir_order_po_detail.id_kasir_order_po=kasir_order_po.id_kasir_order_po')
    						->join('users', 'kasir_order_po.created_by=users.id_user')
    						->order_by('kasir_order_po.tgl_cetak', 'DESC')
    						->order_by('kasir_order_po.created_on', 'DESC')
                            ->find_all_by(['id_m_order_detail' => $id_m_order, 'kasir_order_po_detail.deleted' => 0]);
    	$data->history = $history;

    	$deskripsi   = $this->order_model
                        	->select("`m_order_produk_detail`.`nama_pekerjaan`,
                                `kategori`.`nmkategori`,
                                `produk`.`nm_produk`,
                                `produk`.`st_tipe` as st_tipe_produk,
                                `m_order_produk_detail`.`id_detail_produk_order`,
                                `m_order_produk_detail`.`thumbnail`,
                                `m_order_produk_detail`.`p`,
                                `m_order_produk_detail`.`l`,
                                `m_order_produk_detail`.`id_satuan` as id_satuan_uk,
                                CONCAT(konversi_satuan.satuan_besar,
                                        ' (',
                                        `konversi_satuan`.`jml_kecil`,
                                        ' ',
                                        `satuan_terkecil`.`alias`,
                                        ')') AS tampil2,
                                `m_order_produk_detail`.`st_urgent`,
                                `m_order_produk_detail`.`tgl_permintaan_selesai` AS tgl_selesai,
                                IF(`m_order_produk_detail`.`st_finishing` = 0,
                                    'Finishing Standart',
                                    'Custom Finishing') AS st_finishing")
                        ->join("m_order_produk_detail","order_produk.id_order = m_order_produk_detail.id_order","inner")
                        ->join("kategori","m_order_produk_detail.id_kategori = kategori.idkategori","inner")
                        ->join("produk","m_order_produk_detail.id_produk = produk.idproduk","inner")
                        ->join("konversi_satuan","m_order_produk_detail.id_satuan = konversi_satuan.id_konversi","inner")
                        ->join("satuan_terkecil","konversi_satuan.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","inner")
                        ->find_by("m_order_produk_detail.id_detail_produk_order", $id_m_order);
        
        $data->deskripsi = $deskripsi;

    	$asset 	= 	array(
                        "validasi_cetak/assets/css/css_list.css",
    				);

    	add_assets($asset);

    	$this->template->set("dt", $data);

    	$this->template->set("toolbar_title", lang('vc_history_cetak'));
        $this->template->set("page_title", lang('vc_history_cetak'));
        $this->template->title(lang('vc_history_cetak'));
        $this->template->page_icon('fa fa-print');
        $this->template->render('history');
    }

    protected function get_data($id_order = NULL){
    	$data 	= $this->order_model->select("`order_produk`.`id_order`,
												    `order_produk`.`no_transaksi`,
                                                    `order_produk`.`id_konsumen`,
												    `konsumen`.`panggilan`,
												    `konsumen`.`nama`,
												    `konsumen`.`st`,
												    `order_produk`.`tgl_order`")
	    								->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
	    								->find_by([
	    											'order_produk.deleted' => 0, 
	    											'order_produk.st_simpan' => 1, 
	    											'order_produk.st_kasir' => 1, 
	    											'order_produk.st_po' => 1, 
	    											'order_produk.id_order' => $id_order
	    										]);
	   	if(!$data){
	   		$this->template->set_message(lang('vc_no_records_found'), 'error');
	   		redirect('validasi_cetak');
	   	}

    	$detail   = $this->order_model
                        ->select("`m_order_produk_detail`.`nama_pekerjaan`,
                                `kategori`.`nmkategori`,
                                `produk`.`nm_produk`,
                                `produk`.`st_tipe` as st_tipe_produk,
                                `m_order_produk_detail`.`id_detail_produk_order`,
                                `m_order_produk_detail`.`thumbnail`,
                                `m_order_produk_detail`.`p`,
                                `m_order_produk_detail`.`l`,
                                `m_order_produk_detail`.`id_satuan` as id_satuan_uk,
                                CONCAT(konversi_satuan.satuan_besar,
                                        ' (',
                                        `konversi_satuan`.`jml_kecil`,
                                        ' ',
                                        `satuan_terkecil`.`alias`,
                                        ')') AS tampil2,
                                `m_order_produk_detail`.`st_urgent`,
                                `m_order_produk_detail`.`tgl_permintaan_selesai` AS tgl_selesai,
                                IF(`m_order_produk_detail`.`st_finishing` = 0,
                                    'Finishing Standart',
                                    'Custom Finishing') AS st_finishing,
                                `m_order_produk_detail`.`jml_cetak`,
                                `tkuota`.`jml_tercetak`")
                        ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                        ->join("m_order_produk_detail","order_produk.id_order = m_order_produk_detail.id_order","inner")
                        ->join("kategori","m_order_produk_detail.id_kategori = kategori.idkategori","inner")
                        ->join("produk","m_order_produk_detail.id_produk = produk.idproduk","inner")
                        ->join("konversi_satuan","m_order_produk_detail.id_satuan = konversi_satuan.id_konversi","inner")
                        ->join("satuan_terkecil","konversi_satuan.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","inner")
                        ->join("(SELECT 
								    m_order_produk_detail.id_detail_produk_order,
								    IFNULL(SUM(kasir_order_po_detail.jml_cetak), 0) AS jml_tercetak
								FROM
								    m_order_produk_detail
								        LEFT JOIN
								    kasir_order_po_detail ON m_order_produk_detail.id_detail_produk_order = kasir_order_po_detail.id_m_order_detail
								WHERE
								    m_order_produk_detail.id_order = '".$id_order."' AND kasir_order_po_detail.deleted=0
								GROUP BY m_order_produk_detail.id_detail_produk_order) as tkuota", "m_order_produk_detail.id_detail_produk_order=tkuota.id_detail_produk_order", "left")
                        ->where("order_produk.id_order", $id_order)
                        ->order_by("kategori.nmkategori","asc")
                        ->find_all();
        
        $data->detail = $detail;
		return $data;
    }

    protected function save($id_order = NULL){
    	if(!$id_order){
    		$this->template->set_message(lang('vc_invalid_id'), 'error');
    		return false;
    	}

    	$this->form_validation->set_rules('id_konsumen', 'lang:vc_konsumen', 'required|trim');
    	$this->form_validation->set_rules('tgl_cetak', 'lang:vc_tgl_cetak', 'required|trim');

    	if($this->form_validation->run() === false){
    		$this->save_session();
    		$this->template->set_message(validation_errors(), 'error');

    		return false;
    	}

    	$id_konsumen = $this->input->post('id_konsumen');
    	$tgl_cetak 	 = $this->input->post('tgl_cetak');
    	$id_m_order_detail = $this->input->post('id_m_order_detail');
        $jml_cetak   = $this->input->post('jml_cetak');
    	$ket 	     = $this->input->post('keterangan');

    	if(is_array($jml_cetak)){
    		$cek = false;
    		foreach ($jml_cetak as $key => $val) {
    			if(intval($val) > 0){
    				$cek = true;
    				break;
    			}
    		}

    		if(!$cek){
    			$this->template->set_message(lang('vc_no_jml_cetak'), 'error');
    			return false;
    		}
    	}else{
    		$this->template->set_message(lang('vc_no_detail'), 'error');

    		return false;
    	}

    	// Validate Kuota Cetak
		$ero_msg = [];
        $urut = 0;
        foreach ($jml_cetak as $key => $val) {
            $urut ++;

            if(intval($val) > 0 && !$this->cek_kuota($id_m_order_detail[$key], $val)){
				$ero_msg[] = sprintf(lang('vc_over_cetak'), $urut);
			}
        }

        if($ero_msg)
        {
        	$this->save_session();

            $ero_msg = implode("<br>", $ero_msg);
            $this->template->set_message($ero_msg, 'error');
            return FALSE;
        }

    	$this->db->trans_start();
    	$data = [
    				'id_kasir_order_po' => gen_primary("", "kasir_order_po", "id_kasir_order_po"),
    				'id_konsumen' 		=> $id_konsumen,
    				'tgl_cetak' 		=> date_ymd($tgl_cetak),
    				'order_produk_id' 	=> $id_order
    			];

    	$detail = [];
    	foreach ($jml_cetak as $key => $val) {
			if(intval($val) > 0){
				$detail[] = [
								'id_kasir_order_po_detail'  => gen_primary(),
								'id_kasir_order_po' 		=> $data['id_kasir_order_po'],
								'id_m_order_detail' 		=> $id_m_order_detail[$key],
                                'jml_cetak'                 => intval($val),
								'ket' 				        => $ket[$key],
								'st_acc_produksi' 			=> 0,
                                'st_produksi'               => 0,
								'st_spk' 		 		    => 0
							];
			}
		}

		$sql = "";
		if($data){
			$this->kasir_order_po_model->insert($data);
			$sql .= $this->db->last_query();
		}
		
		if($detail){
			$this->kasir_order_po_detail_model->insert_batch($detail);
			$sql .= $this->db->last_query();
            foreach ($detail as $key => $val) {
                simpan_spk(1, $val['id_kasir_order_po_detail']);
                // Set Log
                set_proses_order(1,1, $val['id_m_order_detail'], $val['jml_cetak']);
            }
		}

    	$this->db->trans_complete();

    	if($this->db->trans_status() == false){
            $return         = false;
            $keterangan     = "Gagal, membuat validasi cetak";
            $total          = 0;
            $status         = 0;
        }else{
            $return         = true;
            $keterangan     = "Sukses, membuat validasi cetak";
            $total          = 0;           
            $status         = 1;
        }

        $nm_hak_akses   = $this->addPermission; 
        $kode_universal = $data['id_kasir_order_po'];
        $jumlah         = 0;
        $sql            = $sql;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $total, $sql, $status);
        // Clear Session
        $this->session->unset_userdata('frm_validasi_cetak');

        return $return;
    }

    protected function save_session(){
    	$tgl_cetak 			= $this->input->post('tgl_cetak');
    	$id_m_order_detail 	= $this->input->post('id_m_order_detail');
        $jml_cetak          = $this->input->post('jml_cetak');
    	$ket 			    = $this->input->post('keterangan');

    	$data = ['tgl_cetak' => $tgl_cetak,
    			 'det_input' => []
    			];
    	if($id_m_order_detail){
    		foreach ($id_m_order_detail as $key => $val) {
                $data['det_input'][$val] = isset($jml_cetak[$key]) ? $jml_cetak[$key] : NULL;
    			$data['ket'][$val]       = isset($ket[$key]) ? $ket[$key] : NULL;
    		}
    	}

    	$this->session->set_userdata('frm_validasi_cetak', $data);
    }

    protected function cek_kuota($id_m_order_detail = NULL, $jml_cetak = 0){
    	$cek = $this->kasir_order_po_detail_model
    				->select('m_order_produk_detail.jml_cetak, IFNULL(SUM(kasir_order_po_detail.jml_cetak), 0) AS jml_tercetak')
    				->join("m_order_produk_detail","kasir_order_po_detail.id_m_order_detail=m_order_produk_detail.id_detail_produk_order", "right")
    				->where('m_order_produk_detail.id_detail_produk_order', $id_m_order_detail)
                    ->where('kasir_order_po_detail.deleted', 0)
    				->find_all();
    	if(($cek[0]->jml_tercetak + $jml_cetak) > $cek[0]->jml_cetak){
    		return false;
    	}

    	return true;
    }

    //Cancel Validasi cetak
    public function cancel(){
        $this->auth->restrict($this->addPermission);

        $this->session->unset_userdata('frm_validasi_cetak');
        $this->template->set_message(lang('label_cancel_sukses'), 'success');

        redirect('validasi_cetak');
    }

    public function delete($id = NULL){
        $this->auth->restrict($this->deletePermission);
        if(!$id){
            $this->template->set_message(lang('vc_invalid_id'), 'error');
            redirect('validasi_cetak');
        }

        $cek_dt = $this->kasir_order_po_detail_model->find($id);
        if(!$cek_dt){
            $this->template->set_message(lang('vc_invalid_id'), 'error');
            redirect('validasi_cetak');
        }

        //Cek apakah masih bisa dihapus
        $cek         = $this->cek_hapus($id);
        $dt_order    = $this->kasir_order_po_detail_model
                            ->select('kasir_order_po.order_produk_id, kasir_order_po_detail.id_m_order_detail')
                            ->join('kasir_order_po','kasir_order_po_detail.id_kasir_order_po = kasir_order_po.id_kasir_order_po')
                            ->find_by('id_kasir_order_po_detail', $id);
        $query_hapus = "";
        $total       = 0;
        if($cek)
        {
            $total      = 0;
            $result     = $this->kasir_order_po_detail_model->delete($id);
            $query_hapus .= $this->db->last_query();
            if($result)
            {
                // Hapus SPK
                delete_spk($cek_dt->id_spk);
                $query_hapus .= $this->db->last_query();
                // Update id SPK jadi NULL
                $this->kasir_order_po_detail_model->update($id, ['id_spk' => NULL]);
                $query_hapus .= $this->db->last_query();

                $keterangan = "SUKSES, hapus data Validasi Cetak Detail dengan ID : ".$id;
                $status     = 1;
            }
            else
            {
                $keterangan = "GAGAL, hapus data Validasi Cetak Detail dengan ID : ".$id;
                $status     = 0;
            } 
        }
        else
        {
            $keterangan = "GAGAL, hapus data Validasi Cetak Detail dengan ID : ".$id." karena sudah masuk produksi";
            $status     = 0;
        }

        $nm_hak_akses   = $this->deletePermission; 
        $kode_universal = $id;
        $jumlah         = $total;
        $sql            = $query_hapus;

        $hasil = simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
        


        if(isset($result) && $result == true && $hasil){
            $this->template->set_message(lang('vc_deleted'), 'success');
        }else{
            $this->template->set_message(lang('vc_delete_failure'), 'error');
        }

        redirect($dt_order ? ('validasi_cetak/history/'.$dt_order->order_produk_id."/".$dt_order->id_m_order_detail) : 'validasi_cetak');
    }
}