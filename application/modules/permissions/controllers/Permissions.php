<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Permissions extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Permission.View";
    protected $addPermission    = "Permission.Add";
    protected $updatePermission = "Permission.Update";
    protected $deletePermission = "Permission.Delete";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('permissions/permissions');
        $this->load->model('permissions/permissions_model');

        $this->template->title(lang('permissions_title_manage'));
		$this->template->page_icon('fa fa-shield');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if (isset($_POST['delete']) && has_permission($this->deletePermission))
        {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                foreach ($checked as $pid)
                {
                    $result         = $this->permissions_model->delete($pid);
                }

                if ($result)
                {
                    $this->template->set_message(count($checked) .' '. lang('permissions_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('permissions_del_failure') . $this->permissions_model->error, 'error');
                }
            }
            else
            {
                $this->template->set_message(lang('permissions_del_error') . $this->permissions_model->error, 'error');
            }
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
        }

        $filter = "";
        if($search!="")
        {
            $filter = "?search=".$search;
        }

        $search2 = $this->db->escape_str($search);
        
        $where = "`nm_permission` LIKE '%$search2%' ESCAPE '!'
                OR `ket` LIKE '%$search2%' ESCAPE '!'";
        
        $total = $this->permissions_model
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->permissions_model
                    ->where($where)
                    ->order_by('nm_permission','ASC')
                    ->limit($limit, $offset)->find_all();

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set("toolbar_title", lang('permissions_title_manage'));
        $this->template->set("page_title", lang('permissions_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

   	//Create New Supplier
   	public function create()
   	{

        $this->auth->restrict($this->addPermission);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_permissions())
            {
              $this->template->set_message(lang("permissions_create_success"), 'success');
              redirect('permissions');
            }
        }

        $this->template->set("page_title", lang('permissions_title_new'));
   	    $this->template->render('permissions_form');
   	}

   	//Edit Supplier
   	public function edit()
   	{
   		
  		$this->auth->restrict($this->updatePermission);
                
        $id = (int)$this->uri->segment(3);

        if (empty($id))
        {
            $this->template->set_message(lang("permissions_invalid_id"), 'error');
            redirect('permissions');
        }

        if (isset($_POST['save']))
        {
            if ($this->save_permissions('update', $id))
            {
                $this->template->set_message(lang("permissions_edit_success"), 'success');
            }

        }

        $data  = $this->permissions_model->find_by(array('id_permission' => $id));

        if(!$data)
        {
            $this->template->set_message(lang("permissions_invalid_id"), 'error');
            redirect('permissions');
        }
        
        $this->template->set('data', $data);
        $this->template->set('toolbar_title', lang("permissions_title_edit"));
        $this->template->set('page_title', lang("permissions_title_edit"));
        $this->template->render('permissions_form');
        
   	}

   	protected function save_permissions($type='insert', $id=0)
   	{

        $this->form_validation->set_rules('nm_permission','lang:permissions_name','required|trim|max_length[255]');
        $this->form_validation->set_rules('ket','lang:permissions_ket','trim');

        if ($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        unset($_POST['submit'], $_POST['save']);

        if ($type == 'insert')
        {
            $id = $this->permissions_model->insert($_POST);

            if (is_numeric($id))
            {
                $return = TRUE;
            }
            else
            {
                $return = FALSE;
            }
        }
        else if ($type == 'update')
        {
            $return = $this->permissions_model->update($id, $_POST);
        }

        return $return;
   	}

}
?>