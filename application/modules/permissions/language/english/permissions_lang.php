<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['permissions_title_manage'] 	= 'Permissions';
$lang['permissions_title_new'] 		= 'New Permission';
$lang['permissions_title_edit'] 	= 'Edit Permission';

// form/table
$lang['permissions_name'] 			= 'Permission Name';
$lang['permissions_ket'] 			= 'Description';

// button
$lang['permissions_btn_new'] 		= 'New';
$lang['permissions_btn_delete'] 	= 'Delete';
$lang['permissions_btn_save'] 		= 'Save';
$lang['permissions_btn_cancel'] 	= 'Cancel';
$lang['permissions_btn_or']			= 'or';

// messages
$lang['permissions_del_error']			= 'Please, select at least an item to be deleted.';
$lang['permissions_del_failure']		= 'Permission can not be deleted: ';
$lang['permissions_delete_confirm']		= 'Are you sure to delete selected permission ?';
$lang['permissions_deleted']			= 'Permission deleted successfully';
$lang['permissions_no_records_found'] 	= 'Data not found.';
$lang['permissions_create_failure'] 	= 'New permission saving failed: ';
$lang['permissions_create_success'] 	= 'New permission saved successfully';
$lang['permissions_edit_success'] 		= 'Permission saved successfully';
$lang['permissions_invalid_id'] 		= 'Invalid ID';