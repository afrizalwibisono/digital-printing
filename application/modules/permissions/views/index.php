<?php 
    $ENABLE_ADD     = has_permission('Permission.Add');
    $ENABLE_UPDATE  = has_permission('Permission.Update');
    $ENABLE_DELETE  = has_permission('Permission.Delete');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_permissions','name'=>'frm_permissions')) ?>
        <div class="box-header">
            <div class="form-group">
                <?php if ($ENABLE_ADD) : ?>
	  	        <a href="<?= site_url('permissions/create') ?>" class="btn btn-success" title="<?= lang('permissions_btn_new') ?>"><i class="fa fa-plus"></i> <?= lang('permissions_btn_new') ?></a>
                <?php endif; ?>
                <div class="pull-right">
                    <div class="input-group">
                        <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="Search" autofocus />
                        <div class="input-group-btn">
                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
	<div class="box-body table-responsive no-padding">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
                        <th width="50">#</th>
                        <th><?= lang('permissions_name') ?></th>
                        <th><?= lang('permissions_ket') ?></th>
                        <?php if($ENABLE_UPDATE) : ?>
                        <th width="25"></th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($results as $record) : ?>
                    <tr>
                        <td class="column-check"><input type="checkbox" name="checked[]" value="<?= $record->id_permission ?>" /></td>
                        <td><?= $numb; ?></td>
                        <td><?= $record->nm_permission ?></td>
                        <td><?= $record->ket ?></td>
                        <?php if($ENABLE_UPDATE) : ?>
                        <td style="padding-right:20px"><a class="text-black" href="<?= site_url('permissions/edit/' . $record->id_permission); ?>" data-toggle="tooltip" data-placement="left" title="Edit Data"><i class="fa fa-pencil"></i></a></td>
                        <?php endif; ?>
                    </tr>
                    <?php $numb++; endforeach; ?>
                </tbody>
	  </table>
	</div><!-- /.panel-body -->
	<div class="box-footer clearfix">
		<?php if($ENABLE_DELETE) : ?>
		<button type="submit" name="delete" class="btn btn-danger" id="delete-me" onclick="return confirm('<?= lang('permissions_delete_confirm'); ?>')"><i class="fa fa-trash-o"></i> <?php echo lang('permissions_btn_delete') ?></button>
		<?php endif;
		echo $this->pagination->create_links(); 
		?>
	</div>
	<?php else: ?>
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('permissions_no_records_found') ?></p>
    </div>
    <?php
	endif;
	echo form_close(); 
	?>
</div><!-- /.panel -->