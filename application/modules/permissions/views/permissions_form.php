<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_permissions','name'=>'frm_permissions','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="form-group <?= form_error('nm_permission') ? ' has-error' : ''; ?>">
			    <label for="nm_permission" class="col-sm-2 control-label"><?= lang('permissions_name') ?></label>
			    <div class="col-sm-3">
			    	<input type="text" class="form-control" id="nm_permission" name="nm_permission" maxlength="255" value="<?php echo set_value('nm_permission', isset($data->nm_permission) ? $data->nm_permission : ''); ?>" required autofocus>
			    </div>
			    <p class="help-block"><i>Example : Barang.Add (separated by dot [.])</i></p>
		  	</div>
		  	<div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
			    <label for="ket" class="col-sm-2 control-label"><?= lang('permissions_ket') ?></label>
			    <div class="col-sm-4">
			    	<textarea class="form-control" id="ket" name="ket"><?php echo set_value('ket', isset($data->ket) ? $data->ket : ''); ?></textarea>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      	<button type="submit" name="save" class="btn btn-primary"><i class="fa fa-save"></i> <?= lang('permissions_btn_save') ?></button>
			    	<?php
	                	echo lang('permissions_btn_or') . ' ' . anchor('permissions', lang('permissions_btn_cancel'));
	                ?>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->