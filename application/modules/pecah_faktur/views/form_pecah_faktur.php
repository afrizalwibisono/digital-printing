<?= form_open($this->uri->uri_string(),array('name'=>'frm_baru','id'=>'frm_baru','role'=>'form','class'=>'form-horizontal')) ?>


<div class="box box-primary">

	<div class="box-body">
		
		<div class="form-group">
			
			<label class="control-label col-sm-2" for="no_faktur"><?= lang("capt-trans-no-faktur") ?></label>
			<div class="col-sm-4">
				<input type="text" name="no_faktur" id="no_faktur" class="form-control">
			</div>

		</div>

		<div class="form-group">
			
			<label class="control-label col-sm-2" for="jml_pecah"><?= lang("capt-trans-jml-pecah") ?></label>
			<div class="col-sm-4">
				<div class="input-group">
					<input type="text" name="qty_pecah" id="qty_pecah" class="form-control">
					<span class="input-group-addon"><?= lang("capt-trans-nota") ?></span>
				</div>
			</div>

		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-4">
				<button type="button" name="simpan" id="simpan" class="btn btn-primary" >
    				<?= lang("btn-save") ?>
    			</button>
    			<?php echo lang("bf_or")." ".anchor('pecah_faktur/batal', lang('btn-batal')) ?>
			</div>
			
		</div>

	</div>
	
</div>

<!-- Modal -->
<div id="myModal" class="modal fade in" role="dialog">
  	<div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	    	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal">&times;</button>
	        	<h4 class="modal-title">Cetak Nota</h4>
	      	</div>
	      	<div class="modal-body form-horizontal">
	        	
	        	<div class="form-group">
	        		<label class="control-label col-sm-3" for="tipe_nota" ><?= lang("capt-modal-jns-nota") ?></label>
	        		<div class="col-sm-9">
	        			<select class="form-control" name="tipe_nota" id="tipe_nota">
	        				<option value="0" selected ><?= lang("isi-jns-nota-besar") ?></option>
	        				<option value="1"><?= lang("isi-jns-nota-kecil") ?></option>
	        			</select>
	        		</div>
	        	</div>
	        	
	      	</div>
	      	<div class="modal-footer">
	      		<a href="#" id="cetak" class="btn btn-default" class="text-black" data-toggle="tooltip" data-placement="left" title="Cetak Nota"> <?= lang("capt-print") ?> </a>	

	      		<!-- <a href="#" id="cetak" class="btn btn-default" onclick="window.open(this.href, '_blank', 'width=744,height=600,scrollbars=yes,menubar=no,status=yes,resizable=yes,screenx=100,screeny=100'); return false;" class="text-black" data-toggle="tooltip" data-placement="left" title="Cetak Nota" data-original-title="Cetak Nota"); return false;" > <?= lang("capt-print") ?> </a>	 -->

	      	</div>
		    	
	    </div>

  	</div>
</div>





<?= form_close() ?>