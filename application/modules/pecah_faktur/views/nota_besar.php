<?= form_open($this->uri->uri_string(),array('id'=>'frm_po','name'=>'frm_po','role'=>'form','class'=>'form-horizontal')) ?>

<?php

	foreach ($data as $key => $isi):
		
		if($key>0):
?>
	<div style="page-break-after: always;"></div>
<?php		
	endif;
?>

<div class="box box-solid">
    <!-- form start -->
   	<?php if($idt) : ?>
    <div class="row header-laporan">
        <div class="col-md-12">
            <div class="col-md-1 laporan-logo">
                <img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo">
            </div>
            <div class="col-md-6 laporan-identitas">
                <address>
                	<h3 class="no-margin"><?= lang('pembelian_po_title_view') ?></h3>
                    <h4><?= $idt->nm_perusahaan ?></h4>
                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?><br>
                </address>
            </div>
            <div class="clearfix"></div>
            <div class="laporan-garis"></div>
        </div>
    </div>
    <?php endif ?>

    <div class="box-body">
		<table class="table-condensed table-summary">
            <tr>
                <td width="80"><?= lang('capt-nota-no-faktur') ?></td>
                <td width="10">:</td>
                <td><strong><?= $isi['no_faktur'] ?></strong></td>
                <td width="80"><?= lang('capt-nota-konsumen') ?></td>
                <td width="10">:</td>
                <td><strong><?= $isi['konsumen'] ?></strong></td>
                <td width="80"><?= lang('capt-nota-lama-tempo') ?></td>
                <td width="10">:</td>
                <td><strong><?= $isi['lama_tempo'] ?></strong></td>
            </tr>
            <tr>
            	<td width="80"><?= lang('capt-nota-tgl-trans') ?></td>
                <td width="10">:</td>
                <td><strong><?= $isi['waktu'] ?></strong></td>
                <td width="80"><?= lang('capt-nota-st-faktur') ?></td>
                <td width="10">:</td>
                <?php

                	if($isi['status'] == 0){
                		$tampil 	= "cash";
                	}else if($isi['status'] == 1){
                		$tampil 	= "dp";
                	}else if($isi['status'] == 2 || $isi['status'] == 3){
                		$tampil 	= "tempo";
                	}else{
                		$tampil 	= "dp";
                	}	

                ?>
                <td><strong><?= $tampil ?></strong></td>
                <td width="80"><?= lang('capt-nota-tgl-tempo') ?></td>
                <td width="10">:</td>
                <td><strong><?= $isi['tgl_tempo'] ?></strong></td>
            </tr>
        </table>
        <h5>Detail Order</h5>
		  	<div class="table-responsive">
		  		<table class="table table-condensed table-bordered table-detail">
		  			<thead>
		  				<tr class="success">
		  					<th width="30" class="text-center">No.</th>
		  					<th> <?= lang('capt-table-nm-file')?></th>
		  					<th> <?= lang('capt-table-nm-produk') ?> </th>
		  					<th class="text-center"> <?= lang('capt-table-ukuran') ?> </th>
		  					<th class="text-center"> <?= lang('capt-table-jml') ?> </th>
		  					<th class="text-right"> <?= lang('capt-table-total') ?> </th>
		  					<th class="text-right"> <?= lang('capt-table-potongan') ?> </th>
		  					<th class="text-right"> <?= lang('capt-table-sub-total') ?> </th>
		  				</tr>
		  			</thead>
		  			<tbody>
		  				<?php
		  					$n = 0;
		  					foreach ($isi['detail'] as $key => $d) :
		  						$n++;
		  				?>

		  				<tr>
		  					<td><?= $n ?></td>
		  					<td><?= ucfirst($d['nm_file']) ?></td>
		  					<td><?= ucfirst($d['produk']) ?></td>
		  					<td><?= $d['ukuran'] ?></td>
		  					<td class="text-center"><?= $d['jml'] ?></td>
		  					<td align="right"><?= number_format($d['total']) ?></td>
		  					<td align="right"><?= number_format($d['potongan']) ?></td>
		  					<td align="right"><?= number_format($d['subtotal']) ?></td>
		  				</tr>

		  				<?php
		  					endforeach;
		  				?>
		  			</tbody>
		  			<tfoot>
		  				<tr>
		  					<td colspan="7" align="right"><?= lang('capt-table-final-total') ?></td>
		  					<td align="right"> <?= number_format($isi['total']) ?> </td>
		  				</tr>
		  				<tr>
		  					<td colspan="7" align="right"><?= lang('capt-table-final-potongan') ?></td>
		  					<td align="right"> <?= number_format($isi['potongan']) ?> </td>
		  				</tr>
		  				<tr>
		  					<td colspan="7" align="right"><?= lang('capt-table-final-diskon') ?></td>
		  					<td align="right"> <?= number_format($isi['diskon']) ?> </td>
		  				</tr>
		  				<tr>
		  					<td colspan="7" align="right"><?= lang('capt-table-final-pajak') ?></td>
		  					<td align="right"> <?= number_format($isi['pajak']) ?> </td>
		  				</tr>
		  				<tr>
		  					<td colspan="7" align="right"><?= lang('capt-table-final-gt') ?></td>
		  					<td align="right"><label><?= number_format($isi['gtotal']) ?></label>  </td>
		  				</tr>
		  				<tr>
		  					<td colspan="7" align="right"><?= lang('capt-table-final-bayar') ?></td>
		  					<td align="right"><label><?= number_format($isi['bayar']) ?></label>  </td>
		  				</tr>

		  			</tfoot>
		  		</table>
		  	</div>	
		  	<div class="row laporan-ttd">
		  		<div class="col-xs-4 col-sm-2 text-center pull-right">
		  			
		  			<?= lang("capt-table-ttd-hormat") ?>
		  			<br>
		  			<br>
		  			<br>
		  			( <label><?= $op->nm_karyawan ?></label> )
		  			<p><?= lang("capt-table-ttd-op") ?></p>

		  		</div>
		  	</div>

    </div>  


</div>
<?php
		
		endforeach;

?>

<?= form_close() ?>  

<script type="text/javascript">
	$(document).ready(function(){
		window.print();
	});
</script>