<?php
		$ENABLE_ADD     = has_permission('Pecah Faktur.Add'); 
    	$ENABLE_DELETE  = has_permission('Pecah Faktur.Delete'); 
?>

<div class="box box-primary">
<?= form_open($this->uri->uri_string(),array('name'=>'frm_index','id'=>'frm_index','role'=>'form')) ?>
	<div class="box-header">
		
		<div class="box-body form-horizontal">
			
			<div class="col-sm-6">			

				<div class="form-group" style="margin-bottom: 5px">
					<label for="tgl1" class="col-sm-4 control-label"><?= lang("capt-filter-waktu") ?></label>
					<div class="col-sm-7">
						<div class="input-group input-daterange" >
							<input type="text" name="tgl1" id="tgl1" class="form-control" readonly placeholder="<?= lang('capt-seacrh-order-tgl1') ?>" value = "<?= set_value('tgl1',isset($tgl1) ? $tgl1 : '') ?>" >
							<span class="input-group-addon">to</span>
							<input type="text" name="tgl2" id="tgl2" class="form-control" readonly placeholder="<?= lang('capt-seacrh-order-tgl2') ?>" value = "<?= set_value('tgl2',isset($tgl2) ? $tgl2 : '') ?>" >
						</div>
					</div>
				</div>

				<div class="form-group" style="margin-bottom: 5px">
					<label for="konsumen" class="col-sm-4 control-label" ><?= lang("capt-filter-konsumen") ?></label>
					<div class="col-sm-7">
						<select class="form-control" name="konsumen" id="konsumen">
							<option></option>
							<?php 
									if(isset($dt_konsumen) && is_array($dt_konsumen) && count($dt_konsumen)):
										foreach ($dt_konsumen as $key => $isi) :
											
							?>

									<option value="<?= $isi->id ?>" <?= set_select('konsumen',$isi->id, isset($konsumen) && $konsumen == $isi->id) ?> ><?= ucwords($isi->nm) ?></option>

							<?php
										endforeach;
									endif;
							?>
						</select>
					</div>

				</div>

				<div class="form-group" style="margin-bottom: 5px">
					<label for="metode" class="col-sm-4 control-label"><?= lang("capt-filter-metode") ?></label>
					<div class="col-sm-7">
						<select class="form-control" name="metode" id="metode">
							<option></option>
							<option value="0" <?= set_select('metode','0',isset($metode) && $metode == '0' ) ?> >
								<?= lang('isi-st-bayar-kontan') ?>
							</option>
							<option value="1" <?= set_select('metode','1',isset($metode) && $metode == '1' ) ?> >
								<?= lang('isi-st-bayar-dp-last') ?>
							</option>
							<option value="2" <?= set_select('metode','2',isset($metode) && $metode == '2' ) ?> >
								<?= lang('isi-st-bayar-tempo-dp') ?>
							</option>
							<option value="3" <?= set_select('metode','3',isset($metode) && $metode == '3' ) ?> >
								<?= lang('isi-st-bayar-tempo-nodp') ?>
							</option>
							<option value="4" <?= set_select('metode','4',isset($metode) && $metode == '4' ) ?> >
								<?= lang('isi-st-bayar-kontan-jadi') ?>
							</option>
						</select>
					</div>
					
				</div>

				<div class="form-group" style="margin-bottom: 5px">
					<label for="status" class="col-sm-4 control-label"><?= lang("capt-filter-status") ?></label>
					<div class="col-sm-7">
						<select class="form-control" id="status" name="status">
							<option></option>
							<option value="0" <?= set_select('status','0',isset($status) && $status == '0') ?> >
								<?= lang("isi-filter-status-utang") ?>
							</option>
							<option value="1" <?= set_select('status','1',isset($status) && $status == '1') ?> >
								<?= lang("isi-filter-status-lunas") ?>
							</option>
						</select>						
					</div>
				</div>


			</div>	

			<div class="col-sm-6">			

				<div class="form-group" style="margin-bottom: 5px">
					<label for="waktu_tempo" class="col-sm-4 control-label"><?= lang("capt-filter-waktu-tempo") ?></label>
					<div class="col-sm-7">
						<div class="input-group input-daterange">
							<input type="text" name="tgl1_tempo" id="tgl1_tempo" class="form-control" readonly placeholder="<?= lang('capt-seacrh-order-tgl1') ?>" value = "<?= set_value('tgl1_tempo',isset($tgl1_tempo) ? $tgl1_tempo : '') ?>" >
							<span class="input-group-addon">to</span>
							<input type="text" name="tgl2_tempo" id="tgl2_tempo" class="form-control" readonly placeholder="<?= lang('capt-seacrh-order-tgl2') ?>" value = "<?= set_value('tgl2_tempo',isset($tgl2_tempo) ? $tgl2_tempo : '') ?>" >
						</div>
					</div>
				</div>

				<div class="form-group" style="margin-bottom: 5px">
					<label for="no_faktur" class="col-sm-4 control-label"><?= lang("capt-filter-no-trans-sub") ?></label>
					<div class="col-sm-7">
						<input type="text" name="no_faktur" id="no_faktur" class="form-control" value="<?= set_value('no_faktur',isset($no_faktur) ? $no_faktur : '') ?>">
					</div>
				</div>	

				<div class="form-group" style="margin-bottom: 5px">
					<label for="no_utama" class="col-sm-4 control-label"><?= lang("capt-filter-no-trans-utama") ?></label>
					<div class="col-sm-7">
						<input type="text" name="no_utama" id="no_utama" class="form-control" value="<?= set_value('no_utama',isset($no_utama) ? $no_utama : '') ?>">
					</div>
				</div>	


			</div>

		</div>

		<div class="box-footer">			

				<?php if($ENABLE_ADD){ ?>

					<a href="<?= site_url('pecah_faktur/create') ?>" class='btn btn-success'>
						<?= lang('btn_create') ?>
					</a>	

				<?php } ?>

			<div class="pull-right">

				<div class="form-group">
					<div class="input-group">
						<input type="text" name="search" id="search" class="form-control" placeholder="<?= lang("capt-filter-uni-nofaktur") ?>" >
						<div class="input-group-btn">
							<button class="btn btn-primary"><?= lang("btn_filter") ?> <span class="fa fa-search"></span></button>
						</div>
					</div>
				</div>

			</div>

			

				
		</div>

		
		
	</div>

	<?php 
			if(isset($data) && is_array($data) && count($data)) :
	?>
	<div class="box-body table-responsive">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
					<th class="text-center" width="30">#</th>
					<th class="text-center" ><?= lang("capt-trans-waktu") ?></th>
					<th class="text-center" ><?= lang("capt-trans-no-faktur") ?></th>
					<th class="text-center" ><?= lang("capt-trans-no-faktur-utama") ?></th>
					<th class="text-center" ><?= lang("capt-trans-konsumen") ?></th>
					<th class="text-center" ><?= lang("capt-trans-st-bayar") ?></th>
					<th class="text-center" ><?= lang("capt-trans-waktu-tempo") ?></th>
					<th class="text-center" ><?= lang("capt-trans-status") ?></th>
					<th class="text-center" ><?= lang("capt-trans-total") ?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($data as $key => $isi) :
				?>
					<tr>
						<td>
							<input type="checkbox" name="checked[]" value="<?= $isi->id ?>">
						</td>
						<td class="text-center" >
							<input type="hidden" name="dft_id_transaksi[]" id="dft_id_transaksi" value="<?= $isi->idsub_transaksi ?>">
							<?= $numb ?>
						</td>
						<td class="text-center" ><?= $isi->waktu ?></td>
						<td class="text-center" ><?= $isi->nofaktur_sub ?></td>
						<td class="text-center" ><?= $isi->nofaktur_utama ?></td>
						<td class="text-center" >
							<?= ucwords($isi->panggilan." ".$isi->nama) ?>
						</td>
						<td class="text-center">
							<?php
								switch ($isi->st_metode_bayar) {
									case 0:
										echo "<span class='label label-success'>". lang('isi-st-bayar-kontan'). "</span>";
										break;
									case 1:
										echo "<span class='label label-warning'>". lang('isi-st-bayar-dp-last'). "</span>";
										break;
									case 2:
										echo "<span class='label label-warning'>". lang('isi-st-bayar-tempo-dp'). "</span>";
										break;		
									case 3:
										echo "<span class='label label-danger'>". lang('isi-st-bayar-tempo-nodp'). "</span>";
										break;
									case 4:
										echo "<span class='label label-danger'>". lang('isi-st-bayar-kontan-jadi'). "</span>";
										break;		
								}

							?>
						</td>
						<td class="text-center" ><?= $isi->tgl_tempo ?></td>
						<td class="text-center" >
							<?php
								if($isi->st_lunas):
							?>
								<span class="label label-success"><?= lang("capt-label-lunas") ?></span>
							<?php
								else:
							?>
								<span class="label label-danger"><?= lang("capt-label-blm-lunas") ?></span>
							<?php endif ?>
						</td>
						<td class="text-center" ><?= number_format($isi->grand_total) ?></td>
						<td>
							<a class="text-black" data-id="<?= $isi->id ?>" onclick="cetak(this,event)" href="#tbl-cetak" data-toggle="tooltip" data-placement="left" title="Cetak Nota"><i class="fa fa-print"></i></a> 
							<!-- &nbsp; | &nbsp;
							<a class="text-black" href="<?= site_url('kasir/view/' . $isi->id); ?>" data-toggle="tooltip" data-placement="left" title="Lihat Transaksi"><i class="fa fa-folder-open"></i></a> 
							  -->
							
						</td>

					</tr>
				<?php
						$numb++;
					endforeach;
				?>
			</tbody>
		</table>
	</div>

	<div class="box box-footer clearfix">
		<?php if($ENABLE_DELETE) : ?>
		<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('btn_delete') ?>" onclick="return confirm('<?= (lang('konfirmasi-delete')); ?>')">
		<?php endif;
		echo $this->pagination->create_links(); 
		?>	
	</div>
	<?php
			else:
	?>
		<div class="alert alert-info" role="alert">
        	<p><i class="fa fa-warning"></i> &nbsp; <?= lang('konfirmasi-data-tidak-ada') ?></p>
    	</div>
	<?php			
			endif;
	?>

<?= form_close() ?>
</div>



<!-- Modal -->
<div id="myModal" class="modal fade in" role="dialog">
  	<div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	    	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal">&times;</button>
	        	<h4 class="modal-title">Cetak Nota</h4>
	      	</div>
	      	<div class="modal-body form-horizontal">
	        	<input type="hidden" name="id_subtrans_cetak" id="id_subtrans_cetak">
	        	<div class="form-group">
	        		<label class="control-label col-sm-3" for="tipe_nota" ><?= lang("capt-modal-jns-nota") ?></label>
	        		<div class="col-sm-9">
	        			<select class="form-control" name="tipe_nota" id="tipe_nota">
	        				<option value="0" selected ><?= lang("isi-jns-nota-besar") ?></option>
	        				<option value="1"><?= lang("isi-jns-nota-kecil") ?></option>
	        			</select>
	        		</div>
	        	</div>
	        	
	      	</div>
	      	<div class="modal-footer">
	      		<a href="#" id="cetak" class="btn btn-default" class="text-black" data-toggle="tooltip" data-placement="left" title="Cetak Nota"> <?= lang("capt-print") ?> </a>	

	      	</div>
		    	
	    </div>

  	</div>
</div>
