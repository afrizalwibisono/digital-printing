<?php

	foreach ($data as $key => $isi):
		
		if($key>0):
?>
	<div style="page-break-after: always;"></div>
<?php		
	endif;
?>

<table class="struk-kecil">
	<thead>
		<tr>
			<th colspan="6"><?= $idt->nm_perusahaan ?></th>
		</tr>
		<tr>
			<th colspan="6"><?= $idt->alamat ?>, <?= $idt->kota ?></th>
		</tr>
		<tr>
			<th colspan="6">Telp. <?= $idt->no_telp ?></th>
		</tr>
		<tr class="no-tran">
			<th colspan="6"><?= $isi['no_faktur'] ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
			
			foreach ($isi['detail'] as $key => $d) :
		?>

		<tr>
			<td colspan="6"><?= ucfirst($d['produk']) ?></td>
		</tr>
		<tr>
			<td align="10"><?= $d['jml']."x" ?></td>
			<td></td>
			<td><?= $d['ukuran'] ?></td>
			<td></td>
			<td></td>
			<td><?= number_format($d['subtotal']) ?></td>
		</tr>

		<?php
			endforeach;
		?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4"><?= lang('capt-table-final-total') ?></td>
			<td>:</td>
			<td><?= number_format($isi['total']) ?></td>
		</tr>
		<tr>
			<td colspan="4"><?= lang('capt-table-final-potongan') ?></td>
			<td>:</td>
			<td><?= number_format($isi['potongan']) ?></td>
		</tr>
		<tr>
			<td colspan="4"><?= lang('capt-table-final-diskon') ?></td>
			<td>:</td>
			<td><?= number_format($isi['diskon']) ?></td>
		</tr>
		<tr>
			<td colspan="4"><?= lang('capt-table-final-pajak') ?></td>
			<td>:</td>
			<td><?= $isi['pajak'] ?></td>
		</tr>
		<tr>
			<td colspan="4"><?= lang('capt-table-final-gt') ?></td>
			<td>:</td>
			<td><b><?= number_format($isi['gtotal']) ?></b></td>
		</tr>
		<tr>
			<td colspan="4"><?= lang('capt-table-final-bayar') ?></td>
			<td>:</td>
			<td><b><?= number_format($isi['bayar']) ?></b></td>
		</tr>
		<tr>
			<td colspan="4"><?= lang('capt-table-final-kembali') ?></td>
			<td>:</td>
			<td>
				<?php 

					$kembali 	= $isi['bayar'] >= $isi['gtotal'] ? $isi['bayar'] - $isi['gtotal'] : 0;
					echo number_format($kembali);

				?>
				
			</td>
		</tr>
		<tr>
			<td colspan="5" class="footer-note">===== Terima Kasih =====</td>
		</tr>
	</tfoot>
</table>

<?php
		
		endforeach;

?>

<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function(e){
		window.print();
		//window.close();
	});
</script>	
