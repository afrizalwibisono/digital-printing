<?php defined('BASEPATH')|| exit('No direct script access allowed');


// ============================================================
$lang['title']							= 'Data Pecah Faktur';
$lang['title_new']						= 'Pecah Faktur Baru';
$lang['title_view']						= 'Faktur';

// ======== index trans =======================================
$lang['capt-seacrh-order-tgl1']			= 'Tanggal Awal';
$lang['capt-seacrh-order-tgl2']			= 'Tanggal Akhir';

$lang['capt-trans-waktu']				= 'Tgl. Buat';
$lang['capt-trans-no-faktur']			= 'No. Faktur';
$lang['capt-trans-no-faktur-utama']		= 'No. Faktur Utama';
$lang['capt-trans-waktu-tempo']			= 'Tgl. Tempo';
$lang['capt-trans-konsumen']			= 'Konsumen';
$lang['capt-trans-total']				= 'Total';
$lang['capt-trans-st-bayar']			= 'Metode';
$lang['capt-trans-status']				= 'Status';
	
	$lang['capt-label-lunas']			= 'Lunas';
	$lang['capt-label-blm-lunas']		= 'Tertanggung';

//---------------------- Filter Index --------------------------
$lang['capt-filter-waktu']				= 'Range Tanggal';
$lang['capt-filter-konsumen']			= 'Konsumen';
$lang['capt-filter-metode']				= 'Metode Transaksi';
$lang['capt-filter-waktu-tempo']		= 'Range Tgl Tempo';
$lang['capt-filter-status']				= 'Status Transaksi';
	
	$lang['isi-filter-status-lunas']	= 'Lunas';
	$lang['isi-filter-status-utang']	= 'Tertanggung';

$lang['capt-filter-no-trans-utama']		= 'No Faktur Utama';
$lang['capt-filter-no-trans-sub']		= 'No Faktur Pecahan';

$lang['capt-filter-uni-nofaktur']		= 'No Faktur';


//==== capt form trans pecah =========

$lang['capt-trans-no-faktur']			= "No. Faktur Utama";
$lang['capt-trans-jml-pecah']			= "Dipecah Menjadi";
$lang['capt-trans-nota']				= "Faktur / Nota";
$lang['capt-modal-jns-nota']			= "Jenis Nota";
	
	$lang['isi-jns-nota-besar']			= "Nota Besar";
	$lang['isi-jns-nota-kecil']			= "Nota Kecil";

$lang['capt-print']						= "Print";

//=== capt cetakan nota ===
$lang['capt-nota-no-faktur']			= "No Faktur";
$lang['capt-nota-tgl-trans']			= "Tgl Trans";
$lang['capt-nota-konsumen']				= "Konsumen";
$lang['capt-nota-st-faktur']			= "Status";
$lang['capt-nota-lama-tempo']			= "Lama Tempo";
$lang['capt-nota-tgl-tempo']			= "Tgl Tempo";

$lang['capt-table-nm-file']				= "Nama File";
$lang['capt-table-nm-produk']			= "Produk";
$lang['capt-table-ukuran']				= "Ukuran";
$lang['capt-table-jml']					= "Qty";
$lang['capt-table-total']				= "Total";
$lang['capt-table-potongan']			= "Potongan";
$lang['capt-table-sub-total']			= "Sub Total";

$lang['capt-table-final-total']			= "Total";
$lang['capt-table-final-potongan']		= "Potongan";
$lang['capt-table-final-diskon']		= "Diskon";
$lang['capt-table-final-pajak']			= "Pajak";
$lang['capt-table-final-gt']			= "Grand Total";
$lang['capt-table-final-bayar']			= "Bayar";
$lang['capt-table-final-kembali']		= "Kembali";

$lang['capt-nota-terbilang']			= "Terbilang";

$lang['capt-table-ttd-hormat']			= "Hormat Kami";
$lang['capt-table-ttd-penerima']		= "Penerima";
$lang['capt-table-ttd-op']				= "Operator / Kasir";


//==== Capt form inputan trans manual ====

$lang['capt-trans-no-order']			= "No. Order";
$lang['capt-trans-konsumen']			= "Konsumen";
$lang['capt-trans-tglorder']			= "Tgl Order";
$lang['capt-trans-tglselesai']			= "Tgl Req. Selesai";
$lang['capt-trans-st-bayar']			= "Status Pembayaran";

	$lang['isi-st-bayar-kontan']		= "Dibayar lunas dimuka";
	$lang['isi-st-bayar-dp-last']		= "DP, sisanya setelah selesai";
	$lang['isi-st-bayar-tempo-dp']		= "Tempo, dengan DP";
	$lang['isi-st-bayar-tempo-nodp']	= "Tempo, tanpa DP";
	$lang['isi-st-bayar-kontan-jadi']	= "Dibayar lunas setelah jadi";

$lang['capt-trans-tempo']				= "Jatuh Tempo";
$lang['capt-trans-tempo-hari']			= "Hari";
$lang['capt-trans-bayar']				= "Nominal Bayar / DP";
$lang['capt-trans-kurang-bayar']		= "Kurang Bayar";
$lang['capt-trans-lebih-bayar']			= "Kembalian";
$lang['capt-trans-st-produksi']			= "Status Produksi";
	
	$lang['isi-st-produksi-tunda']		= "Tunda produksi";
	$lang['isi-st-produksi-masuk']		= "Masuk produksi";

$lang['capt-image-null']				= "Gambar tidak ditemukan";

$lang['capt-trans-cetak-kecil']			= "Cetak Nota Kecil";
$lang['capt-trans-cetak-besar']			= "Cetak Nota Besar";

// ======= tabel form ==========

$lang['capt-trans-table-deskripsi']		= "Deskripsi";
$lang['capt-trans-table-jml-cetak']		= "Qty";
$lang['capt-trans-table-rp-cetak']		= "Biaya Cetak";
$lang['capt-trans-table-rp-finishing']	= "Biaya Finishing";
$lang['capt-trans-table-diskon']		= "Diskon";
$lang['capt-trans-table-potongan']		= "Potongan";
$lang['capt-trans-table-subtotal']		= "Sub Total";

$lang['capt-trans-total-all']			= "Total";
$lang['capt-trans-diskon-all']			= "Diskon(%)";
$lang['capt-trans-pot-all']				= "Potongan(Rp.)";
$lang['capt-trans-pajak-all']			= "Pajak(%)";
$lang['capt-trans-grandtotal-all']		= "Grant Total";

//==============================
$lang['btn_create']						= 'Baru';
$lang['btn_delete']						= 'Hapus';
$lang['btn_filter']						= 'Filter';

// =============================
$lang['btn-save']						= "Simpan / Proses";
$lang['btn-batal']						= "Batal / Kembali";
$lang['btn-delete']						= "Hapus";

$lang['bf_or']							= "or";

// =============================
$lang['label_simpan_sukses'] 			= "Simpan Data Transaksi baru sukses";
$lang['label_cancel_sukses']			= "Data Transaksi telah berhasil dibatalkan";

$lang['label-err-nol-harga-cetak']		= "Harga cetak untuk record ke - %u tidak boleh Nol(0).";
$lang['label-err-bayar-st-0']			= "Bayar tidak boleh kurang dari Grant Total";
$lang['label-err-bayar-st-1-2']			= "Bayar tidak boleh sama / lebih dari Grant Total";

// =============================
$lang['konfirmasi-delete']				= "Semua data Pecah Faktur yang berasal dari Transaksi yang sama akan dihapus. Apakah anda yakin akan menghapus data Pecah Faktur terpilih ? ";
$lang['konfirmasi-data-tidak-ada']		= "Data tidak ditemukan";
$lang['konfirmasi-error-pil-delete']	= "Tidak ada data Pecahan Faktur yang dipilih";
$lang['konfirmasi-delete-sukses']		= "Delete Data Pecah Faktur sukses";



