$(function(){

	$("#konsumen").select2({
        placeholder :"-- Pilih Konsumen --",
        allowClear  : true
  	});

  	$("#metode").select2({
        placeholder :"-- Pilih Metode Pembayaran --",
        allowClear  : true
  	});

	$("#status").select2({
        placeholder :"-- Pilih Status Pembayaran --",
        allowClear  : true
  	});  	

  	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});


	$('.input-daterange-tempo').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});


	$("#cetak").click(function(){

		tombol_cetak();

	});


});

function cetak(obj,e){

	e.preventDefault();

	var id 	= $(obj).closest("tr").find("input[name='dft_id_transaksi[]']").val();
	$("#id_subtrans_cetak").val(id);

	$("#myModal").modal("show");


}

function tombol_cetak(){

	var id_subtrans = $("#id_subtrans_cetak").val();
	var tipe_nota 	= $("#tipe_nota").val();

	var url 		= baseurl+"pecah_faktur/cetak_invoice?tipe="+ tipe_nota +"&id_transaksi=&id_subtrans="+ id_subtrans;

	if(tipe_nota == 0){

		window.open(url, '_blank', 'width=744,height=600,scrollbars=yes,menubar=no,status=yes,resizable=yes,screenx=100,screeny=100'); return false; 
	
	}else{

		window.open(url, '_blank', 'width=310,height=400,scrollbars=yes,menubar=no,status=yes,resizable=yes,screenx=100,screeny=100'); return false;

	}

}