<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for pecah faktur

*/
class Pecah_faktur extends Admin_Controller {

	protected $viewPermission   		= "Pecah Faktur.View";
    protected $addPermission    		= "Pecah Faktur.Add";
    protected $deletePermission 		= "Pecah Faktur.Delete";

	protected $prefixKey        		= "PF-";

    public function __construct(){

        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('pecah_faktur/pecah_faktur');
        $this->load->model(array(
        							"order_model",
        							"order_detail_model",
        							"trans_model",
        							"trans_detail_model",
        							"konsumen_model",
                                    "subtrans_model",
                                    "subtrans_detail_model",
                                    "pelunasan_detail_model"
        						)
    						);

        $this->template->title(lang('title'));
		$this->template->page_icon('fa fa-list');
    }

    public function index(){

        $this->auth->restrict($this->viewPermission);

        if(isset($_POST['delete']) && has_permission($this->deletePermission)){

            if($this->delete()){

                $this->template->set_message(lang("konfirmasi-delete-sukses"),"success");

            }else{

                $this->template->set_message(lang("konfirmasi-error-pil-delete"),"error");

            }

        }

        $this->load->library('pagination');   

        $filter     = "?search=''";

        $where  = "sub_transaksi.deleted=0";

        if(isset($_POST['search'])){

            $search         = isset($_POST['search']) ? $this->input->post('search') : '';
            $tgl1_trans     = isset($_POST['tgl1']) ? $this->input->post('tgl1') : '';
            $tgl2_trans     = isset($_POST['tgl2']) ? $this->input->post('tgl2') : '';

            $tgl1_tempo     = isset($_POST['tgl1_tempo']) ? $this->input->post('tgl1_tempo') : '';
            $tgl2_tempo     = isset($_POST['tgl2_tempo']) ? $this->input->post('tgl2_tempo') : '';

            $konsumen       = isset($_POST['konsumen']) ? $this->input->post('konsumen') : '';
            $metode         = isset($_POST['metode']) ? $this->input->post('metode') : '';
            $status         = isset($_POST['status']) ? $this->input->post('status') : '';

            $no_faktur      = isset($_POST['no_faktur']) ? $this->input->post('no_faktur') : '';
            $no_utama       = isset($_POST['no_utama']) ? $this->input->post('no_utama') : '';

        }else if(isset($_GET['search'])){

            $search         = isset($_GET['search']) ? $this->input->get('search') : '';
            $tgl1_trans     = isset($_GET['tgl1']) ? $this->input->get('tgl1') : '';
            $tgl2_trans     = isset($_GET['tgl2']) ? $this->input->get('tgl2') : '';

            $tgl1_tempo     = isset($_GET['tgl1_tempo']) ? $this->input->get('tgl1_tempo') : '';
            $tgl2_tempo     = isset($_GET['tgl2_tempo']) ? $this->input->get('tgl2_tempo') : '';

            $konsumen       = isset($_GET['konsumen']) ? $this->input->get('konsumen') : '';
            $metode         = isset($_GET['metode']) ? $this->input->get('metode') : '';
            $status         = isset($_GET['status']) ? $this->input->get('status') : '';

            $no_faktur      = isset($_GET['no_faktur']) ? $this->input->get('no_faktur') : '';
            $no_utama       = isset($_GET['no_utama']) ? $this->input->get('no_utama') : '';

        }else{

            $tgl1_trans     = date("d/m/Y");
            $tgl2_trans     = date("d/m/Y");            

            $search         = '';
            
            $tgl1_tempo     = '';
            $tgl2_tempo     = '';

            $konsumen       = '';
            $metode         = '';
            $status         = '';

            $no_faktur      = '';
            $no_utama       = '';

        }




        if(strlen($tgl1_trans) <= 0 && strlen($tgl2_trans) <= 0 && strlen($tgl1_tempo) <= 0 && strlen($tgl2_tempo) <= 0 && strlen($konsumen) <= 0 && strlen($metode) <= 0 && strlen($status) <= 0 && strlen($no_faktur) <= 0 && strlen($no_utama) <= 0 && strlen($search) > 0){

            $search     = $this->db->escape_str($search);

            $where      .= " and `sub_transaksi`.`no_faktur` like '{$search}%' or `transaksi`.`no_faktur` like '{$search}%' ";

            
        }else{

            if(strlen($tgl1_trans)>0){

                $tgl1   = date_ymd($tgl1_trans)." 01:00:00";
                $tgl2   = date_ymd($tgl2_trans)." 23:59:59" ;

                $where  .= " and (`sub_transaksi`.`created_on` >= '{$tgl1}' and `sub_transaksi`.`created_on` <= '{$tgl2}')";

                
            }


            if(strlen($tgl1_tempo)>0){

                $tgl1   = date_ymd($tgl1_tempo);
                $tgl2   = date_ymd($tgl2_tempo);

                $where  .= " and (`sub_transaksi`.`tgl_tempo` >= '{$tgl1}' and `sub_transaksi`.`tgl_tempo` <= '{$tgl2}')";                        



            }

            if(strlen($konsumen)>0 && strlen($tgl1_trans)>0){

                $where  .=  " and `konsumen`.`idkonsumen` = '{$konsumen}'";

            }

            if(strlen($metode)>0 && strlen($tgl1_trans)>0){            

                $where  .=  " and `sub_transaksi`.`st_metode_bayar` = {$metode}";

            }

            if(strlen($status)>0 && strlen($tgl1_trans)>0){            

                $where  .=  " and `sub_transaksi`.`st_lunas` = {$status}";

            }

            if(strlen($no_faktur) > 0){

                $where  = "`sub_transaksi`.`no_faktur` = '{$no_faktur}'";

            }

            if(strlen($no_utama) > 0){

                $where  = "`transaksi`.`no_faktur` = '{$no_utama}'";

            }            


        }

        $filter     = "?search={$search}&tgl1={$tgl1_trans}&tgl2={$tgl2_trans}&tgl1_tempo={$tgl1_tempo}&tgl2_tempo={$tgl2_tempo}&metode={$metode}&status={$status}&no_faktur={$no_faktur}&no_utama={$no_utama}";

        $total      = $this->subtrans_model->select("sub_transaksi.idsub_transaksi")
                            ->join("transaksi","sub_transaksi.id_transaksi = transaksi.id_transaksi","inner")
                            ->join("order_produk","sub_transaksi.id_order = order_produk.id_order","inner")
                            ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                            ->where($where)
                            ->count_all();            


        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $dt_konsumen    = $this->konsumen_model
                                ->select("`idkonsumen` as id, 
                                        CONCAT(IF(panggilan IS NULL, '', panggilan),
                                        ' ',
                                        `nama`,
                                        ' | ',
                                        IF(st = 0,
                                            'konsumen',
                                            IF(st = 1, 'reseller', 'instansi'))) AS nm")
                                ->where("deleted=0")
                                ->order_by("nama","asc")
                                ->find_all();


        $data   = $this->subtrans_model
                            ->select("`sub_transaksi`.`idsub_transaksi`,
                                        `sub_transaksi`.`id_transaksi` as id,
                                        date_format(`sub_transaksi`.`created_on`,'%d/%m/%Y') as waktu,
                                        `sub_transaksi`.`no_faktur` AS nofaktur_sub,
                                        `transaksi`.`no_faktur` AS nofaktur_utama,
                                        `konsumen`.`panggilan`,
                                        `konsumen`.`nama`,
                                        `sub_transaksi`.`st_metode_bayar`,
                                        date_format(`sub_transaksi`.`tgl_tempo`,'%d/%m/%Y') as tgl_tempo,
                                        `sub_transaksi`.`st_lunas`,
                                        `sub_transaksi`.`grand_total`")
                            ->join("transaksi","sub_transaksi.id_transaksi = transaksi.id_transaksi","inner")
                            ->join("order_produk","sub_transaksi.id_order = order_produk.id_order","inner")
                            ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                            ->where($where)
                            ->order_by("`sub_transaksi`.`no_faktur`","asc")
                            ->limit($limit,$offset)
                            ->find_all();

        $asset  =   array(
                            "plugins/select2/js/select2.js",
                            "plugins/select2/css/select2.css",
                            'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                            'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                            'pecah_faktur/assets/js/index_pf.js'
                    );

        add_assets($asset);

        $this->template->set('numb',$offset+1);        

        $this->template->set('tgl1',$tgl1_trans);
        $this->template->set('tgl2',$tgl2_trans);

        $this->template->set('tgl1_tempo',$tgl1_tempo);        
        $this->template->set('tgl2_tempo',$tgl2_tempo);
        $this->template->set('konsumen',$konsumen);
        $this->template->set('metode',$metode);
        $this->template->set('status',$status);
        $this->template->set('no_faktur',$no_faktur);
        $this->template->set('no_utama',$no_utama);

        $this->template->set("data",$data);
        $this->template->set("dt_konsumen",$dt_konsumen);
        $this->template->set("toolbar_title", lang('title'));
        $this->template->set("page_title", lang('title'));
        $this->template->render('index'); 

    }


    public function delete(){

        if(isset($_POST['checked'])){

            $sql_all = "";

            $this->db->trans_start();

                $id         = $this->input->post('checked');
                $id_utama   = array();

                $id_sebelumnya  = "";

                foreach ($id as $key => $isi) {
                    
                    if($isi != $id_sebelumnya){

                        //cek apakah id_transaksinya, sudah dilunasi berdasarkan idsub_transaksinya
                        $where  = "deleted = 0 and `id_transaksi` = '{$isi}' and `st_transaksi` = 1";
                        $dt_ketemu  = $this->pelunasan_detail_model->where($where)->count_all();

                        if($dt_ketemu <= 0){

                            $this->subtrans_model->delete_where(
                                                            array('id_transaksi' => $isi)
                                                        );
                            $sql_all    .= $this->db->last_query()."\n\n";

                            $id_utama[]     = array(
                                                    'st_sub_nota'   => 0,
                                                    'id_transaksi'  => $isi
                                                    );


                        }

                        
                    }

                    $id_sebelumnya = $isi;
                    
                }

                //update status nota utama
                if(is_array($id_utama) && count($id_utama)){
                    
                    $this->trans_model->update_batch($id_utama,'id_transaksi');
                    $sql_all    .= $this->db->last_query()."\n\n";                

                }

            $this->db->trans_complete();

            if($this->db->trans_status() == false){

                $return         = 0;
                $keterangan     = "Gagal, delete data sub Transaksi";
                $total          = 0;
                $status         = 0;

            }else{

                $return         = 1;
                $keterangan     = "Sukses, delete data sub Transaksi";
                $total          = 0;
                $status         = 1;

            }

            $nm_hak_akses   = $this->deletePermission; 
            $kode_universal = "-";
            $jumlah         = 0;
            $sql            = $sql_all;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $total, $sql, $status);

        }else{

            $return = false;

        }

        return $return;

    }

    public function create(){

        $this->auth->restrict($this->addPermission);        

        $asset  = array(
                            "plugins/number/jquery.number.js",
                            "pecah_faktur/assets/js/form_pf.js"
                    );

        add_assets($asset);

        $this->template->set("toolbar_title", lang('title_new'));
        $this->template->set("page_title", lang('title_new'));
        $this->template->title(lang('title_new'));
        $this->template->page_icon('fa fa-clone');
        $this->template->render('form_pecah_faktur');         

    }

    function batal(){

        $this->template->set_message(lang("label_cancel_sukses"),"success");
        redirect('pecah_faktur');        

    }

    function save(){

        if(!$this->input->is_ajax_request()){

            redirect('pecah_faktur');

        } 

        $no_faktur      = $this->input->post("no_faktur");
        $jml_pecah      = $this->input->post("qty_pecah");

        //cek no_faktur     

        $data   = $this->trans_model->select('id_transaksi')->find_by(
                                                                        array(
                                                                                'no_faktur' => $no_faktur,
                                                                                'st_sub_nota'=> 0
                                                                            )   
                                                                    );

        if(isset($data->id_transaksi)){//jika ada datanya

            $id_trans   = $data->id_transaksi;

            $data_detail    = $this->trans_detail_model
                                    ->select("id_detail_produk_order,
                                                (biaya_cetak / {$jml_pecah}) as biaya_cetak,
                                                (biaya_finishing/ {$jml_pecah}) as biaya_finishing,
                                                diskon,
                                                (potongan / {$jml_pecah}) as potongan,
                                                ((biaya_cetak / {$jml_pecah}) + (biaya_finishing/ {$jml_pecah})) - 
                                                ((((biaya_cetak / {$jml_pecah}) + (biaya_finishing/ {$jml_pecah})) * (diskon / 100)) + (potongan / {$jml_pecah})) as subtotal")
                                    ->where("id_transaksi = '{$id_trans}' and deleted = 0")
                                    ->find_all();

            $data_trans     = $this->trans_model->select("id_transaksi,
                                                            no_faktur,
                                                            id_order,
                                                            lama_tempo,
                                                            tgl_tempo,
                                                            total,
                                                            diskon,
                                                            (potongan / {$jml_pecah}) as potongan,
                                                            pajak,
                                                            grand_total,
                                                            st_metode_bayar,
                                                            bayar,
                                                            st_produksi,
                                                            st_lunas")
                                                ->find($id_trans);


            $total              = 0;
            $detail_subtrans    = array();
            $sub_trans          = array();
            $bayar              = $data_trans->bayar;

            for ($i=0; $i < $jml_pecah ; $i++) { 
                
                $primary_subtrans   = gen_primary($this->prefixKey, "sub_transaksi", "idsub_transaksi");

                $total = 0;

                foreach ($data_detail as $key => $isi) {
                
                   $total  += $isi->subtotal;

                   $detail_subtrans[]   = array(
                                                'idsub_transaksi_detail'    => gen_primary("", "sub_transaksi_detail", "idsub_transaksi_detail"), 
                                                'idsub_transaksi'           => $primary_subtrans, 
                                                'id_detail_produk_order'    => $isi->id_detail_produk_order, 
                                                'biaya_cetak'               => $isi->biaya_cetak, 
                                                'biaya_finishing'           => $isi->biaya_finishing,      
                                                'diskon'                    => $isi->diskon, 
                                                'potongan'                  => $isi->potongan, 
                                                'subtotal'                  => $isi->subtotal
                                            );

                }

                $pot_all        = $data_trans->potongan;
                $diskon_all     = $total * ($data_trans->diskon / 100);
                $total_all      = $total - ($pot_all + $diskon_all);
                $pajak          = $total_all * ($data_trans->diskon / 100) ;
                $grand_total    = $total_all + $pajak;

                

                    if($grand_total >= $bayar){

                        $byr_pakai  = $bayar;
                        $bayar      = 0 ;
                        
                    }else{

                        $byr_pakai  = $grand_total;
                        $bayar      -= $grand_total ;

                    }


                $sub_trans[]    = array(
                                        'idsub_transaksi'   => $primary_subtrans, 
                                        'id_transaksi'      => $data_trans->id_transaksi, 
                                        'no_faktur'         => $data_trans->no_faktur."/".$i, 
                                        'id_order'          => $data_trans->id_order, 
                                        'lama_tempo'        => $data_trans->lama_tempo, 
                                        'tgl_tempo'         => $data_trans->tgl_tempo, 
                                        'total'             => $total, 
                                        'diskon'            => $data_trans->diskon, 
                                        'potongan'          => $data_trans->potongan, 
                                        'pajak'             => $data_trans->pajak, 
                                        'grand_total'       => $grand_total, 
                                        'st_metode_bayar'   => $data_trans->st_metode_bayar, 
                                        'bayar'             => $byr_pakai, 
                                        'st_lunas'          => $byr_pakai == $grand_total ? 1 : $data_trans->st_lunas
                                    );


            }

            //update status nota utama
            $arr_trans  = array(
                                    'st_sub_nota'   => 1
                            );

            $sql_all = "";

            $this->db->trans_start();

                $this->subtrans_model->insert_batch($sub_trans);
                $sql_all .= $this->db->last_query();

                $this->subtrans_detail_model->insert_batch($detail_subtrans);
                $sql_all .= "\n\n".$this->db->last_query();    

                $this->trans_model->update($id_trans, $arr_trans);
                $sql_all .= "\n\n".$this->db->last_query();    

            $this->db->trans_complete();

            if($this->db->trans_status() == false){

                $return         = 0;
                $keterangan     = "Gagal, simpan data sub Transaksi";
                $total          = 0;
                $status         = 0;

            }else{

                $return         = 1;
                $keterangan     = "Sukses, simpan data sub Transaksi";
                $total          = 0;
                $status         = 1;

            }

            $nm_hak_akses   = $this->addPermission; 
            $kode_universal = "-";
            $jumlah         = 0;
            $sql            = $sql_all;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $total, $sql, $status);


        }else{

            $return     = 0;

        }

        echo $return;

    }

    function cetak_invoice(){

        /*
            $tipe = 0 => Nota Besar
            $tipe = 1 => Nota Kecil
        */
        $tipe           = $this->input->get("tipe");
        $id_transaksi   = $this->input->get("id_transaksi");
        $id_subtrans    = $this->input->get("id_subtrans");

        $nm_file        = $tipe == 0 ? "nota_besar" : "nota_kecil";
        $layout         = $tipe == 0 ? "cetak" : "cetak_struk";

        if(strlen($id_transaksi) > 0 && strlen($id_subtrans) == 0){

            $data       = $this->trans_model->select('id_transaksi')->find_by('no_faktur', $id_transaksi);
            $id_trans   = $data->id_transaksi;
            $where      = "`sub_transaksi`.`id_transaksi` = '{$id_trans}' and sub_transaksi.deleted = 0";

        }else{

            $where  = "`sub_transaksi`.`idsub_transaksi` = '{$id_subtrans}' and sub_transaksi.deleted = 0";

        }

        $data   = $this->subtrans_model
                            ->select("
                                        `sub_transaksi`.`idsub_transaksi`,
                                        `sub_transaksi`.`no_faktur`,
                                        date_format(`sub_transaksi`.`created_on`,'%d/%m/%Y %H:%i:%s') as waktu,
                                        `konsumen`.`panggilan`,
                                        `konsumen`.`nama`,
                                        `sub_transaksi`.`st_metode_bayar` as st_bayar,
                                        `sub_transaksi`.`lama_tempo`,
                                        date_format(`sub_transaksi`.`tgl_tempo`,'%d/%m/%Y') as tgl_tempo,
                                        `sub_transaksi`.`total` as total_all,
                                        `sub_transaksi`.`diskon` as diskon_all,
                                        `sub_transaksi`.`potongan` as pot_all,
                                        `sub_transaksi`.`pajak` as pajak_all,
                                        `sub_transaksi`.`grand_total`,
                                        `sub_transaksi`.`bayar`,
                                        `sub_transaksi_detail`.`idsub_transaksi` as id_bag_detail,
                                        `order_produk_detail`.`nama_pekerjaan`,
                                        `produk`.`nm_produk`,
                                        `order_produk_detail`.`st_finishing`,
                                        `order_produk_detail`.`p`, 
                                        `order_produk_detail`.`l`, 
                                        `order_produk_detail`.`id_satuan`,
                                        `konversi_satuan`.`satuan_besar`,
                                        `order_produk_detail`.`jml_cetak`,
                                        `sub_transaksi_detail`.`biaya_cetak`,
                                        `sub_transaksi_detail`.`biaya_finishing`,
                                        `sub_transaksi_detail`.`diskon`,
                                        `sub_transaksi_detail`.`potongan`,
                                        `sub_transaksi_detail`.`subtotal`")
                            ->join("order_produk","sub_transaksi.id_order = order_produk.id_order","inner")
                            ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                            ->join("sub_transaksi_detail","sub_transaksi.idsub_transaksi = sub_transaksi_detail.idsub_transaksi","inner")
                            ->join("order_produk_detail","sub_transaksi_detail.id_detail_produk_order = order_produk_detail.id_detail_produk_order","inner")
                            ->join("produk","order_produk_detail.id_produk = produk.idproduk","inner")
                            ->join("konversi_satuan","order_produk_detail.id_satuan = konversi_satuan.id_konversi","inner")
                            ->where($where)
                            ->order_by("`sub_transaksi`.`no_faktur`","asc")
                            ->find_all();


        $idt_op = $this->auth->userdata();

        // tata array agar mudah ditampilkan

        $arr_head       = array();
        $arr_detail     = array();

        $no_faktur_now  = "";

        foreach ($data as $key => $isi) {

            if($isi->no_faktur != $no_faktur_now){ 
                 //jika no_faktur sekarang != no_faktur sebelumnya dan pada record > 1, atau record yang ke 0

                if($key>0){

                    
                    $pos                        = count($arr_head)-1;
                    $arr_head[$pos]['detail']   = $arr_detail;
                    $arr_detail                 = array(); //kosongkan untuk detail baru

                }


                $arr_head[]     = array(
                                            "id"            => $isi->idsub_transaksi,
                                            "no_faktur"     => $isi->no_faktur,
                                            "konsumen"      => $isi->panggilan." ".$isi->nama,
                                            "waktu"         => $isi->waktu,
                                            "status"        => $isi->st_bayar,
                                            "lama_tempo"    => $isi->lama_tempo." Hari",
                                            "tgl_tempo"     => $isi->tgl_tempo,
                                            "total"         => $isi->total_all,
                                            "potongan"      => $isi->pot_all,
                                            "diskon"        => $isi->diskon_all,
                                            "pajak"         => $isi->pajak_all,
                                            "gtotal"        => $isi->grand_total,
                                            "bayar"         => $isi->bayar
                                    );


            }

            $no_faktur_now = $isi->no_faktur;


            $total      = $isi->biaya_cetak + $isi->biaya_finishing;
            $diskon     = $total * ($isi->diskon / 100);
            $potongan   = $diskon + $isi->potongan;

            $arr_detail[]   = array(
                                        "idsub"         => $isi->id_bag_detail,
                                        "nm_file"       => $isi->nama_pekerjaan,
                                        "produk"        => $isi->nm_produk,
                                        "st_finishing"  => $isi->st_finishing,
                                        "ukuran"        => $isi->l ." x ". $isi->p." ".$isi->satuan_besar,
                                        "jml"           => $isi->jml_cetak,
                                        "total"         => $total,
                                        "potongan"      => $potongan,
                                        "subtotal"      => $isi->subtotal  
                                );


        }


        //jika masih ada data pada array detailnya maka masukkan
        if(count($arr_detail)){

            $pos                        = count($arr_head)-1;
            $arr_head[$pos]['detail']   = $arr_detail;
            $arr_detail                 = array(); //kosongkan untuk detail baru            

        }

        $identitas    = $this->identitas_model->find(1);
        
        $this->template->set('data',$arr_head);
        $this->template->set('idt', $identitas);
        $this->template->set('op', $idt_op);
        $this->template->set_layout($layout);
        $this->template->title(lang('title_view'));
        $this->template->render($nm_file);
                            

    }


}
