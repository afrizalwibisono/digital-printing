<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 * 
 * This is controller for Bahan Baku

 */

class Bahan_baku extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Bahan Baku.View";
    protected $addPermission    = "Bahan Baku.Add";
    protected $managePermission = "Bahan Baku.Manage";
    protected $deletePermission = "Bahan Baku.Delete";

    protected $prefix_key       = "BB";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('bahan_baku/bahan_baku');
        $this->load->model(
                            array(
                                    'bahan_baku_model',
                                    'merk_model',
                                    'jenis_model',
                                    'kategori_model',
                                    'satuan_terkecil_model',
                                    'produk_model',
                                    'stok_model'
                                )
                            );
        $this->template->title(lang('bahan_baku_title_manage'));
		$this->template->page_icon('fa fa-table');
    }

    public function index(){
        
        $this->auth->restrict($this->viewPermission);

        //delete data
        if(isset($_POST['delete']) && has_permission($this->deletePermission)){

            $delete = $this->delete();
            if($delete['status']){

                $pesan = $delete['jml']." ".lang('bahan_baku_deleted');
                $this->template->set_message($pesan,'success');

            }else{

                $pesan = $delete['jml']." ".lang("bahan_baku_fail_deleted");
                $this->template->set_message($pesan,'error');

            }

        }


        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
            $idkategori_search   = $this->input->post('idkategori_search');
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
            $idkategori_search   = $this->input->get('kategori');
        }

        $filter = "?search=".$search;

        $search2 = $this->db->escape_str($search);
        
        $addWhere = "";

        if($idkategori_search !=""){

            $addWhere .= "AND barang.idkategori= $idkategori_search ";
            $filter   .= "&kategori=".urldecode($idkategori_search);

        }
        
        $where  = "barang.deleted = 0 $addWhere
                    AND (`nm_barang` like '%$search2%' or `ket_barang` LIKE '%$search2%' ESCAPE '!')";

        
        $total  = $this->bahan_baku_model
                        ->where($where)
                        ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->bahan_baku_model->select("`barang`.`idbarang_bb`,
                                                    `barang`.`st_fix`,
                                                    `barang`.`nm_barang`,
                                                    `merk_bb`.`nm_merk_bb`,
                                                    `jenis_bb`.`nmjenis_bb`,
                                                    `kategori`.`nmkategori`,
                                                    `satuan_terkecil`.`alias`")
                        ->join('kategori',"barang.idkategori=kategori.idkategori","left")
                        ->join('jenis_bb',"barang.idjenis_bb=jenis_bb.idjenis_bb","left")
                        ->join('merk_bb',"barang.idmerk_bb=merk_bb.idmerk_bb","left")
                        ->join('satuan_terkecil',"barang.id_satuan_terkecil=satuan_terkecil.id_satuan_terkecil","left")
                        ->where($where)
                        ->order_by('`barang`.`idkategori`','ASC')
                        ->order_by('nm_barang','ASC')
                        ->limit($limit, $offset)->find_all();
        // die();

        $assets = array('plugins/select2/dist/js/select2.js', 
                        'plugins/select2/dist/css/select2.css',
                        'bahan_baku/assets/js/form_bahan_baku.js',
                        'plugins/number/jquery.number.js'
                        );
        
        add_assets($assets);

        $data_kategori  = $this->kategori_model->where("deleted",0)
                                                ->order_by("nmkategori","ASC")
                                                ->find_all();
        
        $this->template->set('data_kategori', $data_kategori);

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('kategori', $idkategori_search);

        $this->template->set("toolbar_title", lang('bahan_baku_title_manage'));
        $this->template->title(lang('bahan_baku_title_manage'));
        $this->template->set("numb", $offset+1);
        
        $this->template->render('index'); 
    }


    public function delete(){

        if(isset($_POST['checked'])){

            $sql = "";

            $this->db->trans_start();

            $jml_didelete = 0;

            foreach ($this->input->post('checked') as $key => $isi) {
                
                $cek    = $this->produk_model->select("`produk_detail_penyusun`.`idbarang_bb`")
                                ->join("produk_detail_penyusun","produk.idproduk = produk_detail_penyusun.idproduk","inner")
                                ->where("produk.deleted = 0
                                        AND produk_detail_penyusun.deleted = 0
                                        AND produk_detail_penyusun.idbarang_bb = '{$isi}'")
                                ->count_all();

                if($cek <= 0){

                    $this->bahan_baku_model->delete($isi);
                    $jml_didelete++;

                }

            }

            $this->db->trans_complete();            

            $return     = [];

            if($this->db->trans_status() == false){

                $return = ['status' => false, 'jml' => 0];

            }else{

                $return = ['status' => true, 'jml' => $jml_didelete];

            }

            return $return;
        }

    }


    public function create()
    {

        $this->auth->restrict($this->addPermission);
                    
        if (isset($_POST['save'])){

            if ($this->save()){

              $this->template->set_message(lang("bahan_baku_create_success"), 'success');
              redirect('bahan_baku');

            }
        }

        $data_merk = $this->merk_model->where("deleted",0)
                                    ->order_by("nm_merk_bb","ASC")
                                    ->find_all();
        

        $data_jenis = $this->jenis_model->where("deleted",0)
                                        ->order_by("nmjenis_bb","ASC")
                                        ->find_all();
        
        $data_kategori = $this->kategori_model->where("deleted",0)
                                            ->order_by("nmkategori","ASC")
                                            ->find_all();
        
        $data_satuan_terkecil = $this->satuan_terkecil_model->where("deleted",0)
                                                            ->order_by("nm_satuan","ASC")
                                                            ->find_all();
        

        $konversi = get_konversi_by_tipe(1,null);
        

        $assets = array('plugins/select2/dist/js/select2.js', 
                        'plugins/select2/dist/css/select2.css',
                        'plugins/number/jquery.number.js',
                        'bahan_baku/assets/js/form_bahan_baku.js'
                        );
        
        add_assets($assets);

        $this->template->set('data_merk', $data_merk);
        $this->template->set('data_jenis', $data_jenis);
        $this->template->set('data_kategori', $data_kategori);
        $this->template->set('data_satuan_terkecil', $data_satuan_terkecil);
        $this->template->set('data_satuan_spek', $konversi);
        $this->template->set("page_title", lang('bahan_baku_title_new'));
        $this->template->title(lang('bahan_baku_title_new'));

        $this->template->render('bahan_baku_form');
    }

    public function view($id){

        $this->auth->restrict($this->viewPermission);

        $data_barang    = $this->bahan_baku_model->select("`idbarang_bb`,
                                                            `barcode`,
                                                            `idkategori`,
                                                            `idjenis_bb`,
                                                            `idmerk_bb`,
                                                            `nm_barang`,
                                                            `id_satuan_terkecil`,
                                                            `ket_barang`,
                                                            `st_potong_meteran`,
                                                            `panjang_spek`,
                                                            `lebar_spek`,
                                                            `id_konversi`,
                                                            `ket_spek`,
                                                            st_hpp_manual,
                                                            hpp_manual")
                                                ->find($id);

        $data_merk = $this->merk_model->where("deleted",0)
                                    ->order_by("nm_merk_bb","ASC")
                                    ->find_all();
        

        $data_jenis = $this->jenis_model->where("deleted",0)
                                        ->order_by("nmjenis_bb","ASC")
                                        ->find_all();
        
        $data_kategori = $this->kategori_model->where("deleted",0)
                                            ->order_by("nmkategori","ASC")
                                            ->find_all();
        
        $data_satuan_terkecil = $this->satuan_terkecil_model->where("deleted",0)
                                                            ->order_by("nm_satuan","ASC")
                                                            ->find_all();
        

        $konversi = get_konversi_by_tipe(1,null);
        

        $assets = array('plugins/select2/dist/js/select2.js', 
                        'plugins/select2/dist/css/select2.css',
                        'plugins/number/jquery.number.js',
                        'bahan_baku/assets/js/form_bahan_baku.js'
                        );
        
        add_assets($assets);

        $this->template->set("st_view","1");
        $this->template->set("dt",$data_barang);
        $this->template->set('data_merk', $data_merk);
        $this->template->set('data_jenis', $data_jenis);
        $this->template->set('data_kategori', $data_kategori);
        $this->template->set('data_satuan_terkecil', $data_satuan_terkecil);
        $this->template->set('data_satuan_spek', $konversi);
        $this->template->set("page_title", lang('bahan_baku_title_new'));
        $this->template->title(lang('bahan_baku_title_new'));

        $this->template->render('bahan_baku_form');


    }

    public function edit($id){

        $this->auth->restrict($this->managePermission);

        if (isset($_POST['save'])){
            
            if ($this->save(1, $id)){

              $this->template->set_message(lang("bahan_baku_create_success"), 'success');

            }
        }

        $data_barang    = $this->bahan_baku_model->select("`idbarang_bb`,
                                                            `barcode`,
                                                            `idkategori`,
                                                            `idjenis_bb`,
                                                            `idmerk_bb`,
                                                            `nm_barang`,
                                                            `id_satuan_terkecil`,
                                                            `ket_barang`,
                                                            `st_potong_meteran`,
                                                            `panjang_spek`,
                                                            `lebar_spek`,
                                                            `id_konversi`,
                                                            `ket_spek`,
                                                            st_hpp_manual,
                                                            hpp_manual")
                                                ->find($id);

        $data_merk = $this->merk_model->where("deleted",0)
                                    ->order_by("nm_merk_bb","ASC")
                                    ->find_all();
        

        $data_jenis = $this->jenis_model->where("deleted",0)
                                        ->order_by("nmjenis_bb","ASC")
                                        ->find_all();
        
        $data_kategori = $this->kategori_model->where("deleted",0)
                                            ->order_by("nmkategori","ASC")
                                            ->find_all();
        
        $data_satuan_terkecil = $this->satuan_terkecil_model->where("deleted",0)
                                                            ->order_by("nm_satuan","ASC")
                                                            ->find_all();
        
        // start cek apakah bahan baku sudah memiliki stok

        $cek = $this->stok_model->where("id_barang = '{$id}'")->count_all();

        $ketemu_stok = 0;

        if($cek > 0){

            $ketemu_stok = 1;

        }

        // end cek apakah bahan baku sudah memiliki stok

        $konversi = get_konversi_by_tipe(1,null);
        

        $assets = array('plugins/select2/dist/js/select2.js', 
                        'plugins/select2/dist/css/select2.css',
                        'plugins/number/jquery.number.js',
                        'bahan_baku/assets/js/form_bahan_baku.js'
                        );
        
        add_assets($assets);

        $this->template->set("st_edit","1");
        $this->template->set("dt",$data_barang);
        $this->template->set('data_merk', $data_merk);
        $this->template->set('data_jenis', $data_jenis);
        $this->template->set('data_kategori', $data_kategori);
        $this->template->set('data_satuan_terkecil', $data_satuan_terkecil);
        $this->template->set('data_satuan_spek', $konversi);
        $this->template->set('ketemu_stok', $ketemu_stok);
        $this->template->set("page_title", lang('bahan_baku_title_new'));
        $this->template->title(lang('bahan_baku_title_new'));

        $this->template->render('bahan_baku_form');


    }


    function cek_barcode($kode_barcode = null, $id = null){

        if(strlen($kode_barcode) > 0){

            $where      = "deleted = 0 and barcode = '$kode_barcode' ";

            if($kode_barcode && $id){ //jika barcode dan id terisi

                $where  .= "and idbarang_bb <> '$id'";

            }

            $ketemu     = $this->bahan_baku_model->where($where)->count_all();

            if($ketemu > 0){

                return false;

            }else{

                return true;

            }

        }else{

            return true;
        }

    }

    protected function save($type=0, $id=null){ // 0 = baru

        if(!$id){

            $this->form_validation->set_rules('kategori','lang:idkategori','required|trim');
            $this->form_validation->set_rules('satuan_barang','lang:id_satuan_terkecil','required|trim');
            
        }

        $this->form_validation->set_rules('barcode','lang:barcode','trim');

        $this->form_validation->set_rules('jenis','lang:idjenis_bb','required|trim');
        $this->form_validation->set_rules('merk','lang:idmerk_bb','required|trim');
        $this->form_validation->set_rules('ket_barang','lang:bahan_baku_ket','required|trim');
        $this->form_validation->set_rules('nm_barang','lang:bahan_baku_nama','required|trim');
        $this->form_validation->set_rules('st_pakai','lang:st_potong_meteran','required|trim');


                
        if($_POST['st_pakai']==1){

            $this->form_validation->set_rules('satuan_spek','lang:id_konversi','required|trim');
            $this->form_validation->set_rules('spek_p','lang:p_spek','required|trim');
            $this->form_validation->set_rules('spek_l','lang:l_spek','required|trim');
            $this->form_validation->set_rules('ket_spek','lang:ket_spek','trim');

        }

        if ($this->form_validation->run() === FALSE){

            $this->template->set_message(validation_errors(), 'error');
            return FALSE;

        }else{ // jika semua inputan telah diisi

            //cek apakah angka pada inputan spek > 0

            if($_POST['st_pakai']==1){

                $p  = str_replace(",", "", $_POST['spek_p']);
                $l  = str_replace(",", "", $_POST['spek_l']);

                if($p <= 0 || $l <= 0){

                    $this->template->set_message(lang("error_spek_dimensi_0"),"error");
                    return false;

                }

            }

            //cek barcode apakah unique atau tidak
            $hasil  = $this->cek_barcode($_POST['barcode'], $id);

            if(!$hasil){

                $this->template->set_message(lang("konfirmasi-barcode-ditemukan"),"error");
                return false;

            }

        }

        $barcode            = $this->input->post("barcode");
        $kategori           = $this->input->post("kategori");
        $jenis              = $this->input->post("jenis");
        $merk               = $this->input->post("merk");
        $satuan_terkecil    = isset($_POST['satuan_barang']) ? $this->input->post("satuan_barang") : '';
        $ket_barang         = $this->input->post("ket_barang");
        $st_pakai           = $this->input->post("st_pakai");

        $nm_add_ukuran      = $st_pakai == 1 ? $this->input->post("spek_l") : '';

        $nm_barang          = $this->input->post("nm_barang")." ".$nm_add_ukuran;

        $spek_p             = str_replace(",", "", $this->input->post("spek_p"));
        $spek_l             = str_replace(",", "", $this->input->post("spek_l"));
        $satuan_spek        = $this->input->post("satuan_spek");
        $ket_spek           = $this->input->post("ket_spek");

        $hpp_manual         = str_replace(",", "", $this->input->post("harga_manual"));

        $sql                = "";

        $this->db->trans_start();
        
            if($type == 0){

                $primary            = gen_primary($this->prefix_key, "barang", "idbarang_bb");
                $arr_simpan         = array(
                                                'idbarang_bb'           => $primary, 
                                                'barcode'               => $barcode, 
                                                'idkategori'            => $kategori, 
                                                'idjenis_bb'            => $jenis, 
                                                'idmerk_bb'             => $merk, 
                                                'nm_barang'             => $nm_barang, 
                                                'id_satuan_terkecil'    => $satuan_terkecil, 
                                                'ket_barang'            => $ket_barang, 
                                                'st_potong_meteran'     => $st_pakai, 
                                                'panjang_spek'          => $spek_p, 
                                                'lebar_spek'            => $spek_l, 
                                                'id_konversi'           => $satuan_spek, 
                                                'ket_spek'              => $ket_spek
                                            );

                $this->bahan_baku_model->insert($arr_simpan);
                $sql    = $this->db->last_query();

            }else{ //jika edit

                if(strlen($satuan_terkecil) > 0){

                    $arr_simpan         = array(
                                                'barcode'               => $barcode, 
                                                'idjenis_bb'            => $jenis, 
                                                'idmerk_bb'             => $merk, 
                                                'nm_barang'             => $nm_barang, 
                                                'id_satuan_terkecil'    => $satuan_terkecil, 
                                                'ket_barang'            => $ket_barang, 
                                                'st_potong_meteran'     => $st_pakai, 
                                                'panjang_spek'          => $spek_p, 
                                                'lebar_spek'            => $spek_l, 
                                                'id_konversi'           => $satuan_spek, 
                                                'ket_spek'              => $ket_spek,
                                                'st_hpp_manual'         => isset($_POST['st_hpp_manual']) ? 1 : 0,
                                                'hpp_manual'            => isset($_POST['st_hpp_manual']) ? $hpp_manual : 0
                                            );

                }else{

                    $arr_simpan         = array(
                                                'barcode'               => $barcode, 
                                                'idjenis_bb'            => $jenis, 
                                                'idmerk_bb'             => $merk, 
                                                'nm_barang'             => $nm_barang, 
                                                'ket_barang'            => $ket_barang, 
                                                'panjang_spek'          => $spek_p, 
                                                'lebar_spek'            => $spek_l, 
                                                'id_konversi'           => $satuan_spek, 
                                                'ket_spek'              => $ket_spek,
                                                'st_hpp_manual'         => isset($_POST['st_hpp_manual']) ? 1 : 0,
                                                'hpp_manual'            => isset($_POST['st_hpp_manual']) ? $hpp_manual : 0
                                            );


                }

                

                $this->bahan_baku_model->update($id, $arr_simpan);
                $sql    = $this->db->last_query();

            }

        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            $return         = false;
            $keterangan     = "Gagal, tambah data bahan baku";
            $status         = 0;

        }else{

            $return         = true;
            $keterangan     = "Sukses, tambah data bahan baku";
            $status         = 1;

        }

        $nm_hak_akses   = $this->addPermission; 
        $kode_universal = "-";
        $jumlah         = 0;
            
        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        return $return;

    }

   
}
?>