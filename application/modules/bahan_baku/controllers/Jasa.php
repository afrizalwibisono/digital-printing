<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 * 
 * This is controller for Jasa

 */

class Jasa extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Jasa.View";
    protected $addPermission    = "Jasa.Add";
    protected $managePermission = "Jasa.Manage";
    protected $deletePermission = "Jasa.Delete";

    protected $prefix_key       = "JS";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('bahan_baku/jasa');
        $this->load->model(
                            array(
                                    'bahan_baku_model',
                                    'merk_model',
                                    'kategori_model',
                                    'satuan_terkecil_model',
                                )
                            );
        $this->template->title(lang('jasa_title_manage'));
		$this->template->page_icon('fa fa-table');
    }

    public function index(){
        
        $this->auth->restrict($this->viewPermission);
        
        if(isset($_POST['delete']) && has_permission($this->deletePermission)){

            $delete     = $this->delete();

            if($delete){

                $this->template->set_message(lang("pesan_sukses_delete"),"success");

            }

        }


        $search     = "";

        if(isset($_POST['search'])){

            $search     = isset($_POST['search']) ? $this->input->post('search') : '';

        }else{

            $search     = isset($_GET['search']) ? $this->input->get('search') : '';

        }

        $filter     = "";
        if($search != ""){
            $filter = "?search=".$search;
        }

        $search2    = $this->db->escape_str($search);

        $where      = "deleted = 0 and `st_barang` = 1 and `nm_barang` like '%{$search2}%'";

        $total      = $this->bahan_baku_model
                        ->select("`idbarang_bb`")
                        ->where($where)
                        ->count_all();

        $this->load->library('pagination');

        $offset     = $this->input->get('per_page');
        $limit      = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data   = $this->bahan_baku_model
                        ->select("`idbarang_bb`, `nm_barang`, `st_hitung_jasa`")
                        ->where($where)
                        ->order_by('nm_barang','asc')
                        ->find_all();

        $this->template->set("toolbar_title", lang('jasa_title_manage'));
        $this->template->title(lang('jasa_title_manage'));
        $this->template->set("numb",$offset+1);

        $this->template->set('data',$data);
        $this->template->render('jasa_index'); 
    }

    private function delete(){

        $this->auth->restrict($this->deletePermission);        

        $idpilih    = $this->input->post('checked');

        if(count($idpilih) <= 0){

            $this->template->set_message(lang("pesan_error_delete"),"error");
            return false;

        }

        $this->db->trans_start();

            foreach ($idpilih as $key => $id) {
                
                $this->bahan_baku_model->delete($id);

            }

        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            return false;

        }else{

            return true;

        }


    }

    public function create(){

        $this->auth->restrict($this->addPermission);

        if(isset($_POST['save'])){

            $simpan     = $this->simpan(null);

            if($simpan){

                $this->template->set_message(lang("pesan_sukses_simpan"),"success");
                redirect('bahan_baku/jasa');

            }

        }

        $this->template->set("toolbar_title", lang('jasa_title_new'));
        $this->template->title(lang('jasa_title_new'));
        
        $this->template->render('jasa_form');    

    }

    public function view($id = ""){

        $this->auth->restrict($this->managePermission);

        if(isset($_POST['save'])){

            $simpan     = $this->simpan($id);

            if($simpan){

                $this->template->set_message(lang("pesan_sukses_edit"),"success");

            }

        }


        $data   = $this->bahan_baku_model
                        ->select("`idbarang_bb`, `nm_barang`, `st_hitung_jasa`")
                        ->find($id);

        $this->template->set('data',$data);
        $this->template->set("toolbar_title", lang('jasa_title_edit'));
        $this->template->title(lang('jasa_title_edit'));
        
        $this->template->render('jasa_form');    

    }

    private function simpan($id = null){

        /*
            $tipe   =>  0   = baru.
                        1   = edit.  

        */

        $this->form_validation->set_rules('nm_jasa','lang:index-nm-jasa','required|trim');

        if ($this->form_validation->run() === FALSE){

            $this->template->set_message(validation_errors(), 'error');
            return FALSE;

        }

        $nm_jasa            = $this->input->post('nm_jasa');
        $st_jasa            = $this->input->post('st_jasa');

        $satuan_terkecil    = $st_jasa == 0 ? 1 : 7;

        $this->db->trans_start();

            if(!$id){ // tidak ada IDnya

                $isi    =   [
                                'idbarang_bb'           => gen_primary($this->prefix_key,'barang','idbarang_bb'), 
                                'id_satuan_terkecil'    => $satuan_terkecil,
                                'nm_barang'             => $nm_jasa, 
                                'st_barang'             => 1, 
                                'st_hitung_jasa'        => $st_jasa
                            ];

                $this->bahan_baku_model->insert($isi);                            
            
            }else{

                $isi    =   [
                                'id_satuan_terkecil'    => $satuan_terkecil, 
                                'st_hitung_jasa'        => $st_jasa,
                                'nm_barang'             => $nm_jasa 
                            ];  


                $this->bahan_baku_model->update_where('`idbarang_bb`',$id,$isi);

            }

        $this->db->trans_complete();        

        if($this->db->trans_status() == false){
            
            return false;

        }else{

            return true;

        }


    }

}
?>