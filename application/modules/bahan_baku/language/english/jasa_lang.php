<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ============================================================================================
$lang['jasa_title_manage']				= 'Data Jasa';
$lang['jasa_title_new']					= 'Data Jasa Baru';
$lang['jasa_title_edit']				= 'Edit Data Jasa';

// ============================================================================================
// ..:: Tombol :..

$lang['btn-new']						= 'Baru';
$lang['btn-delete']						= 'Hapus';
$lang['btn-save']						= 'Simpan';
$lang['btn-cancel']						= 'batal / kembali';
$lang['bf_or']							= 'or';

// ============================================================================================
//..:: Index ::..

$lang['index-nm-jasa']					= 'Jasa';
$lang['index-st-jasa']					= 'Unit Hitung';

// ============================================================================================
//..:: Form ::..
$lang['frm-isi-stjasa-unit']			= 'Unit / Kali';
$lang['frm-isi-stjasa-waktu']			= 'Waktu / Menit';

// ============================================================================================
// ..:: Form ::..

$lang['pesan_warning_hapus']			= 'Apakah anda yakin akan menghapus data Jasa terpilih?';

$lang['pesan_error_delete'] 			= 'Tidak ada data yang dipilih';


$lang['pesan_sukses_simpan'] 			= 'Data Jasa berhasil disimpan';
$lang['pesan_sukses_edit'] 				= 'Data Jasa berhasil diubah';

$lang['pesan_sukses_delete'] 			= 'Delete Data Jasa berhasil';
