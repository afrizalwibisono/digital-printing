<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ============================
$lang['bahan_baku_title_manage']		= 'Data Bahan Baku';
$lang['bahan_baku_title_new']			= 'Data Bahan Baku Baru';
$lang['bahan_baku_title_edit']			= 'Edit Data Bahan Baku';

// from/table
$lang['barcode']						= 'Barcode';
$lang['idkategori']						= 'Kategori';
$lang['bahan_baku_ket']					= 'Keterangan Barang';
$lang['bahan_baku_nama']				= 'Nama Barang';
$lang['idjenis_bb']						= 'Jenis Barang';
$lang['idmerk_bb']						= 'Merk Barang';
$lang['id_satuan_terkecil']				= 'Satuan Terkecil';
$lang['st_potong_meteran']				= 'Metode Penggunaan';
$lang['spek']							= 'Spesifikasi';
$lang['ukuran']							= 'Ukuran';
$lang['p_spek']							= 'Panjang Spesifikasi';
$lang['l_spek']							= 'Lebar Spesifikasi';
$lang['hpp_manual']						= 'HPP Manual';

$lang["isi_penggunaan_by_jumlah"] 		= 'Berdasarkan Satuan Jumlah';
$lang["isi_penggunaan_by_potongan"]		= 'Dipotong / Disesuaikan Dengan Ukuran(PxL) Order';

$lang['id_konversi']					= 'Dalam Satuan';
$lang['ket_spek']						= 'Keterangan Spek';

//button
$lang['bahan_baku_btn_new']				= 'Baru';
$lang['bahan_baku_btn_delete']			= 'Hapus';
$lang['bahan_baku_btn_save']			= 'Simpan';
$lang['bahan_baku_btn_cancel']			= 'Batal';
$lang['bf_or']							= 'or';

//messages
$lang['error_spek_dimensi_0']			= 'Ukuran Tidak Boleh 0';
$lang['konfirmasi-barcode-ditemukan']	= "Barcode yang sama telah terdaftar dalam system";

$lang['bahan_baku_del_error']			= 'Anda belum memilih data bahan baku yang akan dihapus.';
$lang['bahan_baku_del_failure']			= 'Tidak dapat menghapus data bahan baku';
$lang['bahan_baku_delete_confirm']		= 'Apakah anda yakin akan menghapus data bahan baku terpilih?';
$lang['bahan_baku_deleted']				= 'Data bahan baku berhasil dihapus';
$lang['bahan_baku_fail_deleted']		= 'Data bahan baku tidak dapat dihapus';
$lang['bahan_baku_no_records_found']	= 'Data tidak ditemukan.';

$lang['bahan_baku_create_failure']		= 'Data bahan baku gagal disimpan';
$lang['bahan_baku_create_success']		= 'Data bahan baku berhasil disimpan';

$lang['bahan_baku_edit_success']		= 'Data bahan baku berhasil disimpan';
$lang['bahan_baku_invalid_id']			= 'ID tidak Valid';

