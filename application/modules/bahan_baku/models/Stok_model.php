<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * @author CokesHome
 * @copyright Copyright (c) 2015, CokesHome
 * 
 * This is model class for table "stok"
 */

class Stok_model extends BF_Model
{
    /**
     * @var string  User Table Name
     */
    protected $table_name = 'stok';
    protected $key        = 'id_stok';

    /**
     * @var string Field name to use for the created time column in the DB table
     * if $set_created is enabled.
     */
    protected $created_field = 'created_on';

    /**
     * @var string Field name to use for the modified time column in the DB
     * table if $set_modified is enabled.
     */
    protected $modified_field = 'modified_on';

    /**
     * @var bool Set the created time automatically on a new record (if true)
     */
    protected $set_created = true;

    /**
     * @var bool Enable/Disable soft deletes.
     * If false, the delete() method will perform a delete of that row.
     * If true, the value in $deleted_field will be set to 1.
     */
    protected $soft_deletes = true;

    /**
     * @var bool Set the modified time automatically on editing a record (if true)
     */
    protected $set_modified = true;

    /**
     * @var string The type of date/time field used for $created_field and $modified_field.
     * Valid values are 'int', 'datetime', 'date'.
     */
    protected $date_format = 'datetime';
    //--------------------------------------------------------------------

    /**
     * @var bool If true, will log user id in $created_by_field, $modified_by_field,
     * and $deleted_by_field.
     */
    protected $log_user = true;

    /**
     * Function construct used to load some library, do some actions, etc.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Tampil Stok
    * @type_pencarian 0 = tampil semua barang[tidak ada kriteria], 1 = by Nama barang
    * 2 = Golongan Barang, 3 = Tipe barang, 4 = Merek barang, 5 = range Jumlah stok barang
    * ---Parameter----------------------
    * @min_stok = Stok minimal pencarian
    * @max_stok = Stok maksimal pencarian
    * @pencarian = text pencarian barang by, nama barang, merk barang, tipe barang, dan golongan barang
    * @id_gudang = Id Gudang
    * ---Return-------------------------
    * result data jika sukses, false jika gagal
    */
    public function tampil_stok($type_pencarian = 0, $min_stok = 0, $max_stok = 0, $pencarian = "", $id_gudang = 0)
    {
        $this->load->model('barang/barang_model');

        $data = $this->barang_model->select("barang.*, nm_jenis, nm_tipe, nm_merek, sum(stok_update) as stok, satuan_terkecil.alias as satuan_pendek, satuan_terkecil.nm_satuan")
                    ->join("stok","barang.id_barang = stok.id_barang","left")
                    ->join("jenis_barang","barang.id_jenis_barang = jenis_barang.id_jenis_barang","left")
                    ->join("tipe_barang","barang.id_tipe = tipe_barang.id_tipe","left")
                    ->join("merek","barang.id_merek = merek.id_merek","left")
                    ->join("satuan_terkecil","stok.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","left")
                    ->group_by("stok.id_barang");

        if($type_pencarian > 0)
        {
            switch ($type_pencarian) 
            {
                case 1:
                    $data = $data->like("nm_barang", $pencarian);
                    break;
                case 2:
                    $data = $data->like("nm_jenis", $pencarian);
                    break;
                case 3:
                    $data = $data->like("nm_tipe", $pencarian);
                    break;
                case 3:
                    $data = $data->like("nm_merek", $pencarian);
                    break;
                case 4:
                    $data = $data->having("stok >=", $min_stok)
                                ->having("stok <=", $max_stok);
                    break;
                default:
                    $data = $data->like("nm_barang", $pencarian);
                    break;
            }
        }

        $data = $data
                    ->order_by("barang.nm_barang", "ASC")
                    ->find_all_by(array("barang.deleted" => 0, "id_gudang" => $id_gudang));

        return $data;
    }
    /**
    * Update(Kurangi/Tambah) Stok Barang [Berdasarkan Jumlah Pengurang/Penambah]
    * ---Parameter----------------------
    * @kode_barang = Kode barang yang akan diupdate
    * @jml         = Jumlah barang yang yang diupdate-kan
    * @status      = 0 Berkurang, 1 Bertambah
    * @id_gudang   = Id Gudang
    * ---Return-------------------------
    * array jika sukses, false jika gagal
    */
    public function update_stok_by_kd_barang($kode_barang = "", $jml = 0, $status = 0, $id_gudang = 0)
    {
        if($kode_barang == "" || !($jml > 0))
        {
            return FALSE;
        }

        $where = array('id_barang' => $kode_barang, 'id_gudang' => $id_gudang, 'deleted' => 0);

        if($status == 0)
        {
            $adwhere = "stok_update > 0";
            $order   = array("created_on" => "ASC");
        }
        elseif($status == 1)
        {
            $adwhere = "stok_datang <> stok_update AND st_bonus = 0";
            $order   = array("created_on" => "DESC");
        }
        else{
            return FALSE;
        }

        //Get data stok
        $data = $this->select("id_stok, no_faktur, id_barang, stok_datang, stok_update, id_satuan_terkecil, hpp")
                    ->where($where)
                    ->where($adwhere)
                    ->order_by($order)
                    ->find_all();
        if($data)
        {
            $update_data = array();
            $return_data = array();
            if($status == 0)
            {
                foreach ($data as $key => $dt) {
                    $stok_update   = $dt->stok_update;
                   
                    if($dt->stok_update >= $jml)
                    {
                        $stok_update  -= $jml;
                        $update_data[] = array('id_stok' => $dt->id_stok, 'stok_update' => $stok_update);
                        $return_data[] = array('id_stok' => $dt->id_stok, 'hpp' => $dt->hpp, 'jml' => $jml);

                        $jml = 0;
                        break;
                    }
                    else if($dt->stok_update < $jml)
                    {
                        $stok_update   = 0;
                        $update_data[] = array('id_stok' => $dt->id_stok, 'stok_update' => $stok_update);
                        $return_data[] = array('id_stok' => $dt->id_stok, 'hpp' => $dt->hpp, 'jml' => $dt->stok_update);

                        $jml -= $dt->stok_update;
                    }
                }
            }
            elseif ($status == 1) 
            {
                foreach ($data as $key => $dt) {
                    $stok_update   = $dt->stok_update;
                    $selisih       = $dt->stok_datang - $dt->stok_update;
                   
                    if($selisih >= $jml)
                    {
                        $stok_update  += $jml;
                        $update_data[] = array('id_stok' => $dt->id_stok, 'stok_update' => $stok_update);
                        $return_data[] = array('id_stok' => $dt->id_stok, 'hpp' => $dt->hpp, 'jml' => $jml);

                        $jml = 0;
                        break;
                    }
                    else if($selisih < $jml)
                    {
                        $stok_update   = $dt->stok_update + $selisih;
                        $update_data[] = array('id_stok' => $dt->id_stok, 'stok_update' => $stok_update);
                        $return_data[] = array('id_stok' => $dt->id_stok, 'hpp' => $dt->hpp, 'jml' => $selisih);

                        $jml -= $selisih;
                    }
                }
            }

            if($update_data)
            {
                $result = $this->update_batch($update_data, "id_stok");
                if($result)
                {
                    return $return_data;
                }
            }

            return $return_data;
        }
        
        return FALSE;
    }
    /**
    * Update(Kurangi/Tambah) Stok [Berdasarkan Kode Barang dan Kode Faktur]
    * ---Parameter---------------------------------
    * @kode_barang = kode barang yang akan diupdate
    * @kode_faktur = kode faktur barang yang akan diupdate
    * @id_gudang   = Id Gudang
    * @jml         = Jumlah barang yang yang diupdate-kan
    * @status      = 0 Berkurang, 1 Bertambah
    * ---Return-------------------------
    * array jika sukses, false jika gagal
    */
    public function update_stok_by_kd_faktur($kode_barang = "", $kode_faktur = "", $id_gudang = 0, $jml = 0, $status = 0)
    {
        if($kode_barang == "" || $kode_faktur == "" || is_numeric($jml) == FALSE || !($jml > 0))
        {
            return FALSE;
        }

        $where = array('id_barang' => $kode_barang, 'no_faktur' => $kode_faktur, 'id_gudang' => $id_gudang, 'deleted' => 0);

        if($status == 0)
        {
            $adwhere = "stok_update > 0";
        }
        elseif($status == 1)
        {
            $adwhere = "stok_datang <> stok_update AND st_bonus = 0";
        }
        else
        {
            return FALSE;
        }

        //Get data stok
        $data = $this->select("id_stok, no_faktur, id_barang, stok_update, stok_datang, id_satuan_terkecil, hpp")
                    ->where($where)
                    ->where($adwhere)
                    ->order_by("created_on", "ASC")
                    ->find_all();
        if($data)
        {
            $update_data = array();
            $return_data = array();
            if($status == 0)
            {
                foreach ($data as $key => $dt) {
                    $stok_update   = $dt->stok_update;
                   
                    if($dt->stok_update >= $jml)
                    {
                        $stok_update  -= $jml;
                        $update_data[] = array('id_stok' => $dt->id_stok, 'stok_update' => $stok_update);
                        $return_data[] = array('id_stok' => $dt->id_stok, 'hpp' => $dt->hpp, 'jml' => $jml);

                        $jml = 0;
                        break;
                    }
                    else if($dt->stok_update < $jml)
                    {
                        $stok_update   = 0;
                        $update_data[] = array('id_stok' => $dt->id_stok, 'stok_update' => $stok_update);
                        $return_data[] = array('id_stok' => $dt->id_stok, 'hpp' => $dt->hpp, 'jml' => $dt->stok_update);

                        $jml -= $dt->stok_update;
                    }
                }
            }
            elseif ($status == 1) 
            {
                foreach ($data as $key => $dt) {
                    $stok_update   = $dt->stok_update;
                    $selisih       = $dt->stok_datang - $dt->stok_update;
                   
                    if($selisih >= $jml)
                    {
                        $stok_update  += $jml;
                        $update_data[] = array('id_stok' => $dt->id_stok, 'stok_update' => $stok_update);
                        $return_data[] = array('id_stok' => $dt->id_stok, 'hpp' => $dt->hpp, 'jml' => $jml);

                        $jml = 0;
                        break;
                    }
                    else if($selisih < $jml)
                    {
                        $stok_update   = $dt->stok_update + $selisih;
                        $update_data[] = array('id_stok' => $dt->id_stok, 'stok_update' => $stok_update);
                        $return_data[] = array('id_stok' => $dt->id_stok, 'hpp' => $dt->hpp, 'jml' => $selisih);

                        $jml -= $selisih;
                    }
                }
            }

            if($update_data)
            {
                $result = $this->update_batch($update_data, "id_stok");
                if($result)
                {
                    return $return_data;
                }
            }

            return $return_data;
        }
        
        return FALSE;
    }
    /**
    * Update(Kurangi/Tambah) Stok [Berdasarkan Kode Barang dan Kode Stok]
    * ---Parameter----------------------
    * @kode_barang = Kode barang yang akan diupdate
    * @kode_stok   = Kode stok barang yang akan diupdate
    * @jml         = Jumlah barang yang yang diupdate-kan
    * @status      = 0 Berkurang, 1 Bertambah
    * ---Return-------------------------
    * array jika sukses, false jika gagal
    */
    public function update_stok_by_kd_stok($kode_barang = "", $kode_stok = "", $jml = 0, $status = 0)
    {
        if($kode_barang == "" || $kode_stok == "" || !($jml > 0))
        {
            return FALSE;
        }

        if($status == 0)
        {
            $adwhere = "stok_update > 0";
        }
        elseif($status == 1)
        {
            $adwhere = "stok_datang <> stok_update";
        }
        else
        {
            return FALSE;
        }

        //Cek stok yang bisa ditambahkan atau dikurangi
        $data = $this->select("id_stok, id_barang, stok_datang, stok_update, hpp, (stok_datang - stok_update) as selisih")
                    ->where($adwhere)
                    ->find_by(array('id_barang' => $kode_barang, 'id_stok' => $kode_stok, 'deleted' => 0));

        if($data)
        {
            $update_data = array();
            $return_data = array();
            if($status == 0)
            {
                if($jml > $data->stok_update)
                {
                    return FALSE;
                }

                $stok_update   = $data->stok_update - $jml;
                $update_data[] = array('id_stok' => $data->id_stok, 'stok_update' => $stok_update);
                $return_data[] = array('id_stok' => $data->id_stok, 'hpp' => $data->hpp, 'jml' => $jml);

            }
            elseif($status == 1)
            {
                if($jml > $data->selisih)
                {
                    return FALSE;
                }

                $stok_update   = $data->stok_update + $jml;
                $update_data[] = array('id_stok' => $data->id_stok, 'stok_update' => $stok_update);
                $return_data[] = array('id_stok' => $data->id_stok, 'hpp' => $data->hpp, 'jml' => $jml);
            }

            if($update_data)
            {
                $result = $this->update_batch($update_data, "id_stok");
                if($result)
                {
                    return $return_data;
                }
            }
        }
        
        return FALSE;
    }

    /**
     * Get HPP barang dari stok barang
     * @param  string $id_barang [kode barang yang akan dicari]
     * @param  boolean $st_stok   [TRUE = mencari hpp yang dari stok barang yang masih ada, 
     *                             FALSE = mencari hpp pembelian terakhir tanpa melihat stok masih ada apa tidak]
     * @return [type]             [description]
     */
    public function get_hpp($id_barang = '', $st_stok = FALSE)
    {
        if(!$id_barang)
        {
            return 0;
        }

        if($st_stok)
        {
            $dt_stok = $this->order_by("created_on","DESC")->find_by(array('id_barang' => $id_barang, 'stok_update >' => 0, 'st_bonus' => 0));
        }
        else
        {
            $dt_stok = $this->order_by("created_on","DESC")->find_by(array('id_barang' => $id_barang, 'st_bonus' => 0));
        }

        if($dt_stok)
        {
            $hpp = $dt_stok->hpp;
        }
        else
        {
            $hpp = 0;
        }

        return $hpp;
    }

    /** Update HPP barang
     * @id_barang  integer
     * @no_faktur  string
     * @new_hpp  integer
     * @st_bonus  integer
     * @return bolean
     */
    public function update_hpp($id_barang = 0, $no_faktur = "", $new_hpp = 0, $st_bonus = 0)
    {
        if(!$id_barang || !$new_hpp)
        {
            return FALSE;
        }
        $where = array(
                'id_barang' => $id_barang,
                'no_faktur' => $no_faktur,
                'st_bonus'  => $st_bonus
                );
        
        if(!$no_faktur)
        {
            $where = "id_barang = '".$id_barang."' 
                      AND st_bonus = '".$st_bonus."' 
                      ORDER BY created_on DESC";
        }

        $data = array('hpp' => $new_hpp);
        $return = $this->update_where($where, null, $data);

        return $return;
    }
    /**
    * Update(Kurangi/Tambah) Luas Barang [Berdasarkan Luas Pengurang/Penambah]
    * ---Parameter----------------------
    * @kode_barang = Kode barang yang akan diupdate
    * @luas        = Luas barang yang yang diupdate-kan (milimeter persegi)
    * @status      = 0 Berkurang, 1 Bertambah
    * @id_gudang   = Id Gudang
    * ---Return-------------------------
    * array jika sukses, false jika gagal
    */
    public function update_luas_by_kd_barang($kode_barang = "", $luas = 0, $status = 0, $id_gudang = 0)
    {
        if($kode_barang == "" || !($luas > 0))
        {
            return FALSE;
        }

        $where = array('id_barang' => $kode_barang, 'id_gudang' => $id_gudang, 'deleted' => 0);

        if($status == 0)
        {
            $adwhere = "luas_update > 0";
            $order   = array("created_on" => "ASC");
        }elseif($status == 1){
            $adwhere = "luas_datang <> luas_update AND st_bonus = 0";
            $order   = array("created_on" => "DESC");
        }else{
            return FALSE;
        }

        //Get data stok
        $data = $this->select("id_stok, no_faktur, id_barang, stok_datang, stok_update, luas_datang, luas_update, id_satuan_terkecil, hpp")
                    ->where($where)
                    ->where($adwhere)
                    ->order_by($order)
                    ->find_all();
        if($data)
        {
            $update_data = array();
            $return_data = array();
            if($status == 0)
            {
                foreach ($data as $key => $dt) {
                    $luas_update   = $dt->luas_update;
                   
                    if($dt->luas_update >= $luas)
                    {
                        $luas_update  -= $luas;
                        $update_data[] = array('id_stok' => $dt->id_stok, 'luas_update' => $luas_update, 'stok_update' => 0);
                        $return_data[] = array('id_stok' => $dt->id_stok, 'hpp' => $dt->hpp/$dt->luas_datang, 'luas' => $luas);

                        $luas = 0;
                        break;
                    }else if($dt->luas_update < $luas){
                        $luas_update   = 0;
                        $update_data[] = array('id_stok' => $dt->id_stok, 'luas_update' => $luas_update, 'stok_update' => 0);
                        $return_data[] = array('id_stok' => $dt->id_stok, 'hpp' => $dt->hpp/$dt->luas_datang, 'luas' => $dt->luas_update);

                        $luas -= $dt->luas_update;
                    }
                }
            }
            elseif ($status == 1) 
            {
                foreach ($data as $key => $dt) {
                    $luas_update   = $dt->luas_update;
                    $selisih       = $dt->luas_datang - $dt->luas_update;
                   
                    if($selisih >= $luas)
                    {
                        $luas_update  += $luas;
                        $update_data[] = array('id_stok' => $dt->id_stok, 'luas_update' => $luas_update, 'stok_update' => ($selisih == $luas ? 1 : 0));
                        $return_data[] = array('id_stok' => $dt->id_stok, 'hpp' => $dt->hpp/$dt->luas_datang, 'luas' => $luas);

                        $luas = 0;
                        break;
                    }else if($selisih < $luas){
                        $luas_update   = $dt->luas_update + $selisih;
                        $update_data[] = array('id_stok' => $dt->id_stok, 'luas_update' => $luas_update, 'stok_update' => 1);
                        $return_data[] = array('id_stok' => $dt->id_stok, 'hpp' => $dt->hpp/$dt->luas_datang, 'luas' => $selisih);

                        $luas -= $selisih;
                    }
                }
            }

            if($update_data)
            {
                $result = $this->update_batch($update_data, "id_stok");
                if($result)
                {
                    return $return_data;
                }
            }

            return $return_data;
        }
        
        return FALSE;
    }
    /**
    * Update(Kurangi/Tambah) Luas Barang [Berdasarkan Kode Barang dan Kode Stok]
    * ---Parameter----------------------
    * @kode_barang = Kode barang yang akan diupdate
    * @kode_stok   = Kode stok barang yang akan diupdate
    * @luas        = Luas barang yang yang diupdate-kan (milimeter persegi)
    * @status      = 0 Berkurang, 1 Bertambah
    * ---Return-------------------------
    * array jika sukses, false jika gagal
    */
    public function update_luas_by_kd_stok($kode_barang = "", $kode_stok = "", $luas = 0, $status = 0)
    {
        if($kode_barang == "" || $kode_stok == "" || !($luas > 0))
        {
            return FALSE;
        }

        if($status == 0)
        {
            $adwhere = "luas_update > 0";
        }
        elseif($status == 1)
        {
            $adwhere = "luas_datang <> luas_update";
        }
        else
        {
            return FALSE;
        }

        //Cek stok yang bisa ditambahkan atau dikurangi
        $data = $this->select("id_stok, id_barang, luas_datang, luas_update, hpp, (luas_datang - luas_update) as selisih")
                    ->where($adwhere)
                    ->find_by(array('id_barang' => $kode_barang, 'id_stok' => $kode_stok, 'deleted' => 0));

        if($data)
        {
            $update_data = array();
            $return_data = array();
            if($status == 0)
            {
                if($luas > $data->luas_update)
                {
                    return FALSE;
                }

                $luas_update   = $data->luas_update - $luas;
                $update_data[] = array('id_stok' => $data->id_stok, 'luas_update' => $luas_update, 'stok_update' => 0);
                $return_data[] = array('id_stok' => $data->id_stok, 'hpp' => $data->hpp/$data->luas_datang, 'luas' => $luas);

            }
            elseif($status == 1)
            {
                if($luas > $data->selisih)
                {
                    return FALSE;
                }

                $luas_update   = $data->luas_update + $luas;
                $update_data[] = array('id_stok' => $data->id_stok, 'luas_update' => $luas_update, 'stok_update' => ($data->selisih == $luas ? 1 : 0));
                $return_data[] = array('id_stok' => $data->id_stok, 'hpp' => $data->hpp/$data->luas_datang, 'luas' => $luas);
            }

            if($update_data)
            {
                $result = $this->update_batch($update_data, "id_stok");
                if($result)
                {
                    return $return_data;
                }
            }
        }
        
        return FALSE;
    }

    /**
     * Get HPP luas barang dari stok barang
     * @param  string $id_barang [kode barang yang akan dicari]
     * @param  boolean $st_stok   [TRUE = mencari hpp yang dari stok barang yang masih ada, 
     *                             FALSE = mencari hpp pembelian terakhir tanpa melihat stok masih ada apa tidak]
     * @return float             [hpp barang per milimeter persegi]
     */
    public function get_hpp_luas($id_barang = '', $st_stok = FALSE)
    {
        if(!$id_barang)
        {
            return 0;
        }

        if($st_stok)
        {
            $dt_stok = $this->order_by("created_on","DESC")->find_by(array('id_barang' => $id_barang, 'luas_datang >' => 0, 'luas_update >' => 0, 'st_bonus' => 0));
        }
        else
        {
            $dt_stok = $this->order_by("created_on","DESC")->find_by(array('id_barang' => $id_barang, 'luas_datang >' => 0, 'st_bonus' => 0));
        }

        if($dt_stok)
        {
            $hpp = $dt_stok->hpp/$dt_stok->luas_datang;
        }
        else
        {
            $hpp = 0;
        }

        return $hpp;
    }
}