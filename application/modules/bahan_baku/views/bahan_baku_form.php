<div class="box box-primary">
    <!--form start -->
<?= form_open($this->uri->uri_string(),array('id'=>'frm_bahan_baku','name' => 'frm_bahan_baku','role'=>'form','class'=>'form-horizontal'))?>

    <div class="box-body">
            <div class="form-group <?= form_error('barcode') ? 'has-error' : '';?>">
                <label for="barcode" class="col-sm-2 control-label"><?= lang('barcode') ?></label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="barcode" name="barcode" value="<?php echo set_value('barcode', isset($dt->barcode) ? $dt->barcode : ''); ?>" autofocus></input>
                </div>
            </div>            
            <div class="form-group <?= form_error('kategori') ? 'has-error' : ''; ?>">
                <label for="kategori" class="control-label col-sm-2"><?= lang('idkategori'); ?></label>
                <div class="col-sm-4">
                    <?php
                            $disable = isset($st_edit) || isset($st_view) ? 'disabled' : '';
                    ?>

                    <select id="kategori" name="kategori" class="form-control" <?= $disable ?> >
                        <option></option>
                        <?php foreach ($data_kategori as $key => $value) : ?>
                        <option 
                            value ="<?= $value->idkategori ?>" 
                            data-stukuran = "<?= $value->st_ukuran ?>"
                            <?= set_select('kategori', $value->idkategori, isset($dt->idkategori) && $dt->idkategori == $value->idkategori ) ?> 
                        >
                            <?= strtoupper($value->nmkategori) ?>   
                        </option> 
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group <?= form_error('jenis') ? 'has-error' : ''; ?>">
                <label for="jenis" class="control-label col-sm-2"><?= lang('idjenis_bb'); ?></label>
                <div class="col-sm-4">
                    <select id="jenis" name="jenis" class="form-control">
                        <option></option>
                        <?php foreach ($data_jenis as $key => $value) : ?>
                            <option 
                                value="<?= $value->idjenis_bb; ?>" 
                                <?= set_select('jenis', $value->idjenis_bb, isset($dt->idjenis_bb) && $dt->idjenis_bb == $value->idjenis_bb ) ?>
                            >
                                <?= strtoupper($value->nmjenis_bb) ?>
                            </option> 
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group <?= form_error('merk') ? 'has-error' : ''; ?>">
                <label for="merk" class="control-label col-sm-2"><?= lang('idmerk_bb'); ?></label>
                <div class="col-sm-4">
                    <select id="merk" name="merk" class="form-control">
                        <option></option>
                        <?php foreach ($data_merk as $key => $dat_mer) :?>
                            <option 
                                value="<?= $dat_mer->idmerk_bb; ?>" 
                                <?= set_select('merk', $dat_mer->idmerk_bb, isset($dt->idmerk_bb) && $dt->idmerk_bb == $dat_mer->idmerk_bb) ?>
                            >
                                <?= strtoupper($dat_mer->nm_merk_bb) ?>
                            </option> 
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class="form-group <?= form_error('nm_barang') ? 'has-error' : '';?>">
                <label for="nm_barang" class="col-sm-2 control-label"><?= lang('bahan_baku_nama') ?></label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="nm_barang" name="nm_barang" value="<?php echo set_value('nm_barang', isset($dt->nm_barang) ? $dt->nm_barang : '') ?>"></input>
                </div>
            </div>
            <div class="form-group <?= form_error('satuan_barang') ? 'has-error' : ''; ?>">
                <label for="satuan_barang" class="control-label col-sm-2"><?= lang('id_satuan_terkecil'); ?></label>
                <div class="col-sm-4">
                    <?php
                            //$disable = isset($st_edit) || isset($st_view) ? 'disabled' : '';

                            $disable = "";

                            if(isset($st_view)){

                                $disable = 'disabled';

                            }else{

                                if(isset($st_edit) && isset($ketemu_stok) && $ketemu_stok > 0 ){

                                    $disable = 'disabled';

                                }

                            }


                    ?>
                    <select id="satuan_barang" name="satuan_barang" class="form-control" <?= $disable ?> > 
                        <option></option>
                        <?php foreach ($data_satuan_terkecil as $key => $dat_sat) : ?>
                            <option 
                                value="<?= $dat_sat->id_satuan_terkecil; ?>" 
                                <?= set_select('satuan_barang', $dat_sat->id_satuan_terkecil, isset($dt->id_satuan_terkecil) && $dt->id_satuan_terkecil == $dat_sat->id_satuan_terkecil ) ?>
                            >
                                <?= $dat_sat->alias ?>
                            </option> 
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class="form-group <?= form_error('ket_barang') ? 'has-error' : ''; ?>">
                <label for="ket_barang" class="col-sm-2 control-label"><?= lang('bahan_baku_ket') ?></label>
                <div class="col-sm-4">
                    <textarea class="form-control" id="ket_barang" name="ket_barang"><?php echo set_value('ket_barang', isset($dt->ket_barang) ? $dt->ket_barang : ''); ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="st_pakai" class="col-sm-2 control-label"><?= lang("st_potong_meteran") ?></label>
                <div class="col-sm-4">

                     <?php
                            //$disable = isset($st_edit) || isset($st_view) ? 'disabled' : '';

                            $disable = "";

                            if(isset($st_view)){

                                $disable = 'disabled';

                            }else{

                                if(isset($st_edit) && isset($ketemu_stok) && $ketemu_stok > 0 ){

                                    $disable = 'disabled';

                                }

                            }


                    ?>
                    <select name="st_pakai" id="st_pakai" class="form-control" <?= $disable ?> >
                        <option value="0" <?= set_select("st_pakai","0", isset($dt->st_potong_meteran) && $dt->st_potong_meteran == 0) ?> ><?= lang("isi_penggunaan_by_jumlah") ?></option>
                        <option value="1" <?= set_select("st_pakai","1", isset($dt->st_potong_meteran) && $dt->st_potong_meteran == 1) ?> ><?= lang("isi_penggunaan_by_potongan") ?></option>
                    </select>
                </div>
            </div>


            <div id="inputan_spek">
                <div class="label label-warning"><?= lang("spek") ?></div>
                <hr style="margin-top: 5px !important" >
                <div class="form-group <?= form_error('spek_p') || form_error('spek_l') ? 'has-error' : ''; ?>">
                    <label for="spek_p" class="col-sm-2 control-label"><?= lang('ukuran') ?></label>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <span class="input-group-addon"><strong>L</strong></span>
                            <input type="text" name="spek_l" id="spek_l" class="form-control" value="<?= set_value("spek_l", isset($dt->lebar_spek) ? $dt->lebar_spek : '') ?>">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <span class="input-group-addon"><strong>P</strong></span>
                            <input type="text" name="spek_p" id="spek_p" class="form-control" value="<?= set_value("spek_p", isset($dt->panjang_spek) ? $dt->panjang_spek : '' ) ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group <?= form_error('satuan_spek') ? 'has-error' : ''; ?>">
                    <label for="satuan_spek" class="control-label col-sm-2"><?= lang('id_konversi'); ?></label>
                    <div class="col-sm-4">
                        <select id="satuan_spek" name="satuan_spek" class="form-control">
                            <option></option>
                            <?php 

                                if(isset($data_satuan_spek) && is_array($data_satuan_spek) && count($data_satuan_spek)):
                                    foreach ($data_satuan_spek as $key => $isi): 
                            ?>
                                <option value="<?= $isi->id_konversi; ?>" <?= set_select('satuan_spek', $isi->id_konversi, isset($dt->id_konversi) && $dt->id_konversi == $isi->id_konversi )?>
                                >
                                    <?= $isi->tampil2 ?>
                                </option> 
                            <?php 
                                    endforeach;
                                endif;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group <?= form_error('ket_spek') ? 'has-error' : ''; ?>">
                    <label for="ket_spek" class="col-sm-2 control-label"><?= lang('ket_spek') ?></label>
                    <div class="col-sm-4">
                        <textarea class="form-control" id="ket_spek" name="ket_spek"><?php echo set_value('ket_spek', isset($dt->ket_spek) ? $dt->ket_spek : ''); ?></textarea>
                    </div>
                </div>
                
        </div>

        <?php
                if((isset($st_edit) || isset($st_view)) && $dt->st_hpp_manual == 1) :
        ?>
        <div class="form-group" >
            <label for="harga_manual" class= "col-sm-2 control-label"><?= lang("hpp_manual") ?></label>
            <div class="col-sm-4">
                <input type="hidden" name="st_hpp_manual" value="1">
                <input type="text" name="harga_manual" id="harga_manual" class="form-control" value="<?= set_value('harga_manual', isset($dt->hpp_manual) ? $dt->hpp_manual : '') ?>">
            </div>
        </div>
        <?php
                endif;
        ?>

            
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <?php
                        $disable = isset($st_view) ? 'disabled' : '';
                ?>
                <button type="submit" name="save" class="btn btn-primary" <?= $disable ?> ><?= lang('bahan_baku_btn_save') ?></button>
                <?php
                echo lang('bf_or') . ' ' . anchor('bahan_baku', lang('bahan_baku_btn_cancel'));
                ?>
            </div>
        </div>
    </div>
<?= form_close() ?>
</div>