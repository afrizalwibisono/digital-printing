<?= form_open($this->uri->uri_string(),array('id'=>'frm_index','name'=>'frm_index','class'=>'form-horizontal'))?>
<div class="box box-primary">
	<div class="box-body">
		<div class="form-group">
			<label class="control-label col-sm-2" for="nm_jasa"><?= lang('index-nm-jasa') ?></label>
			<div class="col-sm-4">
				<input type="text" name="nm_jasa" id="nm_jasa" value="<?= set_value('nm_jasa',isset($data) ? $data->nm_barang : '') ?>"  class="form-control">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2" for="st_jasa"><?= lang('index-st-jasa') ?></label>
			<div class="col-sm-4">
				<select class="form-control" id="st_jasa" name="st_jasa">
					<option value="0" <?= set_select('st_jasa','0',isset($data) && $data->st_hitung_jasa == 0) ?> ><?= lang('frm-isi-stjasa-unit') ?></option>
					<option value="1" <?= set_select('st_jasa','1',isset($data) && $data->st_hitung_jasa == 1) ?> ><?= lang('frm-isi-stjasa-waktu') ?></option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-4">
				<button type="submit" name="save" class="btn btn-primary" ><?= lang('btn-save') ?></button>
                <?php
                echo lang('bf_or') . ' ' . anchor('bahan_baku/jasa', lang('btn-cancel'));
                ?>
			</div>
		</div>
	</div>
</div>
<?= form_close() ?>