<?php
	$ENABLE_ADD		= has_permission('Jasa.Add');
	$ENABLE_MANAGE	= has_permission('Jasa.Manage');
	$ENABLE_DELETE	= has_permission('Jasa.Delete');
?>

<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_index','name'=>'frm_index'))?>
	<div class="box-header">
		<div class="form-group">
			<?php if ($ENABLE_ADD): ?>
			<a href="<?= site_url('bahan_baku/jasa/create') ?>" class="btn btn-success" title="<?= lang('btn-new') ?>"><?= lang('btn-new') ?></a>
			<?php endif;?>
            <div class="pull-right form-inline">
                <div class="input-group">
                    <input type="text" name="search" value="<?php echo isset($search) ? $search:''; ?>" class="form-control pull-right" placeholder="Search" autofocus>
                    <div class="input-group-btn">
                        <button class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                </div>  
            </div>
		</div>
	</div>

	<?php if(isset($data) && is_array($data) && count($data)): ?>
	<div class="box-body table-responsive">
		
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th class="column-check" style="width: 30px;" > <input class="check-all" type="checkbox" /> </th>
					<th width="50">#</th>
					<th class="text-center" ><?= lang('index-nm-jasa') ?></th>
					<th class="text-center" ><?= lang('index-st-jasa') ?></th>
					<th width="20"> &nbsp </th> <!-- area tombol edit -->
				</tr>
			</thead>
			<tbody>
				<?php foreach ($data as $key => $dt) : ?>
					<tr>
						<td class="text-center" >
							<input type="checkbox" name="checked[]" value="<?= $dt->idbarang_bb ?>" />
						</td>
						<td ><?= $numb ?></td>
						<td class="text-center" ><?= $dt->nm_barang ?></td>
						<td class="text-center" >
							<?php 
									$tampil = $dt->st_hitung_jasa == 0 ? 'Unit / Kali' : 'Waktu' ; 
									echo $tampil;
							?>
						</td>
						<td class="text-center" >
							<?php if($ENABLE_MANAGE) : ?>
                                <a class="text-black" href="<?= site_url('bahan_baku/jasa/view/' . $dt->idbarang_bb); ?>" data-toggle="tooltip" data-placement="left" title="Edit Data"><i class="fa fa-pencil"></i></a>
                            <?php endif; ?>
						</td>
					</tr>
				<?php 

						$numb++;

					endforeach; 
				?>	
			</tbody>
		</table>

	</div>       
	<div class="box-footer clearfix">
		<?php if($ENABLE_DELETE) : ?>
		<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('btn-delete') ?>" onclick="return confirm('<?= lang('pesan_warning_hapus'); ?>')">
		<?php endif;
		echo $this->pagination->create_links(); 
		?>
	</div>
	<?php else : ?>
	<?php endif; ?>

	<?= form_close(); ?>
</div>