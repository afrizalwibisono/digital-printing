$(function(){

	kondisikan_spek();

	$("#kategori").select2({
        placeholder :"-- Pilih Kategori --",
        allowClear  : true
  	});

  	$("#merk").select2({
        placeholder :"-- Pilih Merk --",
        allowClear  : true
  	});

	$("#jenis").select2({
        placeholder :"-- Pilih Jenis --",
        allowClear  : true
  	});  	
  
	$("#satuan_barang").select2({
        placeholder :"-- Pilih Satuan Terkecil Acuan Hitung --",
        allowClear  : true
  	});  	

	$("#satuan_spek").select2({
        placeholder :"-- Pilih Satuan Spesifikasi --",
        allowClear  : true
  	});  	

  $("#spek_l, #spek_p").number(true,2);

  $("#harga_manual").number(true);

  	$("#st_pakai").change(function(){

  		kondisikan_spek();

  	});

});

function kondisikan_spek(){

	var st_pakai 	= $("#st_pakai").val();

	if(st_pakai == 0 ){

		$("#inputan_spek").hide(400);			

	}else{

		$("#inputan_spek").show(400);				

	}

}