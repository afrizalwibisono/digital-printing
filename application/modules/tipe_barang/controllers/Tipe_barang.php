<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for tipe
 */

class Tipe_barang extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Tipe Barang.View";
    protected $addPermission    = "Tipe Barang.Add";
    protected $managePermission = "Tipe Barang.Manage";
    protected $deletePermission = "Tipe Barang.Delete";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('tipe_barang/tipe_barang');
        $this->load->model(array('tipe_barang/tipe_barang_model',
                            'jenis_barang/jenis_barang_model'
                            ));

        $this->template->title(lang('tipe_title_manage'));
		$this->template->page_icon('fa fa-table');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if (isset($_POST['delete']) && has_permission($this->deletePermission))
        {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                foreach ($checked as $pid)
                {
                    $nm_tipe    = $this->tipe_barang_model->find($pid)->nm_tipe;
                    $result     = $this->tipe_barang_model->delete($pid);

                    if($result)
                    {
                        $keterangan = "SUKSES, hapus data tipe barang ".$nm_tipe.", dengan ID : ".$pid;
                        $status     = 1;
                    }
                    else
                    {
                        $keterangan = "GAGAL, hapus data tipe barang ".$nm_tipe.", dengan ID : ".$pid;
                        $status     = 0;
                    }

                    $nm_hak_akses   = $this->deletePermission; 
                    $kode_universal = $pid;
                    $jumlah         = 1;
                    $sql            = $this->db->last_query();

                    simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                }

                if ($result)
                {
                    $this->template->set_message(count($checked) .' '. lang('tipe_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('tipe_del_failure') . $this->tipe_barang_model->error, 'error');
                }
            }
            else
            {
                $this->template->set_message(lang('tipe_del_error') . $this->tipe_barang_model->error, 'error');
            }
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
        }

        $filter = "";
        if($search!="")
        {
            $filter = "?search=".$search;
        }

        $search2 = $this->db->escape_str($search);
        
        $where = "`tipe_barang`.`deleted` = 0
                AND (`nm_tipe` LIKE '%$search2%' ESCAPE '!'
                OR `nm_jenis` LIKE '%$search2%' ESCAPE '!'
                OR `tipe_barang`.`ket` LIKE '%$search2%' ESCAPE '!')";
        
        $total = $this->tipe_barang_model
                    ->join('jenis_barang','tipe_barang.id_jenis_barang = jenis_barang.id_jenis_barang','left')
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->tipe_barang_model->select("tipe_barang.*, nm_jenis")
                    ->where($where)
                    ->join('jenis_barang','tipe_barang.id_jenis_barang = jenis_barang.id_jenis_barang','left')
                    ->order_by('nm_tipe','ASC')
                    ->limit($limit, $offset)->find_all();

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set("toolbar_title", lang('tipe_title_manage'));
        $this->template->set("page_title", lang('tipe_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

   	//Create New Supplier
   	public function create()
   	{

        $this->auth->restrict($this->addPermission);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_tipe())
            {
              $this->template->set_message(lang("tipe_create_success"), 'success');
              redirect('tipe_barang');
            }
        }

        $jenis = $this->jenis_barang_model->order_by('nm_jenis', 'ASC')
                                        ->find_all_by('deleted', 0);
        $assets = array(
                    'js/plugins/select2/css/select2.min.css',
                    'js/plugins/select2/css/select2-bootstrap.min.css',
                    'js/plugins/select2/js/select2.min.js',
                    'tipe_barang/assets/js/tipe_barang.js'
                    );

        add_assets($assets);

        $this->template->set("jenis", $jenis);
        $this->template->title(lang('tipe_title_new'));
   	    $this->template->render('tipe_form');
   	}

   	//Edit Supplier
   	public function edit($id)
   	{
  		$this->auth->restrict($this->managePermission);
                
        if (empty($id))
        {
            $this->template->set_message(lang("tipe_invalid_id"), 'error');
            redirect('tipe');
        }

        if (isset($_POST['save']))
        {
            if ($this->save_tipe('update', $id))
            {
                $this->template->set_message(lang("tipe_edit_success"), 'success');
            }

        }

        $assets = array(
                    'js/plugins/select2/css/select2.min.css',
                    'js/plugins/select2/css/select2-bootstrap.min.css',
                    'js/plugins/select2/js/select2.min.js',
                    'tipe_barang/assets/js/tipe_barang.js'
                    );

        add_assets($assets);

        $jenis = $this->jenis_barang_model->order_by('nm_jenis', 'ASC')
                                        ->find_all_by('deleted', 0);

        $data  = $this->tipe_barang_model->find_by(array('id_tipe' => $id, 'deleted' => 0));

        if(!$data)
        {
            $this->template->set_message(lang("tipe_invalid_id"), 'error');
            redirect('tipe_barang');
        }
        
        $this->template->set("jenis", $jenis);
        $this->template->set('data', $data);
        $this->template->title(lang("tipe_title_edit"));
        $this->template->render('tipe_form');
        
   	}

   	protected function save_tipe($type='insert', $id=0)
   	{

        $this->form_validation->set_rules('id_jenis_barang','lang:tipe_jenis','required');
        $this->form_validation->set_rules('nm_tipe','lang:tipe_name','required|trim|max_length[255]');
        $this->form_validation->set_rules('ket','lang:tipe_ket','trim|max_length[255]');

        if ($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        unset($_POST['save']);

        if ($type == 'insert')
        {
            //Cek apakah sudah ada
            $cek = $this->tipe_barang_model->find_by(array('id_jenis_barang' => $_POST['id_jenis_barang'], 'nm_tipe' => $_POST['nm_tipe']));
            if($cek)
            {
                $this->template->set_message(lang('tipe_already_exist'), 'error');
                return FALSE;
            }

            $id = $this->tipe_barang_model->insert($_POST);

            if (is_numeric($id))
            {
                //Save Log
                if($id)
                {
                    $keterangan = "SUKSES, tambah data tipe barang ".$this->input->post('nm_tipe').", dengan ID : ".$id;
                    $status     = 1;
                }
                else
                {
                    $keterangan = "GAGAL, tambah data tipe barang ".$this->input->post('nm_tipe').", dengan ID : ".$id;
                    $status     = 0;
                }

                $nm_hak_akses   = $this->addPermission; 
                $kode_universal = $id;
                $jumlah         = 1;
                $sql            = $this->db->last_query();
                //menyimpan ke Log histori
                simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

                $return = TRUE;
            }
            else
            {
                $return = FALSE;
            }
        }
        else if ($type == 'update')
        {
            //Cek apakah sudah ada
            $cek = $this->tipe_barang_model->find_by(array('id_jenis_barang' => $_POST['id_jenis_barang'], 'nm_tipe' => $_POST['nm_tipe'], 'id_tipe <>' => $id));
            if($cek)
            {
                $this->template->set_message(lang('tipe_already_exist'), 'error');
                return FALSE;
            }

            $return = $this->tipe_barang_model->update($id, $_POST);

            if($return)
            {
                $keterangan = "SUKSES, ubah data tipe barang ".$this->input->post('nm_tipe').", dengan ID : ".$id;
                $status     = 1;
            }
            else
            {
                $keterangan = "GAGAL, ubah data tipe barang ".$this->input->post('nm_tipe').", dengan ID : ".$id;
                $status     = 0;
            }

            //Save Log
            $nm_hak_akses   = $this->managePermission; 
            $kode_universal = $id;
            $jumlah         = 1;
            $sql            = $this->db->last_query();
            //menyimpan ke log historis
            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
        }

        return $return;
   	}

}
?>