<div class="panel panel-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_tipe','name'=>'frm_tipe','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="panel-body">
    		<div class="form-group <?= form_error('id_jenis_barang') ? ' has-error' : ''; ?>">
			    <label for="id_jenis_barang" class="col-sm-2 control-label"><?= lang('tipe_jenis') ?></label>
			    <div class="col-sm-3">
			    	<select class="form-control" id="id_jenis_barang" name="id_jenis_barang">
			    		<option></option>
			    		<?php
			    			if($jenis) : 
			    			foreach ($jenis as $key => $jn) : ?>
			    		<option value="<?= $jn->id_jenis_barang ?>" <?= set_select('id_jenis_barang', $jn->id_jenis_barang, isset($data->id_jenis_barang) && $data->id_jenis_barang == $jn->id_jenis_barang) ?>><?= ucwords($jn->nm_jenis) ?></option>
			    		<?php endforeach;endif ?>
			    	</select>
			    </div>
		  	</div>
			<div class="form-group <?= form_error('nm_tipe') ? ' has-error' : ''; ?>">
			    <label for="nm_tipe" class="col-sm-2 control-label"><?= lang('tipe_name') ?></label>
			    <div class="col-sm-3">
			    	<input type="text" class="form-control" id="nm_tipe" name="nm_tipe" maxlength="255" value="<?php echo set_value('nm_tipe', isset($data->nm_tipe) ? $data->nm_tipe : ''); ?>" required autofocus>
			    </div>
		  	</div>
		  	<div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
			    <label for="ket" class="col-sm-2 control-label"><?= lang('tipe_ket') ?></label>
			    <div class="col-sm-4">
			    	<textarea class="form-control" id="ket" name="ket"><?php echo set_value('ket', isset($data->ket) ? $data->ket : ''); ?></textarea>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      	<button type="submit" name="save" class="btn btn-primary"><?= lang('tipe_btn_save') ?></button>
			    	<?php
	                	echo lang('merk_btn_or') . ' ' . anchor('tipe_barang', lang('tipe_btn_cancel'));
	                ?>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.panel -->