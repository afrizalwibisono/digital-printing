<?php 
    $ENABLE_ADD     = has_permission('Merek.Add');
    $ENABLE_MANAGE  = has_permission('Merek.Manage');
    $ENABLE_DELETE  = has_permission('Merek.Delete');
?>
<div class="panel panel-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_tipe','name'=>'frm_tipe')) ?>
        <div class="panel-heading">
            <div class="form-group">
                <?php if ($ENABLE_ADD) : ?>
	  	        <a href="<?= site_url('tipe_barang/create') ?>" class="btn btn-success" title="<?= lang('tipe_btn_new') ?>"><?= lang('tipe_btn_new') ?></a>
                <?php endif; ?>
                <div class="pull-right">
                    <div class="input-group">
                        <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="Search" autofocus />
                        <div class="input-group-btn">
                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
	<div class="panel-body table-responsive no-padding">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
                        <th width="50">#</th>
                        <th><?= lang('tipe_name') ?></th>
                        <th><?= lang('tipe_jenis') ?></th>
                        <th><?= lang('tipe_ket') ?></th>
                        <?php if($ENABLE_MANAGE) : ?>
                        <th width="25"></th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($results as $record) : ?>
                    <tr>
                        <td class="column-check"><input type="checkbox" name="checked[]" value="<?= $record->id_tipe ?>" /></td>
                        <td><?= $numb; ?></td>
                        <td><?= $record->nm_tipe ?></td>
                        <td><?= $record->nm_jenis ?></td>
                        <td><?= $record->ket ?></td>
                        <?php if($ENABLE_MANAGE) : ?>
                        <td style="padding-right:20px"><a class="text-black" href="<?= site_url('tipe_barang/edit/' . $record->id_tipe); ?>" data-toggle="tooltip" data-placement="left" title="Edit Data"><i class="fa fa-pencil"></i></a></td>
                        <?php endif; ?>
                    </tr>
                    <?php $numb++; endforeach; ?>
                </tbody>
	  </table>
	</div><!-- /.box-body -->
	<div class="panel-footer clearfix">
		<?php if($ENABLE_DELETE) : ?>
		<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('tipe_btn_delete') ?>" onclick="return confirm('<?= lang('tipe_delete_confirm'); ?>')">
		<?php endif;
		echo $this->pagination->create_links(); 
		?>
	</div>
	<?php else: ?>
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('tipe_no_records_found') ?></p>
    </div>
    <?php
	endif;
	echo form_close(); 
	?>
</div><!-- /.panel -->