<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['tipe_title_manage']  = 'Tipe Barang';
$lang['tipe_title_new'] 	= 'Tipe Barang Baru';
$lang['tipe_title_edit'] 	= 'Edit Tipe Barang';

// form/table
$lang['tipe_name'] 			= 'Nama Tipe';
$lang['tipe_jenis'] 		= 'Jenis Barang';
$lang['tipe_ket'] 			= 'Keterangan';


// button
$lang['tipe_btn_new'] 		= 'Baru';
$lang['tipe_btn_delete'] 	= 'Hapus';
$lang['tipe_btn_save'] 		= 'Simpan';
$lang['tipe_btn_cancel'] 	= 'Batal';
$lang['merk_btn_or']		= 'atau';

// messages
$lang['tipe_del_error']			= 'Anda belum memilih tipe barang yang akan dihapus.';
$lang['tipe_del_failure']		= 'Tidak dapat menghapus tipe barang: ';
$lang['tipe_delete_confirm']	= 'Apakah anda yakin akan menghapus tipe barang terpilih ?';
$lang['tipe_deleted']			= 'Tipe barang berhasil dihapus';
$lang['tipe_no_records_found'] 	= 'Data tidak ditemukan.';

$lang['tipe_create_failure'] 	= 'Tipe Barang baru gagal disimpan: ';
$lang['tipe_create_success'] 	= 'Tipe Barang baru berhasil disimpan';

$lang['tipe_edit_success'] 		= 'Tipe Barang berhasil disimpan';
$lang['tipe_invalid_id'] 		= 'ID Tidak Valid';
$lang['tipe_already_exist'] 	= 'Tipe sudah ada, silahkan periksa kembali';