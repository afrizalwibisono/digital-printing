<div class="box box-primary">
    <!--form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'worksheet_form','name'=>'worksheet_form','role','class'=>'form-horizontal'))?>
    <div class="box-body">
        <div class="form-group <?= form_error('nm_worksheet') ? 'has-error' : '';?>">
            <label for="nm_worksheet" class="col-sm-2 control-label"><?= lang('worksheet_name') ?></label>
            <div class="col-sm-3">
                <input type="text" class="form-control" id="nm_worksheet" name="nm_worksheet" maxlength="50" value="<?php echo set_value('nm_worksheet', isset($data->nm_worksheet)?$data->nm_worksheet:''); ?>" required autofocus></input>
            </div>
        </div>
        <div class="form-group <?= form_error('lokasi') ? 'has-error' : '';?>">
            <label for="lokasi" class="col-sm-2 control-label"><?= lang('lokasi') ?></label>
            <div class="col-sm-3">
                <input type="text" class="form-control" id="lokasi" name="lokasi" maxlength="50" value="<?php echo set_value('lokasi', isset($data->lokasi)?$data->lokasi:''); ?>"></input>
            </div>
        </div>
        <div class="form-group <?= form_error('ket') ? 'has-error' : '';?>">
            <label for="ket" class="col-sm-2 control-label"><?= lang('ket') ?></label>
            <div class="col-sm-5">
                <textarea class="form-control" id="ket" name="ket"><?php echo set_value('ket', isset($data->ket) ? $data->ket : ''); ?></textarea>
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" name="save" class="btn btn-primary"><?= lang('worksheet_btn_save') ?></button>
                <?php
                 echo lang('bf_or') . ' ' . anchor('worksheet/worksheet', lang('worksheet_btn_cancel'));
                ?>
            </div>
        </div>
    </div>
    <?= form_close() ?>
</div>