<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2018, Cokeshome
 * 
 * This is controller for worksheet Satuan

 */

class Worksheet extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Worksheet
    protected $viewWorksheet   = "Worksheet.View";
    protected $addWorksheet    = "Worksheet.Add";
    protected $manageWorksheet = "Worksheet.Manage";
    protected $deleteWorksheet = "Worksheet.Delete";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewWorksheet);

        $this->lang->load('worksheet/worksheet');
        $this->load->model(array(
                                'worksheet_model',
                                
                            ));

        $this->template->title(lang('worksheet_title_manage'));
		$this->template->page_icon('fa fa-list-ol');
    }

    public function index()
    {
        $this->auth->restrict($this->viewWorksheet);

        if (isset($_POST['delete']) && has_permission($this->deleteWorksheet))
        {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                foreach ($checked as $pid)
                {
                    $nm_worksheet   = $this->worksheet_model->find($pid)->nm_worksheet;
                    $result         = $this->worksheet_model->delete_where(array('id_worksheet' => $pid, 'st_fix' => 0));

                    if($result)
                    {
                        $keterangan = "SUKSES, hapus data worksheet ".$nm_worksheet.", dengan ID : ".$pid;
                        $status     = 1;
                    }
                    else
                    {
                        $keterangan = "GAGAL, hapus data worksheet ".$nm_worksheet.", dengan ID : ".$pid;
                        $status     = 0;
                    }

                    $nm_hak_akses   = $this->deleteWorksheet; 
                    $kode_universal = $pid;
                    $jumlah         = 1;
                    $sql            = $this->db->last_query();

                    simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                }

                if ($result)
                {
                    $this->template->set_message(count($checked) .' '. lang('worksheet_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('worksheet_del_failure') . $this->worksheet_model->error, 'error');
                }
            }
            else
            {
                $this->template->set_message(lang('worksheet_del_error'), 'error');
            }
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
        }

        $filter = "";
        if($search!="")
        {
            $filter = "?search=".$search;
        }

        $search2 = $this->db->escape_str($search);
        
        $where = "`worksheet.deleted` = 0
                AND (`nm_worksheet` LIKE '%$search2%' ESCAPE '!'
                OR `lokasi` LIKE '%$search2%' ESCAPE '!'
                OR `ket` LIKE '%$search2%' ESCAPE '!')";
                
        
        $total = $this->worksheet_model
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->worksheet_model
                    ->where($where)
                    ->order_by('nm_worksheet','ASC')
                    ->limit($limit, $offset)->find_all();
        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->title(lang('worksheet_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

   	//Buat worksheet baru 
   	public function create()
   	{

        $this->auth->restrict($this->addWorksheet);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_worksheet())
            {
              $this->template->set_message(lang("worksheet_create_success"), 'success');
              redirect('worksheet');
            }
        }

        $data = $this->worksheet_model->find_all();

        $this->template->set('results', $data);
        $this->template->title(lang('worksheet_title_new'));
		$this->template->render('worksheet_form');
   	}

   	//Edit worksheet
   	public function edit()
   	{
  		$this->auth->restrict($this->manageWorksheet);
                
        $id = (int)$this->uri->segment(3);

        if (empty($id))
        {
            $this->template->set_message(lang("worksheet_invalid_id"), 'error');
            redirect('worksheet');
        }

        if (isset($_POST['save']))
        {
            if ($this->save_worksheet('update', $id))
            {
                $this->template->set_message(lang("worksheet_edit_success"), 'success');
            }

        }

        $data  = $this->worksheet_model->find_by(array('id_worksheet' => $id, 'deleted' => 0));

        if(!$data)
        {
            $this->template->set_message(lang("worksheet_invalid_id"), 'error');
            redirect('worksheet');
        }

        $data1 = $this->worksheet_model->find_all();
        
        $this->template->set('data', $data);
        $this->template->set('results', $data1);
        $this->template->title(lang("worksheet_title_edit"));
        $this->template->render('worksheet_form');
   	}
protected function save_worksheet($type='insert', $id=0)
    {
        if ($type == 'insert')
        {
            $cek = "|unique[worksheet.nm_worksheet]";
        }
        else
        {
            $_POST['id_worksheet'] = $id;
            $cek = "|unique[worksheet.nm_worksheet,worksheet.id_worksheet]";
        }
        // untuk cek lokasi dan keterangan tidak boleh kosong ---------------
        $this->form_validation->set_rules('nm_worksheet','lang:worksheet_name','required|trim'.$cek);
        $this->form_validation->set_rules('lokasi','lang:lokasi','required|trim');
        $this->form_validation->set_rules('ket','lang:ket','required|trim');
       //---------------------------------- 
        if ($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        unset($_POST['submit'], $_POST['save']);

        if ($type == 'insert')
        {
            $id = $this->worksheet_model->insert($_POST);

            if (is_numeric($id))
            {
                $return = TRUE;
            }
            else
            {
                $return = FALSE;
            }
        }
        else if ($type == 'update')
        {
            $return = $this->worksheet_model->update($id, $_POST);
        }

        return $return;
    }
}
?>