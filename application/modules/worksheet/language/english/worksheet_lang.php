<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ============================
$lang['worksheet_title_manage']		= 'Data Worksheet ';
$lang['worksheet_title_new']		= 'Data Worksheet Baru';
$lang['worksheet_title_edit']		= 'Edit Data Worksheet';

// from/table
$lang['worksheet_name']				= 'Nama Worksheet';
$lang['lokasi']						= 'Lokasi';
$lang['ket']						= 'Keterangan';



//button
$lang['worksheet_btn_new']			= 'Baru';
$lang['worksheet_btn_delete']		= 'Hapus';
$lang['worksheet_btn_save']			= 'Simpan';
$lang['worksheet_btn_cancel']		= 'Batal';

//messages
$lang['worksheet_del_error']		= 'Anda belum memilih Worksheet yang akan dihapus.';
$lang['worksheet_del_failure']		= 'Tidak dapat menghapus Worksheet';
$lang['worksheet_delete_confirm']	= 'Apakah anda yakin akan menghapus data Worksheet terpilih?';
$lang['worksheet_deleted']			= 'Worksheet berhasil dihapus';
$lang['worksheet_no_records_found'] = 'Data tidak ditemukan.';

$lang['worksheet_create_failure']	= 'Worksheet baru gagal disimpan';
$lang['worksheet_create_success']	= 'Worksheet baru berhasil disimpan';

$lang['worksheet_edit_success']		= 'Data Worksheet berhasil disimpan';
$lang['worksheet_invalid_id']		= 'ID tidak Valid';

