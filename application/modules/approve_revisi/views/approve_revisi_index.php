<div class="box box-primary">

	<?php echo form_open($this->uri->uri_string(),[
													'name'		=> 'form_app_po',
													'id'		=> 'form_app_po',
													'role'		=> 'form',
													'class'		=> 'form-inline'
													]) ?>
	<div class="box-header">

		<a href="<?= site_url('approve_revisi/history') ?>" target="_blank" class="btn btn-default" id="tbl_history">
			<i class="fa fa-book"></i> <?= lang('btn-history') ?>
		</a>	

		<div class="pull-right">			

            <div class="form-group">
                <input type="text" name="no_nota" id="no_nota" class="form-control" placeholder="No. Nota" value="<?= set_value('no_nota', isset($param['no_nota']) ? $param['no_nota'] : '')  ?>">
            </div>

            <div class="form-group">
                <input type="hidden" name="id_konsumen_pilih" id="id_konsumen_pilih" value="<?= set_value('konsumen', isset($param['konsumen']) ? $param['konsumen'] : '') ?>">
                <select name="konsumen" id="konsumen" class="form-control" style="min-width: 300px !important" >
                    <option value=""></option>
                    <?php 
                    	if(isset($dt_konsumen) && is_array($dt_konsumen) && count($dt_konsumen)):
                    		foreach ($dt_konsumen as $key => $isi) :	
                    ?>

                    	<option value="<?= $isi->id ?>" <?= set_select('konsumen',$isi->id,isset($param['konsumen']) && $param['konsumen'] == $isi->id ) ?>  ><?= $isi->nm ?></option>

                    <?php
                    		endforeach;
                    	endif;
                    ?>
                </select>
            </div>

            <button type="submit" name='simpan' class="btn btn-primary">
            	<span class="fa fa-search"></span> <?= lang("btn-filter") ?>
            </button>

		</div>

	</div>

	<?php 

			if(isset($data) && is_array($data) && count($data)):

	?>

	<div class="box-body table-responsive">	

		<table class="table table-hover table-striped">
			<thead>
				<tr class="success">
					<th class="text-center" width="30">#</th>
					<th class="text-center"><?= lang("jdl-table-index-tgl") ?></th>
					<th class="text-center"><?= lang("jdl-table-index-nonota") ?></th>
					<th class="text-center"><?= lang("jdl-table-index-konsumen") ?></th>
					<th class="text-center"><?= lang("jdl-table-index-total") ?></th>
					<th class="text-center" width="100"></th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($data as $key => $isi) :

						$class 	= '';

						/*if($isi->st_acc == 1){

							$class 	= "class = 'bg-red'";

						}*/

				?>
				<tr <?= $class ?> >
					<td class="text-right">
						<?= $key+1 ?>
						<input type="hidden" name="dft_id[]" id="dft_id" value="<?= $isi->id_order ?>">						
					</td>		
					<td class="text-center"><?= $isi->tgl ?></td>
					<td class="text-center"><?= $isi->no_faktur ?></td>
					<td class="text-center"><?= $isi->nm_konsumen ?></td>
					<td class="text-center"><?= number_format($isi->total_transaksi) ?></td>
					<td class="text-center">
						<a href="#" id="pil_acc" class="text-green" onclick="perubahan_status(this)" title="Approve Status Revisi Order" >
							<span class="fa fa-check-square-o"></span>
						</a>
						&nbsp | &nbsp
						<a href="<?= site_url('order_produk/view/' . $isi->id_order); ?>" target="_blank" class="text-black">
							<span class="fa fa-folder"></span>
						</a>
					</td>
				</tr>
				<?php
					endforeach;
				?>
			</tbody>
			
		</table>

	</div>
	<div class="box-footer clearfix">
		<?php echo $this->pagination->create_links();  ?>
	</div>
	<?php else: ?>

	<div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('konfirmasi-data-tidak-ada') ?></p>
    </div>

	<?php endif ?>

	<?php echo form_close(); ?>

</div>

<!-- area modal inputan keterangan -->
<div class="modal fade" id="modal">
	<div class="modal-dialog modal-md">
	    <div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          	<span aria-hidden="true">×</span></button>
	        	<h4 class="modal-title"><?= lang("jdl-modal") ?></h4>
	      	</div>
	      	<div class="modal-body">
	      		<?php $tgl_sekarang = date('d/m/Y') ?>
	      		<input type="hidden" name="mdl_idorder_proses" id="mdl_idorder_proses">

	        	<div class="form-horizontal">
	        		<div class="form-group">
	        			<label class="control-label col-sm-3" for = "mdl-notrans"><?= lang('modal-nonota') ?></label>
	        			<div class="col-sm-6">
	        				<span class="form-control input-sm" id="id_modal"></span>
	        			</div>
	        		</div>
	        		<div class="form-group">
	        			<label class="control-label col-sm-3" for = "mdl-stref"><?= lang('modal-strevisi') ?></label>
	        			<div class="col-sm-6">
	        				<span class="form-control input-sm" id="st_modal" data-st="" ></span>
	        			</div>
	        		</div>
	        	</div>	

	        	<p class="text-justify" id="ket_st_ijin" >1. Saat ini Anda memberikan ijin untuk Order dapat direvisi.</p>
	        	<p class="text-justify">2. Anda wajib mengisi keterangan untuk memberikan Approval pada perubahan Status Revisi List Order untuk tgl. <strong> <?= $tgl_sekarang ?> </strong></p>
	        	<textarea class="form-control" name="ket_approval" id="ket_approval"></textarea>

	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-outline pull-left" data-dismiss="modal"><?= lang("btn-batal") ?></button>
	        	<button type="button" class="btn btn-outline" id="simpan_modal"><?= lang("btn-save-app") ?></button>
	      	</div>
	    </div>
	    <!-- /.modal-content -->
	</div>
	  <!-- /.modal-dialog -->
</div>
