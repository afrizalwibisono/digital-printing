<div class="box box-primary">

	<?php echo form_open($this->uri->uri_string(),[
													'name'		=> 'form_history',
													'id'		=> 'form_history',
													'role'		=> 'form',
													'class'		=> 'form-inline'
													]) ?>
	<div class="box-header">

		<div class="pull-right">			

            <div class="form-group">
                <input type="text" name="no_nota" id="no_nota" class="form-control" placeholder="No. Nota" value="<?= set_value('no_nota', isset($param['no_nota']) ? $param['no_nota'] : '')  ?>">
            </div>

            <div class="form-group">
                <div class="input-daterange input-group" title="<?= lang('range_tgl') ?>" data-toggle="tooltip" data-placement="top">
                    <input type="text" style="width: 120px" class="form-control" name="tgl1" id="tgl1" value="<?= isset($param['tgl1']) ? $param['tgl1'] : '' ?>" placeholder="<?= lang('tgl_awal') ?>" readonly />
                    <span class="input-group-addon">to</span>
                    <input type="text" style="width: 120px" class="form-control" name="tgl2" id="tgl2" value="<?= isset($param['tgl2']) ? $param['tgl2'] : '' ?>" placeholder="<?= lang('tgl_akhir') ?>" readonly />
                </div>
            </div>

            <div class="form-group">
                <input type="hidden" name="id_konsumen_pilih" id="id_konsumen_pilih" value="<?= set_value('konsumen', isset($param['konsumen']) ? $param['konsumen'] : '') ?>">
                <select name="konsumen" id="konsumen" class="form-control" style="min-width: 300px !important" >
                    <option value=""></option>
                    <?php 
                    	if(isset($dt_konsumen) && is_array($dt_konsumen) && count($dt_konsumen)):
                    		foreach ($dt_konsumen as $key => $isi) :	
                    ?>

                    	<option value="<?= $isi->id ?>" <?= set_select('konsumen',$isi->id,isset($param['konsumen']) && $param['konsumen'] == $isi->id ) ?>  ><?= $isi->nm ?></option>

                    <?php
                    		endforeach;
                    	endif;
                    ?>
                </select>
            </div>

            <button type="submit" name='simpan' class="btn btn-primary">
            	<span class="fa fa-search"></span> <?= lang("btn-filter") ?>
            </button>

		</div>

	</div>

	<?php 

			if(isset($data) && is_array($data) && count($data)):

	?>

	<div class="box-body table-responsive">	

		<table class="table table-hover table-striped">
			<thead>
				<tr class="success">
					<th class="text-center" width="30">#</th>
					<th class="text-center"><?= lang("jdl-table-index-tgl") ?></th>
					<th class="text-center"><?= lang("jdl-table-index-nonota") ?></th>
					<th class="text-center"><?= lang("jdl-table-index-konsumen") ?></th>
					<th class="text-center"><?= lang("jdl-table-waktu-proses") ?></th>
					<th class="text-center"><?= lang("jdl-table-index-st-revisi") ?></th>
					<th class="text-center"><?= lang("jdl-table-ket") ?></th>
					<th class="text-center"><?= lang("jdl-table-view-op") ?></th>
					<th class="text-center" width="100"></th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($data as $key => $isi) :

						$class 		= '';
						$ket_st 	= "";

						if($isi->st_acc == 0){

							$class 	= "class = 'bg-red'";
							$ket_st = "Ijin dibatalkan";

						}else{

							$ket_st = "Diijinkan";

						}

				?>
				<tr <?= $class ?> >
					<td class="text-right">
						<?= $numb ?>
						<input type="hidden" name="dft_id[]" id="dft_id" value="<?= $isi->id_order ?>">						
					</td>		
					<td class="text-center"><?= $isi->tgl ?></td>
					<td class="text-center"><?= $isi->no_faktur ?></td>
					<td class="text-center"><?= $isi->nm_konsumen ?></td>
					<td class="text-center"><?= $isi->waktu_buat ?></td>
					<td class="text-center"><?= $ket_st ?></td>
					<td class="text-justify"><?= $isi->ket ?></td>
					<td class="text-center"><?= $isi->nm_lengkap ?></td>
					
					<td class="text-center">
						<a href="<?= site_url('order_produk/view/' . $isi->id_order); ?>" target="_blank" class="text-black">
							<span class="fa fa-folder"></span>
						</a>
					</td>
				</tr>
				<?php
					$numb++;
					endforeach;
				?>
			</tbody>
			
		</table>

	</div>
	<div class="box-footer clearfix">
		<?php echo $this->pagination->create_links();  ?>
	</div>
	<?php else: ?>

	<div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('konfirmasi-data-tidak-ada') ?></p>
    </div>

	<?php endif ?>

	<?php echo form_close(); ?>

</div>

