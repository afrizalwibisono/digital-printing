$(function(){

	$("#konsumen").select2({
		placeholder :"konsumen",
        allowClear  : true
	});

	get_nama_konsumen();

	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

});

function get_nama_konsumen(){

	var jns_konsumen 	= "";
	var id_pilih 		= $("#id_konsumen_pilih").val();

	var st_ada_pilih 	= id_pilih.length > 0 ? true : false;

	$.ajax({
				url 		: baseurl + "lap_rekap_trans/get_konsumen",
				type 		: "post",
				dataType 	: "json",
				data 		: {st : jns_konsumen},
				success 	: function(msg){

								if(msg['st'] == 1){

									$("#konsumen option").remove();

									var isi 	= "<option></option>";
									var select 	= "";

									$.each(msg['data'],function(i,n){

										if(st_ada_pilih  && n['id'] == id_pilih){

											select 	= "selected";

										}else{

											select = "";
										}

										isi += "<option value='" +n['id']+"' " + select + ">"+ n['nm'] +"</option>";

									});

									$("#konsumen").append(isi);

								}

							}


	});

}