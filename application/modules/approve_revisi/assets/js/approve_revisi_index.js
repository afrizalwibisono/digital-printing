$(function(){

	$("#konsumen").select2({
		placeholder :"konsumen",
        allowClear  : true
	});

	get_nama_konsumen();

	$("#modal").on("shown.bs.modal", function(e){

		$("#ket_approval").focus();

	});

	$("#simpan_modal").click(function(){

		simpan_perubahan();

	});

});

function simpan_perubahan(){

	var id_order 	= $("#mdl_idorder_proses").val();
	var ket 		= $("#ket_approval").val();
	var st_awal 	= $("#st_modal").data("st");

	if(ket.length <= 10){

		alertify.error("Keterangan harus diisi, minimum 10 karakter");
		$("#ket_approval").focus();

	}else{

		var data 	= {id : id_order, st_awal : st_awal, ket : ket};

		$.ajax({
					url 		: baseurl + "approve_revisi/simpan_acc",
					type 		: "post",
					data 		: data,
					dataType 	: "json",
					success 	: function(msg){

										if(parseFloat(msg['st']) == 0){

											alertify.error(msg['pesan']);

										}else{

											alertify.success(msg['pesan']);	
											$("#modal").modal("hide");

										}

									}

		});

	}

	

}

function perubahan_status(obj){

	var id_order	= $(obj).closest("tr").find("input[name='dft_id[]']").val();
	var no_nota		= $(obj).closest("tr").find("td:eq(2)").text();

	$.ajax({

				url 		: 	baseurl+"approve_revisi/cek_st_approval",
				type 		: 	"post",
				data 		: 	{id:id_order},
				success 	: 	function(msg){

									if(parseFloat(msg) == 0){

										$("#modal").removeClass("modal-danger");
										$("#modal").addClass("modal-success");
										$("#ket_st_ijin").html("1. Saat ini Anda memberikan ijin untuk Order dapat direvisi.");
										$("#st_modal").text("Tidak Boleh Revisi");
										$("#st_modal").data("st","0");

									}else{

										$("#modal").removeClass("modal-success");
										$("#modal").addClass("modal-danger");
										$("#ket_st_ijin").html("1. Saat ini Anda membatalkan ijin revisi Order.");
										$("#st_modal").text("Ijin Revisi Diberikan");
										$("#st_modal").data("st","1");

									}

									$("#ket_approval").val("");
									$("#mdl_idorder_proses").val(id_order);
									$("#id_modal").text(no_nota);
									$("#modal").modal("show");

								}

	});
	

}



function get_nama_konsumen(){

	var jns_konsumen 	= "";
	var id_pilih 		= $("#id_konsumen_pilih").val();

	var st_ada_pilih 	= id_pilih.length > 0 ? true : false;

	$.ajax({
				url 		: baseurl + "lap_rekap_trans/get_konsumen",
				type 		: "post",
				dataType 	: "json",
				data 		: {st : jns_konsumen},
				success 	: function(msg){

								if(msg['st'] == 1){

									$("#konsumen option").remove();

									var isi 	= "<option></option>";
									var select 	= "";

									$.each(msg['data'],function(i,n){

										if(st_ada_pilih  && n['id'] == id_pilih){

											select 	= "selected";

										}else{

											select = "";
										}

										isi += "<option value='" +n['id']+"' " + select + ">"+ n['nm'] +"</option>";

									});

									$("#konsumen").append(isi);

								}

							}


	});

}