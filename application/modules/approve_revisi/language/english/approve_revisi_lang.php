<?php defined('BASEPATH')|| exit('No direct script access allowed');

// =============================================================================================

$lang['judul']									= "Approval Revisi List Order";
$lang['judul-history']							= "History Approval Revisi";

// =============================================================================================
// ..:: Index ::..

$lang['jdl-table-index-tgl']					= 'Tanggal Order';
$lang['jdl-table-index-nonota']					= 'No. Nota';
$lang['jdl-table-index-konsumen']				= 'Konsumen';
$lang['jdl-table-index-total']					= 'Total';
$lang['jdl-table-index-st-revisi']				= 'Status Revisi';
$lang['jdl-table-view-op']						= 'Operator';
$lang['jdl-table-waktu-proses']					= 'Waktu Proses';
$lang['jdl-table-ket']							= 'Keterangan';

$lang['isi-stapp-diijinkan']					= 'Diijinkan';	
$lang['isi-stapp-noijinkan']					= 'Dibatalkan';	

$lang['jdl-modal']								= 'Approve Revisi List Order';
$lang['modal-nonota']							= 'No. Nota';
$lang['modal-strevisi']							= 'Status Sekarang';

// =============================================================================================

$lang['btn-filter']								= "Filter";
$lang['btn-save']								= "Simpan";
$lang['btn-save-app']							= "Simpan Perijinan";
$lang['btn-batal']								= "Batal";
$lang['btn-delete']								= "Hapus";
$lang['btn-or']									= "atau";

$lang['btn-history']							= "History";

$lang['btn-batal-kembali']						= "batal / kembali";

// =============================================================================================

$lang['error-hak-akses-manage']					= "Anda tidak memiliki hak untuk memberikan ijin Revisi";
$lang['error-proses-simpan'] 					= "Proses simpan Approval gagal";

$lang['sukses-simpan']							= "Proses Approval Revisi List Order sukses";

// =============================================================================================
$lang['konfirmasi-data-tidak-ada']				= "Data tidak ditemukan";

