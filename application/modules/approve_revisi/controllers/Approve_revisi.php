<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for Approve Revisis List Order
	
*/
class Approve_revisi extends Admin_Controller {

	/**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Approve Revisi List Order.View";
    protected $managePermission = "Approve Revisi List Order.Manage";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('approve_revisi/approve_revisi');
        $this->load->model(
                            [
                                'app_revisi_model',
                                'order_model',
                                'konsumen_model'
                            ]
    						);

        $this->template->title(lang('judul'));
		$this->template->page_icon('fa fa-clipboard');
    }

    public function index(){

        $this->auth->restrict($this->viewPermission);

        if(isset($_POST['no_nota'])){

            $no_nota    = isset($_POST['no_nota']) ? $this->input->post("no_nota") : '';
            $konsumen   = isset($_POST['konsumen']) ? $this->input->post("konsumen") : '';

        }else{

            $no_nota    = isset($_GET['no_nota']) ? $this->input->get("no_nota") : '';
            $konsumen   = isset($_GET['konsumen']) ? $this->input->get("konsumen") : '';

        }

        $where_add  = "";
        $filter     = "?apprev";

        if(strlen($no_nota) > 0){
            
            $where_add  = "and transaksi.no_faktur = '{$no_nota}'";
            $filter     .= "&no_nota={$no_nota}";
            
        }

        if(strlen($konsumen) > 0){

            $where_add  = "and order_produk.id_konsumen = {$konsumen}";
            $filter     .= "&konsumen={$konsumen}";

        }

        $where          = "order_produk.deleted = 0
                            AND `order_produk`.`st_simpan` = 1
                                AND DATEDIFF(DATE(NOW()),
                                    DATE_ADD(`order_produk`.`tgl_order`,
                                        INTERVAL 1 DAY)) <= 7
                            AND ((transaksi.st_history = 0
                            AND transaksi.deleted = 0)
                            OR transaksi.id_transaksi IS NULL) ".$where_add;
        
        $isi_filter     =   [
                                'no_nota'   => $no_nota,
                                'konsumen'  => $konsumen
                            ];

        $where_total    = "order_produk.deleted = 0
                            AND `order_produk`.`st_simpan` = 1
                                AND DATEDIFF(DATE(NOW()),
                                    DATE_ADD(`order_produk`.`tgl_order`,
                                        INTERVAL 1 DAY)) <= 7".$where_add;

        $total          = $this->order_model
                                ->select("`order_produk`.`id_order`,
                                            DATEDIFF(DATE(NOW()),
                                                DATE_ADD(`order_produk`.`tgl_order`,
                                                    INTERVAL 1 DAY)) AS lama")
                                ->join("transaksi","order_produk.id_order = transaksi.id_order","left")
                                ->where($where)
                                ->order_by("order_produk.tgl_order","desc")
                                ->count_all();    

        

        $this->load->library('pagination');
    
        $offset = $this->input->get('per_page');

        //$limit  = $this->config->item('list_limit');
        $limit  = 10;

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);
        
        $data   =   $this->order_model
                            ->select("`transaksi`.`id_transaksi`,
                                        `order_produk`.`id_order`,
                                        `transaksi`.`no_faktur`,
                                        DATE_FORMAT(`order_produk`.`tgl_order`, '%d/%m/%Y') AS tgl,
                                        `order_produk`.`total_value_order`,
                                        `transaksi`.`grand_total`,
                                        IF(`transaksi`.`grand_total` > 0
                                                OR `transaksi`.`grand_total` IS NOT NULL,
                                            `transaksi`.`grand_total`,
                                            `order_produk`.`total_value_order`) AS total_transaksi,
                                        IF(LENGTH(`konsumen`.`panggilan`) > 0,
                                            CONCAT(`konsumen`.`panggilan`,
                                                    ' ',
                                                    `konsumen`.`nama`),
                                            `konsumen`.`nama`) AS nm_konsumen,
                                        DATE(NOW()),
                                        DATE_ADD(`order_produk`.`tgl_order`,
                                            INTERVAL 1 DAY) AS tgl_acuan_start_hitung,
                                        DATEDIFF(DATE(NOW()),
                                                DATE_ADD(`order_produk`.`tgl_order`,
                                                    INTERVAL 1 DAY)) AS lama")
                            ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                            ->join("transaksi","order_produk.id_order = transaksi.id_order","left")
                            ->where($where)
                            ->order_by("order_produk.tgl_order","desc")
                            ->limit($limit, $offset)
                            ->find_all();

        $dt_konsumen    = $this->get_konsumen();

        $asset =    [
                        "plugins/select2/dist/css/select2.min.css",
                        "plugins/select2/dist/js/select2.min.js",
                        "approve_revisi/assets/js/approve_revisi_index.js"    
                    ];

        add_assets($asset);

        $this->template->set('param',$isi_filter);

        $this->template->set('data',$data);
        $this->template->set('dt_konsumen',$dt_konsumen);

        $this->template->set("toolbar_title", lang('judul'));
        $this->template->set("page_title", lang('judul'));
        $this->template->render('approve_revisi_index'); 

    }

    public function history(){

        $this->auth->restrict($this->viewPermission);

        $where  = "";

        if(isset($_POST['no_nota'])){

            $no_nota    = isset($_POST['no_nota']) ? $this->input->post("no_nota") : '';
            $konsumen   = isset($_POST['konsumen']) ? $this->input->post("konsumen") : '';

            $tgl1       = isset($_POST['tgl1']) ? $this->input->post('tgl1') : '';
            $tgl2       = isset($_POST['tgl2']) ? $this->input->post('tgl2') : '';

        }else{

            $no_nota    = isset($_GET['no_nota']) ? $this->input->get("no_nota") : '';
            $konsumen   = isset($_GET['konsumen']) ? $this->input->get("konsumen") : '';
            $tgl1       = isset($_GET['tgl1']) ? $this->input->get('tgl1') : '';
            $tgl2       = isset($_GET['tgl2']) ? $this->input->get('tgl2') : '';

        }

        if(strlen($tgl1) > 0){

            $tgl1_cr    = date_ymd($tgl1)." 00:00:01";
            $tgl2_cr    = date_ymd($tgl2)." 23:59:59";        
             
        }else{

            $tgl1_cr    = date("Y-m-d")." 00:00:01";
            $tgl2_cr    = date("Y-m-d")." 23:59:59";

            $tgl1       = date("d/m/Y");
            $tgl2       = date("d/m/Y");

        }

        $where  .= "(`app_revisi`.`created_on` >= '{$tgl1_cr}' and `app_revisi`.`created_on` <= '{$tgl2_cr}') 
                        and `order_produk`.`deleted` = 0
                        AND ((transaksi.st_history = 0
                        AND `transaksi`.`deleted` = 0)
                        OR `transaksi`.`id_transaksi` IS NULL)";

        $isi_filter =   [
                            'tgl1'      => $tgl1,
                            'tgl2'      => $tgl2,
                            'no_nota'   => $no_nota,
                            'konsumen'  => $konsumen
                        ];

        $filter     = "?tgl1={$tgl1}&tgl2={$tgl2}";

        $where_add  = "";

        if(strlen($no_nota) > 0){
            
            $where_add  = "and transaksi.no_faktur = '{$no_nota}'";
            $filter .= "&no_nota={$no_nota}";
            
        }

        if(strlen($konsumen) > 0){

            $where_add  = "and order_produk.id_konsumen = {$konsumen}";
            $filter .= "&konsumen={$konsumen}";

        }

        $where .= $where_add;

        $this->load->library('pagination');

        $total  = $this->app_revisi_model->select("`app_revisi`.`idapp_revisi`")
                        ->join("users","app_revisi.created_by = users.id_user","inner")
                        ->join("order_produk","app_revisi.id_order = order_produk.id_order","inner")
                        ->join("`konsumen`","`order_produk`.`id_konsumen` = `konsumen`.`idkonsumen`")
                        ->join("`transaksi`","`order_produk`.`id_order` = `transaksi`.`id_order`","left")
                        ->where($where)
                        ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data   = $this->app_revisi_model
                        ->select("`app_revisi`.`idapp_revisi`,
                                    `app_revisi`.`id_order`,
                                    `app_revisi`.`st_acc`,
                                    `app_revisi`.`tgl_valid`,
                                    `app_revisi`.`ket`,
                                    DATE_FORMAT(`app_revisi`.`created_on`,'%d/%m/%Y %H:%i:%s') as waktu_buat,
                                    `users`.`nm_lengkap`,
                                    `transaksi`.`id_transaksi`,
                                    `transaksi`.`no_faktur`,
                                    DATE_FORMAT(`order_produk`.`tgl_order`, '%d/%m/%Y') AS tgl,
                                    `order_produk`.`total_value_order`,
                                    `transaksi`.`grand_total`,
                                    IF(`transaksi`.`grand_total` > 0
                                            OR `transaksi`.`grand_total` IS NOT NULL,
                                        `transaksi`.`grand_total`,
                                        `order_produk`.`total_value_order`) AS total_transaksi,
                                    IF(LENGTH(`konsumen`.`panggilan`) > 0,
                                        CONCAT(`konsumen`.`panggilan`,
                                                ' ',
                                                `konsumen`.`nama`),
                                        `konsumen`.`nama`) AS nm_konsumen")
                        ->join("users","app_revisi.created_by = users.id_user","inner")
                        ->join("order_produk","app_revisi.id_order = order_produk.id_order","inner")
                        ->join("`konsumen`","`order_produk`.`id_konsumen` = `konsumen`.`idkonsumen`")
                        ->join("`transaksi`","`order_produk`.`id_order` = `transaksi`.`id_order`","left")
                        ->where($where)
                        ->order_by("`app_revisi`.`created_on`","desc")
                        ->limit($limit, $offset)
                        ->find_all();

        $asset =    [
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        "plugins/select2/dist/css/select2.min.css",
                        "plugins/select2/dist/js/select2.min.js",
                        "approve_revisi/assets/js/approve_revisi_history.js"    
                    ];

        add_assets($asset);

        $this->template->set("numb",$offset+1);
        $this->template->set("param",$isi_filter);
        $this->template->set("data",$data);

        $this->template->title(lang('judul-history'));
        $this->template->set("toolbar_title", lang('judul-history'));
        $this->template->set("page_title", lang('judul-history'));
        $this->template->render('approve_revisi_history'); 

    }
    
    function get_konsumen(){

        $data   = $this->konsumen_model->select("`idkonsumen` as id,
                                                CONCAT(`konsumen`.`panggilan`,
                                                ' ',
                                                `konsumen`.`nama`) AS nm")
                        ->where("deleted = 0")
                        ->order_by("`konsumen`.`nama`")
                        ->find_all();

        if(is_array($data) && count($data)){

            return $data;

        }else{

            return false;

        }

    }

    public function cek_st_approval(){

        if(!$this->input->is_ajax_request()){

            redirect("approve_revisi");

        }

        $id_order   = $this->input->post("id");
        $tgl_cek    = date("Y-m-d");

        $st_acc     = 0;

        $cek        = $this->app_revisi_model
                            ->select("st_acc")
                            ->where("deleted = 0 and tgl_valid = '{$tgl_cek}' and id_order = '{$id_order}'")
                            ->order_by("created_on","desc")
                            ->limit(1)
                            ->find_all();

        if(is_array($cek) && count($cek)){

            $st_acc     = $cek[0]->st_acc;

        }

        echo $st_acc;

    }

    public function simpan_acc(){

        if(!$this->input->is_ajax_request()){

            redirect("approve_revisi");

        }

        if(!has_permission($this->managePermission)){

            $return     = [
                                'st'    => 0,
                                'pesan' => lang("error-hak-akses-manage") 
                            ];

            echo json_encode($return);
            return false;

        }

        $id_order   = $this->input->post('id');
        $st_awal    = $this->input->post('st_awal');   
        $ket        = $this->input->post('ket');

        $st_proses  = $st_awal == 0 ? 1 : 0;
        $tgl        = date('Y-m-d');

        $this->db->trans_start();

            $arr_isi    =   [
                                'idapp_revisi'  => gen_primary("","app_revisi","idapp_revisi"), 
                                'id_order'      => $id_order, 
                                'st_acc'        => $st_proses, 
                                'tgl_valid'     => $tgl, 
                                'ket'           => $ket
                            ];

            $this->app_revisi_model->insert($arr_isi);

        $this->db->trans_complete();        

        if($this->db->trans_status() == false){

            $return     =   [
                                'st'    => 0,
                                'pesan' => lang("error-proses-simpan")
                            ];

        }else{

            $return     =   [
                                'st'    => 1,
                                'pesan' => lang("sukses-simpan")
                            ];            

        }

        echo json_encode($return);

    }


}	