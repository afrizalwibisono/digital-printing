<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['gudang_title_manage'] 	= 'Manage Gudang';
$lang['gudang_title_new'] 	= 'Gudang Baru';
$lang['gudang_title_edit'] 	= 'Edit Data Gudang';

// form/table
$lang['gudang_name'] 			= 'Nama Gudang';
$lang['gudang_alamat'] 			= 'Alamat';
$lang['gudang_penanggung_jawab']	= 'Penanggung Jawab';
$lang['gudang_tlp']				= 'Telepon';
$lang['gudang_status']			= 'Status';

// label
$lang['label_retur'] 	= 'Retur';
$lang['label_kanvas'] 	= 'Kanvas';
$lang['label_baik']		= 'Baik';



// button
$lang['gudang_btn_new'] 	= 'Baru';
$lang['gudang_btn_delete'] 	= 'Hapus';
$lang['gudang_btn_save'] 	= 'Simpan';
$lang['gudang_btn_cancel'] 	= 'Batal';
$lang['gudang_or']			= ' Atau ';

// messages
$lang['gudang_del_error']			= 'Anda belum memilih gudang yang akan dihapus.';
$lang['gudang_del_failure']		= 'Tidak dapat menghapus gudang: ';
$lang['gudang_delete_confirm']	= 'Apakah anda yakin akan menghapus gudang terpilih ?';
$lang['gudang_deleted']			= 'gudang berhasil dihapus';
$lang['gudang_no_records_found'] 	= 'Data tidak ditemukan.';

$lang['gudang_create_failure'] 	= 'Gudang baru gagal disimpan: ';
$lang['gudang_create_success'] 	= 'Gudang baru berhasil disimpan';

$lang['gudang_edit_success'] 	= 'Gudang berhasil disimpan';
$lang['gudang_invalid_id'] 		= 'ID Tidak Valid';