<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_gudang','name'=>'frm_gudang','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="form-group <?= form_error('nama_gudang') ? ' has-error' : ''; ?>">
			    <label for="nama_gudang" class="col-sm-2 control-label"><?= lang('gudang_name') ?></label>
			    <div class="col-sm-3">
			    	<input type="text" class="form-control" id="nama_gudang" name="nama_gudang" maxlength="255" value="<?php echo set_value('nama_gudang', isset($data->nama_gudang) ? $data->nama_gudang : ''); ?>" required autofocus>
			    </div>
		  	</div>
		  	<div class="form-group <?= form_error('alamat') ? ' has-error' : ''; ?>">
			    <label for="alamat" class="col-sm-2 control-label"><?= lang('gudang_alamat') ?></label>
			    <div class="col-sm-3">
			    	<input type="text" class="form-control" id="alamat" name="alamat" maxlength="255" value="<?php echo set_value('alamat', isset($data->alamat) ? $data->alamat : ''); ?>" required>
			    </div>
		  	</div>
		  	<div class="form-group <?= form_error('penanggung_jawab') ? ' has-error' : ''; ?>">
			    <label for="penanggung_jawab" class="col-sm-2 control-label"><?= lang('gudang_penanggung_jawab') ?></label>
			    <div class="col-sm-3">
			    	<input type="text" class="form-control" id="penanggung_jawab" name="penanggung_jawab" maxlength="255" value="<?php echo set_value('penanggung_jawab', isset($data->penanggung_jawab) ? $data->penanggung_jawab : ''); ?>" required>
			    </div>
		  	</div>
		  	<div class="form-group <?= form_error('tlp') ? ' has-error' : ''; ?>">
			    <label for="tlp" class="col-sm-2 control-label"><?= lang('gudang_tlp') ?></label>
			    <div class="col-sm-3">
			    	<input type="text" onkeypress="return phone_number_validate(event)" class="form-control" id="tlp" name="tlp" maxlength="255" value="<?php echo set_value('tlp', isset($data->tlp) ? $data->tlp : ''); ?>" required>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      	<button type="submit" name="save" class="btn btn-primary"><?= lang('gudang_btn_save') ?></button>
			    	<?php
	                	echo lang('gudang_or') . ' ' . anchor('gudang', lang('gudang_btn_cancel'));
	                ?>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->