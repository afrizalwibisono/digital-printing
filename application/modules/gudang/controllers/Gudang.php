<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for gudang
 */

class Gudang extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Gudang.View";
    protected $addPermission    = "Gudang.Add";
    protected $managePermission = "Gudang.Manage";
    protected $deletePermission = "Gudang.Delete";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('gudang/gudang');
        $this->load->model('gudang/gudang_model');

        $this->template->title(lang('gudang_title_manage'));
		$this->template->page_icon('fa fa-th-large');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if (isset($_POST['delete']) && has_permission($this->deletePermission))
        {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                foreach ($checked as $pid)
                {
                    $nama_gudang   = $this->gudang_model->find($pid)->nama_gudang;
                    $result     = $this->gudang_model->delete($pid);

                    if($result)
                    {
                        $keterangan = "SUKSES, hapus data gudang ".$nama_gudang.", dengan ID : ".$pid;
                        $status     = 1;
                    }
                    else
                    {
                        $keterangan = "GAGAL, hapus data gudang ".$nama_gudang.", dengan ID : ".$pid;
                        $status     = 0;
                    }

                    $nm_hak_akses   = $this->deletePermission; 
                    $kode_universal = $pid;
                    $jumlah         = 1;
                    $sql            = $this->db->last_query();

                    simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                }

                if ($result)
                {
                    $this->template->set_message(count($checked) .' '. lang('gudang_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('gudang_del_failure') . $this->gudang_model->error, 'error');
                }
            }
            else
            {
                $this->template->set_message(lang('gudang_del_error') . $this->gudang_model->error, 'error');
            }
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
        }

        $filter = "";
        if($search!="")
        {
            $filter = "?search=".$search;
        }

        $search2 = $this->db->escape_str($search);
        
        $where = "`deleted` = 0
                AND (`nama_gudang` LIKE '%$search2%' ESCAPE '!'
                OR `nama_gudang` LIKE '%$search2%' ESCAPE '!'
                OR `alamat` LIKE '%$search2%')";
        
        $total = $this->gudang_model
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        //$limit = $this->settings_lib->item('site.list_limit');
        $limit = 25;

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->gudang_model
                    ->where($where)
                    ->order_by('nama_gudang','ASC')
                    ->limit($limit, $offset)->find_all();

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set("toolbar_title", lang('gudang_title_manage'));
        $this->template->set("page_title", lang('gudang_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

   	//Create New Gudang
   	public function create()
   	{

        $this->auth->restrict($this->addPermission);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_gudang())
            {
              $this->template->set_message(lang("gudang_create_success"), 'success');
              redirect('gudang');
            }
        }
        $assets = array(
                    'plugins/select2/css/select2.min.css',
                    'plugins/select2/js/select2.min.js',
                    'gudang/assets/js/gudang.js',
                    );

        add_assets($assets);
        $this->template->set("page_title", lang('gudang_title_new'));
   	    $this->template->render('gudang_form');
   	}

   	//Edit Supplier
   	public function edit()
   	{
   		
  		$this->auth->restrict($this->managePermission);
                
        $id = (int)$this->uri->segment(3);

        if (empty($id))
        {
            $this->template->set_message(lang("gudang_invalid_id"), 'error');
            redirect('gudang');
        }

        if (isset($_POST['save']))
        {
            if ($this->save_gudang('update', $id))
            {
                $this->template->set_message(lang("gudang_edit_success"), 'success');
            }

        }

        
        $data  = $this->gudang_model->find_by(array('id_gudang' => $id, 'deleted' => 0));

        if(!$data)
        {
            $this->template->set_message(lang("gudang_invalid_id"), 'error');
            redirect('gudang');
        }
        
        $this->template->set('data', $data);
        $this->template->set('toolbar_title', lang("gudang_title_edit"));
        $this->template->set('page_title', lang("gudang_title_edit"));
        $this->template->render('gudang_form');
        
   	}

   	protected function save_gudang($type='insert', $id=0)
   	{

        $this->form_validation->set_rules('nama_gudang','lang:gudang_name','required|trim|max_length[30]');
        $this->form_validation->set_rules('alamat','lang:gudang_alamat','required|trim|max_length[100]','trim');
        $this->form_validation->set_rules('penanggung_jawab','lang:gudang_penanggung_jawab','trim');
        $this->form_validation->set_rules('tlp','lang:gudang_tlp','trim|callback_tlp_validation');

        if ($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        unset($_POST['submit'], $_POST['save']);

        if ($type == 'insert')
        {
            $id = $this->gudang_model->insert($_POST);

            if (is_numeric($id))
            {
                //Save Log
                if($id)
                {
                    $keterangan = "SUKSES, tambah data gudang ".$this->input->post('nama_gudang').", dengan ID : ".$id;
                    $status     = 1;
                }
                else
                {
                    $keterangan = "GAGAL, tambah data gudang ".$this->input->post('nama_gudang').", dengan ID : ".$id;
                    $status     = 0;
                }

                $nm_hak_akses   = $this->addPermission; 
                $kode_universal = $id;
                $jumlah         = 1;
                $sql            = $this->db->last_query();
                //menyimpan ke Log histori
                simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

                $return = TRUE;
            }
            else
            {
                $return = FALSE;
            }
        }
        else if ($type == 'update')
        {
            $return = $this->gudang_model->update($id, $_POST);

            if($return)
            {
                $keterangan = "SUKSES, ubah data gudang ".$this->input->post('nama_gudang').", dengan ID : ".$id;
                $status     = 1;
            }
            else
            {
                $keterangan = "GAGAL, ubah data gudang ".$this->input->post('nama_gudang').", dengan ID : ".$id;
                $status     = 0;
            }

            //Save Log
            $nm_hak_akses   = $this->managePermission; 
            $kode_universal = $id;
            $jumlah         = 1;
            $sql            = $this->db->last_query();
            //menyimpan ke log historis
            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
        }

        return $return;
   	}
    public function tlp_validation($value){
        $value = trim($value);
        if ($value == '') {
                return TRUE;
        }
        else
        {
                if (preg_match("/^[\+0-9\-\(\)\s]*$/", $value))
                {
                        return TRUE;
                }
                else
                {
                        $this->form_validation->set_message("tlp_validation","Invalid phone number format");
                        return FALSE;
                }
        }
    }

}
?>