<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ========================================================================================

$lang['title_index']			= 'Data Gol. Konsumen';

// ========================================================================================
// ..:: ** index dan form ** ::..

$lang['model-nmgol']			= 'Nama Golongan';
$lang['model-ket']				= 'Keterangan';

$lang['table-nama']				= 'Nama Golongan';
$lang['table-ket']				= 'Keterangan';

// ========================================================================================
// ..:: ** button ** ::..

$lang['btn_new']				= 'Baru';
$lang['btn_delete']				= 'Hapus';
$lang['btn_save']				= 'Simpan';
$lang['btn_cancel']				= 'Batal';
$lang['btn_or']					= 'or';

//=========================================================================================
// ..:: ** messages ** ::..

$lang['del_error']				= 'Anda belum memilih data Gol. Konsumen yang akan dihapus.';
$lang['del_failure']			= 'Tidak dapat menghapus data Gol. Konsumen';
$lang['delete_confirm']			= 'Apakah anda yakin akan menghapus data Gol. Konsumen terpilih?';
$lang['deleted']				= 'Data Gol. Konsumen berhasil dihapus';
$lang['no_records_found']		= 'Data tidak ditemukan.';

$lang['create_failure']			= 'Data kategori gagal disimpan';
$lang['create_success']			= 'Data kategori berhasil disimpan';

$lang['edit_success']			= 'Data kategori berhasil disimpan';
$lang['invalid_id']				= 'ID tidak Valid';

