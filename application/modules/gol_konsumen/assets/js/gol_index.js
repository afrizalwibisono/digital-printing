$(function(){

	$("#tbl-baru").click(function(){

		$("#jdl-modal").text("Gol. Konsumen Baru");
		$("#st_inputan").val(0);

		$("#id_konsumen").val("");
		$("#nm_gol").val("");
		$("#ket_gol").val("");

		$("#modal-editor").modal("show");

	});

	$("#simpan-proses").click(function(){

		simpan();

	});

});

function edit_data(obj){

	var id 		= $(obj).closest("tr").find("input[name='dft_id[]']").val();
	var nm 		= $(obj).closest("tr").find("td:eq(2)").text();
	var ket 	= $(obj).closest("tr").find("td:eq(3)").text();
	
	$("#jdl-modal").text("Edit Gol. Konsumen");
	$("#st_inputan").val(1);
	$("#id_konsumen").val(id);
	$("#nm_gol").val(nm);
	$("#ket_gol").val(ket);

	$("#modal-editor").modal("show");

}

function simpan(){

	var data 	= 	{
						st 	: $("#st_inputan").val(),
						id 	: $("#id_konsumen").val(),
						nm 	: $("#nm_gol").val(),
						ket : $("#ket_gol").val()
					};

	$.ajax({
				url 	: baseurl + "gol_konsumen/simpan",
				type 	: "post",
				data 	: data,
				success : function(msg){

								var n = parseFloat(msg);

								if(n == 0){

									alertify.error('Proses Simpan Gagal');

								}else{

									alertify.success('Proses Simpan Success');	
									location.reload();								

								}

								$("#modal-editor").modal("hide");									

							}
							
	})

}