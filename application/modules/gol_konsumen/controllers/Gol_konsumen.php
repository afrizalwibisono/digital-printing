<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 * 
 * This is controller for Merk

 */

class Gol_konsumen extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewGol   = "Golongan Konsumen.View";
    protected $addGol    = "Golongan Konsumen.Add";
    protected $manageGol = "Golongan Konsumen.Manage";
    protected $deleteGol = "Golongan Konsumen.Delete";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewGol);

        $this->lang->load('gol_konsumen/gol_konsumen');
        $this->load->model(['gol_konsumen_model']);

        $this->template->title(lang('title_index'));
		$this->template->page_icon('fa fa-table');
    }

    public function index(){

        $this->auth->restrict($this->viewGol);

        if(has_permission($this->deleteGol) && isset($_POST['delete'])){

            if($this->delete_data()){

                redirect("gol_konsumen");

            }

        }

        $search     = isset($_POST["cari"]) ? $this->input->post("cari") : "";
        
        $where      = "deleted = 0 and st_go_konsumen = 1";

        $search2    = $this->db->escape_str($search);

        if(strlen($search2) > 0){

            $where  .= " and (nm_gol like '%{$search2}%' or ket like '%{$search2}%' )";

        }

        $filter = "?cari=".$search2;

        $total  = $this->gol_konsumen_model
                        ->where($where)
                        ->order_by('nm_gol')
                        ->count_all();

        $this->load->library('pagination');

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data   = $this->gol_konsumen_model
                        ->select("`id_gol`, `st_fix`, `nm_gol`, `ket`")
                        ->where($where)
                        ->order_by('nm_gol')
                        ->limit($limit, $offset)
                        ->find_all();

        $assets     =   [
                            'gol_konsumen/assets/js/gol_index.js'
                        ];

        add_assets($assets);

        $this->template->set('data', $data);
        $this->template->set('cari', $search);
        $this->template->set("toolbar_title", lang('title_index'));
        $this->template->title(lang('title_index'));
        $this->template->set("numb", $offset+1);
        $this->template->render('golkonsumen_index'); 
    }

    private function delete_data(){

        if(!isset($_POST['checked'])){

            $this->template->set_message(lang('del_error'),'error');
            return false;

        }

        $id_pil     = $this->input->post("checked");

        $this->db->trans_start();

        foreach ($id_pil as $key => $id) {
            
            $this->gol_konsumen_model->delete($id);

        }

        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            $this->template->set_message(lang('del_failure'),'error');
            return false;

        }else{

            $this->template->set_message(lang('deleted'),'success');
            return true;            

        }

    }

    public function simpan(){

        if(!$this->input->is_ajax_request()){

            redirect('gol_konsumen');

        }

        $this->auth->restrict($this->addKategori);        

        $st     = $this->input->post('st');
        $id     = $this->input->post('id');
        $nm     = $this->input->post('nm');
        $ket    = $this->input->post('ket');

        if(strlen($nm) <= 0 && $st == 0){

            $return = 0;
            echo $return;

            return false;

        }

        $this->db->trans_start();

        $arr_isi    =   [
                            'nm_gol'    => $nm, 
                            'ket'       => $ket
                        ];

        if($st == 0){ //data baru

            $this->gol_konsumen_model->insert($arr_isi);

        }else{

            $this->gol_konsumen_model->update($id, $arr_isi);

        }


        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            $return     = 0;

        }else{

            $return     = 1;

        }

        echo $return;

    }
    
}
?>