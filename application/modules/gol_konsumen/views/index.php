<?php
	$ENABLE_ADD		= has_permission('Kategori.Add');
	$ENABLE_MANAGE	= has_permission('Kategori.Manage');
	$ENABLE_DELETE	= has_permission('Kategori.Delete');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_kategori','name'=>'frm_kategori','ket'=>'frm_ket')) ?>
	<div class="box-header">
		<div class="form-group">
			<?php if ($ENABLE_ADD): ?>
			<a href="<?= site_url('kategori/create') ?>" class="btn btn-success" title="<?= lang('kategori_btn_new') ?>"><?= lang('kategori_btn_new') ?></a>
			<?php endif;?>
			<div class="pull-right">
				<div class="input-group">
					<input type="text" name="table_search" value="<?php echo isset($search) ? $search:''; ?>" class="form-control pull-right" placeholder="Search" autofocus>
					<div class="input-group-btn">
						<button class="btn btn-default"><i class="fa fa-search"></i></button>
					</div>
				</div>	
			</div>
		</div>
	</div>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
	<div class="box-body table-responsive no-padding">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
                        <th width="50">#</th>
                        <th><?= lang('kategori_name') ?></th>
                        <th class="text-center"><?= lang('kategori_dimensi')."(mm)" ?></th>
                        <th><?= lang('kategori_ket') ?></th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($results as $record) : ?>
                    <tr>
                    	<?php if($record->st_fix == '1') : ?>
                    		<td></td>
                    	<?php else:?>
                    		<td class="column-check"><input type="checkbox" name="checked[]" value="<?= $record->idkategori ?>" /></td>
                    	<?php endif; ?>
                        
                        <td><?= $numb; ?></td>
                        <td><?= strtoupper($record->nmkategori) ?></td>
                        <td class="text-center">
                        	<?php

                        		if($record->st_ukuran == 1 ){

                        			echo "<strong>P:</strong>".number_format($record->p)." x <strong>L:</strong>".number_format($record->p);

                        		}

                        	?>
                        </td>
                        <td><?= $record->ket ?></td>
                    </tr>
                    <?php $numb++; endforeach; ?>
                </tbody>
	  </table>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">
		<?php if($ENABLE_DELETE) : ?>
		<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('kategori_btn_delete') ?>" onclick="return confirm('<?= lang('kategori_delete_confirm'); ?>')">
		<?php endif;
		echo $this->pagination->create_links(); 
		?>
	</div>
	<?php else: ?>
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('kategori_no_records_found') ?></p>
    </div>
    <?php
	endif;
	echo form_close(); 
	?>	
</div><!-- /.box-->