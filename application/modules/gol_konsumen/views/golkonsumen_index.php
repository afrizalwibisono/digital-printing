<?php
	$ENABLE_ADD		= has_permission('Golongan Konsumen.Add');
	$ENABLE_MANAGE	= has_permission('Golongan Konsumen.Manage');
	$ENABLE_DELETE	= has_permission('Golongan Konsumen.Delete');
?>

<div class="box box-primary">
	
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_kategori','name'=>'frm_kategori','ket'=>'frm_ket')) ?>

	<div class="box-header">
		<div class="form-group">
			<?php if ($ENABLE_ADD): ?>
				<a href="#" id="tbl-baru" class="btn btn-success" title="<?= lang('btn_new') ?>"><?= lang('btn_new') ?></a>
			<?php endif;?>
			<div class="pull-right">
                <div class="input-group">
                    <input type="text" name="cari" value="<?php echo isset($cari) ? $cari : ''; ?>" class="form-control pull-right" placeholder="Search" autofocus />
                    <div class="input-group-btn">
                        <button class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
		</div>
	</div>	
	
	<?php if(isset($data) && is_array($data) && count($data)) : ?>

	<div class="box-body no-padding table-responsive">
		
		<table class="table table-hover table-striped">
			<thead>
				<th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
				<th class="text-center" width="50">No.</th>
				<th class="text-center"><?= lang("table-nama") ?></th>
				<th class="text-center"><?= lang("table-ket") ?></th>
				<th class="text-center">&nbsp</th>
			</thead>
			<tbody>
				<?php foreach ($data as $key => $dt) : ?>
				<tr>
					<td class="column-check">
						<?php if($dt->st_fix == 0) : ?>
							<input type="checkbox" name="checked[]" value="<?= $dt->id_gol ?>" />
						<?php endif; ?>
						<input type="hidden" name="dft_id[]" id="dft_id" value="<?= $dt->id_gol ?>">
					</td>
					<td class="text-right"></td>		
					<td class="text-center"><?= strtoupper($dt->nm_gol) ?></td>
					<td ><?= $dt->ket ?></td>
					<td class="text-center">
						<a class="text-black" onclick="edit_data(this);return false;" href="#" data-toggle="tooltip" data-placement="left" title="Edit Golongan"><i class="fa fa-pencil"></i></a>
					</td>
				</tr>
				<?php endforeach; ?>	
			</tbody>
		</table>

	</div>
	<div class="box-footer clearfix">
		<?php if($ENABLE_DELETE) : ?>
		<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('btn_delete') ?>" onclick="return confirm('<?= (lang('delete_confirm')); ?>')">
		<?php 
				endif;
				echo $this->pagination->create_links(); 
		?>
	</div>
	<?php else : ?>
		<div class="alert alert-info" role="alert">
        	<p><i class="fa fa-warning"></i> &nbsp; <?= lang('no_records_found') ?></p>
    	</div>
	<?php endif ?>

	<?= form_close() ?>

</div>

<div class="modal fade" id="modal-editor">
	<div class="modal-dialog">
	    <div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">×</span></button>
	        	<h4 class="modal-title">
	        		<span id="jdl-modal"></span>
	        	</h4>
	      	</div>
	      	<div class="modal-body form-horizontal">

	      		<input type="hidden" name="st_inputan" id="st_inputan">
	      		<input type="hidden" name="id_konsumen" id="id_konsumen">

	        	<div class="form-group">
	        		<label class="control-label col-sm-4" for="nm_gol"><?= lang('model-nmgol') ?></label>
	        		<div class="col-sm-8">
	        			<input type="text" name="nm_gol" id="nm_gol" class="form-control">
	        		</div>
	        	</div>

	        	<div class="form-group">
	        		<label class="control-label col-sm-4" for="ket_gol"><?= lang('model-ket') ?></label>
	        		<div class="col-sm-8">
	        			<textarea class="form-control" name="ket_gol" id="ket_gol"></textarea>
	        		</div>
	        	</div>

	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
	        	<button type="button" id="simpan-proses" class="btn btn-primary"><?= lang('btn_save') ?></button>
	      	</div>
	    </div>
	    <!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>