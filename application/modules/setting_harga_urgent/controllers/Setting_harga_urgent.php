<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 * 
 * This is controller for Merk

 */

class Setting_harga_urgent extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Setting Urgent.View";
    protected $addPermission    = "Setting Urgent.Add";
    protected $managePermission = "Setting Urgent.Manage";
    protected $deletePermission = "Setting Urgent.Delete";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('setting_harga_urgent');
        $this->load->model(array(
                                'setting_harga_urgent_model'
                            ));

        $this->template->title(lang('setting_harga_urgent_title_manage'));
		$this->template->page_icon('fa fa-table');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if (isset($_POST['delete']) && has_permission($this->deletePermission))
        {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                foreach ($checked as $pid)
                {
                    $result = $this->setting_harga_urgent_model->delete($pid);
                }

                if ($result)
                {
                    $this->template->set_message(count($checked) .' '. lang('setting_harga_urgent_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('setting_harga_urgent_del_failure') . $this->setting_harga_urgent_model->error, 'error');
                }
            }
            else
            {
                $this->template->set_message(lang('setting_harga_urgent_del_error') . $this->setting_harga_urgent_model->error, 'error');
            }
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
        }

        $filter = "";
        if($search!="")
        {
            $filter = "?search=".$search;
        }

        $search2 = $this->db->escape_str($search);
        
        $where = "deleted = 0
                AND (`durasi_awal` LIKE '%$search2%' ESCAPE '!'
                OR `durasi_akhir` LIKE '%$search2%' ESCAPE '!')";
                
        $total = $this->setting_harga_urgent_model
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->setting_harga_urgent_model
                    ->where($where)
                    ->order_by('durasi_awal','ASC')
                    ->limit($limit, $offset)->find_all();

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set("toolbar_title", lang('setting_harga_urgent_title_manage'));
        $this->template->title(lang('setting_harga_urgent_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

    public function create()
    {

        $this->auth->restrict($this->addPermission);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_setting_harga_urgent())
            {
              $this->template->set_message(lang("setting_harga_urgent_create_success"), 'success');
              redirect('setting_harga_urgent');
            }
        }

        $this->template->set("page_title", lang('setting_harga_urgent_title_new'));
        $this->template->render('form');
    }

    protected function save_setting_harga_urgent($type='insert', $id=0)
    {

        $this->form_validation->set_rules('durasi_awal','lang:setting_harga_urgent_durasi_awal','required|numeric');
        $this->form_validation->set_rules('durasi_akhir','lang:setting_harga_urgent_durasi_akhir','required|numeric');
        $this->form_validation->set_rules('persen_harga','lang:setting_harga_urgent_persen','required|numeric');

        if ($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        unset($_POST['submit'], $_POST['save']);

        if ($type == 'insert')
        {
            $id = $this->setting_harga_urgent_model->insert($_POST);

            if (is_numeric($id))
            {
                $return = TRUE;
            }
            else
            {
                $return = FALSE;
            }
        }
        else if ($type == 'update')
        {
            $return = $this->setting_harga_urgent_model->update($id, $_POST);
        }

        return $return;
    }

    public function edit()
    {
        
        $this->auth->restrict($this->managePermission);
                
        $id = (int)$this->uri->segment(3);

        if (empty($id))
        {
            $this->template->set_message(lang("setting_harga_urgent_invalid_id"), 'error');
            redirect('setting_harga_urgent');
        }

        if (isset($_POST['save']))
        {
            if ($this->save_setting_harga_urgent('update', $id))
            {
                $this->template->set_message(lang("setting_harga_urgent_edit_success"), 'success');
            }

        }

        $data  = $this->setting_harga_urgent_model->find_by(array('idsetting_urgent' => $id));

        if(!$data)
        {
            $this->template->set_message(lang("setting_harga_urgent_invalid_id"), 'error');
            redirect('setting_harga_urgent');
        }
        
        $this->template->set('data', $data);
        $this->template->set('toolbar_title', lang("setting_harga_urgent_title_edit"));
        $this->template->title(lang("setting_harga_urgent_title_edit"));
        $this->template->render('form');
    }

}
?>