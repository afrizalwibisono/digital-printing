<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ============================
$lang['setting_harga_urgent_title_manage']			= 'Data Setting Urgent';
$lang['setting_harga_urgent_title_new']				= 'Data Setting Urgent Baru';
$lang['setting_harga_urgent_title_edit']			= 'Edit Data Setting Urgent';

// from/table
$lang['setting_harga_urgent_durasi']		= 'Durasi';
$lang['setting_harga_urgent_persen']		= 'Kenaikan Harga';
$lang['setting_harga_urgent_durasi_awal'] 	= 'Dari';
$lang['setting_harga_urgent_durasi_akhir'] 	= 'Sampai';
$lang['setting_harga_urgent_jam']			= '*Jam';
$lang['setting_harga_urgent_%']				= '%';


//button
$lang['setting_harga_urgent_btn_new']				= 'Baru';
$lang['setting_harga_urgent_btn_delete']			= 'Hapus';
$lang['setting_harga_urgent_btn_save']				= 'Simpan';
$lang['setting_harga_urgent_btn_cancel']			= 'Batal';

//messages
$lang['setting_harga_urgent_del_error']			= 'Anda belum memilih data setting_harga_urgent yang akan dihapus.';
$lang['setting_harga_urgent_del_failure']			= 'Tidak dapat menghapus data setting_harga_urgent';
$lang['setting_harga_urgent_delete_confirm']		= 'Apakah anda yakin akan menghapus data setting_harga_urgent terpilih?';
$lang['setting_harga_urgent_deleted']				= 'Data setting harga urgent berhasil dihapus';
$lang['setting_harga_urgent_no_records_found']		= 'Data tidak ditemukan.';

$lang['setting_harga_urgent_create_failure']		= 'Data setting harga urgent gagal disimpan';
$lang['setting_harga_urgent_create_success']		= 'Data setting harga urgent berhasil disimpan';

$lang['setting_harga_urgent_edit_success']			= 'Data setting harga urgent berhasil disimpan';
$lang['setting_harga_urgent_invalid_id']			= 'ID tidak Valid';

