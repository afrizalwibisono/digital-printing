<div class="box box-primary">
    <!--form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_setting_harga_urgent','name'=>'frm_setting_harga_urgent','role','class'=>'form-horizontal'))?>
    <div class="box-body">
        <div class="form-group <?= form_error('durasi') ? ' has-error' : ''; ?>">
            <label for="durasi_awal" class="col-sm-2 control-label"><?= lang('setting_harga_urgent_durasi') ?></label>
            <div class="col-sm-2">
                <div class="input-group">
                    <input type="text" class="form-control" name="durasi_awal" id="durasi_awal" value="<?php echo set_value('durasi_awal', isset($data->durasi_awal) ? $data->durasi_awal : ''); ?>" placeholder="<?= lang('setting_harga_urgent_durasi_awal')?>" required /> 
                    <span class="input-group-addon text-black">s/d</span> 
                    <input type="text" class="form-control" name="durasi_akhir" id="durasi_akhir" value="<?php echo set_value('durasi_akhir', isset($data->durasi_akhir) ? $data->durasi_akhir : ''); ?>" placeholder="<?= lang('setting_harga_urgent_durasi_akhir')?>" required /> 
                </div>
            </div>
            <label for="durasi_awal" class="col-sm-2" style="padding-left: 0px;"><?= lang('setting_harga_urgent_jam') ?></label>
        </div>
        <div class="form-group <?= form_error('setting_harga_urgent_%') ? ' has-error' : ''; ?>">
            <label for="persen_harga" class="col-sm-2 control-label"><?= lang('setting_harga_urgent_persen') ?></label>
            <div class="col-sm-1">
                <input type="text" class="form-control" id="persen_harga" name="persen_harga" min="0.1" value="<?php echo set_value('persen_harga', isset($data->persen_harga) ? $data->persen_harga : ''); ?>" required autofocus>
            </div>
            <label for="durasi_awal" class="col-sm-2" style="padding-left: 0px;"><?= lang('setting_harga_urgent_%') ?></label>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" name="save" class="btn btn-primary"><?= lang('setting_harga_urgent_btn_save') ?></button>
                <?php
                echo lang('bf_or') . ' ' . anchor('setting_harga_urgent', lang('setting_harga_urgent_btn_cancel'));
                ?>
            </div>
        </div>
    </div>
    <?= form_close() ?>
</div>