<?php
	$ENABLE_ADD		= has_permission('Setting Urgent.Add');
	$ENABLE_MANAGE	= has_permission('Setting Urgent.Manage');
	$ENABLE_DELETE	= has_permission('Setting Urgent.Delete');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_setting_harga_urgent','name'=>'frm_setting_harga_urgent','ket'=>'frm_setting_harga_urgent')) ?>
	<div class="box-header">
		<div class="form-group">
			<?php if ($ENABLE_ADD): ?>
			<a href="<?= site_url('setting_harga_urgent/create') ?>" class="btn btn-success" title="<?= lang('setting_harga_urgent_btn_new') ?>"><?= lang('setting_harga_urgent_btn_new') ?></a>
			<?php endif;?>
			<div class="pull-right">
				<div class="input-group">
					<input type="text" name="table_search" value="<?php echo isset($search) ? $search:''; ?>" class="form-control pull-right" placeholder="Search" autofocus>
					<div class="input-group-btn">
						<button class="btn btn-default"><i class="fa fa-search"></i></button>
					</div>
				</div>	
			</div>
		</div>
	</div>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
	<div class="box-body table-responsive no-padding">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
                        <th width="50">#</th>
                        <th><?= lang('setting_harga_urgent_durasi') ?></th>
                        <th><?= lang('setting_harga_urgent_persen') ?></th>
                        <?php if($ENABLE_MANAGE) : ?>
                            <th width="25">Act</th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($results as $record) : ?>
                    <tr>
                        <td class="column-check">
                        	<input type="checkbox" name="checked[]" value="<?= $record->idsetting_urgent ?>" />

                        </td>
                        <td><?= $numb; ?></td>
                        <td><?= $record->durasi_awal ." S/D ". $record->durasi_akhir. " Jam" ?></td>
                        <td><?= $record->persen_harga ." %" ?></td>
                        <?php if($ENABLE_MANAGE) : ?>
                            <td style="padding-right:20px"><a class="text-black" href="<?= site_url('setting_harga_urgent/edit/' . $record->idsetting_urgent); ?>" data-toggle="tooltip" data-placement="left" title="Edit Data"><i class="fa fa-pencil"></i></a></td>
                        <?php endif; ?>
                    </tr>
                    <?php $numb++; endforeach; ?>
                </tbody>
	  </table>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">
		<?php if($ENABLE_DELETE) : ?>
		<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('setting_harga_urgent_btn_delete') ?>" onclick="return confirm('<?= lang('setting_harga_urgent_delete_confirm'); ?>')">
		<?php endif;
		echo $this->pagination->create_links(); 
		?>
	</div>
	<?php else: ?>
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('setting_harga_urgent_no_records_found') ?></p>
    </div>
    <?php
	endif;
	echo form_close(); 
	?>	
</div><!-- /.box-->