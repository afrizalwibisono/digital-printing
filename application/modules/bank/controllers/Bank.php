<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for Bank
 */

class Bank extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Bank.View";
    protected $addPermission    = "Bank.Add";
    protected $managePermission = "Bank.Manage";
    protected $deletePermission = "Bank.Delete";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('bank/bank');
        $this->load->model('bank/bank_model');

        $this->template->title(lang('bank_title_manage'));
		$this->template->page_icon('fa fa-bank');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if (isset($_POST['delete']) && has_permission($this->deletePermission))
        {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                foreach ($checked as $pid)
                {
                    $nama_bank   = $this->bank_model->find($pid)->nama_bank;
                    $result     = $this->bank_model->delete($pid);

                    if($result)
                    {
                        $keterangan = "SUKSES, hapus data banak ".$nama_bank.", dengan ID : ".$pid;
                        $status     = 1;
                    }
                    else
                    {
                        $keterangan = "GAGAL, hapus data bank ".$nama_bank.", dengan ID : ".$pid;
                        $status     = 0;
                    }

                    $nm_hak_akses   = $this->deletePermission; 
                    $kode_universal = $pid;
                    $jumlah         = 1;
                    $sql            = $this->db->last_query();

                    simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                }

                if ($result)
                {
                    $this->template->set_message(count($checked) .' '. lang('bank_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('bank_del_failure') . $this->bank_model->error, 'error');
                }
            }
            else
            {
                $this->template->set_message(lang('bank_del_error') . $this->bank_model->error, 'error');
            }
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
        }

        $filter = "";
        if($search!="")
        {
            $filter = "?search=".$search;
        }

        $search2 = $this->db->escape_str($search);
        
        $where = "`deleted` = 0
                AND (`nama_bank` LIKE '%$search2%' ESCAPE '!'
                OR `nomer_rek` LIKE '%$search2%' ESCAPE '!'
                OR `atas_nama` LIKE '%$search2%' ESCAPE '!')";
        
        $total = $this->bank_model
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        //$limit = $this->settings_lib->item('site.list_limit');
        $limit = 25;

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->bank_model
                    ->where($where)
                    ->order_by('nama_bank','ASC')
                    ->limit($limit, $offset)->find_all();

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set("toolbar_title", lang('bank_title_manage'));
        $this->template->set("page_title", lang('bank_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

   	//Create New Bank
   	public function create()
   	{

        $this->auth->restrict($this->addPermission);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_bank())
            {
              $this->template->set_message(lang("bank_create_success"), 'success');
              redirect('bank');
            }
        }
        $this->template->set("page_title", lang('bank_title_new'));
   	    $this->template->render('bank_form');
   	}

   	//Edit Supplier
   	public function edit()
   	{
   		
  		$this->auth->restrict($this->managePermission);
                
        $id = (int)$this->uri->segment(3);

        if (empty($id))
        {
            $this->template->set_message(lang("bank_invalid_id"), 'error');
            redirect('bank');
        }

        if (isset($_POST['save']))
        {
            if ($this->save_bank('update', $id))
            {
                $this->template->set_message(lang("bank_edit_success"), 'success');
            }

        }

        
        $data  = $this->bank_model->find_by(array('id_bank' => $id, 'deleted' => 0));

        if(!$data)
        {
            $this->template->set_message(lang("bank_invalid_id"), 'error');
            redirect('bank');
        }
        
        $this->template->set('data', $data);
        $this->template->set('toolbar_title', lang("bank_title_edit"));
        $this->template->set('page_title', lang("bank_title_edit"));
        $this->template->render('bank_form');
        
   	}

   	protected function save_bank($type='insert', $id=0)
   	{

        $this->form_validation->set_rules('nama_bank','lang:bank_name','required|trim|max_length[100]');
        $this->form_validation->set_rules('nomer_rek','lang:bank_nomer_rek','required|numeric|max_length[15]|trim');
        $this->form_validation->set_rules('atas_nama','lang:bank_atas_nama','required|max_length[100]|trim');
        $this->form_validation->set_rules('keterangan','lang:bank_keterangan','max_length[225]|trim');

        if ($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        unset($_POST['submit'], $_POST['save']);
        isset($_POST['st_default']) ? $_POST['st_default']=1:$_POST['st_default']=0;
        if ($_POST['st_default']==1) {
            $this->db->query("update bank set `bank`.`st_default`='0'");
        }
        if ($type == 'insert')
        {
            $id = $this->bank_model->insert($_POST);

            if (is_numeric($id))
            {
                //Save Log
                if($id)
                {
                    $keterangan = "SUKSES, tambah data bank ".$this->input->post('nama_bank').", dengan ID : ".$id;
                    $status     = 1;
                }
                else
                {
                    $keterangan = "GAGAL, tambah data bank ".$this->input->post('nama_bank').", dengan ID : ".$id;
                    $status     = 0;
                }

                $nm_hak_akses   = $this->addPermission; 
                $kode_universal = $id;
                $jumlah         = 1;
                $sql            = $this->db->last_query();
                //menyimpan ke Log histori
                simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

                $return = TRUE;
            }
            else
            {
                $return = FALSE;
            }
        }
        else if ($type == 'update')
        {
            $return = $this->bank_model->update($id, $_POST);

            if($return)
            {
                $keterangan = "SUKSES, ubah data bank ".$this->input->post('nama_bank').", dengan ID : ".$id;
                $status     = 1;
            }
            else
            {
                $keterangan = "GAGAL, ubah data bank ".$this->input->post('nama_bank').", dengan ID : ".$id;
                $status     = 0;
            }

            //Save Log
            $nm_hak_akses   = $this->managePermission; 
            $kode_universal = $id;
            $jumlah         = 1;
            $sql            = $this->db->last_query();
            //menyimpan ke log historis
            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
        }

        return $return;
   	}
}
?>