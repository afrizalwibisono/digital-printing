<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['bank_title_manage'] 	= 'Manage Bank';
$lang['bank_title_new'] 	= 'Bank Baru';
$lang['bank_title_edit'] 	= 'Edit Data Bank';

// form/table
$lang['bank_name'] 			= 'Nama Bank';
$lang['bank_nomer_rek'] 	= 'No Rekening';
$lang['bank_atas_nama']		= 'Atas Nama';
$lang['bank_keterangan']	= 'Keterangan';
$lang['bank_st_default']	= 'Default Bank';



// button
$lang['bank_btn_new'] 		= 'Baru';
$lang['bank_btn_delete'] 	= 'Hapus';
$lang['bank_btn_save'] 		= 'Simpan';
$lang['bank_btn_cancel'] 	= 'Batal';
$lang['bank_or']			= ' Atau ';

// messages
$lang['bank_del_error']			= 'Anda belum memilih Bank yang akan dihapus.';
$lang['bank_del_failure']		= 'Tidak dapat menghapus Bank: ';
$lang['bank_delete_confirm']	= 'Apakah anda yakin akan menghapus Bank terpilih ?';
$lang['bank_deleted']			= 'gudang berhasil dihapus';
$lang['bank_no_records_found'] 	= 'Data tidak ditemukan.';

$lang['bank_create_failure'] 	= 'Bank baru gagal disimpan: ';
$lang['bank_create_success'] 	= 'Bank baru berhasil disimpan';

$lang['bank_edit_success'] 		= 'Bank berhasil disimpan';
$lang['bank_invalid_id'] 		= 'ID Tidak Valid';