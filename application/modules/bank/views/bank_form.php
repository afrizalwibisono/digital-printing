<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_customer','name'=>'frm_customer','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="form-group <?= form_error('nama_bank') ? ' has-error' : ''; ?>">
			    <label for="nama_bank" class="col-sm-2 control-label"><?= lang('bank_name') ?></label>
			    <div class="col-sm-3">
			    	<input type="text" class="form-control" id="nama_bank" name="nama_bank" maxlength="100" value="<?php echo set_value('nama_bank', isset($data->nama_bank) ? $data->nama_bank : ''); ?>" required autofocus>
			    </div>
		  	</div>
		  	<div class="form-group <?= form_error('nomer_rek') ? ' has-error' : ''; ?>">
			    <label for="nomer_rek" class="col-sm-2 control-label"><?= lang('bank_nomer_rek') ?></label>
			    <div class="col-sm-3">
			    	<input type="text" class="form-control" id="nomer_rek" name="nomer_rek" maxlength="15" value="<?php echo set_value('nomer_rek', isset($data->nomer_rek) ? $data->nomer_rek : ''); ?>">
			    </div>
		  	</div>
		  	<div class="form-group <?= form_error('atas_nama') ? ' has-error' : ''; ?>">
			    <label for="atas_nama" class="col-sm-2 control-label"><?= lang('bank_atas_nama') ?></label>
			    <div class="col-sm-3">
			    	<input type="text" class="form-control" id="atas_nama" name="atas_nama" maxlength="50" value="<?php echo set_value('atas_nama', isset($data->atas_nama) ? $data->atas_nama : ''); ?>">
			    </div>
		  	</div>
		  	<div class="form-group <?= form_error('keterangan') ? ' has-error' : ''; ?>">
			    <label for="keterangan" class="col-sm-2 control-label"><?= lang('bank_keterangan') ?></label>
			    <div class="col-sm-3">
			    	<textarea class="form-control" id="keterangan" name="keterangan"><?php echo set_value('keterangan', isset($data->keterangan) ? $data->keterangan : ''); ?></textarea>
			    </div>
		  	</div>
		  	<div class="form-group">
                <div class="col-sm-offset-2 col-sm-3">
                    <div class="checkbox">
                      <label class="text-red">
                        <input type="checkbox" name="st_default" id="st_default" <?= isset($data->st_default) && $data->st_default==1 ? 'checked="checked"' : '' ?>> <?= lang('bank_st_default') ?>
                      </label>
                    </div>
                </div>
            </div>

		  	<div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      	<button type="submit" name="save" class="btn btn-primary"><?= lang('bank_btn_save') ?></button>
			    	<?php
	                	echo lang('bank_or') . ' ' . anchor('bank', lang('bank_btn_cancel'));
	                ?>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->