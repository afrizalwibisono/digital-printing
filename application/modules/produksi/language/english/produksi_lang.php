<?php defined('BASEPATH') || exit('No direct script access allowed');

// =========================================================================================
// Laporan pekerjaan produksi
$lang['title-lap-produksi']					= 'Laporan Pekerjaan Produksi';
$lang['title-jdl-lap-produksi']				= 'Laporan Pekerjaan Produksi';

$lang['lap-resume-jdl']						= 'Resume';
$lang['lap-detail-jdl']						= 'Detail';

$lang['lap-detail-waktu']					= 'waktu';
$lang['lap-detail-kategori']				= 'Kategori';
$lang['lap-detail-info']					= 'Info File';
$lang['lap-detail-produk']					= 'Produk';
$lang['lap-detail-jml-order']				= 'Jml Order';
$lang['lap-detail-jml-cetak']				= 'Jml Cetak';
$lang['lap-detail-user']					= 'Operator';

// ============================
$lang['produksi_title_manage'] 	= 'Manage produksi';
$lang['produksi_title_view'] 	= 'View Detail Bahan Baku Penyusun Produk';
$lang['produksi_title_edit'] 	= 'Edit Data produksi';

// form/table
$lang['produksi_no'] 			= 'NO';
$lang['produksi_id_spk']		= 'Kode SPK';
$lang['produksi_id_produksi']	= 'Kode produksi';
$lang['produksi_nama_konsumen']	= 'Nama Konsumen';
$lang['produksi_nama_file'] 	= 'Nama File';
$lang['produksi_tgl_order']	= 'Tanggal Order';
$lang['produksi_qty']		= 'Qty Order';
$lang['produksi_qty_cetak']		= 'Qty Cetak';
$lang['produksi_act']		= 'Action';
$lang['produksi_st_urgent']	= 'St Urgensi';
$lang['produksi_pilih_brg']	= 'Pilih Barang';
$lang['produksi_barcode']			= 'Barcode';
$lang['produksi_nm_barang']			= 'Nama Bahan Baku';
$lang['produksi_konversi_satuan'] 	= 'Konversi Satuan';
$lang['produksi_satuan']			= 'Satuan';
$lang['produksi_scan_brg']			= 'Ketik / Scan Barcode Barang';
$lang['produksi_nm_merk']			= 'Merek Barang';

$lang['produksi_nama_pekerjaan']	= 'Nama File';
$lang['produksi_id_kategori']		= 'Kategori Produk';
$lang['produksi_id_produk']			= 'Nama Produk';
$lang['produksi_gambar_cetak']		= 'Gambar';
$lang['produksi_ukuran_cetak']		= 'Ukuran Cetak';
$lang['produksi_jml_order']			= 'Jumlah Cetak';
$lang['produksi_note_produk']		= 'Catatan';
$lang['produksi_st_finishing']		= 'Status Finishing';

$lang['produksi_nm_finishing']			= 'Nama Finishing';
$lang['produksi_note_finishing']		= 'Catatan Finishing';
$lang['produksi_jml_pakai_finishing']	= 'JML Pakai';
$lang['produksi_ket']					= 'Keterangan';
$lang['produksi_next_target']			= 'Target Selanjutnya';

$lang['produksi_selesai']	= 'Selesai';
$lang['produksi_finishing']	= 'Finishing';	

$lang['produksi_input_bb']		= 'Input Bahan Baku Produksi';
$lang['produksi_spk_detail']	= 'Detail SPK';
$lang['produksi_detail_a3']		= 'Detail A3';

$lang['produksi_belum_input']	= 'On Progress';
$lang['produksi_sudah_input']	= 'Sudah Selesai Produksi';
$lang['produksi_search']		= 'ID SPK';

$lang['produksi_btn_save'] 		= 'Simpan';
$lang['produksi_pilih_btn']		= 'Pilih';
$lang['produksi_btn_cancel'] 	= 'Kembali';
$lang['produksi_btn_delete']	= 'Hapus';
$lang['produksi_btn_new']		= 'Baru';
$lang['produksi_or']			= ' Atau ';

// messages
$lang['produksi_del_error']			= 'Anda belum memilih data produksi yang akan dihapus.';
$lang['produksi_del_failure']		= 'Tidak dapat menghapus produksi: ';
$lang['produksi_delete_confirm']	= 'Apakah anda yakin akan menghapus produksi terpilih ?';
$lang['produksi_take_confirm']		= 'Apakah anda Yakin untuk mengambil produksi terpilih?';
$lang['produksi_cancel_confirm']	= 'Apakah anda yakin untuk membatalkan pengambilan SPK ?';
$lang['produksi_cancle_spk_success']= 'Cancel SPK  Berhasil';
$lang['produksi_cancle_spk_failure']= 'Cancel SPK  Gagal';
$lang['produksi_deleted']			= 'produksi berhasil dihapus';
$lang['produksi_no_records_found'] 	= 'Data tidak ditemukan.';
$lang['produksi_sudah_diambil'] 	= 'produksi dengan Nomer: %s sudah diambil user lain.';
$lang['produksi_no_item']			= 'Anda belum mengisi bahan baku produksi';

$lang['produksi_create_failure'] 	= 'penggunaan baha baku produksi gagal disimpan';
$lang['produksi_create_success'] 	= 'penggunaan baha baku produksi berhasil disimpan';

$lang['produksi_edit_success'] 		= 'produksi berhasil disimpan';
$lang['produksi_invalid_id'] 		= 'ID Tidak Valid';