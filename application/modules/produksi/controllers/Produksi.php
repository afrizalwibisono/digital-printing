<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for produksi
 */

class Produksi extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission       = "Produksi.View";
    protected $addPermission        = "Produksi.Add";
    protected $managePermission     = "Produksi.Manage";
    protected $manageAllPermission  = "Produksi All.Manage";
    protected $deletePermission     = "Produksi.Delete";

    protected $laporanPermission    = "Laporan Produksi List Order.View";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('produksi/produksi');
        $this->load->model(['produksi_model',
                            'order_produk/morder_produk_detail_model',
                            'produksi_detail_model',
                            'produk_detail_penyusun_model',
                            'spk_model',
                            'bahan_baku_model',
                            'tmp_produksi_model',
                            'tmp_produksi_detail_model',
                            'order_produk/morder_produk_detail_finishing_model',
                            'produk_detail_finishing_model',
                            'konversi_satuan/konversi_satuan_model',
                            'setting_upload/setting_upload_model',
                            'konsumen/konsumen_model',
                            'order_produk/morder_produk_detail_a3_model',
                            'bom/produk_model',
                            'worksheet/worksheet_model',
                            'karyawan_model',
                            'kategori_model']);

        $this->template->title(lang('produksi_title_manage'));
		$this->template->page_icon('fa fa-address-card-o');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);
        $idworksheet = $this->uri->segment(3);
        if (isset($_POST['save']))
        {
            if ($this->save_spk())
            {
              $this->template->set_message(lang("spk_create_success"), 'success');
              redirect('produksi/index/'.$idworksheet);
            }
        }
        if($idworksheet == 1){
            $a3 = true;
        }else{
            $a3= false;
        }
        if ($idworksheet){
            if (isset($_POST['delete']) && has_permission($this->deletePermission))
            {
                $checked = $this->input->post('checked');

                if (is_array($checked) && count($checked))
                {
                    $result = FALSE;
                    foreach ($checked as $pid)
                    {
                        //Hapus Barang Keluar
                        $dt_produksi = $this->produksi_model->find_by(['idspk'=>$pid, 'deleted'=>0]);
                        $idproduksi = $dt_produksi->idproduksi;
                        $query_hapus    = "";
                        if ($idproduksi) {
                            $del = $this->produksi_model->delete($idproduksi);
                            $query_hapus    .= $this->db->last_query();
                            if ($del) {
                                $update = $this->spk_model->update($pid, ['st_selesai'=>0,'waktu_selesai'=> NULL]);
                                $query_hapus    .= "\n\n" .$this->db->last_query();
                            }
                        }
                        if($del)
                            {
                                
                                $keterangan = "SUKSES, hapus data produksi : ".$idproduksi;
                                $status     = 1;
                            }
                            else
                            {
                                $keterangan = "GAGAL, hapus data produksi : ".$idproduksi;
                                $status     = 0;
                            } 

                        $nm_hak_akses   = $this->deletePermission; 
                        $kode_universal = $idproduksi;
                        $jumlah         = 1;
                        $sql            = $query_hapus;

                        $hasil = simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                    }

                    if ($del)
                    {
                        $this->template->set_message(count($checked) .' '. lang('produksi_deleted') .'.', 'success');
                    }
                    else
                    {
                        $this->template->set_message(lang('produksi_del_failure'), 'error');
                    }
                }
                else
                {
                    $this->template->set_message(lang('produksi_del_error'), 'error');
                }

                unset($_POST['delete']);
            }//end if
            // Pagination
            $this->load->library('pagination');

            if(isset($_POST['table_search']))
            {
                $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
                $st_selesai = $this->input->post('st_selesai');
                $idkonsumen = $this->input->post('id_konsumen');
                $tgl_awal  = $this->input->post('tgl_awal');
                $tgl_akhir = $this->input->post('tgl_akhir');
            }
            else
            {
                $search = isset($_GET['search'])?$this->input->get('search'):'';
                $st_selesai = $this->input->get('st_selesai');
                $idkonsumen = $this->input->get('id_konsumen');
                $tgl_awal  = $this->input->get('tgl_awal');
                $tgl_akhir = $this->input->get('tgl_akhir');
            }

            $filter = "?search=".urlencode($search);
            $search2 = $this->db->escape_str($search);

            $add_where = "";
            if($tgl_awal !='' && $tgl_akhir !='')
            {
                $filter    .= "&tgl_awal=".urlencode(date_ymd($tgl_awal))."&tgl_akhir=".urlencode(date_ymd($tgl_akhir));
                $add_where .= " AND (tgl_order >='".date_ymd($tgl_awal)."' AND tgl_order <='".date_ymd($tgl_akhir)."')";
            }
            if ($idkonsumen != '') {
                $filter    .= "&idkonsumen=".urlencode($idkonsumen);
                $add_where .= " AND (order_produk.id_konsumen = '$idkonsumen')";
            }
            if ($st_selesai == 1) {
                $filter .="&st_selesai=".urlencode($st_selesai);
                $add_where .="AND spk.st_selesai=1";
            }else{
                $filter .="&st_selesai=".urlencode($st_selesai);
                $add_where .="AND spk.st_selesai=0";
            }
            
            // dimantikan sementara karena permintaan user
            // $user= $this->auth->userdata();
            // $id_user = $user->id_user;

            // if (!$this->auth->is_admin()) {
            //     $add_where.=" AND spk.id_user = $id_user";
            // }
            
            $where = "`spk.deleted` = 0 $add_where AND spk.st_ambil= 1 AND spk.id_worksheet= $idworksheet 
                    AND (`spk.idspk` LIKE '%$search2%' ESCAPE '!'
                    OR `nama_pekerjaan` LIKE '%$search2%' ESCAPE '!')";
            
            $total = $this->spk_model->select("`spk`.`st_selesai`,`spk`.`kode_universal`, `konsumen`.`nama`, `m_order_produk_detail`.`nama_pekerjaan`,`m_order_produk_detail`.`target_server`, `m_order_produk_detail`.`id_produk`, `produk`.`nm_produk`, IF(`m_order_produk_detail`.`st_finishing` = 0, 'Finishing Standar', 'Finishing Custom') as status_finishing, `m_order_produk_detail`.`st_urgent`, `thumbnail`, `spk.idspk`,`spk.jml_cetak`, `konversi_satuan`.`satuan_besar`, `tgl_order`,`m_order_produk_detail`.`tgl_permintaan_selesai`,`users`.`nm_lengkap` ")
                    ->join("order_produk","spk.id_order=order_produk.id_order","left")
                    ->join("konsumen","konsumen.idkonsumen=order_produk.id_konsumen","left")
                    ->join("m_order_produk_detail","spk.id_detail_produk_order=m_order_produk_detail.id_detail_produk_order","left")
                    ->join("users","users.id_user=m_order_produk_detail.created_by","left")
                    ->join("produk","m_order_produk_detail.id_produk = produk.idproduk","left")
                    ->join("konversi_satuan","m_order_produk_detail.id_konversi_jml_cetak=konversi_satuan.id_konversi","left")
                    ->where($where)
                    ->count_all();

            $offset = $this->input->get('per_page');

            $limit = $this->config->item('list_limit');

            $this->pager['base_url']            = current_url().$filter;
            $this->pager['total_rows']          = $total;
            $this->pager['per_page']            = $limit;
            $this->pager['page_query_string']   = TRUE;

            $this->pagination->initialize($this->pager);

            $data = $this->spk_model->select("`spk`.`st_selesai`,`spk`.`kode_universal`,`konsumen`.`nama`, `m_order_produk_detail`.`id_detail_produk_order`, `m_order_produk_detail`.`nama_pekerjaan`,`m_order_produk_detail`.`target_server`, `m_order_produk_detail`.`id_produk`, `produk`.`nm_produk`, IF(`m_order_produk_detail`.`st_finishing` = 0, 'Finishing Standar', 'Finishing Custom') as status_finishing, `m_order_produk_detail`.`st_urgent`, `thumbnail`, `spk.idspk`,`spk.jml_cetak`, `konversi_satuan`.`satuan_besar`, `tgl_order`,`m_order_produk_detail`.`tgl_permintaan_selesai`,`users`.`nm_lengkap`,`m_order_produk_detail`.`catatan`, `produksi`.`qty_tercetak`, `m_order`.`nm_lengkap`")
                    ->join("order_produk","spk.id_order=order_produk.id_order","left")
                    ->join("konsumen","konsumen.idkonsumen=order_produk.id_konsumen","left")
                    ->join("m_order_produk_detail","spk.id_detail_produk_order=m_order_produk_detail.id_detail_produk_order","left")
                    ->join("users","users.id_user=m_order_produk_detail.created_by","left")
                    ->join("produk","m_order_produk_detail.id_produk = produk.idproduk","left")
                    ->join("(select 
                                sum(jml_cetak) as qty_tercetak, kode_universal, idspk
                            from 
                                produksi
                            where
                                deleted=0 
                            group by kode_universal)as produksi","produksi.idspk = spk.idspk","left")
                    ->join("konversi_satuan","m_order_produk_detail.id_konversi_jml_cetak=konversi_satuan.id_konversi","left")
                    ->join("(select 
                            m_order_produk_detail.id_detail_produk_order, created_by, nm_lengkap 
                        from 
                            m_order_produk_detail 
                        left join users on users.id_user=m_order_produk_detail.created_by
                        where 
                            m_order_produk_detail.id_detail_produk_order = m_order_produk_detail.kode_universal) as m_order","m_order.id_detail_produk_order = m_order_produk_detail.kode_universal", "inner")
                    ->where($where)
                    ->order_by('tgl_permintaan_selesai','ASC')
                    ->limit($limit, $offset)
                    ->find_all();
            $worksheet = $this->worksheet_model->select("nm_worksheet")->find($idworksheet);

            // if(is_array($data)){
            //     foreach ($data as $key => $record) {
            //         $query = $this->morder_produk_detail_a3_model
            //         ->order_by('page', 'ASC')
            //         ->find_all_by(["id_detail_produk_order"=> $record->id_detail_produk_order]);
            //         if (is_array($query) && count($query) > 0) {
            //             $data[$key]->detail_a3 = $query;
            //         }else{
            //             $data[$key]->detail_a3 = null;
            //         }
            //     }        
            // }

            $assets = array(
                            'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                            'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                            "produksi/assets/js/index_produksi.js",
                            'plugins/number/jquery.number.js',
                            'plugins/select2/dist/js/select2.js', 
                            'plugins/select2/dist/css/select2.css',
                            );

            add_assets($assets);

            $setting_upload = $this->setting_upload_model->find(1);
            $data_konsumen = $this->konsumen_model->find_all_by(['deleted'=>0]);

            $this->template->set('results', $data);
            $this->template->set('idworksheet', $idworksheet);
            $this->template->set('konsumen', $data_konsumen);
            $this->template->set('setting_upload', $setting_upload);
            $this->template->set('idkonsumen', $idkonsumen);
            $this->template->set('search', $search);
            $this->template->set('a3', $a3);
            $this->template->title(lang('produksi_title_manage')." ".$worksheet->nm_worksheet);
            $this->template->set("numb", $offset+1);
            $this->template->render('index');
        }else{
            show_404();
        }
    }
    public function cancel_spk()
    {
        
        $this->auth->restrict($this->managePermission);
                
        $id = $this->input->post('idspk');
        // $idworksheet = $this->uri->segment(4);

        if (empty($id))
        {
            $this->template->set_message(lang("produksi_invalid_id"), 'error');
        }

        $data_produksi = $this->produksi_model->select("sum(jml_cetak) as jml_tercetak")->find_by(["idspk"=> $id, "deleted"=>0]);
         $query ="";
         $spk = $this->spk_model->find_by(['idspk'=>$id]);
        if ($data_produksi && $data_produksi->jml_tercetak>0) {
            $selisih = $spk->jml_cetak - $data_produksi->jml_tercetak;
            simpan_spk($spk->st_ref, $spk->kode_universal, $spk->id_konversi_jml_cetak, 1, $selisih);
                $query .= "\n\n" .$this->db->last_query();
            $update = $this->spk_model->update($id,["st_selesai"=>1]);
            // simpan log proses
            set_proses_order($id_lokasi = 2, $id_ket_proses = 5, $id_detail_produk_order = $spk->id_detail_produk_order, $jumlah_proses = $selisih);
        }else{
            $update = $this->spk_model->update($id,["st_ambil"=>0,"waktu_ambil"=>NULL, "id_user"=>0]);
            $query .= "\n\n" .$this->db->last_query();
            // simpan log proses
            set_proses_order($id_lokasi = 2, $id_ket_proses = 5, $id_detail_produk_order = $spk->id_detail_produk_order, $jumlah_proses = $spk->jml_cetak);
        }
        if ($update) 
        {
            $message = ['type' => 'success', 'message' => 'Cancel produksi sukses'];
            $keterangan = "SUKSES, Update data SPK dg id : ".$id;
            $status     = 1;
        }
        else
        {
            $message = ['type' => 'success', 'message' => 'Gagal Cancel produksi'];
            $keterangan = "GAGAL, Update data SPK dg id : ".$id;
            $status     = 0;
        }
        $nm_hak_akses   = $this->managePermission; 
        $kode_universal = $id;
        $jumlah         = 1;
        $sql            = $query;
        
        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
        
        echo json_encode($message);
        
    }
    public function cari_barang()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            redirect('produksi');
        }

        $cari = trim($this->db->escape_str($this->input->post('cr_barang')));

        $data = $this->bahan_baku_model->select(array("barang.*","satuan_terkecil.alias as satuan","nm_merk_bb"))
                                    ->join("satuan_terkecil","barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","left")
                                    ->join("merk_bb","barang.idmerk_bb=merk_bb.idmerk_bb","left")
                                    ->where("`barang.deleted` = 0
                                            AND (barcode like '%$cari%' OR nm_barang like '%$cari%')")
                                    ->order_by('nm_barang', 'ASC')
                                    ->find_all();
        if($data)
        {
            foreach ($data as $key => $dt) {
                if ($dt->st_potong_meteran ==1) {
                    $data[$key]->konversi = get_konversi(4,'',FALSE);
                }else{
                    $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
                }
            }
        }

        echo json_encode($data);
    }
    public function save_produksi()
    {
        if (has_permission($this->addPermission))
        {   
            $idspk          = $this->input->post('idspk');
            $kode_universal = $this->input->post('kode_universal');
            $qty_cetak    = str_replace(",", "", $this->input->post('qty_cetak'));

            $this->form_validation->set_rules('idspk','lang:produksi_idspk','required');
            $this->form_validation->set_rules('kode_universal','kode universal', 'required');    
            $this->form_validation->set_rules('qty_cetak','lang:qty_cetak','required');

            if ($this->form_validation->run() === FALSE) 
            {
                echo json_encode(['type'=> 'error', 'message'=> validation_errors()]);
                return FALSE;
            }
            
            $spk = $this->spk_model->select('sum(jml_cetak) as req_cetak, id_detail_produk_order')->find_all_by(['idspk'=> $idspk, 'deleted'=>0]);

            $data_produksi = $this->produksi_model->select('sum(jml_cetak) as jml_tercetak')->find_all_by(['idspk'=> $idspk, 'deleted'=>0]);


            $this->db->trans_start();
                // upddate status produksi
                $this->spk_model->update($idspk, ['st_produksi'=>1]);
                // update st_selesai spk
                if ($spk[0]->req_cetak == ($data_produksi[0]->jml_tercetak +$qty_cetak)) {
                    $this->spk_model->update($idspk, ['st_selesai'=>1]);
                }

                $data = [
                            'idproduksi' => gen_primary("PROD", "produksi", "idproduksi"),
                            'kode_universal' => $kode_universal,
                            'idspk' => $idspk,
                            'jml_cetak' => $qty_cetak,
                        ];
                $this->produksi_model->insert($data);
                $query = $this->db->last_query();
                // simpan log proses
                set_proses_order($id_lokasi = 2, $id_ket_proses = 4, $id_detail_produk_order = $spk[0]->id_detail_produk_order, $jumlah_proses = $qty_cetak);

                // hitung penggunaan bahan baku 
                $detail_order = $this->morder_produk_detail_model->find_by(['kode_universal'=>$kode_universal,'deleted'=>0]);
                $produk = $this->produk_model->find($detail_order->id_produk);
                $material = $this->produk_detail_penyusun_model
                                    ->join("barang","produk_detail_penyusun.idbarang_bb=barang.idbarang_bb","left")
                                    ->find_all_by(['idproduk'=>$detail_order->id_produk, 'produk_detail_penyusun.deleted'=>0]);
                // cek bahan baku cetak
                $bahan_baku_cetak = $this->produk_detail_penyusun_model
                                    ->join("barang","produk_detail_penyusun.idbarang_bb=barang.idbarang_bb","left")
                                    ->find_by(['idproduk'=>$detail_order->id_produk, 'produk_detail_penyusun.deleted'=>0, 'barang.st_cetak_a3' => 1]);
                $finishing= $this->produk_detail_finishing_model->find_all_by(['idproduk'=>$detail_order->id_produk,'deleted'=>0]);
                $finishing_tambahan = $this->morder_produk_detail_finishing_model->find_all_by(['id_detail_produk_order'=>$detail_order->id_detail_produk_order,'deleted'=>0]);

                // jml yang dicetak
                $real_qty_order  = $qty_cetak;
                // real lebar order
                $data_l         = $this->konversi_satuan_model->hitung_ke_satuan_kecil($detail_order->id_satuan, floatval($detail_order->l));
                $real_l         = $data_l['qty'];
                // real panjang order
                $data_p         = $this->konversi_satuan_model->hitung_ke_satuan_kecil($detail_order->id_satuan, floatval($detail_order->p));
                $real_p         = $data_p['qty'];
                $luas           = $real_p * $real_l;

                // bahan baku penyusun
                if ($material) {
                    $bahan_baku = array();
                    $bahan_baku_finishing = array();
                    foreach ($material as $key => $record) {
                        $tipe  = $record->st_tipe;
                        if ($tipe==0) {
                            $kali=1;
                            if (is_array($bahan_baku_cetak) && count($bahan_baku_cetak) > 0) {
                                $st_tipe = $bahan_baku_cetak->st_tipe;
                                if ($st_tipe == 2) {
                                    $kali = $real_qty_order;
                                }else{
                                    $data_qty = $this->konversi_satuan_model->hitung_ke_satuan_kecil($bahan_baku_cetak->id_konversi_jml, floatval($bahan_baku_cetak->jml_pakai));
                                    $real_qty = $data_qty['qty'];
                                    $kali = $real_qty_order * $real_qty;
                                }
                            }

                            $data_qty_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_jml, floatval($record->jml_pakai));
                            $real_qty_bahan = $data_qty_bahan['qty'];

                            if ($record->st_target_cetakan == 1) {
                                $qty = $real_qty_order * $real_qty_bahan * $kali;
                            }else{
                                $qty = $real_qty_order * $real_qty_bahan;
                            }
                            
                            $konversi = get_konversi($data_qty_bahan['id_satuan_terkecil'],'',FALSE);
                        }elseif($tipe==1){
                            $data_panjang_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->panjang));
                            $real_panjang       = $data_panjang_bahan['qty'];
                            $data_lebar_bahan   = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->lebar));
                            $real_lebar       = $data_lebar_bahan['qty'];
                            $luas_bahan       = $real_panjang * $real_lebar;

                            $qty        = $real_qty_order * $luas_bahan;
                            $konversi   = get_konversi($data_lebar_bahan['id_satuan_terkecil'],'',FALSE);
                        }else{
                            if ($produk->st_tipe == 0) {
                                $kali=1;
                                if (is_array($bahan_baku_cetak) && count($bahan_baku_cetak) > 0) {
                                    $st_tipe = $bahan_baku_cetak->st_tipe;
                                    if ($st_tipe == 2) {
                                        $kali = $real_qty_order;
                                    }else{
                                        $data_qty = $this->konversi_satuan_model->hitung_ke_satuan_kecil($bahan_baku_cetak->id_konversi_jml, floatval($bahan_baku_cetak->jml_pakai));
                                        $real_qty = $data_qty['qty'];
                                        $kali = $real_qty_order * $real_qty;
                                    }
                                }
                                if ($record->st_target_cetakan == 1) {
                                    $qty        = $real_qty_order * $kali;
                                }else{
                                    $qty        = $real_qty_order;
                                }
                            }else{
                                $qty        = $real_qty_order * $luas;
                            }
                            // if ($produk) {
                            //     // 0 = fix 
                            //     // 1 = potongan
                            //     $tipe = $produk->st_tipe;   
                            //     if ($tipe==0) {
                            //         $qty        = $real_qty_order;
                            //         $konversi   = get_konversi(1,'',FALSE);
                            //     }else{
                            //         $qty        = $real_qty_order * $luas;
                            //         $konversi   = get_konversi(4,'',FALSE);
                            //     }
                            // }
                            
                        }
                        $bahan_baku[] = array('iddetail_produksi'=>gen_primary("DPROD","produksi_detail","iddetail_produksi"),
                                            'idproduksi'=> $data['idproduksi'],
                                            'idbarang_bb' => $record->idbarang_bb,
                                            'qty' => $qty,
                                            'id_konversi'=> $record->id_satuan_terkecil,
                                            'tipe'=>0);   
                    }
                }

                // bahan baku penyusun Finishing
                if (is_array($finishing)) {
                    foreach ($finishing as $key => $dt) {
                        $penyusun_finishing = $this->produk_detail_penyusun_model
                                    ->join("barang","produk_detail_penyusun.idbarang_bb=barang.idbarang_bb","left")
                                    ->find_all_by(['idproduk'=>$dt->id_produk]);
                        if ($penyusun_finishing) {
                            foreach ($penyusun_finishing as $key => $record) {
                                $tipe = $record->st_tipe;
                                if ($tipe==0){
                                    $data_qty_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_jml, floatval($record->jml_pakai));
                                    $real_qty_bahan = $data_qty_bahan['qty'];
                                    $qty = $real_qty_order * $real_qty_bahan;
                                    $konversi = get_konversi($data_qty_bahan['id_satuan_terkecil'],'',FALSE);
                                }elseif($tipe==1){
                                    $data_panjang_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->panjang));
                                    $real_panjang       = $data_panjang_bahan['qty'];
                                    $data_lebar_bahan   = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->lebar));
                                    $real_lebar       = $data_lebar_bahan['qty'];
                                    $luas_bahan       = $real_panjang * $real_lebar;

                                    $qty        = $real_qty_order * $luas_bahan;
                                    $konversi   = get_konversi($data_lebar_bahan['id_satuan_terkecil'],'',FALSE);
                                }else{
                                    if ($produk->st_tipe == 0) {
                                        // 0 = fix 
                                        // 1 = potongan
                                        $qty        = $real_qty_order;
                                        $konversi   = get_konversi(1,'',FALSE);
                                    }else{
                                        $qty        = $real_qty_order * $luas;
                                        $konversi   = get_konversi(4,'',FALSE);
                                    }
                                    
                                }
                                $bahan_baku_finishing[] = array('iddetail_produksi'=>gen_primary("DPROD","produksi_detail","iddetail_produksi"),
                                                            'idproduksi'=> $data['idproduksi'],
                                                            'idbarang_bb' => $record->idbarang_bb,
                                                            'qty' => $qty,
                                                            'id_konversi'=> $record->id_satuan_terkecil,
                                                            'tipe'=> 1);
                            }
                        }   
                    }
                }
                // bahan baku penyusun Finishing tambahan
                if (is_array($finishing_tambahan)) {
                    foreach ($finishing_tambahan as $key => $dt) {
                        $penyusun_finishing_tambahan = $this->produk_detail_penyusun_model
                                    ->join("barang","produk_detail_penyusun.idbarang_bb=barang.idbarang_bb","left")
                                    ->find_all_by(['idproduk'=> $dt->id_produk ]);
                        if ($penyusun_finishing_tambahan) {
                            foreach ($penyusun_finishing_tambahan as $key => $record) {
                                $tipe = $record->st_tipe;
                                if ($tipe==0){
                                    $data_qty_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_jml, floatval($record->jml_pakai));
                                    $real_qty_bahan = $data_qty_bahan['qty'];
                                    $qty = $real_qty_order * $real_qty_bahan;
                                    $konversi = get_konversi($data_qty_bahan['id_satuan_terkecil'],'',FALSE);
                                }elseif($tipe==1){
                                    $data_panjang_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->panjang));
                                    $real_panjang       = $data_panjang_bahan['qty'];
                                    $data_lebar_bahan   = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->lebar));
                                    $real_lebar       = $data_lebar_bahan['qty'];
                                    $luas_bahan       = $real_panjang * $real_lebar;

                                    $qty        = $real_qty_order * $luas_bahan;
                                    $konversi   = get_konversi($data_lebar_bahan['id_satuan_terkecil'],'',FALSE);
                                }else{
                                    if ($produk->st_tipe == 0) {
                                        // 0 = fix 
                                        // 1 = potongan
                                        $qty        = $real_qty_order;
                                    }else{
                                        $qty        = $real_qty_order * $luas;
                                    }
                                    
                                }
                                $bahan_baku_finishing[] = array(
                                                            'iddetail_produksi'=>gen_primary("DPROD","produksi_detail","iddetail_produksi"),
                                                            'idproduksi'=> $data['idproduksi'],
                                                            'idbarang_bb' => $record->idbarang_bb,
                                                            'qty' => $qty,
                                                            'id_konversi'=> $record->id_satuan_terkecil,
                                                            'tipe'=> 1);   
                            }
                        }   
                    }
                }
                $bb = array_merge($bahan_baku,$bahan_baku_finishing);
                // simpan data detail tmp produksi
                $add_tmp_produksi_detail = $this->produksi_detail_model->insert_batch($bb);
                $query .= "/n/n". $this->db->last_query();



            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE)
            {

                $keterangan = "GAGAL, tambah data produksi dengan ID :".$data['idproduksi'];

                $message = ['type' => 'error', 'message' => 'Data produksi gagal disimpan'];
                $status     = 0;
                
                $return = FALSE;
            }
            else
            {

                $keterangan = "SUKSES, tambah data produksi dengan ID : ".$data['idproduksi'];
                $message = ['type' => 'success', 'message' => 'Data produksi berhasil disimpan'];
                $status     = 1;
                
                $return = TRUE;
            }

            //Save Log
            $nm_hak_akses   = $this->addPermission; 
            $kode_universal = $data['idproduksi'];
            $jumlah         = 1;
            $sql            = $query;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
            
            echo json_encode($message);

        }
    }

   	public function view_detail(){
        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('order_produk_only_ajax'), 'error');
            redirect('order_produk');
        }
        $id = $this->input->post('kode_universal');
        if (isset($id)) {
            $sql="select t.*, t1.satuan_besar as satuan_panjang, t2.satuan_besar as satuan_order, nm_produk, nmkategori
                    from m_order_produk_detail as t
                    left join produk on t.id_produk = produk.idproduk
                    left join konversi_satuan t1 on t1.id_konversi= t.id_satuan
                    left join konversi_satuan t2 on t2.id_konversi= t.id_konversi_jml_cetak
                    left join kategori on kategori.idkategori= t.id_kategori
                    where t.kode_universal = '$id'";
            $data_detail = $this->db->query($sql)->row();
            $detail_a3 = $this->morder_produk_detail_a3_model
                    ->order_by('page', 'ASC')
                    ->find_all_by(["id_detail_produk_order"=> $data_detail->id_detail_produk_order]);
            if ($data_detail->st_finishing ==1){
                $data_det_finishing= $this->morder_produk_detail_finishing_model
                                        ->join('konversi_satuan','m_order_produk_detail_finishing.id_konversi=konversi_satuan.id_konversi','left')
                                        ->join('produk','produk.idproduk=m_order_produk_detail_finishing.id_produk','left')
                                        ->find_all_by(['id_detail_produk_order'=>$data_detail->id_detail_produk_order]);
                if ($data_det_finishing) {
                    $return = ['type'=> 'success', 'data' => $data_detail,'detail'=>$data_det_finishing, 'detail_a3'=>$detail_a3];    
                }else{
                    $return = ['type'=> 'success', 'data' => $data_detail,'detail'=>'','detail_a3'=>$detail_a3];
                }
            }else{
                $return = ['type'=> 'success', 'data' => $data_detail, 'detail'=>'', 'detail_a3'=>$detail_a3];
            }
        }else{
            $return = ['type' => 'error', 'msg' => 'Data Tidak Ditemukan.'];
        }
        echo json_encode($return);
    }

    public function selesai_produksi()
    {
        if (has_permission($this->addPermission))
        {   
            $idspk          = $this->input->post('idspk');
            $kode_universal = $this->input->post('kode_universal');
            $qty_cetak    = str_replace(",", "", $this->input->post('qty_cetak'));

            $this->form_validation->set_rules('idspk','lang:produksi_idspk','required');
            $this->form_validation->set_rules('kode_universal','kode universal', 'required');    
            $this->form_validation->set_rules('qty_cetak','lang:qty_cetak','required');

            if ($this->form_validation->run() === FALSE) 
            {
                echo json_encode(['type'=> 'error', 'message'=> validation_errors()]);
                return FALSE;
            }

            
            // $spk = $this->spk_model->select('sum(jml_cetak) as req_cetak')->find_all_by(['idspk'=> $idspk, 'deleted'=>0]);
            $spk = $this->spk_model->find($idspk);
            $data_produksi = $this->produksi_model->select('sum(jml_cetak) as jml_tercetak')->find_all_by(['idspk'=> $idspk, 'deleted'=>0]);

            $this->db->trans_start();
                $query="";
                $input_selisih = false;
                // upddate status produksi jadi selesai
                $update = $this->spk_model->update($idspk, ['st_produksi'=>1, 'st_selesai'=> 1]);
                $query .= "/n/n". $this->db->last_query();

                if ($update) {
                    $selisih = 0;
                    $data = [
                            'idproduksi' => gen_primary("PROD", "produksi", "idproduksi"),
                            'kode_universal' => $kode_universal,
                            'idspk' => $idspk,
                            'jml_cetak' => $qty_cetak,
                        ];
                    // if ($data_produksi[0]->jml_tercetak > 0) {
                    if ( ($data_produksi[0]->jml_tercetak +  $qty_cetak) < $spk->jml_cetak) {
                        $selisih = $spk->jml_cetak -  ($data_produksi[0]->jml_tercetak +  $qty_cetak);
                        if ($selisih> 0) {
                            $input_selisih = true;
                            $data_selisih = [
                                'idproduksi' => gen_primary("PROD", "produksi", "idproduksi"),
                                'kode_universal' => $kode_universal,
                                'idspk' => $idspk,
                                'jml_cetak' => $selisih,
                                'st_dianggap_selesai' => 1,
                            ];                  
                            $this->produksi_model->insert($data_selisih);
                            $query = $this->db->last_query();
                        }
                    }
                    // }
                    $this->produksi_model->insert($data);
                    $query = $this->db->last_query();

                    // simpan log proses
                    set_proses_order($id_lokasi = 2, $id_ket_proses = 4, $id_detail_produk_order = $spk->id_detail_produk_order, $jumlah_proses = $selisih);

                    // hitung penggunaan bahan baku 
                    $detail_order = $this->morder_produk_detail_model->find_by(['kode_universal'=>$kode_universal,'deleted'=>0]);
                    $produk = $this->produk_model->find($detail_order->id_produk);
                    $material = $this->produk_detail_penyusun_model
                                        ->join("barang","produk_detail_penyusun.idbarang_bb=barang.idbarang_bb","left")
                                        ->find_all_by(['idproduk'=>$detail_order->id_produk, 'produk_detail_penyusun.deleted'=>0]);
                    $finishing= $this->produk_detail_finishing_model->find_all_by(['idproduk'=>$detail_order->id_produk,'deleted'=>0]);
                    $finishing_tambahan = $this->morder_produk_detail_finishing_model->find_all_by(['id_detail_produk_order'=>$detail_order->id_detail_produk_order,'deleted'=>0]);

                    // jml yang dicetak
                    $real_qty_order  = $qty_cetak;
                    // real lebar order
                    $data_l         = $this->konversi_satuan_model->hitung_ke_satuan_kecil($detail_order->id_satuan, floatval($detail_order->l));
                    $real_l         = $data_l['qty'];
                    // real panjang order
                    $data_p         = $this->konversi_satuan_model->hitung_ke_satuan_kecil($detail_order->id_satuan, floatval($detail_order->p));
                    $real_p         = $data_p['qty'];
                    $luas           = $real_p * $real_l;

                    // bahan baku penyusun
                    if ($material) {
                        $bahan_baku = array();
                        $bahan_baku_finishing = array();
                        foreach ($material as $key => $record) {
                            $tipe = $record->st_tipe;
                            if ($tipe==0) {
                                $data_qty_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_jml, floatval($record->jml_pakai));
                                $real_qty_bahan = $data_qty_bahan['qty'];
                                $qty = $real_qty_order * $real_qty_bahan;
                                $konversi = get_konversi($data_qty_bahan['id_satuan_terkecil'],'',FALSE);
                            }elseif($tipe==1){
                                $data_panjang_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->panjang));
                                $real_panjang       = $data_panjang_bahan['qty'];
                                $data_lebar_bahan   = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->lebar));
                                $real_lebar       = $data_lebar_bahan['qty'];
                                $luas_bahan       = $real_panjang * $real_lebar;

                                $qty        = $real_qty_order * $luas_bahan;
                                $konversi   = get_konversi($data_lebar_bahan['id_satuan_terkecil'],'',FALSE);
                            }else{
                                if ($produk) {
                                    // 0 = fix 
                                    // 1 = potongan
                                    $tipe = $produk->st_tipe;   
                                    if ($tipe==0) {
                                        $qty        = $real_qty_order;
                                        $konversi   = get_konversi(1,'',FALSE);
                                    }else{
                                        $qty        = $real_qty_order * $luas;
                                        $konversi   = get_konversi(4,'',FALSE);
                                    }
                                }
                                
                            }
                            $bahan_baku[] = array('iddetail_produksi'=>gen_primary("DPROD","produksi_detail","iddetail_produksi"),
                                                'idproduksi'=> $data['idproduksi'],
                                                'idbarang_bb' => $record->idbarang_bb,
                                                'qty' => $qty,
                                                'id_konversi'=> $record->id_satuan_terkecil,
                                                'tipe'=>0);   
                        }

                        // input produksi jika ada selisih ketika dianggap selesai
                        if ($input_selisih == true) {
                            foreach ($material as $key => $record) {
                                $tipe = $record->st_tipe;
                                if ($tipe==0) {
                                    $data_qty_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_jml, floatval($record->jml_pakai));
                                    $real_qty_bahan = $data_qty_bahan['qty'];
                                    $qty = $data_selisih['jml_cetak'] * $real_qty_bahan;
                                    $konversi = get_konversi($data_qty_bahan['id_satuan_terkecil'],'',FALSE);
                                }elseif($tipe==1){
                                    $data_panjang_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->panjang));
                                    $real_panjang       = $data_panjang_bahan['qty'];
                                    $data_lebar_bahan   = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->lebar));
                                    $real_lebar       = $data_lebar_bahan['qty'];
                                    $luas_bahan       = $real_panjang * $real_lebar;

                                    $qty        = $data_selisih['jml_cetak'] * $luas_bahan;
                                    $konversi   = get_konversi($data_lebar_bahan['id_satuan_terkecil'],'',FALSE);
                                }else{
                                    if ($produk) {
                                        // 0 = fix 
                                        // 1 = potongan
                                        $tipe = $produk->st_tipe;   
                                        if ($tipe==0) {
                                            $qty        = $data_selisih['jml_cetak'];
                                            $konversi   = get_konversi(1,'',FALSE);
                                        }else{
                                            $qty        = $data_selisih['jml_cetak'] * $luas;
                                            $konversi   = get_konversi(4,'',FALSE);
                                        }
                                    }
                                    
                                }
                                $bahan_baku[] = array('iddetail_produksi'=>gen_primary("DPROD","produksi_detail","iddetail_produksi"),
                                                    'idproduksi'=> $data['idproduksi'],
                                                    'idbarang_bb' => $record->idbarang_bb,
                                                    'qty' => $qty,
                                                    'id_konversi'=> $record->id_satuan_terkecil,
                                                    'tipe'=>0);   
                            }
                        }
                    }

                    // bahan baku penyusun Finishing
                    if (is_array($finishing)) {
                        foreach ($finishing as $key => $dt) {
                            $penyusun_finishing = $this->produk_detail_penyusun_model
                                        ->join("barang","produk_detail_penyusun.idbarang_bb=barang.idbarang_bb","left")
                                        ->find_all_by(['idproduk'=>$dt->id_produk]);
                            if ($penyusun_finishing) {
                                foreach ($penyusun_finishing as $key => $record) {
                                    $tipe = $record->st_tipe;
                                    if ($tipe==0){
                                        $data_qty_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_jml, floatval($record->jml_pakai));
                                        $real_qty_bahan = $data_qty_bahan['qty'];
                                        $qty = $real_qty_order * $real_qty_bahan;
                                        $konversi = get_konversi($data_qty_bahan['id_satuan_terkecil'],'',FALSE);
                                    }elseif($tipe==1){
                                        $data_panjang_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->panjang));
                                        $real_panjang       = $data_panjang_bahan['qty'];
                                        $data_lebar_bahan   = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->lebar));
                                        $real_lebar       = $data_lebar_bahan['qty'];
                                        $luas_bahan       = $real_panjang * $real_lebar;

                                        $qty        = $real_qty_order * $luas_bahan;
                                        $konversi   = get_konversi($data_lebar_bahan['id_satuan_terkecil'],'',FALSE);
                                    }else{
                                        if ($produk) {
                                            // 0 = fix 
                                            // 1 = potongan
                                            $tipe = $produk->st_tipe;   
                                            if ($tipe==0) {
                                                $qty        = $real_qty_order;
                                                $konversi   = get_konversi(1,'',FALSE);
                                            }else{
                                                $qty        = $real_qty_order * $luas;
                                                $konversi   = get_konversi(4,'',FALSE);
                                            }
                                        }
                                        
                                    }
                                    $bahan_baku_finishing[] = array('iddetail_produksi'=>gen_primary("DPROD","produksi_detail","iddetail_produksi"),
                                                                'idproduksi'=> $data['idproduksi'],
                                                                'idbarang_bb' => $record->idbarang_bb,
                                                                'qty' => $qty,
                                                                'id_konversi'=> $record->id_satuan_terkecil,
                                                                'tipe'=> 1);   
                                }
                            }   
                        }
                        // input bahan baku finishing selisih
                        if ($input_selisih == true) {
                            foreach ($finishing as $key => $dt) {
                                $penyusun_finishing = $this->produk_detail_penyusun_model
                                            ->join("barang","produk_detail_penyusun.idbarang_bb=barang.idbarang_bb","left")
                                            ->find_all_by(['idproduk'=>$dt->id_produk]);
                                if ($penyusun_finishing) {
                                    foreach ($penyusun_finishing as $key => $record) {
                                        $tipe = $record->st_tipe;
                                        if ($tipe==0){
                                            $data_qty_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_jml, floatval($record->jml_pakai));
                                            $real_qty_bahan = $data_qty_bahan['qty'];
                                            $qty = $data_selisih['jml_cetak'] * $real_qty_bahan;
                                            $konversi = get_konversi($data_qty_bahan['id_satuan_terkecil'],'',FALSE);
                                        }elseif($tipe==1){
                                            $data_panjang_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->panjang));
                                            $real_panjang       = $data_panjang_bahan['qty'];
                                            $data_lebar_bahan   = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->lebar));
                                            $real_lebar       = $data_lebar_bahan['qty'];
                                            $luas_bahan       = $real_panjang * $real_lebar;

                                            $qty        = $data_selisih['jml_cetak']* $luas_bahan;
                                            $konversi   = get_konversi($data_lebar_bahan['id_satuan_terkecil'],'',FALSE);
                                        }else{
                                            if ($produk) {
                                                // 0 = fix 
                                                // 1 = potongan
                                                $tipe = $produk->st_tipe;   
                                                if ($tipe==0) {
                                                    $qty        = $data_selisih['jml_cetak'];
                                                    $konversi   = get_konversi(1,'',FALSE);
                                                }else{
                                                    $qty        = $data_selisih['jml_cetak'] * $luas;
                                                    $konversi   = get_konversi(4,'',FALSE);
                                                }
                                            }
                                            
                                        }
                                        $bahan_baku_finishing[] = array('iddetail_produksi'=>gen_primary("DPROD","produksi_detail","iddetail_produksi"),
                                                                    'idproduksi'=> $data['idproduksi'],
                                                                    'idbarang_bb' => $record->idbarang_bb,
                                                                    'qty' => $qty,
                                                                    'id_konversi'=> $record->id_satuan_terkecil,
                                                                    'tipe'=> 1);   
                                    }
                                }   
                            }
                        }
                    }
                    // bahan baku penyusun Finishing tambahan
                    if (is_array($finishing_tambahan)) {
                        foreach ($finishing_tambahan as $key => $dt) {
                            $penyusun_finishing_tambahan = $this->produk_detail_penyusun_model
                                        ->join("barang","produk_detail_penyusun.idbarang_bb=barang.idbarang_bb","left")
                                        ->find_all_by(['idproduk'=> $dt->id_produk ]);
                            if ($penyusun_finishing_tambahan) {
                                foreach ($penyusun_finishing_tambahan as $key => $record) {
                                    $tipe = $record->st_tipe;
                                    if ($tipe==0){
                                        $data_qty_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_jml, floatval($record->jml_pakai));
                                        $real_qty_bahan = $data_qty_bahan['qty'];
                                        $qty = $real_qty_order * $real_qty_bahan;
                                        $konversi = get_konversi($data_qty_bahan['id_satuan_terkecil'],'',FALSE);
                                    }elseif($tipe==1){
                                        $data_panjang_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->panjang));
                                        $real_panjang       = $data_panjang_bahan['qty'];
                                        $data_lebar_bahan   = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->lebar));
                                        $real_lebar       = $data_lebar_bahan['qty'];
                                        $luas_bahan       = $real_panjang * $real_lebar;

                                        $qty        = $real_qty_order * $luas_bahan;
                                        $konversi   = get_konversi($data_lebar_bahan['id_satuan_terkecil'],'',FALSE);
                                    }else{
                                        if ($produk) {
                                            // 0 = fix 
                                            // 1 = potongan
                                            $tipe = $produk->st_tipe;   
                                            if ($tipe==0) {
                                                $qty        = $real_qty_order;
                                                $konversi   = get_konversi(1,'',FALSE);
                                            }else{
                                                $qty        = $real_qty_order * $luas;
                                                $konversi   = get_konversi(4,'',FALSE);
                                            }
                                        }
                                        
                                    }
                                    $bahan_baku_finishing[] = array(
                                                                'iddetail_produksi'=>gen_primary("DPROD","produksi_detail","iddetail_produksi"),
                                                                'idproduksi'=> $data['idproduksi'],
                                                                'idbarang_bb' => $record->idbarang_bb,
                                                                'qty' => $qty,
                                                                'id_konversi'=> $record->id_satuan_terkecil,
                                                                'tipe'=> 1);   
                                }
                            }   
                        }
                        // input finishing tambahan selisih
                        if ($input_selisih == true) {
                            foreach ($finishing_tambahan as $key => $dt) {
                                $penyusun_finishing_tambahan = $this->produk_detail_penyusun_model
                                            ->join("barang","produk_detail_penyusun.idbarang_bb=barang.idbarang_bb","left")
                                            ->find_all_by(['idproduk'=> $dt->id_produk ]);
                                if ($penyusun_finishing_tambahan) {
                                    foreach ($penyusun_finishing_tambahan as $key => $record) {
                                        $tipe = $record->st_tipe;
                                        if ($tipe==0){
                                            $data_qty_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_jml, floatval($record->jml_pakai));
                                            $real_qty_bahan = $data_qty_bahan['qty'];
                                            $qty = $data_selisih['jml_cetak'] * $real_qty_bahan;
                                            $konversi = get_konversi($data_qty_bahan['id_satuan_terkecil'],'',FALSE);
                                        }elseif($tipe==1){
                                            $data_panjang_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->panjang));
                                            $real_panjang       = $data_panjang_bahan['qty'];
                                            $data_lebar_bahan   = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->lebar));
                                            $real_lebar       = $data_lebar_bahan['qty'];
                                            $luas_bahan       = $real_panjang * $real_lebar;

                                            $qty        = $data_selisih['jml_cetak'] * $luas_bahan;
                                            $konversi   = get_konversi($data_lebar_bahan['id_satuan_terkecil'],'',FALSE);
                                        }else{
                                            if ($produk) {
                                                // 0 = fix 
                                                // 1 = potongan
                                                $tipe = $produk->st_tipe;   
                                                if ($tipe==0) {
                                                    $qty        = $data_selisih['jml_cetak'];
                                                    $konversi   = get_konversi(1,'',FALSE);
                                                }else{
                                                    $qty        = $data_selisih['jml_cetak'] * $luas;
                                                    $konversi   = get_konversi(4,'',FALSE);
                                                }
                                            }
                                            
                                        }
                                        $bahan_baku_finishing[] = array(
                                                                    'iddetail_produksi'=>gen_primary("DPROD","produksi_detail","iddetail_produksi"),
                                                                    'idproduksi'=> $data['idproduksi'],
                                                                    'idbarang_bb' => $record->idbarang_bb,
                                                                    'qty' => $qty,
                                                                    'id_konversi'=> $record->id_satuan_terkecil,
                                                                    'tipe'=> 1);   
                                    }
                                }   
                            }
                        }
                    }
                    $bb = array_merge($bahan_baku,$bahan_baku_finishing);
                    // simpan data detail tmp produksi
                    $add_tmp_produksi_detail = $this->produksi_detail_model->insert_batch($bb);
                    $query .= "/n/n". $this->db->last_query();

                }
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE)
            {

                $keterangan = "GAGAL, tambah data produksi dengan ID SPK :".$idspk;

                $message = ['type' => 'error', 'message' => 'Data produksi gagal disimpan'];
                $status     = 0;
                
                $return = FALSE;
            }
            else
            {

                $keterangan = "SUKSES, tambah data produksi dengan ID SPK : ".$idspk;
                $message = ['type' => 'success', 'message' => 'Data produksi berhasil disimpan'];
                $status     = 1;
                
                $return = TRUE;
            }

            //Save Log
            $nm_hak_akses   = $this->addPermission; 
            $kode_universal = $idspk;
            $jumlah         = 1;
            $sql            = $query;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
            
            echo json_encode($message);

        }
    }

    public function laporan_produksi(){

        $this->auth->restrict($this->laporanPermission);

        $where      = "produksi.deleted = 0 ";
        $where2     = "";

        $tgl1       = isset($_POST['tgl1']) ? $this->input->post('tgl1') : '';
        $tgl2       = isset($_POST['tgl2']) ? $this->input->post('tgl2') : '';
        $kategori   = isset($_POST['kategori']) ? $this->input->post('kategori') : '';
        $user       = isset($_POST['user']) ? $this->input->post('user') : '';

        if(strlen($tgl1) > 0){

            $tgl1_cr    = date_ymd($tgl1). " 01:00:00";
            $tgl2_cr    = date_ymd($tgl2). " 23:59:00";        
             
        }else{

            $tgl1_cr    = date("Y-m-d"). " 01:00:00";
            $tgl2_cr    = date("Y-m-d"). " 23:59:00";

            $tgl1       = date("d/m/Y");
            $tgl2       = date("d/m/Y");

        }
        
        $where      .= " AND (produksi.created_on >= '{$tgl1_cr}' AND produksi.created_on <= '{$tgl2_cr}')";

        $filter     =   [
                            'tgl1'      => $tgl1,
                            'tgl2'      => $tgl2,
                            'kategori'  => $kategori,
                            'user'      => $user
                        ];


        if(strlen($kategori) > 0){

            $where2  = " and `m_order_produk_detail`.`id_kategori` = {$kategori} ";

        }

        if(strlen($user) > 0){

            $where  .= " and `produksi`.`created_by` = {$user} ";

        }


        $dt_kategori    = $this->kategori_model 
                                ->select("`idkategori` as id, `nmkategori` as nm")
                                ->where("deleted = 0")
                                ->order_by("idkategori")
                                ->find_all();

        $dt_karyawan    = $this->karyawan_model
                                    ->distinct()
                                    ->select("`karyawan`.`nama` AS nm, 
                                            `karyawan`.`id_user` AS id")
                                    ->join("karyawan_detail_worksheet","karyawan.idkaryawan = karyawan_detail_worksheet.idkaryawan","inner")
                                    ->where("karyawan.deleted = 0
                                                AND karyawan.id_user IS NOT NULL")
                                    ->order_by("nm")
                                    ->find_all();

        $sql_resume     =  "SELECT 
                                    id_kategori, `nmkategori`, SUM(total_cetak) AS total
                                FROM
                                    (SELECT 
                                        `produksi`.`kode_universal`,
                                            SUM(`produksi`.`jml_cetak`) AS total_cetak,
                                            `produksi`.`created_by`,
                                            `produksi`.`created_on`,
                                            `users`.`nm_lengkap`
                                    FROM
                                        produksi
                                    INNER JOIN users ON produksi.created_by = users.id_user
                                    WHERE " . $where . "
                                    GROUP BY `produksi`.`kode_universal` , `produksi`.`created_by`) AS tbl_produksi
                                        INNER JOIN
                                    (SELECT 
                                        `m_order_produk_detail`.`kode_universal` AS id,
                                            `m_order_produk_detail`.`id_order`,
                                            `m_order_produk_detail`.`nama_pekerjaan`,
                                            `m_order_produk_detail`.`id_kategori`,
                                            `kategori`.`nmkategori`,
                                            `m_order_produk_detail`.`id_produk`,
                                            `produk`.`nm_produk`,
                                            `produk`.`st_tipe`,
                                            `m_order_produk_detail`.`p`,
                                            `m_order_produk_detail`.`l`,
                                            `m_order_produk_detail`.`id_satuan`,
                                            `m_order_produk_detail`.`st_finishing`,
                                            `konversi_satuan`.`satuan_besar`,
                                            `m_order_produk_detail`.`jml_cetak` AS jml_order
                                    FROM
                                        m_order_produk_detail
                                    INNER JOIN kategori ON m_order_produk_detail.id_kategori = kategori.idkategori
                                    INNER JOIN produk ON m_order_produk_detail.id_produk = produk.idproduk
                                    INNER JOIN konversi_satuan ON m_order_produk_detail.id_satuan = konversi_satuan.id_konversi
                                    INNER JOIN order_produk ON m_order_produk_detail.id_order = order_produk.id_order
                                    WHERE
                                        m_order_produk_detail.deleted = 0 ". $where2 ."
                                            AND order_produk.st_nota = 1) AS tbl_order ON tbl_produksi.kode_universal = tbl_order.id
                                GROUP BY id_kategori
                                ORDER BY id_kategori";

        $sql            =  "SELECT 
                                    *
                                FROM
                                    (SELECT 
                                        `produksi`.`kode_universal`,
                                            SUM(`produksi`.`jml_cetak`) AS total_cetak,
                                            `produksi`.`created_by`,
                                            `produksi`.`created_on`,
                                            date_format(`produksi`.`created_on`,'%d/%m/%Y %H:%i:%s') as waktu_produksi,
                                            `users`.`nm_lengkap`
                                    FROM
                                        produksi
                                    INNER JOIN users ON produksi.created_by = users.id_user
                                    WHERE " . $where . "
                                    GROUP BY `produksi`.`kode_universal` , `produksi`.`created_by`) AS tbl_produksi
                                        INNER JOIN
                                    (SELECT 
                                        `m_order_produk_detail`.`kode_universal` AS id,
                                            `m_order_produk_detail`.`id_order`,
                                            `m_order_produk_detail`.`nama_pekerjaan`,
                                            `m_order_produk_detail`.`id_kategori`,
                                            `kategori`.`nmkategori`,
                                            `m_order_produk_detail`.`id_produk`,
                                            `produk`.`nm_produk`,
                                            `produk`.`st_tipe`,
                                            `m_order_produk_detail`.`p`,
                                            `m_order_produk_detail`.`l`,
                                            `m_order_produk_detail`.`id_satuan`,
                                            `m_order_produk_detail`.`st_finishing`,
                                            `konversi_satuan`.`satuan_besar`,
                                            `m_order_produk_detail`.`jml_cetak` AS jml_order
                                    FROM
                                        m_order_produk_detail
                                    INNER JOIN kategori ON m_order_produk_detail.id_kategori = kategori.idkategori
                                    INNER JOIN produk ON m_order_produk_detail.id_produk = produk.idproduk
                                    INNER JOIN konversi_satuan ON m_order_produk_detail.id_satuan = konversi_satuan.id_konversi
                                    INNER JOIN order_produk ON m_order_produk_detail.id_order = order_produk.id_order
                                    WHERE
                                        m_order_produk_detail.deleted = 0 ". $where2 ."
                                            AND order_produk.st_nota = 1) AS tbl_order ON tbl_produksi.kode_universal = tbl_order.id
                                ORDER BY created_on , kode_universal";

        $data   = $this->db->query($sql)->result();
        $resume = $this->db->query($sql_resume)->result();

        //echo $this->db->last_query();

        $asset  =   [
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'produksi/assets/js/laporan_list_produksi.js'
                    ]; 

        add_assets($asset);
    
        $this->template->set("filter",$filter);

        $this->template->set("kategori",$dt_kategori);        
        $this->template->set("user",$dt_karyawan);        

        $this->template->set("resume",$resume);        
        $this->template->set("data",$data);        

        $this->template->set("toolbar_title", lang('title-lap-produksi'));
        $this->template->title(lang('title-lap-produksi'));
        $this->template->page_icon('fa fa-file-o');
        $this->template->render('laporan_produksi');                 

    }
}
?>