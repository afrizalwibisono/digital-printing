<?php 
    $ENABLE_ADD     = has_permission('Produksi.Add');
    $ENABLE_MANAGE  = has_permission('Produksi.Manage');
    $ENABLE_DELETE  = has_permission('Produksi.Delete');
?>
<style type="text/css">
    .select2-container .select2-selection {
          max-height: 30px; 
        }
    .
</style>
<div class="box box-primary">
    <!--form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_produksi','name'=>'frm_produksi','role','class'=>'form-inline'))?>
    <div class="box-header">
        <div class="pull-right">
            <div class="form-group">
                <select id="id_konsumen" name="id_konsumen" class="form-control input-sm">
                    <option></option>
                    <?php foreach ($konsumen as $key => $record) : ?>
                    <option value="<?= $record->idkonsumen; ?>" <?= set_select('id_konsumen', $record->idkonsumen, isset($data['id_konsumen']) && $data['id_konsumen'] == $record->idkonsumen) ?> data-tipe ="<?= $record->st;?>"><?= $record->panggilan ."  ". $record->nama ?></option> 
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <div class="input-group input-daterange">
                    <input type="text" name="tgl_awal" id="tgl_awal" class="form-control input-sm" readonly placeholder="<?= lang('produksi_tgl_order') ?>" value = "<?= set_value('tgl_awal',isset($tgl_awal) ? $tgl_awal : '') ?>"  placeholder="Tanggal Awal Order">
                    <span class="input-group-addon input-sm">to</span>
                    <input type="text" name="tgl_akhir" id="tgl_akhir" class="form-control input-sm" readonly placeholder="<?= lang('produksi_tgl_order') ?>" value = "<?= set_value('tgl_akhir',isset($tgl_akhir) ? $tgl_akhir : '') ?>"  placeholder="Tanggal Akhir Order">
                </div>
            </div>
            <div class="form-group">
                <select class="form-control input-sm" name="st_selesai" id="st_selesai">
                    <option value="0" <?= set_select('st_selesai','0', isset($st_selesai) && $st_selesai == 0) ?> ><?= lang('produksi_belum_input') ?></option>
                    <option value="1" <?= set_select('st_selesai','1', isset($st_selesai) && $st_selesai == 1) ?> ><?= lang('produksi_sudah_input') ?></option>
                </select>
            </div>
            <div class="form-group">
                <input type="text" name="table_search" id="table_search" class="form-control input-sm" placeholder="<?= lang('produksi_search') ?>" value="<?php echo isset($search) ? $search : ''; ?>">
            </div>
            <div class="form-group">
                <button class="btn btn-default btn-sm" name="btn_cari" id="btn_cari">
                    <span class="fa fa-search"></span>
                </button>
            </div>
        </div>

    </div>
    <?php if($results) : ?>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="tdet_order" name="tdet_order">
                <thead>
                    <tr class="success">
                        <th width="50">#</th>
                        <th style="vertical-align:middle; text-align: center;" colspan="2"><?= lang('produksi_nama_file') ?></th>
                        <th style="vertical-align:middle; text-align: center;"><?= lang('produksi_nama_konsumen') ?></th>
                        <th style="vertical-align:middle; text-align: center;"><?= lang('produksi_tgl_order') ?></th>
                        <th style="vertical-align:middle;text-align: center; "><?= lang('produksi_st_urgent') ?></th>
                        <th width="120px" style="vertical-align:middle;text-align: center;"><?= lang('produksi_qty') ?></th>
                        <th width="60px" style="vertical-align:middle;text-align: center;" class="danger"><?= lang('produksi_qty_cetak') ?></th>
                        <th style="vertical-align:middle;text-align: center;width: 150px; "><?= lang('produksi_act') ?></th>
                    </tr>
                </thead>
                <tbody>
                            <?php 
                                foreach($results as $key => $record) :
                            ?>
                        <tr>
                            <td><?= $numb++; ?></td>
                            <td width="65px" style="vertical-align: middle;"><img src="<?= base_url($record->thumbnail)?>" width="65" height="65"></td>
                            <td style="vertical-align: middle;">
                                <p style="font-weight: bold; margin-bottom: 0px;">
                                    <?= strtoupper($record->nama_pekerjaan) ?>
                                </p>
                                <p style="margin-bottom: 0px;">
                                    <?= strtoupper($record->nm_produk) ?>    
                                </p>
                                <p style="margin-bottom: 0px;">
                                    <?= $record->status_finishing ?>
                                </p>
                                <p style="margin-bottom: 0px;" class="text-red">
                                    <?= $record->catatan ?>
                                </p>
                                <p style="margin-bottom: 0px;" class="text-green">
                                   <b>Designer :</b> <i><?= $record->nm_lengkap ?></i>
                                </p>
                                <p>
                                    <a class="text-blue" target="_blank" href="<?= site_url('spk/view/' . $record->idspk .'/'.$record->id_produk); ?>" data-toggle="tooltip" data-placement="left" title="View Data">View Bahan Penyusun <i class="fa fa-folder-open"></i></a>
                                </p>
                            </td>
                            <td style="text-align: center; vertical-align: middle;"><?= $record->nama;?></td>
                            <td style="text-align: center; vertical-align: middle;"> <?= date('d M Y', strtotime($record->tgl_order)) ?></td>
                            <td style="text-align: center; vertical-align: middle;">
                                <?= ($record->st_urgent==1 ? "<span class='label label-danger'>". date("d-m-Y H:i", strtotime($record->tgl_permintaan_selesai)) ."</span>" : "<span class='label label-info'>".date("d-m-Y H:i", strtotime($record->tgl_permintaan_selesai))."</span>")?>
                            </td>
                            <td style="text-align: center; vertical-align: middle;"> <?= $record->jml_cetak ?> </td>
                            <td style="text-align: center; vertical-align: middle;" width="60px"> 
                                <input type="text" class="form-control input-sm" style="margin-bottom: 5px;" size="13" name="qty_cetak" id="qty_cetak" value="0" autocomplete="off">
                                <span class="label label-info" style="font-size: 12px;padding-top: 5px;">
                                  Tercetak:  <?= number_format($record->qty_tercetak>0? $record->qty_tercetak:0)?>
                                </span>

                            </td>
                            <td style="padding-right:20px;text-align: center; vertical-align: middle;">
                                <!-- hiden element -->
                                <input type="hidden" name="request_cetak" value="<?= $record->jml_cetak?>">
                                <input type="hidden" name="qty_tercetak" value="<?= $record->qty_tercetak?>">
                                <input type="hidden" name="idspk" value="<?= $record->idspk ?>">
                                <input type="hidden" name="kode_universal" value="<?= $record->kode_universal?>">

                                <a href="#" title="Simpan jumlah cetak produksi" onclick="<?= ($record->st_selesai == 1) ? 'event.preventDefault()' : 'simpan_produksi(this, event)'?>"> <button class="btn btn-success" style="margin-bottom: 5px"><i class="fa fa-save"></i></button></a>
                                <a class="text-black" href="#" onclick="view_detail(this, event)" data-toggle="tooltip" data-placement="left" title="View Detail Order"><button class="btn btn-warning" style="margin-bottom: 5px"><i class="fa fa-folder-open"></i></button></a>
                                <a class="text-green" href="#" onclick="<?= ($record->st_selesai == 1) ? 'event.preventDefault()' : 'selesai_produksi(this, event)'?>"  data-toggle="tooltip" data-placement="left" title="Selesai Produksi"><button class="btn btn-info"><i class="fa fa-check-circle"></i></button></a>
                                <a class="text-red" href="#" onclick="<?= ($record->st_selesai == 1) ? 'event.preventDefault()' : 'cancel_spk(this, event)'  ?>" data-toggle="tooltip" data-placement="left" title="Cancel SPK"><button class="btn btn-danger"><i class="fa fa-reply"></i></button></a>
                            </td>
                        </tr>
                    <?php 
                        endforeach;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="box-footer clearfix">
        <?php
        echo $this->pagination->create_links(); 
        ?>
    </div>
    <?php else: ?>
     <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('produksi_no_records_found') ?></p>
    </div>
    <?php
    endif;
    echo form_close() ?>
</div>

<!-- modal view -->
<div id="lightbox-view" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="border-radius: 5px !important;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">View Detail</h4>
            </div>
            <div class="modal-body form-horizontal">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group <?= form_error('gambar') ? ' has-error' : ''; ?>">
                                <label for="gambar" class="col-sm-5 control-label"><?= lang('produksi_gambar_cetak') ?></label>
                                <div class="col-sm-7">
                                    <img width="100" height="100" src="" id="view_img" name="view_img">
                                </div>
                            </div>
                            <div class="form-group <?= form_error('nama_pekerjaan') ? ' has-error' : ''; ?>">
                                <label for="nama_pekerjaan" class="col-sm-5 control-label"><?= lang('produksi_nama_pekerjaan') ?></label>
                                <div class="col-sm-7">
                                    <span class="form-control" id="view_nama_pekerjaan"></span>
                                </div>
                            </div>
                            <div class="form-group <?= form_error('id_kategori') ? ' has-error' : ''; ?>">
                                <label for="id_kategori" class="col-sm-5 control-label"><?= lang('produksi_id_kategori') ?></label>
                                <div class="col-sm-7">
                                    <span class="form-control" id="view_id_kategori_produk"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group <?= form_error('id_produk') ? ' has-error' : ''; ?>">
                                <label for="id_produk" class="col-sm-5 control-label"><?= lang('produksi_id_produk') ?></label>
                                <div class="col-sm-7">
                                    <span class="form-control" id="view_id_produk"></span>
                                </div>
                            </div>
                            <div class="form-group <?= form_error('ukuran_cetak') ? ' has-error' : ''; ?>">
                                <label for="ukuran_cetak" class="col-sm-5 control-label"><?= lang('produksi_ukuran_cetak') ?></label>
                                <div class="col-sm-7">
                                    <span class="form-control" id="view_p"></span>
                                </div>
                            </div>
                            <div class="form-group <?= form_error('note_produk') ? ' has-error' : ''; ?>">
                                <label for="note_produk" class="col-sm-5 control-label"><?= lang('produksi_note_produk') ?></label>
                                <div class="col-sm-7">
                                    <textarea id="view_note_produk" disabled class="form-control">
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-group <?= form_error('st_finishing') ? ' has-error' : ''; ?>">
                                <label for="st_finishing" class="col-sm-5 control-label"><?= lang('produksi_st_finishing') ?></label>
                                <div class="col-sm-7">
                                    <span class="form-control" id="view_st_finishing"></span>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-12" id="detail-hide">
                        <!-- view detail finishing -->
                        <p style="margin-top: 15px; margin-bottom: 3px;">
                            <span class="label label-warning" style="font-size: 12px"><?= lang('produksi_list_finishing') ?></span>
                        </p>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="view_tdet_finishing" name="view_tdet_finishing">
                                <thead>
                                    <tr class="success">
                                        <th style="vertical-align:middle;"><?= lang('produksi_no') ?></th>
                                        <th style="vertical-align:middle;"><?= lang('produksi_id_finishing') ?></th>
                                        <th style="vertical-align: middle;"><?= lang('produksi_note_finishing')?></th>
                                        <th width="150px" style="vertical-align:middle;" class="text-center"><?= lang('produksi_jml_pakai_finishing') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12" id="detail-a3">
                        <p style="margin-bottom: 0px">Detail A3:</p>
                        <hr style="padding-top:0px; margin-top: 0px; margin-bottom: 10px;">
                        <div id="body">
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                 
            </div>  
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
