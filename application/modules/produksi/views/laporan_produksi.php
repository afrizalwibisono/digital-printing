<div class="box box-primary">
	
	<?= form_open($this->uri->uri_string(), [
												'name'	=> 'form_laporan',
												'id'	=> 'form_laporan',
												'role'	=> 'form',
												'class'	=> 'form-inline'
											]) ?>

	<div class="box-header hidden-print">

		<div class="pull-right">
			
			<div class="form-group">
				<div class="input-group input-daterange">
					<input type="text" name="tgl1" id="tgl1" style="width: 120px" class="form-control" placeholder="<?= lang("tgl_awal") ?>" value="<?= set_value('tgl1',isset($filter['tgl1']) ? $filter['tgl1'] : '' ) ?>" >
					<span class="input-group-addon">to</span>
					<input type="text" name="tgl2" id="tgl2" style="width: 120px" class="form-control" placeholder="<?= lang("tgl_akhir") ?>" value="<?= set_value('tgl2',isset($filter['tgl2']) ? $filter['tgl2'] : '' ) ?>">
				</div>
			</div>

			<div class="form-group">
				<select class="form-control" name="kategori" id="kategori" style="min-width: 120px">
					<option></option>
					<?php if(isset($kategori) && is_array($kategori) && count($kategori)): 

							foreach ($kategori as $key => $val) :		
					?>
						<option value="<?= $val->id ?>"  <?= set_select('kategori',$val->id,isset($filter['gudang']) && $filter['gudang'] == $val->id )?> ><?= $val->nm ?></option>
					<?php 
							endforeach;
						endif 
					?>
					
				</select>
			</div>

			<div class="form-group">
				<select class="form-control" name="user" id="user" style="min-width: 120px">
					<option></option>
					<?php if(isset($user) && is_array($user) && count($user)): 

							foreach ($user as $key => $val) :		
					?>
						<option value="<?= $val->id ?>"  <?= set_select('user',$val->id,isset($filter['user']) && $filter['user'] == $val->id )?> ><?= $val->nm ?></option>
					<?php 
							endforeach;
						endif 
					?>
					
				</select>
			</div>



		</div>


		<div class="clearfix"></div>
		<hr style="margin-top: 8px;margin-bottom: 5px">
		
		<div class="pull-left">
			<button type="button" id="cetak" class="btn btn-default" onclick="cetak_halaman()"><i class="fa fa-print"></i> Cetak</button>
		</div>
		<div class="pull-right">
			<button type="submit" id="cari" name="cari" class="btn btn-default"><i class="fa fa-search"></i> Filter</button>
		</div>

	</div>

	<?= form_close() ?>	

	<?php if($idt) : ?>
    <div class="row header-laporan">
        <div class="col-md-12">
            <div class="col-md-1 laporan-logo">
                <img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo">
            </div>
            <div class="col-md-6 laporan-identitas">
                <address>
                    <h4><?= $idt->nm_perusahaan ?></h4>
                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?>
                </address>
            </div>
            <div class="col-md-5 text-right">
                <h3><?= lang("title-jdl-lap-produksi") ?></h3>
                <h5>Periode Cetak <strong><?= $filter["tgl1"]." - ".$filter["tgl2"] ?></strong></h5>
            </div>
            <div class="clearfix"></div>
            <div class="laporan-garis"></div>
        </div>
    </div>
    <?php endif ?>

    <?php if(is_array($data) && isset($data) && count($data)) : ?>

    <div class="box-body laporan-body">
    		
    	<h4><?= lang('lap-resume-jdl') ?></h4>
    	<table class="table-condensed table-summary">
    		<tbody>
    			<?php foreach ($resume as $key => $isi) : ?>
    			<tr>
    				<td width="200"><?= $isi->nmkategori ?></td>
    				<td width="10">:</td>
    				<td ><strong> <?= $isi->total ?> </strong></td>
    			</tr>
    			<?php endforeach ?>
    		</tbody>
    	</table>

    	<h4><?= lang('lap-detail-jdl') ?></h4>
    	<div class="table-responsive no-padding">
    		<table class="table table-condensed table-bordered table-detail">
    			<thead>
    				<tr class="success">
    					<th class="text-center" width="10">#</th>
    					<th class="text-center"><?= lang("lap-detail-waktu") ?></th>
    					<th class="text-center"><?= lang("lap-detail-kategori") ?></th>
    					<th class="text-center"><?= lang("lap-detail-info") ?></th>
    					<th class="text-center"><?= lang("lap-detail-produk") ?></th>
    					<th class="text-center"><?= lang("lap-detail-jml-order") ?></th>
    					<th class="text-center"><?= lang("lap-detail-jml-cetak") ?></th>
    					<th class="text-center"><?= lang("lap-detail-user") ?></th>

    				</tr>
    			</thead>
    			<tbody>
    				<?php foreach ($data as $keydetail => $isi) : ?>
    				<tr>
    					<td class="text-right"><?= $keydetail+1 ?></td>	
    					<td class="text-center"><?= $isi->waktu_produksi ?></td>	
    					<td class="text-center"><?= $isi->nmkategori ?></td>	
    					<td>
    						<?php 
    						$nm_file	= $isi->nama_pekerjaan;
    						$ukuran 	= $isi->st_tipe == 1 ? $isi->p." x ".$isi->l." (".$isi->satuan_besar.")" : "";

    						echo $nm_file."<br>".$ukuran;

    						?>
    					</td>	
    					<td class="text-center"><?= $isi->nm_produk ?></td>	
    					<td class="text-center"><?= number_format($isi->jml_order) ?></td>	
    					<td class="text-center"><?= number_format($isi->total_cetak) ?></td>	
    					<td class="text-center"><?= $isi->nm_lengkap ?></td>	
    				</tr>
    				<?php endforeach ?>
    			</tbody>
    		</table>
    	</div>

    </div>	

    <?php else : ?>

    	<div class="alert alert-info" role="alert">
	        <p><i class=fa fa-warning"></i> &nbsp; <?= lang('produksi_no_records_found') ?></p>
	    </div>

    <?php endif ?>

</div>