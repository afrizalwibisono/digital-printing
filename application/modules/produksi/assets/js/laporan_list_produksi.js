$(function(){

	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	$("#user").select2({
		placeholder :"Operator",
        allowClear  : true
	});

	$("#kategori").select2({
		placeholder :"Kategori Barang",
        allowClear  : true
	});	

});

function cetak_halaman(){
	window.print();
}