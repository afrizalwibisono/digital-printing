$(function() {
	$("#btn_simpan").on("click", function(e){
		e.preventDefault();
		alertify.confirm("Yakin untuk menyimpan Produksi?",
		  function(){
		    alertify.success('Simpan Produksi Success');
		  },
		  function(){
		    alertify.error('cancel simpan produksi');
		  }).setHeader('<em> Warning!! </em> ');
	});
	//Date Picker
	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});
	$("#id_konsumen").select2({placeholder : '-Pilih Konsumen-', allowClear: true});		
	$("input[name='qty_cetak']").number(true);
});

function simpan_produksi(obj, e){
	e.preventDefault();
	var idspk = $(obj).closest('tr').find('input[name="idspk"]').val();
	var qty_cetak = $(obj).closest('tr').find('input[name="qty_cetak"]').val();
	var qty_tercetak = $(obj).closest('tr').find('input[name="qty_tercetak"]').val();
	var request_cetak = $(obj).closest('tr').find('input[name="request_cetak"]').val();
	var kode_universal = $(obj).closest('tr').find('input[name="kode_universal"]').val();
	var selisih = qty_cetak - qty_tercetak;
	var cek = true;
	alertify.confirm("Yakin untuk menyimpan Produksi?",
  	function(){
  		if (parseFloat(qty_cetak) + parseFloat(qty_tercetak) > parseFloat(request_cetak)) {
  			alertify.error('jumlah input Cetak lebih besar dari jumlah cetak yang diminta!');
			cek = false;
		// }else if (parseFloat(qty_tercetak) > parseFloat(qty_cetak)) {
		// 	alertify.error('jumlah cetak yang ada inputkan kurang dari jml yang terlah dicetak sebelumnya!');
		// 	cek = false;
		}else if (parseFloat(qty_cetak) < 1) {
			cek = false;
		}
		if (cek == true) {
			// ajax simpan data produksi
	  		$.ajax({
				url : baseurl+"produksi/save_produksi",
				type : "post",
				dataType : "json",
				data : {
					idspk : idspk,
					qty_cetak: qty_cetak,
					kode_universal: kode_universal,
				},
				success : function(msg){
					if(msg['type'] == 'success') {
						alertify.success(msg['message']);
						setTimeout(function(){ $("#btn_cari").trigger("click") }, 1000);
					} else{
						alertify.error(msg['message']);
						location.reload();
					}
					
				},
				error : function(err){
					alert(JSON.stringify(err));
				}
			});
		}
    	


  	},
  	function(){
    	alertify.error('cancel simpan produksi');
  	}).setHeader('<em> Warning!! </em> ');
}

function selesai_produksi(obj, e){
	e.preventDefault();
	var selisih = 0;
	var idspk = $(obj).closest('tr').find('input[name="idspk"]').val();
	var qty_cetak = $(obj).closest('tr').find('input[name="qty_cetak"]').val();
	var qty_tercetak = $(obj).closest('tr').find('input[name="qty_tercetak"]').val();
	var request_cetak = $(obj).closest('tr').find('input[name="request_cetak"]').val();
	var kode_universal = $(obj).closest('tr').find('input[name="kode_universal"]').val();
	if (parseFloat(qty_cetak)>0 && parseFloat(qty_tercetak) > 0 ) {
		selisih = qty_cetak - qty_tercetak;
	}
	var cek = true;
	alertify.confirm("Yakin untuk menyimpan Produksi?",
  	function(){
  		if ((parseFloat(qty_cetak) + parseFloat(qty_tercetak)) > parseFloat(request_cetak)) {
  			alertify.error('jumlah input Cetak lebih besar dari jumlah cetak yang diminta!');
			cek = false;
		}
		else if (parseFloat(qty_cetak)<1) {
			qty_cetak=0;
		}
		// else if (parseFloat(qty_cetak) == parseFloat(qty_tercetak)) {
		// 	cek = false;
		// }
		if (cek == true) {
			// ajax simpan data produksi
	  		$.ajax({
				url : baseurl+"produksi/selesai_produksi",
				type : "post",
				dataType : "json",
				data : {
					idspk : idspk,
					qty_cetak: qty_cetak,
					kode_universal: kode_universal,
				},
				success : function(msg){
					if(msg['type'] == 'success') {
						alertify.success(msg['message']);
						setTimeout(function(){ $("#btn_cari").trigger("click"); }, 1000);
					} else{
						alertify.error(msg['message']);
						location.reload();
					}
					
				},
				error : function(err){
					alert(JSON.stringify(err));
				}
			});
		}
    	


  	},
  	function(){
    	alertify.error('cancel selesai produksi');
  	}).setHeader('<em> Warning!! </em> ');
}


function cancel_spk(obj, e){
	e.preventDefault();
	var idspk = $(obj).closest('tr').find('input[name="idspk"]').val();
	alertify.confirm("Yakin untuk menyimpan Produksi?",
  	function(){	
		// ajax simpan data produksi
		$.ajax({
			url : baseurl+"produksi/cancel_spk",
			type : "post",
			dataType : "json",
			data : {
				idspk : idspk,
			},
			success : function(msg){
				if(msg['type'] == 'success') {
					alertify.success(msg['message']);
					setTimeout(function(){ $("#btn_cari").trigger("click"); }, 1000);
				} else{
					alertify.error(msg['message']);
					location.reload();
				}
				
			},
			error : function(err){
				alert(JSON.stringify(err));
			}
		});

  	},
  	function(){
    	alertify.error('cancel simpan produksi');
  	}).setHeader('<em> Critical!! </em> ');
}

function view_detail(obj, e){
	e.preventDefault();
	var kode_universal = $(obj).closest('tr').find('input[name="kode_universal"]').val();
	var src_img='';
    if (kode_universal !="") {
        $.ajax({
            url : baseurl+"produksi/view_detail",
            type : "post",
            dataType : "json",
            data : {kode_universal : kode_universal},
            success : function(msg){
            	if (msg['type']=='success') {
            		console.log(msg);
        			src_img = baseurl+msg['data']['thumbnail'];
            		$("#view_nama_pekerjaan").text(msg['data']['nama_pekerjaan']);
            		$("#view_id_kategori_produk").text(msg['data']['nmkategori']);
            		$("#view_id_produk").text(msg['data']['nm_produk']);
            		$("#view_img").attr('src', src_img);
            		$("#view_st_dimensi").text(msg['data']['st_dimensi']);
            		$("#view_p").text(msg['data']['p']+"x"+msg['data']['l']+" "+msg['data']['satuan_panjang']);
            		$("#view_jml_order").text( $.number(msg['data']['jml_cetak'])+" "+msg['data']['satuan_order']);
            		$("#view_biaya_cetak").text($.number( parseFloat(msg['data']['biaya_cetak']) + parseFloat(msg['data']['kenaikan_value']) ));
            		$("#view_biaya_desain").text($.number(msg['data']['biaya_design']));
            		$("#view_note_produk").text(msg['data']['catatan']);
            		$("#view_st_finishing").text((msg['data']['st_finishing']==0)?'Finishing Standart':'Finishing Custom');

            		if (msg['detail']!='') {
            			$("table#view_tdet_finishing tbody tr").remove();
            				var no=0;
            				var total =0;
							$.each(msg['detail'], function(i,n){
								var $tr = $("<tr>"
												+"<td>"+ (no+=1) +"</td>"
												+"<td>"+n['nm_produk']+"</td>"
												+"<td>"+n['catatan']+"</td>"
												+"<td>"+n['jml']+" Set </td>"
											+"</tr>");
								$("table#view_tdet_finishing tbody").append($tr);
								total += n['harga'];
							});
						$("#view_gtotal_finishing").append($.number(total));
						$("#detail-hide").show();
            		}else{
            			$("#detail-hide").hide();
            		}
            		if (msg['detail_a3'] !='') {
            			$("#detail-a3 #body").empty();
            			$.each(msg['detail_a3'], function(j,k){
            				var $box= $(
            						"<div class='col-sm-2'>"
			                            +"<div class='box box-default'>"
			                            	+"<div class='box-header bg-danger' style='max-height: 35px'>"
			                             		+"<p>Page: "+ k['page'] +"</p>"
			                               	+"</div>"
			                                + "<div class='box-body bg-aqua' style='text-align: center; vertical-align: middle;max-height: 60px'>"
			                                    +"<h4><b>"+ k['jml_print'] +"</b></h4>"
			                                +"</div>"
			                            +"</div>"
			                        +"</div>"
			                        );
            				$("#detail-a3 #body").append($box);
            			});
            			$("#detail-a3").show();
            		}else{
            			$("#detail-a3").hide();
            		}

            		$("#lightbox-view").modal('show');  
            	}
                
            },
            error : function(err){
                alert(JSON.stringify(err));
            }
        });
    }
}