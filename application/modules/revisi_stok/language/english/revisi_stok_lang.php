<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
// untuk laporan stok opname by value
$lang['so_val_laporan_title']					= "Value Revisi Stok";
$lang['so_val_laporan_jdl']						= "Rekap Value Revisi Stok";

$lang['isi-so-status-berkurang']				= "Stok Berkurang";
$lang['isi-so-status-bertambah']				= "Stok Bertambah";


$lang['so-table-kategori']						= "Kategori";	
$lang['so-table-barang']						= "Nama Barang";	
$lang['so-table-jml']							= "Jml Opname";	
$lang['so-table-value']							= "Value(Rp.)";	
$lang['so-table-st']							= "Status Opname";	

// ============================
$lang['revisi_stok_title_manage'] 	= 'Revisi Stok';
$lang['revisi_stok_title_new'] 		= 'Revisi Stok Baru';
$lang['revisi_stok_title_view'] 	= 'Detail Revisi Stok';
$lang['revisi_stok_laporan_title_view'] = 'Laporan Stok Revisi Stok';

// form/table
$lang['revisi_stok_kode'] 		= 'Kode';
$lang['revisi_stok_tanggal'] 	= 'Tanggal Pelaksanaan';
$lang['revisi_stok_id_gudang']	= 'Gudang';
$lang['revisi_stok_ket'] 		= 'Keterangan';
$lang['revisi_stok_operator'] 	= 'Operator';
$lang['revisi_stok_cetak'] 		= 'Cetak';

$lang['revisi_stok_no'] 		= '#';
$lang['revisi_stok_barcode'] 	= 'Barcode';
$lang['revisi_stok_nm_barang'] 	= 'Nama Barang';
$lang['revisi_stok_jenis'] 		= 'Jenis';
$lang['revisi_stok_stok_awal']	= 'Stok Awal';
$lang['revisi_stok_stok_sistem'] 	= 'Stok Sistem';
$lang['revisi_stok_stok_saat_ini'] 	= 'Stok Saat Ini';
$lang['revisi_stok_satuan'] 	= 'Satuan';
$lang['revisi_stok_selisih'] 	= 'Selisih';
$lang['revisi_stok_st_barang'] 	= 'Status Selisih';
$lang['revisi_stok_ket_catatan'] = 'Catatan';

$lang['revisi_stok_pilih'] 		= '-- Pilih --';
$lang['revisi_stok_pilih_gudang'] = '-- Pilih Gudang --';


$lang['revisi_stok_ringkasan'] 		= 'Ringkasan';
$lang['revisi_stok_detail'] 		= 'Detail Transaksi';
$lang['revisi_stok_total_tran_qty'] = 'Transaksi';
$lang['revisi_stok_total_tran'] 	= 'Total Seluruh Transaksi';
$lang['revisi_stok_kali'] 			= 'kali';

$lang['revisi_stok_range_tgl'] = 'Range Tanggal';
$lang['revisi_stok_tgl_awal']  = 'Tanggal awal';
$lang['revisi_stok_tgl_akhir'] = 'Tanggal akhir';

$lang['revisi_stok_pilih_brg'] = 'Pilih Barang';
$lang['revisi_stok_scan_brg']  = 'Ketik / Scan Barcode Barang';
$lang['revisi_stok_cari_brg']  = 'Cari Barang ...';
$lang['revisi_stok_cari_btn']  = 'Cari';
$lang['revisi_stok_pilih_btn'] = 'Pilih';

// button
$lang['revisi_stok_btn_new'] 		= 'Baru';
$lang['revisi_stok_btn_delete'] 	= 'Hapus';
$lang['revisi_stok_btn_draft'] 		= 'Draft';
$lang['revisi_stok_btn_save'] 		= 'Simpan';
$lang['revisi_stok_btn_cancel'] 	= 'Batal';
$lang['revisi_stok_btn_back'] 		= 'Kembali';
$lang['revisi_stok_or'] 			= 'atau';

// messages
$lang['revisi_stok_del_error']			= 'Anda belum memilih stok opname yang akan dihapus.';
$lang['revisi_stok_del_failure']		= 'Tidak dapat menghapus stok opname : ';
$lang['revisi_stok_delete_confirm']		= 'Apakah anda yakin akan menghapus stok opname terpilih ?';
$lang['revisi_stok_cancel_confirm']		= 'Apakah anda yakin akan membatalkan stok opname ?';
$lang['revisi_stok_deleted']			= 'Data Stok Opname berhasil dihapus';
$lang['revisi_stok_no_records_found'] 	= 'Data tidak ditemukan.';

$lang['revisi_stok_create_failure'] 	= 'Stok Opname baru gagal disimpan: ';
$lang['revisi_stok_create_success'] 	= 'Stok Opname baru berhasil disimpan';
$lang['revisi_stok_draft_success'] 		= 'Draft Stok Opname berhasil disimpan';
$lang['revisi_stok_canceled'] 			= 'Stok Opname telah berhasil dibatalkan';

$lang['revisi_stok_edit_success'] 		= 'Stok Opname berhasil disimpan';
$lang['revisi_stok_invalid_id'] 		= 'ID Tidak Valid';

$lang['revisi_stok_no_item'] 			= 'Anda belum menambahkan data barang stok opname.';
$lang['revisi_stok_insert_ket']  		= 'Silahkan isi keteragan pada baris ke : %s';
$lang['revisi_stok_less_then_zero']  	= 'Kolom stok saat ini baris ke : %s tidak boleh kurang dari 0';
$lang['revisi_stok_choose_max_selisih'] = 'Selisih barang pada baris ke : %s melebihi batas maksimal sebesar : %s';

$lang['revisi_stok_barcode_found'] 	= 'Barcode : %s ditemukan.';
$lang['revisi_stok_barcode_not_found'] = 'Barcode : %s tidak ditemukan.';

$lang['revisi_stok_only_ajax'] 		= 'Hanya ajax request yang diijinkan.';

$lang['revisi_stok_date_required'] 	= 'Silahkan pilih tanggal awal dan akhir.';