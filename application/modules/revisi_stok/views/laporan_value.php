<div class="box box-primary">

	<?= form_open($this->uri->uri_string(), [
												'name'	=> 'form_laporan',
												'id'	=> 'form_laporan',
												'role'	=> 'form',
												'class'	=> 'form-inline'
											]) ?>

	<div class="box-header hidden-print">
		
		<div class="pull-right">
			
			<div class="form-group">
				<div class="input-group input-daterange">
					<input type="text" name="tgl1" id="tgl1" style="width: 120px" class="form-control" placeholder="<?= lang("tgl_awal") ?>" value="<?= set_value('tgl1',isset($filter['tgl1']) ? $filter['tgl1'] : '' ) ?>" >
					<span class="input-group-addon">to</span>
					<input type="text" name="tgl2" id="tgl2" style="width: 120px" class="form-control" placeholder="<?= lang("tgl_akhir") ?>" value="<?= set_value('tgl2',isset($filter['tgl2']) ? $filter['tgl2'] : '' ) ?>">
				</div>
			</div>

			<div class="form-group">
				<select class="form-control" name="gudang" id="gudang" style="min-width: 120px">
					<?php if(isset($dt_gudang) && is_array($dt_gudang) && count($dt_gudang)): 

							foreach ($dt_gudang as $key => $val) :		
					?>
						<option value="<?= $val->id ?>"  <?= set_select('gudang',$val->id,isset($filter['gudang']) && $filter['gudang'] == $val->id )?> ><?= $val->nm ?></option>
					<?php 
							endforeach;
						endif 
					?>
				</select>
			</div>

			<div class="form-group">
				<select class="form-control" name="kategori" id="kategori" style="min-width: 120px">
					<option></option>
					<?php if(isset($dt_kategori) && is_array($dt_kategori) && count($dt_kategori)): 

							foreach ($dt_kategori as $key => $val) :		
					?>
						<option value="<?= $val->id ?>"  <?= set_select('kategori',$val->id,isset($filter['kategori']) && $filter['kategori'] == $val->id )?> ><?= $val->nm ?></option>
					<?php 
							endforeach;
						endif 
					?>
					
				</select>
			</div>

			<div class="form-group">
				<select class="form-control" name="status" id="status" style="min-width: 120px">
					<option></option>
					<option value="0" <?= set_select("status","0",$filter['status'] === 0 && isset($filter['status'])) ?> ><?= lang("isi-so-status-berkurang") ?></option>
					<option value="1" <?= set_select("status","1",$filter['status'] === 1 && isset($filter['status'])) ?> ><?= lang("isi-so-status-bertambah") ?></option>
				</select>
			</div>			

		</div>

		<div class="clearfix"></div>
		<hr style="margin-top: 8px;margin-bottom: 5px">
		
		<div class="pull-left">
			<button type="button" id="cetak" class="btn btn-default" onclick="cetak_halaman()"><i class="fa fa-print"></i> Cetak</button>
		</div>
		<div class="pull-right">
			<button type="submit" id="cari" name="cari" class="btn btn-default"><i class="fa fa-search"></i> Filter</button>
		</div>

	</div>

	<?= form_close() ?>

	<?php if($idt) : ?>
    <div class="row header-laporan">
        <div class="col-md-12">
            <div class="col-md-1 laporan-logo">
                <img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo">
            </div>
            <div class="col-md-6 laporan-identitas">
                <address>
                    <h4><?= $idt->nm_perusahaan ?></h4>
                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?>
                </address>
            </div>
            <div class="col-md-5 text-right">
                <h3><?= lang("so_val_laporan_jdl") ?></h3>
                <h5>Periode Cetak <strong><?= $filter["tgl1"]." - ".$filter["tgl2"] ?></strong></h5>
            </div>
            <div class="clearfix"></div>
            <div class="laporan-garis"></div>
        </div>
    </div>
    <?php endif ?>


    <?php if(isset($data) && is_array($data) && count($data)) : ?>

    	<div class="box-body laporan-body">

    		<div class="table-responsive no-padding">
    			<table class="table-condensed table-head">
    				<tr>
    					<td width="50">Gudang</td>
    					<td width="20">:</td>
    					<td >
    						<strong><?= $nm_gudang ?></strong></td>
    				</tr>
    			</table>
    		</div>

    		<div class="table-responsive no-padding">
    			<table class="table table-condensed table-bordered table-detail">
    				<thead>
    					<tr class="success">
    						<th width="10" class="text-center">#</th>
    						<th class="text-center"><?= lang("so-table-kategori") ?></th>
    						<th class="text-center"><?= lang("so-table-barang") ?></th>
    						<th class="text-center"><?= lang("so-table-jml") ?></th>
    						<th class="text-center"><?= lang("so-table-value") ?></th>
    						<th class="text-center"><?= lang("so-table-st") ?></th>
    					</tr>
    				</thead>
    				<tbody>
    					<?php foreach ($data as $key => $isi) : ?>
    						<tr>
    							<td class="text-right"><?= $key+1 ?></td>
    							<td class="text-center"><?= strtoupper($isi->nmkategori) ?></td>
    							<td class="text-center"><?= strtoupper($isi->nm_barang) ?></td>
    							<td class="text-center"><?= number_format($isi->jml_total) ?></td>
    							<td class="text-center"><?= number_format($isi->value_st_opname) ?></td>
    							<td class="text-center">
    								<?php 

    									if($isi->st_stok == 0){
    										echo lang("isi-so-status-berkurang");
    									}else{
    										echo lang("isi-so-status-bertambah");
    									}
    								?>
    							</td>
   
    						</tr>
    					<?php endforeach ?>
    				</tbody>
    			</table>
    		</div>


    	</div>

    <?php else : ?>
    	<div class="alert alert-info" role="alert">
	        <p><i class=fa fa-warning"></i> &nbsp; <?= lang('revisi_stok_no_records_found') ?></p>
	    </div>
	<?php endif ?>

</div>