<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_laporan','name'=>'frm_laporan','class' => 'form-inline')) ?>
        <div class="box-header">
            <div class="pull-left no-print">
                <button type="button" id="cetak" class="btn btn-default" onclick="cetak_halaman()"><i class="fa fa-print"></i> <?= lang('revisi_stok_cetak') ?></button>
            </div>
            <div class="pull-right no-print">
                <div class="form-group">
                    <div class="input-daterange input-group" title="<?= lang('revisi_stok_range_tgl') ?>" data-toggle="tooltip" data-placement="top">
                        <input type="text" style="width: 120px" class="form-control" name="tgl_awal" id="tgl_awal" value="<?= isset($period1) ? $period1 : '' ?>" placeholder="<?= lang('revisi_stok_tgl_awal') ?>" />
                        <span class="input-group-addon">to</span>
                        <input type="text" style="width: 120px" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?= isset($period2) ? $period2 : '' ?>" placeholder="<?= lang('revisi_stok_tgl_akhir') ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <select class="form-control" id="id_gudang" name="id_gudang">
                        <option value=""><?= lang('revisi_stok_pilih_gudang') ?></option>
                        <?php foreach ($gudang as $key => $dt_gudang) : ?>
                        <option value="<?= $dt_gudang->id_gudang ?>" <?= set_select('id_gudang', $dt_gudang->id_gudang, isset($id_gudang) && $id_gudang == $dt_gudang->id_gudang ) ?>><?= $dt_gudang->nama_gudang ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="<?= lang('revisi_stok_nm_barang') ?>" autofocus />
                        <div class="input-group-btn">
                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?= form_close() ?>
    <?php if($idt) : ?>
    <div class="row header-laporan">
        <div class="col-md-12">
            <div class="col-md-1 laporan-logo">
                <img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo">
            </div>
            <div class="col-md-6 laporan-identitas">
                <address>
                    <h4><?= $idt->nm_perusahaan ?></h4>
                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?>
                </address>
            </div>
            <div class="col-md-5 text-right">
                <h3>Lap. Revisi Stok <?= $nm_gudang ?></h3>
                <h5>Periode <?= $period1." - ".$period2 ?></h5>
            </div>
            <div class="clearfix"></div>
            <div class="laporan-garis"></div>
        </div>
    </div>
    <?php endif ?>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
    	<div class="box-body laporan-body">
            <?php foreach ($results as $key => $data): ?>
                <div class="table-responsive no-padding">
                    <table class="table-condensed table-head" width="90%">
                        <tr>
                            <td><?= lang('revisi_stok_kode') ?></td>
                            <td>:</td>
                            <td><strong><?= $data->id_stok_opname ?></strong></td>
                            <td><?= lang('revisi_stok_tanggal') ?></td>
                            <td>:</td>
                            <td><?= date('d F Y', strtotime($data->pelaksanaan)) ?></td>
                            <td><?= lang('revisi_stok_ket') ?></td>
                            <td>:</td>
                            <td><?= $data->ket ?></td>
                        </tr>
                        <tr>
                            <td><?= lang('revisi_stok_id_gudang') ?></td>
                            <td>:</td>
                            <td><?= $data->nama_gudang ?></td>
                            <td><?= lang('revisi_stok_operator') ?></td>
                            <td>:</td>
                            <td><?= ucwords($data->operator) ?></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </div>
                <div class="table-responsive no-padding">
                    <table class="table table-condensed table-bordered table-detail">
                        <thead>
                            <tr class="bg-success">
                                <th width="50"><?= lang('revisi_stok_no') ?></th>
                                <th><?= lang('revisi_stok_barcode') ?></th>
                                <th><?= lang('revisi_stok_nm_barang') ?></th>
                                <th><?= lang('revisi_stok_jenis') ?></th>
                                <th><?= lang('revisi_stok_stok_awal')?></th>
                                <th><?= lang('revisi_stok_stok_saat_ini') ?></th>
                                <th><?= lang('revisi_stok_satuan') ?></th>
                                <th><?= lang('revisi_stok_ket_catatan') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($data->detail) : ?>
                                <?php 
                                    $no = 1;
                                    foreach($data->detail as $key => $dt) : 
                                ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td>
                                    <?= $dt->barcode ?>
                                </td>
                                <td><?= $dt->nm_barang ?></td>
                                <td><?= $dt->nmjenis_bb ?></td>
                                <td class="text-right"><?= number_format($dt->stok_sistem)?></td>
                                <td class="text-right"><?= number_format($dt->stok_saat_ini) ?></td>
                                <td><?= $dt->satuan ?></td>
                                <td>
                                    <?= $dt->ket ?>
                                </td>
                            </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
        	       </table>
               </div>
          <?php endforeach ?>
    	</div><!-- /.box-body -->
        
	<?php else: ?>
    <div class="callout callout-info">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('revisi_stok_no_records_found') ?></p>
    </div>
    <?php
	endif;
	?>
</div><!-- /.box -->