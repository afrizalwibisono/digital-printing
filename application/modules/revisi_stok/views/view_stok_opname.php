<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_view_stok_op','name'=>'frm_view_stok_op','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="form-group">
			    <div class="col-md-3">
			    	<label for="id_stok_opname" class="control-label"><?= lang('revisi_stok_kode') ?></label>
			    	<input type="text" class="form-control" id="id_stok_opname" name="id_stok_opname" maxlength="100" value="<?= $data->id_stok_opname ?>" readonly>
			    </div>
			    <div class="col-md-3">
			    	<label for="id_gudang" class="control-label"><?= lang('revisi_stok_id_gudang') ?></label>
			    	<input type="text" class="form-control" id="id_gudang" name="id_gudang" maxlength="100" value="<?= $data->nama_gudang ?>" readonly>
			    </div>
			    <div class="col-md-3">
			    	<label for="pelaksanaan" class="control-label"><?= lang('revisi_stok_tanggal') ?></label>
			    	<input type="text" class="form-control" id="pelaksanaan" name="pelaksanaan" maxlength="10" value="<?= date('d/m/Y', strtotime($data->pelaksanaan)) ?>" readonly>
			    </div>
		  	</div>
		  	<div class="table-responsive">
		  		<table class="table table-bordered" id="tdet_stok_opname">
		  			<thead>
		  				<tr class="success">
			  				<th width="50"><?= lang('revisi_stok_no') ?></th>
			  				<th><?= lang('revisi_stok_barcode') ?></th>
			  				<th><?= lang('revisi_stok_nm_barang') ?></th>
			  				<th><?= lang('revisi_stok_jenis') ?></th>
			  				<th><?= lang('revisi_stok_stok_awal')?></th>
			  				<th><?= lang('revisi_stok_stok_saat_ini') ?></th>
			  				<th><?= lang('revisi_stok_satuan') ?></th>
			  				<th><?= lang('revisi_stok_ket_catatan') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($detail) : ?>
		  					<?php 
		  						$no = 1;
		  						foreach($detail as $key => $dt) : 
		  					?>
		  				<tr>
		  					<td><?= $no++ ?></td>
		  					<td>
		  						<?= $dt->barcode ?>
		  					</td>
		  					<td><?= $dt->nm_barang ?></td>
		  					<td><?= $dt->nmjenis_bb ?></td>
		  					<td><?= number_format($dt->stok_sistem)?></td>
		  					<td width="100"><?= number_format($dt->stok_saat_ini) ?></td>
		  					<td><?= $dt->satuan ?></td>
		  					<td>
		  						<?= $dt->ket ?>
		  					</td>
		  				</tr>
		  					<?php endforeach; ?>
		  				<?php endif; ?>
		  			</tbody>
		  		</table>
		  	</div>
		  	<div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
			    <div class="col-sm-6">
			    	<label for="ket" class="control-label"><?= lang('revisi_stok_ket') ?></label>
			    	<textarea class="form-control" id="ket" name="ket" maxlength="255" readonly><?= $data->ket ?></textarea>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <div class="col-md-12">
			      	<a class="btn btn-success" href="<?= site_url('revisi_stok') ?>"><?= lang('revisi_stok_btn_back') ?></a>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.panel_body -->