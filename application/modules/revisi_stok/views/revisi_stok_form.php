<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string().(isset($_POST['id_gudang']) ? '?gudang='.urlencode($_POST['id_gudang']) : ''),array('id'=>'frm_stok_opname','name'=>'frm_stok_opname','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-header">
    		<div class="form-group">
				<div class="col-md-3">
			    	<label for="id_stok_opname" class="control-label"><?= lang('revisi_stok_kode') ?></label>
			    	<input type="text" class="form-control" id="id_stok_opname" name="id_stok_opname" maxlength="100" value="<?= $data['id_stok_opname'] ?>" readonly autofocus>
				</div>
			    <div class="col-md-3">
		    		<label for="id_gudang" class="control-label"><?= lang('revisi_stok_id_gudang')?></label>
			    	<select class="form-control" id="id_gudang" name="id_gudang">
			    		<?php foreach ($gudang as $key => $dt_gudang) : ?>
			    		<option value="<?= $dt_gudang->id_gudang ?>" <?= set_select('id_gudang', $dt_gudang->id_gudang, isset($data['id_gudang']) && $data['id_gudang'] == $dt_gudang->id_gudang ) ?>><?= $dt_gudang->nama_gudang ?></option>
			    		<?php endforeach ?>
			    	</select>
			    </div>
				<div class="col-md-3 <?= form_error('pelaksanaan') ? 'has-error' : '' ?>">
			    	<label for="pelaksanaan" class="control-label"><?= lang('revisi_stok_tanggal') ?></label>
			    	<input type="text" class="form-control" id="pelaksanaan" name="pelaksanaan" maxlength="10" value="<?= set_value('pelaksanaan', isset($data['pelaksanaan']) && $data['pelaksanaan'] !='' ? date('d/m/Y', strtotime($data['pelaksanaan'])) : '') ?>" required>
				</div>
			</div>
    	</div>
    	<div class="box-body">
		  	<div class="table-responsive" style="padding: 0;">
		  		<table class="table table-bordered" id="tdet_stok_opname">
		  			<thead>
		  				<tr class="success">
			  				<th width="50"><?= lang('revisi_stok_no') ?></th>
			  				<th><?= lang('revisi_stok_barcode') ?></th>
			  				<th><?= lang('revisi_stok_nm_barang') ?></th>
			  				<th><?= lang('revisi_stok_jenis') ?></th>
			  				<th><?= lang('revisi_stok_stok_sistem') ?></th>
			  				<th width="110"><?= lang('revisi_stok_stok_saat_ini') ?></th>
			  				<th><?= lang('revisi_stok_satuan') ?></th>
			  				<th><?= lang('revisi_stok_ket_catatan') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($data['items']) : ?>
		  					<?php 
		  						$no = 1;
		  						foreach($data['items'] as $key => $dt) : 
		  					?>
		  				<tr>
		  					<td><?= $no++ ?></td>
		  					<td>
		  						<?= $dt->barcode ?>
		  						<input type="hidden" name="id_barang[]" value="<?= $dt->id_barang ?>" />
		  					</td>
		  					<td><?= $dt->nm_barang ?></td>
		  					<td><?= $dt->nmjenis_bb ?></td>
		  					<td width="100"><?= number_format($dt->stok) ?></td>
		  					<td width="100"><input type="number" min="0" name="stok_saat_ini[]" class="form-control" value="<?= isset($dt->stok_saat_ini) ? $dt->stok_saat_ini : 0 ?>" /></td>
		  					<td><?= $dt->satuan_terkecil ?></td>
		  					<td>
		  						<input type="text" name="catatan[]" class="form-control" value="<?= isset($dt->catatan) ? $dt->catatan : '' ?>" />
		  					</td>
		  				</tr>
		  					<?php endforeach; ?>
		  				<?php endif; ?>
		  			</tbody>
		  		</table>
		  	</div>
		  	<div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
			    <div class="col-sm-6">
			    	<label for="ket" class="control-label"><?= lang('revisi_stok_ket') ?></label>
			    	<textarea class="form-control" id="ket" name="ket" maxlength="255"><?php echo set_value('ket', isset($data['ket']) ? $data['ket'] : ''); ?></textarea>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <div class="col-md-12">
			    	<input type="hidden" name="type" value="1" />
			      	<button type="submit" name="save" class="btn btn-primary"><?= lang('revisi_stok_btn_save') ?></button>
			    	<button type="submit" name="draft" class="btn btn-success" title="save as draft"><?= lang('revisi_stok_btn_draft') ?></button>
			    	<?php
	                	echo lang('revisi_stok_or') . ' ' . anchor('revisi_stok/cancel', lang('revisi_stok_btn_cancel'), array("onclick" => "return confirm('".lang('revisi_stok_cancel_confirm')."')"));
	                ?>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->