<?php 
    $ENABLE_ADD     = has_permission('Revisi Stok.Add');
    $ENABLE_MANAGE  = has_permission('Revisi Stok.Manage');
    $ENABLE_DELETE  = has_permission('Revisi Stok.Delete');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'index_stok_opname','name'=>'index_stok_opname', 'class' => 'form-inline')) ?>
        <div class="box-header">
            <div class="form-group">
                <?php if ($ENABLE_ADD) : ?>
	  	        <a href="<?= site_url('revisi_stok/create') ?>" class="btn btn-success" title="<?= lang('revisi_stok_btn_new') ?>"><?= lang('revisi_stok_btn_new') ?></a>
                <?php endif; ?>
            </div>
            <div class="pull-right">
                <div class="form-group">
                    <div class="input-daterange input-group">
                        <input type="text" class="form-control" name="tgl_awal" id="tgl_awal" value="<?= isset($tgl_awal) && $tgl_awal !='' ? date('d/m/Y', strtotime($tgl_awal)) : '' ?>" placeholder="<?= lang('pembelian_tgl_awal') ?>" required />
                        <span class="input-group-addon">to</span>
                        <input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?= isset($tgl_akhir) && $tgl_akhir !='' ? date('d/m/Y', strtotime($tgl_akhir)) : '' ?>" placeholder="<?= lang('pembelian_tgl_akhir') ?>" required />
                    </div>
                </div>
                <select id="id_gudang" name="id_gudang" class="form-control" style="min-width: 150px">
                    <?php foreach ($gudang as $key => $gd) : ?>
                    <option value="<?= $gd->id_gudang ?>" <?= set_select('id_gudang', $gd->id_gudang, isset($id_gudang) && ($id_gudang === $gd->id_gudang)) ?>><?= $gd->nama_gudang ?></option>
                    <?php endforeach; ?>
                </select>
                <div class="input-group">
                    <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="Search" autofocus />
                    <div class="input-group-btn">
                        <button class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
	<div class="box-body">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
                        <th width="50">#</th>
                        <th><?= lang('revisi_stok_tanggal') ?></th>
                        <th><?= lang('revisi_stok_kode') ?></th>
                        <th><?= lang('revisi_stok_id_gudang') ?></th>
                        <th><?= lang('revisi_stok_operator') ?></th>
                        <th><?= lang('revisi_stok_ket') ?></th>
                        <?php if($ENABLE_MANAGE) : ?>
                        <th width="25"></th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($results as $record) : ?>
                    <tr>
                        <td class="column-check"><input type="checkbox" name="checked[]" value="<?= $record->id_stok_opname ?>" /></td>
                        <td><?= $numb; ?></td>
                        <td><?= date('d-m-Y', strtotime($record->pelaksanaan)) ?></td>
                        <td><?= $record->id_stok_opname ?></td>
                        <td><?= $record->nama_gudang ?></td>
                        <td><?= $record->operator ?></td>
                        <td><?= $record->ket ?></td>
                        <?php if($ENABLE_MANAGE) : ?>
                        <td style="padding-right:20px"><a class="text-black" href="<?= site_url('revisi_stok/view/' . $record->id_stok_opname); ?>" data-toggle="tooltip" data-placement="left" title="View Detail"><i class="fa fa-folder"></i></a></td>
                        <?php endif; ?>
                    </tr>
                    <?php $numb++; endforeach; ?>
                </tbody>
	  </table>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">
		<?php if($ENABLE_DELETE) : ?>
		<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('revisi_stok_btn_delete') ?>" onclick="return confirm('<?= (lang('revisi_stok_delete_confirm')); ?>')">
		<?php endif;
		echo $this->pagination->create_links(); 
		?>
	</div>
	<?php else: ?>
    <div class="alert alert-info">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('revisi_stok_no_records_found') ?></p>
    </div>
    <?php
	endif;
	echo form_close(); 
	?>
</div><!-- /.box -->