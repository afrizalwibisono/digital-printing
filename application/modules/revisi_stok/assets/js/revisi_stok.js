$(document).ready(function(){
	//Date Picker
	$("#tgl_awal, #tgl_akhir").datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	
	$("#ck_all").on("click", function(){
		if($(this).prop("checked")){
			$("input[name='item_code[]']").prop("checked", true);
		}else{
			$("input[name='item_code[]']").prop("checked", false);
		}
	});

	$("#cari_btn").on("click", function(){
		cari_barang();
	});

	$("#cr_barang").on("keydown", function(e){
		if(e.keyCode == 13){
			cari_barang();
			e.preventDefault();
		}
	});

	$("input[name='stok_saat_ini[]'], input[name='catatan[]']").on("change", function(){
		hitung_selisih(this);
	});

	//Scan Barcode
	$("input[name='barcode']").on("keydown", function(e){
		if(e.keyCode == 13){
			scan_barcode();
			e.preventDefault();
		}
	});

	//Date Picker
	$('#pelaksanaan').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	$("#pilih_btn").on("click", function(){
		add_item();
	});

	$("#frm_stok_opname #id_gudang").on('change', function(){
		var url = updateQueryStringParameter(siteurl+'revisi_stok\/create', 'gudang', $(this).val());

		window.location.href=url;
	});

	var curUrl 		  = window.location.href;
	var paramIDGudang = getParameterByName('gudang', curUrl);
	var formURI 	  = $("#frm_stok_opname").prop('action');
	var formIDGudang  = getParameterByName('gudang', formURI);
	if(!paramIDGudang && parseInt(formIDGudang) > 0){
		setTimeout(function(){
			window.location.href = siteurl+'revisi_stok\/create?gudang='+formIDGudang;
		}, 2000);
	}
});