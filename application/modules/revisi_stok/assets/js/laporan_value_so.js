$(function(){

	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	$("#gudang").select2({
		placeholder :"Gudang",
        allowClear  : true
	});

	$("#kategori").select2({
		placeholder :"Kategori Barang",
        allowClear  : true
	});	

	$("#status").select2({
		placeholder :"Status Revisi Stok",
        allowClear  : true
	});	


});

function cetak_halaman(){
	window.print();
}