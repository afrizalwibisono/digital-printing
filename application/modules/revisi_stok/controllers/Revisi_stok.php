<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2018, Cokeshome
 * 
 * This is controller for Stok Opname
 */

class Revisi_stok extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Revisi Stok.View";
    protected $addPermission    = "Revisi Stok.Add";
    protected $managePermission = "Revisi Stok.Manage";
    protected $deletePermission = "Revisi Stok.Delete";
    protected $laporanPermission = "Laporan Revisi Stok.View";
    protected $laporanValuePermission = "Laporan Value Revisi Stok.View";



    protected $prefixKey        = "RS";
    protected $prefixKeyStok    = "STK";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('revisi_stok/revisi_stok');
        $this->load->model(array('revisi_stok/stok_opname_model',
                                'revisi_stok/stok_opname_detail_model',
                                'revisi_stok/stok_opname_detail_hpp_model',
                                'revisi_stok/gudang_model',
                                'bahan_baku/bahan_baku_model',
                                'stok_model',
                                'identitas_model',
                                'kategori_model'));

        $this->template->title(lang('revisi_stok_title_manage'));
		$this->template->page_icon('fa fa-retweet');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if (isset($_POST['delete']) && has_permission($this->deletePermission))
        {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                foreach ($checked as $pid)
                {
                    $query_hapus = "";
                    
                    $del_opname = $this->stok_opname_model->delete_where(array('id_stok_opname' => $pid));
                    $query_hapus .= "\n\n".$this->db->last_query();

                    $del_detail_hpp = FALSE;
                    if($del_opname)
                    {
                        //Hapus Detail HPP
                        $data_detail_hpp = $this->stok_opname_detail_hpp_model->find_all_by(array('id_stok_opname' => $pid));
                        $del_detail_hpp  = $this->stok_opname_detail_hpp_model->delete_where(array('id_stok_opname' => $pid));
                        $query_hapus .= "\n\n".$this->db->last_query();
                        //Kembalikan Stok
                        if($del_detail_hpp)
                        {
                            foreach ($data_detail_hpp as $key => $dthpp) {
                                $this->stok_model->update_stok_by_kd_stok($dthpp->id_barang, $dthpp->id_stok, $dthpp->jml, (($dthpp->st_stok == 0) ? 1 : 0));
                                $query_hapus .= "\n\n".$this->db->last_query();
                            }
                        }
                    }
                    else
                    {
                        $del_opname == FALSE;
                    }
                    

                    if($del_opname && $del_detail_hpp)
                    {
                        $keterangan = "SUKSES, hapus data stok opname dengan ID : ".$pid;
                        $status     = 1;
                    }
                    else
                    {
                        $keterangan = "GAGAL, hapus data stok opname dengan ID : ".$pid;
                        $status     = 0;
                    } 
                    
                    $nm_hak_akses   = $this->deletePermission; 
                    $kode_universal = $pid;
                    $jumlah         = 1;
                    $sql            = $query_hapus;

                    $hasil = simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                }

                if ($del_opname && $del_detail_hpp)
                {
                    $this->template->set_message(count($checked) .' '. lang('revisi_stok_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('revisi_stok_del_failure') . (isset($err) ? $err." " : "") . $this->stok_opname_model->error, 'error');
                }
            }
            else
            {
                $this->template->set_message(lang('revisi_stok_del_error').$this->stok_opname_model->error, 'error');
            }

            unset($_POST['delete']);
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search     = isset($_POST['table_search'])?$this->input->post('table_search'):'';
            $tgl_awal   = isset($_POST['tgl_awal']) ? date_ymd($this->input->post('tgl_awal')) : '';
            $tgl_akhir  = isset($_POST['tgl_awal']) ? date_ymd($this->input->post('tgl_akhir')) : '';
            $id_gudang  = isset($_POST['id_gudang']) ? $this->input->post('id_gudang') :'';
        }
        else
        {
            $search     = isset($_GET['search'])?$this->input->get('search'):'';
            $tgl_awal   = isset($_GET['tgl_awal']) ? date_ymd($this->input->get('tgl_awal')) : '';
            $tgl_akhir  = isset($_GET['tgl_awal']) ? date_ymd($this->input->get('tgl_akhir')) : '';
            $id_gudang  = isset($_GET['id_gudang']) ? $this->input->get('id_gudang') :'';
        }

        $filter = "";
        if($search!="")
        {
            $filter = "?search=".$search;
        }

        $addWhere = "";
        if($tgl_awal != '' && $tgl_akhir != '')
        {
            $filter   .= "&tgl_awal=".$tgl_awal."&tgl_akhir=".$tgl_akhir;
            $addWhere .= " AND ( date(pelaksanaan) >='".$tgl_awal."' AND date(pelaksanaan) <='".$tgl_akhir."')";
        }
        else
        {
            $tgl_awal = date('Y-m-01');
            $tgl_akhir = date('Y-m-d');
            
            $filter   .= "&tgl_awal=".$tgl_awal."&tgl_akhir=".$tgl_akhir;
            $addWhere .= " AND ( date(pelaksanaan) >='".$tgl_awal."' AND date(pelaksanaan) <='".$tgl_akhir."')";
        }

        if($id_gudang !='')
        {
            $filter .= "&id_gudang=".$id_gudang;
            $addWhere .= " AND stok_opname.id_gudang='".$id_gudang."'";
        }

        $search2 = $this->db->escape_str($search);
        
        $where = "`stok_opname.deleted` = 0 $addWhere
                AND (`ket` LIKE '%$search2%' ESCAPE '!')";

        
        $total = $this->stok_opname_model
                ->join("users","stok_opname.created_by = users.id_user","left")
                ->join("gudang","stok_opname.id_gudang = gudang.id_gudang","left")
                ->where($where)
                ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);
        $data = $this->stok_opname_model->select(array("stok_opname.*","gudang.nama_gudang as nama_gudang","users.nm_lengkap AS operator"))
                ->join("users","stok_opname.created_by = users.id_user","left")
                ->join("gudang","stok_opname.id_gudang = gudang.id_gudang","left")
                ->where($where)
                ->order_by('pelaksanaan','DESC')
                ->limit($limit, $offset)->find_all();    

        $assets= array('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'plugins/number/jquery.number.js',
                        'revisi_stok/assets/js/revisi_stok.js',
                        );
        add_assets($assets);
        $data_gudang= $this->gudang_model->where("deleted = 0")->find_all();
        
        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('tgl_awal', $tgl_awal);
        $this->template->set('tgl_akhir', $tgl_akhir);
        $this->template->set('id_gudang', $id_gudang);
        $this->template->set('gudang', $data_gudang);
        $this->template->title(lang('revisi_stok_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

   	//Create Stok Opname
   	public function create()
   	{
        $this->auth->restrict($this->addPermission);

        $id_gudang = intval($this->input->get('gudang')) ? intval($this->input->get('gudang')) : 1;
        if (isset($_POST['save']) || isset($_POST['draft'])){
            if ($this->save_stok_opname()){
                if(isset($_POST['draft'])){
                    $this->template->set_message(lang("revisi_stok_draft_success"), 'success');
                }else{
                    $this->template->set_message(lang("revisi_stok_create_success"), 'success');
                    redirect('revisi_stok');
                }
              
                
            }
        }
        //Cek Draft
        $ck = $this->stok_opname_model->find_by(['is_draft' => 1, 'id_gudang' => $id_gudang]);
        if(!$ck)
        {
            $dt_barang = $this->bahan_baku_model
                        ->select(array("barang.*","barang.idbarang_bb as id_barang","jenis_bb.nmjenis_bb","kategori.nmkategori","merk_bb.nm_merk_bb","satuan_terkecil.alias as satuan_terkecil","nama_gudang","SUM(IF(stok.deleted = 0, stok_update, 0)) AS stok"))
                        ->join("jenis_bb","barang.idjenis_bb = jenis_bb.idjenis_bb","left")
                        ->join("kategori","barang.idkategori = kategori.idkategori","left")
                        ->join("satuan_terkecil","barang.id_satuan_terkecil=satuan_terkecil.id_satuan_terkecil","left")
                        ->join("merk_bb","barang.idmerk_bb=merk_bb.idmerk_bb","left")
                        ->join("stok","barang.idbarang_bb = stok.id_barang","left")
                        ->join("gudang","stok.id_gudang = gudang.id_gudang","left")
                        ->where(['stok.id_gudang' => $id_gudang, 'barang.deleted' => 0])
                        ->group_by(array("barang.idbarang_bb","stok.id_gudang"))
                        ->order_by('nmjenis_bb','ASC')
                        ->order_by('nm_barang','ASC')
                        ->find_all();

            $data = array(
                        'id_stok_opname' => gen_primary($this->prefixKey),
                        'pelaksanaan' => '',
                        'items'     => $dt_barang,
                        'ket'       => '',
                        'id_gudang' => $id_gudang
                        );
        }else{   
            $dt_barang = $this->stok_opname_detail_model
                                            ->select(array("stok_opname.id_gudang","stok_opname_detail.*","barang.barcode","barang.nm_barang","jenis_bb.nmjenis_bb","satuan_terkecil.alias as satuan_terkecil","stok_sistem as stok", "stok_opname_detail.ket as catatan"))
                                            ->join('barang','stok_opname_detail.id_barang=barang.idbarang_bb','left')
                                            ->join("jenis_bb","barang.idjenis_bb = jenis_bb.idjenis_bb","left")
                                            ->join("satuan_terkecil","barang.id_satuan_terkecil=satuan_terkecil.id_satuan_terkecil","left")
                                            ->join('stok_opname','stok_opname_detail.id_stok_opname=stok_opname.id_stok_opname')
                                            ->order_by('nmjenis_bb','ASC')
                                            ->order_by('nm_barang','ASC')
                                            ->find_all_by('stok_opname_detail.id_stok_opname', $ck->id_stok_opname);
            $data = array(
                        'id_stok_opname' => $ck->id_stok_opname,
                        'pelaksanaan' => $ck->pelaksanaan,
                        'items'     => $dt_barang,
                        'ket'       => $ck->ket,
                        'id_gudang' => $ck->id_gudang
                        );
        }

        $gudang= $this->gudang_model->where(['deleted' => 0])->find_all();
        
        $assets= array('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/number/jquery.number.js',
                        'revisi_stok/assets/js/revisi_stok.js');

        add_assets($assets);
        $this->template->set('gudang', $gudang);
        $this->template->set('data', $data);

        $this->template->title(lang('revisi_stok_title_new'));
		$this->template->render('revisi_stok_form');
   	}

    //Get Stok By ID barang
    protected function stok_by_id($id = "", $id_gudang=0)
    {
        if($id == "")
        {
            return 0;
        }

        $cek = $this->stok_model->select(array("sum(stok_update) AS stok"))
                                ->find_by(array("deleted" => 0, "id_barang" => $id, "id_gudang" => $id_gudang));
        if($cek){
            $stok = $cek->stok;
        }
        else
        {
            $stok = 0;
        }

        return $stok;
    }

    //Get max selisih
    protected function get_max_selisih($id = "", $id_gudang)
    {
        if($id == "")
        {
            return 0;
        }

        $cek = $this->stok_model->select(array("sum(stok_datang - stok_update) AS max_selisih"))
                                ->find_by(array("deleted" => 0, "id_barang" => $id,"id_gudang"=>$id_gudang));
        if($cek){
            $max_selisih = $cek->max_selisih;
        }
        else
        {
            $max_selisih = 0;
        }

        return $max_selisih;
    }

    //View Detail
    public function view($id_stok_opname = '')
    {
        $this->auth->restrict($this->viewPermission);

        if (!$id_stok_opname)
        {
            $this->template->set_message(lang("revisi_stok_invalid_id"), 'error');
            redirect('revisi_stok');
        }

        $data  = $this->stok_opname_model->select("stok_opname.*, gudang.nama_gudang")
                                        ->join("gudang","stok_opname.id_gudang= gudang.id_gudang","left")
                                        ->find_by("stok_opname.id_stok_opname",$id_stok_opname);
        $detail = $this->stok_opname_detail_model->select("stok_opname_detail.*, satuan_terkecil.alias as satuan, barcode, nm_barang, nmjenis_bb")
                                            ->join('barang','stok_opname_detail.id_barang = barang.idbarang_bb')
                                            ->join('satuan_terkecil','barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil')
                                            ->join("jenis_bb","barang.idjenis_bb = jenis_bb.idjenis_bb","left")
                                            ->order_by('nmjenis_bb','ASC')
                                            ->order_by('nm_barang','ASC')
                                            ->find_all_by('id_stok_opname', $id_stok_opname);

        $this->template->set('id_stok_opname', $id_stok_opname);
        $this->template->set('data', $data);
        $this->template->set('detail', $detail);

        $this->template->set("page_title", lang('revisi_stok_title_view'));
        $this->template->render('view_stok_opname');
    }

    //Cancel Barang masuk
    public function cancel()
    {
        $this->auth->restrict($this->addPermission);

        $this->stok_opname_model->delete_where(['is_draft' => 1]);
        $this->template->set_message(lang('revisi_stok_canceled'), 'success');

        redirect('revisi_stok');
    }

   	protected function save_stok_opname()
   	{
        $id_stok_opname = $this->input->post('id_stok_opname');
        $id_barang      = $this->input->post('id_barang');
        $id_gudang      = $this->input->post('id_gudang');
        $stok_saat_ini  = $this->input->post('stok_saat_ini');
        $catatan        = $this->input->post('catatan');
        $pelaksanaan    = $this->input->post('pelaksanaan');
        $ket            = $this->input->post('ket');
        //validasi
        $this->form_validation->set_rules('id_stok_opname', 'lang:revisi_stok_kode','required');
        $this->form_validation->set_rules('pelaksanaan', 'lang:revisi_stok_tanggal','required');
        $this->form_validation->set_rules('id_gudang','lang:revisi_stok_id_gudang','callback_pilih');
        $this->form_validation->set_message('pilih', 'You should choose the "%s" field');

        if ($this->form_validation->run() === FALSE){
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }
        
        if(count($id_barang) == 0){
            $this->template->set_message(lang('revisi_stok_no_item'), 'error');
            return FALSE;
        }

        $ero_msg = array();
        $urut    = 0;
        $items   = [];
        foreach ($id_barang as $key => $ib) {
            $urut ++;
            if (floatval($stok_saat_ini[$key]) < 0) {
                $ero_msg[] = sprintf(lang('revisi_stok_less_then_zero'), $urut);
            }

            $stok_sistem    = $this->stok_by_id($ib, $id_gudang);
            $selisih        = abs($stok_sistem - $stok_saat_ini[$key]);
            $max_selisih    = $this->get_max_selisih($ib, $id_gudang);
            if ($stok_sistem < $stok_saat_ini[$key]) {
                if ($selisih>$max_selisih) {
                    $ero_msg[] = sprintf(lang('revisi_stok_choose_max_selisih'), $urut, $max_selisih);
                }
            }
        }

        if($ero_msg)
        {
            $ero_msg = implode("<br>", $ero_msg);
            $this->template->set_message($ero_msg, 'error');
            return FALSE;
        }
        //Simpan Ke table stok_opname
        $data_stk_opname = ['id_stok_opname' => $id_stok_opname,
                            'id_gudang'      => $id_gudang,
                            'pelaksanaan'    => date_ymd($pelaksanaan),
                            'ket'            => $ket
                            ];
        if(isset($_POST['save'])){
            $data_stk_opname['is_draft'] = 0;
        }else{
            $data_stk_opname['is_draft'] = 1;
        }

        //Get Tipe gudang
        $dt_gudang   = $this->gudang_model->find($id_gudang);
        $tipe_gudang = $dt_gudang->tipe;
        
        $this->db->trans_start();

        $ck = $this->stok_opname_model->find($id_stok_opname);
        if(!$ck){
            $this->stok_opname_model->insert($data_stk_opname);    
        }else{
            unset($data_stk_opname['id_stok_opname']);
            $this->stok_opname_model->update($id_stok_opname, $data_stk_opname);
        }
        
        $query_stok_opname = $this->db->last_query();
        //Check sukses atau tidak
        $st_stk_opname = $this->stok_opname_model->count_by('id_stok_opname', $id_stok_opname);
        $st_det_opname = FALSE;
        $st_det_hpp   = FALSE;

        if($st_stk_opname)
        {
            //Insert Detail Stok Opname
            $data_det_opname  = [];
            $data_stok_opname = [];
            $total            = 0;
            foreach ($id_barang as $key => $it) {
                $stok_sistem         = $this->stok_by_id($it, $id_gudang);
                $stok_saat_ini[$key] = floatval($stok_saat_ini[$key]);

                if($stok_sistem > $stok_saat_ini[$key]){
                    $st_selisih = 0; //Berkurang
                }elseif($stok_sistem < $stok_saat_ini[$key]){
                    $st_selisih = 1; //Bertambah
                }else{
                    $st_selisih = NULL;
                }

                $data_det_opname[] = array('id_detail_stok_opname' => gen_primary(),
                                        'id_stok_opname' => $id_stok_opname,
                                        'id_barang'      => $it,
                                        'stok_sistem'    => $stok_sistem,
                                        'stok_saat_ini'  => $stok_saat_ini[$key],
                                        'selisih'        => abs($stok_sistem - $stok_saat_ini[$key]),
                                        'st_selisih'     => $st_selisih,
                                        'ket'            => $catatan[$key]
                                        );

                $total += $stok_saat_ini[$key];
            }
            //Delete old data
            if($ck){
                $this->stok_opname_detail_model->delete_where(['id_stok_opname' => $id_stok_opname]);
            }
            $this->stok_opname_detail_model->insert_batch($data_det_opname);
            $query_stok_opname .= "\n\n".$this->db->last_query();

            $st_det_opname = $this->stok_opname_detail_model->count_by('id_stok_opname', $id_stok_opname);
            if($st_det_opname == count($data_det_opname) && isset($_POST['save']))
            {
                $st_det_opname = TRUE;
                //Update Stok
                $data_det_opname_hpp    = array();
                $data_det_kembali_hpp   = array();
                foreach ($data_det_opname as $key => $dtop) {
                    if($dtop['selisih'] > 0){
                        $update_stk_op = $this->stok_model->update_stok_by_kd_barang($dtop['id_barang'], $dtop['selisih'], $dtop['st_selisih'],$id_gudang);
                        $query_stok_opname .= "\n\n".$this->db->last_query();
                        
                        if($update_stk_op)
                        {
                            $dt_barang = $this->bahan_baku_model->find($dtop['id_barang']);
                            $st_potong_meteran = $dt_barang->st_potong_meteran;
                            //Kosongkan luas_update yang stok_update == 0 dan luas_update > 0, jika tipe gudang == 2
                            if($tipe_gudang == 2 && $st_potong_meteran == 1){ //Jika gudang produksi dan st_potong_meteran==1
                                $sql = "UPDATE stok SET luas_update=0 WHERE id_barang=? AND id_gudang=? AND stok_update = 0 AND luas_update > 0";
                                $this->db->query($sql, array($dtop['id_barang'], $id_gudang));
                                $query_stok_opname .= "\n\n".$this->db->last_query();
                            }

                            foreach ($update_stk_op as $key => $uso) {
                                //Update Luas update sama dengan luas datang
                                if($tipe_gudang == 2 && $st_potong_meteran == 1){
                                    $sql = "UPDATE stok SET luas_update=luas_datang WHERE id_stok=?";
                                    $this->db->query($sql, $uso['id_stok']);
                                    $query_stok_opname .= "\n\n".$this->db->last_query();
                                }

                                $data_det_opname_hpp[] = array('id_detail_stok_opname_hpp' => gen_primary(),
                                                                'id_stok_opname'           => $id_stok_opname,
                                                                'id_barang'                => $dtop['id_barang'],
                                                                'id_stok'                  => $uso['id_stok'],
                                                                'jml'                      => $uso['jml'],
                                                                'hpp'                      => $uso['hpp'],
                                                                'st_stok'                  => $dtop['st_selisih']
                                                                );

                                $data_det_kembali_hpp[] = array('id_barang'                => $dtop['id_barang'],
                                                                'id_stok'                  => $uso['id_stok'],
                                                                'jml'                      => $uso['jml'],
                                                                'hpp'                      => $uso['hpp'],
                                                                'st_barang'                => ($dtop['st_selisih'] == 0) ? 1 : 0
                                                                );
                            }
                        }
                    }
                }
                //Insert to detail_stok_opname_hpp
                if($data_det_opname_hpp){
                    $this->stok_opname_detail_hpp_model->insert_batch($data_det_opname_hpp);
                    $query_stok_opname .= "\n\n".$this->db->last_query();    
                }
            }
            else
            {
                $st_det_opname = FALSE;
                $this->template->set_message(lang('revisi_stok_create_failure'),'error');
            }
        }

        $this->db->trans_complete();

        //Save Log
        if ($st_stk_opname > 0 && $this->db->trans_status() == TRUE)
        {
            $keterangan = "SUKSES, tambah data stok opname dengan ID : ".$id_stok_opname;
            $status     = 1;
            
            $return = TRUE;
        }
        else
        {
            $keterangan = "GAGAL, tambah data stok opname dengan ID ".$id_stok_opname;
            $status     = 0;
            
            $return = FALSE;
        }

        //Save Log
        $nm_hak_akses   = $this->addPermission; 
        $kode_universal = $id_stok_opname;
        $jumlah         = $total;
        $sql            = $query_stok_opname;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        if($return)
        {
            //Clear Session
            $this->session->unset_userdata('stk_opname');
        }
        
        return $return;
   	}

    public function laporan()
    {
        $this->auth->restrict($this->laporanPermission);

        $this->form_validation->set_rules('tgl_awal', 'lang:revisi_stok_tgl_awal', 'required|trim');
        $this->form_validation->set_rules('tgl_akhir', 'lang:revisi_stok_tgl_akhir', 'required|trim');
        $this->form_validation->set_rules('id_gudang', 'lang:revisi_stok_id_gudang', 'required|trim');
        
        $valid = TRUE;
        if ($this->form_validation->run() === FALSE) {
            $this->template->set_message(validation_errors(), 'error');
            $valid = FALSE;
        }

        if(isset($_POST['table_search'])) {
            $search     = $this->input->post('table_search');
            $tgl_awal   = $this->input->post('tgl_awal');
            $tgl_akhir  = $this->input->post('tgl_akhir');
        } else {
            $search     = $this->input->get('table_search');
            $tgl_awal   = $this->input->get('tgl_awal');
            $tgl_akhir  = $this->input->get('tgl_akhir');
        }

        $id_gudang  = $this->input->post('id_gudang');
        $search2    = $this->db->escape_str($search);

        $where = "stok_opname.deleted = 0 AND is_draft = 0 AND nm_barang like '%$search2%'";

        if($id_gudang){
            $where .= " AND stok_opname.id_gudang='".$id_gudang."'";
        }

        if($tgl_awal !="" && $tgl_akhir !="")
        {
            $tgl1 = date_ymd($tgl_awal);
            $tgl2 = date_ymd($tgl_akhir);
            
            $where .= " AND (stok_opname.pelaksanaan >='".$tgl1."' AND stok_opname.pelaksanaan <='".$tgl2."')";
        }

        if($valid){
            $data = $this->stok_opname_model->select(array("stok_opname.*","users.nm_lengkap as operator", "nama_gudang"))
                                            ->join('stok_opname_detail','stok_opname.id_stok_opname = stok_opname_detail.id_stok_opname')
                                            ->join('barang','stok_opname_detail.id_barang = barang.idbarang_bb')
                                            ->join('users','stok_opname.created_by = users.id_user',"left")
                                            ->join("gudang","stok_opname.id_gudang= gudang.id_gudang","left")
                                            ->where($where)
                                            ->group_by("stok_opname.id_stok_opname")
                                            ->order_by("stok_opname.created_on","DESC")
                                            ->find_all();
        }else{
            $data = [];
        }
        
        if($data)
        {
            foreach ($data as $key => $dt) {
                $detail = $this->stok_opname_detail_model->select("stok_opname_detail.*, satuan_terkecil.alias as satuan, barcode, nm_barang, nmjenis_bb")
                                            ->join('barang','stok_opname_detail.id_barang = barang.idbarang_bb')
                                            ->join('satuan_terkecil','barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil')
                                            ->join("jenis_bb","barang.idjenis_bb = jenis_bb.idjenis_bb","left")
                                            ->order_by('nmjenis_bb','ASC')
                                            ->order_by('nm_barang','ASC')
                                            ->find_all_by('id_stok_opname', $dt->id_stok_opname);

                $data[$key]->detail = $detail;
            }
        }

        $gudang     = $this->gudang_model->order_by("nama_gudang","ASC")->find_all_by("deleted", 0);
        $nm_gudang  = '';
        if($id_gudang){
            $dt_gudang  = $this->gudang_model->find($id_gudang);  
            $nm_gudang  = $dt_gudang->nama_gudang;
        }
        
        //Identitas
        $identitas = $this->identitas_model->find(1);

        $assets = array('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'revisi_stok/assets/js/laporan_revisi_stok.js');

        add_assets($assets);

        $this->template->set('gudang', $gudang);
        $this->template->set('id_gudang', $id_gudang);
        $this->template->set('nm_gudang', $nm_gudang);
        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('idt', $identitas);
        $this->template->set('period1', $this->input->post('tgl_awal'));
        $this->template->set('period2', $this->input->post('tgl_akhir'));
        $this->template->set("toolbar_title", lang('revisi_stok_laporan_title_view'));
        $this->template->set("page_title", lang('revisi_stok_laporan_title_view'));
        $this->template->page_icon('fa fa-bar-chart');
        $this->template->render('laporan');
    }
     public function pilih($val)
    {
        return $val == "" ? false : true;
    }

    public function laporan_by_value(){

        $this->auth->restrict($this->laporanValuePermission);

        $where  = "stok_opname.deleted = 0";

        $tgl1       = isset($_POST['tgl1']) ? $this->input->post('tgl1') : '';
        $tgl2       = isset($_POST['tgl2']) ? $this->input->post('tgl2') : '';

        $gudang     = isset($_POST['gudang']) ? $this->input->post('gudang') : '';
        $kategori   = isset($_POST['kategori']) ? $this->input->post('kategori') : '';
        $status     = isset($_POST['status']) ? $this->input->post('status') : '';

        if(strlen($tgl1) > 0){

            $tgl1_cr    = date_ymd($tgl1);
            $tgl2_cr    = date_ymd($tgl2);        
             
        }else{

            $tgl1_cr    = date("Y-m-d");
            $tgl2_cr    = date("Y-m-d");

            $tgl1       = date("d/m/Y");
            $tgl2       = date("d/m/Y");

        }
        
        $where      .= " AND (`stok_opname`.`pelaksanaan` >= '{$tgl1_cr}' AND `stok_opname`.`pelaksanaan` <= '{$tgl2_cr}')";

        $filter     =   [
                            'tgl1'      => $tgl1,
                            'tgl2'      => $tgl2,
                            'gudang'    => $gudang,
                            'kategori'  => $kategori,
                            'status'    => $status
                        ];

        if(strlen($kategori) > 0){

            $where  .= " and kategori.idkategori = {$kategori} ";

        }

        if(strlen($gudang) > 0){

            $where  .= " and gudang.id_gudang = {$gudang} ";

        }        

        if(strlen($status) > 0){

            $where  .= " and `stok_opname_detail_hpp`.`st_stok` = {$status} ";

        }        

        $dt_gudang      = $this->gudang_model
                                ->select("`id_gudang` as id, `nama_gudang` as nm")
                                ->where("deleted = 0")
                                ->find_all();

        $dt_kategori    = $this->kategori_model 
                                ->select("`idkategori` as id, `nmkategori` as nm")
                                ->where("deleted = 0")
                                ->order_by("idkategori")
                                ->find_all();

        $data   = $this->stok_opname_model
                        ->select("`gudang`.`nama_gudang`,
                                    `kategori`.`nmkategori`,
                                    `barang`.`nm_barang`,
                                    SUM(`stok_opname_detail_hpp`.`jml`) AS jml_total,
                                    SUM(`stok_opname_detail_hpp`.`jml` * `stok_opname_detail_hpp`.`hpp`) AS value_st_opname,
                                    `stok_opname_detail_hpp`.`st_stok`")
                        ->join("gudang","stok_opname.id_gudang = gudang.id_gudang","inner")
                        ->join("stok_opname_detail_hpp","stok_opname.id_stok_opname = stok_opname_detail_hpp.id_stok_opname","inner")
                        ->join("barang","stok_opname_detail_hpp.id_barang = barang.idbarang_bb","inner")
                        ->join("kategori","barang.idkategori = kategori.idkategori","inner")
                        ->where($where)
                        ->group_by("barang.idbarang_bb")
                        ->group_by("stok_opname_detail_hpp.st_stok")
                        ->order_by("kategori.idkategori","asc")
                        ->order_by("barang.nm_barang","asc")
                        ->find_all();


        $nm_gudang  = "";

        if(is_array($data) && count($data) > 0){

            $nm_gudang  = $data[0]->nama_gudang;

        }

        $asset  =   [
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'revisi_stok/assets/js/laporan_value_so.js'
                    ]; 

        add_assets($asset);
    
        $this->template->set("filter",$filter);
        $this->template->set("dt_gudang",$dt_gudang);        
        $this->template->set("dt_kategori",$dt_kategori); 

        $this->template->set("nm_gudang",$nm_gudang);
        $this->template->set("data",$data);

        $this->template->set("toolbar_title", lang('so_val_laporan_title'));
        $this->template->title(lang('so_val_laporan_title'));
        $this->template->page_icon('fa fa-bar-chart');
        $this->template->render('laporan_value');                 
        
    }

}