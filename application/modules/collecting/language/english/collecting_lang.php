<?php defined('BASEPATH')|| exit('No direct script access allowed');

$lang['title_data']								= 'Data Collecting';
$lang['title_new']								= 'Collecting Baru';
$lang['title_view']								= 'Data Collecting';

// ===================================================================================================================
/* --- Placeholder --- */

$lang['capt-plh-tgl1'] 							= 'Tanggal Awal';
$lang['capt-plh-tgl2'] 							= 'Tanggal Akhir';

// ===================================================================================================================
/* --- View --- */

$lang['capt-view-no-order']						= 'No. Order';
$lang['capt-view-no-faktur']					= 'No. Faktur / Nota';
$lang['capt-view-konsumen']						= 'Konsumen';
$lang['capt-view-selesai-semua']				= "Checklist / Centang pada inputan <b class='text-black'>JML COLLECTING</b> jika dianggap semuanya sudah lengkap";

$lang['capt-view-tb-deskripsi']					= 'Deskripsi';
$lang['capt-view-tb-jml-col']					= 'Jml Collecting';
$lang['capt-view-tb-jml-order']					= 'Qty Cetak';
$lang['capt-view-tb-status']					= 'Status Collecting';

	
	$lang['capt-view-tb-isi-ya']				= 'Sudah';
	$lang['capt-view-tb-isi-batal']				= 'Dibatalkan';



// ===================================================================================================================
/* --- Index --- */

$lang['capt-filter-waktu']						= 'Tanggal Order';
$lang['capt-filter-konsumen']					= 'Konsumen';
$lang['capt-filter-st']							= 'Status Collecting';
	
	$lang['isi-status-belum-komplete']			= 'Belum Lengkap';
	$lang['isi-status-komplete']				= 'Sudah Lengkap';

$lang['capt-filter-no-order']					= 'No. Order';
$lang['capt-filter-no-faktur']					= 'No. Faktur / Nota';
$lang['capt-filter-nm-file']					= 'Nama File Project';

$lang['capt-index-tb-waktu-order']				= 'Waktu Transaksi';
$lang['capt-index-tb-no-faktur']				= 'No. Faktur';
$lang['capt-index-tb-konsumen']					= 'Konsumen';
$lang['capt-index-tb-item-order']				= 'Total Cetakan';
$lang['capt-index-tb-item-colecting']			= 'Total Collecting';
$lang['capt-index-tb-status']					= 'Status';

$lang['capt-index-title-label']					= 'Label Collecting';

// ===================================================================================================================
/* --- Form --- */

$lang['capt-frm-waktu']							= 'Waktu Order';
$lang['capt-frm-no-order']						= 'No. Order';
$lang['capt-frm-no-faktur']						= 'No. Faktur Utama';
$lang['capt-frm-nm-file']						= 'Nama File Project';
$lang['capt-frm-konsumen']						= 'Konsumen';
$lang['capt-frm-produk']						= 'Produk';
$lang['capt-frm-kategori']						= 'Kategori';

$lang['capt-frm-tb-deskripsi']					= 'Deskripsi';
$lang['capt-frm-tb-dicollecting']				= 'Sisa';
$lang['capt-frm-tb-collecting']					= 'Qty Collecting';

$lang['capt-image-null']						= 'Gambar Tidak Ditemukan';

$lang['capt-info-1']							= 'Centang Data Produk yang akan dicollecting barangnya.';
$lang['capt-info-2']							= 'Data yang dicentang akan dianggap telah terpenuhi semua Qty Cetaknya.';

$lang['capt-subject-email']						= 'Order Selesai';

// ===================================================================================================================
$lang['btn_create']								= 'Baru';
$lang['btn_delete']								= 'Hapus';
$lang['btn_filter']								= 'Filter';

// ===================================================================================================================
$lang['btn-list-order']							= 'List Order';
$lang['btn-save']								= "Simpan";
$lang['btn-batal']								= "Batal / Kembali";
$lang['btn-delete']								= "Hapus";
$lang['btn-filter']								= "Filter";
$lang['btn-reset']								= "batal / reset";
$lang['btn-kembali']							= "Kembali";

$lang['bf_or']									= "or";



// ===================================================================================================================
$lang['label_simpan_sukses'] 					= "Simpan Data Jml Collecting sukses";
$lang['label_update_sukses'] 					= "Simpan Update Data Collecting sukses";
$lang['label_cancel_sukses']					= "Data Collecting telah berhasil dibatalkan";

// =======================================================================================================================================================================================================
$lang['err_akses_delete']						= "Anda tidak memiliki hak akses untuk MENGHAPUS data";
$lang['sukses_delete']							= "Delete data Collecting sukses";
$lang['err_delete'] 							= "Simpan JML Collecting gagal, Error SQL Script";
$lang['err_hilang_delete'] 						= "Data Collecting yang akan Anda hapus tidak ditemukan.<br><br>Refresh halaman untuk memperbaharui data";

$lang['err_data_tidak_ada'] 					= "Cek kembali data Transaksi, pastikan Transaksi tidak dalam kondisi menggantung";

// =======================================================================================================================================================================================================
$lang['err-akses-tambah']						= "Anda tidak memiliki hak akses untuk MENAMBAH data";
$lang['err-list-order-kosong']					= "Simpan JML Collecting gagal, karena data LIST ORDER tidak ditemukan.<br><br>Refresh halaman untuk update data";
$lang['err-jml-kosong']							= "JML Collecting harus lebih(>) dari 0";
$lang['err-jml-lebih']							= "JML Collecting melebihi JML SISA terbaru.<br><br> Refresh Halaman untuk update data";
$lang['err-simpan']								= "Simpan JML Collecting gagal, Error SQL Script";

// =======================================================================================================================================================================================================
$lang['konfirmasi-tidak-ada-perubahan']			= "Tidak ada perubahan pada Data Collecting";
$lang['konfirmasi-delete']						= "Apakah anda yakin akan menghapus Data Collecting terpilih ? ";
$lang['konfirmasi-data-tidak-ada']				= "Data tidak ditemukan";
$lang['konfirmasi-error-pil-delete']			= "Tidak ada data Pelunasan Collecting yang dipilih";
$lang['konfirmasi-delete-sukses']				= "Delete data Pelunasan Collecting sukses";



