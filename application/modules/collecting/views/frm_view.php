<?php 
	
	if(isset($data) && is_array($data) && count($data)):

?>

<?= form_open($this->uri->uri_string(),[
										'name' 	=> "frm_view",
										'id'	=> "frm_view"
										]); ?>	
<div class="box box-primary">

	<div class="box-header">
		<a href="<?= site_url("collecting") ?>" title='Kembali' class="btn btn-primary" >
			<span class="fa fa-long-arrow-left"> Kembali</span>
		</a>
	</div>

	<div class="box-body">
		
		

		<div class="row">
			
			<div class="col-sm-3">
				<div class="form-group">
					<label><?= lang("capt-index-tb-no-faktur") ?></label>	
					<span class="form-control input-sm"><?= $data['no_nota'] ?></span>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<label><?= lang("capt-index-tb-waktu-order") ?></label>	
					<span class="form-control input-sm"><?= $data['waktu'] ?></span>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<label><?= lang("capt-index-tb-konsumen") ?></label>	
					<span class="form-control input-sm"><?= ucwords($data['konsumen']) ?></span>
				</div>
			</div>

		</div>
	
		<div class="table-responsive">
			
			<table class="table">
				
				<thead>
					<tr>
						<tr class="success">
							<th class="text-center" width="30">#</th>
							<th colspan="2"><?= lang('capt-frm-tb-deskripsi') ?></th>
							<th colspan="3" class="text-center"><?= lang('capt-view-tb-jml-col') ?></th>
						</tr>					
					</tr>
				</thead>	

				<tbody>
					<?php 
							foreach($data['detail'] as $key => $isi) :

								$brs_kol 	= count($data['detail'][$key]['det_kol']);

								$ttl_cek_col = 0;
							
								foreach ($data['detail'][$key]['det_kol'] as $key_cek => $cek) :
								
									if(!is_null($cek['id_kol'])) :

										$ttl_cek_col += $cek['jml_kol'];

									endif;

								endforeach;
					?>
					
					<tr>
						<td class="text-center">
							<?php if($ttl_cek_col < $isi['jml_cetak']): ?>
								<input type="hidden" name="dft_id_detail_order[]" id="dft_id_detail_order" value="<?= $isi['id_det_order'] ?>">
							<?php endif; ?>
							<?= $key+1 ?>
						</td>
						<td class="text-center thumb-daftar table-bordered" style="width: 10%">					
							<img src="<?= base_url().$isi['thumb'] ?>" class="img-rounded" alt="<?= lang('capt-image-null') ?>">
						</td>
						<td class="deskripsi">
							<p class="nama-pekerjaan"><?= ucwords($isi['nm_project']) ?></p>
							<p><?= strtoupper($isi['kat_pro']) ?></p>
							<?php 
									if($isi['st_pro'] == 1){

										echo $isi['ukuran'];

									}
							?>
							<p><?= $isi['finishing'] ?></p>
							<p>
								<b>Order Cetak : <?= number_format($isi['jml_cetak']) ?></b>
							</p>
						</td>
						<td colspan="3">

							<?php if($ttl_cek_col < $isi['jml_cetak']): ?>

							<div class="input-group">
								<input type="text" name="jml_kol[]" id="jml_kol" class="form-control">
								<span class="input-group-addon">

									<?php

										$value 	= $isi['id_det_order'];

									?>

									<input type="checkbox" onclick="centang(this)" value="<?= $value ?>" name="pil_semua[]" class="checkbox">

								</span>
							</div>

							<?php endif; ?>
						</td>
						
					</tr>

					<?php  
							$ttl_cetak = 0;
							
							foreach ($data['detail'][$key]['det_kol'] as $key => $det) :
							
								if(!is_null($det['id_kol'])) :

								$ttl_cetak += $det['jml_kol'];
					?>
					
					<tr>
						<td>
							<input type="hidden" name="dft_idkolekting[]" id="dft_idkolekting" value="<?= $det['id_kol'] ?>">
							<input type="hidden" name="dft_iduniv[]" id="dft_iduniv" value="<?= $det['id_univ'] ?>">
						&nbsp;
						</td>
						<td>&nbsp;</td>
						<td class="text-right"><?= $det['waktu_kol'] ?></td>
						<td class="text-center"><?= number_format($det['jml_kol']) ?></td>
						<td class="text-center" width="120"><?= $det['admin'] ?></td>
						<td class="text-center">
							
							<a href="#delete_kolekting" onclick="konfirm_del(this);" title="hapus" class="text-red">
								<span class="fa fa-trash"></span>
							</a>
						</td>
					</tr>

					<?php 
									if($key == ($brs_kol-1)):	
					?>

					<tr>
						<td colspan="3" class="text-right bg-primary">Total Kolekting</td>
						<td class="text-center">
							<b class="text-blue"><?= number_format($ttl_cetak) ?></b>
						</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<?php 
									endif;
								endif;
							endforeach;
						endforeach;
					?>
				</tbody>

			</table>

		</div>
		<div class="pull-right">
			<?php echo anchor('collecting', lang('btn-batal'))." ".lang("bf_or")." " ?>
			<button type="submit" name="simpan" title="Simpan Data Collecting" class="btn btn-primary ">
				<?= lang("btn-save") ?>
			</button>
		</div>

	</div>
	<div class="callout callout-success">
		<strong><span class="fa fa-check-square-o"></span> </strong><?= lang("capt-view-selesai-semua") ?>
	</div>
	
</div>	

<?php 
	
	endif;

	form_close();

?>

