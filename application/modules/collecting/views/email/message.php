<p>Selamat <?= $full_name ?>,
    <br>
    Order Anda,
    <br>
    No. Order   : <strong><?= $no_order ?></strong>,
    <br>
    No. Faktur  : <strong><?= $no_faktur ?></strong>,
    <br>
    Telah selesai diproduksi.
    <br>
    Silahkan menghubungi Admin kami untuk perihal pengambilan, atau Anda dapat mengunjungi kantor kami untuk pengambilan barang.
    
    <?php if($st_reseller == 1): ?>    
    <br>
    <br>
    Untuk Anda <strong>Reseller</strong> , kini Anda dapat langsung Order tanpa harus datang ke lokasi.
    <br>
    Hubungi Admin kami di no. <strong><?= $no_telp ?></strong>, untuk mendapatkan bantuan mengenai Produk - Produk kami.
    <br>
    <strong>Daftarkan Diri Anda Sekarang Juga</strong>
    <br>
    <a href="<?= site_url('register') ?>" target="_blank" >Klik Disini</a>
    <?php endif; ?>    
    <br>
    <br>
    Terima Kasih.
</p>