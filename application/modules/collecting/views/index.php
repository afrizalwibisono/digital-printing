<?php 
		
	$ENABLE_ADD 	= has_permission("Collecting Order.Add");
	$ENABLE_MANAGE 	= has_permission("Collecting Order.Manage");
	$ENABLE_DELETE 	= has_permission("Collecting Order.Delete");

	echo form_open($this->uri->uri_string(),["name" => "frm_index", "id" => "frm_index","role" => "index"]); 
?>

<div class="box box-primary">
	<div class="box-header">

		<div class="row">
			
			<div class="form-horizontal">

				<div class="col-sm-6">

					<div class="form-group" style="margin-bottom: 5px">
						<label for="tgl1" class="col-sm-4 control-label"><?= lang("capt-filter-waktu") ?></label>
						<div class="col-sm-8">
							<div class="input-group input-daterange" >
								<input type="text" name="tgl1" id="tgl1" class="form-control" readonly placeholder="<?= lang('capt-plh-tgl1') ?>" value = "<?= set_value('tgl1',isset($tgl1) ? $tgl1 : '') ?>" >
								<span class="input-group-addon">to</span>
								<input type="text" name="tgl2" id="tgl2" class="form-control" readonly placeholder="<?= lang('capt-plh-tgl2') ?>" value = "<?= set_value('tgl2',isset($tgl2) ? $tgl2 : '') ?>" >
							</div>
						</div>
					</div>

					<div class="form-group" style="margin-bottom: 5px">
						<label for="konsumen" class="col-sm-4 control-label" ><?= lang("capt-filter-konsumen") ?></label>
						<div class="col-sm-8">
							<select class="form-control" name="konsumen" id="konsumen">
								<option></option>
								<?php 
										if(isset($dt_konsumen) && is_array($dt_konsumen) && count($dt_konsumen)):
											foreach ($dt_konsumen as $key => $isi) :
												
								?>

										<option value="<?= $isi->id ?>" <?= set_select('konsumen',$isi->id, isset($konsumen) && $konsumen == $isi->id) ?> ><?= ucwords($isi->nm) ?></option>

								<?php
											endforeach;
										endif;
								?>
							</select>
						</div>	
					</div>

					<!-- <div class="form-group" style="margin-bottom: 5px">
						
						<label class="control-label col-sm-4" for="status"><?= lang("capt-filter-st") ?></label>
						<div class="col-sm-8">
							
							<select class="form-control" name="status" id="status">
								<option></option>
								<option value="0" <?= set_select("status","0", isset($status) && $status == "0" ) ?> ><?= lang('isi-status-belum-komplete') ?></option>
								<option value="1" <?= set_select("status","1", isset($status) && $status == "1" ) ?>><?= lang('isi-status-komplete') ?></option>
							</select>

						</div>

					</div> -->

				</div>

				<div class="col-sm-6">
					
					<div class="form-group" style="margin-bottom: 5px">
						<label class="control-label col-sm-4" for="no_faktur"><?= lang("capt-filter-no-faktur") ?></label>
						<div class="col-sm-8">
							<input type="text" name="no_faktur" id="no_faktur" value="<?= set_value("no_faktur", isset($no_faktur) ? $no_faktur : '') ?>" class="form-control">
						</div>
					</div>	

					<div class="form-group" style="margin-bottom: 5px">
						
						<label class="control-label col-sm-4" for="status"><?= lang("capt-filter-st") ?></label>
						<div class="col-sm-8">
							
							<select class="form-control" name="status" id="status">
								<option></option>
								<option value="0" <?= set_select("status","0", isset($status) && $status == "0" ) ?> ><?= lang('isi-status-belum-komplete') ?></option>
								<option value="1" <?= set_select("status","1", isset($status) && $status == "1" ) ?>><?= lang('isi-status-komplete') ?></option>
							</select>

						</div>

					</div>
					

				</div>

			</div>

		</div>

		<hr style="margin-bottom: 10px; margin-top: 10px">

		<?php if ($ENABLE_ADD) : ?> 
        	<a href="<?= site_url('collecting/create') ?>" class="btn btn-success" title="<?= lang('btn-list-order') ?>"><?= lang('btn-list-order') ?></a> 
        <?php endif; ?> 

        <div class="pull-right">

			<button class="btn btn-primary" tipe="submit">
				<?= lang("btn_filter") ?> <span class="fa fa-search"></span>
			</button>
			
		</div>

		
	</div>
	<?php

		//print_r($data);

		if(isset($data) && is_array($data) && count($data)):

	?>
	<div class="box-body table-responsive">
		
		<table class="table table-hover table-striped" id="daftar">
			<thead>
				<tr>
					<th class="text-center" width="30">#</th>
					<th class="text-center"><?= lang("capt-index-tb-waktu-order") ?></th>
					<th class="text-center"><?= lang("capt-index-tb-no-faktur") ?></th>
					<th class="text-center"><?= lang("capt-index-tb-konsumen") ?></th>
					<th class="text-center"><?= lang("capt-index-tb-item-order") ?></th>
					<th class="text-center"><?= lang("capt-index-tb-item-colecting") ?></th>
					<th class="text-center"><?= lang("capt-index-tb-status") ?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php  
						foreach ($data as $key => $dt) :
				?>

					<tr>
						<td class="text-center">
							<input type="hidden" name="id_transaksi[]" value="<?= $dt->id_transaksi ?>">
							<?= $numb ?>
						</td>
						<td class="text-center"><?= $dt->tgl ?></td>
						<td class="text-center">
							<a href="<?= site_url('collecting/view/' . $dt->id_transaksi); ?>" class="text-blue" data-toggle="tooltip" data-placement="left" title="Lihat Data Order">
							<strong><?= $dt->no_faktur ?></strong>
							</a>
						</td>
						<td class="text-center"><?= ucwords($dt->nm_konsumen) ?></td>
						<td class="text-center"><?= $dt->total_cetak ?></td>
						<td class="text-center"><?= $dt->total_sudah_cetak ?></td>
						<?php 
								$st = 0;

								if($dt->total_sudah_cetak < $dt->total_cetak):

									$label 	= "<span class='label label-danger'>". lang('isi-status-belum-komplete'). "</span>";
						
								else:

									$st 	= 1;
									$label 	= "<span class='label label-success'>". lang('isi-status-komplete'). "</span>";

								endif;	
						?>
						<td class="text-center"><?= $label ?></td>
						<td class="text-right">
							
							<?php if($ENABLE_MANAGE && $st == 0): ?>
								<a class="text-green" onclick="konfirmasi_bypass(this);" href="#lengkap" data-toggle="tooltip" data-placement="left" title="Sudah Lengkap Semua" >
									<span class="fa fa-check-square-o"></span>
								</a>
								&nbsp;|&nbsp;
							<?php endif; ?>

							<a class="text-black" href="<?= site_url('collecting/view/' . $dt->id_transaksi); ?>" data-toggle="tooltip" data-placement="left" title="Lihat Data Order"><i class="fa  fa-folder-open"></i></a> 

						</td>
					</tr>		

				<?php 
							$numb++;
						endforeach;
				?>

			</tbody>
		</table>

	</div>
	<div class="box-footer clearfix">
		<?php echo $this->pagination->create_links(); ?>
	</div>
	<?php

		else:

	?>

		<div class="alert alert-info" role="alert">
        	<p><i class="fa fa-warning"></i> &nbsp; <?= lang('konfirmasi-data-tidak-ada') ?></p>
    	</div>

	<?php

		endif;

	?>
</div>


<?= form_close() ?>