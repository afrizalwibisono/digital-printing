<?= form_open($this->uri->uri_string(),["name" => "frm_baru", "id" => "frm_baru","role" => "index"]) ?>

<div class="box box-primary">
	
	<div class="box-header">

		<div class="row">
			
			<div class="form-horizontal">

				<div class="col-sm-6">

					<div class="form-group" style="margin-bottom: 5px">
						<label for="tgl1" class="col-sm-4 control-label"><?= lang("capt-frm-waktu") ?></label>
						<div class="col-sm-8">
							<div class="input-group input-daterange" >
								<input type="text" name="tgl1" id="tgl1" class="form-control" readonly placeholder="<?= lang('capt-plh-tgl1') ?>" value = "<?= set_value('tgl1',isset($tgl1) ? $tgl1 : '') ?>" >
								<span class="input-group-addon">to</span>
								<input type="text" name="tgl2" id="tgl2" class="form-control" readonly placeholder="<?= lang('capt-plh-tgl2') ?>" value = "<?= set_value('tgl2',isset($tgl2) ? $tgl2 : '') ?>" >
							</div>
						</div>
					</div>

					<div class="form-group" style="margin-bottom: 5px">
						<label class="control-label col-sm-4" for="no_faktur"><?= lang("capt-frm-no-faktur") ?></label>
						<div class="col-sm-8">
							<input type="text" name="no_faktur" id="no_faktur" class="form-control" value="<?= set_value("no_faktur",isset($no_faktur) ? $no_faktur : '' ) ?>">
						</div>
					</div>	

					<div class="form-group" style="margin-bottom: 5px">
						<label for="konsumen" class="col-sm-4 control-label" ><?= lang("capt-frm-konsumen") ?></label>
						<div class="col-sm-8">
							<select class="form-control" name="konsumen" id="konsumen">
								<option></option>
								<?php 
										if(isset($dt_konsumen) && is_array($dt_konsumen) && count($dt_konsumen)):
											foreach ($dt_konsumen as $key => $isi) :
												
								?>

										<option value="<?= $isi->id ?>" <?= set_select('konsumen',$isi->id, isset($id_konsumen) && $id_konsumen == $isi->id) ?> ><?= ucwords($isi->nm) ?></option>

								<?php
											endforeach;
										endif;
								?>
							</select>
						</div>	
					</div>

				</div>

				<div class="col-sm-6">

					<div class="form-group" style="margin-bottom: 5px">
						<label class="control-label col-sm-4" for="nm_project"><?= lang("capt-frm-nm-file") ?></label>
						<div class="col-sm-8">
							<input type="text" name="nm_project" id="nm_project" class="form-control" value="<?= set_value("nm_project", isset($nm_file) ? $nm_file : '') ?>">
						</div>
					</div>	

					<div class="form-group" style="margin-bottom: 5px">
						<label class="control-label col-sm-4" for="kategori"><?= lang("capt-frm-kategori") ?></label>
						<div class="col-sm-8">
							<select class="form-control" name="kategori" id="kategori">
								<option></option>
								<?php 

									if(isset($dt_kategori) && is_array($dt_kategori) && count($dt_kategori)) :
										foreach ($dt_kategori as $key => $kat) :
										
								?>
											<option value="<?= $kat->id ?>" <?= set_select('kategori',$kat->id,isset($kategori) && $kategori == $kat->id) ?> >
												<?= strtoupper($kat->nm) ?>
											</option>

								<?php 
										endforeach;
									endif; 
								?>
							</select>
						</div>
					</div>						

				</div>

			</div>

		</div>

		<hr style="margin-bottom: 10px; margin-top: 10px">

		<a href="<?= site_url('collecting') ?>" title="Kembali ke Index" class="btn btn-default" >
			<span class="fa fa-long-arrow-left"> <?= lang("btn-kembali") ?></span>
		</a>

        <div class="pull-right">

			<button class="btn btn-primary" name="filter">
				<?= lang("btn_filter") ?> <span class="fa fa-search"></span>
			</button>
			
		</div>

		
	</div>

	<?php if(isset($data) && is_array($data) && count($data)): ?>

	<div class="box-body table-responsive">

		<table class="table table-bordered" id="daftar_">
			
			<thead>
				<tr class="success">
					<th class="text-center" width="30">#</th>
					<th colspan="2"><?= lang('capt-frm-tb-deskripsi') ?></th>
					<th class="text-center"><?= lang('capt-frm-tb-dicollecting') ?></th>
					<th class="text-center"><?= lang('capt-frm-tb-collecting') ?></th>
				</tr>
			</thead>

			<tbody>
				<?php

					if(isset($data) && is_array($data) && count($data)):

						foreach ($data as $key => $isi):
							$numb += 1;

				?>

					<tr>
						<td class="text-center">
							<?= $numb ?>
							<input type="hidden" name="dft_iddetail_order[]" 		id="dft_iddetail_order" 	value="<?= $isi->id_detail_produk_order ?>">
							<input type="hidden" name="dft_iddetail_universal[]" 	id="dft_iddetail_universal" value="<?= $isi->kode_universal ?>">
							<input type="hidden" name="dft_idtrans_master[]" 		id="dft_idtrans_master" 	value="<?= $isi->id_trans_ref_asli ?>">
						</td>
						<td class="text-center thumb-daftar" style="width: 10%">					
							<img src="<?= base_url().$isi->thumbnail ?>" class="img-rounded" alt="<?= lang('capt-image-null') ?>">
						</td>
						<td class="deskripsi">
							<?php

									// ------------ samakan spasi ------------
									$arr_var 	= 	[
														'var0'	=> 'No. Faktur',
														'var1'	=> 'Konsumen'
													];

							?>
							<p class="nama-pekerjaan text-info">No. Faktur : <?= $isi->no_faktur ?></p>
							<p class="nama-pekerjaan text-info">Konsumen : <span class="text-danger"> <?= ucwords($isi->nm_konsumen)?> </span> </p>
							<p class="nama-pekerjaan"><?= ucwords($isi->nama_pekerjaan) ?></p>
							<p><?= ucwords($isi->nmkategori)." - ".ucwords($isi->nm_produk) ?></p>
							<?php 
									if($isi->st_tipe_produk == 1){

										echo "<p>".$isi->p." x ".$isi->l."(".$isi->tampil2.")</p>";

									}
							?>
							<p><?= $isi->st_finishing ?></p>
							<p>
								<b>Order Cetak : <?= number_format($isi->jml_cetak) ?></b>
							</p>
						</td>
						<td class="text-center">

							<?php 

								if($isi->jml_kolekting > 0){

									echo number_format($isi->selisih);
										
								}else{

									echo number_format($isi->jml_cetak);

								}

							?>

						</td>

						<td class="deskripsi text-center">

							<div class="input-group">
								<input type="text" name="dft_jml_kolekting[]" id="dft_jml_kolekting" class="form-control input-sm">
								<div class="input-group-btn">
									<button onclick="simpan_data(this);" type="button" class="btn btn-success btn-sm" title="Simpan Qty Kolekting">
										<span class="fa fa-save"></span>									
									</button>	
								</div>
								<div class="input-group-btn">
									<button onclick="selesai_semua(this);" type="button" class="btn btn-info btn-sm" title="Selesai semua">
										<span class="fa fa-check"></span>									
									</button>	
								</div>
							</div>


							<?php 

								if($isi->jml_kolekting > 0){

									echo "<hr style='margin-top:15px;margin-bottom:2px'>";
									echo "<p><b>".number_format($isi->jml_kolekting)."</b>, [ <span class='text-blue'>{$isi->tgl_akhir}</span>  ]</p>";
									
								}
							
							?>
						</td>
						
					</tr>

				<?php

						endforeach;

					endif;

				?>
			</tbody>

		</table>

	</div>
	<div class="box-footer clearfix">
		<?php echo $this->pagination->create_links(); ?>	
	</div>

	<?php else : ?>

		<div class="alert alert-info" role="alert">
	    	<p><i class="fa fa-warning"></i> &nbsp; <?= lang('konfirmasi-data-tidak-ada') ?></p>
		</div>

	<?php endif; ?>
	

</div>

<?= form_close() ?>