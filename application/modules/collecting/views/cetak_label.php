<table class="struk-kecil-58">
	<thead>
		<tr>
			<th colspan="3" class="barcode">
				<img class="img-responsive" src="data:image/png;base64,<?= $symbol_barcode ?>">
				<span>[ <?= $kode_barcode ?> ]</span>
			</th>
		</tr>
		<tr>
			<th width="70">Konsumen</th>
			<th colspan="2"><?= ucwords($data[0]->nm) ?></th>
		</tr>
		<tr>
			<th>No. Order</th>
			<th colspan="2"><?= $data[0]->no_transaksi ?></th>
		</tr>
		<tr>
			<th>No. Faktur</th>
			<th colspan="2"><?= $data[0]->no_faktur ?></th>
		</tr>
	</thead>
	<tbody>
		<?php

			foreach ($data as $key => $dt):

		?>
		<tr>
			<td colspan="3"> <strong> <?= ucwords($dt->nama_pekerjaan) ?> </strong></td>
		</tr>
		<tr>
			<td colspan="2" ><?= substr(ucwords($dt->nm_produk), 0,25)  ?></td>
			<td><?= $dt->jml_cetak ?></td>
		</tr>


		<?php

			endforeach;

		?>
		
	</tbody>
</table>
<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function(e){
		window.print();
		window.close();
	});
</script>