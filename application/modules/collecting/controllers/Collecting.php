<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2018, Cokeshome
 * 
 */

class Collecting extends Admin_Controller {
    
    protected $viewPermission       = "Collecting Order.View";
    protected $addPermission        = "Collecting Order.Add";
    protected $managePermission     = "Collecting Order.Manage";
    protected $deletePermission     = "Collecting Order.Delete";

    protected $prefix_key           = "CLO";

    
    public function __construct(){
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('collecting/collecting');
        $this->load->model([
                                'konsumen_model',
                                'order_model',
                                'order_detail_model',
                                'trans_model',
                                'kolekting_model',
                                'spk_model',
                                'kategori_model'
                            ]);

        $this->template->title(lang('title_data'));
		$this->template->page_icon('fa fa-list');
    }

    public function force(){

        $data_order     = $this->order_model
                                ->select("`m_order_produk_detail`.`id_detail_produk_order`,
                                            `m_order_produk_detail`.`kode_universal`,
                                            `m_order_produk_detail`.`jml_cetak`")
                                ->join("m_order_produk_detail","order_produk.id_order = m_order_produk_detail.id_order","inner")
                                ->where("order_produk.st_simpan = 1
                                            AND order_produk.deleted = 0
                                            AND order_produk.st_kasir = 1
                                            AND m_order_produk_detail.deleted = 0
                                            AND (order_produk.tgl_order >= '2018-10-01'
                                            AND order_produk.tgl_order <= '2018-11-31')")
                                ->find_all();

        $this->db->trans_start();

        foreach ($data_order as $key => $isi) {
            
            $arr    =   [
                            'idkolekting'               => gen_primary("fr","kolekting","idkolekting"),  
                            'id_detail_produk_order'    => $isi->id_detail_produk_order, 
                            'kode_universal'            => $isi->kode_universal, 
                            'jml'                       => $isi->jml_cetak,
                            'st_bypass'                 => 1
                        ];            

            $this->kolekting_model->insert($arr);
            set_proses_order($id_lokasi = 3, $id_ket_proses = 6, $id_detail_produk_order = $isi->id_detail_produk_order, $jumlah_proses = $isi->jml_cetak);            

        }

        $this->db->trans_complete();

    }

    public function index(){

        $this->auth->restrict($this->viewPermission);

        //$this->force();

        // Pagination
        $this->load->library('pagination');

        $where  = "transaksi.deleted = 0
                    AND transaksi.st_history = 0";

        $filter = "?cl";        

        if(isset($_POST['tgl1'])){

            $tgl1       = isset($_POST['tgl1']) ? $this->input->post('tgl1') : '';
            $tgl2       = isset($_POST['tgl2']) ? $this->input->post('tgl2') : '';

            $konsumen   = isset($_POST['konsumen']) ? $this->input->post('konsumen') : '';
            $no_faktur  = isset($_POST['no_faktur']) ? $this->input->post('no_faktur') : '';
            $status     = isset($_POST['status']) ? $this->input->post('status') : '';

        }else if(isset($_GET['tgl1'])){

            $tgl1       = isset($_GET['tgl1']) ? $this->input->get('tgl1') : '';
            $tgl2       = isset($_GET['tgl2']) ? $this->input->get('tgl2') : '';

            $konsumen   = isset($_GET['konsumen']) ? $this->input->get('konsumen') : '';
            $no_faktur  = isset($_GET['no_faktur']) ? $this->input->get('no_faktur') : '';
            $status     = isset($_GET['status']) ? $this->input->get('status') : '';

        }else{

            //$tgl1       = date('d/m/Y', strtotime("-30 days"));
            $tgl1       = date('d/m/Y');
            $tgl2       = date('d/m/Y');

            $konsumen   = '';
            $no_faktur  = '';

            $status     = '0';

        }


        if(strlen($tgl1)>0){

            $tgl1_cek   = date_ymd($tgl1)." 01:00:00";
            $tgl2_cek   = date_ymd($tgl2)." 23:59:00";

            $where  .= " AND (`transaksi`.`waktu` >= '{$tgl1_cek}'
                        AND `transaksi`.`waktu` <= '{$tgl2_cek}')";

            $filter .= "&tgl1={$tgl1}&tgl2={$tgl2}";

        }

        if(strlen($konsumen)>0){

            $where  .= " AND `order_produk`.`id_konsumen` = '{$konsumen}'";
            $filter .= "&konsumen={$konsumen}";

        }

        if(strlen($no_faktur)>0){

            $where      = "`transaksi`.`no_faktur` = '{$no_faktur}'";
            $filter     = "?cl&no_faktur={$no_faktur}";

        }

        if(strlen($status) > 0){

            if($status > 0){

                $having = "selisih <= 0";
            
            }else{

                $having = "selisih > 0";    

            }

            $filter .= "&status={$status}";            

        }

        $total  =   $this->trans_model
                        ->select("SUM(`m_order_produk_detail`.`jml_cetak`) - IF(SUM(t_kol.total) IS NULL,
                                        0,
                                        SUM(t_kol.total)) AS selisih") 
                        ->join("`order_produk`","`transaksi`.`id_order` = `order_produk`.`id_order`","inner")
                        ->join("`konsumen`","`konsumen`.`idkonsumen` = `order_produk`.`id_konsumen`","inner")
                        ->join("`transaksi_detail`","`transaksi_detail`.`id_transaksi` = `transaksi`.`id_transaksi`","inner")
                        ->join("`m_order_produk_detail`","`m_order_produk_detail`.`id_detail_produk_order` = `transaksi_detail`.`id_detail_produk_order`","inner")
                        ->join("(SELECT 
                                        `kolekting`.`kode_universal` AS id,
                                            SUM(`kolekting`.`jml`) AS total
                                    FROM
                                        kolekting
                                    WHERE
                                        deleted = 0
                                    GROUP BY kode_universal) AS t_kol","m_order_produk_detail.kode_universal = t_kol.id","left")
                        ->where($where)
                        ->group_by("transaksi.id_transaksi");
                        if(strlen($status) > 0){
                            $total = $total->having($having);    
                        }
                        $total = $total->count_all();

        $offset = $this->input->get('per_page');

        $limit  = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);
                            
        $data   =   $this->trans_model
                            ->select("`transaksi`.`id_transaksi`,
                                        date_format(`transaksi`.`waktu`,'%d/%m/%Y %H:%i:%s') as tgl,
                                        `transaksi`.`no_faktur`,
                                        `konsumen`.`st`,
                                        IF(LENGTH(`konsumen`.`panggilan`) > 0,
                                            CONCAT(`konsumen`.`panggilan`,
                                                    ' ',
                                                    `konsumen`.`nama`),
                                            `konsumen`.`nama`) AS nm_konsumen,
                                        SUM(`m_order_produk_detail`.`jml_cetak`) AS total_cetak,
                                        SUM(t_kol.total) AS total_sudah_cetak,
                                        SUM(`m_order_produk_detail`.`jml_cetak`) - IF(SUM(t_kol.total) IS NULL,
                                        0,
                                        SUM(t_kol.total)) AS selisih")
                            ->join("`order_produk`","`transaksi`.`id_order` = `order_produk`.`id_order`","inner")
                            ->join("`konsumen`","`konsumen`.`idkonsumen` = `order_produk`.`id_konsumen`","inner")
                            ->join("`transaksi_detail`","`transaksi_detail`.`id_transaksi` = `transaksi`.`id_transaksi`","inner")
                            ->join("`m_order_produk_detail`","`m_order_produk_detail`.`id_detail_produk_order` = `transaksi_detail`.`id_detail_produk_order`","inner")
                            ->join("(SELECT 
                                            `kolekting`.`kode_universal` AS id,
                                                SUM(`kolekting`.`jml`) AS total
                                        FROM
                                            kolekting
                                        WHERE
                                            deleted = 0
                                        GROUP BY kode_universal) AS t_kol","m_order_produk_detail.kode_universal = t_kol.id","left")
                            ->where($where)
                            ->group_by("transaksi.id_transaksi")
                            ->order_by("`transaksi`.`waktu`");
                            if(strlen($status) > 0){
                                $data = $data->having($having);    
                            }
                            $data = $data->limit($limit,$offset)
                            ->find_all();

        $data_konsumen  = $this->konsumen_model
                                ->select("`idkonsumen` as id, 
                                        CONCAT(IF(panggilan IS NULL, '', panggilan),
                                        ' ',
                                        `nama`,
                                        ' | ',
                                        IF(st = 0,
                                            'konsumen',
                                            IF(st = 1, 'reseller', 'instansi'))) AS nm")
                                ->where("deleted=0")
                                ->order_by("nama","asc")
                                ->find_all();


        $asset  =   [
                        "plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
                        "plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
                        "plugins/select2/dist/css/select2.min.css",
                        "plugins/select2/dist/js/select2.min.js",
                        "collecting/assets/js/index_collecting.js"
                    ];

        add_assets($asset);

        $this->template->set("numb",$offset+1);

        $this->template->set("tgl1",$tgl1);
        $this->template->set("tgl2",$tgl2);

        $this->template->set("no_faktur",$no_faktur);
        $this->template->set("konsumen",$konsumen);        

        $this->template->set("status",$status);                

        $this->template->set("data",$data);
        $this->template->set("dt_konsumen",$data_konsumen);

        $this->template->title(lang('title_data'));
        $this->template->set("toolbar_title", lang('title_data'));
        $this->template->set("page_title", lang('title_data'));
        $this->template->page_icon('fa fa-list');
        $this->template->render('index'); 
    }

    public function dianggap_lengkap(){

        if(!$this->input->is_ajax_request()){

            redirect("collecting");

        }

        $id     = $this->input->post("id");

        $cek_selisih    = $this->trans_model
                                ->select("`transaksi`.`id_transaksi`,
                                            `m_order_produk_detail`.`id_detail_produk_order`,
                                            `m_order_produk_detail`.`kode_universal`,
                                            `m_order_produk_detail`.`jml_cetak`,
                                            `t_kol`.`jml_kolekting`,
                                            (`m_order_produk_detail`.`jml_cetak` - IF(`t_kol`.`jml_kolekting` IS NULL,
                                                0,
                                                `t_kol`.`jml_kolekting`)) AS selisih")
                                ->join("`transaksi_detail`","`transaksi_detail`.`id_transaksi` = `transaksi`.`id_transaksi`","inner")
                                ->join("`m_order_produk_detail`","`m_order_produk_detail`.`id_detail_produk_order` = `transaksi_detail`.`id_detail_produk_order`","inner")
                                ->join("(SELECT 
                                            `kolekting`.`kode_universal` AS id,
                                                SUM(`jml`) AS jml_kolekting,
                                                DATE_FORMAT(MAX(created_on), '%d/%m/%Y %H:%i:%s') AS tgl_akhir
                                        FROM
                                            kolekting
                                        WHERE
                                            deleted = 0
                                        GROUP BY `kolekting`.`kode_universal`) AS t_kol","`t_kol`.`id` = `m_order_produk_detail`.`kode_universal`","left")
                                ->where("`transaksi`.`id_transaksi` = '{$id}'")
                                ->having('selisih > 0')
                                ->find_all();        

        $this->db->trans_start();

        foreach ($cek_selisih as $key => $isi) {
            
            // simpan data kolekting
            $arr_insert     =   [
                                    'idkolekting'               => gen_primary("","kolekting","idkolekting"), 
                                    'id_detail_produk_order'    => $isi->id_detail_produk_order, 
                                    'kode_universal'            => $isi->kode_universal, 
                                    'jml'                       => $isi->selisih, 
                                    'st_bypass'                 => '1'
                                ]; 

            $this->kolekting_model->insert($arr_insert);
            set_proses_order($id_lokasi = 3, $id_ket_proses = 6, $id_detail_produk_order = $isi->id_detail_produk_order, $jumlah_proses = $isi->selisih);

            // update spk selesai
            $arr_spk    = ['st_selesai' => 1, 'st_ambil' => 1];
            $where  = "`kode_universal` = '{$isi->kode_universal}'";
            $this->spk_model->update_where($where,null,$arr_spk);            

        }

        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            $return     = 0;

        }else{

            $return     = 1;

        }


        echo $return;

    }

   /* public function cetak($id){

        $barcode    = $this->order_model->select("barcode")->find($id);

        if(!$barcode || $barcode->barcode == NULL){

            $barcode    = $this->gen_kode_barcode();

            $this->db->trans_start();
                $this->order_model->update($id,['barcode'=>$barcode]);
                $sql    = $this->db->last_query();
            $this->db->trans_complete();
            if($this->db->trans_status() == false){

                $return         = false;
                $keterangan     = "Gagal, update kode barcode order";
                $status         = 0;

            }else{

                $return         = true;
                $keterangan     = "Sukses, update kode barcode order";
                $status         = 1;

            }

            $nm_hak_akses       = $this->managePermission; 
            $kode_universal     = $id;
            $total              = 0;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $total, $sql, $status);                

        }else{

            $barcode = $barcode->barcode;

        }

        $data   = $this->order_model->select("`order_produk`.`no_transaksi`,
                                                `transaksi`.`no_faktur`,
                                                CONCAT(IF(konsumen.panggilan IS NULL,
                                                            '',
                                                            konsumen.panggilan),
                                                        ' ',
                                                        konsumen.`nama`) AS nm,
                                                konsumen.st AS st_konsumen,
                                                `order_produk_detail`.`nama_pekerjaan`,
                                                `produk`.`nm_produk`,
                                                `order_produk_detail`.`jml_cetak`")
                                    ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                                    ->join("transaksi","order_produk.id_order = transaksi.id_order","inner")
                                    ->join("order_produk_detail","order_produk.id_order = order_produk_detail.id_order","inner")
                                    ->join("produk","order_produk_detail.id_produk = produk.idproduk","inner")
                                    ->where("order_produk.deleted = 0 and
                                                transaksi.deleted = 0 and order_produk.id_order = '{$id}'")
                                    ->order_by("`order_produk_detail`.`nama_pekerjaan`","asc")
                                    ->find_all();
        
        $this->load->library("barcode");
        $hasil      = $this->barcode->gen_barcode($barcode,0,1);
        $symbol     = base64_encode($hasil);

        //echo '<br><img class="img-responsive" style="width: 40mm;height: auto;" src="data:image/png;base64,' . base64_encode($hasil) . '">';

        $this->template->set("data", $data);
        $this->template->set("kode_barcode", $barcode);
        $this->template->set("symbol_barcode", $symbol);

        $this->template->set_layout('cetak_struk');
        $this->template->title(lang('capt-index-title-label'));
        $this->template->render('cetak_label');

    }*/

    public function gen_kode_barcode(){

        $kd_rnd     = "";
        $st_ketemu  = 1;       
        $total      = 0;
        $barcode    = "";

        while ($st_ketemu == 1) {
            
            $kode = array();
            /* -- Hitung check digit -- */
            for ($i = 1; $i <= 12; $i++) { 
                
                $numb       = mt_rand(1,9);
                $kode[$i]   = $numb;

                if(fmod($i, 2) == 0 ){

                    $numb *= 3;

                }
                
                $total  += $numb;

            }

            $check_digit    = fmod($total, 10);
            $check_digit    = 10-$check_digit;
            $check_digit    = $check_digit == 10 ? 0 : $check_digit;

            $barcode        = join($kode).$check_digit;

            /* -- Cek apakah sudah ada barcode yang sama di order -- */
            $cek_unique     = $this->order_model->is_unique('barcode',$barcode);

            if($cek_unique){

                //looping dihentikan
                $st_ketemu      = 0;
                    
            }else{

                //looping dilanjut
                $st_ketemu      = 1;

            }

        }

        /*echo $barcode;
        $this->load->library("barcode");

        //$hasil  = $this->barcode->barcode_html($kode = $barcode,$tipe = 0);
        $hasil  = $this->barcode->gen_barcode($barcode,0,1);

        echo '<br><img class="img-responsive" style="width: 180px;height: auto;" src="data:image/png;base64,' . base64_encode($hasil) . '">';*/

        return $barcode;

    }

    public function delete_data(){

        /*
            Keterangan :
                1.  St_collecting tidak akan dihapus atau diganti 0, agar transaksi tidak dapat dihapus walaupun semua collectingnya sudah didelete
        */

        if(!$this->input->is_ajax_request()){

            redirect('collecting');

        }

        $id             = $this->input->post('id');
        $ket            = $this->input->post('ket');
        $id_universal   = $this->input->post('id_univ');
        
        $user           = $this->auth->userdata();
        $idt_op         = $user->id_user;

        $this->db->trans_start();

        //echo "op = ".$idt_op;

        if(!has_permission($this->deletePermission)){

            $arr_sess   =   [
                                'status'    => 0,
                                'ket'       => lang('err_akses_delete')
                            ];

            echo json_encode($arr_sess);

            return false;

        }

        // start cek apakah id_kolekting yang akan dihapus masih ada.

        $cek    = $this->kolekting_model->find($id);
        if(!$cek){

            $arr_sess   =   [
                                'status'    => 0,
                                'ket'       => lang('err_hilang_delete')
                            ];

            echo json_encode($arr_sess);

            return false;            

        }
        
        $arr_del    =   [
                            'deleted'       => 1,
                            'deleted_by'    => $idt_op,
                            'ket_delete'    => $ket
                        ];

        $this->kolekting_model->update($id, $arr_del);

        // end delete data kolekting

        set_proses_order($id_lokasi = 3, 
                            $id_ket_proses = 12, 
                            $id_detail_produk_order = $cek->id_detail_produk_order, 
                            $jumlah_proses = $cek->jml);                                    

        // start update status 



        $this->db->trans_complete();

        if($this->db->trans_status() == true){

            $arr_return =   [
                                'status'    => 1,
                                'ket'       => lang('sukses_delete')
                            ];

        }else{

            $arr_return =   [
                                'status'    => 0,
                                'ket'       => lang('err_delete')
                            ];

        }

        echo json_encode($arr_return);

    }

    public function create(){

        $data = "";

        if(isset($_POST['tgl1'])){
            
            $tgl1           = isset($_POST['tgl1']) ? $this->input->post('tgl1') : '';
            $tgl2           = isset($_POST['tgl2']) ? $this->input->post('tgl2') : '';
            $no_faktur      = isset($_POST['no_faktur']) ? $this->input->post('no_faktur') : '';
            $id_konsumen    = isset($_POST['konsumen']) ? $this->input->post('konsumen') : '';
            $nm_file        = isset($_POST['nm_project']) ? $this->input->post('nm_project') : '';
            $kategori       = isset($_POST['kategori']) ? $this->input->post('kategori') : '';

            //$data   = $this->buka_data();
        }else if(isset($_GET['tgl1'])){

            $tgl1           = isset($_GET['tgl1']) ? $this->input->get('tgl1') : '';
            $tgl2           = isset($_GET['tgl2']) ? $this->input->get('tgl2') : '';
            $no_faktur      = isset($_GET['no_faktur']) ? $this->input->get('no_faktur') : '';
            $id_konsumen    = isset($_GET['konsumen']) ? $this->input->get('konsumen') : '';
            $nm_file        = isset($_GET['nm_project']) ? $this->input->get('nm_project') : '';
            $kategori       = isset($_GET['kategori']) ? $this->input->get('kategori') : '';

        }else{

            $tgl1           = date('d/m/Y');
            $tgl2           = date('d/m/Y');

            $no_faktur      = '';
            $id_konsumen    = '';
            $nm_file        = '';
            $kategori       = '';

        }

        $this->load->library('pagination');
        
        $filter = "?cl";

        $where          = "transaksi.deleted = 0 
                            AND transaksi.st_history = 0
                            AND `m_order_produk_detail`.deleted = 0
                            AND (t_kol.jml_kolekting IS NULL
                                    OR t_kol.jml_kolekting < `m_order_produk_detail`.`jml_cetak`)";


        if(strlen($tgl1)>0){

            $tgl1_cek   = date_ymd($tgl1);
            $tgl2_cek   = date_ymd($tgl2);

            $where  .= " and `order_produk`.`tgl_order` >= '{$tgl1_cek}' and `order_produk`.`tgl_order` <= '{$tgl2_cek}'";

            $filter .= "&tgl1={$tgl1}&tgl2={$tgl2}";

        }


        if(strlen($no_faktur)>0){

            $where  .= " and `transaksi`.`no_faktur` = '{$no_faktur}'";
            $filter .= "&no_faktur={$no_faktur}";
            
        }

        if(strlen($id_konsumen)>0){

            $where  .= " and `order_produk`.`id_konsumen` = '{$id_konsumen}'";
            $filter .= "&id_konsumen={$id_konsumen}";

        }

        if(strlen($nm_file)>0){

            $where  .= " and `m_order_produk_detail`.`nama_pekerjaan` like '%{$nm_file}%'";
            $filter .= "&nm_file={$nm_file}";

        }

        if(strlen($kategori)>0){

            $where  .= " and `kategori`.idkategori = {$kategori}";
            $filter .= "&kategori={$kategori}";

        }        


        $total   =   $this->trans_model
                    ->select("`transaksi`.`id_transaksi`")                                
                    ->join("`order_produk`","`transaksi`.`id_order` = `order_produk`.`id_order`","inner")
                    ->join("`konsumen`","`konsumen`.`idkonsumen` = `order_produk`.`id_konsumen`","inner")
                    ->join("`transaksi_detail`","`transaksi_detail`.`id_transaksi` = `transaksi`.`id_transaksi`","inner")
                    ->join("`m_order_produk_detail`","`m_order_produk_detail`.`id_detail_produk_order` = `transaksi_detail`.`id_detail_produk_order`","inner")
                    ->join("`kategori`","`kategori`.`idkategori` = `m_order_produk_detail`.`id_kategori`","inner")
                    ->join("`produk`","`produk`.`idproduk` = `m_order_produk_detail`.`id_produk`","inner")
                    ->join("`konversi_satuan`","`konversi_satuan`.`id_konversi` = `m_order_produk_detail`.`id_satuan`","inner")
                    ->join("`satuan_terkecil`","`konversi_satuan`.`id_satuan_terkecil` = `satuan_terkecil`.`id_satuan_terkecil`","inner")
                    ->join("(SELECT 
                                `id_konversi` AS id, `satuan_besar` AS satuan
                            FROM
                                konversi_satuan
                            WHERE
                                deleted = 0) AS satuan_harga "," `transaksi_detail`.`satuan_harga_by` = `satuan_harga`.`id`","inner")
                    ->join("(SELECT 
                                `kolekting`.`kode_universal` AS id,
                                    SUM(`jml`) AS jml_kolekting,
                                    DATE_FORMAT(MAX(created_on), '%d/%m/%Y %H:%i:%s') AS tgl_akhir
                            FROM
                                kolekting
                            WHERE
                                deleted = 0
                            GROUP BY `kolekting`.`kode_universal`) AS t_kol "," t_kol.id = m_order_produk_detail.kode_universal","left")
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit  = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);                    

        $data   =   $this->trans_model
                    ->select("`transaksi`.`id_transaksi`,
                                `transaksi`.`waktu`,
                                `transaksi`.`no_faktur`,
                                `transaksi`.`id_trans_ref_asli`,
                                `transaksi`.`id_order`,
                                `konsumen`.`panggilan`,
                                `konsumen`.`nama`,
                                `konsumen`.`st`,
                                if(length(`konsumen`.`panggilan`) > 0, CONCAT(`konsumen`.`panggilan`,' ',`konsumen`.`nama`),`konsumen`.`nama`) as nm_konsumen,
                                `order_produk`.`no_transaksi`,
                                `order_produk`.`id_konsumen`,
                                DATE_FORMAT(`order_produk`.`tgl_order`, '%d/%m/%Y') AS tglorder,
                                `m_order_produk_detail`.`nama_pekerjaan`,
                                `kategori`.`nmkategori`,
                                `produk`.`nm_produk`,
                                `produk`.`st_tipe` AS `st_tipe_produk`,
                                `m_order_produk_detail`.`id_detail_produk_order`,
                                `m_order_produk_detail`.`kode_universal`,
                                `m_order_produk_detail`.`thumbnail`,
                                `m_order_produk_detail`.`p`,
                                `m_order_produk_detail`.`l`,
                                `m_order_produk_detail`.`id_satuan` AS `id_satuan_uk`,
                                CONCAT(konversi_satuan.satuan_besar,
                                        ' (',
                                        `konversi_satuan`.`jml_kecil`,
                                        ' ',
                                        `satuan_terkecil`.`alias`,
                                        ')') AS tampil2,
                                `m_order_produk_detail`.`st_urgent`,
                                DATE_FORMAT(`m_order_produk_detail`.`tgl_permintaan_selesai`,
                                        '%d/%m/%Y') AS tgl_selesai,
                                IF(`m_order_produk_detail`.`st_finishing` = 0,
                                    'Finishing Standart',
                                    'Custom Finishing') AS st_finishing,
                                `m_order_produk_detail`.`jml_cetak`,
                                t_kol.tgl_akhir,
                                t_kol.jml_kolekting,
                                (`m_order_produk_detail`.`jml_cetak` - IF(`t_kol`.`jml_kolekting` IS NULL,
                                                0,
                                                `t_kol`.`jml_kolekting`)) AS selisih")
                    ->join("`order_produk`","`transaksi`.`id_order` = `order_produk`.`id_order`","inner")
                    ->join("`konsumen`","`konsumen`.`idkonsumen` = `order_produk`.`id_konsumen`","inner")
                    ->join("`transaksi_detail`","`transaksi_detail`.`id_transaksi` = `transaksi`.`id_transaksi`","inner")
                    ->join("`m_order_produk_detail`","`m_order_produk_detail`.`id_detail_produk_order` = `transaksi_detail`.`id_detail_produk_order`","inner")
                    ->join("`kategori`","`kategori`.`idkategori` = `m_order_produk_detail`.`id_kategori`","inner")
                    ->join("`produk`","`produk`.`idproduk` = `m_order_produk_detail`.`id_produk`","inner")
                    ->join("`konversi_satuan`","`konversi_satuan`.`id_konversi` = `m_order_produk_detail`.`id_satuan`","inner")
                    ->join("`satuan_terkecil`","`konversi_satuan`.`id_satuan_terkecil` = `satuan_terkecil`.`id_satuan_terkecil`","inner")
                    ->join("(SELECT 
                                `id_konversi` AS id, `satuan_besar` AS satuan
                            FROM
                                konversi_satuan
                            WHERE
                                deleted = 0) AS satuan_harga "," `transaksi_detail`.`satuan_harga_by` = `satuan_harga`.`id`","inner")
                    ->join("(SELECT 
                                `kolekting`.`kode_universal` AS id,
                                    SUM(`jml`) AS jml_kolekting,
                                    DATE_FORMAT(MAX(created_on), '%d/%m/%Y %H:%i:%s') AS tgl_akhir
                            FROM
                                kolekting
                            WHERE
                                deleted = 0
                            GROUP BY `kolekting`.`kode_universal`) AS t_kol "," t_kol.id = m_order_produk_detail.kode_universal","left")
                    ->where($where)
                    ->order_by("`order_produk`.`tgl_order`", "asc")
                    ->order_by("`m_order_produk_detail`.`nama_pekerjaan`", "asc")
                    ->limit($limit, $offset)
                    ->find_all();

        $data_konsumen  = $this->konsumen_model
                                ->select("`idkonsumen` as id, 
                                        CONCAT(IF(panggilan IS NULL, '', panggilan),
                                        ' ',
                                        `nama`,
                                        ' | ',
                                        IF(st = 0,
                                            'konsumen',
                                            IF(st = 1, 'reseller', 'instansi'))) AS nm")
                                ->where("deleted=0")
                                ->order_by("nama","asc")
                                ->find_all();

        $data_kategori  = $this->kategori_model
                                    ->select("`idkategori` as id, `nmkategori` as nm")
                                    ->where("deleted=0")
                                    ->order_by("nmkategori","asc")
                                    ->find_all();

        $asset  =   [
                        "plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
                        "plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
                        "plugins/select2/dist/css/select2.min.css",
                        "plugins/number/jquery.number.js",
                        "plugins/select2/dist/js/select2.min.js",
                        "collecting/assets/js/frm_collecting.js",
                        "collecting/assets/css/css_list_kolekting.css"
                    ];

        add_assets($asset);

        $this->template->set("numb",$offset);

        $this->template->set("tgl1",$tgl1);
        $this->template->set("tgl2",$tgl2);
        
        $this->template->set("no_faktur",$no_faktur);
        $this->template->set("id_konsumen",$id_konsumen);
        $this->template->set("nm_file",$nm_file);
        $this->template->set("kategori",$kategori);

        $this->template->set("data",$data);
        $this->template->set("dt_konsumen",$data_konsumen);
        $this->template->set("dt_kategori",$data_kategori);

        $this->template->title(lang('title_new'));
        $this->template->set("toolbar_title", lang('title_new'));
        $this->template->set("page_title", lang('title_new'));
        $this->template->page_icon('fa fa-list');
        $this->template->render('frm_collecting');    

    }

    public function view($id_transaksi){

        // start simpan jml kolecting
        if(isset($_POST['simpan']) && has_permission($this->addPermission)){

            $simpan = $this->simpan_from_view($id_transaksi);

            if($simpan){

                $this->template->set_message(lang("label_simpan_sukses"),"success");
                redirect("collecting");

            }

        }
        // end simpan jml kolecting


        $data   =   $this->trans_model
                        ->select("`transaksi`.`id_transaksi`,
                                    date_format(`transaksi`.`waktu`,'%d/%m/%Y %H:%i:%s') as waktu_trans,
                                    `transaksi`.`no_faktur`,
                                    `transaksi`.`id_trans_ref_asli`,
                                    `transaksi`.`id_order`,
                                    `konsumen`.`panggilan`,
                                    `konsumen`.`nama`,
                                    `konsumen`.`st`,
                                    IF(LENGTH(`konsumen`.`panggilan`) > 0,
                                        CONCAT(`konsumen`.`panggilan`,
                                                ' ',
                                                `konsumen`.`nama`),
                                        `konsumen`.`nama`) AS nm_konsumen,
                                    `order_produk`.`no_transaksi`,
                                    `order_produk`.`id_konsumen`,
                                    DATE_FORMAT(`order_produk`.`tgl_order`, '%d/%m/%Y') AS tglorder,
                                    `m_order_produk_detail`.`nama_pekerjaan`,
                                    `kategori`.`nmkategori`,
                                    `produk`.`nm_produk`,
                                    `produk`.`st_tipe` AS `st_tipe_produk`,
                                    `m_order_produk_detail`.`id_detail_produk_order`,
                                    `m_order_produk_detail`.`thumbnail`,
                                    `m_order_produk_detail`.`p`,
                                    `m_order_produk_detail`.`l`,
                                    `m_order_produk_detail`.`id_satuan` AS `id_satuan_uk`,
                                    CONCAT(konversi_satuan.satuan_besar,
                                            ' (',
                                            `konversi_satuan`.`jml_kecil`,
                                            ' ',
                                            `satuan_terkecil`.`alias`,
                                            ')') AS tampil2,
                                    `m_order_produk_detail`.`st_urgent`,
                                    DATE_FORMAT(`m_order_produk_detail`.`tgl_permintaan_selesai`,
                                            '%d/%m/%Y') AS tgl_selesai,
                                    IF(`m_order_produk_detail`.`st_finishing` = 0,
                                        'Finishing Standart',
                                        'Custom Finishing') AS st_finishing,
                                    `m_order_produk_detail`.`jml_cetak`,
                                    t_kol.idkolekting,
                                    t_kol.id as iduniv,
                                    t_kol.tgl_buat,
                                    t_kol.jml as jml_kol,
                                    t_kol.user_proses")
                        ->join("`order_produk`","`transaksi`.`id_order` = `order_produk`.`id_order`","inner")
                        ->join("`konsumen`","`konsumen`.`idkonsumen` = `order_produk`.`id_konsumen`","inner")
                        ->join("`transaksi_detail`","`transaksi_detail`.`id_transaksi` = `transaksi`.`id_transaksi`","inner")
                        ->join("`m_order_produk_detail`","`m_order_produk_detail`.`id_detail_produk_order` = `transaksi_detail`.`id_detail_produk_order`","inner")
                        ->join("`kategori`","`kategori`.`idkategori` = `m_order_produk_detail`.`id_kategori`","inner")
                        ->join("`produk`","`produk`.`idproduk` = `m_order_produk_detail`.`id_produk`","inner")
                        ->join("`konversi_satuan`","`konversi_satuan`.`id_konversi` = `m_order_produk_detail`.`id_satuan`","inner")
                        ->join("`satuan_terkecil`","`konversi_satuan`.`id_satuan_terkecil` = `satuan_terkecil`.`id_satuan_terkecil`","inner")
                        ->join("(SELECT 
                                        `id_konversi` AS id, `satuan_besar` AS satuan
                                    FROM
                                        konversi_satuan
                                    WHERE
                                        deleted = 0) AS satuan_harga","`transaksi_detail`.`satuan_harga_by` = `satuan_harga`.`id`","inner")
                        ->join("(SELECT 
                                        kolekting.idkolekting,
                                        `kolekting`.`kode_universal` AS id,
                                        kolekting.`jml`,
                                        DATE_FORMAT(kolekting.created_on, '%d/%m/%Y %H:%i:%s') AS tgl_buat,
                                        `users`.`nm_lengkap` AS user_proses,
                                        kolekting.created_on
                                    FROM
                                        kolekting
                                    INNER JOIN users ON kolekting.created_by = users.id_user
                                    WHERE
                                        kolekting.deleted = 0
                                    ORDER BY kolekting.created_on desc, `kolekting`.`kode_universal` asc) AS t_kol","t_kol.id = m_order_produk_detail.kode_universal","left")
                        ->where("`transaksi`.`deleted` = 0
                                    AND transaksi.st_history = 0
                                    AND `m_order_produk_detail`.deleted = 0
                                    AND transaksi.id_transaksi = '{$id_transaksi}'")
                        ->order_by("`m_order_produk_detail`.`nama_pekerjaan`","asc")
                        ->order_by("t_kol.created_on","desc")
                        ->find_all();

        //echo $this->db->last_query();

        if(is_array($data) && count($data)){

            //susun field detail

            $detail     = [];
            $kode_acuan = '';
            $key_array  = 0;

            foreach ($data as $key => $isi) {
                
                $kode_det   = $isi->id_detail_produk_order;

                if($kode_det != $kode_acuan){

                    $detail[]   =   [
                                        'id_det_order'  => $isi->id_detail_produk_order,
                                        'thumb'         => $isi->thumbnail,
                                        'nm_project'    => $isi->nama_pekerjaan,
                                        'kat_pro'       => $isi->nmkategori." - ".$isi->nm_produk,
                                        'st_pro'        => $isi->st_tipe_produk,
                                        'ukuran'        => $isi->p." x ".$isi->l."(".$isi->tampil2.")",
                                        'finishing'     => $isi->st_finishing,
                                        'jml_cetak'     => $isi->jml_cetak,
                                        'det_kol'       =>  [
                                                                [
                                                                    'id_kol'    => $isi->idkolekting,
                                                                    'id_univ'   => $isi->iduniv,
                                                                    'waktu_kol' => $isi->tgl_buat,
                                                                    'jml_kol'   => $isi->jml_kol,
                                                                    'admin'     => $isi->user_proses
                                                                ]
                                                            ]

                                    ];

                    if($key > 0){

                        $key_array += 1;

                    }

                    $kode_acuan = $kode_det;

                }else{

                    $detail[$key_array]['det_kol'][]    =  [
                                                                'id_kol'    => $isi->idkolekting,
                                                                'id_univ'   => $isi->iduniv,
                                                                'waktu_kol' => $isi->tgl_buat,
                                                                'jml_kol'   => $isi->jml_kol,
                                                                'admin'     => $isi->user_proses
                                                            ];

                }


            }

            //susun field header transaksinya

            $arr_head_trans     =   [
                                        'id_trans'  => $data[0]->id_transaksi,
                                        'no_nota'   => $data[0]->no_faktur,
                                        'waktu'     => $data[0]->waktu_trans,
                                        'konsumen'  => $data[0]->nm_konsumen,
                                        'detail'    => $detail
                                    ];

            $data   = $arr_head_trans;

        }else{

            $data   = null;

        }

        //print_r($data);

        $asset  =   [
                        "plugins/number/jquery.number.js",
                        "collecting/assets/js/view_collecting.js",
                        "collecting/assets/css/css_list_kolekting.css"
                    ];

        add_assets($asset);

        $this->template->set('data', $data);
        $this->template->title(lang('title_view'));
        $this->template->set("toolbar_title", lang('title_view'));
        $this->template->set("page_title", lang('title_view'));
        $this->template->page_icon('fa fa-list');
        $this->template->render('frm_view');    

    }

    /*private function update($id = ""){

        $id_order       = $id;
        $id             = $this->input->post('id_kolekting');
        $st_awal        = $this->input->post('st_kolekting_awal');
        $st_proses      = $this->input->post('st_baru');
        $kd_detail      = $this->input->post('kd_detail_order');

        //cek apakah ada data yang berubah
        $update_kolekting       = array();
        $update_st_det_order    = array();
        $update_st_order        = array();

        $st_ada_dibatalkan      = 0;
        $st_perubahan           = 0; //0 => tidak ada perubahan, 1 => ada perubahan

        foreach ($id as $key => $isi) {
            
            if($st_awal[$key] !=  $st_proses[$key]){

                $st_perubahan   = 1;

                $st_awal        = 1;

                //-- Update data kolekting --
                $update_kolekting[] =   [

                                            'idkolekting'   => $isi, 
                                            'st_collecting' => $st_proses[$key]

                                        ];

                //-- Update data detail order --
                $update_st_det_order[] =    [
                                                'id_detail_produk_order'    => $kd_detail[$key],
                                                'st_collecting'             => $st_proses[$key]
                                            ];

            }

            if($st_proses[$key] == 0 ){

                $st_ada_dibatalkan = 1;

            }

        }

        //-- Update status data order --
        if(is_array($update_kolekting) && count($update_kolekting)){        

            $update_st_order    =   [
                                        'st_selesai'    => $st_ada_dibatalkan == 1 ? 0 : 1
                                    ];

        }

        $sql_all    = "";

        if($st_perubahan == 1){

            $this->db->trans_start();

                if(is_array($update_kolekting) && count($update_kolekting)){

                    $this->kolekting_model->update_batch($update_kolekting,'idkolekting');
                    $sql_all    .= "\n\n".$this->db->last_query().";";

                }

                if(is_array($update_st_det_order) && count($update_st_det_order)){

                    $this->order_detail_model->update_batch($update_st_det_order,'id_detail_produk_order');
                    $sql_all    .= "\n\n".$this->db->last_query().";";

                }            

                if(is_array($update_st_order) && count($update_st_order)){

                    $this->order_model->update($id_order,$update_st_order);
                    $sql_all    .= "\n\n".$this->db->last_query().";";

                }            

            $this->db->trans_complete();

            if($this->db->trans_status() == false){

                $return         = false;
                $keterangan     = "Gagal, edit data produk yang sudah dicollecting";
                $status         = 0;

            }else{

                $return         = true;
                $keterangan     = "Sukses, edit data produk yang sudah dicollecting";
                $status         = 1;

            }

            $nm_hak_akses       = $this->managePermission; 
            $kode_universal     = $id_order;
            $total              = 0;
            $sql                = $sql_all;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $total, $sql, $status);

        }else{

            $this->template->set_message(lang('konfirmasi-tidak-ada-perubahan'),"warning");
            return false;        

        }

        return $return;

    }*/

    public function simpan_from_view($id){

        $id_detail_order    = $this->input->post('dft_id_detail_order');
        $pil_semua          = $this->input->post('pil_semua');
        $jml_kolekting      = $this->input->post('jml_kol');

        // ================= Area pengkondisian inputan manual =======================

        $susun_or   =   "";

        foreach ($id_detail_order as $key => $cek) {

            if($key > 0){

                $susun_or .= ' or ';

            }else if($key == 0){

                $susun_or .= ' AND (';

            }

            $susun_or   .= "`m_order_produk_detail`.id_detail_produk_order = '{$cek}'";

            if($key == (count($id_detail_order) -1)){

                $susun_or .= ')';                

            }


        }

        //echo $susun_or;

        //die();

        $data   = $this->trans_model
                        ->select("`transaksi`.`id_transaksi`,
                                    `transaksi`.`id_trans_ref_asli`,
                                    `m_order_produk_detail`.id_detail_produk_order,
                                    `m_order_produk_detail`.kode_universal,
                                    `m_order_produk_detail`.`nama_pekerjaan`,
                                    `m_order_produk_detail`.`jml_cetak`,
                                    `t_kol`.`jml_kolekting`,
                                    (`m_order_produk_detail`.`jml_cetak` - IF(`t_kol`.`jml_kolekting` IS NULL,
                                        0,
                                        `t_kol`.`jml_kolekting`)) AS selisih")
                        ->join("`transaksi_detail`","`transaksi_detail`.`id_transaksi` = `transaksi`.`id_transaksi`","inner")
                        ->join("`m_order_produk_detail`","`m_order_produk_detail`.`id_detail_produk_order` = `transaksi_detail`.`id_detail_produk_order`","inner")
                        ->join("(SELECT 
                                    `kolekting`.`kode_universal` AS id,
                                        SUM(`jml`) AS jml_kolekting,
                                        DATE_FORMAT(MAX(created_on), '%d/%m/%Y %H:%i:%s') AS tgl_akhir
                                FROM
                                    kolekting
                                WHERE
                                    deleted = 0
                                GROUP BY `kolekting`.`kode_universal`) AS t_kol","`t_kol`.`id` = `m_order_produk_detail`.`kode_universal`","left")
                        ->where("`transaksi`.`deleted` = 0
                                    AND `transaksi`.`st_history` = 0
                                    AND `m_order_produk_detail`.`deleted` = 0
                                    AND `transaksi`.`id_transaksi` = '{$id}'". $susun_or )
                        ->find_all();

        $arr_insert     = [];
        $arr_st_col     = [];
        $pesan_error    = [];

        if(is_array($data) && count($data)){

            foreach ($id_detail_order as $i => $kode) {
            
                foreach ($data as $index => $isi) {
                    
                    if($kode == $isi->id_detail_produk_order){

                        if($isi->selisih >= $jml_kolekting[$i]){

                            if($jml_kolekting[$i] > 0){

                                $arr_insert[]   =   [
                                                        'idkolekting'               => gen_primary("","kolekting","idkolekting"), 
                                                        'id_detail_produk_order'    => $isi->id_detail_produk_order, 
                                                        'kode_universal'            => $isi->kode_universal, 
                                                        'jml'                       => $jml_kolekting[$i]
                                                    ];

                                $arr_st_col[]   =   [
                                                        'st_collecting'     => 1,
                                                        'kode_universal'    => $isi->kode_universal, 
                                                    ];                        
                            
                            }else{

                                /* 
                                    Karena ada 2 kemungkinan jika $jml_kolekting <= 0
                                    1. Memang tidak diisi.
                                    2. Dicentang.   
                                */

                                if(array_key_exists($jml_kolekting[$i], $pil_semua)){

                                    $arr_insert[]   =   [
                                                            'idkolekting'               => gen_primary("","kolekting","idkolekting"), 
                                                            'id_detail_produk_order'    => $isi->id_detail_produk_order, 
                                                            'kode_universal'            => $isi->kode_universal, 
                                                            'jml'                       => $isi->selisih
                                                        ];

                                    $arr_st_col[]   =   [
                                                            'st_collecting'     => 1,
                                                            'kode_universal'    => $isi->kode_universal, 
                                                        ];      

                                }

                            }

                        }else{

                            $pesan_error[]  = "Jml Collecting yang diinputkan tidak sesuai dengan selisih terakhir, untuk Nama Pekerjaan ". $isi->nama_pekerjaan;
                        }

                    }


                }

            }

            //start cek apakah ada pesan error, jika ada batalkan proses

            if(count($pesan_error) > 0){

                $pesan_final = "";

                foreach ($pesan_error as $key_e => $error) {
                    $pesan_final .= $error;

                    if($key_e < (count($pesan_error)-1)){

                        $pesan_final .= "<br>";

                    }

                }

                $this->template->set_message($pesan_final,"error");
                return false;

            }

            //end cek apakah ada pesan error, jika ada batalkan proses

            $this->db->trans_start();

                // start simpan proses collectingnya
                $this->kolekting_model->insert_batch($arr_insert);
                //echo $this->db->last_query();
                // end simpan proses collectingnya

                // start update log prosesnya

                foreach ($arr_insert as $key => $val) {
            
                    set_proses_order($id_lokasi = 3, 
                                        $id_ket_proses = 6, 
                                        $id_detail_produk_order = $val['id_detail_produk_order'], 
                                        $jumlah_proses = $val['jml']);                            


                }                

                // end update log prosesnya


                // start update status collecting list order
                $this->order_detail_model->update_batch($arr_st_col, 'kode_universal');
                // end update status collecting list order

            $this->db->trans_complete();

            if($this->db->trans_status() == false){

                $return = false;

            }else{

                // start proses update st_selesai

                $data   = $this->trans_model
                                    ->select("`m_order_produk_detail`.kode_universal,
                                                (`m_order_produk_detail`.`jml_cetak` - IF(`t_kol`.`jml_kolekting` IS NULL,
                                                    0,
                                                    `t_kol`.`jml_kolekting`)) AS selisih")
                                    ->join("`transaksi_detail`","`transaksi_detail`.`id_transaksi` = `transaksi`.`id_transaksi`","inner")
                                    ->join("`m_order_produk_detail`","`m_order_produk_detail`.`id_detail_produk_order` = `transaksi_detail`.`id_detail_produk_order`","inner")
                                    ->join("(SELECT 
                                                `kolekting`.`kode_universal` AS id,
                                                    SUM(`jml`) AS jml_kolekting,
                                                    DATE_FORMAT(MAX(created_on), '%d/%m/%Y %H:%i:%s') AS tgl_akhir
                                            FROM
                                                kolekting
                                            WHERE
                                                deleted = 0
                                            GROUP BY `kolekting`.`kode_universal`) AS t_kol","`t_kol`.`id` = `m_order_produk_detail`.`kode_universal`","left")
                                    ->where("`transaksi`.`deleted` = 0
                                                AND `transaksi`.`st_history` = 0
                                                AND `m_order_produk_detail`.`deleted` = 0
                                                AND `transaksi`.`id_transaksi` = '{$id}'". $susun_or )
                                    ->having("selisih = 0")
                                    ->find_all();

                if(is_array($data) && count($data)){

                    $this->db->trans_start();

                        $arr_update_st_selesai = [];

                        foreach ($data as $key => $isi) {
                            
                            $arr_update_st_selesai[]    =   [
                                                                'st_selesai'        => 1,
                                                                'st_ambil'          => 1,
                                                                'kode_universal'    => $isi->kode_universal
                                                            ];

                            $this->spk_model->update_batch($arr_update_st_selesai, 'kode_universal');


                        }

                    $this->db->trans_complete();                    

                    if($this->db->trans_status() == false){

                        $return = false;

                    }else{

                        $return = true;

                    }

                }else{

                    $return = true;
                        
                }

                // end proses update st_selesai

            }

            return $return;

        }else{

            $this->template->set_message(lang("err_data_tidak_ada"),"error");
            return false;

        }
                        


    }

    public function simpan_complete(){

        if(!$this->input->is_ajax_request()){

            redirect("collecting");

        }

        $status     = 0;// 0 error , 1 sukkses
        $ket        = "";

        $id_universal   = $this->input->post("univ");

        $where          = "`transaksi`.`deleted` = 0
                            AND `transaksi`.`st_history` = 0
                            AND `m_order_produk_detail`.`deleted` = 0
                            AND  `m_order_produk_detail`.kode_universal = '{$id_universal}'";

        $data   = $this->trans_model
                        ->select("`transaksi`.`id_transaksi`,
                                    `transaksi`.`id_trans_ref_asli`,
                                    `m_order_produk_detail`.id_detail_produk_order,
                                    `m_order_produk_detail`.kode_universal,
                                    `m_order_produk_detail`.`nama_pekerjaan`,
                                    `m_order_produk_detail`.`jml_cetak`,
                                    `t_kol`.`jml_kolekting`,
                                    (`m_order_produk_detail`.`jml_cetak` - IF(`t_kol`.`jml_kolekting` IS NULL,
                                        0,
                                        `t_kol`.`jml_kolekting`)) AS selisih")
                        ->join("`transaksi_detail`","`transaksi_detail`.`id_transaksi` = `transaksi`.`id_transaksi`","inner")
                        ->join("`m_order_produk_detail`","`m_order_produk_detail`.`id_detail_produk_order` = `transaksi_detail`.`id_detail_produk_order`","inner")
                        ->join("(SELECT 
                                    `kolekting`.`kode_universal` AS id,
                                        SUM(`jml`) AS jml_kolekting,
                                        DATE_FORMAT(MAX(created_on), '%d/%m/%Y %H:%i:%s') AS tgl_akhir
                                FROM
                                    kolekting
                                WHERE
                                    deleted = 0
                                GROUP BY `kolekting`.`kode_universal`) AS t_kol","`t_kol`.`id` = `m_order_produk_detail`.`kode_universal`","left")
                        ->where($where)
                        ->find_all();

        // -- cek apakah datanya masih ada.
        if(!is_array($data) || !count($data)){

            $arr_return  =  [
                                'status' => 0,
                                'ket'    => lang('err-list-order-kosong')

                            ];

            echo json_encode($arr_return);

            return false;            

        }

        $this->db->trans_start();

        $sisa_cetakan   = $data[0]->selisih;

        $arr_insert =   [
                            'idkolekting'               => gen_primary("","kolekting","idkolekting"), 
                            'id_detail_produk_order'    => $data[0]->id_detail_produk_order, 
                            'kode_universal'            => $id_universal, 
                            'jml'                       => $sisa_cetakan
                        ];

        $this->kolekting_model->insert($arr_insert);                        

        $arr_st_col =   ['st_collecting'     => 1]; 

        $this->order_detail_model->update_where('kode_universal',$id_universal ,$arr_st_col);

        $arr_update_st_selesai  =   [
                                        'st_selesai'        => 1,
                                        'st_ambil'          => 1            
                                    ];

        $this->spk_model->update_where('kode_universal',$id_universal ,$arr_update_st_selesai);

        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            $arr_return =   [
                                'status' => 0,
                                'ket'    => lang('err-simpan')

                            ];

        }else{

            $arr_return =   [
                                'status' => 1,
                                'ket'    => lang('label_simpan_sukses')
                            ];

        }

        echo json_encode($arr_return);

    }

    public function simpan(){

        if(!$this->input->is_ajax_request()){

            redirect("collecting");

        }

        $status     = 0;// 0 error , 1 sukkses
        $ket        = "";

        if(!has_permission($this->addPermission)){

            $arr_return  =  [
                                'status' => 0,
                                'ket'    => lang('err-akses-tambah')

                            ];

            echo json_encode($arr_return);

            return false;

        }

        $id_detail_order    = $this->input->post("id");
        $id_universal       = $this->input->post("univ");
        $jml_kolekting      = $this->input->post("jml");

        $id_trans_master    = $this->input->post("id_master");         

        // start cek perbandingan antara jumlah cetak dan jumlah collecting

        $where      = "`transaksi`.`deleted` = 0
                        AND `transaksi`.`st_history` = 0
                        AND `m_order_produk_detail`.`kode_universal` = '{$id_universal}'
                        AND `m_order_produk_detail`.deleted = 0";

        $cek_selisih        = $this->trans_model
                                ->select("`transaksi`.`id_transaksi`,
                                            `m_order_produk_detail`.`id_detail_produk_order`,
                                            `m_order_produk_detail`.`kode_universal`,
                                            `m_order_produk_detail`.`jml_cetak`,
                                            `t_kol`.`jml_kolekting`,
                                            (`m_order_produk_detail`.`jml_cetak` - IF(`t_kol`.`jml_kolekting` IS NULL,
                                                0,
                                                `t_kol`.`jml_kolekting`)) AS selisih")
                                ->join("`transaksi_detail`","`transaksi_detail`.`id_transaksi` = `transaksi`.`id_transaksi`","inner")
                                ->join("`m_order_produk_detail`","`m_order_produk_detail`.`id_detail_produk_order` = `transaksi_detail`.`id_detail_produk_order`","inner")
                                ->join("(SELECT 
                                            `kolekting`.`kode_universal` AS id,
                                                SUM(`jml`) AS jml_kolekting,
                                                DATE_FORMAT(MAX(created_on), '%d/%m/%Y %H:%i:%s') AS tgl_akhir
                                        FROM
                                            kolekting
                                        WHERE
                                            deleted = 0
                                        GROUP BY `kolekting`.`kode_universal`) AS t_kol","`t_kol`.`id` = `m_order_produk_detail`.`kode_universal`","left")
                                ->where($where)
                                ->having('selisih > 0')
                                ->find_all();

        //echo $this->db->last_query();
        // -- cek apakah datanya masih ada.
        if(!is_array($cek_selisih) || !count($cek_selisih)){

            $arr_return  =  [
                                'status' => 0,
                                'ket'    => lang('err-list-order-kosong')

                            ];

            echo json_encode($arr_return);

            return false;            

        }

        // -- cek jml yang diinputkan dengan sisa
        $sisa_cetakan   = $cek_selisih[0]->selisih;
        if($jml_kolekting > $sisa_cetakan ){

            $arr_return  =  [
                                'status' => 0,
                                'ket'    => lang('err-jml-lebih')
                            ];

            echo json_encode($arr_return);

            return false;                        

        }


        // end cek perbandingan antara jumlah cetak dan jumlah collecting

        // start simpan data jml collecting

        $this->db->trans_start();

        // --- simpan data kolekting

        $arr_collecting     =   [
                                    'idkolekting'               => gen_primary("","kolekting","idkolekting"), 
                                    'id_detail_produk_order'    => $id_detail_order, 
                                    'kode_universal'            => $id_universal, 
                                    'jml'                       => $jml_kolekting
                                ];

        $this->kolekting_model->insert($arr_collecting);

        // --- start buat log_status order

        foreach ($arr_collecting as $key => $val) {
            
            set_proses_order($id_lokasi = 3, 
                                $id_ket_proses = 6, 
                                $id_detail_produk_order = $val['id_detail_produk_order'], 
                                $jumlah_proses = $val['jml']);                            


        }

        // --- end buat log_status order

        // --- update status kolekting

        $arr_update_status  =   ['st_collecting' => 1];

        $this->order_detail_model->update_where("`kode_universal` = '{$id_universal}' and `deleted` = 0",null,$arr_update_status);

        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            $arr_return =   [
                                'status' => 0,
                                'ket'    => lang('err-simpan')

                            ];

        }else{

            $this->db->trans_start();

            $cek_selisih    = $this->trans_model
                                ->select("(`m_order_produk_detail`.`jml_cetak` - IF(`t_kol`.`jml_kolekting` IS NULL,
                                                0,
                                                `t_kol`.`jml_kolekting`)) AS selisih")
                                ->join("`transaksi_detail`","`transaksi_detail`.`id_transaksi` = `transaksi`.`id_transaksi`","inner")
                                ->join("`m_order_produk_detail`","`m_order_produk_detail`.`id_detail_produk_order` = `transaksi_detail`.`id_detail_produk_order`","inner")
                                ->join("(SELECT 
                                            `kolekting`.`kode_universal` AS id,
                                                SUM(`jml`) AS jml_kolekting,
                                                DATE_FORMAT(MAX(created_on), '%d/%m/%Y %H:%i:%s') AS tgl_akhir
                                        FROM
                                            kolekting
                                        WHERE
                                            deleted = 0
                                        GROUP BY `kolekting`.`kode_universal`) AS t_kol","`t_kol`.`id` = `m_order_produk_detail`.`kode_universal`","left")
                                ->where($where)
                                ->find_all();

            $selisih    = $cek_selisih[0]->selisih;

            if($selisih == 0){

                //update status selesai dari SPK
                $arr_update     = ['st_selesai' => 1, 'st_ambil' => 1];                                
                $where  = "`kode_universal` = '{$id_universal}'";
                $this->spk_model->update_where($where,null,$arr_update);
                //echo $this->db->last_query();

            }

            $this->db->trans_complete();

            if($this->db->trans_status() == false){

                $arr_return =   [
                                    'status' => 0,
                                    'ket'    => lang('err-simpan')
                                ];                

            }else{

                $arr_return =   [
                                    'status' => 1,
                                    'ket'    => lang('label_simpan_sukses')
                                ];
                    
            }

            

        }

        // end simpan data jml collecting

        echo json_encode($arr_return);

    }

    /*private function cek_collecting_all(){

        $data_order     = $this->order_model
                            ->select("`order_produk`.`id_order`,
                                        `order_produk`.`id_konsumen`,
                                        `order_produk`.`no_transaksi`,
                                        COUNT(`order_produk_detail`.`id_detail_produk_order`) AS jml_order,
                                        COUNT(col.`id_detail_produk_order`) AS jml_kolekting,
                                        IF(COUNT(col.`id_detail_produk_order`) < COUNT(`order_produk_detail`.`id_detail_produk_order`),
                                            0,
                                            1) AS st")
                            ->join("order_produk_detail","order_produk.id_order = order_produk_detail.id_order","inner")
                            ->join("(SELECT 
                                        `idkolekting`, `id_detail_produk_order`, `st_collecting`
                                    FROM
                                        kolekting
                                    WHERE
                                        deleted = 0 AND st_collecting = 1) AS col","order_produk_detail.id_detail_produk_order = col.id_detail_produk_order","left")
                            ->where("order_produk.deleted = 0
                                        AND order_produk.st_simpan = 1
                                        AND order_produk_detail.deleted = 0
                                        AND order_produk.st_selesai = 0")
                            ->group_by("`order_produk`.`id_order`")
                            ->find_all();

        $arr_update     = array();
        $arr_konsumen   = array();

        foreach ($data_order as $key => $dt) {
            
            $arr_update[] = [
                                'id_order'      => $dt->id_order,
                                'st_selesai'    => $dt->st
                            ];

            if($dt->st == 1){
            
                $arr_msg[] =    [
                                    'idorder'       => $dt->id_order,
                                    'idkonsumen'    => $dt->id_konsumen,
                                    'no_order'      => $dt->no_transaksi
                                ];
            }

        }

        $this->db->trans_start();

            if(is_array($arr_update) && count($arr_update)){

                $this->order_model->update_batch($arr_update,'id_order');
                $sql = $this->db->last_query();

            }

        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            $return         = false;
            $keterangan     = "Gagal, update status collecting data order";
            $status         = 0;

        }else{

            $return         = true;
            $keterangan     = "Sukses, update status collecting data order";
            $status         = 1;

        }

        $nm_hak_akses       = $this->addPermission; 
        $kode_universal     = "-";
        $total              = 0;
        
        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $total, $sql, $status);

        $this->kirim_email($arr_msg);

    }*/

   /* private function kirim_email($data  = array()){

        if(is_array($data) && count($data)){

            $identitas    = $this->identitas_model->find(1);

            foreach ($data as $key => $dt) {
            
                $dt_konsumen    = $this->konsumen_model
                                    ->select("`idkonsumen` as id,
                                            `email`,
                                            `st`, 
                                            CONCAT(IF(panggilan IS NULL, '', panggilan),
                                            ' ',
                                            `nama`) AS nm")
                                    ->order_by("nama","asc")
                                    ->find($dt['idkonsumen']);

                $dt_nofaktur    = $this->trans_model->select("`no_faktur`, `id_order`")
                                                    ->where("deleted = 0 and id_order = '{$dt['idorder']}'")
                                                    ->find_all();


                $data_msg   =   [
                                    'full_name'     => ucwords($dt_konsumen->nm),
                                    'no_order'      => $dt['no_order'],
                                    'no_faktur'     => $dt_nofaktur[0]->no_faktur,
                                    'st_reseller'   => $dt_konsumen->st,
                                    'no_telp'       => $identitas->no_telp
                                ];

                //isi emailnya
                $message = $this->load->view('email/message', $data_msg, TRUE);
                $message_body  = $this->load->view('email/_header', '', TRUE);
                $message_body .= $message;
                $message_body .= $this->load->view('email/_footer', '', TRUE);

                //tujuan
                $to         = $this->db->escape_str(trim($dt_konsumen->email));
                $subject    = lang("capt-subject-email");

                $this->load->library('mailer');
                $mail = $this->mailer->load();

                //Clear All Recipients
                $mail->clearAllRecipients();
                
                $mail->Subject          = $subject;
                $mail->Body             = $message_body;
                $mail->AddAddress($to);
                $email_kirim            = $mail->Send();
                
            }

        }   


    }*/


    private function buka_data(){

        $tgl1           = $this->input->post('tgl1');
        $tgl2           = $this->input->post('tgl2');

        //$no_order       = $this->input->post('no_order');
        $no_faktur      = $this->input->post('no_faktur');
        $id_konsumen    = $this->input->post('konsumen');
        $nm_file        = $this->input->post('nm_project');
        $kategori       = $this->input->post('kategori');

        $where          = "transaksi.deleted = 0 
                            AND transaksi.st_history = 0
                            AND `m_order_produk_detail`.deleted = 0
                            AND (t_kol.jml_kolekting IS NULL
                                    OR t_kol.jml_kolekting < `m_order_produk_detail`.`jml_cetak`)";

        if(strlen($tgl1)>0){

            $tgl1   = date_ymd($tgl1);
            $tgl2   = date_ymd($tgl2);

            $where  .= " and `order_produk`.`tgl_order` >= '{$tgl1}' and `order_produk`.`tgl_order` <= '{$tgl2}'";

        }else{

            $tgl1   = date('Y-m-d');
            $tgl2   = date('Y-m-d');

            $where  .= " and `order_produk`.`tgl_order` >= '{$tgl1}' and `order_produk`.`tgl_order` <= '{$tgl2}'";

        }


        if(strlen($no_faktur)>0){

            $where  .= " and `transaksi`.`no_faktur` = '{$no_faktur}'";
            
        }

        if(strlen($id_konsumen)>0){

            $where  .= " and `order_produk`.`id_konsumen` = '{$id_konsumen}'";
        }

        if(strlen($nm_file)>0){

            $where  .= " and `m_order_produk_detail`.`nama_pekerjaan` like '%{$nm_file}%'";
        }

        if(strlen($kategori)>0){

            $where  .= " and `kategori`.idkategori = {$kategori}";
        }        


        $data   =   $this->trans_model
                    ->select("`transaksi`.`id_transaksi`,
                                `transaksi`.`waktu`,
                                `transaksi`.`no_faktur`,
                                `transaksi`.`id_trans_ref_asli`,
                                `transaksi`.`id_order`,
                                `konsumen`.`panggilan`,
                                `konsumen`.`nama`,
                                `konsumen`.`st`,
                                if(length(`konsumen`.`panggilan`) > 0, CONCAT(`konsumen`.`panggilan`,' ',`konsumen`.`nama`),`konsumen`.`nama`) as nm_konsumen,
                                `order_produk`.`no_transaksi`,
                                `order_produk`.`id_konsumen`,
                                DATE_FORMAT(`order_produk`.`tgl_order`, '%d/%m/%Y') AS tglorder,
                                `m_order_produk_detail`.`nama_pekerjaan`,
                                `kategori`.`nmkategori`,
                                `produk`.`nm_produk`,
                                `produk`.`st_tipe` AS `st_tipe_produk`,
                                `m_order_produk_detail`.`id_detail_produk_order`,
                                `m_order_produk_detail`.`kode_universal`,
                                `m_order_produk_detail`.`thumbnail`,
                                `m_order_produk_detail`.`p`,
                                `m_order_produk_detail`.`l`,
                                `m_order_produk_detail`.`id_satuan` AS `id_satuan_uk`,
                                CONCAT(konversi_satuan.satuan_besar,
                                        ' (',
                                        `konversi_satuan`.`jml_kecil`,
                                        ' ',
                                        `satuan_terkecil`.`alias`,
                                        ')') AS tampil2,
                                `m_order_produk_detail`.`st_urgent`,
                                DATE_FORMAT(`m_order_produk_detail`.`tgl_permintaan_selesai`,
                                        '%d/%m/%Y') AS tgl_selesai,
                                IF(`m_order_produk_detail`.`st_finishing` = 0,
                                    'Finishing Standart',
                                    'Custom Finishing') AS st_finishing,
                                `m_order_produk_detail`.`jml_cetak`,
                                t_kol.tgl_akhir,
                                t_kol.jml_kolekting,
                                (`m_order_produk_detail`.`jml_cetak` - IF(`t_kol`.`jml_kolekting` IS NULL,
                                                0,
                                                `t_kol`.`jml_kolekting`)) AS selisih")
                    ->join("`order_produk`","`transaksi`.`id_order` = `order_produk`.`id_order`","inner")
                    ->join("`konsumen`","`konsumen`.`idkonsumen` = `order_produk`.`id_konsumen`","inner")
                    ->join("`transaksi_detail`","`transaksi_detail`.`id_transaksi` = `transaksi`.`id_transaksi`","inner")
                    ->join("`m_order_produk_detail`","`m_order_produk_detail`.`id_detail_produk_order` = `transaksi_detail`.`id_detail_produk_order`","inner")
                    ->join("`kategori`","`kategori`.`idkategori` = `m_order_produk_detail`.`id_kategori`","inner")
                    ->join("`produk`","`produk`.`idproduk` = `m_order_produk_detail`.`id_produk`","inner")
                    ->join("`konversi_satuan`","`konversi_satuan`.`id_konversi` = `m_order_produk_detail`.`id_satuan`","inner")
                    ->join("`satuan_terkecil`","`konversi_satuan`.`id_satuan_terkecil` = `satuan_terkecil`.`id_satuan_terkecil`","inner")
                    ->join("(SELECT 
                                `id_konversi` AS id, `satuan_besar` AS satuan
                            FROM
                                konversi_satuan
                            WHERE
                                deleted = 0) AS satuan_harga "," `transaksi_detail`.`satuan_harga_by` = `satuan_harga`.`id`","inner")
                    ->join("(SELECT 
                                `kolekting`.`kode_universal` AS id,
                                    SUM(`jml`) AS jml_kolekting,
                                    DATE_FORMAT(MAX(created_on), '%d/%m/%Y %H:%i:%s') AS tgl_akhir
                            FROM
                                kolekting
                            WHERE
                                deleted = 0
                            GROUP BY `kolekting`.`kode_universal`) AS t_kol "," t_kol.id = m_order_produk_detail.kode_universal","left")
                    ->where($where)
                    ->order_by("`order_produk`.`tgl_order`", "asc")
                    ->order_by("`m_order_produk_detail`.`nama_pekerjaan`", "asc")
                    ->find_all();

        //echo $this->db->last_query();
        return $data;

    }


   	

}
