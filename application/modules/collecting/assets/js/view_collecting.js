$(function(){

	$("input[name='jml_kol[]']").number(true);

});

function centang(obj){

	var posisi 	= $(obj).closest("tr").find("input[name='jml_kol[]']");

	if($(obj).is(":checked")){

		posisi.val("");
		posisi.prop("readonly","readonly");

	}else{

		posisi.removeAttr("readonly");
		posisi.focus();

	}
}

function proses_delete(id, id_universal , keterangan){

	var data 	= 	{
						id 				: id,
						id_univ 		: id_universal,
						ket 			: keterangan,
					};

	$.ajax({
				url 		: baseurl + "collecting/delete_data",
				type 		: "post",
				data 		: data,
				dataType 	: "json",
				success 	: function(msg){

								st = parseFloat(msg['status']);

								if(st == 1){

									alertify.success(msg['ket']);
									location.reload();

								}else{

									alertify.error(msg['ket']);

								}


							}


	});


}

function konfirm_del(obj){

	var id 				= $(obj).closest("tr").find("input[name='dft_idkolekting[]']").val();
	var id_universal 	= $(obj).closest("tr").find("input[name='dft_iduniv[]']").val();

	var pesan 	= "Proses delete Kolekting Order akan mengakibatkan : <br><br>"+
					"<b>1.</b> Status Produksi order akan dibuka kembali, jika proses produksinya dilewati<br><br>"+
					"<b>Lanjutkan proses delete?</b>";	

	alertify.prompt(pesan,"",

		function(evt, value ){

			if(value.length < 10){

				alertify.error('Minimum Keterangan 10 Karakter');

			}else{

				//alertify.success('Ok: ' + value);
				//location.reload();

				proses_delete(id,id_universal, value);

			}

			
		},
		function(){
		    alertify.error('Proses hapus, dibatalkan');
		}
	);


}

/*function delete(obj){

	var pesan 	= "Proses delete Kolekting Order akan mengakibatkan : <br><br>"+
					"<b>1.</b> Status Produksi order akan dibuka kembali, jika proses produksinya dilewati<br><br>"+
					"<b>Lanjutkan proses delete?</b>";

	alertify.prompt(pesan,"",

		function(evt, value ){

			if(value.length < 10){

				alertify.error('Minimum Keterangan 10 Karakter');

			}else{

				alertify.success('Ok: ' + value);
				//location.reload();

				//proses_delete(waktu, idmaster, value);

			}

			
		},
		function(){
		    alertify.error('Proses hapus, dibatalkan');
		}
	);


}*/