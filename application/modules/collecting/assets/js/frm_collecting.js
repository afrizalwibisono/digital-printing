$(function(){

	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	$("#konsumen").select2({
        placeholder :"-- Pilih Konsumen --",
        allowClear  : true
  	});  

  	$("#kategori").select2({
        placeholder :"-- Pilih Kategori --",
        allowClear  : true
  	});  

  	$("input[name='dft_jml_kolekting[]']").number(true);

});

function selesai_semua(obj){

	var kode_universal 		= $(obj).closest("tr").find("input[name='dft_iddetail_universal[]']").val();

	var data 	=	{
						univ 		: kode_universal
					}; 	


	$.ajax({
				url 		: baseurl + 'collecting/simpan_complete',
				type 		: "post",
				data 		: data,
				dataType	: "json",
				success 	: function(msg){

								if(msg){

									var st = parseFloat(msg['status']);

									if(st == 0){

										alertify.error(msg['ket']);

									}else{

										alertify.success(msg['ket']);	

										$(obj).closest("tr").animate({backgroundColor:'green'}, 1000).fadeOut(1000,function() {
											$(obj).closest("tr").remove();
										});   

									}

								}

							}


	});

}

function simpan_data(obj){

	var id_order_detail 	= $(obj).closest("tr").find("input[name='dft_iddetail_order[]']").val();
	var kode_universal 		= $(obj).closest("tr").find("input[name='dft_iddetail_universal[]']").val();
	var jml_kolekting 		= $(obj).closest("tr").find("input[name='dft_jml_kolekting[]']").val();
	var id_trans_master 	= $(obj).closest("tr").find("input[name='dft_idtrans_master[]']").val();

	if(jml_kolekting.length <= 0 ||  parseFloat(jml_kolekting) <= 0){

		alertify.error('JML Kolekting tidak boleh 0');

		return false;

	}

	var data 	=	{
						id  		: id_order_detail,
						univ 		: kode_universal,
						jml 		: jml_kolekting,
						id_master 	: id_trans_master
					}; 	

	$.ajax({
				url 		: baseurl + "collecting/simpan",
				type 		: "post",
				data 		: data,
				dataType 	: "json",
				success 	: function(msg){

								if(msg){

									var st = parseFloat(msg['status']);

									if(st == 0){

										alertify.error(msg['ket']);

									}else{

										alertify.success(msg['ket']);	

										$(obj).closest("tr").animate({backgroundColor:'green'}, 1000).fadeOut(1000,function() {
											$(obj).closest("tr").remove();
										});   

									}

								}

							}
							
	});

}