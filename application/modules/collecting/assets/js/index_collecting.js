$(function(){
	
	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	$("#konsumen").select2({
        placeholder :"-- Pilih Konsumen --",
        allowClear  : true
  	});  

});


function dianggap_lengkap(id, index){

	var data 	= {id:id};

	$.ajax({

				url 	: baseurl + "collecting/dianggap_lengkap",
				type 	: 'post',
				data 	: data,
				success : function(msg){

								var st = parseFloat(msg);

								if(st == 0){

									alertify.error("Proses Perubahan Status Gagal");

								}else{

									alertify.success("Proses Perubahan Status Suksess");
									
									var posisi = $("#daftar tbody tr:eq("+ index +")")	
									posisi.animate({backgroundColor:'green'}, 1000).fadeOut(1000,function() {
										posisi.remove();
									});   

								}

							}

	});

}

function konfirmasi_bypass(obj){

	var id 		= $(obj).closest('tr').find("input[name='id_transaksi[]']").val();

	var index 	= $(obj).closest('tr').index();


	var pesan 	= "Anda yakin akan membuat semua List order menjadi sudah di Collecting ?"

	alertify.confirm('ByPass Status Collecting', pesan, 
					function(){ 
						dianggap_lengkap(id, index);
					},
				    function(){ 
				    	
				    });

}


function cetak(obj,e){

	e.preventDefault();
	var id = $(obj).data("id");

	var url 	= baseurl+"collecting/cetak/"+id;

	window.open(url, '_blank', 'width=250,height=400,scrollbars=yes,menubar=no,status=yes,resizable=yes,screenx=100,screeny=100'); return false;

}