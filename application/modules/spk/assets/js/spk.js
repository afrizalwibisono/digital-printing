$(function(){
	hitung_jml_konversi();

	//Hitung Konversi
	$("select[name='konversi[]']").on("change", function(){
		var stok = parseFloat($(this).closest("tr").find("input[name='stok[]']").val());

		if(isNaN(stok)){
			stok = 0;
		}

		var jml_kecil = $(this).find(":selected").data("jml-kecil");
		var jml_besar = $(this).find(":selected").data("jml-besar");
		var tipe	  = $(this).find(":selected").data("tipe");

		if (tipe==1) {
			jml_konversi = ((stok/Math.pow(jml_kecil,2)) * jml_besar);
			
		}else{
			jml_konversi = (stok/jml_kecil) * jml_besar;
		}
		
		if(isFloat(jml_konversi)){
			$(this).closest("tr").find("label.lbl-stok").text($.number(jml_konversi, 1));
		}else{
			$(this).closest("tr").find("label.lbl-stok").text($.number(jml_konversi, 0));
		}
	});
});

function hitung_jml_konversi(){
	$.each($("#frm_detail tbody tr"), function(i){
		var stok = parseFloat($(this).find("input[name='stok[]']").val());

		if(isNaN(stok)){
			stok = 0;
		}

		var jml_kecil = $(this).find("select option:selected").data("jml-kecil");
		var jml_besar = $(this).find("select option:selected").data("jml-besar");
		var tipe	  = $(this).find(":selected").data("tipe");
		if (tipe==1) {
			jml_konversi = (stok/Math.pow(jml_kecil,2)) * jml_besar;
		}else{
			jml_konversi = (stok/jml_kecil) * jml_besar;
		}

		if(isFloat(jml_konversi)){
			$(this).find("label.lbl-stok").text($.number(jml_konversi, 1));
		}else{
			$(this).find("label.lbl-stok").text($.number(jml_konversi, 0));
		}
	});
}