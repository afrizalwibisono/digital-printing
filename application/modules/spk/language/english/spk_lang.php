<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['spk_title_manage'] 	= 'Manage SPK';
$lang['spk_title_view'] 	= 'View Detail Bahan Baku Penyusun Produk';
$lang['spk_title_edit'] 	= 'Edit Data SPK';

// form/table
$lang['spk_no'] 		= 'NO';
$lang['spk_id_spk']		= 'Kode SPK';
$lang['spk_nama_file'] 	= 'Nama File';
$lang['spk_tgl_order']	= 'Tanggal Order';
$lang['spk_qty']		= 'Qty';
$lang['spk_act']		= 'Action';
$lang['spk_st_urgent']	= 'Status Urgent';
$lang['spk_nama_konsumen'] = 'Nama Konsumen';
$lang['spk_designer']	 = 'Desainer';
$lang['spk_metode_bayar'] = 'Status Bayar';
$lang['spk_detail_a3'] 	= 'Detail A3';

$lang['spk_nm_barang']			= 'Nama Bahan Baku';
$lang['spk_konversi_satuan'] 	= 'Konversi Satuan';


$lang['spk_btn_save'] 		= 'Ambil Pekerjaan';
$lang['spk_btn_cancel'] 	= 'Kembali';
$lang['spk_or']			= ' Atau ';

// messages
$lang['spk_del_error']			= 'Anda belum memilih data spk yang akan dihapus.';
$lang['spk_del_failure']		= 'Tidak dapat menghapus spk: ';
$lang['spk_delete_confirm']		= 'Apakah anda yakin akan menghapus spk terpilih ?';
$lang['spk_take_confirm']		= 'Yakin untuk mengambil SPK terpilih?';
$lang['spk_deleted']			= 'gudang berhasil dihapus';
$lang['spk_no_records_found'] 	= 'Data tidak ditemukan.';
$lang['spk_sudah_diambil'] 		= 'SPK dengan Nomer: %s sudah diambil user lain.';
$lang['spk_no_item_selected']	= 'Anda belum memilih SPK yang akan diambil';

$lang['spk_create_failure'] 	= 'spk gagal diambil: ';
$lang['spk_create_success'] 	= 'spk berhasil diambil';

$lang['spk_edit_success'] 		= 'spk berhasil disimpan';
$lang['spk_invalid_id'] 		= 'ID Tidak Valid';