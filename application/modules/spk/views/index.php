<div class="box box-primary">
    <!--form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_spk','name'=>'frm_spk','role','class'=>'form-horizontal','enctype'=>'multipart/form-data'))?>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="tdet_order" name="tdet_order">
                <thead>
                    <tr class="success">
                        <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th> 
                        <th width="50">#</th> 
                        <th style="vertical-align:middle; text-align: center;" colspan="2"><?= lang('spk_nama_file') ?></th>
                        <th style="vertical-align:middle; text-align: center;"><?= lang('spk_nama_konsumen') ?></th>
                        <th style="vertical-align:middle; text-align: center;"><?= lang('spk_tgl_order') ?></th>
                        <th width="120px" style="vertical-align:middle;text-align: center;"><?= lang('spk_qty') ?></th>
                        <th style="vertical-align:middle;text-align: center; "><?= lang('spk_st_urgent') ?></th>
                        <th style="vertical-align:middle;text-align: center; "><?= lang('spk_designer') ?></th>
                        <th style="vertical-align:middle;text-align: center; "><?= lang('spk_metode_bayar') ?></th>
                        <th style="vertical-align:middle;text-align: center;width: 50px; "><?= lang('spk_act') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($results) : ?>
                            <?php 
                                $no = 1;
                                foreach($results as $key => $record) :
                            ?>
                        <tr class=<?= $record->st_revisi==1 ? "danger":""?>>
                            <td class="column-check">
                                <input type="checkbox" name="checked[]" value="<?= $record->idspk ?>" />
                            </td> 
                            <td><?= $no++ ?></td>
                            <td width="65px" style="vertical-align:middle;"><img src="<?= base_url($record->thumbnail)?>" width="65" height="65" /></td>
                            <td style="vertical-align:middle;">
                                <p style="font-weight: bold; margin-bottom: 0px;">
                                    <?= strtoupper($record->nama_pekerjaan) ?>
                                </p>
                                <p style="margin-bottom: 0px;">
                                    <?= strtoupper($record->nm_produk) ?>    
                                </p>
                                <p style="margin-bottom: 0px;">
                                    <?= $record->status_finishing ?>
                                </p>
                                <p style="margin-bottom: 0px;" class="text-red">
                                    <?= $record->catatan?>
                                </p>
                            </td>
                            <td style="vertical-align:middle; text-align: center;"><?= $record->nama;?></td>
                            <td style="vertical-align:middle; text-align: center;"> <?= date('d M Y', strtotime($record->tgl_order)) ?></td>
                            <td style="vertical-align:middle; text-align: center;"> <?= $record->jml_cetak ?> </td>
                            <td class="text-center" style="vertical-align:middle; text-align: center;">
                                <?= ($record->st_urgent==1 ? "<span class='label label-danger'>". date("d-m-Y H:i", strtotime($record->tgl_permintaan_selesai)) ."</span>" : "<span class='label label-info'>".date("d-m-Y H:i", strtotime($record->tgl_permintaan_selesai))."</span>")?>
                            </td>
                            <td style="vertical-align:middle; text-align: center;"> <?= $record->nm_lengkap ?> </td>
                            <td style="vertical-align:middle; text-align: center;"><?= strtolower($record->st_lunas) == "lunas" ? "<span class='label label-success'>". $record->st_lunas ."</span>" :  "<span class='label label-warning'>". $record->st_lunas ."</span>" ?></td>
                            <td style="padding-right:20px;text-align: center;vertical-align:middle;"><a class="text-black" href="<?= site_url('spk/view/' . $record->idspk .'/'.$record->id_produk); ?>" data-toggle="tooltip" data-placement="left" title="View Data"><i class="fa fa-folder-open"></i></a></td>
                        </tr>
                    <?php 
                        endforeach;
                    endif;
                    ?>
                </tbody>
            </table>
        </div>
        <div class="form-group">
            <div class="col-sm-9">
                <input type="submit" name="save" id="save" class="btn btn-success" id="delete-me" value="<?php echo lang('spk_btn_save') ?>" onclick="return confirm('<?= lang('spk_take_confirm'); ?>')"> 
            </div>
        </div>
    </div>
    <?= form_close() ?>
</div>
