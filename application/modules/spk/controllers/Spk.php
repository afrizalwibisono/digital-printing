<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for spk
 */

class Spk extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "SPK.View";
    protected $addPermission    = "SPK.Add";
    protected $managePermission = "SPK.Manage";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('spk/spk');
        $this->load->model(['spk_model',
                            'order_produk/morder_produk_detail_model',
                            'order_produk/morder_produk_detail_a3_model',
                            'worksheet/worksheet_model',
                            'produk_detail_penyusun_model',
                            'tmp_produksi_model',
                            'tmp_produksi_detail_model',
                            'order_produk/morder_produk_detail_finishing_model',
                            'produk_detail_finishing_model',
                            'konversi_satuan/konversi_satuan_model',
                            'setting_upload/setting_upload_model',
                            'bom/produk_model']);

        $this->template->title(lang('spk_title_manage'));
		$this->template->page_icon('fa fa-address-card-o');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);
        $idworksheet = $this->uri->segment(3);

        if($idworksheet == 1){
            $a3 = true;
        }else{
            $a3= false;
        }
        if (isset($_POST['save']))
        {
            if ($this->save_spk())
            {
              $this->template->set_message(lang("spk_create_success"), 'success');
              redirect('spk/index/'.$idworksheet);
            }
        }
        if ($idworksheet) {
            //spk from transaksi
            $data_order_non_po = $this->spk_model->select("
            `m_order_produk_detail`.`id_detail_produk_order`,`m_order_produk_detail`.`nama_pekerjaan`,`konsumen`.`nama`,
            `m_order_produk_detail`.`target_server`, `m_order_produk_detail`.`tgl_permintaan_selesai`,`m_order_produk_detail`.`catatan`,
            `m_order_produk_detail`.`id_produk`, `produk`.`nm_produk`, 
            IF(`m_order_produk_detail`.`st_finishing` = 0, 'Finishing Standar', 'Finishing Custom') as status_finishing, 
            `m_order_produk_detail`.`st_urgent`, `thumbnail`, `spk`.`idspk`,`spk`.`jml_cetak`,`spk`.`st_revisi`, `konversi_satuan`.`satuan_besar`, 
            `tgl_order`,`m_order`.`nm_lengkap`,
            IF(`transaksi`.`st_lunas`=0, 'Belum Lunas','Lunas') as st_lunas
            ")
                ->join("m_order_produk_detail","spk.id_detail_produk_order=m_order_produk_detail.id_detail_produk_order","left")
                ->join("order_produk","m_order_produk_detail.id_order=order_produk.id_order","left")
                ->join("konsumen","konsumen.idkonsumen=order_produk.id_konsumen","left")
                ->join("produk","m_order_produk_detail.id_produk = produk.idproduk","left")
                ->join("konversi_satuan","spk.id_konversi_jml_cetak=konversi_satuan.id_konversi","left")
                // ->join("users","users.id_user=m_order_produk_detail.created_by","left")
                ->join("(select 
                            m_order_produk_detail.id_detail_produk_order, created_by, nm_lengkap 
                        from 
                            m_order_produk_detail 
                        left join users on users.id_user=m_order_produk_detail.created_by
                        where 
                            m_order_produk_detail.id_detail_produk_order = m_order_produk_detail.kode_universal) as m_order","m_order.id_detail_produk_order = m_order_produk_detail.kode_universal", "inner")
                ->join("transaksi","transaksi.id_order=spk.id_order","left")
                ->order_by('tgl_permintaan_selesai', 'ASC')
                ->order_by('st_lunas', 'ASC')
                ->find_all_by(['spk.deleted'=> 0,'spk.st_ambil'=> 0,'spk.id_worksheet' => $idworksheet,'transaksi.st_history'=>0, 'transaksi.deleted'=>0,'spk.st_ref'=>0]);

            // spk from Validasi Cetak
            $data_order_po = $this->spk_model->select("
                `m_order_produk_detail`.`id_detail_produk_order`,`m_order_produk_detail`.`nama_pekerjaan`,`konsumen`.`nama`,
                `m_order_produk_detail`.`target_server`, `m_order_produk_detail`.`tgl_permintaan_selesai`,`m_order_produk_detail`.`catatan`,
                `m_order_produk_detail`.`id_produk`, `produk`.`nm_produk`, 
                IF(`m_order_produk_detail`.`st_finishing` = 0, 'Finishing Standar', 'Finishing Custom') as status_finishing, 
                `m_order_produk_detail`.`st_urgent`, `thumbnail`, `spk`.`idspk`,`spk`.`jml_cetak`,`spk`.`st_revisi`, `konversi_satuan`.`satuan_besar`, 
                `tgl_order`, `m_order`.`nm_lengkap`,
                IF(`transaksi`.`st_lunas`=0, 'Belum Lunas','Lunas') as st_lunas
                ")
                    ->join("m_order_produk_detail","spk.id_detail_produk_order=m_order_produk_detail.id_detail_produk_order","left")
                    ->join("order_produk","m_order_produk_detail.id_order=order_produk.id_order","left")
                    ->join("konsumen","konsumen.idkonsumen=order_produk.id_konsumen","left")
                    ->join("produk","m_order_produk_detail.id_produk = produk.idproduk","left")
                    ->join("konversi_satuan","spk.id_konversi_jml_cetak=konversi_satuan.id_konversi","left")
                    ->join("(select 
                            m_order_produk_detail.id_detail_produk_order, created_by, nm_lengkap 
                        from 
                            m_order_produk_detail 
                        left join users on users.id_user=m_order_produk_detail.created_by
                        where 
                            m_order_produk_detail.id_detail_produk_order = m_order_produk_detail.kode_universal) as m_order","m_order.id_detail_produk_order = m_order_produk_detail.kode_universal", "inner")
                    // ->join("users","users.id_user=m_order_produk_detail.created_by","left")
                    ->join("transaksi","transaksi.id_order=spk.id_order","left")
                    ->order_by('spk.created_on','ASC')
                    ->order_by('st_lunas','DESC')
                    ->find_all_by(['spk.deleted'=> 0,'spk.st_ambil'=> 0,
                                    'spk.id_worksheet' => $idworksheet,
                                    'transaksi.st_history'=>0, 'transaksi.deleted'=>0,
                                    'spk.st_ref'=>1]);

            $all_data=[];
            if(is_array($data_order_non_po) && is_array($data_order_po)){
                $all_data = array_merge($data_order_non_po, $data_order_po);
            }elseif(is_array($data_order_po)){
                $all_data = $data_order_po;
            }elseif(is_array($data_order_non_po)){
                $all_data = $data_order_non_po;
            }

            if(is_array($all_data) && count($all_data)>0){
                foreach ($all_data as $key => $record) {
                    $query = $this->morder_produk_detail_a3_model
                    ->order_by('page', 'ASC')
                    ->find_all_by(["id_detail_produk_order"=> $record->id_detail_produk_order]);
                    if (is_array($query) && count($query) > 0) {
                        $all_data[$key]->detail_a3 = $query;
                    }else{
                        $all_data[$key]->detail_a3 = null;
                    }
                }        
            }
            $setting_upload = $this->setting_upload_model->find(1);
            $worksheet = $this->worksheet_model->select("nm_worksheet")->find($idworksheet);
            $this->template->set('results', $all_data);
            $this->template->set('a3', $a3);
            $this->template->set('setting_upload', $setting_upload);
            $this->template->title(lang('spk_title_manage')." ". $worksheet->nm_worksheet);
            $this->template->render('index'); 
        }else{
            show_404();
        }
    }

   	//Create New spk
   	public function create()
   	{

        $this->auth->restrict($this->addPermission);
                    
        if (isset($_POST['save']))
        {

            if ($this->save_spk())
            {
              $this->template->set_message(lang("spk_create_success"), 'success');
              redirect('spk');
            }
        }
        $assets = array(
                'plugins/select2/dist/css/select2.min.css',
                'plugins/select2/dist/js/select2.min.js',
                'spk/assets/js/spk.js',
                );

        add_assets($assets);

        $this->template->set("page_title", lang('spk_title_new'));
   	    $this->template->render('spk_form');
   	}

   	//Edit Supplier
   	public function view()
   	{
   		
  		$this->auth->restrict($this->managePermission);
                
        $id = $this->uri->segment(3);

        if (empty($id))
        {
            $this->template->set_message(lang("spk_invalid_id"), 'error');
            redirect('spk');
        }

        if (isset($_POST['save']))
        {
            if ($this->save_spk('update', $id))
            {
                $this->template->set_message(lang("spk_edit_success"), 'success');
            }

        }
        $data  = $this->spk_model->find_by(array('idspk' => $id, 'deleted' => 0));
        $detail_order = $this->morder_produk_detail_model->find_by(['id_detail_produk_order'=>$data->id_detail_produk_order]);
        $detail_a3 = $this->morder_produk_detail_a3_model
                    ->order_by('page', 'ASC')
                    ->find_all_by(["id_detail_produk_order"=> $data->id_detail_produk_order]);

        $produk = $this->produk_model->find($detail_order->id_produk);
        $material = $this->produk_detail_penyusun_model
                            ->join("barang","produk_detail_penyusun.idbarang_bb=barang.idbarang_bb","left")
                            ->find_all_by(['idproduk'=>$detail_order->id_produk, "produk_detail_penyusun.deleted" =>0]);
        $finishing= $this->produk_detail_finishing_model->find_all_by(['idproduk'=>$detail_order->id_produk]);
        $finishing_tambahan = $this->morder_produk_detail_finishing_model->find_all_by(['id_detail_produk_order'=>$data->id_detail_produk_order]);

        // real qty order
        $data_qty_order = $this->konversi_satuan_model->hitung_ke_satuan_kecil($detail_order->id_konversi_jml_cetak, floatval($detail_order->jml_cetak));
        $rea_qty_order  = $data_qty_order['qty'];
        // real lebar order
        $data_l         = $this->konversi_satuan_model->hitung_ke_satuan_kecil($detail_order->id_satuan, floatval($detail_order->l));
        $real_l         = $data_l['qty'];
        // real panjang order
        $data_p         = $this->konversi_satuan_model->hitung_ke_satuan_kecil($detail_order->id_satuan, floatval($detail_order->p));
        $real_p         = $data_p['qty'];
        $luas           = $real_p * $real_l;

        // bahan baku penyusun
        if ($material) {
            $bahan_baku = array();
            foreach ($material as $key => $record) {
                $tipe = $record->st_tipe;
                if ($tipe==0) {
                    $data_qty_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_jml, floatval($record->jml_pakai));
                    $real_qty_bahan = $data_qty_bahan['qty'];
                    $qty = $rea_qty_order * $real_qty_bahan;
                    $konversi = get_konversi($data_qty_bahan['id_satuan_terkecil'],'',FALSE);
                }elseif($tipe==1){
                    $data_panjang_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->panjang));
                    $real_panjang       = $data_panjang_bahan['qty'];
                    $data_lebar_bahan   = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->lebar));
                    $real_lebar       = $data_lebar_bahan['qty'];
                    $luas_bahan       = $real_panjang * $real_lebar;

                    $qty        = $rea_qty_order * $luas_bahan;
                    $konversi   = get_konversi($data_lebar_bahan['id_satuan_terkecil'],'',FALSE);
                }else{
                    if ($produk) {
                        // 0 = fix 
                        // 1 = potongan
                        $tipe = $produk->st_tipe;   
                        if ($tipe==0) {
                            $qty        = $rea_qty_order;
                            $konversi   = get_konversi(1,'',FALSE);
                        }else{
                            $qty        = $rea_qty_order * $luas;
                            $konversi   = get_konversi(4,'',FALSE);
                        }
                    }
                    
                }
                $bahan_baku[] = array('idbarang_bb' => $record->idbarang_bb,
                                    'nm_barang'=> $record->nm_barang,
                                    'qty' => $qty,
                                    'konversi'=> $konversi);   
            }
        }

        // bahan baku penyusun Finishing
        if (is_array($finishing)) {
            $bahan_baku_finishing = array();
            foreach ($finishing as $key => $dt) {
                $penyusun_finishing = $this->produk_detail_penyusun_model
                            ->join("barang","produk_detail_penyusun.idbarang_bb=barang.idbarang_bb","left")
                            ->find_all_by(['idproduk'=>$dt->id_produk]);
                if ($penyusun_finishing) {
                    foreach ($penyusun_finishing as $key => $record) {
                        $tipe = $record->st_tipe;
                        if ($tipe==0){
                            $data_qty_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_jml, floatval($record->jml_pakai));
                            $real_qty_bahan = $data_qty_bahan['qty'];
                            $qty = $rea_qty_order * $real_qty_bahan;
                            $konversi = get_konversi($data_qty_bahan['id_satuan_terkecil'],'',FALSE);
                        }elseif($tipe==1){
                            $data_panjang_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->panjang));
                            $real_panjang       = $data_panjang_bahan['qty'];
                            $data_lebar_bahan   = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->lebar));
                            $real_lebar       = $data_lebar_bahan['qty'];
                            $luas_bahan       = $real_panjang * $real_lebar;

                            $qty        = $rea_qty_order * $luas_bahan;
                            $konversi   = get_konversi($data_lebar_bahan['id_satuan_terkecil'],'',FALSE);
                        }else{
                            if ($produk) {
                                // 0 = fix 
                                // 1 = potongan
                                $tipe = $produk->st_tipe;   
                                if ($tipe==0) {
                                    $qty        = $rea_qty_order;
                                    $konversi   = get_konversi(1,'',FALSE);
                                }else{
                                    $qty        = $rea_qty_order * $luas;
                                    $konversi   = get_konversi(4,'',FALSE);
                                }
                            }
                            
                        }
                        $bahan_baku_finishing[] = array('idbarang_bb' => $record->idbarang_bb,
                                                    'nm_barang'=> $record->nm_barang,
                                                    'qty' => $qty,
                                                    'konversi'=> $konversi);   
                    }
                }   
            }
            $this->template->set('bahan_baku_finishing', $bahan_baku_finishing);
        }

        // bahan baku penyusun Finishing tambahan
        if (is_array($finishing_tambahan)) {
            $bahan_baku_finishing_tambahan = array();
            foreach ($finishing_tambahan as $key => $dt) {
                $penyusun_finishing_tambahan = $this->produk_detail_penyusun_model
                            ->join("barang","produk_detail_penyusun.idbarang_bb=barang.idbarang_bb","left")
                            ->find_all_by(['idproduk'=> $dt->id_produk ]);
                if ($penyusun_finishing_tambahan) {
                    foreach ($penyusun_finishing_tambahan as $key => $record) {
                        $tipe = $record->st_tipe;
                        if ($tipe==0){
                            $data_qty_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_jml, floatval($record->jml_pakai));
                            $real_qty_bahan = $data_qty_bahan['qty'];
                            $qty = $rea_qty_order * $real_qty_bahan;
                            $konversi = get_konversi($data_qty_bahan['id_satuan_terkecil'],'',FALSE);
                        }elseif($tipe==1){
                            $data_panjang_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->panjang));
                            $real_panjang       = $data_panjang_bahan['qty'];
                            $data_lebar_bahan   = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->lebar));
                            $real_lebar       = $data_lebar_bahan['qty'];
                            $luas_bahan       = $real_panjang * $real_lebar;

                            $qty        = $rea_qty_order * $luas_bahan;
                            $konversi   = get_konversi($data_lebar_bahan['id_satuan_terkecil'],'',FALSE);
                        }else{
                            if ($produk) {
                                // 0 = fix 
                                // 1 = potongan
                                $tipe = $produk->st_tipe;   
                                if ($tipe==0) {
                                    $qty        = $rea_qty_order;
                                    $konversi   = get_konversi(1,'',FALSE);
                                }else{
                                    $qty        = $rea_qty_order * $luas;
                                    $konversi   = get_konversi(4,'',FALSE);
                                }
                            }
                            
                        }
                        $bahan_baku_finishing_tambahan[] = array('idbarang_bb' => $record->idbarang_bb,
                                                    'nm_barang'=> $record->nm_barang,
                                                    'qty' => $qty,
                                                    'konversi'=> $konversi);   
                    }
                }   
            }
            $this->template->set('bahan_baku_finishing_tambahan', $bahan_baku_finishing_tambahan);
        }

        if(!$data)
        {
            $this->template->set_message(lang("spk_invalid_id"), 'error');
            redirect('spk');
        }
        $assets = array(
                'plugins/select2/dist/css/select2.min.css',
                'plugins/select2/dist/js/select2.min.js',
                'plugins/number/jquery.number.js',
                'spk/assets/js/spk.js',
                );

        add_assets($assets);
        $this->template->set('data', $data);
        $this->template->set('bahan_baku', $bahan_baku);
        $this->template->set('detail_a3', is_array($detail_a3) ? $detail_a3 : '');
        $this->template->title(lang('spk_title_view'));
        $this->template->render('form');
   	}

   	protected function save_spk()
   	{
        if (isset($_POST['save']) && has_permission($this->addPermission))
        {   
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {

                $this->db->trans_start();
                foreach ($checked as $pid)
                {
                    $data  = $this->spk_model->select("spk.*, transaksi.id_transaksi")
                            ->join("transaksi","transaksi.id_order=spk.id_order","left")
                            ->find_by(array('idspk' => $pid, 'spk.deleted' => 0,'transaksi.deleted'=>0,'transaksi.st_history=0'));
                    if ($data->st_ambil!=0 || $data->deleted == 1 ) {
                        $this->template->set_message(sprintf(lang('spk_sudah_diambil'), $pid), 'error');
                        $this->db->trans_rollback();
                        return FALSE;
                    }

                    // update spk
                    $userdata = $this->auth->userdata();
                    $iduser   = $userdata->id_user;
                    $update_spk = $this->spk_model->update($pid, ['st_ambil'=>1,'id_user'=> $iduser,'waktu_ambil'=> date("Y-m-d H:i:s") ]); 
                    $query = $this->db->last_query();
                    set_proses_order($id_lokasi = 1, $id_ket_proses = 3, $id_detail_produk_order = $data->id_detail_produk_order, $jumlah_proses = $data->jml_cetak);
                    if ($update_spk !== FALSE)
                    {
                        $keterangan = "SUKSES, Update st_ambail SPK, dengan ID : ".$pid;
                        $status     = 1;
                    }
                    else
                    {
                        $keterangan = "GAGAL, Update st_ambail SPK, dengan ID : ".$pid;
                        $status     = 0;
                    }
                    //Save Log
                    $nm_hak_akses   = $this->managePermission; 
                    $kode_universal = $pid;
                    $jumlah         = 1;
                    $sql            = $query;

                    simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

                    // isert data into tabel tmp_produk
                    $data_tmp = array('idtmp_produksi'=> gen_primary('TMPP'),
                                    'id_transaksi' => $data->id_transaksi,
                                    'id_order'     => $data->id_order,
                                    'id_detail_order'=> $data->id_detail_produk_order,
                                    'idspk'        => $data->idspk);
                    $add_tmp_produksi = $this->tmp_produksi_model->insert($data_tmp);

                    $query = $this->db->last_query();
                    if ($add_tmp_produksi !== FALSE)
                    {
                        $keterangan = "SUKSES, Tambah Data tmp_produksi dengan ID : ".$data_tmp['idtmp_produksi'];
                        $status     = 1;
                    }
                    else
                    {
                        $keterangan = "GAGAL, Tambah Data tmp_produksi dengan ID : ".$data_tmp['idtmp_produksi'];
                        $status     = 0;
                    }
                    //Save Log
                    $nm_hak_akses   = $this->managePermission; 
                    $kode_universal = $data_tmp['idtmp_produksi'];
                    $jumlah         = 1;
                    $sql            = $query;

                    simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

                    $spk = $this->spk_model->find($pid);
                    if ($spk->id_worksheet != 5) {
                        $detail_order = $this->morder_produk_detail_model->find_by(['id_detail_produk_order'=>$data->id_detail_produk_order]);
                        $produk = $this->produk_model->find($detail_order->id_produk);
                        $material = $this->produk_detail_penyusun_model
                                            ->join("barang","produk_detail_penyusun.idbarang_bb=barang.idbarang_bb","left")
                                            ->find_all_by(['idproduk'=>$detail_order->id_produk]);
                        $finishing= $this->produk_detail_finishing_model->find_all_by(['idproduk'=>$detail_order->id_produk]);
                        $finishing_tambahan = $this->morder_produk_detail_finishing_model->find_all_by(['id_detail_produk_order'=>$data->id_detail_produk_order]);

                        // real qty order
                        $data_qty_order = $this->konversi_satuan_model->hitung_ke_satuan_kecil($detail_order->id_konversi_jml_cetak, floatval($detail_order->jml_cetak));
                        $rea_qty_order  = $data_qty_order['qty'];
                        // real lebar order
                        $data_l         = $this->konversi_satuan_model->hitung_ke_satuan_kecil($detail_order->id_satuan, floatval($detail_order->l));
                        $real_l         = $data_l['qty'];
                        // real panjang order
                        $data_p         = $this->konversi_satuan_model->hitung_ke_satuan_kecil($detail_order->id_satuan, floatval($detail_order->p));
                        $real_p         = $data_p['qty'];
                        $luas           = $real_p * $real_l;

                        // bahan baku penyusun
                        if ($material) {
                            $bahan_baku = array();
                            $bahan_baku_finishing = array();
                            foreach ($material as $key => $record) {
                                $tipe = $record->st_tipe;
                                if ($tipe==0) {
                                    $data_qty_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_jml, floatval($record->jml_pakai));
                                    $real_qty_bahan = $data_qty_bahan['qty'];
                                    $qty = $rea_qty_order * $real_qty_bahan;
                                    $konversi = get_konversi($data_qty_bahan['id_satuan_terkecil'],'',FALSE);
                                }elseif($tipe==1){
                                    $data_panjang_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->panjang));
                                    $real_panjang       = $data_panjang_bahan['qty'];
                                    $data_lebar_bahan   = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->lebar));
                                    $real_lebar       = $data_lebar_bahan['qty'];
                                    $luas_bahan       = $real_panjang * $real_lebar;

                                    $qty        = $rea_qty_order * $luas_bahan;
                                    $konversi   = get_konversi($data_lebar_bahan['id_satuan_terkecil'],'',FALSE);
                                }else{
                                    if ($produk) {
                                        // 0 = fix 
                                        // 1 = potongan
                                        $tipe = $produk->st_tipe;   
                                        if ($tipe==0) {
                                            $qty        = $rea_qty_order;
                                            $konversi   = get_konversi(1,'',FALSE);
                                        }else{
                                            $qty        = $rea_qty_order * $luas;
                                            $konversi   = get_konversi(4,'',FALSE);
                                        }
                                    }
                                    
                                }
                                $bahan_baku[] = array('idtmp_produksi_detail'=>gen_primary('DTMP'),
                                                    'idtmp_produksi'=> $data_tmp['idtmp_produksi'],
                                                    'idbarang_bb' => $record->idbarang_bb,
                                                    'qty' => $qty,
                                                    'id_konversi'=> $record->id_satuan_terkecil,
                                                    'tipe'=>0);   
                            }
                        }

                        // bahan baku penyusun Finishing
                        if (is_array($finishing)) {
                            foreach ($finishing as $key => $dt) {
                                $penyusun_finishing = $this->produk_detail_penyusun_model
                                            ->join("barang","produk_detail_penyusun.idbarang_bb=barang.idbarang_bb","left")
                                            ->find_all_by(['idproduk'=>$dt->id_produk]);
                                if ($penyusun_finishing) {
                                    foreach ($penyusun_finishing as $key => $record) {
                                        $tipe = $record->st_tipe;
                                        if ($tipe==0){
                                            $data_qty_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_jml, floatval($record->jml_pakai));
                                            $real_qty_bahan = $data_qty_bahan['qty'];
                                            $qty = $rea_qty_order * $real_qty_bahan;
                                            $konversi = get_konversi($data_qty_bahan['id_satuan_terkecil'],'',FALSE);
                                        }elseif($tipe==1){
                                            $data_panjang_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->panjang));
                                            $real_panjang       = $data_panjang_bahan['qty'];
                                            $data_lebar_bahan   = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->lebar));
                                            $real_lebar       = $data_lebar_bahan['qty'];
                                            $luas_bahan       = $real_panjang * $real_lebar;

                                            $qty        = $rea_qty_order * $luas_bahan;
                                            $konversi   = get_konversi($data_lebar_bahan['id_satuan_terkecil'],'',FALSE);
                                        }else{
                                            if ($produk) {
                                                // 0 = fix 
                                                // 1 = potongan
                                                $tipe = $produk->st_tipe;   
                                                if ($tipe==0) {
                                                    $qty        = $rea_qty_order;
                                                    $konversi   = get_konversi(1,'',FALSE);
                                                }else{
                                                    $qty        = $rea_qty_order * $luas;
                                                    $konversi   = get_konversi(4,'',FALSE);
                                                }
                                            }
                                            
                                        }
                                        $bahan_baku_finishing[] = array('idtmp_produksi_detail'=>gen_primary('DTMP'),
                                                                    'idtmp_produksi'=> $data_tmp['idtmp_produksi'],
                                                                    'idbarang_bb' => $record->idbarang_bb,
                                                                    'qty' => $qty,
                                                                    'id_konversi'=> $record->id_satuan_terkecil,
                                                                    'tipe'=> 1);   
                                    }
                                }   
                            }
                        }
                        // bahan baku penyusun Finishing tambahan
                        if (is_array($finishing_tambahan)) {
                            foreach ($finishing_tambahan as $key => $dt) {
                                $penyusun_finishing_tambahan = $this->produk_detail_penyusun_model
                                            ->join("barang","produk_detail_penyusun.idbarang_bb=barang.idbarang_bb","left")
                                            ->find_all_by(['idproduk'=> $dt->id_produk ]);
                                if ($penyusun_finishing_tambahan) {
                                    foreach ($penyusun_finishing_tambahan as $key => $record) {
                                        $tipe = $record->st_tipe;
                                        if ($tipe==0){
                                            $data_qty_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_jml, floatval($record->jml_pakai));
                                            $real_qty_bahan = $data_qty_bahan['qty'];
                                            $qty = $rea_qty_order * $real_qty_bahan;
                                            $konversi = get_konversi($data_qty_bahan['id_satuan_terkecil'],'',FALSE);
                                        }elseif($tipe==1){
                                            $data_panjang_bahan = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->panjang));
                                            $real_panjang       = $data_panjang_bahan['qty'];
                                            $data_lebar_bahan   = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_konversi_panjang, floatval($record->lebar));
                                            $real_lebar       = $data_lebar_bahan['qty'];
                                            $luas_bahan       = $real_panjang * $real_lebar;

                                            $qty        = $rea_qty_order * $luas_bahan;
                                            $konversi   = get_konversi($data_lebar_bahan['id_satuan_terkecil'],'',FALSE);
                                        }else{
                                            if ($produk) {
                                                // 0 = fix 
                                                // 1 = potongan
                                                $tipe = $produk->st_tipe;   
                                                if ($tipe==0) {
                                                    $qty        = $rea_qty_order;
                                                    $konversi   = get_konversi(1,'',FALSE);
                                                }else{
                                                    $qty        = $rea_qty_order * $luas;
                                                    $konversi   = get_konversi(4,'',FALSE);
                                                }
                                            }
                                            
                                        }
                                        $bahan_baku_finishing[] = array(
                                                                    'idtmp_produksi_detail'=>gen_primary('DTMP'),
                                                                    'idtmp_produksi'=> $data_tmp['idtmp_produksi'],
                                                                    'idbarang_bb' => $record->idbarang_bb,
                                                                    'qty' => $qty,
                                                                    'id_konversi'=> $record->id_satuan_terkecil,
                                                                    'tipe'=> 1);   
                                    }
                                }   
                            }
                        }
                        $bb = array_merge($bahan_baku,$bahan_baku_finishing);
                        // simpan data detail tmp produksi
                        $add_tmp_produksi_detail = $this->tmp_produksi_detail_model->insert_batch($bb);
                        $query = $this->db->last_query();
                        if ($add_tmp_produksi_detail !== FALSE)
                        {
                            $keterangan = "SUKSES, Tambah Data tmp_produksi dengan ID : ".$data_tmp['idtmp_produksi'];
                            $status     = 1;
                        }
                        else
                        {
                            $keterangan = "GAGAL, Tambah Data tmp_produksi dengan ID : ".$data_tmp['idtmp_produksi'];
                            $status     = 0;
                        }
                        //Save Log
                        $nm_hak_akses   = $this->managePermission; 
                        $kode_universal = $data_tmp['idtmp_produksi'];
                        $jumlah         = 1;
                        $sql            = $query;

                        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                    }
                }
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE)
                {
                    $this->template->set_message($this->order_produk_model->error."\n\n".$this->morder_produk_detail_finishing_model->error."\n\n".$this->morder_produk_detail_model->error, 'error');
                    return FALSE;
                }
                else
                {
                    return TRUE;
                }
            }else{
                $this->template->set_message(lang('spk_no_item_selected'), 'error');
                return FALSE;
            }
        }
    }
}
?>