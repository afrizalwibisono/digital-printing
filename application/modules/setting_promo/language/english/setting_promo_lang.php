<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ============================
$lang['setting_promo_title_manage']		= 'Data Setting Promo';
$lang['setting_promo_title_new']		= 'Data Setting Promo Baru';
$lang['setting_promo_title_edit']		= 'Edit Data Setting Promo';
// from/table
$lang['setting_promo_id_produk']		= 'Nama Produk';
$lang['setting_promo_id_kategori']		= 'Kategori Produk';
$lang['setting_promo_tgl_awal']			= 'Tanggal Promo Mulai';
$lang['setting_promo_tgl_akhir']		= 'Tanggal Promo Berakhir';


//label
$lang['setting_promo_status']		= 'Status';
$lang['setting_promo_aktive']		= 'Aktif';
$lang['setting_promo_not_active']	= 'Tidak Aktif';


//button
$lang['setting_promo_btn_new']			= 'Baru';
$lang['setting_promo_btn_delete']		= 'Hapus';
$lang['setting_promo_btn_save']			= 'Simpan';
$lang['setting_promo_btn_cancel']		= 'Batal';
$lang['setting_promo_btn_add']			= 'Tambah';
$lang['setting_promo_or']				= 'Atau';

//messages
$lang['setting_promo_del_error']			= 'Anda belum memilih data setting promo yang akan dihapus.';
$lang['setting_promo_del_failure']			= 'Tidak dapat menghapus data setting promo';
$lang['setting_promo_delete_confirm']		= 'Apakah anda yakin akan menghapus data setting promo terpilih?';
$lang['setting_promo_deleted']				= 'Data setting promo berhasil dihaspus';
$lang['setting_promo_no_records_found']		= 'Data tidak ditemukan.';

$lang['setting_promo_create_failure']		= 'Data setting promo gagal disimpan';
$lang['setting_promo_create_success']		= 'Data setting promo berhasil disimpan';

$lang['setting_promo_edit_success']			= 'Data setting promo berhasil disimpan';
$lang['setting_promo_invalid_id']			= 'ID tidak Valid';