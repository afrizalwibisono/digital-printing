<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 * 
 * This is controller for setting_promo

 */

class setting_promo extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Setting Promo.View";
    protected $addPermission    = "Setting Promo.Add";
    protected $managePermission = "Setting Promo.Manage";
    protected $deletePermission = "Setting Promo.Delete";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('setting_promo/setting_promo');
        $this->load->model(array(
                                'setting_promo_model','produk_model'
                            ));

        $this->template->title(lang('setting_promo_title_manage'));
        $this->template->page_icon('fa fa-table');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if (isset($_POST['delete']) && has_permission($this->deletePermission))
        {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                foreach ($checked as $pid)
                {
                    $result         = $this->setting_promo_model->delete($pid);
                }

                if($result)
                {
                    $keterangan = "SUKSES, hapus data setting promo dengan id : ".$pid;
                    $status     = 1;
                }
                else
                {
                    $keterangan = "GAGAL, hapus data setting promo dengan id : ".$pid;
                    $status     = 0;
                }

                $nm_hak_akses   = $this->deletePermission; 
                $kode_universal = $pid;
                $jumlah         = 1;
                $sql            = $this->db->last_query();

                simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                if ($result)
                {
                    $this->template->set_message(count($checked) .' '. lang('setting_promo_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('konversi_del_failure') . $this->setting_promo_model->error, 'error');
                }
            }
            else
            {
                $this->template->set_message(lang('setting_promo_del_error') . $this->setting_promo_model->error, 'error');
            }
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search     = isset($_POST['table_search']) ? $this->input->post('table_search') :'';
            $tgl_awal   = isset($_POST['tgl_awal']) ? $this->input->post('tgl_awal') : '';
            $tgl_akhir  = isset($_POST['tgl_awal']) ? $this->input->post('tgl_akhir') : '';
            $id_customer= isset($_POST['id_customer']) ? $this->input->post('id_customer') :'';
        }
        else
        {
            $search     = isset($_GET['search']) ? $this->input->get('search') :'';
            $tgl_awal   = isset($_GET['tgl_awal']) ? $this->input->get('tgl_awal') : '';
            $tgl_akhir  = isset($_GET['tgl_awal']) ? $this->input->get('tgl_akhir') : '';
            $id_customer= isset($_GET['id_customer']) ? $this->input->get('id_customer') :'';
        }

        $filter = "";
        if($search!="")
        {
            $filter = "?search=".$search;
        }
        $addWhere = "";
        if($tgl_awal != '' && $tgl_akhir != '')
        {
            $filter   .= "&tgl_awal=".$tgl_awal."&tgl_akhir=".$tgl_akhir;
            $addWhere .= " AND ( date(tgl_awal) >='".$tgl_awal."' AND date(tgl_akhir) >='".$tgl_akhir."')";
        }
        else
        {
            $tgl_awal = date('Y-m-d');
            $tgl_akhir = date('Y-m-d');
            
            $filter   .= "&tgl_awal=".$tgl_awal."&tgl_akhir=".$tgl_akhir;
            $addWhere .= " AND ( date(tgl_awal) >='".$tgl_awal."' AND date(tgl_akhir) >='".$tgl_akhir."')";
        }
        $search2 = $this->db->escape_str($search);
        
        $where = "setting_promo.deleted = 0 $addWhere
                AND (`nm_produk` LIKE '%$search2%' ESCAPE '!'
                OR `nmkategori` LIKE '%$search2%' ESCAPE '!')";
                
        $total = $this->setting_promo_model
                    ->join("produk","produk.idproduk=setting_promo.id_produk","left")
                    ->join("kategori","kategori.idkategori=produk.idkategori")
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $assets = array('plugins/select2/dist/js/select2.js', 
                        'plugins/select2/dist/css/select2.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js', 
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
                        'setting_promo/assets/js/setting_promo.js'
                        );
        
        add_assets($assets);

        $data = $this->setting_promo_model
                    ->join("produk","produk.idproduk=setting_promo.id_produk","left")
                    ->join("kategori","kategori.idkategori=produk.idkategori")
                    ->where($where)
                    ->order_by('nm_produk','ASC')
                    ->limit($limit, $offset)->find_all();

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('tgl_awal',$tgl_awal);
        $this->template->set('tgl_akhir',$tgl_akhir);
        $this->template->set("toolbar_title", lang('setting_promo_title_manage'));
        $this->template->title(lang('setting_promo_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

    public function create()
    {

        $this->auth->restrict($this->addPermission);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_setting_promo())
            {
              $this->template->set_message(lang("setting_promo_create_success"), 'success');
              redirect('setting_promo');
            }
        }
        $assets = array('plugins/select2/dist/js/select2.js', 
                        'plugins/select2/dist/css/select2.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js', 
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
                        'setting_promo/assets/js/setting_promo.js'
                        );
        
        add_assets($assets);
        $produk = $this->produk_model
                        ->join('kategori','produk.idkategori=kategori.idkategori','left')
                        ->find_all_by(['produk.deleted'=>0]);
        $this->template->set('produk',$produk);
        $this->template->set("page_title", lang('setting_promo_title_new'));
        $this->template->render('form');
    }

    protected function save_setting_promo($type='insert', $id=0)
    {

        $this->form_validation->set_rules('id_produk','lang:setting_promo_id_produk','callback_default_select');
        $this->form_validation->set_rules('tgl_awal','lang:setting_promo_tgl_awal','date|required');
        $this->form_validation->set_rules('tgl_akhir','lang:setting_promo_tgl_akhir','date|required');


        $this->form_validation->set_message('default_select', 'You should choose the "%s" field');

        if ($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        unset($_POST['submit'], $_POST['save']);

        if ($type == 'insert')
        {
            $id = $this->setting_promo_model->insert($_POST);

            if (is_numeric($id))
            {
                $return = TRUE;
            }
            else
            {
                $return = FALSE;
            }
        }
        else if ($type == 'update')
        {
            $return = $this->setting_promo_model->update($id, $_POST);
        }

        return $return;
    }

    public function edit()
    {
        
        $this->auth->restrict($this->managePermission);
                
        $id = (int)$this->uri->segment(3);

        if (empty($id))
        {
            $this->template->set_message(lang("setting_promo_invalid_id"), 'error');
            redirect('setting_promo');
        }

        if (isset($_POST['save']))
        {
            if ($this->save_setting_promo('update', $id))
            {
                $this->template->set_message(lang("setting_promo_edit_success"), 'success');
            }

        }

        $assets = array('plugins/select2/dist/js/select2.js', 
                        'plugins/select2/dist/css/select2.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js', 
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
                        'setting_promo/assets/js/setting_promo.js'
                        );
        
        add_assets($assets);

        $data  = $this->setting_promo_model->find_by(array('id_setting_promo' => $id));

        if(!$data)
        {
            $this->template->set_message(lang("setting_promo_invalid_id"), 'error');
            redirect('setting_promo');
        }
        $produk = $this->produk_model
                        ->join('kategori','produk.idkategori=kategori.idkategori','left')
                        ->find_all_by(['produk.deleted'=>0]);

        
        $this->template->set('data', $data);
        $this->template->set('produk',$produk);
        $this->template->set('page_title', lang("setting_promo_title_edit"));
        $this->template->render('form');
    }
    public function default_select($val)
    {
        return $val == "" ? FALSE : TRUE;
    }

}
?>