$(document).ready(function(){  
  $("#id_produk").select2({placeholder : '-Pilih Produk-', allowclear: true});
  $('#tgl_awal,#tgl_akhir').datepicker({
      todayBtn: "linked",
      format: "yyyy-mm-dd",
      autoclose: true,
      clearBtn: true,
      todayHighlight: true
  });
});