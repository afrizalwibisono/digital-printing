<?php
	$ENABLE_ADD		= has_permission('Setting Promo.Add');
	$ENABLE_MANAGE	= has_permission('Setting Promo.Manage');
	$ENABLE_DELETE	= has_permission('Setting Promo.Delete');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_setting_promo','name'=>'frm_setting_promo','class'=>'form-inline')) ?>
	<div class="box-header">
		<?php if ($ENABLE_ADD) : ?>
            <div class="pull-left">
                <a href="<?= site_url('setting_promo/create') ?>" class="btn btn-success" title="<?= lang('setting_promo_btn_new') ?>"><?= lang('setting_promo_btn_new') ?></a>
            </div>
            <?php endif; ?>
            <div class="pull-right">
                <div class="form-group">
                    <div class="input-daterange input-group">
                        <input type="text" class="form-control" name="tgl_awal" id="tgl_awal" value="<?= isset($tgl_awal) && $tgl_awal !='' ? $tgl_awal : '' ?>" placeholder="<?= lang('penjulan_tgl_awal') ?>" required readonly/>
                        <span class="input-group-addon text-black">to</span>
                        <input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?= isset($tgl_akhir) && $tgl_akhir !='' ? $tgl_akhir : '' ?>" placeholder="<?= lang('setting_promo_tgl_akhir') ?>" required readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="Search" autofocus />
                        <div class="input-group-btn">
                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
	</div>
	
	<?php if (isset($results) && is_array($results) && count($results)>0) : ?>
		<div class="box-body table-responsive no-padding">
	            <table class="table table-hover table-striped">
	                <thead>
	                    <tr>
	                        <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
	                        <th width="50">#</th>
	                        <th><?= lang('setting_promo_id_produk') ?></th>
	                        <th><?= lang('setting_promo_id_kategori') ?></th>
	                        <th><?= lang('setting_promo_tgl_awal') ?></th>
	                        <th><?= lang('setting_promo_tgl_akhir') ?></th>
	                        <th><?= lang('setting_promo_status') ?></th>
	                        <?php if($ENABLE_MANAGE) : ?>
	                        <th width="25"></th>
	                        <?php endif; ?>
	                    </tr>
	                </thead>
	                <tbody>
	                    <?php foreach ($results as $record) : ?>
	                    	<!-- <?php echo"<pre>",print_r($record),"</pre>"?> -->
	                    <tr>
	                        <td class="column-check"><input type="checkbox" name="checked[]" value="<?= $record->id_setting_promo ?>" /></td>
	                        <td><?= $numb; ?></td>
	                        <td><?= $record->nm_produk ?></td>
	                       	<td><?= $record->nmkategori ?></td>
	                        <td><?= $record->tgl_awal ?></td>
	                        <td><?= $record->tgl_akhir ?></td>
	                        <td><?=  (date('Y-m-d', strtotime($record->tgl_awal)) <= date("Y-m-d") && date('Y-m-d', strtotime($record->tgl_akhir)) >= date("Y-m-d") )?'<span class="label label-success">Aktif</span>':'<span class="label label-danger">Tidak Aktif</span>';?></td>
	                        <?php if($ENABLE_MANAGE) : ?>
	                            <td style="padding-right:20px"><a class="text-black" href="<?= site_url('setting_promo/edit/' . $record->id_setting_promo); ?>" data-toggle="tooltip" data-placement="left" title="Edit Setting"><i class="fa fa-pencil"></i></a></td>
	                        <?php endif; ?>
	                    </tr>
	                    <?php $numb++; endforeach; ?>
	                </tbody>
		  </table>
		</div><!-- /.box-body -->
		<div class="box-footer clearfix">
			<?php if($ENABLE_DELETE) : ?>
			<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('setting_promo_btn_delete') ?>" onclick="return confirm('<?= lang('setting_promo_delete_confirm'); ?>')">
			<?php endif;
			echo $this->pagination->create_links(); 
			?>
		</div>
	<?php else: ?>
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('setting_promo_no_records_found') ?></p>
    </div>
    <?php
	endif;
	echo form_close(); 
	?>	
</div><!-- /.box-->