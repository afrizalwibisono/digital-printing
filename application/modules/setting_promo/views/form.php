<div class="box box-primary">
    <!--form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_setting_promo','name'=>'frm_setting_promo','role','class'=>'form-horizontal'))?>
    <div class="box-body">
        <div class="form-group <?= form_error('id_produk') ? ' has-error' : ''; ?>">
                <label for="id_produk" class="col-sm-3 control-label"><?= lang('setting_promo_id_produk') ?></label>
                <div class="col-sm-4">
                    <select id="id_produk" name="id_produk" class="form-control">
                        <option></option>
                        <?php foreach ($produk as $key => $record) : ?>
                        <option value="<?= $record->idproduk; ?>" <?= set_select('id_produk', $record->idproduk, isset($data->id_produk) && $data->id_produk == $record->idproduk) ?>><?= $record->nm_produk ." |  ". $record->nmkategori ?></option> 
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group <?= form_error('tgl_awal') ? ' has-error' : ''; ?>">
                <label for="tgl_awal" class="col-sm-3 control-label"><?= lang('setting_promo_tgl_awal') ?></label>
                <div class="col-sm-3">
                    <input class="form-control" type="text" name="tgl_awal" id="tgl_awal" value="<?= set_value('tgl_awal', isset($data->tgl_awal)?$data->tgl_awal:'')?>" readonly>
                </div>
            </div>
            <div class="form-group <?= form_error('tgl_akhir') ? ' has-error' : ''; ?>">
                <label for="tgl_akhir" class="col-sm-3 control-label"><?= lang('setting_promo_tgl_akhir') ?></label>
                <div class="col-sm-3">
                    <input class="form-control" type="text" name="tgl_akhir" id="tgl_akhir" value="<?= set_value('tgl_akhir', isset($data->tgl_akhir)?$data->tgl_akhir:'')?>" readonly>
                </div>
            </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" name="save" class="btn btn-primary"><?= lang('setting_promo_btn_save') ?></button>
                <?php
                echo lang('setting_promo_or') . ' ' . anchor('setting_promo', lang('setting_promo_btn_cancel'));
                ?>
            </div>
        </div>
    </div>
    <?= form_close() ?>
</div>