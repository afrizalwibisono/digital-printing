<?php
		
		$enableViewAll 	= has_permission("Kas Kasir All Data.View");
		$enableAdd 		= has_permission("Kas Kasir.Add");
		$enabelManage 	= has_permission("Kas Kasir.Manage");

		if($this->uri->segment(3) === false){

    		$id 	= "";

    	}else{

    		$id 	= urldecode($this->uri->segment(3));

    	}

?>

<div class="box box-primary">
	
	<?= form_open($this->uri->uri_string(), [
												'name' 		=> 'frm_index',
												'id'		=> 'frm_index',
												'role'		=> 'form',
												'class' 	=> 'form-horizontal'
																								
											]); 
	?>
	<div class="box-header">
			
		<div class="box-body">
			
			<div class="col-sm-6">
				<div class="form-group" style="margin-bottom: 5px">
					<label class="control-label col-sm-3" for="tgl1" ><?= lang("capt-ind-search-range") ?></label>
					<div class="col-sm-9">
						<div class="input-group input-date-range">
							<input type="text" name="tgl1" id="tgl1" value="<?= $tgl1 ?>" class="form-control">
							<span class="input-group-addon">s/d</span>
							<input type="text" name="tgl2" id="tgl2" value="<?= $tgl2 ?>" class="form-control">
						</div>
					</div>
				</div>

				<?php if($enableViewAll): ?>

				<div class="form-group" style="margin-bottom: 5px">
					<label class="control-label col-sm-3" for="tgl1" ><?= lang("capt-ind-search-user") ?></label>
					<div class="col-sm-7">
						<select name="karyawan" id="karyawan" class="form-control"> 
							<option></option>
							<?php 
								if(isset($dt_karyawan) && is_array($dt_karyawan) && count($dt_karyawan)):
									foreach ($dt_karyawan as $key => $dt) :
							?>
								<option value="<?= $dt->id_user ?>" <?= set_select("karyawan",$dt->id_user,$dt->id_user==$iduser) ?> ><?= ucfirst($dt->nama) ?></option>
							<?php
									endforeach;
								endif;
							?>
						</select>
					</div>
				</div>				

				<?php endif; ?>

			</div>	
			<div class="col-sm-6">
				<div class="form-group" style="margin-bottom: 5px">
					<label class="control-label col-sm-3" for="tgl1" ><?= lang("capt-ind-search-cr-bayar") ?></label>
					<div class="col-sm-8">
						<select class="form-control" name="metode" id="metode">
							<option value="0" <?= set_select("metode","0",$metode == "0") ?> ><?= lang("capt-ind-search-cr-bayar-cash") ?></option>
							<option value="1" <?= set_select("metode","1",$metode == "1") ?> ><?= lang("capt-ind-search-cr-bayar-debit") ?></option>
							<option value="2" <?= set_select("metode","2",$metode == "2") ?>><?= lang("capt-ind-search-cr-bayar-transfer") ?></option>
							<option value="3" <?= set_select("metode","3",$metode == "3") ?>><?= lang("capt-ind-search-cr-bayar-semua") ?></option>
						</select>	
					</div>
				</div>
			</div>	

		</div>
		
		<div class="box-footer">

			<a href="<?= site_url('kas_kasir/create') ?>" class="btn btn-success">
				<?= lang("btn-create") ?>
			</a>

			<?php if($enabelManage): ?>

			<a href="<?= site_url('kas_kasir/closing') ?>" class="btn btn-danger">
				<?= lang("btn-close-trans") ?>
			</a>			
			
			<?php endif ?>

			<div class="pull-right">

				<button class="btn btn-primary"><?= lang("btn-filter") ?> <span class="fa fa-search"></span></button>

			</div>
				
		</div>

		
		

	</div>

	<?php if(isset($data) && is_array($data) && count($data)): ?>
	<div class="box-body table-responsive">

		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th width="30" class="text-center">#</th>
					<th class="text-center"><?= lang("capt-ind-tbl-waktu") ?></th>
					<th class="text-center"><?= lang("capt-ind-tbl-ket") ?></th>
					<th class="text-center"><?= lang("capt-ind-tbl-metode") ?></th>
					<th class="text-center"><?= lang("capt-ind-tbl-debit") ?></th>
					<th class="text-center"><?= lang("capt-ind-tbl-kredit") ?></th>
					<th class="text-center"><?= lang("capt-ind-tbl-kasir") ?></th>
				</tr>
			</thead>
			<tbody>
				<?php
						foreach ($data as $key => $dt) :
				?>
					<tr>
						<td class="text-right"><?= $numb ?></td>
						<td class="text-center"><?= $dt->waktu ?></td>
						<td ><?= $dt->ket ?></td>
						<?php 
								switch ($dt->st_metode) {
									case 0:
										$metode 	= "<span class='label label-success'>Cash / Tunai</span>";	
										break;
									case 1:
										$metode 	= "<span class='label label-warning'>Debit</span>";	
										break;
									case 2:
										$metode 	= "<span class='label label-primary'>Transfer</span>";	
										break;
								}
						?>
						<td class="text-center"><?= $metode ?></td>
						<?php 
								switch ($dt->status) {
									case 0:
										$value = "-";
										break;
									case 1:
										$value = number_format($dt->jumlah);
										break;
									
								}
						?>
						<td class="text-right"><?= $value ?></td>
						<?php 
								switch ($dt->status) {
									case 0:
										$value = number_format($dt->jumlah);
										break;
									case 1:
										$value = "-";
										break;
									
								}
						?>
						<td class="text-right"><?= $value ?></td>
						<td class="text-center"><?= ucfirst($dt->nama) ?></td>

					</tr>
				<?php 
						$numb++;
						endforeach;
				?>
			</tbody>
		</table>
		
	</div>
	<div class="box-footer clearfix">
		<?php 
		echo $this->pagination->create_links(); 
		?>	
	</div>
	<?php else: ?>
		<div class="alert alert-info" role="alert">
        	<p><i class="fa fa-warning"></i> &nbsp; <?= lang('konfirmasi-data-tidak-ada') ?></p>
    	</div>
	<?php 
		
		endif;

		form_close();

	?>

	

</div>
<script type="text/javascript">
		
	var iddata 	= "<?= $id ?>";

</script>