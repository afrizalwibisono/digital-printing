<?= form_open($this->uri->uri_string(),	[
											"name"		=> 'form_kas',
											'id'		=> 'form_kas',
											'role'		=> 'form',
											'class'		=> 'form-horizontal'
										]
			) ?>



<input type="hidden" name="st_ambil_kas" id="st_ambil_kas" value="<?= $data['st_ambil_kas'] ?>">
<input type="hidden" name="st_saldo_awal" id="st_saldo_awal" value="<?= $data['st_saldo_awal'] ?>">

<div class="box box-primary">
	<div class="box-body">
		
		<?php if($data['st_ambil_kas']==1): ?>

		<div class="form-group">
			<label class="col-sm-2 control-label"><?= lang("capt-kas-user") ?></label>
			<div class="col-sm-4">
				<select class="form-control" name="kas_kasir_close" id="kas_kasir_close">
					<?php foreach ($data['dt_user_kas'] as $key => $ks) : ?>
						<option value="<?= $ks['idkas_kasir'] ?>" <?= set_select("kas_kasir_close",$ks['idkas_kasir']) ?> ><?= ucfirst($ks['nama']) ?></option>	
					<?php endforeach; ?>
					
				</select>
			</div>
		</div>

		<?php endif; ?>

		<div class="form-group">
			<label class="col-sm-2 control-label"><?= lang("capt-kas-waktu") ?></label>
			<div class="col-sm-4">
				<span class="form-control"><?= $data['waktu'] ?></span>
			</div>
		</div>

		<div class="form-group <?= form_error('ket') ? 'has-error' : ''?> ">
			<label class="col-sm-2 control-label"><?= lang("capt-kas-ket") ?></label>
			<div class="col-sm-4">
				<?php 

					$readonly 	= $data['st_saldo_awal'] == 1 ? "readonly" : "";

				?>
				<textarea class="form-control" name="ket" id="ket" <?= $readonly ?> ><?= set_value("ket",isset($data['ket']) ? $data['ket'] : '' )  ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label"><?= lang("capt-kas-status") ?></label>
			<div class="col-sm-4">
				<div class="radio">
					<label> 
						<?php 
								
							$pil_status 	= set_value("status", isset($data['status']) ? $data['status'] : 1);

							$centang = $pil_status == 1 ? "checked" : "" 
						?>
						<input type="radio" name="status" id="st_debet" value="1" <?= $centang ?> ><?= lang("capt-kas-status-debet") ?>
					</label>
				</div>
				<div class="radio">
					<label>
						<?php $centang = $pil_status == 0 ? "checked" : "" ?> 
						<input type="radio" name="status" id="st_kredit" value="0" <?= $centang ?> ><?= lang("capt-kas-status-kredit") ?>
					</label>
				</div>
			</div>
		</div>

		<div class="form-group <?= form_error('jml') ? 'has-error' : ''?>">
			<label class="col-sm-2 control-label"><?= lang("capt-kas-jumlah") ?></label>
			<div class="col-sm-4">
				<input type="text" name="jml" id="jml" class="form-control" 
				value="<?= set_value('jml',isset($data['jml']) ? $data['jml'] : '') ?> " >
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-4">

				<button type="submit" name="simpan" class="btn btn-primary" ><?= lang('btn-save') ?></button>
                <?php
                	echo lang('bf_or') . ' ' . anchor('kas_kasir/batal', lang('btn-batal'));
                ?>

			</div>
		</div>

		

	</div>
</div>

<?= form_close(); ?>