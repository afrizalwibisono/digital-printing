<?= form_open($this->uri->uri_string(),[
											'name'		=> 'frm_close',
											'id'		=> 'frm_close',
											'role' 		=> 'form',
											'class'		=> 'form_horizontal'
										]); 

?>

<input type="hidden" name="idkas_kasir" id="idkas_kasir" value="<?= $dt['id'] ?>">

<div class="box box-primary">
	
	<div class="box-body">
		
		<div class="label label-danger">
			<?= lang("capt-judul-dt-trans") ?>
		</div>	
		<div class="table-responsive">
			<table class="table table-bordered table-condensed">
				<tbody>
					<tr>
						<th colspan="2" class="success"><?= lang('capt-omset-nota-order') ?></th>
						<td class="text-right">
							<input type="hidden" name="omset_nota" value="<?= $dt['omset_nota'] ?>">		
							<?= number_format($dt['omset_nota']) ?></td>
						<td>&nbsp;</td>
					</tr>

					<?php foreach ($dt_trans as $key => $isi) : ?>

						<tr>
							<td class="bg-info text-center">
								<input type="hidden" name="id_kategori[]" value="<?= $isi['id'] ?>">		
								<input type="hidden" name="value_kategori[]" value="<?= $isi['trans'] ?>">
								<?= $isi['kategori'] ?>
							</td>
							<td class="text-right">
								<?= number_format($isi['trans']) ?>
							</td>
						</tr>

					<?php endforeach; ?>

					<tr>
						<th colspan="2" class="danger"><?= lang('capt-piutang-nota-order') ?></th>
						<td class="text-right text-red">
							<?= isset($dt['piutang']) ? number_format($dt['piutang']) : '' ?>
							<input type="hidden" name="total_piutang" value="<?= $dt['piutang'] ?>">		
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<th colspan="3">&nbsp;</th>
						<th class="text-right">
							<?php 
									$selisih_order 	= $dt['omset_nota'] - $dt['piutang'];
									echo number_format($selisih_order);
							?>
						</th>
					</tr>
					<tr>
						<th colspan="2" class="success"><?= lang('capt-omset-bb') ?></th>
						<td class="text-right">
							<?= number_format($dt['omset_bb'])?>
							<input type="hidden" name="omset_bb" value="<?= $dt['omset_bb'] ?>">		
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<th colspan="2" class="danger"><?= lang('capt-piutang-bb') ?></th>
						<td class="text-right text-red">
							<?= number_format($dt['piutang_bb'])?>
							<input type="hidden" name="piutang_bb" value="<?= $dt['piutang_bb'] ?>">		
						</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<th colspan="3">&nbsp;</th>
						<th class="text-right">
							<?php 
								$selisih_jual =  $dt['omset_bb'] - $dt['piutang_bb'];
								echo number_format($selisih_jual);
							?>
						</th>
					</tr>
					<tr class="bg-primary">
						<th colspan="3"><?= lang('capt-total-omset') ?></th>
						<th class="text-right">
							<?php 
									$total 	= $selisih_order + $selisih_jual;
									echo number_format($total);
							?>
						</th>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="label label-success">
			<?= lang("capt-judul-dt-bayar") ?>
		</div>	

		<div class="table-responsive">
			<table class="table table-bordered">
				<tbody>
					<tr>
						<th class="bg-info"><?= lang('capt-bayar-pelunasan') ?></th>
						<th class="text-right">
							<?= number_format($dt['pelunasan']) ?>
							<input type="hidden" name="total_pelunasan" id="total_pelunasan" value="<?= $dt['pelunasan'] ?>">
						</th>
					</tr>
					<tr>
						<th class="bg-info"><?= lang('capt-bayar-pelunasan-bb') ?></th>
						<th class="text-right">
							<?= number_format($dt['pelunasan_bb']) ?>
							<input type="hidden" name="total_pelunasan_bb" id="total_pelunasan_bb" value="<?= $dt['pelunasan_bb'] ?>">
						</th>
					</tr>
					<tr>
						<th class="bg-info"><?= lang('capt-bayar-dp') ?></th>
						<th class="text-right">
							<?= isset($dt['total_dp']) ? number_format($dt['total_dp']) : '' ?>
							<input type="hidden" name="total_dp_rev" value="<?= $dt['total_dp'] ?>">		
						</th>
					</tr>
					<tr>
						<th class="bg-success"><?= lang('capt-bayar-trans') ?></th>
						<th class="text-right">
							<?= number_format($dt['total_bayar'])?>
							<input type="hidden" name="total_bayar" value="<?= $dt['total_bayar'] ?>">		
						</th>
					</tr>
				</tbody>
			</table>
		</div>		


		<!-- <div class="label label-danger">
			<?= lang("capt-judul-dt-trans") ?>
		</div>	
		<div class="table-responsive">
			<table class="table table-bordered table-condensed">
				<thead>
					<tr class="success">
						<th></th>
						<?php foreach ($dt_trans as $key => $isi) : ?>
							<th class="text-center">
								<?= $isi['kategori'] ?>
								<input type="hidden" name="id_kategori[]" value="<?= $isi['id'] ?>">		
							</th>
						<?php endforeach; ?>	
					</tr>
				</thead>
				<tbody>
					<tr>
						<th class="success"><?= lang("capt-judul-baris-total") ?></th>
						<?php 
							$jml_col = count($dt_trans);
							foreach ($dt_trans as $key => $isi) : 
						?>
						<th class="text-center">
							<?= number_format($isi['trans']) ?>
							<input type="hidden" name="value_kategori[]" value="<?= $isi['trans'] ?>">
						</th>
						<?php endforeach; ?>	
					</tr>
					<tr>
						<th class="success"><?= lang("capt-judul-baris-omset") ?></th>
						<th class="text-center" colspan="<?= $jml_col ?>">
							<?= number_format($dt['omset_nota']) ?>
						</th>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="label label-warning">
			<?= lang('capt-judul-dt-pelunasan') ?>
		</div>
		<div class="table-responsive">
			<table class="table table-bordered table-condensed">
				<tr>
					<th class="bg-primary"><?= lang("capt-judul-baris-total") ?></th>
					<th class="text-right">
						<?= number_format($dt['pelunasan']) ?>
						<input type="hidden" name="total_pelunasan" id="total_pelunasan" value="<?= $dt['pelunasan'] ?>">
					</th>
				</tr>
			</table>
			
		</div> -->

		<div class="table-responsive">
			<table class="table table-bordered table-condensed">
				<thead>
					<tr class="success">
						<th width="30" class="text-center">#</th>
						<th class="text-center"><?= lang("capt-close-transaksi") ?></th>
						<th class="text-center"><?= lang("capt-close-netral") ?></th>
						<th class="text-center"><?= lang("capt-close-debet") ?></th>
						<th class="text-center"><?= lang("capt-close-kredit") ?></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-center">1</td>
						<td ><?= lang("isi-close-ket-saldo-awal") ?></td>
						<td class="text-right"></td>
						<td class="text-right"><?= isset($dt['saldo_awal']) ? number_format($dt['saldo_awal']) : '' ?></td>
						<td class="text-right"></td>
					</tr>
					<tr>
						<td class="text-center">2</td>
						<td style="padding-left: 50px !important" ><?= lang("isi-close-ket-kas") ?></td>
						<td class="text-right"></td>
						<td class="text-right">
							<?= isset($dt['cash_tambah']) ? number_format($dt['cash_tambah']) : '' ?>
							<input type="hidden" name="kas_tambah" value="<?= $dt['cash_tambah'] ?>">		
						</td>
						<td class="text-right"></td>
					</tr>
					<tr>
						<td class="text-center">3</td>
						<td style="padding-left: 50px !important" ><?= lang("isi-close-ket-kas") ?></td>
						<td class="text-right"></td>
						<td class="text-right"></td>
						<td class="text-right">
							<?= isset($dt['cash_kurang']) ? number_format($dt['cash_kurang']) : '' ?>
							<input type="hidden" name="kas_keluar" value="<?= $dt['cash_kurang'] ?>">		
						</td>
					</tr>
					<tr>
						<td class="text-center">4</td>
						<td style="padding-left: 50px !important" ><?= lang("isi-close-ket-debit") ?></td>
						<td class="text-right">
							<?= isset($dt['kas_debit']) ? number_format($dt['kas_debit']) : '' ?>
							<input type="hidden" name="kas_debit" value="<?= $dt['kas_debit'] ?>">		
						</td>
						<td class="text-right"></td>
						<td class="text-right"></td>
					</tr>
					<tr>
						<td class="text-center">5</td>
						<td style="padding-left: 50px !important" ><?= lang("isi-close-ket-transfer") ?></td>
						<td class="text-right">
							<?= isset($dt['kas_transfer']) ? number_format($dt['kas_transfer']) : '' ?>
							<input type="hidden" name="kas_transfer" value="<?= $dt['kas_transfer'] ?>">		
						</td>
						<td class="text-right"></td>
						<td class="text-right"></td>
					</tr>
					<tr>
						<td class="text-center">6</td>
						<td style="padding-left: 50px !important" ><?= lang("isi-close-ket-deposit") ?></td>
						<td class="text-right">
							<?= isset($dt['kas_deposit']) ? number_format($dt['kas_deposit']) : '' ?>
							<input type="hidden" name="kas_deposit" value="<?= $dt['kas_deposit'] ?>">		
						</td>
						<td class="text-right"></td>
						<td class="text-right"></td>
					</tr>
					<!-- <tr>
						<td class="text-center">6</td>
						<th style="padding-left: 50px !important" ><?= lang("isi-close-ket-dp") ?></th>
						<th class="text-right">
							<?= isset($dt['total_dp']) ? number_format($dt['total_dp']) : '' ?>
							<input type="hidden" name="total_dp_rev" value="<?= $dt['total_dp'] ?>">		
						</th>
						<td class="text-right"></td>
						<td class="text-right"></td>
					</tr> -->
					<!-- <tr>
						<td class="text-center">7</td>
						<th style="padding-left: 50px !important" ><?= lang("isi-close-ket-piutang") ?></th>
						<th class="text-right">
							<?= isset($dt['piutang']) ? number_format($dt['piutang']) : '' ?>
							<input type="hidden" name="total_piutang" value="<?= $dt['piutang'] ?>">		
						</th>
						<td class="text-right"></td>
						<td class="text-right"></td>
					</tr> -->

				</tbody>
				<tfoot>
					<tr class="bg-primary">
						<td colspan="3" class="text-right">
							<strong><?= lang("capt-close-saldo-akhir") ?></strong>
						</td>
						<th colspan="2" class="text-right">
							<?= isset($dt['saldo_akhir']) ? number_format($dt['saldo_akhir']) : '' ?>
							<input type="hidden" name="isi_saldo_akhir" id="isi_saldo_akhir" value="<?= isset($dt['saldo_akhir']) ? $dt['saldo_akhir'] : '' ?>" >		
						</th>
					</tr>
					<tr class="bg-info">
						<td colspan="3" class="text-right" style="vertical-align: middle;">
							<strong><?= lang("capt-close-saldo-revisi") ?></strong>
						</td>
						<td colspan="2" class="text-right">
							<input type="text" name="revisi" id="revisi" class="form-control input-sm" value="<?= set_value("revisi") ?>" >
						</td>
					</tr>
					
					<tr id="area-selisih" style="display: none">
						<td colspan="3" class="text-right" style="vertical-align: middle;">
							<strong><?= lang("capt-close-saldo-selisih") ?></strong>
						</td>
						<td colspan="2" class="text-right">
							<span id="selisih"></span>
						</td>
					</tr>
					<tr class="bg-success" id="area-st-selisih" style="display: none">
						<td colspan="3" class="text-right" style="vertical-align: middle;">
							<strong><?= lang("capt-close-saldo-status") ?></strong>
						</td>
						<td colspan="2" class="text-right" id="st_selisih">
							
						</td>
					</tr>
					<tr class="bg-success" id="area-ket-selisih" style="display: none">
						<td colspan="3" class="text-right" style="vertical-align: top;">
							<strong><?= lang("capt-close-saldo-ket-selisih") ?></strong>
						</td>
						<td colspan="2" class="text-right">
							<textarea id="ket_selisih" name="ket_selisih" class="form-control"></textarea>
						</td>
					</tr>
					<tr class="bg-primary" id="area-status-rolling" >
						<td colspan="3" class="text-right" style="vertical-align: middle;" >
							<strong><?= lang("capt-close-st-rolling-saldo") ?></strong>
						</td>
						<td colspan="2" class="text-right">
							<select name="st_rolling" id="st_rolling" class="form-control">
								<option value="1" selected = "selected"  ><?= lang('isi-strolling-saldo-rolling') ?></option>\
								<option value="0" ><?= lang('isi-strolling-saldo-tidak') ?></option>
							</select>
						</td>
					</tr>
					
				</tfoot>

			</table>
		</div>
		<div class="form-group">
			<div class="col-sm-12">
				<div class="pull-right">
	    			<?php 

	    				echo anchor('kas_kasir/batal', lang('btn-batal'))." ".lang("bf_or")." ";

	    				$kunci_simpan 	= '';

	    				if(!$st_closing){

	    					$kunci_simpan = 'disabled';

	    				}

	    			?>

	    			<button type="submit" name="simpan" id="simpan" class="btn btn-primary" <?= $kunci_simpan ?> >
	    				<?= lang("btn-save") ?>
	    			</button>
    			</div>
			</div>
		</div>

	</div>

	<?php if($jml_order_cek > 0 || $jml_kasir_cek > 0) : ?>

		<div class="callout callout-info">
			<?= lang('label-info-simpan') ?>
		</div>

	<?php endif ?>

</div>

<?php 
	
	if($jml_order_cek > 0) :
	
?>

	<div class="callout callout-danger" ><?= lang('label-info-order-block-closing') ?></div>

<?php 

	endif; 
	if($jml_kasir_cek > 0) :

?>
	<div class="callout callout-danger" ><?= lang('label-info-trans-block-closing') ?></div>

<?php endif ?>






<?= form_close() ?>