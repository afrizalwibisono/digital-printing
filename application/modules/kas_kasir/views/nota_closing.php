<?php
		
		//print_r($data);

?>
<table class="lap-kasir">
	<thead>
		<tr>
			<td colspan="2" class="title">Clossing Kasir</td>
		</tr>
		<tr>
			<td colspan="2"><?= $data[0]->waktu_close ?></td>
		</tr>
		<tr class="border-solid">
			<td colspan="2"><?= ucwords($data[0]->nm_lengkap) ?></td>
		</tr>		
	</thead>
	<tbody>
		<tr class="border-dot">
			<td colspan="2" class="title">Transaksi Kategori</td>
		</tr>
		<?php
			$jml 	= count($data);
			$total 	= 0;
			foreach ($data as $key => $isi) :

				$garis 	= "";

				if($key == ($jml-1)){

					$garis = "class='border-dot'";

				}

				$total 	+= $isi->value;

		?>

			<tr <?= $garis ?> >
				<td><strong><?= $isi->nmkategori ?><strong></td>
				<td><?= number_format($isi->value) ?></td>
			</tr>	

		<?php
			endforeach;
		?>
		<!-- <tr>
			<td>A3</td>
			<td>2,000,000</td>
		</tr>
		<tr>
			<td>Indoor</td>
			<td>2,000,000</td>
		</tr>
		<tr class="border-dot">
			<td>Outdoor</td>
			<td>2,000,000</td>
		</tr> -->
		<tr class="border-solid">
			<td colspan="2" class="total"><?= number_format($total) ?></td>
		</tr>
		<!-- area omset nota order -->
		<tr>
			<td>Transaksi Nota Order</td>
			<td><?= number_format($data[0]->total_omset_nota) ?></td>
		</tr>
		<tr>
			<td>Piutang</td>
			<td><?= number_format($data[0]->total_piutang) ?></td>
		</tr>
		<tr>
			<?php $omset_nota 	= $data[0]->total_omset_nota - $data[0]->total_piutang; ?>
			<td colspan="2" class="total"><?= number_format($omset_nota) ?></td>
		</tr>
		<!-- area omset nota penjualan -->
		<tr>
			<td>Transaksi Nota Penjualan</td>
			<td><?= number_format($data[0]->total_omset_nota_bb) ?></td>
		</tr>
		<tr>
			<td>Piutang</td>
			<td><?= number_format($data[0]->total_piutang_nota_bb) ?></td>
		</tr>
		<tr>
			<?php $omset_bb 	= $data[0]->total_omset_nota_bb - $data[0]->total_piutang_nota_bb; ?>
			<td colspan="2" class="total"><?= number_format($omset_bb) ?></td>
		</tr>
		<tr class="border-solid">
			<td class="total">Total Pendapatan</td>
			<td class="total"><?= number_format($omset_nota + $omset_bb) ?></td>
		</tr>

		<tr>
			<td><strong>Pelunasan Nota Order</strong></td>
			<td><?= number_format($data[0]->total_pelunasan) ?></td>
		</tr>
		<tr>
			<td><strong>Pelunasan Nota Penjualan</strong></td>
			<td><?= number_format($data[0]->total_pelunasan_nota_bb) ?></td>
		</tr>
		<tr>
			<td><strong>DP Nota Order</strong></td>
			<td><?= number_format($data[0]->total_dp) ?></td>
		</tr>
		<tr class="border-solid">
			<td><strong>Total Bayar Hari Ini</strong></td>
			<td><?= number_format($data[0]->total_bayar_hari_ini) ?></td>
		</tr>

		<tr class="border-dot">
			<td colspan="2" class="title">Setoran Kasir</td>
		</tr>
		<tr>
			<td>Saldo Awal</td>
			<td><?= number_format($data[0]->saldo_awal) ?></td>
		</tr>
		<tr>
			<td>Kas Masuk</td>
			<td><?= number_format($data[0]->total_masuk) ?></td>
		</tr>
		<tr class="border-dot">
			<td>Kas Keluar</td>
			<td>(<?= number_format($data[0]->total_keluar) ?>)</td>
		</tr>
		<tr>
			<td>Saldo Akhir</td>
			<td><?= number_format($data[0]->saldo_akhir_system) ?></td>
		</tr>
		<tr>
			<td>Saldo Ref</td>
			<td><?= number_format($data[0]->saldo_revisi) ?></td>
		</tr>
		<tr class="border-solid">
			<td>Selisih</td>
			<td><?= number_format($data[0]->selisih) ?></td>
		</tr>
		<tr class="border-dot">
			<td colspan="2" class="title">Non Tunai</td>
		</tr>
		<tr>
			<td>Debit</td>
			<td><?= number_format($data[0]->total_debit) ?></td>
		</tr>
		<tr>
			<td>Transfer</td>
			<td><?= number_format($data[0]->total_transfer) ?></td>
		</tr>
		<tr>
			<td>Deposit</td>
			<td><?= number_format($data[0]->total_deposit) ?></td>
		</tr>
	</tbody>
	<tfoot>
		<tr class="ttd">
			<td>( <?= ucwords($data[0]->nm_lengkap) ?> )</td>
			<td>( ................ )</td>
		</tr>
	</tfoot>
</table>
<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function(e){
		window.print();
		// window.close();
	});
</script>