<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['title']							= 'Kas Kasir';
$lang['title_new']						= 'Alur Kas Kasir';
$lang['title_close']					= 'Closing Alur Kas Kasir';

/* ================================================================================ */
/* ..:: INDEX ::.. */

$lang['capt-ind-search-range']						= 'Range Waktu';
$lang['capt-ind-search-tgl-awal']					= 'Tanggal Awal';
$lang['capt-ind-search-jam-awal']					= 'Jam Awal';

$lang['capt-ind-search-tgl-akhir']					= 'Tanggal Akhir';
$lang['capt-ind-search-jam-akhir']					= 'Jam Akhir';

$lang['capt-ind-search-user']						= 'Karyawan';
$lang['capt-ind-search-cr-bayar']					= 'Metode Bayar';
	
	$lang['capt-ind-search-cr-bayar-cash']			= 'Cash / Tunai';
	$lang['capt-ind-search-cr-bayar-debit']			= 'Debit';
	$lang['capt-ind-search-cr-bayar-transfer']		= 'Transfer';
	$lang['capt-ind-search-cr-bayar-semua']			= 'Semua Data';

/* ..:: Table Index ::.. */
$lang['capt-ind-tbl-waktu']							= 'Waktu';
$lang['capt-ind-tbl-ket']							= 'Keterangan';
$lang['capt-ind-tbl-metode']						= 'Metode';
$lang['capt-ind-tbl-debit']							= 'D';
$lang['capt-ind-tbl-kredit']						= 'K';
$lang['capt-ind-tbl-kasir']							= 'Kasir';

/* ================================================================================ */
/* ..:: FORM Closing ::.. */

$lang['capt-judul-dt-trans']						= 'Rekap Transaksi';

$lang['capt-omset-nota-order']						= 'Nota Order Cetak';
$lang['capt-piutang-nota-order']					= 'Piutang Nota Order';
$lang['capt-omset-bb']								= 'Nota Penjualan Barang';
$lang['capt-piutang-bb']							= 'Piutang Penjualan Barang';
$lang['capt-total-omset']							= 'Total Pendapatan';


$lang['capt-judul-dt-bayar']						= 'Rekap Pembayaran';

$lang['capt-bayar-pelunasan']						= 'Bayar Pelunasan';
$lang['capt-bayar-pelunasan-bb']					= 'Bayar Pelunasan Penjualan Barang';
$lang['capt-bayar-dp']								= 'Bayar DP Transaksi Ref';
$lang['capt-bayar-trans']							= 'Bayar Transaksi Hari Ini';


$lang['capt-judul-baris-total']						= 'Total';
$lang['capt-judul-baris-omset']						= 'Nota';

$lang['capt-judul-dt-pelunasan']					= 'Rekap Pelunasan';

$lang['capt-close-transaksi'] 						= 'Transaksi';
$lang['capt-close-debet']	 						= 'Debet / Masuk';
$lang['capt-close-kredit']	 						= 'Kredit / Keluar';
$lang['capt-close-netral']	 						= 'N';

$lang['capt-close-saldo-akhir']	 					= 'Saldo Akhir';
$lang['capt-close-saldo-revisi']	 				= 'Saldo Revisi';
$lang['capt-close-saldo-selisih']	 				= 'Selisih';
$lang['capt-close-saldo-status']	 				= 'Status Selisih';
$lang['capt-close-saldo-ket-selisih']	 			= 'Keterangan Selisih';
$lang['capt-close-st-rolling-saldo']	 			= 'Status Rolling Saldo Akhir';
	
	// Start isi pilihan rolling
	
	$lang['isi-strolling-saldo-rolling']	 		= 'Saldo Akhir Dipakai Kasir Lain';
	$lang['isi-strolling-saldo-tidak']	 			= 'Saldo Akhir Tidak di Rolling';
	
	// End isi pilihan rolling

	
	/*isi tabel*/

	$lang['isi-close-ket-saldo-awal']				= 'Saldo Awal';
	$lang['isi-close-ket-kas']						= 'Cash / Tunai';
	$lang['isi-close-ket-debit']					= 'Debit';
	$lang['isi-close-ket-transfer']					= 'Transfer';
	$lang['isi-close-ket-deposit']					= 'Deposit';
	$lang['isi-close-ket-dp']						= 'Total DP';
	$lang['isi-close-ket-piutang']					= 'Piutang';

/* tabel transaksi total */

$lang['capt-nilai-trans']							= 'Total';

/* ================================================================================ */
/* ..:: FORM KAS ::.. */

$lang['capt-kas-user']								= 'Kasir Sebelumnya';
$lang['capt-kas-waktu']								= 'Waktu';
$lang['capt-kas-ket']								= 'Keterangan';
$lang['capt-kas-status']							= 'Status';
	$lang['capt-kas-status-debet']					= 'Debet / Kas Masuk';
	$lang['capt-kas-status-kredit']					= 'Kredit / Kas Keluar';

$lang['capt-kas-jumlah']							= 'Nominal / Jumlah';

/* ================================================================================ */
/* ..:: TOMBOL ::.. */

$lang['btn-create']						= 'Baru';
$lang['btn-close-trans']				= 'Closing Kas Kasir';
$lang['btn-filter']						= 'Filter';
$lang['btn-save']						= "Simpan";
$lang['btn-batal']						= "Batal / Kembali";
$lang['btn-kembali']					= "Kembali";
$lang['bf_or']							= "or";

/* ================================================================================ */
/* ..:: KONFIRMASI LABEL ::.. */

$lang['label_info_load_kas'] 			= "Anda diharuskan me-Load data Kas Kasir yang sudah ada sebagai Kas Awal";

$lang['label-info-simpan'] 				= "Untuk dapat menyimpan data Closing Kas Kasir.<br>Maka pastikan semua data - data Order dan Transaksi telah diValidasi";

$lang['label-info-order-block-closing']	= "Anda masih memiliki Data Order yang belum dibuat transaksinya.";
$lang['label-info-trans-block-closing']	= "Anda masih memiliki Data Transaksi yang belum divalidasi.";

$lang['label_simpan_sukses'] 			= "Simpan Data Alur Kas baru sukses";
$lang['label_simpan_close_sukses'] 		= "Simpan Data Closing Alur Kas sukses";

$lang['label_cancel_sukses']			= "Data Alur Kas telah berhasil dibatalkan";
$lang['label_error_jml']				= "Nominal Jumlah Kas, tidak boleh <= 0";
$lang['label_error_saldo_kurang']		= "Saldo tidak mencukupi";
$lang['label_error_reuse_kas']			= "KAS Kasir tidak dapat digunakan, karena telah digunakan oleh user lain";

$lang['label_error_kas_open_kosong']	= "Anda tidak memiliki Alur Kas Kasir aktif";
$lang['label_error_jam_kerja_tutup']	= "Anda tidak dapat melakukan Closing,<br>Saat ini Jam Kerja Tutup";

$lang['label-error-kas-ket-selisih']	= "Pastikan Anda telah mengisi keterangan selisih Saldo Akhir Kas Kasir. Minimal 10 Karakter";

$lang['konfirmasi-data-tidak-ada']		= "Data tidak ditemukan";
