<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is model class for table "alur_kas"
 */

class Kas_model extends BF_Model
{

    /**
     * @var string  User Table Name
     */
    protected $table_name   = 'alurkas';
    protected $key          = 'idalurkas';

    protected $soft_deletes = true;

    protected $set_created  = true;

    protected $set_modified = false;
    
    protected $date_format  = 'datetime';
    
    protected $log_user = true;

    /**
     * Function construct used to load some library, do some actions, etc.
     */
    public function __construct()
    {
        parent::__construct();
    }
}