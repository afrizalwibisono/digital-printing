$(function(){

	$("#revisi").number(true);

	cek_saldo_revisi();

	$("#revisi").keyup(function(){

		cek_saldo_revisi();		

	});

});

function cek_saldo_revisi(){

	var saldo_akhir 	= parseFloat($("#isi_saldo_akhir").val());
	var saldo_revisi 	= parseFloat($("#revisi").val());

	if(saldo_revisi > 0){

		$("#area-selisih").show(400);
		$("#area-st-selisih").show(400);
		$("#area-ket-selisih").show(400);

		var selisih_value 	= saldo_revisi > saldo_akhir ? saldo_revisi - saldo_akhir : saldo_akhir - saldo_revisi;
		var st_selisih 		= saldo_revisi < saldo_akhir ? "<span class='label label-danger'>Berkurang</span>" : "<span class='label label-success'>Bertambah</span>";

		$("#selisih").text($.number(selisih_value));
		$("#st_selisih").html(st_selisih);

	}else{

		$("#area-selisih").hide(400);
		$("#area-st-selisih").hide(400);
		$("#area-ket-selisih").hide(400);

	}

}