$(function(){
	
	$("#karyawan").select2({
		placeholder :"-- Kasir --",
        allowClear  : true
	});

	$('#tgl1, #tgl2').datetimepicker({
		format: "dd/mm/yyyy hh:ii:ss",
        todayBtn: "linked",
        autoclose: true,
        clearBtn: true,
        todayHighlight: true,
    });

	cek_cetak();

});

function cek_cetak(){

	var id = iddata;

	if(id.length>0){

		var url 		= baseurl+"kas_kasir/cetak_closing?id="+ id;
		window.open(url, '_blank', 'width=310,height=400,scrollbars=yes,menubar=no,status=yes,resizable=yes,screenx=100,screeny=100'); return false;
	
	}

}