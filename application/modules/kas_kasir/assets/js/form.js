$(function(){

	$("#jml").number(true);
	cek_fokus();
	load_saldo_awal();

	$("#kas_kasir_close").change(function(){

		load_saldo_awal();

	});

});

function cek_fokus(){

	var st_saldo_awal 	= $("#st_saldo_awal").val();

	if(st_saldo_awal == 1){

		$("#jml").focus();

	}else{

		$("#ket").focus();

	}

}

function load_saldo_awal(){

	var idkas_pakai 	= $("#kas_kasir_close").val();

	if(idkas_pakai.length > 0){

		$.ajax({

					url			: baseurl + "kas_kasir/load_kas_kasir",
					type		: "post",
					data		: {idkas : idkas_pakai},
					dataType	: "json",
					success 	: function(msg){

										if(msg['st'] == 0){

											$("#jml").val(msg['saldo']);
											$("#jml").prop("readonly","readonly");

											$("#st_debet").prop("checked","checked");

										}else{

											alertify.error('Saldo Kasir milik '+ $("#kas_kasir_close option:selected").text()+", telah digunakan oleh Kasir lain");

										}

									}

		});

	}


}