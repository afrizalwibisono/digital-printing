<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for Kas Kasir
 */

class Kas_kasir extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission       = "Kas Kasir.View";
    protected $addPermission        = "Kas Kasir.Add";
    protected $managePermission     = "Kas Kasir.Manage";
    
    protected $viewAllPermission    = "Kas Kasir All Data.View";

    protected $idkas_kasir_cetak    = "";

    public function __construct()
    {
        parent::__construct();

        $this->lang->load('kas_kasir/kas_kasir');
        $this->load->model( [
                                'kas_kasir_model',
                                'kas_kasir_detail_model',
                                'kas_kasir_closing_detail_model',
                                'users_model',
                                'karyawan_model',
                                'bank_model',
                                'kas_model',
                                'trans_model',
                                'bayar_trans_model',
                                'penjualan_model',
                                'jam_fix_model',
                                'jam_khusus_model',
                                'order_model'
                            ]         
                        );

        $this->template->title(lang('title'));
		$this->template->page_icon('fa fa-table');
    }

    public function index(){

        $this->auth->restrict($this->viewPermission);

        $user           = $this->auth->userdata();
        $admin          = $this->auth->is_admin();
        $iduser         = $user->id_user;

        if(isset($_POST['tgl1'])){

            $tgl1   = isset($_POST['tgl1']) ? $this->input->post('tgl1') : '';
            $tgl2   = isset($_POST['tgl2']) ? $this->input->post('tgl2') : '';
            $metode = isset($_POST['metode']) ? $this->input->post('metode') : '';

            if(has_permission($this->viewAllPermission) || $admin){

                $id_user    = isset($_POST['karyawan']) ? $this->input->post('karyawan') : '';

            }else{

                $id_user    = $iduser;

            }

        }else if(isset($_GET['tgl1'])){

            $tgl1   = isset($_GET['tgl1']) ? urldecode($this->input->get('tgl1')) : '';
            $tgl2   = isset($_GET['tgl2']) ? urldecode($this->input->get('tgl2')) : '';
            $metode = isset($_GET['metode']) ? $this->input->get('metode') : '';

            if(has_permission($this->viewAllPermission) || $admin ){

                $id_user    = isset($_GET['karyawan']) ? $this->input->get('karyawan') : '';

            }else{

                $id_user    = $iduser;

            }


        }else{

            $tgl1   = date("d/m/Y")." 01:00:00";
            $tgl2   = date("d/m/Y")." 23:59:00";
            $metode = "";

            if(has_permission($this->viewAllPermission) || $admin ){

                $id_user    = '';

            }else{

                $id_user    = $iduser;

            }


        }

        $this->load->library('pagination');       
        $filter     = "";

        $where      = "kas_kasir.deleted = 0";

        if(strlen($tgl1 > 0)){

            $tgl_pakai1     = date_ymd($tgl1);
            $tgl_pakai2     = date_ymd($tgl2);

            $where  .= " AND `kas_kasir_detail`.`waktu` >= '{$tgl_pakai1}' AND `kas_kasir_detail`.`waktu` <= '{$tgl_pakai2}'";

        }

        if(strlen($metode)>0){

            if($metode < 3){

                $where  .= " and `kas_kasir_detail`.`st_metode` = {$metode}";

            }

        }

        if(strlen($iduser)>0){

            $where  .= " and `kas_kasir`.`created_by` = {$id_user}";

        }

        $filter     = "?tgl1=".urlencode($tgl1)."&tgl2=".urlencode($tgl2)."&metode={$metode}&karyawan={$id_user}";

        $total      = $this->kas_kasir_model
                            ->join("kas_kasir_detail","kas_kasir.idkas_kasir = kas_kasir_detail.idkas_kasir","inner")
                            ->join("karyawan","kas_kasir.created_by = karyawan.id_user","inner")
                            ->where($where)
                            ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data_kas   = $this->kas_kasir_model->select(" date_format(`kas_kasir_detail`.`waktu`,'%d/%m/%Y %H:%i:%s') as waktu,
                                                        `kas_kasir_detail`.`ket`,
                                                        `kas_kasir_detail`.`st_metode`,
                                                        `kas_kasir_detail`.`jumlah`,
                                                        `kas_kasir_detail`.`status`,
                                                        `karyawan`.`nama`")
                            ->join("kas_kasir_detail","kas_kasir.idkas_kasir = kas_kasir_detail.idkas_kasir","inner")
                            ->join("karyawan","kas_kasir.created_by = karyawan.id_user","inner")
                            ->where($where)
                            ->order_by("`kas_kasir_detail`.`waktu`","asc")
                            ->limit($limit,$offset)
                            ->find_all();                    

        //echo $this->db->last_query();

        $dt_user    = $this->kas_kasir_model->select("`kas_kasir`.`idkas_kasir`,karyawan.id_user, `karyawan`.`nama`")
                                ->join("karyawan","kas_kasir.created_by = karyawan.id_user","inner")
                                ->group_by("karyawan.id_user")
                                ->order_by("`karyawan`.`nama`","asc")
                                ->find_all();
        

        $asset  =   [
                        "plugins/select2/js/select2.js",
                        "plugins/select2/css/select2.css",
                        "plugins/datetimepicker/bootstrap-datetimepicker.css",
                        "plugins/datetimepicker/bootstrap-datetimepicker.js",
                        "kas_kasir/assets/js/kas_kasir_index.js"      
                    ];

        add_assets($asset);
    
        $this->template->set('numb',$offset+1);    
        $this->template->set("data",$data_kas);
        $this->template->set('tgl1', $tgl1);
        $this->template->set('tgl2', $tgl2);
        $this->template->set('metode', $metode);
        $this->template->set('iduser', $id_user);
        $this->template->set('dt_karyawan',$dt_user);
        $this->template->title(lang('title'));
        $this->template->render('index');

    }

    public function closing(){

        $this->auth->restrict($this->managePermission);            

        //$this->cek_jam_closing();
        //$dt     = $this->cek_validasi_trans();

        //if(!$dt['status']) echo "false";

        /* cek apakah ada Kas Kasir yang tebuka */

        if(isset($_POST['simpan'])){

            if($this->save_close()){

                $this->template->set_message(lang("label_simpan_close_sukses"),"success");
                redirect("kas_kasir/index/".$this->idkas_kasir_cetak."/c");

            }

        }

        // start variable validasi

        $jml_kasir_blm_valid    = 0;
        $jml_order_blm_valid    = 0;
        $st_proses_closing      = true;

        // end variable validasi        


        $user           = $this->auth->userdata();
        $id_user        = $user->id_user;

        $date           = date("Y-m-d");
        
            /*$dt_kas     = $this->kas_kasir_model
                                    ->where("DATE(waktu_buat) = '{$date}'
                                                AND created_by = {$id_user}
                                                AND st_close = 0")
                                    ->count_all();*/

        // start cek apakah user login punya kas kasir yang terbuka.

        $dt_kas     = $this->kas_kasir_model
                                ->where("created_by = {$id_user}
                                            AND st_close = 0")
                                ->count_all();                                

        if($dt_kas <= 0){

            $this->template->set_message(lang("label_error_kas_open_kosong"),"info");
            redirect("kas_kasir");

        }

        // end cek apakah user login punya kas kasir yang terbuka.

        // start ambil data kas kasir yang belum close
            /*$dt_kas_head    = $this->kas_kasir_model
                                    ->select("`idkas_kasir`, `waktu_buat`, `saldo_awal`")
                                    ->where("DATE(waktu_buat) = '{$date}'
                                            AND created_by = {$id_user}
                                            AND st_close = 0")
                                    ->find_all();*/

        $dt_kas_head    = $this->kas_kasir_model
                                ->select("`idkas_kasir`, `waktu_buat`, `saldo_awal`")
                                ->where("created_by = {$id_user}
                                        AND st_close = 0")
                                ->find_all();                        

        $arr_isi_kas    = [];

        $id_kasir       = '';
        $waktu_buat     = ''; //waktu kas kasir mulai diisi saldo

        if(is_array($dt_kas_head) && count($dt_kas_head)){

            $id_kasir   = $dt_kas_head[0]->idkas_kasir;
            $waktu_buat = $dt_kas_head[0]->waktu_buat;

            $cash_kurang    = $this->kas_kasir_detail_model->select("sum(jumlah) as total")
                                    ->where(" status = 0 
                                            AND st_metode = 0
                                            AND idkas_kasir = '{$id_kasir}'")
                                    ->group_by("idkas_kasir")
                                    ->find_all();

            $cash_tambah    = $this->kas_kasir_detail_model->select("sum(jumlah) as total")
                                    ->where("st_saldo_awal = 0 
                                            AND st_metode = 0
                                            AND status = 1
                                            AND idkas_kasir = '{$id_kasir}'")
                                    ->group_by("idkas_kasir")
                                    ->find_all();

            $kas_debit     = $this->kas_kasir_detail_model->select("sum(jumlah) as total")
                                    ->where(" status = 1 
                                            AND st_metode = 1
                                            AND idkas_kasir = '{$id_kasir}'")
                                    ->group_by("idkas_kasir")
                                    ->find_all();

            $kas_transfer  = $this->kas_kasir_detail_model->select("sum(jumlah) as total")
                                    ->where(" status = 1 
                                            AND st_metode = 2
                                            AND idkas_kasir = '{$id_kasir}'")
                                    ->group_by("idkas_kasir")
                                    ->find_all();

            $kas_deposit    = $this->kas_kasir_detail_model
                                    ->select("status, sum(jumlah) as total")
                                    ->where("st_metode = 3
                                            AND idkas_kasir = '{$id_kasir}'")
                                    ->group_by("idkas_kasir")
                                    ->group_by("status")
                                    ->find_all();

            $nilai_kas_deposit  = 0;

            if(is_array($kas_deposit)){

                foreach ($kas_deposit as $key => $isi) {
                    
                    if($isi->status == 0){

                        $nilai_kas_deposit -= $isi->total;

                    }else{

                        $nilai_kas_deposit += $isi->total;

                    }

                }

            }

            $kas_saldo_akhir    = $this->kas_kasir_detail_model->select("saldo")
                                            ->where("idkas_kasir = '{$id_kasir}'")
                                            ->order_by("waktu","desc")
                                            ->order_by("order","desc")
                                            ->limit(1,0)
                                            ->find_all();
        
            $arr_isi_kas        =   [
                                        'id'            => $id_kasir,
                                        'saldo_awal'    => $dt_kas_head[0]->saldo_awal,
                                        'cash_tambah'   => is_array($cash_tambah) ? $cash_tambah[0]->total : 0,
                                        'cash_kurang'   => is_array($cash_kurang) ? $cash_kurang[0]->total : 0,
                                        'kas_debit'     => is_array($kas_debit) ? $kas_debit[0]->total : 0 ,
                                        'kas_transfer'  => is_array($kas_transfer) ? $kas_transfer[0]->total : 0,
                                        'kas_deposit'   => $nilai_kas_deposit,
                                        'saldo_akhir'   => $kas_saldo_akhir[0]->saldo
                                    ];

            //$tgl1   = date('Y-m-d')." 01:00:00";
            $tgl1   = $waktu_buat; //start waktu diambil dari kas kasir dibuat.                       
            //$tgl2   = date('Y-m-d')." 24:00:00";
            $tgl2   = date('Y-m-d H:i:s');

            $where_trans    = "transaksi.deleted = 0
                                AND transaksi.st_history = 0
                                AND transaksi.st_kasir = 1
                                AND transaksi.created_by = {$id_user}
                                AND (transaksi.created_on >= '{$tgl1}'
                                AND transaksi.created_on <= '{$tgl2}')";

            // start ambil data transaksi 

            /* $dt_trans   = $this->trans_model
                                ->select("transaksi.id_transaksi,
                                            transaksi.id_trans_ref_asli,
                                            `m_order_produk_detail`.`id_kategori`,
                                            `kategori`.`nmkategori`,
                                            SUM(`transaksi_detail`.`subtotal` - (`transaksi_detail`.`subtotal` * `transaksi`.`diskon`)) AS total,
                                            dtpot.pot,
                                            COUNT(transaksi_detail.id_transaksi_detail) AS jml_list,
                                            dtpot.pot * COUNT(transaksi_detail.id_transaksi_detail) AS pot_kategori,
                                            SUM(`transaksi_detail`.`subtotal` - (`transaksi_detail`.`subtotal` * `transaksi`.`diskon`)) - IF(dtpot.pot IS NULL,
                                            0,
                                            (dtpot.pot * COUNT(transaksi_detail.id_transaksi_detail))) AS total_final")
                                ->join("transaksi_detail","transaksi.id_transaksi = transaksi_detail.id_transaksi","inner")
                                ->join("m_order_produk_detail"," transaksi_detail.id_detail_produk_order = m_order_produk_detail.id_detail_produk_order","inner")
                                ->join("kategori","m_order_produk_detail.id_kategori = kategori.idkategori","inner")
                                ->join("(SELECT 
                                            `transaksi`.`id_transaksi`,
                                                `transaksi`.`potongan`,
                                                COUNT(transaksi_detail.id_transaksi_detail) AS jml,
                                                `transaksi`.`potongan` / COUNT(transaksi_detail.id_transaksi_detail) AS pot
                                        FROM
                                            transaksi
                                        INNER JOIN transaksi_detail ON transaksi.id_transaksi = transaksi_detail.id_transaksi
                                        WHERE
                                            transaksi.deleted = 0
                                                AND transaksi.st_history = 0
                                                AND transaksi_detail.deleted = 0
                                                AND transaksi.potongan > 0
                                        GROUP BY transaksi.id_transaksi) AS dtpot","transaksi.id_transaksi = dtpot.id_transaksi","left")
                                ->where($where_trans)
                                ->group_by('transaksi.id_transaksi')
                                ->group_by('`m_order_produk_detail`.`id_kategori`')
                                ->order_by("m_order_produk_detail.id_kategori")
                                ->find_all(); */

            $dt_trans   =   $this->trans_model
                                ->select("`transaksi`.`id_transaksi`,
                                            `transaksi`.`id_trans_ref_asli`,
                                            `m_order_produk_detail`.`id_kategori`,
                                            `kategori`.`nmkategori`,
                                            (`transaksi_detail`.`biaya_cetak`+`transaksi_detail`.`biaya_design`+`transaksi_detail`.`biaya_finishing`) as total_final")
                                ->join("transaksi_detail","transaksi.id_transaksi = transaksi_detail.id_transaksi","inner")
                                ->join("m_order_produk_detail","transaksi_detail.id_detail_produk_order = m_order_produk_detail.id_detail_produk_order","inner")
                                ->join("kategori","m_order_produk_detail.id_kategori = kategori.idkategori","inner")
                                ->where($where_trans)
                                ->order_by("m_order_produk_detail.id_kategori")
                                ->find_all();


            //echo $this->db->last_query();

            /* tata arraynya */

            $lastid             = 0;
            $nm_kat             = "";
            $jml_total          = 0;

            $arr_lap            = [];

            $id_trans_master    = [];

            if(is_array($dt_trans) && $dt_trans != false){

                $jmlrow         = count($dt_trans);

                foreach ($dt_trans as $key => $ttl) {

                    $id_trans_master [] = $ttl->id_trans_ref_asli;

                    if($ttl->id_kategori == $lastid){

                        $jml_total += $ttl->total_final;
                        $nm_kat     = $ttl->nmkategori;

                    }else{

                        /* jika kategorinya sudah berbeda buat arraynya */
                        if($key > 0){
                            $arr_lap[]  =   [
                                                'id'        => $lastid,
                                                'kategori'  => $nm_kat,
                                                'trans'     => $jml_total
                                            ];
                        }

                        $jml_total  = $ttl->total_final;
                        $lastid     = $ttl->id_kategori;
                        $nm_kat     = $ttl->nmkategori;

                    }

                    if($key == $jmlrow-1){

                        /* jika mencapai perulangan terakhir maka buat arraynya */

                        $arr_lap[]  =   [
                                            'id'        => $lastid,
                                            'kategori'  => $nm_kat,
                                            'trans'     => $jml_total
                                        ];

                    }

                }

            }

            // start akumulasi nilai tagihan Nota / omset by nota

            $omset_by_nota = 0;

            $dt_omset_nota  = $this->trans_model
                                    ->select("sum(`transaksi`.`grand_total`) as total")
                                    ->where($where_trans)
                                    ->find_all();

            if(is_array($dt_omset_nota) && count($dt_omset_nota)){

                $omset_by_nota = $dt_omset_nota[0]->total;

            }

            // end akumulasi nilai tagihan Nota / / omset by nota

            // start akumulasi nilai tagihan dan piutang nota penjualan bahan baku

            $dt_penjualan_bb    = $this->penjualan_model
                                        ->select("SUM(`grand_total`) AS total_trans,
                                                    SUM(t_bayar.total) AS bayar,
                                                    SUM(`grand_total`) - IF(SUM(t_bayar.total) IS NULL,
                                                        0,
                                                        SUM(t_bayar.total)) AS piutang")
                                        ->join("(SELECT 
                                                    `id_transaksi_master` AS id, SUM(`bayar`) AS total
                                                FROM
                                                    bayar_trans_kasir
                                                WHERE
                                                    deleted = 0
                                                GROUP BY id_transaksi_master) AS t_bayar","penjualan.id_penjualan = t_bayar.id","left")
                                        ->where("penjualan.deleted = 0 AND penjualan.created_by = {$id_user}
                                                    AND (penjualan.created_on >= '{$tgl1}'
                                                    AND penjualan.created_on <= '{$tgl2}')")
                                        ->find_all();

            $omset_bb   = 0;
            $piutang_bb = 0;

            if(is_array($dt_penjualan_bb) && count($dt_penjualan_bb)){

                $omset_bb   = $dt_penjualan_bb[0]->total_trans;
                $piutang_bb = $dt_penjualan_bb[0]->piutang;

            }

            // end akumulasi nilai tagihan dan piutang nota penjualan bahan baku

            //print_r($arr_lap);

            // end ambil data transaksi 

            // start ambil data pembayaran pelunasan

            // Gunakan waktu buat kas kasir sebagai Validasi untuk pengecekan pelunasan

            $waktu_cek      = strlen($waktu_buat) > 0 ? $waktu_buat : (date('Y-m-d')." 01:00:00");

            $dt_cek_pelunasan   = $this->bayar_trans_model
                                        ->select("SUM(`bayar_trans_kasir`.`bayar`) AS total_pelunasan")
                                        ->join("(SELECT 
                                                    `id_transaksi` AS id
                                                FROM
                                                    transaksi
                                                WHERE
                                                    waktu < '{$waktu_cek}'
                                                        AND deleted = 0) AS trans","bayar_trans_kasir.id_transaksi = trans.id","inner")
                                        ->where("deleted = 0 AND created_by = {$id_user}
                                                AND created_on >= '{$tgl1}'
                                                AND created_on <= '{$tgl2}'")
                                        ->find_all();


            //echo "\n pelunasan \n";
            //echo $this->db->last_query();

            $nilai_pelunasan    = 0;
                                        
            if(count($dt_cek_pelunasan) && is_array($dt_cek_pelunasan)){

                $nilai_pelunasan = $dt_cek_pelunasan[0]->total_pelunasan;

            }

            // end ambil data pembayaran pelunasan

            // start ambil data pelunasan nota penjualan bb

            $pelunasan_bb   = $this->bayar_trans_model
                                    ->select("SUM(`bayar_trans_kasir`.`bayar`) AS total_pelunasan")    
                                    ->join("(SELECT 
                                                `id_penjualan` AS id
                                            FROM
                                                penjualan
                                            WHERE
                                                `tgl_tran` < '{$waktu_cek}'
                                                    AND deleted = 0) AS t_jual","bayar_trans_kasir.id_transaksi = t_jual.id","inner")
                                    ->where("deleted = 0 AND created_by = {$id_user}
                                                AND created_on >= '{$tgl1}'
                                                AND created_on <= '{$tgl2}'")
                                    ->find_all();

            $nilai_pelunasan_bb = 0;

            if(is_array($pelunasan_bb) && count($pelunasan_bb)){

                $nilai_pelunasan_bb     = $pelunasan_bb[0]->total_pelunasan;

            }

            // end ambil data pelunasan nota penjualan bb



            // start ambil data piutang transaksi

            $where_piutang  = $where_trans." AND `transaksi`.st_lunas = 0";

            $dt_piutang     = $this->trans_model
                                    ->select("SUM(`grand_total`) AS total,
                                                SUM(total_bayar) AS total_bayar,
                                                SUM(`grand_total`) - IF(SUM(total_bayar) IS NULL,
                                                0,
                                                SUM(total_bayar)) AS piutang")
                                    ->join("(SELECT 
                                                id_transaksi_master AS id, SUM(bayar) AS total_bayar
                                            FROM
                                                bayar_trans_kasir
                                            WHERE
                                                deleted = 0
                                            GROUP BY id_transaksi_master) AS bayar","transaksi.id_trans_ref_asli = bayar.id","left")
                                    ->where($where_piutang)
                                    ->find_all();

            //echo "\n piutang \n";
            //echo $this->db->last_query();

            $nilai_piutang  = $dt_piutang[0]->piutang;

            // end ambil data piutang transaksi

            // start ambil dp yang sudah dibayar sebelum tgl aktiv, untuk nota ref
            
            $total_sudah_dibayar    = 0;

            if(count($id_trans_master)){

                $id             = array_unique($id_trans_master);
                $arr_id         = $id;
                $where_trans    = "";
                
                $n              = 0;

                foreach ($arr_id as $key => $isi) {
                    
                    if($n == 0){

                        $where_trans = "and (";

                    }

                    $where_trans    .= "id_transaksi_master = '{$isi}'";

                    if($n == (count($arr_id)-1) ){

                        $where_trans .= ")";
                        //echo "posisi koma ". $key;

                    }elseif($n > 0 || $n == 0 ){

                        $where_trans .= " or ";

                    }

                    $n++;

                }

                $where_cari     = "`created_on` < '{$tgl1}' and `deleted` = 0 ".$where_trans;
                $dt_sudah_bayar = $this->bayar_trans_model
                                            ->select("sum(`bayar`) as total")
                                            ->where($where_cari)
                                            ->find_all();



                if(is_array($dt_sudah_bayar) && count($dt_sudah_bayar)){

                    $total_sudah_dibayar    = is_null($dt_sudah_bayar[0]->total) ? 0 : $dt_sudah_bayar[0]->total;

                }else{

                    $total_sudah_dibayar = 0;

                }


            }else{

                $total_sudah_dibayar = 0;

            }

            // end ambil dp yang sudah dibayar sebelum tgl aktiv, untuk nota ref

            // start ambil semua data pembayaran yang dilakukan 

            $bayar_trans    = $this->bayar_trans_model
                                    ->select("SUM(`bayar`) AS total")
                                    ->where("deleted = 0 AND created_by = {$id_user}
                                                AND (created_on >= '{$tgl1}'
                                                AND created_on <= '{$tgl2}}')")
                                    ->find_all();

            $total_bayar    = 0;

            if(is_array($bayar_trans) && count($bayar_trans)){

                $total_bayar = $bayar_trans[0]->total;

            }


            // end ambil semua data pembayaran yang dilakukan 

            $arr_tambah     =   [
                                    "omset_nota"    => $omset_by_nota,
                                    "piutang"       => $nilai_piutang,
                                    "omset_bb"      => $omset_bb,
                                    "piutang_bb"    => $piutang_bb,
                                    "total_dp"      => $total_sudah_dibayar,
                                    "pelunasan"     => $nilai_pelunasan,
                                    "pelunasan_bb"  => $nilai_pelunasan_bb,
                                    "total_bayar"   => $total_bayar
                                ];

            $arr_isi_kas    = array_merge($arr_isi_kas, $arr_tambah);

            //print_r($arr_isi_kas);

            // start checking Validasi

            $dt_cek_waktu   = $this->cek_jam_closing();


            if(!$dt_cek_waktu['st_buka']){

                $this->template->set_message(lang("label_error_jam_kerja_tutup"),'error');
                $st_proses_closing = false;

            }else{

                if(!$dt_cek_waktu['status']){ //false, closing seperti biasa

                    $st_proses_closing      = true;
                    $jml_order_blm_valid    = 0;
                    $jml_kasir_blm_valid    = 0;

                }else{ //cek validasi terlebih dahulu

                    $cek_order  = $this->cek_order_nanggung();
                    $cek_trans  = $this->cek_validasi_trans();

                    $jml_order_blm_valid = $cek_order['jml_trans'];
                    $jml_kasir_blm_valid = $cek_trans['jml_trans'];

                    $st_proses_closing      = $jml_order_blm_valid > 0 || $jml_kasir_blm_valid > 0 ? false : true;

                }

            }

            // end checking Validasi

        }else{

            $this->template->set_message(lang("label_error_kas_open_kosong"),"error");
            redirect("kas_kasir");

        }

        $asset  =   [
                        "plugins/number/jquery.number.js",
                        "kas_kasir/assets/js/form_close.js"      
                    ];

        add_assets($asset);

        $this->template->set('jml_order_cek',$jml_order_blm_valid);
        $this->template->set('jml_kasir_cek',$jml_kasir_blm_valid);
        $this->template->set('st_closing',$st_proses_closing);

        $this->template->set("dt_trans",$arr_lap);
        $this->template->set("dt",$arr_isi_kas);
        $this->template->title(lang('title_close'));
        $this->template->render('form_close');        

    }

    public function create(){

        $this->auth->restrict($this->addPermission);    

        if(isset($_POST['simpan'])){

            if($this->save()){

                $this->template->set_message(lang("label_simpan_sukses"),"success");
                redirect("kas_kasir");

            }

        }

        $isi    = $this->set_arr_view();

        $asset  =   [
                        "plugins/select2/js/select2.js",
                        "plugins/select2/css/select2.css",
                        "plugins/number/jquery.number.js",
                        "kas_kasir/assets/js/form.js"      
                    ];

        add_assets($asset);

        

        $this->template->set("data",$isi);
        $this->template->title(lang('title_new'));
        $this->template->render('form_kas');        

    }

    private function cek_jam_closing(){

        /*
            Keterangan.
                -   Cek jam tutup kerja.
                    -   Cek jam kerja khusus, untuk tanggal sekarang.
                    -   Jika tidak ada, menggunakan jam kerja khusus.
                -   Pengkondisian.
                    -   Tutup, tidak dapat melakukan clossing kas kasir.
                    -   Buka, hanya ketika closing yang dicek adalah jam tutup.
                -   Status.
                    -   True, munculkan semua validasi pengecekkan closing.
                    -   False, closing seperti biasa.

        */

        $tgl_sekarang   = date('Y-m-d');

        $jam_tutup      = '';
        $st_buka        = true;

        // start cek jam kerja

        $cek_jam_khusus     = $this->jam_khusus_model
                                    ->select('`jam_tutup`')
                                    ->where("deleted = 0 and `tgl` = '{$tgl_sekarang}'")
                                    ->find_all();

        if(is_array($cek_jam_khusus) && count($cek_jam_khusus)){

            $jam_tutup  = $cek_jam_khusus[0]->jam_tutup;

        }else{

            $hari_ke            = date('N');

            $cek_jam_normal     = $this->jam_fix_model
                                        ->select('`jam_tutup`, 
                                                    `st_buka`')
                                        ->find($hari_ke);

            $st_buka            = $cek_jam_normal->st_buka == 0 ? false : true;
            $jam_tutup          = $cek_jam_normal->jam_tutup;

        }

        //echo $jam_tutup." ";

        // end cek jam kerja

        // start cek jam closing dengan jam tutup

            $jam_sekarang   = date('H:i:s');
            $jam_sekarang   = strtotime($jam_sekarang);
            $jam_cek        = strtotime($jam_tutup);


            $st_cek_closing = false;

           /* echo $jam_tutup." ";
            echo $jam_sekarang." ";*/

            if($jam_sekarang >= $jam_cek){

                $st_cek_closing = true;

            }


        // end cek jam closing dengan jam tutup


        $hasil  =   [
                        'jam_tutup'     => $jam_tutup,
                        'st_buka'       => $st_buka,
                        'status'        => $st_cek_closing
                    ];


        //print_r($hasil);
        return $hasil;

    }

    private function cek_validasi_trans(){

        /*
            Keterangan.
                -   Mendapatkan jumlah transaksi yang dusah direvisi tapi belum validasi revisinya.
                -   Status maih ada atau sudah bersih.
                    -   True, Sudah bisa di closing.
                    -   False, masih ada yang belum di closing

        */

        // start ambil data transaksi yang memiliki revisi

        $where  = "`transaksi`.`deleted` = 0
                    AND `order_produk`.`st_order` = 0
                    AND `transaksi`.`st_history` = 0
                    AND `order_produk`.`st_revisi` = 1
                    AND `transaksi`.`st_kasir` = 1";

        $cek_ketemu     = $this->trans_model
                                ->select("`transaksi`.`id_transaksi` AS `id`")
                                ->join("order_produk","transaksi.id_order = order_produk.id_order","inner")
                                ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                                ->join("(SELECT 
                                            `id_transaksi` AS id, `no_faktur`
                                        FROM
                                            transaksi
                                        WHERE
                                            deleted = 0) AS trans_ref","trans_ref.id = transaksi.id_trans_ref_asli","inner")
                                ->where($where)
                                ->count_all();

        if($cek_ketemu > 0){

            $status     = false;

        }else{

            $status     = true;

        }

        $hasil  =   [
                        'jml_trans'     => $cek_ketemu,
                        'status'        => $status
                    ];

        //print_r($hasil);


        return $hasil;


        // end ambil data transaksi yang memiliki revisi                    


    }

    private function cek_order_nanggung(){

        /*
            Keterangan.
                -   Mendapatkan jumlah order yang belum diselesaikan, dengan syarat.
                    -   Bukan Order Instansi.
                    -   Status cetak struk sementaranya 0 atau ( status cetak struk sementara = 1 dan Acc delete = 1 )
                -   Status Masih ada atau sudah bersih.
                    -   True, sudah bisa Closing karena sudah dibersihkan.
                    -   False, masih ada data order yang nanggung.    
        */

        $where  = "`order_produk`.`deleted` = 0
                    AND `order_produk`.`st_simpan` = 1
                    AND `order_produk`.`st_kasir` = 0
                    AND `konsumen`.`st` < 2
                    AND NOT (`order_produk`.st_ctk_struk_tmp = 1 AND `order_produk`.st_acc_delete = 0)";   

        $cek_ketemu     = $this->order_model
                                ->select('`order_produk`.`id_order`')        
                                ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                                ->where($where)
                                ->count_all();


        if($cek_ketemu > 0){

            $status     = false;

        }else{

            $status     = true;

        }

        $hasil  =   [
                        'jml_trans'     => $cek_ketemu,
                        'status'        => $status
                    ];

        return $hasil;

    }

    public function save_close(){

        $this->form_validation->set_rules("revisi","lang:capt-close-saldo-revisi");
        $this->form_validation->set_rules("ket_selisih","lang:capt-close-saldo-ket-selisih");

        $this->form_validation->run();

        $idkas_kasir        = $this->input->post("idkas_kasir");

        $kas_masuk          = $this->input->post("kas_tambah");
        $kas_keluar         = $this->input->post("kas_keluar");
        $kas_debit          = $this->input->post("kas_debit");
        $kas_transfer       = $this->input->post("kas_transfer");

        $total_dp           = $this->input->post("total_dp_rev");
        $total_pelunasan    = $this->input->post("total_pelunasan");
        $total_pelunasan_bb = $this->input->post("total_pelunasan_bb");
        $total_bayar        = $this->input->post("total_bayar");

        $omset_nota_order   = $this->input->post("omset_nota");
        $omset_nota_bb      = $this->input->post("omset_bb");
        $total_piutang      = $this->input->post("total_piutang");
        $total_piutang_bb   = $this->input->post("piutang_bb");

        $saldo_akhir        = $this->input->post("isi_saldo_akhir");
        $saldo_revisi       = str_replace(",", "", $this->input->post("revisi"));
        $ket_revisi         = $this->input->post("ket_selisih");
        $st_rolling         = $this->input->post("st_rolling");

        if($saldo_revisi > 0 && strlen($ket_revisi) < 10 ){

            $this->template->set_message(lang("label-error-kas-ket-selisih"),"error");
            return false;

        }

        /* update close transaksi kas kasir */
        //cek selisih revisi

        $selisih_revisi = 0;
        $st_selisih     = 0;

        if(strlen($saldo_revisi) > 0 && $saldo_revisi > 0){

            $selisih_revisi     = $saldo_revisi > $saldo_akhir ? $saldo_revisi - $saldo_akhir : $saldo_akhir - $saldo_revisi;

            $st_selisih         = $saldo_revisi > $saldo_akhir ? 1 : 0;

        }

        $saldo_akhir_final  = strlen($saldo_revisi) > 0 ? $saldo_revisi : $saldo_akhir;

        $waktu      = date("Y-m-d H:i:s");

        $sql_all    = "";

        $this->db->trans_start();

        $arr_up_kas_kasir   =   [
                                    'st_close'                  => 1,
                                    'waktu_close'               => $waktu,
                                    'total_masuk'               => $kas_masuk, 
                                    'total_keluar'              => $kas_keluar, 
                                    'total_debit'               => $kas_debit, 
                                    'total_transfer'            => $kas_transfer, 
                                    'total_dp'                  => $total_dp, 
                                    'total_piutang'             => $total_piutang,
                                    'total_pelunasan'           => $total_pelunasan,
                                    'saldo_akhir_system'        => $saldo_akhir,
                                    'saldo_revisi'              => $saldo_revisi,
                                    'selisih'                   => $selisih_revisi,
                                    'st_selisih'                => $st_selisih,
                                    'saldo_akhir_final'         => $saldo_akhir_final,
                                    'ket_saldo_revisi'          => $ket_revisi,
                                    'total_omset_nota'          => $omset_nota_order, 
                                    'total_omset_nota_bb'       => $omset_nota_bb, 
                                    'total_piutang_nota_bb'     => $total_piutang_bb, 
                                    'total_pelunasan_nota_bb'   => $total_pelunasan_bb, 
                                    'total_bayar_hari_ini'      => $total_bayar,
                                    'st_saldo_rolling'          => $st_rolling
                                ];

        $this->kas_kasir_model->update($idkas_kasir, $arr_up_kas_kasir);
        $sql_all    = $this->db->last_query();

        //insert detail kas closing
        if($_POST['id_kategori']){

            $id_kategori    = $this->input->post('id_kategori');
            $value_kategori = $this->input->post('value_kategori');

            $arr_det_kategori  = [];

            foreach ($id_kategori as $key => $isi) {
                
                $arr_det_kategori[]    =   [
                                                'id'            => gen_primary('','kas_kasir_closing_detail','id') , 
                                                'idkas_kasir'   => $idkas_kasir, 
                                                'idkategori'    => $isi, 
                                                'value'         => $value_kategori[$key]
                                            ];

            }

            $this->kas_kasir_closing_detail_model->insert_batch($arr_det_kategori);
            $sql_all    .= "\n\n". $this->db->last_query();
        }


        /* ==== ..:: Insert ke kas utama ::. ==== */

        /* get kode bank devault */
        $dt_bank = $this->bank_model->select("id_bank")->where("st_default=1")->find_all();
        $id_bank = $dt_bank[0]->id_bank;


        $dt_det_kas_kasir   = $this->kas_kasir_detail_model->select("`ket`,
                                                                    `st_saldo_awal`, 
                                                                    `st_metode`, 
                                                                    `jumlah`, 
                                                                    `status`")
                                    ->where("`idkas_kasir` = '{$idkas_kasir}'")
                                    ->find_all();

        $arr_kas    = [];

        foreach ($dt_det_kas_kasir as $key => $dt) {

            if($dt->st_saldo_awal == 1){

                $id_account     = '1';

            }else{

                if(strtolower(substr($dt->ket, 0,7)) == "deposit"){

                    $id_account = 17;

                }else{

                    $id_account     = $dt->status == 0 ? 8 : 2;
                        
                }

            }

            
            $arr_kas[]  =   [
                                'idalurkas'         => gen_primary("","alurkas","idalurkas"),
                                'waktu'             => $waktu,
                                'id_account_kas'    => $id_account,
                                'status'            => $dt->status,
                                'model_transaksi'   => $dt->st_metode,
                                'id_bank'           => $dt->st_metode > 0 ? $id_bank : null,
                                'ket'               => $dt->ket,
                                'total'             => $dt->jumlah
                            ];

        }

        if(count($arr_kas) && is_array($arr_kas)){

            $this->kas_model->insert_batch($arr_kas);
            $sql_all    .= "\n\n".$this->db->last_query();

        }

        $this->db->trans_complete();    

        if($this->db->trans_status() == false){

            $return         = false;
            $keterangan     = "GAGAL, close data kasir, id = ".$idkas_kasir;
            $status         = 0;

        }else{

            $return         = true;
            $keterangan     = "SUKSES, close data kasir, id = ".$idkas_kasir;
            $status         = 1;

        }

        $nm_hak_akses   = $this->managePermission; 
        $kode_universal = $idkas_kasir;
        $jumlah         = 0;
        $sql            = $sql_all;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        $this->idkas_kasir_cetak    = $idkas_kasir;

        return $return;

    }

    protected function set_arr_view(){

        $user           = $this->auth->userdata();
        $id_user        = $user->id_user;

        $date           = date("Y-m-d");
        $waktu_tampil   = date("d-m-Y H:i:s");

        /* cek apakah ada kas yang sudah close */
        $dt_kas_close   = $this->kas_kasir_model->where("DATE(waktu_buat) = '{$date}'
                                                         AND st_close = 1 
                                                         AND st_reuse = 0
                                                         AND st_saldo_rolling = 1")
                                                ->count_all();

        //echo $this->db->last_query();

        /* cek apakah KAS user aktiv sudah ada / belum */
                /*$dt_cek_kas     = $this->kas_kasir_model->where("DATE(waktu_buat) = '{$date}'
                                                                AND created_by = {$id_user}
                                                                AND st_close = 0")
                                                        ->count_all();
                */

        // ..:: Selama masih ada transaksi kas kasir yang belum diclose, maka dianggap masih memiliki kas kasir aktiv ::..
        $dt_cek_kas     = $this->kas_kasir_model->where("created_by = {$id_user}
                                                        AND st_close = 0")
                                                ->count_all();


        //echo $this->db->last_query();

        if($dt_kas_close > 0 && $dt_cek_kas <= 0){

            /* belum punya KAS, dan sudah ada Close Kas */

            // Ambil data user yang sudah Close Kasirnya

            $dt_user    = $this->kas_kasir_model->select("`kas_kasir`.`idkas_kasir`, `karyawan`.`nama`")
                                ->join("karyawan","kas_kasir.created_by = karyawan.id_user","inner")
                                ->where("karyawan.deleted = 0
                                        AND kas_kasir.st_close = 1
                                        AND kas_kasir.st_saldo_rolling = 1
                                        AND kas_kasir.st_reuse = 0")
                                ->order_by("`karyawan`.`nama`","asc")
                                ->as_array()
                                ->find_all();

            $arr    =   [
                            'st_ambil_kas'      => 1, // mengambil kas dari user lain
                            'dt_user_kas'       => $dt_user,
                            'st_saldo_awal'     => 1, // dianggap saldo awal
                            'waktu'             => $waktu_tampil,
                            'ket'               => "Saldo Awal",
                            'status'            => 1,
                            'jml'               => 0
                        ];

            $this->template->set_message(lang("label_info_load_kas"),"info");


        }else if($dt_kas_close <= 0 && $dt_cek_kas <= 0){

            /* belum punya KAS, dan tidak ada Close Kas */

            $arr    =   [
                            'st_ambil_kas'      => 0, // tidak mengambil kas dari user lain
                            'dt_user_kas'       => [],
                            'st_saldo_awal'     => 1, // dianggap saldo awal
                            'waktu'             => $waktu_tampil,
                            'ket'               => "Saldo Awal",
                            'status'            => 1,
                            'jml'               => 0
                        ];

        }else{

            /* KAS sudah ada , dan terbuka */
            $ket    = $this->input->post("ket");
            $status = $this->input->post("status");
            $jml    = $this->input->post("jml");

            $arr    =   [
                            'st_ambil_kas'      => 0, // tidak mengambil kas dari user lain
                            'dt_user_kas'       => [],
                            'st_saldo_awal'     => 0, // tidak dianggap saldo awal
                            'waktu'             => $waktu_tampil,
                            'ket'               => $ket,
                            'status'            => $status,
                            'jml'               => $jml
                        ];


        }    

        return $arr;

    }

    public function batal(){

        $this->template->set_message(lang("label_cancel_sukses"),"success");
        redirect("kas_kasir");

    }

    public function save(){

        $st_ambil_kas       = $this->input->post("st_ambil_kas");
        $kas_kasir_close    = $this->input->post("kas_kasir_close"); /* Idkas_kasir, yang dipakai saldonya */
        $st_saldo_awal  = $this->input->post("st_saldo_awal");

        $ket            = $this->input->post("ket");
        $status         = $this->input->post("status");
        $jml            = str_replace(",","",$this->input->post("jml"));        

        $date           = date('Y-m-d');

        $user           = $this->auth->userdata();
        $id_user        = $user->id_user;

        $saldo_akhir    = 0;

        if($_POST['st_ambil_kas'] == 1){

            $this->form_validation->set_rules('kas_kasir_close','lang:capt-kas-user','required');

        }

        $this->form_validation->set_rules('ket','lang:capt-kas-ket','required');
        $this->form_validation->set_rules('jml','lang:capt-kas-jumlah','required');
        $this->form_validation->set_rules('status','lang:capt-kas-status','required');

        if($this->form_validation->run() === false){

            $this->template->set_message(validation_errors(), 'error');
            return FALSE;

        }else{

            /* cek jml nominal */
            if(strlen($jml) > 0 && $jml <= 0){

                $this->template->set_message(lang("label_error_jml"), 'error');
                return false;

            }

            /* cek apakah jml nominal > saldo akhir */

            /*$dt_saldo   = $this->kas_kasir_detail_model
                                        ->select("`kas_kasir_detail`.`saldo`")
                                        ->join("kas_kasir","kas_kasir_detail.idkas_kasir = kas_kasir.idkas_kasir")
                                        ->where("DATE(`kas_kasir`.`waktu_buat`) = '{$date}'
                                                        AND `kas_kasir`.`created_by` = {$id_user}
                                                        AND `kas_kasir`.`st_close` = 0")    
                                        ->order_by("`kas_kasir_detail`.`waktu`","desc")
                                        ->limit(1,0)
                                        ->find_all();*/

            $dt_saldo   = $this->kas_kasir_detail_model
                                        ->select("`kas_kasir_detail`.`saldo`")
                                        ->join("kas_kasir","kas_kasir_detail.idkas_kasir = kas_kasir.idkas_kasir")
                                        ->where("`kas_kasir`.`created_by` = {$id_user}
                                                        AND `kas_kasir`.`st_close` = 0")    
                                        ->order_by("`kas_kasir_detail`.`waktu`","desc")
                                        ->limit(1,0)
                                        ->find_all();
                            

            if(count($dt_saldo) && is_array($dt_saldo)){

                $saldo_akhir    = $dt_saldo[0]->saldo;

            }else{

                $saldo_akhir = 0;

            }


            if($status == 0 ){/* jika kas keluar / kredit */

                
                if($saldo_akhir > 0){

                    if($saldo_akhir < $jml){

                        $this->template->set_message(lang("label_error_saldo_kurang"),"error");
                        return false;        

                    }

                }else{

                    $this->template->set_message(lang("label_error_saldo_kurang"),"error");
                    return false;

                }


            }

        }

        $sql_all    = "";

        $this->db->trans_start();

            $waktu_simpan   = date("Y-m-d H:i:s");

            $id_primary     = "";

            if($st_saldo_awal == 1){

                $id_primary     = gen_primary("","kas_kasir","idkas_kasir");
                
                if($st_ambil_kas == 0){/* jika Saldo Kas, adalah inputan baru */

                    $arr_head       =   [
                                            'idkas_kasir'   => $id_primary,
                                            'waktu_buat'    => $waktu_simpan, 
                                            'saldo_awal'    => $jml
                                        ];

                }else{

                    $dt_kas_kasir   = $this->kas_kasir_model->select("`saldo_akhir_final` as saldo")
                                            ->where("`idkas_kasir` = '{$kas_kasir_close}' AND `st_reuse` = 0")
                                            ->find_all();

                    if(is_array($dt_kas_kasir) && count($dt_kas_kasir)){

                        $arr_head   =   [
                                            'idkas_kasir'       => $id_primary,
                                            'idkas_kasir_sumber'=> $kas_kasir_close,
                                            'waktu_buat'        => $waktu_simpan,
                                            'saldo_awal'        => $jml
                                        ];

                        /* update kas kasir yang dipakai */

                        $arr_up_kas =   [
                                            'st_reuse'          => 1, 
                                            'idkas_kasir_reuse' => $id_primary
                                        ];

                        $this->kas_kasir_model->update($kas_kasir_close, $arr_up_kas);
                        $sql_all    = $this->db->last_query();

                    }else{

                        $this->template->set_message(lang("label_error_reuse_kas"),"error");
                        return false;

                    }


                }

                $this->kas_kasir_model->insert($arr_head);
                $sql_all    .= ";\n\n".$this->db->last_query();

            }else{

                /*$dt_kas_pakai   = $this->kas_kasir_model->select("idkas_kasir")
                                        ->where("DATE(waktu_buat) = '{$date}'
                                                        AND created_by = {$id_user}
                                                        AND st_close = 0")
                                        ->find_all();*/

                $dt_kas_pakai   = $this->kas_kasir_model->select("idkas_kasir")
                                        ->where("created_by = {$id_user}
                                                        AND st_close = 0")
                                        ->find_all();

                $id_primary     = $dt_kas_pakai[0]->idkas_kasir;


            }



            $saldo_sekarang = $status == 0 ? $saldo_akhir - $jml :  $saldo_akhir + $jml;

            $arr_detail     =   [
                                    'idkas_kasir_detail'    => gen_primary("","kas_kasir_detail","idkas_kasir_detail"),
                                    'idkas_kasir'           => $id_primary,
                                    'st_saldo_awal'         => $st_saldo_awal,
                                    'waktu'                 => $waktu_simpan,
                                    'ket'                   => $ket,
                                    'jumlah'                => $jml,
                                    'status'                => $status,
                                    'saldo'                 => $saldo_sekarang
                                ];

            $this->kas_kasir_detail_model->insert($arr_detail);
            $sql_all    .= ";\n\n".$this->db->last_query();

        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            $return         = false;
            $keterangan     = "SUKSES, simpan data kasir, id = ".$id_primary;
            $status         = 0;

        }else{

            $return         = true;
            $keterangan     = "SUKSES, SUKSES, simpan data kasir, id = ".$id_primary;
            $status         = 1;

        }

        $nm_hak_akses   = $this->addPermission; 
        $kode_universal = "-";
        $jumlah         = $jml;
        $sql            = $sql_all;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        return $return;

    }

    function load_kas_kasir(){

        if(!$this->input->is_ajax_request()){

            redirect("kas_kasir");

        }

        $idkas_kasir    = $this->input->post("idkas");

        $dt     = $this->kas_kasir_model->select("saldo_akhir_final as saldo, st_reuse")->find($idkas_kasir);

        //echo $this->db->last_query();

        $arr_data   = ['st' => $dt->st_reuse , 'saldo' => $dt->saldo];

        echo json_encode($arr_data);

    }

    public function cetak_closing(){

        $idkas_kasir    = $this->input->get("id");

        // buka data clossing
        $dt_rekap   = $this->kas_kasir_model
                            ->select("kas_kasir.idkas_kasir,
                                    `kas_kasir`.`waktu_buat`,
                                    date_format(`kas_kasir`.`waktu_close`,'%d/%m/%Y %H:%i:%s') as waktu_close,
                                    `kas_kasir`.`saldo_awal`,
                                    `kas_kasir`.`total_masuk`,
                                    `kas_kasir`.`total_keluar`,
                                    `kas_kasir`.`total_debit`,
                                    `kas_kasir`.`total_transfer`,
                                    `kas_kasir`.`total_deposit`,
                                    `kas_kasir`.`total_dp`,
                                    `kas_kasir`.`total_piutang`,
                                    `kas_kasir`.`total_pelunasan`,
                                    `kas_kasir`.`total_omset_nota`, 
                                    `kas_kasir`.`total_omset_nota_bb`, 
                                    `kas_kasir`.`total_piutang_nota_bb`, 
                                    `kas_kasir`.`total_pelunasan_nota_bb`, 
                                    `kas_kasir`.`total_bayar_hari_ini`,
                                    `kas_kasir`.`saldo_akhir_system`,
                                    `kas_kasir`.`saldo_revisi`,
                                    `kas_kasir`.`selisih`,
                                    `kas_kasir`.`st_selisih`,
                                    `kas_kasir`.`created_by`,
                                    `users`.`nm_lengkap`,
                                    `kas_kasir_closing_detail`.`idkategori`,
                                    `kategori`.`nmkategori`,
                                    `kas_kasir_closing_detail`.`value`")
                            ->join("users","kas_kasir.created_by = users.id_user","inner")
                            ->join("kas_kasir_closing_detail","kas_kasir.idkas_kasir = kas_kasir_closing_detail.idkas_kasir","left")
                            ->join("kategori","kas_kasir_closing_detail.idkategori = kategori.idkategori","left")
                            ->where("kas_kasir.idkas_kasir = '{$idkas_kasir}'")
                            ->order_by("`kas_kasir_closing_detail`.`idkategori`")
                            ->find_all();
        //echo $this->db->last_query();

        $this->template->set("data",$dt_rekap);

        $this->template->set_layout("cetak_struk");
        $this->template->title(lang('title_close'));
        $this->template->render("nota_closing");

    }

  
}

?>