$(function(){

	//$("input[name='jam_masuk[]']").timepicker();
	$("input[name='jam_buka[]']").mdtimepicker({
		timeFormat: 'hh:mm',
		format: 'hh:mm', 
		hourPadding: true 
	});

	$("input[name='jam_tutup[]']").mdtimepicker({
		timeFormat: 'hh:mm',
		format: 'hh:mm', 
		hourPadding: true 
	});	

	$("input[name='jam_buka_khusus']").mdtimepicker({
		timeFormat: 'hh:mm',
		format: 'hh:mm', 
		hourPadding: true 
	});	

	$("input[name='jam_tutup_khusus']").mdtimepicker({
		timeFormat: 'hh:mm',
		format: 'hh:mm', 
		hourPadding: true 
	});	

	$('#tgl_khusus').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	$('#batal').click(function(){

		$("#tgl_khusus").val('');
		$("#jam_buka_khusus").val('');
		$("#jam_tutup_khusus").val('');

	});

	$('#simpan_jam_khusus').click(function(){

		simpan_jam_masuk_khusus();

	});


});

function delete_jam_khusus(obj){

	var id 		= $(obj).closest("tr").find("input[name='id_jam_kerja[]']").val();

	var pesan 	= "Apakah Anda akan menghapus data setting Jam Kerja Khusus?";

	alertify.confirm(pesan,
	function(){ //ok
	
		var data 	= {id:id};

		ajaxloader("show");

		$.ajax({

					url 		: baseurl + 'jam_kerja/delete_jam_khusus',
					type 		: 'post',
					dataType 	: 'json',
					data 		: data,
					success 	: function(msg){

									var st = msg['st'];
									if(st == 1){

										$(obj).closest("tr").animate({backgroundColor:'red'}, 1000).fadeOut(1000,function() {
	    									$(obj).closest("tr").remove();
										});	

										alertify.success(msg['pesan']);

									}else{

										alertify.error(msg['pesan']);

									}

								}

		});

	},
	function(){ //cancel
		
		$("#st_delete").val(0);	
		alertify.success("Delete batal");

	});

}

function simpan_jam_masuk_khusus(){

	var tgl 		= $("#tgl_khusus").val();
	var jam_buka 	= $("#jam_buka_khusus").val();
	var jam_tutup 	= $("#jam_tutup_khusus").val();

	if(tgl.length <= 0 || jam_buka.length <= 0 || jam_tutup <= 0){

		alertify.error('Cek kembali inputan jam kerja');

	}

	var data 		= {tgl_khusus : tgl, jam_buka_khusus : jam_buka, jam_tutup_khusus : jam_tutup};

	ajaxloader("show");

	$.ajax({
				url 		: baseurl + "jam_kerja/simpan_jam_kerja_khusus",
				type 		: "post",
				dataType 	: "json",
				data 		: data,
				success 	: function(msg){

								var st = parseFloat(msg['st']);

								if(st == 1){

									//console.log(msg['pesan']);
									$("#daftar tbody tr").remove();
									$("#daftar tbody").append(msg['pesan']);
									alertify.success('Simpan Jam Kerja Baru, Sukses');

									$("#tgl_khusus").val('');
									$("#jam_buka_khusus").val('');
									$("#jam_tutup_khusus").val('');

								}else{

									//console.log(msg['pesan']);

									alertify.error(msg['pesan']);

								}

							}

	});

}


function cek_status(obj){

	var hari_ke 	= $(obj).data('hari');
	var value 		= $(obj).val();

	var id_jam_buka 	= "#jam_buka"+hari_ke;
	var id_jam_tutup 	= "#jam_tutup"+hari_ke;
	var id_area 		= "#area_jam"+hari_ke;

	if(value == 0){

		$(id_jam_buka).val('');
		$(id_jam_tutup).val('');
		$(id_area).hide(400);

	}else{

		var jam_buka_asli	= $(id_jam_buka).data("time");
		var jam_tutup_asli	= $(id_jam_tutup).data("time");

		$(id_jam_buka).val(jam_buka_asli);
		$(id_jam_tutup).val(jam_tutup_asli);
		$(id_area).show(400);

	}

}