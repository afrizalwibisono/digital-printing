<div class="box box-primary">
<?php echo form_open($this->uri->uri_string(),[
										'name'	=> 'form_jam_kerja',
										'id'	=> 'form_jam_kerja',
										'role'	=> 'form',
										'class'	=> 'form-horizontal'
										]) ?>


	<div class="box-body">

		<label class="label label-info">Jam Kerja Utama</label>
		<hr style="margin-top: 5px !important">
	
		<?php 

				foreach ($jam_kerja as $key => $isi) : 
						
					$day 			= $isi->id;
					$id_select 		= "st_buka_d".$day;
					$id_jam_buka 	= "jam_buka".$day;
					$id_jam_tutup 	= "jam_tutup".$day;
					$lang_hari		= "lbl-hari-ke-".$day;
					$id_area 		= "area_jam".$day;
		?>

		<div class="form-group">
			
			<label class="control-label col-sm-2"><?= lang($lang_hari) ?></label>
			<div class="col-sm-1" data-hari = "<?= $day ?>" style="padding-right: 0px !important">
				<select data-hari = "<?= $day ?>" onchange="cek_status(this)" class="form-control" name="st_buka[]" id="<?= $id_select ?>" >
					<option  value="1" <?= $isi->st_buka == 1 ? "selected='selected'" : '' ?> ><?= lang("isi-status-buka") ?></option>
					<option  value="0" <?= $isi->st_buka == 0 ? "selected='selected'" : '' ?> ><?= lang("isi-status-tutup") ?></option>
				</select>
			</div>
			<div class="col-sm-4" id="<?= $id_area ?>" style="<?= $isi->st_buka == 0 ? 'display: none' : ''; ?>">
				<div class="col-sm-8">
					<div class="input-group">
						<input type="text" name="jam_buka[]" id="<?= $id_jam_buka ?>" class="form-control" value = "<?= $isi->jam_buka ?>" >
						<span class="input-group-addon">s/d</span>
						<input type="text" name="jam_tutup[]" id="<?= $id_jam_tutup ?>" class="form-control" value = "<?= $isi->jam_tutup ?>" >
					</div>
				</div>
				<div class="col-sm-4" style="padding-left: 0px !important">
					<p class="small">* 24jam</p>
				</div>
			</div>

		</div>

		<?php

				endforeach;

		?>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-4">
				<button class="btn btn-primary" type="submit" name="simpan">
					<?= lang('btn-save') ?>
				</button>
			</div>
		</div>

		<label class="label label-warning">Jam Kerja Khusus</label>
		<hr style="margin-top: 5px !important">

		<div class="col-sm-2">
			
			<div class="form-group">	
				<label><?= lang("lbl-tgl-khusus") ?></label>
				<input type="text" name="tgl_khusus" id="tgl_khusus" class="form-control" readonly >
			</div>
		</div>
		<div class="col-sm-3">
			<div class="form-group">	
				<label><?= lang("lbl-jam-kerja") ?></label>
				<div class="input-group">
					<input type="text" name="jam_buka_khusus" id="jam_buka_khusus" class="form-control">
					<span class="input-group-addon">s/d</span>
					<input type="text" name="jam_tutup_khusus" id="jam_tutup_khusus" class="form-control">
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="form-group">
				<div style="padding-top: 25px;padding-left: 5px">
					<button class="btn btn-primary" type="button" name="simpan_jam_khusus" id="simpan_jam_khusus">
						<?= lang('btn-save') ?>
					</button>
					<?php
		                echo lang('btn-or');
		            ?>
		            <a href="#batal_jam_kerja" id="batal" ><?= lang('btn-batal') ?> </a>
	        	</div>
        	</div>			
		</div>

		<div class="clearfix"></div>

		<div class="table-responsive no-padding">
			
			<table class="table table-hover table-striped table-bordered" id="daftar">
				<thead>
					<tr class="success">
						<th class="text-center" width="30">#</th>
						<th class="text-center"><?= lang("table-jdl-tanggal") ?></th>
						<th class="text-center"><?= lang("table-jdl-jam") ?></th>
						<th class="text-center" width="50"></th>
					</tr>
				</thead>
				<tbody>
					<?php 
						
						if(isset($jam_kerja_khusus) && is_array($jam_kerja_khusus) && count($jam_kerja_khusus)) :
							foreach ($jam_kerja_khusus as $key => $isi) :	
					?>

						<tr>
							<td class="text-right">
								<?= $key+1 ?>
								<input type="hidden" name="id_jam_kerja[]" id="id_jam_kerja" value="<?= $isi->id ?>">
							</td>
							<td class='text-center'><?= $isi->tgl_pakai ?></td>
                            <td class='text-center'>
                            	<?php 
                            			$jam_kerja  = $isi->jam_buka." s/d ".$isi->jam_tutup;
                            			echo $jam_kerja;
                            	?>
                            </td>
                            <td class='text-center'>
                                <a href="#delete_jam_khusus" onclick="delete_jam_khusus(this)"><span class="fa fa-trash text-red"></span></a>
                            </td>
						</tr>

					<?php 
							endforeach;		
						endif;
					?>
				</tbody>
			</table>

		</div>


	</div>


<?= form_close() ?>

</div>