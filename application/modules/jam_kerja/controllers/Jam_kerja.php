<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for jam_operasi
	
*/
class Jam_kerja extends Admin_Controller {

	/**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Jam Kerja.View";
    protected $addPermission    = "Jam Kerja.Add";
    protected $managePermission = "Jam Kerja.Manage";
    protected $deletePermission = "Jam Kerja.Delete";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('jam_kerja/jam_kerja');
        $this->load->model(
                            [
                                'jam_fix_model',
                                'jam_khusus_model'
                            ]
    						);

        $this->template->title(lang('jam_op_title'));
		$this->template->page_icon('fa fa-clock-o');
    }

    public function index(){

        $this->auth->restrict($this->viewPermission);

        if(isset($_POST['simpan']) && has_permission($this->managePermission)){

            $simpan     = $this->simpan_jam_normal();

            if($simpan){

                $this->template->set_message(lang("konfirmasi-simpan-jam-kerja-normal"),"success");

            }

        }


        $dt_jam_umum    = $this->jam_fix_model
                            ->select("`idjam_operasi` AS id,
                                        `hari`,
                                        `jam_buka`,
                                        `jam_tutup`,
                                        `st_buka`")
                            ->where("deleted = 0")
                            ->order_by("idjam_operasi","asc")
                            ->find_all();

        $dt_jam_khusus  = $this->jam_khusus_model
                                ->select("`idjam_operasi_khusus` AS id,
                                            DATE_FORMAT(`tgl`, '%d/%m/%Y') AS tgl_pakai,
                                            `jam_buka`,
                                            `jam_tutup`")
                                ->where("deleted = 0")
                                ->order_by("tgl","desc")
                                ->find_all();

        $asset =   [
                        'plugins/md-time-picker/mdtimepicker.js',
                        'plugins/md-time-picker/mdtimepicker.css',
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'jam_kerja/assets/js/index_jam_kerja.js'
                    ];

        add_assets($asset);

        $this->template->set("jam_kerja",$dt_jam_umum);
        $this->template->set("jam_kerja_khusus",$dt_jam_khusus);

        $this->template->set("toolbar_title", lang('jam_op_title'));
        $this->template->set("page_title", lang('jam_op_title'));
        $this->template->render('index_jam_kerja'); 

    }
    
    public function delete_jam_khusus(){

        if(!$this->input->is_ajax_request()){

            redirect("jam_kerja");

        }

        if(!has_permission($this->deletePermission)){

            $dt_return  =   [
                                'st'    => 0,
                                'pesan' => lang("konfirmasi-error-permission-delete")
                            ];

            echo json_encode($return);

            return false;

        }else{

            $id = $this->input->post('id');

            $this->db->trans_start();

                $del    = $this->jam_khusus_model->delete($id);
                $sql    = $this->db->last_query();

            $this->db->trans_complete();

            if($this->db->trans_status() == false){

                $keterangan = "GAGAL, delete setting jam kerja";
                $status     = 0;
                $return     = true;

            }else{

                $keterangan = "SUKSES, delete setting jam kerja";
                $status     = 1;
                $return     = true;

            }

            $nm_hak_akses   = $this->deletePermission; 
            $kode_universal = "-";
            $jumlah         = 0;
            $sql            = $sql;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        
            if($return){

                $hasil  =   [
                                'st'    => 1,
                                'pesan' => lang('konfirmasi-delete-jam-kerja-khusus')
                            ];

            }else{

                $hasil  =   [
                                'st'    => 0,
                                'pesan' => lang('konfirmasi-error-delete-jam')
                            ];

            }

            echo json_encode($hasil);

            return false;

        }


    }

    public function simpan_jam_normal(){

        $status         = $this->input->post('st_buka');
        $jam_buka       = $this->input->post('jam_buka');
        $jam_tutup      = $this->input->post('jam_tutup');

        // start cek kondisi inputan jam

        $st_error       = 0;

        foreach ($status as $key => $isi) {
            
            if($isi == 1){

                if(strtotime($jam_buka[$key]) >= strtotime($jam_tutup[$key])){

                    $st_error   = 1;
                    break;

                }

            }

        }


        if($st_error == 1){

            $this->template->set_message(lang("konfirmasi-error-jam-kerja"),"error");
            return false;

        }else{

            $data   = [];

            foreach ($status as $key => $isi) {
                
                if($isi == 1){

                    $isi_jb     = $jam_buka[$key];
                    $isi_jt     = $jam_tutup[$key];

                }else{

                    $isi_jb     = null;
                    $isi_jt     = null;

                }

                $data[]     =   [
                                    'jam_buka'  => $isi_jb,
                                    'jam_tutup' => $isi_jt,
                                    'st_buka'   => $isi,
                                    'hari'      => $key+1
                                ];

            }

            $this->db->trans_start();

                $update = $this->jam_fix_model->update_batch($data,'hari');
                $sql    = $this->db->last_query();

            $this->db->trans_complete();

            if($this->db->trans_status()==false){

                $keterangan = "GAGAL, update setting jam kerja";
                $status     = 0;
                $return     = true;

            }else{

                $keterangan = "SUKSES, update setting jam kerja";
                $status     = 1;
                $return     = true;

            }

            $nm_hak_akses   = $this->managePermission; 
            $kode_universal = "";
            $jumlah         = 0;
            $sql            = $sql;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

            return $return;
        }


    }

    public function simpan_jam_kerja_khusus(){

        if(!$this->input->is_ajax_request()){

            redirect("jam_kerja");

        }

        if(!has_permission($this->addPermission)){

            $dt_return  =   [
                                'st'    => 0,
                                'pesan' => lang("konfirmasi-error-permission-add")
                            ];

            echo json_encode($return);

            return false;

        }else{

            $tgl            = $this->input->post('tgl_khusus');
            $jam_masuk      = $this->input->post('jam_buka_khusus');
            $jam_keluar     = $this->input->post('jam_tutup_khusus');

            $tgl_simpan     = date_ymd($tgl);

            if(strtotime($jam_masuk) > strtotime($jam_keluar)){

                $return     =   [
                                    'st'    => 0,
                                    'pesan' => lang("konfirmasi-error-jam-kerja")
                                ];

                echo json_encode($return);

                return false;


            }else{

                // start cek apakah sudah ada tanggal yang sama yang sudah diinputkan

                $cek    = $this->jam_khusus_model->where("deleted = 0 and tgl = '{$tgl_simpan}'")
                                ->count_all();

                if($cek){

                    $return     =   [
                                        'st'    => 0,
                                        'pesan' => lang("konfirmasi-error-tgl-ketemu")
                                    ];

                    echo json_encode($return);

                    return false;

                }

                // end cek apakah sudah ada tanggal yang sama yang sudah diinputkan                

                $data   =   [
                                'tgl'       => $tgl_simpan, 
                                'jam_buka'  => $jam_masuk, 
                                'jam_tutup' => $jam_keluar, 
                                'st_buka'   => 1
                            ];

                $this->db->trans_start();

                    $simpan = $this->jam_khusus_model->insert($data);
                    $sql    = $this->db->last_query();

                $this->db->trans_complete();                

                if($this->db->trans_status()==false){

                    $keterangan = "GAGAL, tambah data jam kerja khusus";
                    $status     = 0;
                    $return     = true;

                }else{

                    $keterangan = "SUKSES, tambah data jam kerja khusus";
                    $status     = 1;
                    $return     = true;

                }

                $nm_hak_akses   = $this->addPermission; 
                $kode_universal = "";
                $jumlah         = 0;
                $sql            = $sql;

                simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

                if($return){

                    $data   = $this->jam_khusus_model
                                    ->select("`idjam_operasi_khusus` AS id,
                                                DATE_FORMAT(`tgl`, '%d/%m/%Y') AS tgl_pakai,
                                                `jam_buka`,
                                                `jam_tutup`")
                                    ->where("deleted = 0")
                                    ->order_by("tgl","desc")
                                    ->find_all();

                    $isi_table  = "";

                    if(is_array($data) && count($data)){

                        foreach ($data as $key => $isi) {

                            $no_urut    = $key+1;
                            $jam_kerja  = $isi->jam_buka." s/d ".$isi->jam_tutup;

                            $isi_table  .=   "<tr>
                                                <td class='text-right'>
                                                    {$no_urut}
                                                    <input type='hidden' name='id_jam_kerja[]' id='id_jam_kerja' value='{$isi->id}' >
                                                </td>
                                                <td class='text-center'>{$isi->tgl_pakai}</td>
                                                <td class='text-center'>{$jam_kerja}</td>
                                                <td class='text-center'>
                                                    <a href='#delete_jam_khusus' onclick='delete_jam_khusus(this)'><span class='fa fa-trash text-red'></span></a>
                                                </td>
                                            </tr>";
                        }


                    }

                    $dt_return  =   [
                                        'st'    => 1,
                                        'pesan' => $isi_table
                                    ];

                    echo json_encode($dt_return);

                    return false;

                }


            }

        }

    }


}	