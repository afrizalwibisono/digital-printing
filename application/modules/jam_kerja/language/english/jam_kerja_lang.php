<?php defined('BASEPATH')|| exit('No direct script access allowed');


// ========================================================================================================
// ..:: Title ::..
$lang['jam_op_title']						= 'Jam Kerja';

// ========================================================================================================
// ..:: Form ::..

$lang['lbl-jam-kerja-utama']					= 'Jam Kerja Utama';

$lang['lbl-hari-ke-1']							= 'Senin';
$lang['lbl-hari-ke-2']							= 'Selasa';
$lang['lbl-hari-ke-3']							= 'Rabo';
$lang['lbl-hari-ke-4']							= 'Kamis';
$lang['lbl-hari-ke-5']							= "Jum'at";
$lang['lbl-hari-ke-6']							= "Sabtu";
$lang['lbl-hari-ke-7']							= "Minggu";

$lang['lbl-tgl-khusus']							= "Tanggal";
$lang['lbl-jam-kerja']							= "Jam Kerja";

$lang['isi-status-buka']						= "Buka";
$lang['isi-status-tutup']						= "Tutup";

$lang['lbl-jam-kerja-khusus']					= 'Jam Kerja Khusus';

$lang['table-jdl-tanggal']						= 'Tanggal';
$lang['table-jdl-jam']							= 'Jam Kerja';



// ========================================================================================================
// ..:: Univ Lang ::..

$lang['btn-save']							= "Simpan";
$lang['btn-batal']							= "Batal";
$lang['btn-delete']							= "Hapus";
$lang['btn-or']								= "atau";

$lang['btn-batal-kembali']					= "batal / kembali";

// ========================================================================================================
// ..:: Konfirmasi ::..

$lang['konfirmasi-data-tidak-ada']			= "Data tidak ditemukan";

$lang['konfirmasi-simpan-jam-kerja-normal']	= "Jam kerja baru, sukses disimpan";
$lang['konfirmasi-delete-jam-kerja-khusus']	= "Jam kerja khusus, sukses dihapus";

$lang['konfirmasi-error-jam-kerja']			= "Jam kerja baru tidak dapat disimpan, pastikan inputan Anda benar";
$lang['konfirmasi-error-permission-add']	= "Anda tidak memiliki hak untuk menambahkan Jam Kerja baru";
$lang['konfirmasi-error-permission-delete']	= "Anda tidak memiliki hak untuk menghapus data Jam Kerja";
$lang['konfirmasi-error-tgl-ketemu']		= "Tanggal yang sama telah Anda inputkan ke dalam system";
$lang['konfirmasi-error-delete-jam']		= "Proses Delete gagal, refresh kembali halaman ini";

