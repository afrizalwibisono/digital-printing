<?php defined('BASEPATH') || exit('No direct script access allowed');

// =============================================================================================

$lang['judul']									= "Approval Piutang Order PO";
$lang['judul-history']							= "History Approval PO";


$lang['lbl_isi_kategori_0']						= 'Umum / Konsumen Biasa';
$lang['lbl_isi_kategori_1']						= 'Reseller';
$lang['lbl_isi_kategori_2']						= 'Instansi / Perusahaan';
$lang['lbl_isi_kategori_all']					= '';

$lang['jdl-table-index-tgl']					= 'Tanggal';
$lang['jdl-table-index-gol-konsumen']			= 'Gol. Konsumen';
$lang['jdl-table-index-konsumen']				= 'Konsumen';
$lang['jdl-table-index-total']					= 'Total';
$lang['jdl-table-index-st-piutang']				= 'Status Bayar';
$lang['jdl-table-view-op']						= 'Operator';
$lang['jdl-table-waktu-proses']					= 'Waktu Proses';

$lang['isi-stapp-cash']							= 'Harus Lunas';	
$lang['isi-stapp-piutang']						= 'Boleh Piutang';	

$lang['btn-filter']								= "Filter";
$lang['btn-save']								= "Simpan";
$lang['btn-batal']								= "Batal";
$lang['btn-delete']								= "Hapus";
$lang['btn-or']									= "atau";

$lang['btn-history']							= "History";

$lang['btn-batal-kembali']						= "batal / kembali";

// ============================================================================================

$lang['konfirmasi-sukses-proses-simpan']		= "Proses Perubahan Status Metode Pembayaran Sukses";

$lang['konfirmasi-data-tidak-ada']				= "Data tidak ditemukan";

$lang['konfirmasi-error-manage-akses']			= "Anda tidak memiliki hak akses.<br>Hubungi Administrator Anda untuk mendapatkan hak akses";
$lang['konfirmasi-error-data-tidak-ada']		= "Proses gagal.<br>Cek kembali data order, pastikan data masih berstatus Order";
$lang['konfirmasi-error-proses-simpan']			= "Proses gagal.<br>Error system";

