$(function(){

	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	$("#gol_konsumen").select2({
		placeholder :"Golongan",
        allowClear  : true
	});

	$("#konsumen").select2({
		placeholder :"konsumen",
        allowClear  : true
	});

	$("#gol_konsumen").change(function(){

  		$("#id_konsumen_pilih").val('');
  		get_nama_konsumen();

  	});

	get_nama_konsumen();

});

function simpan_perubahan(id_order, st_acc_baru, obj){

	var data 	= {id : id_order, st : st_acc_baru};

	$.ajax({
				url 		: baseurl + "approve_po/set_status_approve",
				type 		: "post",
				data 		: data,
				dataType 	: "json",
				success 	: function(msg){

								var st 	= parseFloat(msg['st']);

								if(st == 0){

									alertify.error(msg['message']);

								}else if(st == 2){

									alertify.error(msg['message']);									
									$(obj).closest("tr").animate({backgroundColor:'red'}, 1000).fadeOut(1000,function() {
    									$(obj).closest("tr").remove();
									});	

								}else{

									alertify.success(msg['message']);	
									$(obj).closest("tr").addClass('success');

								}

							}

	});

}

function perubahan_status(obj){

	var id_order	= $(obj).closest("tr").find("input[name='dft_id[]']").val();
	var st_awal 	= $(obj).closest("tr").find("input[name='dft_isi_st_acc[]']").val();
	var st_baru 	= $(obj).closest("tr").find("select[name='dft_st_acc[]']").val();
	
	alertify.confirm("Anda yakin akan merubah Status Metode Pembayarannya?",
	  	function(){ // tombol ok
	    	
	  		simpan_perubahan(id_order, st_baru, obj);

	  	},
	  	function(){ // tombol cancel
	    	var test = $(obj).closest("tr").find("select[name='dft_st_acc[]']").val(st_awal);
	    	alertify.warning('Status baru dibatalkan, dan dikembalikan ke Status awal');
	  	});

}

function get_nama_konsumen(){

	var jns_konsumen 	= $("#gol_konsumen").val();
	var id_pilih 		= $("#id_konsumen_pilih").val();

	var st_ada_pilih 	= id_pilih.length > 0 ? true : false;

	$.ajax({
				url 		: baseurl + "approve_po/get_konsumen",
				type 		: "post",
				dataType 	: "json",
				data 		: {st : jns_konsumen},
				success 	: function(msg){

								$("#konsumen option").remove();

								if(msg['st'] == 1){

									var isi 	= "<option></option>";
									var select 	= "";

									$.each(msg['data'],function(i,n){

										if(st_ada_pilih  && n['id'] == id_pilih){

											select 	= "selected";

										}else{

											select = "";
										}

										isi += "<option value='" +n['id']+"' " + select + ">"+ n['nm'] +"</option>";

									});

									$("#konsumen").append(isi);

								}

							}


	});

}