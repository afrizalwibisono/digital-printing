<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for approval po

*/
class Approve_po extends Admin_Controller {

	protected $viewPermission   		= "Approve Order PO - Piutang.View";
    protected $managePermission    		= "Approve Order PO - Piutang.Manage";
    protected $deletePermission 		= "Approve Order PO - Piutang.Delete";

    public function __construct(){

        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('approve_po/approve_po');
        $this->load->model( [
    							"order_model",
    							"konsumen_model",
                                "approve_model",
                                "gol_konsumen/gol_konsumen_model"
        					]	
    						);

        $this->template->title(lang('judul'));
		$this->template->page_icon('fa fa-clipboard');
    }

    public function index(){

        $this->auth->restrict($this->viewPermission);        

        $where  = "`order_produk`.`st_po` = 1 
                    and `order_produk`.st_kasir = 0 
                    and `order_produk`.st_nota = 0 
                    and `order_produk`.deleted =0";

        $tgl1       = isset($_POST['tgl1']) ? $this->input->post('tgl1') : '';
        $tgl2       = isset($_POST['tgl2']) ? $this->input->post('tgl2') : '';

        $gol_kons   = isset($_POST['gol_konsumen']) ? $this->input->post('gol_konsumen') : '';        
        $konsumen   = isset($_POST['konsumen']) ? $this->input->post('konsumen') : '';                

        if(strlen($tgl1) > 0){

            $tgl1_cr    = date_ymd($tgl1);
            $tgl2_cr    = date_ymd($tgl2);        
             
        }else{

            $tgl1_cr    = date("Y-m-d");
            $tgl2_cr    = date("Y-m-d");

            $tgl1       = date("d/m/Y");
            $tgl2       = date("d/m/Y");

        }

        $where      .= " and (order_produk.tgl_order >= '{$tgl1_cr}' and order_produk.tgl_order <= '{$tgl2_cr}')";

        $filter     =   [
                            'tgl1'      => $tgl1,
                            'tgl2'      => $tgl2,
                            'gol_kons'  => $gol_kons,
                            'konsumen'  => $konsumen
                        ];


        if(strlen($konsumen) > 0){

            $where  .= " and `konsumen`.`nama` = '{$konsumen}'";

        }else{

            if(strlen($gol_kons) > 0){

                $where  .= " and `konsumen`.`st` = '{$gol_kons}'";

            }

        }

        $data   = $this->order_model->select("`order_produk`.`id_order`,
                                                `order_produk`.`no_transaksi`,
                                                DATE_FORMAT(`order_produk`.`tgl_order`, '%d/%m/%Y') AS tgl_order,
                                                DATE_FORMAT(`order_produk`.`tgl_permintaan_selesai`,
                                                        '%d/%m/%Y') AS tgl_selesai,
                                                `order_produk`.`total_value_order`,
                                                `konsumen`.`panggilan`,
                                                `konsumen`.`nama`,
                                                `konsumen`.`st`,
                                                gol_harga.nm_gol as st_konsumen,
                                                `order_produk`.`st_acc_po_tempo` AS st_acc")
                                    ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                                    ->join('gol_harga','gol_harga.id_gol = konsumen.st', "inner")
                                    ->where($where)
                                    ->order_by("order_produk.tgl_order","desc")
                                    ->find_all();

        //echo $this->db->last_query();

        $dt_gol     = $this->gol_konsumen_model
                        ->select("`id_gol` as id,`nm_gol` as nm")
                        ->where("deleted = 0")
                        ->order_by('nm_gol')
                        ->find_all();

        $asset  =   [
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'approve_po/assets/js/approve_po_index.js'
                    ]; 

        add_assets($asset);

        $this->template->set("dt_gol",$dt_gol);
        $this->template->set("data",$data);
        $this->template->set("filter",$filter);

    	$this->template->set("toolbar_title", lang('judul'));
        $this->template->set("page_title", lang('judul'));
        $this->template->render('approve_po_index'); 

    }

    public function set_status_approve(){

        /*
            Keterangan Status return.

                1. 0 = error, memberikan keterangan.
                2. 1 = sukses
                3. 2 = error, data harus dihapus dari tabel index

        */

        if(!$this->input->is_ajax_request()){

            redirect("approve_po");

        }

        if(!has_permission($this->managePermission)){

            $return     =   [
                                'st'        => 0,
                                'message'   => lang('konfirmasi-error-manage-akses')
                            ];

            echo json_encode($return);

            return false;

        }

        $id_order   = $this->input->post('id');
        $status     = $this->input->post('st');

        // start cek apakah data order sudah menjadi kasir

        $dt_cek     = $this->order_model->select('id_order')    
                                        ->where(" id_order = '{$id_order}' and (deleted = 1 or st_kasir = 1 or st_nota = 1)")
                                        ->count_all();

        if($dt_cek > 0){

            $return     =   [
                                'st'        => 2,
                                'message'   => lang('konfirmasi-error-data-tidak-ada')
                            ];

            echo json_encode($return);

            return false;

        }

        // end cek apakah data order sudah menjadi kasir

        $id_primary = gen_primary('','app_po_piutang','id_app_po_piutang');

        //start simpan data perubahan dan history approval

        $sql    = "";

        $this->db->trans_start();

        $arr_simpan_history =   [
                                    'id_app_po_piutang' => $id_primary, 
                                    'id_order'          => $id_order, 
                                    'st_acc'            => $status
                                ];

        $this->approve_model->insert($arr_simpan_history);
        $sql    = $this->db->last_query();

        $arr_update     =   [
                                'st_acc_po_tempo' => $status
                            ];

        $this->order_model->update($id_order,$arr_update);
        $sql    .= "\n\n". $this->db->last_query();

        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            $keterangan = "GAGAL, update approval piutang order po";
            $status     = 0;
            $return     = true;

        }else{

            $keterangan = "SUKSES, update approval piutang order po";
            $status     = 1;
            $return     = true;

        }

        $nm_hak_akses   = $this->managePermission; 
        $kode_universal = "";
        $jumlah         = 0;
        $sql            = $sql;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        //end simpan data perubahan dan history approval

        if($return){

            $return     =   [
                                'st'        => 1,
                                'message'   => lang('konfirmasi-sukses-proses-simpan')
                            ];

        }else{

            $return     =   [
                                'st'        => 0,
                                'message'   => lang('konfirmasi-error-proses-simpan')
                            ];

        }

        echo json_encode($return);

    }

    public function get_konsumen(){

        if(!$this->input->is_ajax_request()){

            redirect("approve_po");

        }

        $st_konsumen    = $this->input->post("st");

        $data   = $this->konsumen_model->select("`idkonsumen` as id,
                                                CONCAT(`konsumen`.`panggilan`,
                                                ' ',
                                                `konsumen`.`nama`) AS nm")
                        ->where("st = {$st_konsumen}")
                        ->order_by("nm")
                        ->find_all();

        if(is_array($data) && count($data)){

            $isi    = ["st" => 1, "data" => $data];

        }else{

            $isi    = ["st" => 0, "data" => ""];

        }

        echo json_encode($isi);

    }

    public function history(){

        $this->auth->restrict($this->viewPermission);

        $where  = "";

        if(isset($_POST['tgl1'])){

            $tgl1       = isset($_POST['tgl1']) ? $this->input->post('tgl1') : '';
            $tgl2       = isset($_POST['tgl2']) ? $this->input->post('tgl2') : '';

            $gol_kons   = isset($_POST['gol_konsumen']) ? $this->input->post('gol_konsumen') : '';        
            $konsumen   = isset($_POST['konsumen']) ? $this->input->post('konsumen') : '';                

        }else{

            $tgl1       = isset($_GET['tgl1']) ? $this->input->get('tgl1') : '';
            $tgl2       = isset($_GET['tgl2']) ? $this->input->get('tgl2') : '';

            $gol_kons   = isset($_GET['gol_konsumen']) ? $this->input->get('gol_konsumen') : '';        
            $konsumen   = isset($_GET['konsumen']) ? $this->input->get('konsumen') : '';                                        

        }

        if(strlen($tgl1) > 0){

            $tgl1_cr    = date_ymd($tgl1)." 00:00:01";
            $tgl2_cr    = date_ymd($tgl2)." 23:59:59";        
             
        }else{

            $tgl1_cr    = date("Y-m-d")." 00:00:01";
            $tgl2_cr    = date("Y-m-d")." 23:59:59";

            $tgl1       = date("d/m/Y");
            $tgl2       = date("d/m/Y");

        }

        $where      .= " (app_po_piutang.created_on >= '{$tgl1_cr}' and app_po_piutang.created_on <= '{$tgl2_cr}')";
        $filter     = "?tgl1={$tgl1}&tgl2={$tgl2}";

        $isi_filter =   [
                            'tgl1'      => $tgl1,
                            'tgl2'      => $tgl2,
                            'gol_kons'  => $gol_kons,
                            'konsumen'  => $konsumen
                        ];


        if(strlen($konsumen) > 0){

            $where  .= " and `konsumen`.`nama` = '{$konsumen}'";
            $filter .= "&konsumen={$konsumen}";

        }else{

            if(strlen($gol_kons) > 0){

                $where  .= " and `konsumen`.`st` = '{$gol_kons}'";
                $filter .= "&gol_konsumen={$gol_kons}";

            }

        }

        $this->load->library('pagination');

        $total  = $this->order_model
                            ->select('order_produk.id_order')
                            ->join("`konsumen`","`order_produk`.`id_konsumen` = `konsumen`.`idkonsumen`","inner")
                            ->join("app_po_piutang","order_produk.id_order = app_po_piutang.id_order","inner")
                            ->join("users","app_po_piutang.created_by = users.id_user","inner")
                            ->where($where)
                            ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data   =   $this->order_model
                            ->select("`order_produk`.`id_order`,
                                        `order_produk`.`no_transaksi`,
                                        DATE_FORMAT(`order_produk`.`tgl_order`, '%d/%m/%Y') AS tgl_order,
                                        DATE_FORMAT(`order_produk`.`tgl_permintaan_selesai`,
                                                '%d/%m/%Y') AS tgl_selesai,
                                        `order_produk`.`total_value_order`,
                                        `konsumen`.`panggilan`,
                                        `konsumen`.`nama`,
                                        `konsumen`.`st`,
                                        gol_harga.nm_gol as st_konsumen,
                                        `app_po_piutang`.`st_acc` AS `st_acc`,
                                        date_format(`app_po_piutang`.created_on,'%d/%m/%Y %H:%i:%s') as waktu_proses,
                                        `users`.`nm_lengkap` AS op")
                            ->join("`konsumen`","`order_produk`.`id_konsumen` = `konsumen`.`idkonsumen`","inner")
                            ->join('gol_harga','gol_harga.id_gol = konsumen.st', "inner")
                            ->join("app_po_piutang","order_produk.id_order = app_po_piutang.id_order","inner")
                            ->join("users","app_po_piutang.created_by = users.id_user","inner")
                            ->where($where)
                            ->order_by("app_po_piutang.created_on","desc")
                            ->find_all();

        $dt_gol     = $this->gol_konsumen_model
                            ->select("`id_gol` as id,`nm_gol` as nm")
                            ->where("deleted = 0")
                            ->order_by('nm_gol')
                            ->find_all();

        $asset  =   [
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'approve_po/assets/js/approve_po_history.js'
                    ]; 

        add_assets($asset);

        $this->template->set("dt_gol",$dt_gol);
        $this->template->set("data",$data);
        $this->template->set("filter",$isi_filter);

        $this->template->title(lang('judul-history'));
        $this->template->set("toolbar_title", lang('judul-history'));
        $this->template->set("page_title", lang('judul-history'));
        $this->template->render('approve_po_view'); 


    }


}
	
?>