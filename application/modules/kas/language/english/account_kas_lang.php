<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['account_kas_title_manage'] 	= 'Account Kas';
$lang['account_kas_title_new'] 		= 'Account Kas Baru';
$lang['account_kas_title_edit'] 	= 'Edit Account Kas';

// form/table
$lang['account_kas_code'] 			= 'Kode';
$lang['account_kas_name'] 			= 'Nama Account Kas';
$lang['account_kas_gol'] 			= 'Golongan Account';
$lang['account_kas_ket'] 			= 'Keterangan';
$lang['account_kas_can_del'] 		= 'Boleh dihapus';

$lang['account_kas_yes'] 			= 'Ya';
$lang['account_kas_no'] 			= 'Tidak';

// button
$lang['account_kas_btn_new'] 		= 'Baru';
$lang['account_kas_btn_delete'] 	= 'Hapus';
$lang['account_kas_btn_save'] 		= 'Simpan';
$lang['account_kas_btn_cancel'] 	= 'Batal';

// messages
$lang['account_kas_del_error']			= 'Anda belum memilih account kas yang akan dihapus.';
$lang['account_kas_del_failure']		= 'Tidak dapat menghapus account kas: ';
$lang['account_kas_delete_confirm']		= 'Apakah anda yakin akan menghapus account kas terpilih ?';
$lang['account_kas_deleted']			= 'Account Kas berhasil dihapus';
$lang['account_kas_no_records_found'] 	= 'Data tidak ditemukan.';

$lang['account_kas_create_failure'] 	= 'Account Kas baru gagal disimpan: ';
$lang['account_kas_create_success'] 	= 'Account Kas baru berhasil disimpan';

$lang['account_kas_edit_success'] 		= 'Account Kas berhasil disimpan';
$lang['account_kas_edit_failed'] 		= 'Account Kas gagal disimpan';
$lang['account_kas_invalid_id'] 		= 'ID Tidak Valid';