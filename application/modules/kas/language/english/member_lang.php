<?php defined('BASEPATH') || exit('No direct script access allowed');

/* ...::: Harga Golongan :::... */

$lang['judul_harga'] 							= 'Biaya Registrasi Membership';
$lang['judul_tambah_data_baru_harga']			= 'Data Biaya Registrasi Baru';
$lang['judul_edit_data_harga']					= 'Edit Data Biaya Registrasi';

$lang['label_nama_gol_harga']			 		= 'Golongan Member';
$lang['label_masa_berlaku_harga']			 	= 'Masa Berlaku';
$lang['label_biaya_harga']			 			= 'Biaya Registrasi';
$lang['label_aktive_harga']			 			= 'Status Pakai';

$lang['informasi_simpan_sukses_harga']			= 'Penyimpanan Data Biaya Registrasi Sukses';
$lang['informasi_simpan_gagal_harga']			= 'Penyimpanan Data Biaya Registrasi Gagal';
$lang['informasi_edit_sukses_harga']			= 'Editing Data Biaya Registrasi Sukses';
$lang['informasi_delete_sukses_harga']			= 'Data Biaya Registrasi Sukses diHapus';
$lang['informasi_nonaktif_gagal']				= 'Gagal, menonaktifkan biaya pendaftaran lama';

$lang['konfirmasi_delete_harga']				= 'Apakah anda yakin akan menghapus Biaya Registrasi terpilih ?';

$lang['label_pilihan_aktiv'] 					= 'Aktif';
$lang['label_pilihan_tidakAktiv'] 				= 'Tidak Aktif';


// ...::: Golongan member :::...

$lang['judul_gol'] 								= 'Golongan Membership';
$lang['judul_tambah_data_baru_gol']				= 'Data Golongan Member Baru';
$lang['judul_edit_data_gol']					= 'Edit Data Golongan Member';

$lang['label_nama_gol']			 				= 'Nama Golongan';
$lang['label_masa_berlaku_gol']			 		= 'Masa Berlaku';

$lang['informasi_simpan_sukses_gol']			= 'Penyimpanan Data Golongan Member Sukses';
$lang['informasi_simpan_gagal_gol']				= 'Penyimpanan Data Golongan Member Gagal';
$lang['informasi_edit_sukses_gol']				= 'Editing Data Golongan Member Sukses';
$lang['informasi_delete_sukses_gol']			= 'Data Golongan Member Sukses diHapus';

$lang['konfirmasi_delete_gol']					= 'Apakah anda yakin akan menghapus Golongan Member terpilih ?';


// ...::: Umum :::...

$lang['label_ket']								= 'Keterangan';

$lang['tombol_tambah_baru']						= 'Baru';
$lang['tombol_hapus']							= 'Hapus';
$lang['tombol_simpan']							= 'Simpan';
$lang['tombol_batal']							= 'Batal';
$lang['penghubung']								= 'atau';

$lang['konfirmasi_awal_gagal_delete'] 			= "Hanya";
$lang['informasi_delete_gagal']					= "yang dapat dihapus, cek kembali data Anda";

$lang['Warning_data_kosong']					= 'Data tidak ditemukan';
$lang['Warning_kode_edit_kosong']				= 'ID tidak valid!!';
$lang['Warning_tidak_punya_hak_delete']			= 'Hapus gagal, Anda tidak memiliki hak akses';
$lang['warning_data_kosong']					= 'Data tidak ditemukan';
$lang['warning_tidak_unique']					= 'Data yang sama telah terdaftar dalam system';
$lang['konfirmasi_delete']						= 'Apakah anda yakin akan menghapus data terpilih ?';