<?php defined('BASEPATH') || exit('No direct script access allowed');

//....:: Promo :::...
$lang['judul_daftar'] 							= 'Pencatatan Penggunaan Kas';
$lang['judul_baru'] 							= 'Tambah Data Penggunaan Kas';

$lang['label_id_account']						= 'Account Kas';
$lang['label_ket_trans']						= 'Keterangan Transaksi';
$lang['label_status_alur']						= 'Status';
$lang['label_nilai_total']						= 'Total';
$lang['label_waktu']							= 'Waktu Transaksi';
$lang['label_cabang']							= 'Cabang';
$lang['label_model_transaksi']					= 'Model Transaksi';
$lang['label_bank']								= 'Kode Bank';

$lang['label_via_bank']							= 'Via Bank';
$lang['label_tunai']							= 'Tunai';

$lang['label_status_keluar']					= 'Arus Uang Keluar';
$lang['label_status_masuk']						= 'Arus Uang Masuk';

$lang['label_tabel_total_masuk']				= 'Uang Masuk';
$lang['label_tabel_total_keluar']				= 'Uang Keluar';

$lang['label_simpan_sukses']					= 'Penyimpanan Pencatatan Kas Sukses';
$lang['label_simpan_gagal']						= 'Penyimpanan Pencatatan Kas Gagal';

$lang['label_place_holder_range']				= 'Range Tanggal';
$lang['label_place_holder_account']				= '------ Pilih Account Kas ------';
$lang['label_place_holder_bank']				= '--Pilih Bank--';
$lang['label_place_holder_model_transaksi']		= '--Pilih Model Pembayaran--';

// ...::: Umum :::...

$lang['label_pilihan_aktiv'] 					= 'Aktif';
$lang['label_pilihan_tidakAktiv'] 				= 'Tidak Aktif';

$lang['label_ket']								= 'Keterangan';

$lang['tombol_tambah_baru']						= 'Baru';
$lang['tombol_hapus']							= 'Hapus';
$lang['tombol_simpan']							= 'Simpan';
$lang['tombol_batal']							= 'Batal';
$lang['penghubung']								= 'atau';

$lang['konfirmasi_awal_gagal_delete'] 			= "Hanya";
$lang['informasi_delete_gagal']					= "yang dapat dihapus, cek kembali data Anda";

$lang['Warning_kode_edit_kosong']				= 'ID tidak valid!!';
$lang['Warning_tidak_punya_hak_delete']			= 'Hapus gagal, Anda tidak memiliki hak akses';
$lang['warning_data_kosong']					= 'Data tidak ditemukan';
$lang['warning_tidak_unique']					= 'Data yang sama telah terdaftar dalam system';
$lang['konfirmasi_delete']						= 'Apakah anda yakin akan menghapus data terpilih ?';