<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is model class for table "alur_kas"
 */

class Kas_model extends BF_Model
{

    /**
     * @var string  User Table Name
     */
    protected $table_name   = 'alurkas';
    protected $key          = 'idalurkas';

    protected $soft_deletes = true;

    protected $set_created  = true;

    protected $set_modified = false;
    
    protected $date_format  = 'datetime';
    
    protected $log_user = true;

    /**
     * Function construct used to load some library, do some actions, etc.
     */
    public function __construct()
    {
        parent::__construct();
    }

    function simpan_alurkas_manual($kode_accountKas = null, $ket = "", $total = null , $status = null, $nm_hak_akses = "",$waktu = 'now()',$model_transaksi="",$id_bank=null){

        if($kode_accountKas == null || $ket == "" || $total == null || $status == null){

            return false;

        }

        //Get ID Cabang
        $usr_session = $this->session->userdata('cokes_session');
        $id_cabang = $usr_session['id_cabang'];

        $kodePakai  = gen_primary("AKL/","alurkas","idalurkas");

        $data   = array( 
                        'idalurkas'         => $kodePakai,
                        'id_cabang'         => $id_cabang,
                        'id_account_kas'    => $kode_accountKas,
                        'ket'               => $ket,
                        'total'             => $total,
                        'status'            => $status,
                        'waktu'             => $waktu,
                        'model_transaksi'   => $model_transaksi,
                        'id_bank'           => $id_bank
                    );

        $this->insert($data);
        $sql = $this->db->last_query();
        //buat keterangan LOG Aktivitas
        if($status == "0"){

            $keteranganSukses = 'Sukses, tambahkan alurkas Keluar untuk kode kas : '.$kode_accountKas;
            $keteranganGagal = 'Gagal, tambahkan alurkas Keluar untuk kode kas : '.$kode_accountKas;

        }else{

            $keteranganSukses = 'Sukses, tambahkan alurkas Masuk untuk kode kas : '.$kode_accountKas;
            $keteranganGagal = 'Gagal, tambahkan alurkas Masuk untuk kode kas : '.$kode_accountKas;

        }

        //cek apakah data sudah masuk
        $cek = $this->find($kodePakai);

        if($cek != false ){

            $return = true;
            simpan_aktifitas($nm_hak_akses,$kode_accountKas,$keteranganSukses,$total,$sql,1);

        }else{

            $return = false;
            simpan_aktifitas($nm_hak_akses,$kode_accountKas,$keteranganGagal,$total,$sql,0);

        }

        return $return;

    }

    public function simpan_alurKas($kode_accountKas = null, $ket = "", $total = null , $status = null, $nm_hak_akses = "", $model_transaksi = 0, $id_bank = 0, $tgl_proses = ""){

        if( $kode_accountKas === null || $ket === ""  || $total === null || $status === null){

            //return false;
            return $kode_accountKas."-".$ket."-".$total."-".$status;

        }

        //Get ID Cabang
        $usr_session = $this->session->userdata('cokes_session');
        $id_cabang = $usr_session['id_cabang'];

        $kodePakai  = "AK/".gen_primary();

         if(strlen($tgl_proses) > 0){

            $waktu      = $tgl_proses;

        }else{

            $waktu      = date("Y-m-d h:i:s");

        }  

        /* 
            Jika $model_transaksi = 1 / via bank, dan $id_bank = 0, maka akan otomatis dimasukkan ke bank devault

        */  

        if($model_transaksi == 1 && $id_bank == 0 ){

            $dt_bank    = $this->db->query("SELECT `id_bank` FROM `bank` where st_default = 1")->result();
            if(count($dt_bank) && is_array($dt_bank)){

                $id_bank    = $dt_bank[0]->id_bank;

            }

        }


        $data   = array( 
                        'idalurkas'         => $kodePakai,
                        'id_cabang'         => $id_cabang,
                        'id_account_kas'    => $kode_accountKas,
                        'ket'               => $ket,
                        'total'             => $total,
                        'status'            => $status,
                        'waktu'             => $waktu,
                        'model_transaksi'   => $model_transaksi,
                        'id_bank'           => $id_bank
                    );

        $this->insert($data);
        $sql = $this->db->last_query();
        //buat keterangan LOG Aktivitas
        if($status == "0"){

            $keteranganSukses = 'Sukses, tambahkan alurkas Keluar untuk kode kas : '.$kode_accountKas;
            $keteranganGagal = 'Gagal, tambahkan alurkas Keluar untuk kode kas : '.$kode_accountKas;

        }else{

            $keteranganSukses = 'Sukses, tambahkan alurkas Masuk untuk kode kas : '.$kode_accountKas;
            $keteranganGagal = 'Gagal, tambahkan alurkas Masuk untuk kode kas : '.$kode_accountKas;

        }

        //cek apakah data sudah masuk
        $cek = $this->find($kodePakai);

        if($cek != false ){

            $return = true;
            simpan_aktifitas($nm_hak_akses,$kode_accountKas,$keteranganSukses,$total,$sql,1);

        }else{

            $return = false;
            simpan_aktifitas($nm_hak_akses,$kode_accountKas,$keteranganGagal,$total,$sql,0);

        }

        return $return;

    }


}
