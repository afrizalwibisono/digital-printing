<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('name'=>'frm_baru', 'id'=>'frm_baru','class'=>'form-horizontal')) ?>
	<div class="box-body">
		
		<div class="form-group <?= form_error('model_transaksi') ? ' has-error' : ''; ?>">
			<label for="model_transaksi"	class="control-label col-sm-2"><?= lang('label_model_transaksi') ?></label>
			<div class="col-sm-4">
				<select class="form-control" name="model_transaksi" id="model_transaksi">
					<option value="0" <?= set_select('model_transaksi','0') ?> ><?= lang('label_tunai') ?> </option>
					<option value="1" <?= set_select('model_transaksi','1') ?> ><?= lang('label_via_bank') ?> </option>
				</select>
			</div>
		</div>
		<div style="display: none;" class="form-group <?= form_error('id_bank') ? ' has-error' : ''; ?> trans_model">
			<label for="id_account" class="control-label col-sm-2"><?= lang('label_bank') ?></label>
			<div class="col-sm-3">
				<select class="form-control" name="id_bank" id="id_bank">
					<?php 
						if(isset($dt_bank) && is_array($dt_bank) && count($dt_bank)):
							foreach ($dt_bank as $key => $bank):
					?>
							<option value="<?= $bank->id_bank ?>"  <?= set_select('id_bank',$bank->id_bank) ?> ><?= $bank->nama_bank; ?></option>
					<?php
							endforeach;
						endif;
					?>
				</select>
			</div>
		</div>

		<div class="form-group <?= form_error('waktu_trans') ? ' has-error' : ''; ?>">
			<label for="waktu_trans" class="control-label col-sm-2"><?= lang('label_waktu') ?></label>
			<div class="col-sm-4">
				<input type="text" name="waktu_trans" id="waktu_trans" class="form-control" value="<?= set_value('waktu_trans',isset($waktu_sekarang) ? $waktu_sekarang : '') ?>" placeholder="dd/mm/yyyy">
			</div>
		</div>

		<div class="form-group <?= form_error('id_account') ? ' has-error' : ''; ?>">
			<label for="id_account" class="control-label col-sm-2"><?= lang('label_id_account') ?></label>
			<div class="col-sm-4">
				<select class="form-control" name="id_account" id="id_account">
					<option value=""></option>
					<?php 
						if(isset($dt_acc) && is_array($dt_acc) && count($dt_acc)):
							foreach ($dt_acc as $key => $isi):
					?>
							<option value="<?= $isi->id_account_kas ?>"  <?= set_select('id_account',$isi->id_account_kas) ?> ><?= $isi->kode," | ".$isi->nm_account ?></option>
					<?php
							endforeach;
						endif;
					?>
				</select>
			</div>
		</div>
		<div class="form-group <?= form_error('ket_transaksi') ? ' has-error' : ''; ?>">
			<label for="ket_transaksi"	class="control-label col-sm-2"><?= lang('label_ket_trans') ?></label>
			<div class="col-sm-4">
				<textarea class="form-control" name="ket_transaksi" id="ket_transaksi"><?= set_value("ket_transaksi") ?></textarea>
			</div>
		</div>
		<div class="form-group <?= form_error('status') ? ' has-error' : ''; ?>">
			<label for="status"	class="control-label col-sm-2"><?= lang('label_status_alur') ?></label>
			<div class="col-sm-3">
				<select class="form-control" name="status" id="status">
					<option value="1" <?= set_select('status','1') ?> ><?= lang('label_status_masuk') ?> </option>
					<option value="0" <?= set_select('status','0') ?> ><?= lang('label_status_keluar') ?> </option>
				</select>
			</div>
		</div>
		<div class="form-group <?= form_error('isi_total') ? ' has-error' : ''; ?>">
			<label for="total"	class="control-label col-sm-2"><?= lang('label_nilai_total') ?></label>
			<div class="col-sm-4">
				<input type="text" name="total" id="total" class="form-control" value="<?= set_value('isi_total') ?>">
			</div>
			<input type="hidden" name="isi_total" id="isi_total" value="<?= set_value('isi_total') ?>">
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-4">
				<button type="submit" id="tombol_simpan_semua" class="btn btn-primary" name="simpan" id="simpan" title="<?= lang('tombol_simpan') ?>"><?= lang('tombol_simpan') ?></button>
				<?php
					echo lang('penghubung').' '. anchor('kas/kas',lang('tombol_batal'));
				?>
			</div>
		</div>

	</div>
	
</div>
