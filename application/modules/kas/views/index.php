<?php 
    $ENABLE_ADD     = has_permission('Pencatatan Kas.Add');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_daftar','name'=>'frm_daftar','class'=>'form-horizontal')) ?>
    <div class="box-header">
        <div class="box-body">
            <div class="col-sm-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label for="waktu" class="control-label col-sm-4"><?=lang('label_id_account')?></label>
                    <div class="col-sm-7">
                        <select class="form-control" name="id_account" id="id_account" style="width: 100%;">
                            <option value=""></option>
                            <?php 
                                if(isset($dt_acc) && is_array($dt_acc) && count($dt_acc)):
                                    foreach ($dt_acc as $key => $isi):
                            ?>
                                    <option value="<?= $isi->id_account_kas ?>"  <?= set_select('id_account',$isi->id_account_kas, isset($account) && $account == $isi->id_account_kas ) ?> ><?= $isi->kode," | ".$isi->nm_account ?></option>
                            <?php
                                    endforeach;
                                endif;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 5px;">
                    <label for="model_transaksi" class="control-label col-sm-4"><?=lang('label_model_transaksi')?></label>
                    <div class="col-md-7">
                        <select class="form-control" name="model_transaksi" id="model_transaksi" placeholder="Pilih Model Transaksi" style="width: 100%;">
                            <option value=""></option>
                            <option value="0" <?= set_select('model_transaksi','0') ?> ><?=lang('label_tunai') ?> </option>
                            <option value="1" <?= set_select('model_transaksi','1') ?> ><?=lang('label_via_bank') ?> </option>
                        </select>
                    </div>
                </div>
                <div class="form-group <?= form_error('id_bank') ? ' has-error' : ''; ?>">
                    <label for="id_account" class="control-label col-sm-4"><?=lang('label_bank') ?></label>
                    <div class="col-sm-7">
                        <select class="form-control" name="id_bank" id="id_bank" style="width: 100%;">
                            <option value=""></option>
                            <?php 

                                if(isset($dt_bank) && is_array($dt_bank) && count($dt_bank)):
                                    foreach ($dt_bank as $key => $bank):

                            ?>
                                <option value="<?= $bank->id_bank ?>"  <?= set_select('id_bank',$bank->id_bank) ?> ><?= $bank->nama_bank; ?></option>
                            <?php
                                    endforeach;
                                endif;
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group" style="margin-bottom: 5px;">
                    <label for="range_tanggal" class="control-label col-md-5"><?= lang('label_waktu')?></label>
                    <div class="col-md-7">
                        <div class="input-group">
                            <input type="text" name="range_tanggal" id="range_tanggal" class="form-control" readonly placeholder="<?= lang('label_place_holder_range') ?>" value="<?= isset($range) ? $range : '' ?>" >
                            <span class="input-group-addon "><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <span></span>
                <div class="form-group">
                    <label for="isi_cari" class="control-label col-md-5"><?= lang('label_ket_trans') ?></label>
                    <div class="col-md-7">
                        <input type="text" name="isi_cari" id="isi_cari" class="form-control" value="<?= isset($search) ? $search : '' ?>" autofocus placeholder="search">
                    </div>
                </div>
            </div> 
        </div>     
        <div class="box-footer">
            <div class="pull-left">
                <?php if ($ENABLE_ADD) : ?>
                    <a href="<?= site_url('kas/kas/baru') ?>" class="btn btn-success" title="<?= lang('tombol_tambah_baru') ?>"><?= lang('tombol_tambah_baru') ?></a>
                <?php endif; ?> 
            </div>
            <div class="pull-right">
                <button type="button" class="btn btn-info" name="search" id="search">Filter <i class="fa fa-search"></i></button> 
            </div>                                
           
        </div>                            
    </div>
    <?= form_close();?>  


    <?php if(isset($data) && is_array($data) && count($data)): ?>

    <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped">
            
            <thead>
                <tr>
                    <th width="50" class="text-center">#</th>
                    <th class="text-center"> <?= lang('label_model_transaksi') ?> </th>
                    <th class="text-center"> <?= lang('label_bank') ?></th>
                    <th class='text-center'> <?= lang('label_waktu') ?> </th>
                    <th class='text-center'> <?= lang('label_id_account') ?> </th>
                    <th class='text-center'> <?= lang('label_ket_trans') ?> </th>
                    <th class='text-center'> <?= lang('label_tabel_total_keluar') ?> </th>
                    <th class='text-center'> <?= lang('label_tabel_total_masuk') ?> </th>
                </tr>    
            </thead>            
            <tbody>
                <?php 
                    foreach ($data as $key => $isi):
                ?>
                    <tr>
                        <td class="text-center"><?= $numb++ ?></td>
                        <td class="text-center"><?= $isi->model_transaksi ?></td>
                        <td class="text-center"><?= ($isi->nama_bank !=""?$isi->nama_bank:"-") ?></td>
                        <td class="text-center"><?= $isi->waktu ?></td>
                        <td class="text-center"><?= $isi->nm_account ?></td>
                        <td class="text-center"><?= $isi->ket ?></td>
                        <?php if($isi->status=="1"): ?>
                            <td class="text-right">-</td>
                            <td class="text-right"><?= number_format($isi->total) ?></td>    
                        <?php else: ?> 
                            <td class="text-right text-red"><?= number_format($isi->total) ?></td>
                            <td class="text-right">-</td>
                        <?php endif; ?>    
                        
                        

                    </tr>

                <?php 
                            endforeach;
                        
                ?>

            </tbody>

        </table>

    </div>
    <div class="box-footer clearfix">
        <?= $this->pagination->create_links() ?> 
    </div>
    
    <?php else: ?>

    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('warning_data_kosong') ?></p>
    </div>
    
    <?php endif; ?>
        
</div><!-- /.box -->
