<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_account_kas','name'=>'frm_account_kas','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
		  	<div class="form-group <?= form_error('kode') ? ' has-error' : ''; ?>">
			    <label for="kode" class="col-sm-2 control-label"><?= lang('account_kas_code') ?></label>
			    <div class="col-sm-3">
			    	<input type="text" class="form-control" id="kode" name="kode" maxlength="11" value="<?php echo set_value('kode', isset($data->kode) ? $data->kode : ''); ?>" required autofocus>
			    </div>
		  	</div>
		  	<div class="form-group <?= form_error('nm_account') ? ' has-error' : ''; ?>">
			    <label for="nm_account" class="col-sm-2 control-label"><?= lang('account_kas_name') ?></label>
			    <div class="col-sm-3">
			    	<input type="text" class="form-control" id="nm_account" name="nm_account" maxlength="45" value="<?php echo set_value('nm_account', isset($data->nm_account) ? $data->nm_account : ''); ?>" required>
			    </div>
		  	</div>
		  	<div class="form-group <?= form_error('idgol') ? ' has-error' : ''; ?>">
			    <label for="idgol" class="col-sm-2 control-label"><?= lang('account_kas_gol') ?></label>
			    <div class="col-sm-3">
			    	<select class="form-control" id="idgol" name="idgol">
			    		<option></option>
			    		<?php foreach ($golongan_kas as $key => $gol_kas) : ?>
			    		<option value="<?= $gol_kas->idgol ?>" <?= set_select('idgol', $gol_kas->idgol, isset($data->idgol) && $data->idgol == $gol_kas->idgol ) ?>><?= $gol_kas->nama ?></option>
			    		<?php endforeach ?>
			    	</select>
			    </div>
		  	</div>
		  	<div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
			    <label for="ket" class="col-sm-2 control-label"><?= lang('account_kas_ket') ?></label>
			    <div class="col-sm-4">
			    	<textarea class="form-control" id="ket" name="ket" maxlength="255"><?php echo set_value('ket', isset($data->ket) ? $data->ket : ''); ?></textarea>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      	<button type="submit" name="save" class="btn btn-primary"><?= lang('account_kas_btn_save') ?></button>
			    	<?php
	                	echo lang('bf_or') . ' ' . anchor('kas/account_kas/index', lang('account_kas_btn_cancel'));
	                ?>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->
<script type="text/javascript">
	$(document).ready(function(){
		$("#idgol").select2({ placeholder : "-- Pilih --"});
	});
</script>