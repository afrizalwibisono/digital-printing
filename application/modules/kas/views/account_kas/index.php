<?php 
    $ENABLE_ADD     = has_permission('Data Master.Account Kas.Add');
    $ENABLE_MANAGE  = has_permission('Data Master.Account Kas.Manage');
    $ENABLE_DELETE  = has_permission('Data Master.Account Kas.Delete');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_account_kas','name'=>'frm_account_kas')) ?>
        <div class="box-header">
            <div class="form-group">
                <?php if ($ENABLE_ADD) : ?>
	  	        <a href="<?= site_url('kas/account_kas/create') ?>" class="btn btn-success" title="<?= lang('account_kas_btn_new') ?>"><?= lang('account_kas_btn_new') ?></a>
                <?php endif; ?>
                <div class="pull-right">
                    <div class="input-group">
                        <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="Search" autofocus />
                        <div class="input-group-btn">
                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
	<div class="box-body table-responsive no-padding">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
                        <th width="50">#</th>
                        <th><?= lang('account_kas_code') ?></th>
                        <th><?= lang('account_kas_name') ?></th>
                        <th><?= lang('account_kas_gol') ?></th>
                        <th><?= lang('account_kas_ket') ?></th>
                        <?php if($ENABLE_MANAGE) : ?>
                        <th width="25"></th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($results as $record) : ?>
                    <tr>
                        <td class="column-check">
                            <?php if($record->st_fix == 0) : ?>
                            <input type="checkbox" name="checked[]" value="<?= $record->id_account_kas ?>" />
                            <?php endif; ?>
                        </td>
                        <td><?= $numb; ?></td>
                        <td><?= $record->kode ?></td>
                        <td><?= $record->nm_account ?></td>
                        <td><?= $record->nama ?></td>
                        <td><?= $record->ket ?></td>
                        <?php if($ENABLE_MANAGE) : ?>
                        <td style="padding-right:20px">
                            <?php if($record->st_fix == 0) : ?>
                            <a class="text-black" href="<?= site_url('kas/account_kas/edit/' . $record->id_account_kas); ?>" data-toggle="tooltip" data-placement="left" title="Edit Data"><i class="fa fa-pencil"></i></a>
                            <?php endif; ?>
                        </td>
                        <?php endif; ?>
                    </tr>
                    <?php $numb++; endforeach; ?>
                </tbody>
	  </table>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">
		<?php if($ENABLE_DELETE) : ?>
		<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('account_kas_btn_delete') ?>" onclick="return confirm('<?= lang('account_kas_delete_confirm'); ?>')">
		<?php endif;
		echo $this->pagination->create_links(); 
		?>
	</div>
	<?php else: ?>
    <div class="callout callout-info">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('account_kas_no_records_found') ?></p>
    </div>
    <?php
	endif;
	form_close(); 
	?>
</div><!-- /.box -->