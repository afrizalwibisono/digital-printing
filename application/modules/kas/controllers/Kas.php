<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for alur kas
 */

class Kas extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Pencatatan Kas.View";
    protected $addPermission    = "Pencatatan Kas.Add";
    protected $managePermission = "Pencatatan Kas.Manage";
    protected $deletePermission = "Pencatatan Kas.Delete";

    public function __construct()
    {
        parent::__construct();

        $this->lang->load('kas/kas');
        $this->load->model(array(
                                    'kas/account_kas_model',
                                    'kas/kas_model',
                                    'kas/bank_model'
                                ));

        $this->template->title(lang('judul_daftar'));
		$this->template->page_icon('fa fa-money');
    }

    public function index(){
        
        $this->auth->restrict($this->viewPermission);

        $this->load->library('pagination');

        if(isset($_POST['range_tanggal'])){

            $search         = isset($_POST['isi_cari']) ? $this->input->post('isi_cari') : '';
            $account        = isset($_POST['id_account']) ? $this->input->post('id_account') : '';
            $range          = isset($_POST['range_tanggal']) ? $this->input->post('range_tanggal') : '';
            $model_transaksi= isset($_POST['model_transaksi']) ? $this->input->post('model_transaksi'):'';
            $bank           = isset($_POST['id_bank']) ? $this->input->post('id_bank'):'';
            
        }else{

            $search         = isset($_GET['isi_cari']) ? $this->input->get('isi_cari') : '';
            $account        = isset($_GET['id_account']) ? $this->input->get('id_account') : '';
            $range          = isset($_GET['range_tanggal']) ? $this->input->get('range_tanggal') : '';
            $model_transaksi= isset($_GET['model_transaksi']) ? $this->input->get('model_transaksi'):'';
            $bank           = isset($_GET['id_bank']) ? $this->input->get('model_transaksi'):'';
            
        }


         // Target url jika di klik paginationnya
        // $filter = "";

        // if($range!=""){

            $filter = "?isi_cari=".$search."&id_account=".$account."&range_tanggal=".$range."&id_bank=".$bank."&model_transaksi=".$model_transaksi;

        // }

        $search     = $this->db->escape_str($search);

        if($range != ""){
            
            $pecah_tgl  = explode("-", $range);
            $tgl1       = date_ymd(trim($pecah_tgl[0]))." 01:00:00";
            $tgl2       = date_ymd(trim($pecah_tgl[1]))." 23:59:00";

        }else{

            $sekarang   = date("Y-m-d");
            $tgl1       = $sekarang . " 01:00:00";
            $tgl2       = $sekarang . " 23:59:00";    

        }

        //Get ID Cabang
        $usr_session    = $this->session->userdata('cokes_session');
        $id_cabang      = $usr_session['id_cabang'];
        

        // ambil total data
        $where="alurkas.deleted=0 and (waktu >= '$tgl1' and waktu <= '$tgl2')";
        if($account !='')
        {
            $where .="AND alurkas.id_account_kas='{$account}'";
        }
        if ($search !='')
        {
            $where .="AND alurkas.ket like '%$search%'";
        }
        if ($model_transaksi !='')
        {
            $where .="AND alurkas.model_transaksi= $model_transaksi ";
        }
        if ($bank!='')
        {
            $where .="AND alurkas.id_bank='{$bank}'";
        }


        //total semua data
        $total      = $this->kas_model->where($where)
                                        ->count_all();


        $offset = $this->input->get('per_page');
        $limit      = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        // data yang ditampilkan
        $data       = $this->kas_model->select("DATE_FORMAT(`waktu`, '%d/%m/%Y %h:%i') AS waktu,
                                                    `status`,
                                                    alurkas.`ket`,
                                                    `total`,
                                                    `nm_account`,
                                                    if(`model_transaksi`=0,'Tunai','Via Bank') as model_transaksi,
                                                    `bank.nama_bank` as nama_bank")
                                        ->join('account_kas','account_kas.id_account_kas = alurkas.id_account_kas','join')
                                        ->join('bank','bank.id_bank=alurkas.id_bank','left')
                                        ->where($where)
                                        ->order_by('waktu')
                                        ->limit($limit,$offset)
                                        ->find_all();
        $assets = array(
                    'plugins/datepicker/bootstrap-datepicker3.css',
                    'plugins/daterangepicker/daterangepicker.css',
                    'plugins/select2/dist/css/select2.min.css',
                    'plugins/datepicker/bootstrap-datepicker.min.js',
                    'plugins/select2/dist/js/select2.min.js',
                    'plugins/daterangepicker/moment.js',
                    'plugins/daterangepicker/moment.min.js',
                    'plugins/daterangepicker/daterangepicker.js',
                    'plugins/number/jquery.number.js',
                    'kas/assets/js/kas.js',
                    'plugins/toastr/toastr.js',
                    'plugins/toastr/toastr.css'
                    );

        add_assets($assets);


        $dt_acc = $this->account_kas_model->select('id_account_kas, kode, nm_account')
                                            ->where('deleted',0)
                                            ->order_by('CAST(kode as SIGNED INTEGER)', 'asc')
                                            ->find_all();
        $dt_bank= $this->bank_model->where('deleted',0)->order_by('nama_bank','asc')->find_all();
        
        $this->template->set("search", $search);
        $this->template->set("account", $account);
        $this->template->set("range", $range);
        $this->template->set('bank', $bank);
        $this->template->set('model_trans', $model_transaksi);
        $this->template->set('data',$data);
        $this->template->set('dt_acc', $dt_acc);
        $this->template->set('dt_bank',$dt_bank);
        $this->template->set("numb", $offset+1);
        $this->template->title(lang('judul_daftar'));
        $this->template->render('index');

    }

    function baru(){

        if(isset($_POST['simpan']) && $this->auth->restrict($this->addPermission) ){

            if($this->simpan()){

                $this->template->set_message(lang('label_simpan_sukses'),'success');
                redirect('kas/kas');

            }

        }

        $dt_acc = $this->account_kas_model->select('id_account_kas, kode, nm_account')
                                            ->where('deleted',0)
                                            ->order_by('CAST(kode as SIGNED INTEGER)', 'asc')
                                            ->find_all();
        $dt_bank= $this->bank_model->where('deleted',0)->order_by('nama_bank','asc')->find_all();

         $assets = array(
                    'plugins/select2/dist/css/select2.min.css',
                    'plugins/datetimepicker/bootstrap-datetimepicker.css',
                    'plugins/datetimepicker/bootstrap-datetimepicker.js',
                    'plugins/select2/dist/js/select2.min.js',
                    'plugins/number/jquery.number.js',
                    'kas/assets/js/create.js',
                    'plugins/toastr/toastr.js',
                    'plugins/toastr/toastr.css'
                    );

        add_assets($assets);
        $this->template->set('dt_acc', $dt_acc);
        $this->template->set('dt_bank',$dt_bank);
        $this->template->title(lang('judul_baru'));
        $this->template->render('kas_form');

    }

    function pilihan_awal($val){

        return $val == "" ? false : true;

    }

    function simpan(){

        $this->form_validation->set_rules('waktu_trans','lang:label_waktu','required|trim');
        $this->form_validation->set_rules('id_account','lang:label_id_account','callback_pilihan_awal');
        $this->form_validation->set_rules('ket_transaksi','lang:label_ket_trans','required|trim');
        $this->form_validation->set_rules('status','lang:label_status_alur','callback_pilihan_awal');
        $this->form_validation->set_rules('isi_total','lang:label_nilai_total','required|numeric');
        $this->form_validation->set_message("pilihan_awal", "You should choose the {field} field");
        if ($this->input->post('model_transaksi')==1) {
            $this->form_validation->set_rules('id_bank','lang:label_bank','required|trim');
            $id_bank=$this->input->post('id_bank');
        }else{
            $id_bank=null;
        }

        if($this->form_validation->run() === false){

            $this->template->set_message(validation_errors(),'error');
            return false;

        }
        $tgl_trans          = $this->input->post("waktu_trans");
        $id_account         = $this->input->post("id_account");
        $ket                = $this->input->post("ket_transaksi");
        $status             = $this->input->post("status");
        $isi_total          = $this->input->post("isi_total");
        $model_transaksi    = $this->input->post('model_transaksi');

        $status     = $this->kas_model->simpan_alurkas_manual($id_account, $ket, $isi_total, $status, $this->addPermission, $tgl_trans, $model_transaksi, $id_bank);

        if(!$status){

            $this->template->set_message(lang('label_simpan_gagal'),'error' );

        }


        return $status;

    }


}

?>