<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for Account kas
 */

class Account_kas extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Account Kas.View";
    protected $addPermission    = "Account Kas.Add";
    protected $managePermission = "Account Kas.Manage";
    protected $deletePermission = "Account Kas.Delete";

    protected $prefixKey        = "AK/";

    public function __construct()
    {
        parent::__construct();

        $this->lang->load('kas/account_kas');
        $this->load->model(array('kas/account_kas_model','kas/gol_account_kas_model'));

        $this->template->title(lang('account_kas_title_manage'));
		$this->template->page_icon('fa fa-money');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if (isset($_POST['delete']) && has_permission($this->deletePermission))
        {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                $qty_del = 0;
                foreach ($checked as $pid)
                {
                    $data_kas       = $this->account_kas_model->find($pid);
                    $nm_account_kas = $data_kas->nm_account;

                    if($data_kas->st_fix == 0)
                    {
                        $qty_del++;
                    }

                    $result  = $this->account_kas_model->delete_where(array('id_account_kas' => $pid, 'st_fix' => 0));

                    if($result)
                    {
                        $keterangan = "SUKSES, hapus data account kas ".$nm_account_kas.", dengan ID : ".$pid;
                        $status     = 1;
                    }
                    else
                    {
                        $keterangan = "GAGAL, hapus data account kas ".$nm_account_kas.", dengan ID : ".$pid;
                        $status     = 0;
                    }

                    $nm_hak_akses   = $this->deletePermission; 
                    $kode_universal = $pid;
                    $jumlah         = 1;
                    $sql            = $this->db->last_query();

                    simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                }

                if ($result)
                {
                    $this->template->set_message($qty_del.' '. lang('account_kas_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('account_kas_del_failure') . $this->account_kas_model->error, 'error');
                }
            }
            else
            {
                $this->template->set_message(lang('account_kas_del_error') . $this->account_kas_model->error, 'error');
            }
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
        }

        $filter = "";
        if($search!="")
        {
            $filter = "?search=".$search;
        }

        $search2 = $this->db->escape_str($search);
        
        $where = "`account_kas.deleted` = 0
                AND (`kode` LIKE '%$search2%' ESCAPE '!'
                OR `account_kas.nm_account` LIKE '%$search2%' ESCAPE '!'
                OR `account_kas.ket` LIKE '%$search2%' ESCAPE '!')";
        
        $total = $this->account_kas_model
                    ->join("gol_account_kas","account_kas.idgol = gol_account_kas.idgol","left")
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->account_kas_model
                    ->select("*, account_kas.ket as ket")
                    ->join("gol_account_kas","account_kas.idgol = gol_account_kas.idgol","left")
                    ->where($where)
                    ->order_by('nm_account','ASC')
                    ->limit($limit, $offset)->find_all();

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->title(lang('account_kas_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('kas/account_kas/index'); 
    }

   	//Create New Account Kas
   	public function create()
   	{
        $this->auth->restrict($this->addPermission);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_account_kas())
            {
              $this->template->set_message(lang("account_kas_create_success"), 'success');
              redirect('kas/account_kas');
            }
        }

        $golongan_kas = $this->gol_account_kas_model->order_by("nama","ASC")
                                ->find_all_by(array('deleted' => 0));

        $assets = array(
                    'plugins/select2/dist/css/select2.css',
                    'plugins/select2/dist/js/select2.js',
                    );

        add_assets($assets);

        $this->template->set("golongan_kas", $golongan_kas);

        $this->template->title(lang('account_kas_title_new'));
		$this->template->render('account_kas/account_kas_form');
   	}

   	//Edit Kas
   	public function edit($id = NULL)
   	{
  		$this->auth->restrict($this->managePermission);
                
        if (empty($id))
        {
            $this->template->set_message(lang("account_kas_invalid_id"), 'error');
            redirect('account_kas');
        }

        if (isset($_POST['save']))
        {
            if ($this->save_account_kas('update', $id))
            {
                $this->template->set_message(lang("account_kas_edit_success"), 'success');
            }
            else
            {
                $this->template->set_message(lang("account_kas_edit_failed"), 'error');
            }
        }
        
        $data  = $this->account_kas_model->find_by(array('id_account_kas' => $id, 'account_kas.deleted' => 0));

        if(!$data)
        {
            $this->template->set_message(lang("account_kas_invalid_id"), 'error');
            redirect('account_kas');
        }

        $golongan_kas = $this->gol_account_kas_model->order_by("nama","ASC")
                                ->find_all_by(array('deleted' => 0));

        $this->template->set("golongan_kas", $golongan_kas);

        $this->template->set('data', $data);
        $this->template->title(lang("account_kas_title_edit"));
        $this->template->render('account_kas/account_kas_form');
   	}

   	protected function save_account_kas($type='insert', $id=0)
   	{
        if($type == 'insert')
        {
            $extra_rule_kode = "|unique[account_kas.kode]";
            $extra_rule_nama = "|unique[account_kas.nm_account]";
        }
        else
        {
            $_POST['id_account_kas'] = $id;

            $extra_rule_kode = "|unique[account_kas.kode, account_kas.id_account_kas]";
            $extra_rule_nama = "|unique[account_kas.nm_account, account_kas.id_account_kas]";
        }

        $this->form_validation->set_rules('kode','lang:account_kas_code','required|trim|max_length[45]'.$extra_rule_kode);
        $this->form_validation->set_rules('nm_account','lang:account_kas_name','required|trim|max_length[45]'.$extra_rule_nama);
        $this->form_validation->set_rules('idgol','lang:account_kas_gol','callback_default_select');
        $this->form_validation->set_rules('ket','lang:account_kas_ket','trim|trim|max_length[255]');

        $this->form_validation->set_message('default_select', 'You should choose the "%s" field');

        if ($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        unset($_POST['submit'], $_POST['save']);

        if ($type == 'insert')
        {
            $_POST['id_account_kas'] = gen_primary('AK','account_kas','id_account_kas');
            
            $id = $this->account_kas_model->insert($_POST);

            if ($id !== FALSE)
            {
                $keterangan = "SUKSES, tambah data account kas ".$this->input->post('nm_account').", dengan ID : ".$_POST['id_account_kas'];
                $status     = 1;
                
                $return = TRUE;
            }
            else
            {
                $keterangan = "GAGAL, tambah data account kas ".$this->input->post('nm_account').", dengan ID : ".$_POST['id_account_kas'];
                $status     = 0;
                
                $return = FALSE;
            }

            //Save Log
            $nm_hak_akses   = $this->addPermission; 
            $kode_universal = $_POST['id_account_kas'];
            $jumlah         = 1;
            $sql            = $this->db->last_query();

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        }
        else if ($type == 'update')
        {
            $cek_fix = $this->account_kas_model->find($id);
            if($cek_fix->st_fix == 1) //Protect editing fix account kas
            {
                return FALSE;
            }

            $return = $this->account_kas_model->update($id, $_POST);

            if($return)
            {
                $keterangan = "SUKSES, ubah data account kas ".$this->input->post('nm_account').", dengan ID : ".$id;
                $status     = 1;
            }
            else
            {
                $keterangan = "GAGAL, ubah data account kas ".$this->input->post('nm_account').", dengan ID : ".$id;
                $status     = 0;
            }

            //Save Log
            $nm_hak_akses   = $this->managePermission; 
            $kode_universal = $id;
            $jumlah         = 1;
            $sql            = $this->db->last_query();

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
        }

        return $return;
   	}

    public function default_select($val)
    {
        return $val == "" ? FALSE : TRUE;
    }

}
?>