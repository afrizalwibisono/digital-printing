$(document).ready(function(){
        $("#id_account").select2({ placeholder : '--Pilih Account Kas--', allowClear: true });
        $("#id_bank").select2({ placeholder:'--Pilih Bank--', allowClear: true, width:'100%' });
        $("#model_transaksi").select2({ placeholder:'--Pilih Model Transaksi--', allowClear: true });
        $("#status").select2({ placeholder:'--Pilih Status--', allowClear: true });

        $('#waktu_trans').datetimepicker({
            todayBtn: "linked",
            autoclose: true,
            clearBtn: true,
            todayHighlight: true,
        });
        $("#total").number(true,0);

        $("#total").keyup(function(){
            $("#isi_total").val($(this).val());
        });
        $("#model_transaksi").change(function(){
            var cek= $("#model_transaksi").val();
            if (cek==0) {
                $(".trans_model").hide();
            }else{
                $(".trans_model").show();
            }
        });

	});