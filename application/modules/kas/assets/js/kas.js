$(document).ready(function(){
        $("#id_account").select2({ placeholder : '--Pilih Account Kas--', allowClear: true });
        $("#id_bank").select2({ placeholder:'--Pilih Bank--', allowClear: true, width:'100%' });
        $("#model_transaksi").select2({ placeholder:'--Pilih Model Transaksi--', allowClear: true });

		$('#range_tanggal').daterangepicker({
        	autoUpdateInput: false,
          	locale: {
                format : 'DD/MM/YYYY',
                cancelLabel: 'Clear'
          	},
          	opens : 'left',
            startDate : new Date(),
            endDate : new Date()
      	});

	    $('#range_tanggal').on('apply.daterangepicker', function(ev, picker) {
	        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
	    });

	    $('#range_tanggal').on('cancel.daterangepicker', function(ev, picker) {
	        $(this).val('');
        });

        $("#search").click(function(){

            if($("#range_tanggal").val().length <= 0){

                toastr.clear();
                Command: toastr['error']('Range Tanggal, tidak boleh kosong', 'Error !');

            }else{

                $("#frm_daftar").submit();

            }

        });
        $(".panel-filter").on("click",function(){
            panel_filter($(this).parents("#box_filter"));
            $(this).parents(".dropdown").removeClass("open");
            return false;
        });

        $("#id_account").select2({placeholder : '------ Pilih Salah Satu Account ------'});
        $("#status").select2({placeholder: 'Pilih Status Alur Kas'});
        $("#total").number(true,0);

        $("#total").keyup(function(){
            $("#isi_total").val($(this).val());
        });
        $("#model_transaksi").change(function(){
            var cek= $("#model_transaksi").val();
            if (cek==0) {
                $(".trans_model").hide();
            }else{
                $(".trans_model").show();
            }
        });

	});
    function panel_filter(panel,action,callback){

        if(panel.hasClass("panel-toggled")){        
            panel.removeClass("panel-toggled");
            
            panel.find(".panel-filter .fa-angle-up").removeClass("fa-angle-up").addClass("fa-angle-down");

            if(action && action === "shown" && typeof callback === "function")
                callback();            

            onload();
                    
        }else{
            panel.addClass("panel-toggled");
                    
            panel.find(".panel-filter .fa-angle-down").removeClass("fa-angle-down").addClass("fa-angle-up");

            if(action && action === "hidden" && typeof callback === "function")
                callback();

            onload();        
            
        }
    }   