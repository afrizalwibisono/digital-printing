<?php 
    $ENABLE_ADD     = has_permission('Konsumen.Add');
    $ENABLE_MANAGE  = has_permission('Konsumen.Manage');
    $ENABLE_DELETE  = has_permission('Konsumen.Delete');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_konsumen','name'=>'frm_konsumen')) ?>
        <div class="box-header">
            <?php if ($ENABLE_ADD) : ?>
            <a href="<?= site_url('konsumen/create') ?>" class="btn btn-success" title="<?= lang('konsumen_btn_new') ?>"><?= lang('konsumen_btn_new') ?></a>
            <?php endif; ?>
            <!-- <div class="form-group"> -->
                <div class="pull-right form-inline">

                    <div class="form-group">
                        <select id="st_tmp" name="st_tmp" class="form-control" style="min-width: 200px;">
                            <option></option>
                            <option value="0"  <?= set_select("st_tmp", 0, isset($data->st_tmp) && $data->st_tmp == 0) ?> ><?= lang('konsumen_permanent') ?></option>
                            <option value="1"  <?= set_select("st_tmp", 1, isset($data->st_tmp) && $data->st_tmp == 0) ?> ><?= lang('konsumen_temporary') ?></option>
                        </select>
                    </div>

                    <div class="form-group">
                        <select id="st" name="st" class="form-control" style="min-width: 200px;">
                            <option></option>
                            <?php 
                                if(is_array($gol_konsumen) && count($gol_konsumen) > 0):
                                    foreach ($gol_konsumen as $key => $gk):
                                
                            ?>
                                <option value=<?= $gk->id_gol ?>  <?= set_select("st", $gk->id_gol, isset($data->st) && $data->st == $gk->id_gol) ?> ><?= $gk->nm_gol ?></option>
                            <?php 
                                    endforeach;
                                endif;
                            ?>
                        </select>
                    </div>
                    
                    <div class="input-group">
                        <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="Search" autofocus />
                        <div class="input-group-btn">
                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            <!-- </div> -->
        </div>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
	<div class="box-body table-responsive no-padding">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
                        <th width="50">#</th>
                        <th><?= lang('konsumen_nama') ?><span class="su"></span> </th>
                        <th class="text-center"><?= lang('konsumen_st_tmp') ?></th>
                        <th class="text-center"><?= lang('konsumen_st') ?></th>
                        <th><?= lang('konsumen_kota') ?></th>
                        <th><?=lang('konsumen_tlp')?></th>
                        <th><?=lang('konsumen_wa')?></th>
                        <th><?=lang('konsumen_email')?></th>
                        <?php if($ENABLE_MANAGE) : ?>
                        <th width="25"></th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($results as $record) : ?>
                    <tr>
                        <td class="column-check"><input type="checkbox" name="checked[]" value="<?= $record->idkonsumen ?>" /></td>
                        <td><?= $numb; ?></td>
                        <td><?= $record->panggilan." ".$record->nama  ?></td>
                        <td><?= $record->st_tmp == 0 ? lang('konsumen_permanent') : lang('konsumen_temporary') ?></td>
                        <td class="text-center"><?= $record->status  ?></td>
                        <td><?= $record->kota  ?></td>
                        <td><?= $record->telp ?></td>
                        <td><?= $record->wa ?></td>
                        <td><?= $record->email ?></td>
                        <?php if($ENABLE_MANAGE) : ?>
                        <td style="padding-right:20px"><a class="text-black" href="<?= site_url('konsumen/edit/' . $record->idkonsumen); ?>" data-toggle="tooltip" data-placement="left" title="Edit Data"><i class="fa fa-pencil"></i></a></td>
                        <?php endif; ?>
                    </tr>
                    <?php $numb++; endforeach; ?>
                </tbody>
	  </table>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">
		<?php if($ENABLE_DELETE) : ?>
		<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('konsumen_btn_delete') ?>" onclick="return confirm('<?= lang('konsumen_delete_confirm'); ?>')">
		<?php endif;
		echo $this->pagination->create_links(); 
		?>
	</div>
	<?php else: ?>
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('konsumen_no_records_found') ?></p>
    </div>
    <?php
	endif;
	echo form_close(); 
	?>
</div><!-- /.box -->