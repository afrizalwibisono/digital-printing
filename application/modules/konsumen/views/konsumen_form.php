</style>

<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_customer','name'=>'frm_customer','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="form-group <?= form_error('panggilan') ? ' has-error' : ''; ?>">
				<label for="nama" class="control-label col-sm-2"><?= lang('konsumen_nama') ?></label>
				<div class="col-sm-2" style="margin-right: 0px;padding-right: 5px;">
					<select class="form-control" name="panggilan" id="panggilan">
						<option></option>
						<option value="Bpk." <?= set_select('panggilan','Bpk.' , isset($data->panggilan) && $data->panggilan == 'Bpk.' ) ?>>Bpk.</option>
						<option value="Bapak."  <?= set_select('panggilan','Bapak.',isset($data->panggilan) && $data->panggilan == 'Bapak.') ?> >Bapak.</option>
						<option value="Ib."  <?= set_select('panggilan','Ib.',isset($data->panggilan) && $data->panggilan == 'Ib.') ?> >Ib.</option>
						<option value="Ibu."  <?= set_select('panggilan','Ibu.',isset($data->panggilan) && $data->panggilan == 'Ibu.') ?> >Ibu.</option>
						<option value="Sdr."  <?= set_select('panggilan','Sdr.',isset($data->panggilan) && $data->panggilan == 'Sdr.') ?> >Sdr.</option>
						<option value="Saudara."  <?= set_select('panggilan','Saudara.',isset($data->panggilan) && $data->panggilan == 'Saudara.') ?> >Saudara.</option>
						<option value="Mr."  <?= set_select('panggilan','Mr.',isset($data->panggilan) && $data->panggilan == 'Mr.') ?> >Mr.</option>
						<option value="Mrs."  <?= set_select('panggilan','Mrs.',isset($data->panggilan) && $data->panggilan == 'Mrs.') ?> >Mrs.</option>
						<option value="PT."  <?= set_select('panggilan','PT.',isset($data->panggilan) && $data->panggilan == 'PT.') ?> >PT.</option>
						<option value="CV."  <?= set_select('panggilan','CV.',isset($data->panggilan) && $data->panggilan == 'CV.') ?> >CV.</option>
					</select>
				</div>
				<div class="col-sm-3" style="margin-left: 0;padding-left: 0">
					<input type="text" class="form-control" id="nama" name="nama" maxlength="225" value="<?php echo set_value('nama', isset($data->nama) ? $data->nama : ''); ?>" placeholder="Nama Konsumen">
				</div>
			</div>

			<div class="form-group <?= form_error('st_tmp') ? ' has-error' : ''; ?>">
			    <label for="st" class="col-sm-2 control-label"><?= lang('konsumen_st_tmp') ?></label>
			    <div class="col-sm-3">
					<select id="st_tmp" name="st_tmp" class="form-control" style="min-width: 200px;">
						<option></option>
						<option value="0"  <?= set_select("st_tmp", 0, isset($data->st_tmp) && $data->st_tmp == 0) ?> ><?= lang('konsumen_permanent') ?></option>
						<option value="1"  <?= set_select("st_tmp", 1, isset($data->st_tmp) && $data->st_tmp == 1) ?> ><?= lang('konsumen_temporary') ?></option>
					</select>
			    </div>
		  	</div>

		  	<div class="form-group <?= form_error('st') ? ' has-error' : ''; ?>">
			    <label for="st" class="col-sm-2 control-label"><?= lang('konsumen_st') ?></label>
			    <div class="col-sm-3">
			    	<select class="form-control" name="st" id="st">
						<option></option>
						<?php 
							if(is_array($gol_konsumen) && count($gol_konsumen) > 0):
								foreach ($gol_konsumen as $key => $gk):
							
						?>
								<option value=<?= $gk->id_gol ?>  <?= set_select("st", $gk->id_gol, isset($data->st) && $data->st == $gk->id_gol) ?> ><?= $gk->nm_gol ?></option>
						<?php 
								endforeach;
							endif;
						?>
					</select>
			    </div>
		  	</div>
		  	<div class="form-group <?= form_error('alamat') ? ' has-error' : ''; ?>">
			    <label for="alamat" class="col-sm-2 control-label"><?= lang('konsumen_alamat') ?></label>
			    <div class="col-sm-5">
			    	<textarea class="form-control" id="alamat" name="alamat"><?php echo set_value('alamat', isset($data->alamat) ? $data->alamat : ''); ?></textarea>
			    </div>
		  	</div>
		  	<div class="form-group <?= form_error('kota') ? ' has-error' : ''; ?>">
			    <label for="kota" class="col-sm-2 control-label"><?= lang('konsumen_kota') ?></label>
			    <div class="col-sm-3">
			    	<input type="text" class="form-control" id="kota" name="kota" maxlength="45" value="<?php echo set_value('kota', isset($data->kota) ? $data->kota : ''); ?>">
			    </div>
		  	</div>
		  	<div class="form-group <?= form_error('telp') ? ' has-error' : ''; ?>">
			    <label for="telp" class="col-sm-2 control-label"><?= lang('konsumen_tlp') ?></label>
			    <div class="col-sm-3">
			    	<input type="text" class="form-control" id="telp" name="telp" maxlength="45" value="<?php echo set_value('telp', isset($data->telp) ? $data->telp : ''); ?>">
			    </div>
		  	</div>
		  	<div class="form-group <?= form_error('wa') ? ' has-error' : ''; ?>">
			    <label for="wa" class="col-sm-2 control-label"><?= lang('konsumen_wa') ?></label>
			    <div class="col-sm-3">
			    	<input type="text" class="form-control" id="wa" name="wa" maxlength="45" value="<?php echo set_value('wa', isset($data->wa) ? $data->wa : ''); ?>">
			    </div>
		  	</div>
		  	<div class="form-group <?= form_error('email') ? ' has-error' : ''; ?>">
			    <label for="email" class="col-sm-2 control-label"><?= lang('konsumen_email') ?></label>
			    <div class="col-sm-5">
			    	<input type="email" class="form-control" id="email" name="email" maxlength="225" value="<?php echo set_value('email', isset($data->email) ? $data->email : ''); ?>">
			    </div>
		  	</div>
		  	<div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
			    <label for="ket" class="col-sm-2 control-label"><?= lang('konsumen_ket') ?></label>
			    <div class="col-sm-5">
			    	<textarea class="form-control" id="ket" name="ket"><?php echo set_value('ket', isset($data->ket) ? $data->ket : ''); ?></textarea>
			    </div>
		  	</div>

		  	<div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      	<button type="submit" name="save" class="btn btn-primary"><?= lang('konsumen_btn_save') ?></button>
			    	<?php
	                	echo lang('konsumen_or') . ' ' . anchor('konsumen', lang('konsumen_btn_cancel'));
	                ?>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->