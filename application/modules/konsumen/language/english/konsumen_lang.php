<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['konsumen_title_manage'] 	= 'Manage Konsumen';
$lang['konsumen_title_new'] 	= 'Konsumen Baru';
$lang['konsumen_title_edit'] 	= 'Edit Data konsumen';

// form/table
$lang['konsumen_nama'] 		= 'Nama Konsumen';
$lang['konsumen_panggilan'] = 'Panggialan';
$lang['konsumen_st']		= 'Kategori';
$lang['konsumen_st_tmp']    = 'Status';
$lang['konsumen_deposit'] 	= 'Deposit';
$lang['konsumen_alamat']	= 'Alamat';
$lang['konsumen_kota']		= 'Kota';
$lang['konsumen_tlp']		= 'Telepon';
$lang['konsumen_wa']		= 'No. Whatshapp';
$lang['konsumen_email']		= 'Email';
$lang['konsumen_id_user']	= ' ID User';
$lang['konsumen_ket']		= 'Keterangan';

$lang['konsumen_permanent']     = 'Tetap';
$lang['konsumen_temporary']     = 'Sementara';



//status

$lang['konsumen_st_0']	= 'Konsumen Biasa';
$lang['konsumen_st_1']	= 'Reseller';
$lang['konsumen_st_2']	= 'Instansi/Perusahaan';


// button
$lang['konsumen_btn_new'] 		= 'Baru';
$lang['konsumen_btn_delete'] 	= 'Hapus';
$lang['konsumen_btn_save'] 		= 'Simpan';
$lang['konsumen_btn_cancel'] 	= 'Batal';
$lang['konsumen_or']			= ' Atau ';

// messages
$lang['konsumen_del_error']			= 'Anda belum memilih data konsumen yang akan dihapus.';
$lang['konsumen_del_failure']		= 'Tidak dapat menghapus konsumen: ';
$lang['konsumen_delete_confirm']	= 'Apakah anda yakin akan menghapus konsumen terpilih ?';
$lang['konsumen_deleted']			= 'gudang berhasil dihapus';
$lang['konsumen_no_records_found'] 	= 'Data tidak ditemukan.';

$lang['konsumen_create_failure'] 	= 'konsumen baru gagal disimpan: ';
$lang['konsumen_create_success'] 	= 'konsumen baru berhasil disimpan';

$lang['konsumen_edit_success'] 		= 'konsumen berhasil disimpan';
$lang['konsumen_invalid_id'] 		= 'ID Tidak Valid';