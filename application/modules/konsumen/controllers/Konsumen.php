<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for konsumen
 */

class Konsumen extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Konsumen.View";
    protected $addPermission    = "Konsumen.Add";
    protected $managePermission = "Konsumen.Manage";
    protected $deletePermission = "Konsumen.Delete";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('konsumen/konsumen');
        $this->load->model(['konsumen_model','setting_harga/kategori_harga_model']);

        $this->template->title(lang('konsumen_title_manage'));
		$this->template->page_icon('fa fa-address-card-o');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if (isset($_POST['delete']) && has_permission($this->deletePermission))
        {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                foreach ($checked as $pid)
                {
                    $nama   = $this->konsumen_model->find($pid)->nama;
                    $result     = $this->konsumen_model->delete($pid);

                    if($result)
                    {
                        $keterangan = "SUKSES, hapus data konsumen ".$nama.", dengan ID : ".$pid;
                        $status     = 1;
                    }
                    else
                    {
                        $keterangan = "GAGAL, hapus data konsumen ".$nama.", dengan ID : ".$pid;
                        $status     = 0;
                    }

                    $nm_hak_akses   = $this->deletePermission; 
                    $kode_universal = $pid;
                    $jumlah         = 1;
                    $sql            = $this->db->last_query();

                    simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                }

                if ($result)
                {
                    $this->template->set_message(count($checked) .' '. lang('konsumen_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('konsumen_del_failure') . $this->konsumen_model->error, 'error');
                }
            }
            else
            {
                $this->template->set_message(lang('konsumen_del_error') . $this->konsumen_model->error, 'error');
            }
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
            $st = isset($_POST['st']) ? $this->input->post('st') :'';
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
            $st = isset($_GET['st']) ? $this->input->get('st') :'';
        }

        $filter = "?search=".$search;
        $addWhere = "";
        

        if($st){
            $filter .= "&st=".$st;
            $addWhere .= " AND konsumen.st='".$st."' ";
        }

        $search2 = $this->db->escape_str($search);
        
        $where = "`konsumen.deleted` = 0
                $addWhere
                AND (`nama` LIKE '%$search2%' ESCAPE '!'
                OR `panggilan` LIKE '%$search2%' ESCAPE '!'
                OR `kota` LIKE '%$search2%' ESCAPE '!'
                OR `alamat` LIKE '%$search2%' ESCAPE '!')";
        
        $total = $this->konsumen_model
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->konsumen_model->select('konsumen.*, gol_harga.nm_gol as status')
                    ->join('gol_harga','gol_harga.id_gol = konsumen.st', "left")
                    ->where($where)
                    ->order_by('nama','ASC')
                    ->limit($limit, $offset)->find_all();
        
        $gol_konsumen = $this->kategori_harga_model->find_all_by(['deleted' => 0,'st_go_konsumen' => 1 ]);

        $assets = array(
            'plugins/select2/dist/css/select2.min.css',
            'plugins/select2/dist/js/select2.min.js',
            'konsumen/assets/js/index_konsumen.js',
            );

        add_assets($assets);

        $this->template->set('gol_konsumen', $gol_konsumen);
        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set("toolbar_title", lang('konsumen_title_manage'));
        $this->template->set("page_title", lang('konsumen_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

   	//Create New konsumen
   	public function create()
   	{

        $this->auth->restrict($this->addPermission);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_konsumen())
            {
              $this->template->set_message(lang("konsumen_create_success"), 'success');
              redirect('konsumen');
            }
        }
        $assets = array(
                'plugins/select2/dist/css/select2.min.css',
                'plugins/select2/dist/js/select2.min.js',
                'plugins/number/jquery.number.js',
                'konsumen/assets/js/konsumen.js',
                );

        add_assets($assets);

        $gol_konsumen = $this->kategori_harga_model->find_all_by(['deleted' => 0,'st_go_konsumen' => 1 ]);

        $this->template->set('gol_konsumen', $gol_konsumen);
        $this->template->set("page_title", lang('konsumen_title_new'));
   	    $this->template->render('konsumen_form');
   	}

   	//Edit Supplier
   	public function edit()
   	{
   		
  		$this->auth->restrict($this->managePermission);
                
        $id = $this->uri->segment(3);

        if (empty($id))
        {
            $this->template->set_message(lang("konsumen_invalid_id"), 'error');
            redirect('konsumen');
        }

        if (isset($_POST['save']))
        {
            if ($this->save_konsumen('update', $id))
            {
                $this->template->set_message(lang("konsumen_edit_success"), 'success');
            }

        }

        
        $data  = $this->konsumen_model->find_by(array('idkonsumen' => $id, 'deleted' => 0));

        if(!$data)
        {
            $this->template->set_message(lang("konsumen_invalid_id"), 'error');
            redirect('konsumen');
        }
        $assets = array(
                'plugins/select2/dist/css/select2.min.css',
                'plugins/select2/dist/js/select2.min.js',
                'plugins/number/jquery.number.js',
                'konsumen/assets/js/konsumen.js',
                );
        add_assets($assets);

        $gol_konsumen = $this->kategori_harga_model->find_all_by(['deleted' => 0,'st_go_konsumen' => 1 ]);
        
        $this->template->set('gol_konsumen', $gol_konsumen);
        $this->template->set('data', $data);
        $this->template->set('toolbar_title', lang("konsumen_title_edit"));
        $this->template->set('page_title', lang("konsumen_title_edit"));
        $this->template->render('konsumen_form');
        
   	}

   	protected function save_konsumen($type='insert', $id=0)
   	{
        $this->form_validation->set_rules('panggilan', 'lang:konsumen_panggilan','callback_default_select');
        $this->form_validation->set_rules('st','lang: konsumen_st','callback_default_select');
        $this->form_validation->set_rules('nama','lang:konsumen_name','required|trim|max_length[100]');
        $this->form_validation->set_rules('alamat','lang:konsumen_alamat','required|max_length[225]|trim');
        $this->form_validation->set_rules('kota','lang:konsumen_kota','required|max_length[45]|trim');
        $this->form_validation->set_rules('telp','lang:konsumen_tlp','max_length[45]|trim');
        $this->form_validation->set_rules('wa','lang:konsumen_wa','max_length[45]|trim');
        $this->form_validation->set_rules('email','lang:konsumen_email','max_length[225]|valid_email');

        $this->form_validation->set_message('default_select', 'Anda Harus memilih field "%s"');

        if ($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }


        unset($_POST['submit'], $_POST['save']);

        if ($type == 'insert')
        {
            $_POST['idkonsumen'] = gen_primary("KONS","konsumen","idkonsumen");
            $id = $this->konsumen_model->insert($_POST);

            if (is_numeric($id))
            {
                //Save Log
                if($id)
                {
                    $keterangan = "SUKSES, tambah data konsumen ".$this->input->post('nama').", dengan ID : ".$id;
                    $status     = 1;
                }
                else
                {
                    $keterangan = "GAGAL, tambah data konsumen ".$this->input->post('nama').", dengan ID : ".$id;
                    $status     = 0;
                }

                $nm_hak_akses   = $this->addPermission; 
                $kode_universal = $id;
                $jumlah         = 1;
                $sql            = $this->db->last_query();
                //menyimpan ke Log histori
                simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

                $return = TRUE;
            }
            else
            {
                $return = FALSE;
            }
        }
        else if ($type == 'update')
        {
            $return = $this->konsumen_model->update($id, $_POST);

            if($return)
            {
                $keterangan = "SUKSES, ubah data konsumen ".$this->input->post('nama').", dengan ID : ".$id;
                $status     = 1;
            }
            else
            {
                $keterangan = "GAGAL, ubah data konsumen ".$this->input->post('nama').", dengan ID : ".$id;
                $status     = 0;
            }

            //Save Log
            $nm_hak_akses   = $this->managePermission; 
            $kode_universal = $id;
            $jumlah         = 1;
            $sql            = $this->db->last_query();
            //menyimpan ke log historis
            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
        }

        return $return;
   	}
    public function default_select($val)
    {
        return $val == "" ? FALSE : TRUE;
    }
}
?>