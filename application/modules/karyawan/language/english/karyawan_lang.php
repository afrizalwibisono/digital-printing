<?php defined('BASEPATH')|| exit('No direct script access allowed');


// ============================
$lang['karyawan_title']				= 'Data Karyawan';
$lang['karyawan_title_new']			= 'Data Karyawan Baru';
$lang['karyawan_title_edit']		= 'Edit Data Karyawan';


// ============================
$lang['btn_create']					= 'Baru';
$lang['btn_delete']					= 'Hapus';

// ============================
$lang['capt-posisi']				= 'Posisi / Jabatan';
$lang['capt-nama']					= 'Nama';
$lang['capt-alm']					= 'Alamat';
$lang['capt-kota']					= 'Kota';
$lang['capt-telp']					= 'Telp';
$lang['capt-wa']					= 'No WA';
$lang['capt-email']					= 'Email';
$lang['capt-ktp']					= 'Id KTP';
$lang['capt-ket']					= 'Keterangan';
$lang['capt-pil-user']				= 'Buat User LogIn';
$lang['capt-username']				= 'User Name LogIn';
$lang['capt-email-user']			= 'Email LogIn';
$lang['capt-email-valid']			= 'Ketik Ulang Email';

$lang['capt-st-worksheet']			= 'Worksheet';
$lang['capt-st-user']				= 'Status User';

$lang['capt-aktiv']					= 'Aktif';	
$lang['capt-tidak-aktiv']			= 'Disable';	

// =============================
$lang['table-no']					= 'No.';
$lang['table-ws']					= 'Worksheet';
$lang['table-lokasi']				= 'Lokasi';
$lang['table-btn-pil']				= 'Pilih Data Worksheet';

// =============================
$lang['btn-save']					= "Simpan";
$lang['btn-batal']					= "Batal";
$lang['btn-delete']					= "Hapus";


$lang['karyawan_or']				= "atau";
// =============================
$lang['plc-wajib-isi']				= "Wajib Diisi";
 
// ============================= 
$lang['modal-judul']				= "Pilih Data Worksheet";
$lang['modal-pilih']				= "Pilih";

// =============================
$lang['label_simpan_sukses'] 		= "Simpan Data Karyawan baru sukses";
$lang['label_ws_kosong']			= "Worksheet tidak boleh kosong";
$lang['label_cancel_sukses']		= "Data Karyawan telah berhasil dibatalkan";
$lang['label_edit_sukses'] 			= "Edit Data Karyawan baru sukses";

// =============================
$lang['konfirmasi-delete']				= "Apakah anda yakin akan menghapus Karyawan terpilih ?";
$lang['konfirmasi-data-tidak-ada']		= "Data tidak ditemukan";
$lang['konfirmasi-error-pil-delete']	= "Tidak ada data karyawan yang dipilih";
$lang['konfirmasi-delete-sukses']		= "Delete dan Disable User Data Karyawan sukses";



