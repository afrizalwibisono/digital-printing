<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for karyawan
	
	..:: Design Array Session ::..

	array(
			st_proses 	=>,
			idkaryawan 	=>, 
			jabatan		=>, 
			nama 		=>, 
			id_user 	=>, 
			alamat 		=>, 
			kota 		=>, 
			telp 		=>, 
			wa 			=>, 
			email 		=>, 
			ket 		=>,
			no_id 		=>,
			user_name 	=>,
			email_user	=>,
			dt_ws 		=> array(
									idws 	=>,
									nm_ws 	=>,
									lokasi	=>
								)
	
		)	




*/
class Karyawan extends Admin_Controller {

	/**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Karyawan.View";
    protected $addPermission    = "Karyawan.Add";
    protected $managePermission = "Karyawan.Manage";
    protected $deletePermission = "Karyawan.Delete";

    protected $prefixKey        = "KY";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('karyawan/karyawan');
        $this->load->model(array(
        							'karyawan_model',
        							'karyawan_detail_workshet_model',
        							'users_model',
        							'jabatan_model',
        							'worksheet_model'
        						)
    						);

        $this->template->title(lang('karyawan_title'));
		$this->template->page_icon('fa fa-list');
    }


    public function index(){

        $this->auth->restrict($this->viewPermission);

        if(isset($_POST["delete"]) && has_permission($this->deletePermission)){

        	if($this->delete()){

        		$this->template->set_message(lang("konfirmasi-delete-sukses"),"success");

        	}

        }


        if(isset($_POST['search'])){

            $search     = isset($_POST['search']) ? $this->input->post("search") : '';
            $jabatan    = $this->input->post("jabatan");
            $worksheet  = $this->input->post("worksheet");

            

        }else{

            $search     = isset($_GET['search']) ? $this->input->get("search") : '';
            $jabatan    = $this->input->get("jabatan");
            $worksheet  = $this->input->get("worksheet");

        }

        $this->load->library('pagination'); //librari pagination

        $filter = "?search=".$search;

        $search2 = $this->db->escape_str($search);

        $where  = "karyawan.deleted = 0 and karyawan.st_tampil = 1 and (karyawan.nama like '%$search2%' or karyawan.alamat like '%$search2%' or karyawan.kota like '%$search2%')";

        if($jabatan!=""){

            $dt_status_ws   = $this->jabatan_model->select("st_worksheet as st")
                                                ->find($jabatan);

            if($dt_status_ws->st == 0){// tidak punya worksheet

                $filter     .= "&jabatan=".urlencode($jabatan);
                $where      .= "and karyawan.idjabatan = $jabatan";

            }else{

                if($worksheet!=""){

                    $filter .= "&worksheet=".urlencode($worksheet);
                    $where  .= "and karyawan_detail_worksheet.id_worksheet = $worksheet";

                }else{

                    $filter     .= "&jabatan=".urlencode($jabatan);
                    $where      .= "and karyawan.idjabatan = $jabatan";

                }

            }

        }

        $total   = $this->karyawan_model->select("karyawan.idkaryawan")
                                ->join("jabatan","karyawan.idjabatan = jabatan.idjabatan","inner")
                                ->join("users","users.id_user = karyawan.id_user","left")
                                ->join("karyawan_detail_worksheet","karyawan_detail_worksheet.idkaryawan = karyawan.idkaryawan","left")
                                ->group_by("karyawan.idkaryawan")
                                ->where($where)
                                ->count_all();


        $offset = $this->input->get('per_page'); // start record

        $limit = $this->config->item('list_limit'); //jml tiap halaman

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);
       

        $data   = $this->karyawan_model->select("`karyawan`.`idkaryawan` as id,
                                            `karyawan`.`nama`,
                                            `karyawan`.`telp`,
                                            `jabatan`.`nm_jabatan`,
                                            `users`.`username`,
                                            IF(`users`.`st_aktif` IS NOT NULL
                                                    AND `users`.`st_aktif` = 1,
                                                1,
                                                0) AS st_user,
                                            IF(`karyawan_detail_worksheet`.`idkaryawan_detail_worksheet` IS NOT NULL,
                                                1,
                                                0) AS ws"
                                            )
                                ->join("jabatan","karyawan.idjabatan = jabatan.idjabatan","inner")
                                ->join("users","users.id_user = karyawan.id_user","left")
                                ->join("karyawan_detail_worksheet","karyawan_detail_worksheet.idkaryawan = karyawan.idkaryawan","left")
                                ->group_by("karyawan.idkaryawan")
                                ->where($where)
                                ->order_by("karyawan.nama","asc")
                                ->limit($limit, $offset)
                                ->find_all();

      
        $data_jabatan   = $this->jabatan_model->select("idjabatan as id, nm_jabatan as nama, st_worksheet as st")
                                                ->order_by("idjabatan", "asc")
                                                ->find_all();

        $data_ws        = $this->worksheet_model->select("`id_worksheet` as id, `nm_worksheet` as nama, `lokasi`")
                                                ->where("deleted=0")
                                                ->order_by("nm_worksheet", "asc")
                                                ->find_all();   



        $asset  =   [
                        'karyawan/assets/js/utama.js',
                        'plugins/select2/dist/css/select2.min.css',
                        'plugins/select2/dist/js/select2.min.js'
                    ];
                    
        add_assets($asset);

        $this->template->set("worksheet",$data_ws);
        $this->template->set("jabatan",$data_jabatan);                                        
        $this->template->set("dt",$data);
    	$this->template->set("toolbar_title", lang('konsumen_title_manage'));
        $this->template->set("page_title", lang('konsumen_title_manage'));

        $this->template->set("numb", $offset+1);

        $this->template->render('index'); 

    }

    public function create(){

		$this->auth->restrict($this->addPermission);

		if(isset($_POST['simpan'])){

            if($this->save(0,"")){

                Template::set_message(lang('label_simpan_sukses'),'success');
                redirect('karyawan');

            }

		}


		$asset 	= 	[
						'karyawan/assets/js/frm_karyawan.js',
						'plugins/select2/dist/css/select2.min.css',
                		'plugins/select2/dist/js/select2.min.js'
                	];

		$data_jabatan 	= $this->jabatan_model->select("idjabatan as id, nm_jabatan as nama, st_worksheet as st")
												->order_by("idjabatan", "asc")
												->find_all();

       
        //kondisikan session
        $data_sess  = $this->session->userdata("dt_karyawan");
        if(!$data_sess){

            $data_array     = array(
                                        "st_proses"     =>  0,      
                                        "idkaryawan"    =>  "",
                                        "id_user"       =>  0,   
                                        "jabatan"       =>  "",
                                        "nama"          =>  "",
                                        "alamat"        =>  "",
                                        "kota"          =>  "",
                                        "telp"          =>  "",
                                        "wa"            =>  "",
                                        "email"         =>  "",
                                        "ket"           =>  "",
                                        "no_id"         =>  "",
                                        "pil-user"      =>  "",       
                                        "user_name"     =>  "",
                                        "email_user"    =>  "",
                                        "st_pakai_ws"   =>  "0",
                                        "daftar_ws"     =>  array()
                                );

            $this->session->set_userdata("dt_karyawan",$data_array);
            $data_sess  = $this->session->userdata("dt_karyawan");

        }

        add_assets($asset);

        $this->template->set("dts",$data_sess);
		$this->template->set("jabatan",$data_jabatan);
		$this->template->set("page_title", lang('karyawan_title_new'));
   	    $this->template->render("form_karyawan");    	

    }

    
    public function edit($id){

        $this->auth->restrict($this->managePermission);


        if(isset($_POST['simpan'])){

            if($this->save(1,$id)){

                Template::set_message(lang('label_edit_sukses'),'success');
                redirect('karyawan');

            }

        }

        
        $this->set_array_session(1,$id);

        $data_sess      = $this->session->userdata("dt_karyawan");

        $this->session->unset_userdata('dt_karyawan');

        $data_jabatan   = $this->jabatan_model->select("idjabatan as id, nm_jabatan as nama, st_worksheet as st")
                                                ->order_by("idjabatan", "asc")
                                                ->find_all();

        $asset  =   [
                        'karyawan/assets/js/frm_karyawan.js',
                        'plugins/select2/dist/css/select2.min.css',
                        'plugins/select2/dist/js/select2.min.js'
                    ];

        add_assets($asset);

        $this->template->set("dts",$data_sess);
        $this->template->set("jabatan",$data_jabatan);
        $this->template->set("page_title", lang('karyawan_title_edit'));
        $this->template->render("form_karyawan");       

    }


    public function delete(){

    	/*
			Delete karyawan, berarti mendelete 2 Data, yaitu
				1.	Data karyawan;
				2. 	Data usernya;
    	*/

    	$return = false;

    	if(isset($_POST['checked'])){

    		$id 		= $this->input->post("checked");
    		$sql_all	= ""; 		

    		$this->db->trans_start();

	    		foreach ( $id as $key => $isi) {
	    			
	    			// ===== Delete usernya ======
	    			$id_user 	= $this->karyawan_model->select("id_user")->find($isi);
	    			if(strlen($id_user->id_user)>0){

	    				$this->users_model->delete($id_user->id_user);
	    				$sql_all 	= $this->db->last_query()."\n\n" ;

	    			}

	    			// ==== delete data karyawannya ======

	    			$this->karyawan_model->delete($isi);
	    			$sql_all 	.= $this->db->last_query();


	    		}

    		$this->db->trans_complete();

    		if($this->db->trans_status() == false){

    			$return 		= false;
    			$keterangan 	= "SUKSES, delete data karyawan dan usernya";
    			$status 		= 0;

    		}else{

    			$return 		= true;
    			$keterangan 	= "SUKSES, delete data karyawan dan usernya";
    			$status 		= 1;

    		}

    		$nm_hak_akses   = $this->deletePermission; 
            $kode_universal = "-";
            $jumlah         = 0;
            $sql            = $sql_all;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);


    	}else{

    		$this->template->set_message(lang("konfirmasi-error-pil-delete"),"error");
    		return false;

    	}


    	return $return;

    }





    public function cancel(){

        $this->auth->restrict($this->addPermission);

        $this->session->unset_userdata('dt_karyawan');
        $this->template->set_message(lang('label_cancel_sukses'), 'success');

        redirect('karyawan');
    }





    function cek_jabatan($val){

        return $val == "" ? false : true;

    }

    function save($status = 0, $id = ""){ // 0 = baru, 1 = edit

        $this->form_validation->set_rules("nama","lang:capt-nama","required");
        $this->form_validation->set_rules("telp","lang:capt-telp","required|numeric");
        $this->form_validation->set_rules("posisi","lang:capt-posisi","callback_cek_jabatan");

        $this->form_validation->set_message("cek_jabatan", "You should choose the {field} field");

        if(isset($_POST['pil-user'])){ // jika dicentang pemakaian user

            $this->form_validation->set_rules("pil-user","capt-pil-user");
            $this->form_validation->set_rules("username","lang:capt-username","required");
            $this->form_validation->set_rules("email-login","lang:capt-email-user","required|valid_email");
            $this->form_validation->set_rules("ketik-ulang","lang:capt-email-valid","required|matches[email-login]");            

        }

        if($this->form_validation->run() === false){ //jika validasi error

            $this->set_array_session();
            Template::set_message(validation_errors(),'error');
            return false;

        }else{ // validasi benar

            if(!isset($_POST['idws-pakai']) && $_POST['st_buka_daftar'] == "1"){

                $this->set_array_session();
                Template::set_message(lang('label_ws_kosong'),'error');
                return false;

            }

        }


        $jabatan        = $this->input->post("posisi");
        $nama           = $this->input->post("nama");
        $alamat         = trim($this->input->post("alm"));
        $kota           = $this->input->post("kota");
        $telp           = $this->input->post("telp");
        $wa             = $this->input->post("wa");
        $email          = $this->input->post("email");
        $ket            = trim($this->input->post("ket"));
        $no_id          = $this->input->post("ktp");
        $user_name      = $this->input->post("username");
        $email_user     = $this->input->post("email-login");
        $pil_user       = isset($_POST['pil-user']) ? true : false;

        $dt_idws        = $this->input->post("idws-pakai");


        if($status == 0 && strlen(trim($id)) <= 0){ //data baru

            // proses simpan data baru
            /*$jabatan        = $this->input->post("posisi");
            $nama           = $this->input->post("nama");
            $alamat         = trim($this->input->post("alm"));
            $kota           = $this->input->post("kota");
            $telp           = $this->input->post("telp");
            $wa             = $this->input->post("wa");
            $email          = $this->input->post("email");
            $ket            = trim($this->input->post("ket"));
            $no_id          = $this->input->post("ktp");
            $user_name      = $this->input->post("username");
            $email_user     = $this->input->post("email-login");
            $pil_user       = isset($_POST['pil-user']) ? true : false;

            //$st_pakai_ws    = $this->input->post("st_buka_daftar");
            $dt_idws        = $this->input->post("idws-pakai");*/

            

            // Simpan data user
            if($pil_user){// jika dipilih

                $id_user_pakai          = $this->auth->register($user_name, $email_user, $nama, $telp, 2);

                if(!$id_user_pakai){ // jika pembuatan user gagal

                    $this->set_array_session();
                    Template::message();
                    return false;

                }

                $idkaryawan             = gen_primary($this->prefixKey, "karyawan", "idkaryawan");    
                $data_simpan_karyawan   = array(
                                                "idkaryawan"    => $idkaryawan,
                                                "idjabatan"     => $jabatan, 
                                                "nama"          => $nama, 
                                                "id_user"       => $id_user_pakai, 
                                                "alamat"        => $alamat, 
                                                "kota"          => $kota, 
                                                "telp"          => $telp, 
                                                "wa"            => $wa, 
                                                "email"         => $email, 
                                                "ket"           => $ket, 
                                                "no_id"         => $no_id
                                            );

            }else{

                $idkaryawan             = gen_primary($this->prefixKey, "karyawan", "idkaryawan");
                $data_simpan_karyawan   = array(
                                                "idkaryawan"    => $idkaryawan,
                                                "idjabatan"     => $jabatan, 
                                                "nama"          => $nama, 
                                                "alamat"        => $alamat, 
                                                "kota"          => $kota, 
                                                "telp"          => $telp, 
                                                "wa"            => $wa, 
                                                "email"         => $email, 
                                                "ket"           => $ket, 
                                                "no_id"         => $no_id
                                            );    
            }

            $st_daftar_terisi = 0;

            if(isset($dt_idws) && count($dt_idws)){ //berarti memiliki daftar worksheet

                //Buat susunan arraynya untuk insert Batch
                $st_daftar_terisi   = 1;
                //$dt_idws            = $_POST["idws-pakai"];
                $idws_simpan        = gen_primary($this->prefixKey, "karyawan_detail_worksheet", "idkaryawan_detail_worksheet");

                foreach ($dt_idws as $key => $isi) {
                    
                    $data_simpan_workshet[] = array(
                                                    "idkaryawan_detail_worksheet"   => $idws_simpan.$key, 
                                                    "idkaryawan"                    => $idkaryawan,           
                                                    "id_worksheet"                  => $isi
                                                    );
                
                }

            }

            //simpan semua data

            $all_sql    = "";
            $this->db->trans_start();

                $this->karyawan_model->insert($data_simpan_karyawan);
                $all_sql = $this->db->last_query();

                if($st_daftar_terisi == 1){
                    
                    $this->karyawan_detail_workshet_model->insert_batch($data_simpan_workshet);
                    $all_sql .= "\n\n".$this->db->last_query();

                }

            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE){

                $keterangan = "GAGAL, simpan data karyawan dengan ID ".$idkaryawan;
                $status     = 0;

                $return     = FALSE;    

            }else{

                $keterangan = "SUKSES, simpan data karyawan dengan ID ".$idkaryawan;
                $status     = 1;

                $return     = TRUE;                    

            }

            $nm_hak_akses   = $this->addPermission; 
            $kode_universal = $idkaryawan;
            $jumlah         = 0;
            $sql            = $all_sql;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

            if($return)
            {
                //Clear Session
                $this->session->unset_userdata('dt_karyawan');
            }


        }else{ //edit

            // =========== cek pembanding data worksheet
            $dt_idws            = $this->input->post("idws-pakai"); // dari inputan form
            $st_daftar_form     = count($dt_idws) > 0 ? TRUE : FALSE; //true jika ada daftarnya | False jika tidak ada

            $dt_table_idws      = $this->karyawan_detail_workshet_model
                                        ->select("idkaryawan_detail_worksheet as id,id_worksheet")
                                        ->where("deleted = 0 and idkaryawan='$id'")
                                        ->find_all();
            $st_daftar_tabel    = count($dt_table_idws) > 0 ? TRUE : FALSE;

            $st_del_all_table   = FALSE; // TRUE = Delete semua worksheet dalam tabel
            $st_del_pil_table   = FALSE; // TRUE = Delete hanya yang ditemukan dihapus dari list
            $st_add_all_table   = FALSE; // TRUE = Ada yang harus ditambahkan ke dalam tabel

            if($st_daftar_form && $st_daftar_tabel){ //jika diform dan tabel sama2 mempunyai daftar worksheet
            
                // Konversi data idworksheet menjadi array biasa
                $ws_tb          = array();
                foreach ($dt_table_idws as $key => $value) {
                    $ws_tb[]    = $value->id_worksheet;
                }

                // Konversi data worksheet dengan dibalik => idworksheet sebagai key && iddetail sebagai value
                // Fungsinya untuk mengambil primary key data detail yang harus dihapus, dengan menggunakan idworksheet sebagai key
                $ws_key_del     = array();
                foreach ($dt_table_idws as $key => $balik) {
                    $ws_key_del[$balik->id_worksheet] = $balik->id;         
                }     

                $hasil          = array_diff($dt_idws, $ws_tb); //idworksheet yang ditambahkan
                $hasil_del      = array_diff($ws_tb,$dt_idws); //idworksheet yang didelete, sebagai acuan array

                $st_del_pil_table = count($hasil_del) > 0 ? TRUE : FALSE;

                // Catatan : untuk mendelete yang ada ditabel caranya
                // Ambil PK dari $ws_key_del, dengan key Array dari $hasil_del
                // PK = $ws_key_del[$hasil_del[1]]

                /*print_r($dt_idws);
                print_r($ws_tb);

                echo "----Hasil----";

                print_r($hasil);
                print_r($hasil_del);  */              

                //susun array untuk insert idworksheet tambahan
                if(count($hasil)){ //jika ada yang ditambahkan

                    $st_add_all_table   = TRUE;

                    $kdPrimary          = gen_primary($this->prefixKey,"karyawan_detail_worksheet","idkaryawan_detail_worksheet");
                    $arr_tambah         = array();
                    foreach ($hasil as $key => $isi_hasil) {
                        
                        $arr_tambah[]   = array(
                                                    "idkaryawan_detail_worksheet"   => $kdPrimary.$key,
                                                    "idkaryawan"                    => $id,
                                                    "id_worksheet"                  => $isi_hasil
                                                );

                    }
                }

            }elseif ($st_daftar_form && !$st_daftar_tabel) {

                $st_add_all_table   = TRUE;
                $kdPrimary          = gen_primary($this->prefixKey,"karyawan_detail_worksheet","idkaryawan_detail_worksheet");
                $arr_tambah         = array();
                foreach ($dt_idws as $key => $isi) {
                    
                    $arr_tambah[]   = array(
                                                "idkaryawan_detail_worksheet"   => $kdPrimary.$key,
                                                "idkaryawan"                    => $id,
                                                "id_worksheet"                  => $isi
                                            );

                }

            }elseif (!$st_daftar_form && $st_daftar_tabel) {
                
                $st_del_all_table = TRUE;

            }

            // ----- Pengkondisian User Account -------
            $id_user_account    = 0;
            $tambah_user        = false;
            if(isset($_POST["pil-user"])){ //jika statusnya membuat user, maka buat usernya

                $id_user_account    = $this->auth->register($user_name, $email_user, $nama, $telp, 2);

                if(!$id_user_account){ // jika pembuatan user gagal

                    $this->set_array_session();
                    Template::message();
                    return false;

                }else{

                    $tambah_user = true;

                }


            }  


            //---- Buat array untuk update data karyawan ------
            
            if($tambah_user){


                $data_simpan_karyawan   =   [
                                                "idjabatan"     => $jabatan, 
                                                "nama"          => $nama, 
                                                "id_user"       => $id_user_account,
                                                "alamat"        => $alamat, 
                                                "kota"          => $kota, 
                                                "telp"          => $telp, 
                                                "wa"            => $wa, 
                                                "email"         => $email, 
                                                "ket"           => $ket, 
                                                "no_id"         => $no_id
                                            ];

            }else{

                $data_simpan_karyawan   =   [
                                                "idjabatan"     => $jabatan, 
                                                "nama"          => $nama, 
                                                "alamat"        => $alamat, 
                                                "kota"          => $kota, 
                                                "telp"          => $telp, 
                                                "wa"            => $wa, 
                                                "email"         => $email, 
                                                "ket"           => $ket, 
                                                "no_id"         => $no_id
                                            ];


            } 


            $this->db->trans_start();

                $sql_all    = "";

                //simpan Update Data karyawan
                $this->karyawan_model->update($id,$data_simpan_karyawan);
                $sql_all    = $this->db->last_query();

                //delete detail worksheetnya
                if($st_del_all_table){

                    $where_del  = array("idkaryawan" => $id);    
                    $this->karyawan_detail_workshet_model->delete_where($where_del);
                    $sql_all    .= "\n\n". $this->db->last_query();

                }

                if($st_del_pil_table){

                    if(count($hasil_del)){

                        foreach ($hasil_del as $key => $isi_del) {
                            
                            $iddelete   = $ws_key_del[$isi_del];
                            $this->karyawan_detail_workshet_model->delete($iddelete);
                            $sql_all    .= "\n\n". $this->db->last_query();

                        }

                    }

                }


                //simpan worksheet yang ditambahkan
                if($st_add_all_table){

                    $this->karyawan_detail_workshet_model->insert_batch($arr_tambah);
                    $sql_all    .= "\n\n". $this->db->last_query();                    

                }

            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE){

                $keterangan = "GAGAL, edit data karyawan dengan ID ".$id;
                $status     = 0;

                $return     = FALSE;    

            }else{

                //echo $sql_all;

                $keterangan = "SUKSES, edit data karyawan dengan ID ".$id;
                $status     = 1;

                $return     = TRUE;                    

            }

            $nm_hak_akses   = $this->managePermission; 
            $kode_universal = $id;
            $jumlah         = 0;
            $sql            = $sql_all;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

            if($return)
            {
                //Clear Session
                $this->session->unset_userdata('dt_karyawan');
            }


        }

        return $return;

    }


    public function isi_worksheet(){

    	$this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request()){
            redirect('karyawan');
        }

        $cari = $this->db->escape_str($this->input->post('cr_ws')); //bersihkan inputan pencarian dari karakter aneh

        $where 		= "deleted=0 and nm_worksheet like '%$cari%'";

        $data_ws 	= $this->worksheet_model->select("`id_worksheet` as id, `nm_worksheet` as nama, `lokasi`")
        									->where($where)
        									->order_by("nm_worksheet", "asc")
        									->find_all();	

        echo json_encode($data_ws);


    }


    private function set_array_session($st_proses = 0, $idkaryawan = ""){

        /*Keterangan :
                    1.    $st_proses = 0 , Jika status data baru / create
                    2.    $st_proses = 1 , Jika status data edit   
                    3.  $st_session = Trus, berarti session dibuat
                    4.  $st_session = False, berarti session di destroy      
        */

        if($st_proses == 0){            
            
            $st_proses      = 0;
            $idkaryawan     = "";
            $id_user        = 0;        	
            $jabatan 	    = $this->input->post("posisi");
    		$nama 		    = $this->input->post("nama");
    		$alamat 	    = $this->input->post("alm");
    		$kota 		    = $this->input->post("kota");
    		$telp 		    = $this->input->post("telp");
    		$wa 		    = $this->input->post("wa");
    		$email 		    = $this->input->post("email");
    		$ket 		    = $this->input->post("ket");
    		$no_id 		    = $this->input->post("ktp");
    		$user_name	    = $this->input->post("username");
    		$email_user	    = $this->input->post("email-login");
            $pil_user       = isset($_POST['pil-user']) ? 'checked' : '';

            $st_pakai_ws    = $this->input->post("st_buka_daftar");
            $dt_idws        = $this->input->post("idws-pakai");
            $dt_nmws        = $this->input->post("nmws-pakai");
            $dt_lkws        = $this->input->post("lkws-pakai");

            //buat array untuk ws terpilih
            if(count($dt_idws) && $st_pakai_ws == 1){

                $dtws = array();

                for ($i=0; $i < count($dt_idws) ; $i++) { 
                    
                    $dtws[] = array(
                                        "id_detail" => "",
                                        "id"        => $dt_idws[$i],
                                        "nm"        => $dt_nmws[$i],  
                                        "lk"        => $dt_lkws[$i]   
                                    );            

                }

            }else{

                $dtws = array();

            }

        }else{

            $data_karyawan  = $this->karyawan_model->select("
                                                                `karyawan`.`idkaryawan`,
                                                                `karyawan`.`idjabatan`,
                                                                `karyawan`.`nama`,
                                                                `karyawan`.`id_user`,
                                                                `karyawan`.`alamat`,
                                                                `karyawan`.`kota`,
                                                                `karyawan`.`telp`,
                                                                `karyawan`.`wa`,
                                                                `karyawan`.`email`,
                                                                `karyawan`.`ket`,
                                                                `karyawan`.`no_id`,
                                                                `users`.`username`,
                                                                `users`.`email` as useremail,
                                                                `karyawan_detail_worksheet`.`idkaryawan_detail_worksheet`,
                                                                `karyawan_detail_worksheet`.`id_worksheet`,
                                                                `worksheet`.`nm_worksheet`,
                                                                `worksheet`.`lokasi`
                                                            ")
                                                    ->join("users","users.id_user = karyawan.id_user","left")
                                                    ->join("karyawan_detail_worksheet","karyawan_detail_worksheet.idkaryawan = karyawan.idkaryawan","left")
                                                    ->join("worksheet","worksheet.id_worksheet = karyawan_detail_worksheet.id_worksheet","left")
                                                    ->where("karyawan.deleted = 0 and 
                                                                karyawan.idkaryawan = '$idkaryawan'
                                                                and (`karyawan_detail_worksheet`.`deleted` = 0 
                                                                or `karyawan_detail_worksheet`.`deleted` is null)")
                                                    ->order_by("karyawan_detail_worksheet.id_worksheet","asc")
                                                    ->find_all();


            //echo $this->db->last_query();
            $st_proses      = 1;
            $idkaryawan     = $idkaryawan;
            $id_user        = $data_karyawan[0]->id_user;
            $jabatan        = $data_karyawan[0]->idjabatan;
            $nama           = $data_karyawan[0]->nama;
            $alamat         = $data_karyawan[0]->alamat;
            $kota           = $data_karyawan[0]->kota;
            $telp           = $data_karyawan[0]->telp;
            $wa             = $data_karyawan[0]->wa;
            $email          = $data_karyawan[0]->email;
            $ket            = $data_karyawan[0]->ket;
            $no_id          = $data_karyawan[0]->no_id;
            $user_name      = $data_karyawan[0]->username;
            $email_user     = $data_karyawan[0]->useremail;
            $pil_user       = '';

            $st_pakai_ws    = strlen($data_karyawan[0]->idkaryawan_detail_worksheet) > 0 ? 1 : 0 ;
            

            if(strlen($data_karyawan[0]->idkaryawan_detail_worksheet) > 0) {
                //jika baris pertama tidak null, maka dianggap memiliki worksheet

                $dtws = array();

                foreach ($data_karyawan as $key => $val_ws) {
                    
                    $dtws[] = array(
                                        "id_detail" => $val_ws->idkaryawan_detail_worksheet,
                                        "id"        => $val_ws->id_worksheet,
                                        "nm"        => $val_ws->nm_worksheet,  
                                        "lk"        => $val_ws->lokasi
                                    );   

                }

            }else{

                $dtws = array();

            }



        }

        

        $data_array     = array(
                                "st_proses"     =>  $st_proses,      
                                "idkaryawan"    =>  $idkaryawan,  
                                "id_user"       =>  $id_user,   
                                "jabatan"       =>  $jabatan,        
                                "nama"          =>  $nama,           
                                "alamat"        =>  $alamat,         
                                "kota"          =>  $kota,           
                                "telp"          =>  $telp,           
                                "wa"            =>  $wa,             
                                "email"         =>  $email,          
                                "ket"           =>  $ket,            
                                "no_id"         =>  $no_id,
                                "pil-user"      =>  $pil_user,       
                                "user_name"     =>  $user_name,      
                                "email_user"    =>  $email_user,     
                                "st_pakai_ws"   =>  $st_pakai_ws,    
                                "daftar_ws"     =>  $dtws
                                );



        $this->session->set_userdata("dt_karyawan",$data_array);

    }



}	