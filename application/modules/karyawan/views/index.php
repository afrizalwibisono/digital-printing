<?php 
   
  $ENABLE_ADD     = has_permission('Karyawan.Add'); 
    $ENABLE_MANAGE  = has_permission('Karyawan.Manage'); 
    $ENABLE_DELETE  = has_permission('Karyawan.Delete'); 
   
?> 
 
<div class="box box-primary"> 
  <?= form_open($this->uri->uri_string(),array('id'=>'frm_karyawan','name'=>'frm_karyawan')) ?> 
    <div class="box-header"> 
        <?php if ($ENABLE_ADD) : ?> 
        <a href="<?= site_url('karyawan/create') ?>" class="btn btn-success" title="<?= lang('btn_create') ?>"><?= lang('btn_create') ?></a> 
        <?php endif; ?> 

        <div class="pull-right form-inline"> 
            <div class="form-group">
                <select id="jabatan" name="jabatan" class="form-control" style="min-width: 200px;">
                    <option></option>
                    <?php foreach ($jabatan as $jn) : ?>
                    <option data-status="<?= $jn->st ?>" value="<?= $jn->id ?>" <?= set_select('jabatan', $jn->id, isset($idjabatan) && $idjabatan == $jn->id)?>> <?= $jn->nama ?> </option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="form-group" id="ws-input">
                <select id="worksheet" name="worksheet" class="form-control" style="min-width: 200px;">
                    <option></option>
                    <?php foreach ($worksheet as $ws) : ?>
                    <option value="<?= $ws->id ?>" <?= set_select('worksheet', $ws->id, isset($idws) && $idws == $ws->id)?>> <?= strtoupper($ws->nama) ?> </option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="input-group"> 
                <input type="text" name="search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="Search" autofocus /> 
                <div class="input-group-btn"> 
                    <button class="btn btn-default"><i class="fa fa-search"></i></button> 
                </div> 
            </div> 
        </div> 
        
    </div>
    

    <?php 

        if(isset($dt) && is_array($dt) && count($dt)):

    ?>

    <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
                    <th width="50">#</th>
                    <th><?= lang("capt-nama") ?></th>
                    <th class="text-center"><?= lang("capt-telp") ?></th>
                    <th ><?= lang("capt-posisi") ?></th>
                    <th class="text-center"><?= lang("capt-st-worksheet") ?></th>
                    <th class="text-center"><?= lang("capt-username") ?></th>
                    <th class="text-center"><?= lang("capt-st-user") ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                
                <?php 

                    foreach ($dt as $key => $isi) :
                       
                ?>

                    <tr>
                        <td>
                            <input type="checkbox" name="checked[]" value="<?= $isi->id ?>">
                        </td>
                        <td class="text-center"><?= $numb ?></td>                        
                        <td><?= ucwords($isi->nama) ?></td>
                        <td class="text-center"><?= $isi->telp ?></td>
                        <td><?= strtoupper($isi->nm_jabatan) ?></td>
                        <td class="text-center">
                            
                            <?php 
                                if($isi->ws == 1):
                            ?>
                                    <span class="fa fa-check text-green"></span>
                            <?php
                                endif;
                            ?>
                                
                        </td>
                        <td class="text-center"><?= $isi->username ?></td>
                        <td class="text-center">
                            <?php 

                                if($isi->st_user == 1): 
                            
                            ?>    
                                    <span class="label label-success"><?= lang("capt-aktiv") ?></span>

                            <?php
        
                                endif;

                            ?>

                        </td>
                        <?php if($ENABLE_MANAGE) : ?>
                        <td style="padding-right:20px"><a class="text-black" href="<?= site_url('karyawan/edit/' . $isi->id); ?>" data-toggle="tooltip" data-placement="left" title="Edit Data"><i class="fa fa-pencil"></i></a></td>
                        <?php endif; ?>

                    </tr>

                <?php               
                         $numb++;
                    endforeach;

                ?>

            </tbody>
        </table> 
    </div> 
    <div class="box-footer clearfix">

        <?php if($ENABLE_DELETE) : ?>
        <input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('btn-delete') ?>" onclick="return confirm('<?= lang('konfirmasi-delete'); ?>')">
        <?php endif;
        echo $this->pagination->create_links(); 
        ?>

    </div>

    <?php 
        
        else:
    ?>

    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('konfirmasi-data-tidak-ada') ?></p>
    </div>

    <?php

        endif;    

        echo form_close(); 
    ?> 
 
</div>   