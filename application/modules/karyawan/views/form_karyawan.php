<?php
		//print_r($dts);
?>

<div class="box box-primary">
	
	<?= form_open($this->uri->uri_string(), array('name' => 'frm_karyawan', 'id' => 'frm_karyawan','role'=>'form','class' => 'form-horizontal')) ?>

	<div class="box-body">
		
		<div class="form-group <?= form_error('nama') ? 'has-error' : '' ?>">
			
			<label for="nama" class="col-sm-2 control-label"><?= lang("capt-nama") ?></label>
			<div class="col-sm-4">
				
				<input type="text" name="nama" id="nama" class="form-control" required placeholder="<?= lang('plc-wajib-isi') ?>" value="<?= set_value("nama",isset($dts['nama']) ? $dts['nama'] : '' ) ?>" >

			</div>


		</div>

		<div class="form-group <?= form_error('alm') ? 'has-error' : '' ?>">
			
			<label for="alm" class="col-sm-2 control-label"><?= lang("capt-alm") ?></label>
			<div class="col-sm-4">
				
				<textarea class="form-control" name="alm" id="alm"><?= trim(set_value("alm",isset($dts['alamat']) ? $dts['alamat'] : '' )) ?></textarea>

			</div>

		</div>		

		<div class="form-group <?= form_error('kota') ? 'has-error' : '' ?>">
			
			<label for="kota" class="col-sm-2 control-label"><?= lang("capt-kota") ?></label>
			<div class="col-sm-4">
				
				<input type="text" name="kota" id="kota" class="form-control" value="<?= set_value("kota",isset($dts['kota']) ? $dts['kota'] : '' ) ?>">

			</div>

		</div>

		<div class="form-group <?= form_error('telp') ? 'has-error' : '' ?>">
			
			<label for="telp" class="col-sm-2 control-label"><?= lang("capt-telp") ?></label>
			<div class="col-sm-4">
				
				<input type="text" name="telp" id="telp" class="form-control" value="<?= set_value("telp",isset($dts['telp']) ? $dts['telp'] : '' ) ?>">

			</div>


		</div>

		<div class="form-group <?= form_error('wa') ? 'has-error' : '' ?>">
			
			<label for="wa" class="col-sm-2 control-label"><?= lang("capt-wa") ?></label>
			<div class="col-sm-4">
				
				<input type="text" name="wa" id="wa" class="form-control" value="<?= set_value("wa",isset($dts['wa']) ? $dts['wa'] : '' ) ?>">

			</div>


		</div>

		<div class="form-group <?= form_error('email') ? 'has-error' : '' ?>">
			
			<label for="email" class="col-sm-2 control-label"><?= lang("capt-email") ?></label>
			<div class="col-sm-4">
				
				<input type="text" name="email" id="email" class="form-control" value="<?= set_value("email",isset($dts['email']) ? $dts['email'] : '' ) ?>">

			</div>


		</div>

		<div class="form-group <?= form_error('ktp') ? 'has-error' : '' ?>">
			
			<label for="ktp" class="col-sm-2 control-label"><?= lang("capt-ktp") ?></label>
			<div class="col-sm-4">
				
				<input type="text" name="ktp" id="ktp" class="form-control" value="<?= set_value("ktp",isset($dts['ktp']) ? $dts['ktp'] : '' ) ?>">

			</div>

		</div>

		<div class="form-group <?= form_error('ket') ? 'has-error' : '' ?>">
			
			<label for="ket" class="col-sm-2 control-label"><?= lang("capt-ket") ?></label>
			<div class="col-sm-4">
				
				<textarea class="form-control" name="ket" id="ket"><?= set_value("ket",isset($dts['ket']) ? $dts['ket'] : '' ) ?></textarea>

			</div>

		</div>

		<div class="form-group <?= form_error('posisi') ? 'has-error' : '' ?> " >
			
			<label class="control-label col-sm-2" for="posisi" ><?= lang('capt-posisi') ?></label>
			<div class="col-sm-4">
				
				<select name="posisi" id="posisi" class="form-control" >

					<option></option>

					<?php 

						if(isset($jabatan) && is_array($jabatan) && count($jabatan)):



							foreach ($jabatan as $key => $jb):

					?>

						<option value="<?= $jb->id ?>" data-ws="<?= $jb->st ?>" <?= set_select("posisi", $jb->id, isset($dts['jabatan']) && $dts['jabatan']==$jb->id)?> ><?= $jb->nama ?> </option>

					<?php
							endforeach;
						endif;	
					?>

				</select>	

			</div>

		</div>

		<div class="table-responsive" id="daftar-ws" >

			<input type="hidden" name="st_buka_daftar" id="st_buka_daftar" value=<?= $dts["st_pakai_ws"] ?>>
			<table class="table table-bordered" style="margin-bottom: 0px !important" id="tabel-ws-terpilih">
				
				<thead>
					<tr class="success">
						<th></th>
						<th><?= lang('table-ws') ?></th>
						<th><?= lang('table-lokasi') ?></th>
					</tr>
				</thead>
				<tbody>
					<?php 
							if(isset($dts['daftar_ws']) && count($dts['daftar_ws'])):
								foreach ($dts['daftar_ws'] as $key => $isi):
										
					?>
								<tr>
									<td></td>
									<td>
										<input type='hidden' name='idws-pakai[]' value="<?= $isi['id'] ?>">
										<input type='hidden' name='nmws-pakai[]' value="<?= $isi['nm'] ?>">
										<input type='hidden' name='lkws-pakai[]' value="<?= $isi['lk'] ?>">
										<?= ucwords($isi['nm']) ?>
										- <a href='#hapus' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a>
									</td>
									<td><?= $isi['lk'] ?></td>
								</tr>
					<?php
							
								endforeach;
							endif;
					?>

				</tbody>
				<tfoot>
					<tr>
						<td colspan="3">
							<button class="btn btn-success" type="button" data-toggle="modal" data-target="#modal-ws">
								
								<span class="fa fa-plus-square"></span>
								<?= lang('table-btn-pil') ?>
								
							</button>

						</td>
					</tr>
				</tfoot>
				
			</table>

			<small class="text-red" style="margin-top: 0px !important" >
				<?= lang('plc-wajib-isi') ?>
			</small>

		</div>






		<div class="form-group" <?= $dts['st_proses'] == 1 && $dts['id_user'] > 0 ? "style='display:none'" : ""  ?> >
			
			<!-- <label for="email" class="col-sm-2 control-label"><?= lang("capt-nama") ?></label> -->
			<div class="col-sm-offset-2 col-sm-4">
				
				<div class="checkbox">
					<label>
						<input type="checkbox" name="pil-user" id="pil-user" value="1" <?= $dts['pil-user'] ?> />
						<?= lang('capt-pil-user') ?>
					</label>
				</div>

			</div>


		</div>

		<div id="area-uses-account" <?= $dts['st_proses'] == 1 && $dts['id_user'] > 0 ? "style='display:none'" : ""  ?> >

			<div class="form-group <?= form_error('username') ? 'has-error' : '' ?>">
				
				<label for="username" class="col-sm-2 control-label"><?= lang("capt-username") ?></label>
				<div class="col-sm-4">
					
					<input type="text" name="username" id="username" class="form-control" value="<?= set_value("username",isset($dts['user_name']) ? $dts['user_name'] : '' ) ?>" readonly >

				</div>


			</div>

			<div class="form-group <?= form_error('email-login') ? 'has-error' : '' ?>">
				
				<label for="email-login" class="col-sm-2 control-label"><?= lang("capt-email-user") ?></label>
				<div class="col-sm-4">
					
					<input type="text" name="email-login" id="email-login" class="form-control" value="<?= set_value("email-login",isset($dts['email-user']) ? $dts['email-user'] : '' ) ?>" readonly>

				</div>


			</div>

			<div class="form-group <?= form_error('ketik-ulang') ? 'has-error' : '' ?>">
				
				<label for="ketik-ulang" class="col-sm-2 control-label"><?= lang("capt-email-valid") ?></label>
				<div class="col-sm-4">
					
					<input type="text" name="ketik-ulang" id="ketik-ulang" class="form-control" readonly>

				</div>


			</div>

		</div>

		<div class="form-group">
			
			<div class="col-sm-offset-2 col-sm-4">
				
				<button class="btn btn-primary" type="submit" name="simpan">
					<?= lang('btn-save') ?>
				</button>
				<?php
	                echo lang('karyawan_or') . ' ' . anchor('karyawan/cancel', lang('btn-batal'));
	            ?>

			</div>

		</div>


	</div>


	<?= form_close() ?>

</div>

<!-- Area Modal -->

<div class="modal fade" id="modal-ws">
  	<div class="modal-dialog"> <!-- modal dalam bentuk besar -->
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          		<span aria-hidden="true">×</span></button>
        		<h4 class="modal-title"><?= lang("modal-judul") ?></h4>
      		</div>
	      	<div class="modal-body">
	        		
	        		<div class="form-group">
	        			<div class="input-group">
	        				<input type="text" name="cr-ws" id="cr-ws" class="form-control"/>
	        				<div class="input-group-btn">
	        					<button class="btn btn-primary" type="button" id="tbl-cari"><span class="fa fa-search"></span></button>
	        				</div>
	        			</div>

	        		</div>

	        		<div class="table-responsive">
	        			
	        			<table class="table table-hover table-striped" id="daftar_pil_ws">
	        				<thead>
	        					<tr class="success">
	        						<th>
	        							<input id="ck_all" name="ck_all" type="checkbox">
	        						</th>
	        						<th>#</th>
	        						<th><?= lang('table-ws') ?></th>
	        						<th><?= lang('table-lokasi') ?></th>
	        					</tr>
	        				</thead>
	        				<tbody>
	        					
	        				</tbody>
	        			</table>

	        		</div>

	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-primary" id="pilih-ws"><?= lang('modal-pilih') ?></button>
	      	</div>
    	</div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div>