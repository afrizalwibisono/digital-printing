$(function(){

	cek_jabatan();

	$("#jabatan").select2({
    			placeholder:'Jabatan',
    			allowClear:true
    });

    $("#worksheet").select2({
    			placeholder:'Worksheet',
    			allowClear:true
    });


    $("#jabatan").change(function(){

    	cek_jabatan();

	});


});

function cek_jabatan(){

	var status = $("#jabatan").find("option:selected").data("status");

	if(status == 1){

		$("#ws-input").show(400);

	}else{

		$("#worksheet").select2({placeholder:'Worksheet',allowClear:true}).val(null).trigger("change");
		$("#ws-input").hide(400);

	}

}