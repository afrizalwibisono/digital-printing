$(function(){
/*
	$('#pil-user').iCheck({
      	checkboxClass: 'icheckbox_minimal-blue',
      	increaseArea: '20%' // optional
    });*/

    cek_inputan_user();

    $("#pil-user").click(function(){
   		
   		cek_inputan_user();

    });

    $("#ck_all").on("click", function(){
		
		if($(this).prop("checked")){
			$("input[name='item-pil[]']").prop("checked", true);
		}else{
			$("input[name='item-pil[]']").prop("checked", false);
		}

	});

    cek_jabatan();

    $("#posisi").select2({
    			placeholder:'Jabatan',
    			allowClear:true
    });

    $("#posisi").change(function(){

    	cek_jabatan();

    });

    $("#tbl-cari").click(function(){

    	isi_ws();

    });

    $("#pilih-ws").click(function(){

    	tambah_ws();

    });

});

function cek_inputan_user(){

	if($("#pil-user").prop("checked")){

		$("#area-uses-account input").prop("readonly", false);

		

	}else{

		$("#area-uses-account input").prop("readonly", true);

		$("#username").val("");
		$("#email-login").val("");
		$("#ketik-ulang").val("");

	}

}


function remove_item(obj){

	$(obj).closest("tr").animate({backgroundColor:'red'}, 1000).fadeOut(1000,function() {
	    $(obj).closest("tr").remove();
	});

}

function tambah_ws(){

	var $obj_ids   	= $("#daftar_pil_ws input[name='item-pil[]']:checked");

	var ids_ws 		= $("#daftar_pil_ws input[name='item-pil[]']:checked").map(function(){
		return $(this).val();
	}).get();


	if(ids_ws.length == 0)
	{
		toastr.clear();
        Command: toastr['error']('Silahkan pilih Worksheet yang akan ditambahkan dulu', 'Error !');
        return false;
	}

	// masukkan semua data di hidden field kedalam array
	var nmws =[];
	var lkws =[];

	$.each($obj_ids, function(){

		nmws.push($(this).closest("tr").find("input[name='namaws[]']").val());
		lkws.push($(this).closest("tr").find("input[name='lokasiws[]']").val());

	});


	var pjg = ids_ws.length;
	var urut = 0;

	for (var i = 0; i < pjg; i++) {

				
		// cek apakah sudah ada data yang sama
		var brs = $("#tabel-ws-terpilih tr").length;
		var cek = false; // jika ketemu data yang sama maka TRUE

		if(brs>3){ // 2 = header dan tombol pilih. 1 data yang sudah diinput

			for (var j = 0; j <= brs-2; j++) {
				
				var kode = $("#tabel-ws-terpilih tr:eq("+ (j+1) +")").find("input[name='idws-pakai[]']").val();

				if(kode != "" && kode == ids_ws[i]){

					cek = true;
					break;

				}

			}


		}

		if(cek==false){

			urut++;

			// buat susunan untuk memasukkan row
			var isi 	= $("<tr>"+
								"<td></td>"+
								"<td>"+
									"<input type='hidden' name='idws-pakai[]' value='"+ ids_ws[i] +"'/>"+
									"<input type='hidden' name='nmws-pakai[]' value='"+ nmws[i] +"'/>"+
									"<input type='hidden' name='lkws-pakai[]' value='"+ lkws[i] +"'/>"+
									nmws[i]+
									" - <a href='#hapus' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a>"+
								"</td>"+
								"<td>"+ lkws[i] +"</td>"+
							"</tr>");

			$("#tabel-ws-terpilih tbody").append(isi);

		}


	}

	$("#modal-ws").modal("hide");
	$("#daftar_pil_ws input[type='checkbox']").prop('checked', false);

}


function isi_ws(){

	var data 	= $("#cr-ws").val();

	$.ajax({
			url 		: baseurl + "karyawan/isi_worksheet",
			type 		: "post",
			dataType	: "json",
			data 		: {cr_ws : data},
			success		: function(msg){

								$("#daftar_pil_ws tbody tr").remove(); //kosongkan tabel
								if(!msg){

									var $tr = $("<tr>"
												+"<td colspan='5' class='text-center'>Data tidak ditemukan</td>"
												+"</tr>");

									$("#daftar_pil_ws tbody").append($tr);

								}else{

									$.each(msg, function(i,n){

										var isi 	= 	"<tr>"+
														"<td> " +
																"<input type='checkbox' name='item-pil[]' value="+n['id']+" >" +
																"<input type='hidden' name='namaws[]' value="+n['nama']+">"+
																"<input type='hidden' name='lokasiws[]' value="+n['lokasi']+">"+
														"</td>"+
														"<td>"+(i+1)+"</td>"+
														"<td>"+n['nama']+"</td>"+
														"<td>"+n['lokasi']+"</td>"+
														"</tr>";

										$("#daftar_pil_ws tbody").append(isi);				

									});	
								}	

							}


	})

}

function cek_jabatan(){

	var status = $("#posisi").find("option:selected").data("ws");

    if(status == 1){

    	$("#daftar-ws").show(400);
    	$("#st_buka_daftar").val("1");

    }else{

    	$("#daftar-ws tbody tr").remove();
    	$("#daftar-ws").hide(400);
    	$("#st_buka_daftar").val("0");

    }

}