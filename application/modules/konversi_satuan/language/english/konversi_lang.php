<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ============================
$lang['konversi_title_manage']		= 'Data Konversi Satuan';
$lang['konversi_title_new']			= 'Data Satuan Baru';
$lang['konversi_title_edit']		= 'Edit Data Satuan';

// from/table
$lang['konversi_name']				= 'Nama Konversi';
$lang['jml_besar']					= 'Jumlah Besar';
$lang['satuan_besar']				= 'Satuan Besar';
$lang['jml_kecil']					= 'Jumlah Kecil';
$lang['id_satuan_terkecil']			= 'Satuan Terkecil';
$lang['st_tipe']					= 'Tipe';
$lang['stn_barang']					= 'Satuan Barang';
$lang['stn_panjang']				= 'Satuan Panjang';
$lang['stn_berat']					= 'Satuan Berat';


//button
$lang['konversi_btn_new']			= 'Baru';
$lang['konversi_btn_delete']		= 'Hapus';
$lang['konversi_btn_save']			= 'Simpan';
$lang['konversi_btn_cancel']		= 'Batal';

//messages
$lang['konversi_del_error']			= 'Anda belum memilih konversi satuan yang akan dihapus.';
$lang['konversi_del_failure']		= 'Tidak dapat menghapus konversi satuan';
$lang['konversi_delete_confirm']	= 'Apakah anda yakin akan menghapus data konversi satuan terpilih?';
$lang['konversi_deleted']			= 'Konversi satuan berhasil dihaspus';
$lang['konversi_no_records_found']= 'Data tidak ditemukan.';

$lang['konversi_create_failure']	= 'Konversi satuan baru gagal disimpan';
$lang['konversi_create_success']	= 'Konversi satuan baru berhasil disimpan';

$lang['konversi_edit_success']		= 'Data konversi satuan berhasil disimpan';
$lang['konversi_invalid_id']		= 'ID tidak Valid';

