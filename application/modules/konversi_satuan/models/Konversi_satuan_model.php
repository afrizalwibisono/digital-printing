<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 * 
 * This is model class for table "Konversi Satuan"
 */

class Konversi_satuan_model extends BF_Model
{

    /**
     * @var string  User Table Name
     */
    protected $table_name = 'konversi_satuan';
    protected $key        = 'id_konversi';

    /**
     * @var bool Set the created time automatically on a new record (if true)
     */
    protected $set_created = true;

    /**
     * @var bool Set the modified time automatically on editing a record (if true)
     */
    protected $set_modified = false;
    /**
     * @var string The type of date/time field used for $created_field and $modified_field.
     * Valid values are 'int', 'datetime', 'date'.
     */
    /**
     * @var bool Enable/Disable soft deletes.
     * If false, the delete() method will perform a delete of that row.
     * If true, the value in $deleted_field will be set to 1.
     */
    protected $soft_deletes = true;

    protected $date_format = 'datetime';

    /**
     * @var bool If true, will log user id in $created_by_field, $modified_by_field,
     * and $deleted_by_field.
     */
    protected $log_user = true;

    /**
     * Function construct used to load some library, do some actions, etc.
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * [Menghitung berapa banyak item ke dalam satuan terkecil]
     * @param  [integer] $id_konversi [acuan id konversi yang akan dihitung]
     * @param  [integer] $qty         [qty item yang akan dihitung]
     * @return [array]                [array('qty' => '', 'satuan' => '')]
     */
    public function hitung_ke_satuan_kecil($id_konversi = 1, $qty = 0)
    {
        $satuan = $this->select("konversi_satuan.*, satuan_terkecil.alias")
                        ->join('satuan_terkecil', 'konversi_satuan.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil')
                        ->find_by(array('konversi_satuan.id_konversi' => $id_konversi, 'konversi_satuan.deleted' => 0));
        if(!$satuan)
        {
            $return = array('qty' => 0, 'satuan' => '', 'id_satuan_terkecil' => 0, 'id_konversi_kecil'=> 0);
            return $return;
        }

        $real_qty = ($qty / $satuan->jml_besar) * $satuan->jml_kecil;

        //Get ID Konversi Kecil
        $konversi = $this->where("jml_kecil = jml_besar")
                        ->find_by(array('id_satuan_terkecil' => $satuan->id_satuan_terkecil));

        $return = array('qty' => $real_qty, 'satuan' => $satuan->alias, 'id_satuan_terkecil' => $satuan->id_satuan_terkecil, 'id_konversi_kecil' => $konversi->id_konversi);

        return $return;
    }

    /**
     * [Menghitung berapa banyak item ke dalam satuan olah/satuan besar]
     * @param  [integer] $id_konversi [acuan id konversi yang akan dihitung]
     * @param  [integer] $qty         [qty item yang akan dihitung]
     * @return [array]                [array('qty' => '', 'satuan' => '')]
     */
    public function hitung_ke_satuan_besar($id_konversi = 1, $qty = 0)
    {
        $satuan = $this->select("konversi_satuan.*")
                        ->find_by(array('konversi_satuan.id_konversi' => $id_konversi, 'konversi_satuan.deleted' => 0));
        if(!$satuan)
        {
            $return = array('qty' => 0, 'satuan' => '');
            return $return;
        }

        $qty_konversi = ($qty / $satuan->jml_kecil) * $satuan->jml_besar;

        $return = array('qty' => $qty_konversi, 'satuan' => $satuan->satuan_besar);

        return $return;
    }

    /**
     * [Fungsi ini akan menghasilkan list konversi berdasarkan satuan terkecil]
     * @param  [integer] $id_satuan  [id satuan terkecil]
     * @param  [integer] $id_konversi [jika diisi maka akan terpilih sebagai "selected" sesuai dengan id_konversi yang dimasukkan]
     * @param  [integer] $auto_select [jika diisi maka akan terpilih sebagai "selected" sesuai dengan satuan terkecil barang]
     * @return [array]
     */
    public function get_konversi($id_satuan = 1, $id_konversi = "", $auto_select = TRUE)
    {
        if($id_konversi)
        {
            $selected = "if(konversi_satuan.id_konversi = $id_konversi, 1, 0) as selected";
        }
        else
        {
            if($auto_select){
                $selected = "if((jml_kecil = jml_besar) AND (konversi_satuan.id_satuan_terkecil = barang.id_satuan_terkecil), 1, 0) as selected";
            }else{
                $selected = "0 as selected";
            }
        }

        $satuan = $this->select(array("konversi_satuan.st_tipe","konversi_satuan.id_konversi","satuan_besar", "jml_kecil", "jml_besar", "alias as satuan", "$selected", "concat(jml_kecil,' ', alias) as tampil", "concat(satuan_besar, ' (', jml_kecil, ' ', alias,')') as tampil2"))
                        ->distinct()
                        ->join('satuan_terkecil', 'konversi_satuan.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil');
                        
                        if($auto_select){
                            $satuan = $satuan->join('barang', 'barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil');
                        }
                        
                        $satuan = $satuan->where(['konversi_satuan.id_satuan_terkecil' => $id_satuan, 'konversi_satuan.deleted' => 0])
                        ->order_by("satuan_besar","ASC")
                        ->find_all();
        return $satuan;
    }
    
    /**
     * [Fungsi ini digunakan untuk mendapatkan data konversi satuan berdasarkan tipe konversi]
     * @param  [type] $tipe        [0 = satuan barang [pcs, roll, biji box ] 
     *                              1 = panjang [ cm, m, kaki ] 
     *                              2 = berat [kg , ons, gram]]
     * @param  [type] $id_konversi [optional, jika diisi maka data yang 'selected' sama dengan 1 berarti itu yang 
     *                              terpilih (di html tag select)]
     * @return [array]             [Return dari fungsi ini berupa array]
     */
    public function get_konversi_by_tipe($tipe = NULL, $id_konversi = NULL){
        if($id_konversi)
        {
            $selected = "if(konversi_satuan.id_konversi = $id_konversi, 1, 0) as selected";
        }else{
            $selected = "0 as selected";
        }
        
        $satuan = $this->select(array("konversi_satuan.st_tipe","konversi_satuan.id_konversi","satuan_besar", "jml_kecil", "jml_besar", "alias as satuan", "$selected", "concat(jml_kecil,' ', alias) as tampil", "concat(satuan_besar, ' (', jml_kecil, ' ', alias,')') as tampil2"))
                        ->distinct()
                        ->join('satuan_terkecil', 'konversi_satuan.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil')
                        ->where(['konversi_satuan.st_tipe' => $tipe, 'konversi_satuan.deleted' => 0])
                        ->order_by("satuan_besar","ASC")
                        ->find_all();
        if(!$satuan){
            return array();
        }

        return $satuan;
    }

    /**
     * [Fungsi ini akan menghasilkan option untuk select element berdasarkan satuan terkecil]
     * @param  [integer] $id_satuan  [id satuan terkecil]
     * @param  [integer] $format  [1 = tidak ada satuan kecil disamping satuan, 2 = ada satuan kecil disamping satuan]
     * @return [array]
     */
    public function get_konversi_select($id_satuan = 1, $format = 1, $auto_select = TRUE)
    {
        switch ($format) {
            case 1:
                $arrFormat = array("concat('<option value=',konversi_satuan.id_konversi,' ',if((jml_kecil = jml_besar) AND (konversi_satuan.id_satuan_terkecil = barang.id_satuan_terkecil), 'selected', ''),'>',satuan_besar,'</option>') as option_select");
                break;
            case 2:
                $arrFormat = array("concat('<option value=',konversi_satuan.id_konversi,' ',if((jml_kecil = jml_besar) AND (konversi_satuan.id_satuan_terkecil = barang.id_satuan_terkecil), 'selected', ''),'>',satuan_besar,'(',jml_kecil,' ',alias,')','</option>') as option_select");
                break;
            default:
                $arrFormat = array("concat('<option value=',konversi_satuan.id_konversi,' ',if((jml_kecil = jml_besar) AND (konversi_satuan.id_satuan_terkecil = barang.id_satuan_terkecil), 'selected', ''),'>',satuan_besar,'</option>') as option_select");
                break;
        }

        $satuan = $this->select($arrFormat)
                        ->distinct()
                        ->join('satuan_terkecil', 'konversi_satuan.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil');
                        if($auto_select){
                            $satuan = $satuan->join('barang', 'barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil');
                        }

                        $satuan = $satuan->where('konversi_satuan.id_satuan_terkecil', $id_satuan)
                        ->order_by("satuan_besar","ASC")
                        ->find_all();
        if($satuan)
        {
            $options = "";
            foreach ($satuan as $key => $val) {
                $options .= $val->option_select;
            }

            return $options;    
        }
        
        return '';
    }
}
