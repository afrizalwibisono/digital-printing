<div class="box box-primary">
    <!--form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_konversi_satuan','name'=>'frm_konversi_satuan','role','class'=>'form-horizontal'))?>
    <div class="box-body">
        <div class="form-group <?= form_error('jml_besar') ? 'has-error' : '';?>">
            <label for="jml_besar" class="col-sm-2 control-label"><?= lang('jml_besar') ?></label>
            <div class="col-sm-1">
                <input type="text" class="form-control" id="jml_besar" name="jml_besar" maxlength="11" value="<?php echo set_value('jml_besar', isset($data->jml_besar)?$data->jml_besar:''); ?>" required autofocus numeric></input>
            </div>
        </div>
        <div class="form-group <?= form_error('satuan_besar') ? 'has-error' : '';?>">
            <label for="satuan_besar" class="col-sm-2 control-label"><?= lang('satuan_besar') ?></label>
            <div class="col-sm-3">
                <input type="text" class="form-control" id="satuan_besar" name="satuan_besar" maxlength="50" value="<?php echo set_value('satuan_besar', isset($data->satuan_besar)?$data->satuan_besar:''); ?>"></input>
            </div>
        </div>
        <div class="form-group <?= form_error('jml_kecil') ? 'has-error' : '';?>">
            <label for="jml_kecil" class="col-sm-2 control-label"><?= lang('jml_kecil') ?></label>
            <div class="col-sm-2">
                <input type="text" class="form-control" id="jml_kecil" name="jml_kecil" maxlength="50" value="<?php echo set_value('jml_kecil', isset($data->jml_kecil)?$data->jml_kecil:''); ?>"></input>
            </div>
        </div>
        <div class="form-group <?= form_error('id_satuan_terkecil') ? ' has-error' : ''; ?>">
            <label for="id_satuan_terkecil" class="col-sm-2 control-label"><?= lang('id_satuan_terkecil') ?></label>
            <div class="col-sm-2">
                <select class = "form-control" id="id_satuan_terkecil" name="id_satuan_terkecil">
                    <option value=""> -- Pilih -- </option>
                    <?php foreach ($results as $record) : ?>
                    
                    <option value="<?php echo $record->id_satuan_terkecil;?>" <?php echo  set_select('id_satuan_terkecil', $record->id_satuan_terkecil, isset($data->id_satuan_terkecil) && $data->id_satuan_terkecil == $record->id_satuan_terkecil); ?>> <?php echo $record->alias;?> </option>
                    
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group <?= form_error('st_tipe') ? ' has-error' : ''; ?>">
            <label for="st_tipe" class="col-sm-2 control-label"><?= lang('st_tipe') ?></label>
            <div class="col-sm-2">
                <select class = "form-control" id="st_tipe" name="st_tipe">
                    <option value=""> -- Pilih -- </option>
                    <option value="0" <?php echo  set_select('st_tipe', 0, isset($data->st_tipe) && $data->st_tipe == 0); ?>> <?= lang('stn_barang') ?></option>
                    <option value="1" <?php echo  set_select('st_tipe', 1, isset($data->st_tipe) && $data->st_tipe == 1); ?>> <?= lang('stn_panjang') ?></option>
                    <option value="2" <?php echo  set_select('st_tipe', 2, isset($data->st_tipe) && $data->st_tipe == 2); ?>> <?= lang('stn_berat') ?></option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" name="save" class="btn btn-primary"><?= lang('konversi_btn_save') ?></button>
                <?php
                echo lang('bf_or') . ' ' . anchor('konversi_satuan', lang('konversi_btn_cancel'));
                ?>
            </div>
        </div>
    </div>
    <?= form_close() ?>
</div>