<?php
	$ENABLE_ADD		= has_permission('Konversi Satuan.Add');
	$ENABLE_MANAGE	= has_permission('Konversi Satuan.Manage');
	$ENABLE_DELETE	= has_permission('Konversi Satuan.Delete');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_konversi_satuan','name'=>'frm_konversi_satuan')) ?>
	<div class="box-header">
		<div class="form-group">
			<?php if ($ENABLE_ADD): ?>
			<a href="<?= site_url('konversi_satuan/create') ?>" class="btn btn-success" title="<?= lang('konversi_btn_new') ?>"><?= lang('konversi_btn_new') ?></a>
			<?php endif;?>
			<div class="pull-right">
				<div class="input-group">
					<input type="text" name="table_search" value="<?php echo isset($search) ? $search:''; ?>" class="form-control pull-right" placeholder="Search" autofocus>
					<div class="input-group-btn">
						<button class="btn btn-default"><i class="fa fa-search"></i></button>
					</div>
				</div>	
			</div>
		</div>
	</div>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
	<div class="box-body table-responsive no-padding">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
                        <th width="50">#</th>
                        <th><?= lang('jml_besar') ?></th>
                        <th><?= lang('satuan_besar') ?></th>
                        <th><?= lang('jml_kecil') ?></th>
                        <th><?= lang('id_satuan_terkecil') ?></th>
                        <th><?= lang('st_tipe') ?></th>
                        <?php if($ENABLE_MANAGE) : ?>
                        <th width="25"></th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($results as $record) : ?>
                    <tr>
                        <td class="column-check">
                        	<?php if($record->st_fix == 0) : ?>
                        	<input type="checkbox" name="checked[]" value="<?= $record->id_konversi ?>" />
                        	<?php endif; ?>
                        </td>
                        <td><?= $numb; ?></td>
                        <td><?= number_format($record->jml_besar) ?></td>
                        <td><?= strtoupper($record->satuan_besar) ?></td>
                        <td><?= number_format($record->jml_kecil) ?></td>
                        <td><?= strtoupper($record->alias) ?></td>
                        <td><?php 
                            switch ($record->st_tipe) {
                                case 0:
                                    $tipe = lang('stn_barang');
                                    break;
                                case 1:
                                    $tipe = lang('stn_panjang');
                                    break;
                                case 2:
                                    $tipe = lang('stn_berat');
                                    break;
                                default:
                                    $tipe = '-';
                                    break;
                            }

                            echo $tipe;
                         ?></td>
                        <?php if($ENABLE_MANAGE) : ?>
                        <td style="padding-right:20px">
                            <?php if($record->st_fix == 0) : ?>
                            <a class="text-black" href="<?= site_url('konversi_satuan/edit/' . $record->id_konversi); ?>" data-toggle="tooltip" data-placement="left" title="Edit Data"><i class="fa fa-pencil"></i></a>
                            <?php endif; ?>
                        </td>
                        <?php endif; ?>
                    </tr>
                    <?php $numb++; endforeach; ?>
                </tbody>
	  </table>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">
		<?php if($ENABLE_DELETE) : ?>
		<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('konversi_btn_delete') ?>" onclick="return confirm('<?= lang('konversi_delete_confirm'); ?>')">
		<?php endif;
		echo $this->pagination->create_links(); 
		?>
	</div>
	<?php else: ?>
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('konversi_no_records_found') ?></p>
    </div>
    <?php
	endif;
	echo form_close(); 
	?>	
</div><!-- /.box-->