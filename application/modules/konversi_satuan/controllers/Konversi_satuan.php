<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 * 
 * This is controller for Konversi Satuan

 */

class Konversi_satuan extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Konversi Satuan.View";
    protected $addPermission    = "Konversi Satuan.Add";
    protected $managePermission = "Konversi Satuan.Manage";
    protected $deletePermission = "Konversi Satuan.Delete";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('konversi_satuan/konversi');
        $this->load->model(array(
                                'konversi_satuan_model',
                                'satuan_terkecil_model'
                            ));

        $this->template->title(lang('konversi_title_manage'));
		$this->template->page_icon('fa fa-table');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if (isset($_POST['delete']) && has_permission($this->deletePermission))
        {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                foreach ($checked as $pid)
                {
                    $satuan_besar   = $this->konversi_satuan_model->find($pid)->satuan_besar;
                    $result         = $this->konversi_satuan_model->delete_where(array('id_konversi' => $pid, 'st_fix' => 0));

                    if($result)
                    {
                        $keterangan = "SUKSES, hapus data konversi satuan ".$satuan_besar.", dengan ID : ".$pid;
                        $status     = 1;
                    }
                    else
                    {
                        $keterangan = "GAGAL, hapus data konversi satuan ".$satuan_besar.", dengan ID : ".$pid;
                        $status     = 0;
                    }

                    $nm_hak_akses   = $this->deletePermission; 
                    $kode_universal = $pid;
                    $jumlah         = 1;
                    $sql            = $this->db->last_query();

                    simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                }

                if ($result)
                {
                    $this->template->set_message(count($checked) .' '. lang('konversi_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('konversi_del_failure') . $this->konversi_satuan_model->error, 'error');
                }
            }
            else
            {
                $this->template->set_message(lang('konversi_del_error'), 'error');
            }
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
        }

        $filter = "";
        if($search!="")
        {
            $filter = "?search=".$search;
        }

        $search2 = $this->db->escape_str($search);
        
        $where = "`konversi_satuan.deleted` = 0
                AND (`satuan_besar` LIKE '%$search2%' ESCAPE '!'
                OR `nm_satuan` LIKE '%$search2%' ESCAPE '!'
                OR `alias` LIKE '%$search2%' ESCAPE '!')";
        
        $total = $this->konversi_satuan_model
                    ->join('satuan_terkecil',"konversi_satuan.id_satuan_terkecil=satuan_terkecil.id_satuan_terkecil","left")
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->konversi_satuan_model
                    ->join('satuan_terkecil',"konversi_satuan.id_satuan_terkecil=satuan_terkecil.id_satuan_terkecil","left")
                    ->where($where)
                    ->order_by('satuan_besar','ASC')
                    ->limit($limit, $offset)->find_all();
        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->title(lang('konversi_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

   	//Create New konversi satuan
   	public function create()
   	{

        $this->auth->restrict($this->addPermission);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_konversi_satuan())
            {
              $this->template->set_message(lang("konversi_create_success"), 'success');
              redirect('konversi_satuan');
            }
        }

        $assets = array('js/plugins/number/jquery.number.js',
        				          'konversi_satuan/assets/js/form_konversi.js'
                        );
        
        add_assets($assets);

        $data = $this->satuan_terkecil_model->find_all();

        $this->template->set('results', $data);
        $this->template->title(lang('konversi_title_new'));
		$this->template->render('konversi_satuan_form');
   	}

   	//Edit konversi satuan
   	public function edit()
   	{
  		$this->auth->restrict($this->managePermission);
                
        $id = (int)$this->uri->segment(3);

        if (empty($id))
        {
            $this->template->set_message(lang("konversi_invalid_id"), 'error');
            redirect('konversi_satuan');
        }

        if (isset($_POST['save']))
        {
            if ($this->save_konversi_satuan('update', $id))
            {
                $this->template->set_message(lang("konversi_edit_success"), 'success');
            }

        }

        $assets = array('js/plugins/number/jquery.number.js',
                        'konversi_satuan/assets/js/form_konversi.js'
                        );
        
        add_assets($assets);
        
        $data  = $this->konversi_satuan_model->find_by(array('id_konversi' => $id, 'deleted' => 0));

        if(!$data)
        {
            $this->template->set_message(lang("konversi_invalid_id"), 'error');
            redirect('konversi_satuan');
        }

        $data1 = $this->satuan_terkecil_model->find_all();
        
        $this->template->set('data', $data);
        $this->template->set('results', $data1);
        $this->template->title(lang("konversi_title_edit"));
        $this->template->render('konversi_satuan_form');
   	}

    public function cek_satuan_terkecil($val){
        return ($val=="")? FALSE : TRUE;
    }

    public function cek_tipe($val){
        return $val == "" ? FALSE : TRUE;
    }

   	protected function save_konversi_satuan($type='insert', $id=0)
   	{
        //Remove , from post
        $_POST['jml_besar'] = floatval(remove_comma($_POST['jml_besar']));
        $_POST['jml_kecil'] = floatval(remove_comma($_POST['jml_kecil']));
        // form Validation
        $this->form_validation->set_rules('jml_besar','lang:jml_besar','required|numeric');
        $this->form_validation->set_rules('satuan_besar','lang:jml_besar','required|trim');
        $this->form_validation->set_rules('jml_kecil','lang:jml_kecil','required|numeric');
        $this->form_validation->set_rules('id_satuan_terkecil','lang:id_satuan_terkecil','callback_cek_satuan_terkecil');
        $this->form_validation->set_rules('st_tipe','lang:st_tipe','callback_cek_tipe');
        $this->form_validation->set_message('cek_satuan_terkecil','Pilih satuan dahulu.');
        $this->form_validation->set_message('cek_tipe','Pilih tipe dahulu.');

        if ($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        unset($_POST['save']);

        if ($type == 'insert')
        {
            $id = $this->konversi_satuan_model->insert($_POST);

            if (is_numeric($id))
            {
                //Save Log
                if($id)
                {
                    $keterangan = "SUKSES, tambah data konversi satuan ".$this->input->post('satuan_besar').", dengan ID : ".$id;
                    $status     = 1;
                }
                else
                {
                    $keterangan = "GAGAL, tambah data konversi satuan".$this->input->post('satuan_besar').", dengan ID : ".$id;
                    $status     = 0;
                }

                $nm_hak_akses   = $this->addPermission; 
                $kode_universal = $id;
                $jumlah         = 1;
                $sql            = $this->db->last_query();
                //menyimpan ke Log histori
                simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

                $return = TRUE;
            }
            else
            {
                $return = FALSE;
            }
        }
        else if ($type == 'update')
        {
            $return = $this->konversi_satuan_model->update($id, $_POST);

            if($return)
            {
                $keterangan = "SUKSES, ubah data konversi satuan ".$this->input->post('satuan_besar').", dengan ID : ".$id;
                $status     = 1;
            }
            else
            {
                $keterangan = "GAGAL, ubah data konversi satuan ".$this->input->post('satuan_besar').", dengan ID : ".$id;
                $status     = 0;
            }

            //Save Log
            $nm_hak_akses   = $this->managePermission; 
            $kode_universal = $id;
            $jumlah         = 1;
            $sql            = $this->db->last_query();
            //menyimpan ke log historis
            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
        }

        return $return;
   	}

}
?>