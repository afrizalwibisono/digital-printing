<div class="box box-primary">

	<?php echo form_open($this->uri->uri_string(),[
													'name'		=> 'form_app_po',
													'id'		=> 'form_app_po',
													'role'		=> 'form',
													'class'		=> 'form-inline'
													]) ?>
	<div class="box-header">

		<a href="<?= site_url('approve_order_delete/history') ?>" target="_blank" class="btn btn-default" id="tbl_history">
			<i class="fa fa-book"></i> <?= lang('btn-history') ?>
		</a>	

		<div class="pull-right">			
			<div class="form-group">
                <div class="input-daterange input-group" title="<?= lang('range_tgl') ?>" data-toggle="tooltip" data-placement="top">
                    <input type="text" style="width: 120px" class="form-control" name="tgl1" id="tgl1" value="<?= isset($filter['tgl1']) ? $filter['tgl1'] : '' ?>" placeholder="<?= lang('tgl_awal') ?>" readonly />
                    <span class="input-group-addon">to</span>
                    <input type="text" style="width: 120px" class="form-control" name="tgl2" id="tgl2" value="<?= isset($filter['tgl2']) ? $filter['tgl2'] : '' ?>" placeholder="<?= lang('tgl_akhir') ?>" readonly />
                </div>
            </div>

            <div class="form-group">
                <select name="gol_konsumen" id="gol_konsumen" class="form-control">
                    <option value="" <?= set_select('gol_konsumen','',isset($filter['gol_kons']) && $filter['gol_kons'] == '') ?> ><?= lang("lbl_isi_kategori_all") ?></option>
                    <?php 
						if(isset($dt_gol) && is_array($dt_gol)): 
							foreach ($dt_gol as $key => $gol) :
						
					?>

					<option value="<?= $gol->id ?>" <?= isset($filter['gol_kons']) && $filter['gol_kons'] != "" ? 'selected' : '' ?> ><?= $gol->nm ?></option>

					<?php 
							endforeach;
						endif; 
					?>
                </select>
            </div>

            <div class="form-group">
                <input type="hidden" name="id_konsumen_pilih" id="id_konsumen_pilih" value="<?= set_value('konsumen', isset($filter['konsumen']) ? $filter['konsumen'] : '') ?>">
                <select name="konsumen" id="konsumen" class="form-control" style="min-width: 300px !important" >
                    <option value=""></option>
                </select>
            </div>

            <button type="submit" name='simpan' class="btn btn-primary">
            	<span class="fa fa-search"></span> <?= lang("btn-filter") ?>
            </button>

		</div>

	</div>

	<div class="alert alert-warning">
		<span class="fa fa-warning"></span> &nbsp; <?= lang("label-informasi") ?>
	</div>

	<?php 

			if(isset($data) && is_array($data) && count($data)):

	?>

	<div class="box-body table-responsive">	

		<table class="table table-hover table-striped">
			<thead>
				<tr class="success">
					<th class="text-center" width="30">#</th>
					<th class="text-center"><?= lang("jdl-table-index-tgl") ?></th>
					<th class="text-center"><?= lang("jdl-table-index-gol-konsumen") ?></th>
					<th class="text-center"><?= lang("jdl-table-index-konsumen") ?></th>
					<th class="text-center"><?= lang("jdl-table-index-total") ?></th>
					<th class="text-center"><?= lang("jdl-table-index-st-delete") ?></th>
					<th class="text-center" width="50"></th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($data as $key => $isi) :

						$class 	= '';

						if($isi->st_acc == 1){

							$class 	= "class = 'bg-red'";

						}

				?>
				<tr <?= $class ?> >
					<td class="text-right">
						<?= $key+1 ?>
						<input type="hidden" name="dft_id[]" id="dft_id" value="<?= $isi->id_order ?>">						
					</td>		
					<td class="text-center"><?= $isi->tgl_order ?></td>
					<td class="text-center"><?= $isi->st_konsumen ?></td>
					<td class="text-center"><?= $isi->panggilan.' '.$isi->nama ?></td>
					<td class="text-center"><?= number_format($isi->total_value_order) ?></td>
					<td class="text-center">
						<input type="hidden" name="dft_isi_st_acc[]" id="dft_isi_st_acc" value="<?= $isi->st_acc ?>">
						<select class="form-control input-sm" name="dft_st_acc[]" id="dft_st_acc" onchange="perubahan_status(this)" >
							<option value="0" <?= $isi->st_acc == 0 ? 'selected' : ''  ?> ><?= lang("isi-stapp-delete-no") ?></option>
							<option value="1" <?= $isi->st_acc == 1 ? 'selected' : ''  ?> ><?= lang("isi-stapp-delete-yes") ?></option>
						</select>
					</td>
					<td>
						<a href="<?= site_url('order_produk/view/' . $isi->id_order); ?>" target="_blank" class="text-black">
							<span class="fa fa-folder"></span>
						</a>
					</td>
				</tr>
				<?php
					endforeach;
				?>
			</tbody>
			
		</table>

	</div>
	<?php else: ?>

	<div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('konfirmasi-data-tidak-ada') ?></p>
    </div>

	<?php endif ?>

	<?php echo form_close(); ?>

</div>