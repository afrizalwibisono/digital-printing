$(function(){
	//Gudang pada form
	$("#id_gudang").select2({ 
		placeholder : "-- Pilih Gudang --",
		allowClear : true
	});
	//Jenis Barang pada form
	$("#id_jenis").select2({ 
		placeholder : "-- Pilih Jenis --",
		allowClear : true
	});
	// Kategori Barang
	$("#id_kategori").select2({ 
		placeholder : "-- Pilih Kategori --",
		allowClear : true
	});
	// Merek Barang
	$("#id_merek").select2({ 
		placeholder : "-- Pilih Merek --",
		allowClear : true
	});
	//Date Picker
	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	hitung_jml_konversi();

	//Hitung Konversi
	$("select[name='konversi[]']").on("change", function(){
		var stok = parseFloat($(this).closest("tr").find("input[name='stok[]']").val());

		if(isNaN(stok)){
			stok = 0;
		}

		var jml_kecil = $(this).find(":selected").data("jml-kecil");
		var jml_besar = $(this).find(":selected").data("jml-besar");
		var tipe	  = $(this).find(":selected").data("tipe");
		if (tipe==1) {
			jml_konversi = (stok/Math.pow(jml_kecil,2)) * jml_besar;
		}else{
			jml_konversi = (stok/jml_kecil) * jml_besar;
		}
		
		if(isFloat(jml_konversi)){
			$(this).closest("tr").find("label.lbl-stok").text($.number(jml_konversi, 1));
		}else{
			$(this).closest("tr").find("label.lbl-stok").text($.number(jml_konversi, 0));
		}
	});
});

function hitung_jml_konversi(){
	$.each($("#tbarang tbody tr"), function(i){
		var stok = parseFloat($(this).find("input[name='stok[]']").val());

		if(isNaN(stok)){
			stok = 0;
		}

		var jml_kecil = $(this).find("select option:selected").data("jml-kecil");
		var jml_besar = $(this).find("select option:selected").data("jml-besar");
		var tipe	  = $(this).find("select option:selected").data("tipe");
		if (tipe==1) {
			jml_konversi = (stok/Math.pow(jml_kecil,2)) * jml_besar;
		}else{
			jml_konversi = (stok/jml_kecil) * jml_besar;
		}
		// jml_konversi = (stok/jml_kecil) * jml_besar;

		if(isFloat(jml_konversi)){
			$(this).find("label.lbl-stok").text($.number(jml_konversi, 1));
		}else{
			$(this).find("label.lbl-stok").text($.number(jml_konversi, 0));
		}
	});
}

function cetak_halaman(){
	window.print();
}