<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Suwito
 * @copyright Copyright (c) 2018, Suwito
 * 
 * This is controller for Authentication
 */

class Users extends Front_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */

    public function __construct()
    {
    	parent::__construct();
    	$this->load->model(array(
                                'users/users_model',
                                'users/groups_model',
                                'users/user_groups_model'
                            ));
    	$this->load->library('users/auth');
        $this->lang->load('users/users');
    }

    public function index()
    {
        redirect('users/setting');
    }

    public function login()
    {
        if($this->auth->is_login())
        {
            redirect('/');
        }

    	$username = $this->input->post('username');
        $password = $this->input->post('password');
        $remember = $this->input->post('remember');

        if(isset($_POST['login']))
        {
            $this->auth->login($username, $password, $remember);
        }

        $this->template->set_theme('default');
        $this->template->set_layout('login');
        $this->template->title('Login');
    	$this->template->render('login');
    }

    public function register()
    {
        if($this->auth->is_login())
        {
            redirect('/');
        }

        if(isset($_POST['register']))
        {
            $result = $this->save_register();
            if($result)
            {
                $this->template->set_message(lang('users_register_success'), 'success');
                redirect('login');
            }
        }

        $assets = ['users/assets/js/register.js'];

        add_assets($assets);

        $this->template->set_theme('default');
        $this->template->set_layout('login');
        $this->template->title(lang('users_register_title'));
        $this->template->render('register');
    }

    protected function save_register()
    {
        $this->form_validation->set_rules('full_name', 'lang:users_nm_lengkap','trim|required|max_length[100]');
        $this->form_validation->set_rules('username', 'lang:users_username','trim|required|callback_check_register_username|max_length[45]');
        $this->form_validation->set_rules('email_address', 'lang:users_email','trim|required|callback_check_register_email|max_length[100]');
        $this->form_validation->set_rules('phone', 'lang:users_hp','trim|required');
        $this->form_validation->set_rules('agree', 'lang:users_register_agree','callback_cek_agree');

        if($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        $full_name        = $this->input->post('full_name');
        $username         = $this->input->post('username');
        $email_address    = $this->input->post('email_address');
        $phone            = $this->input->post('phone');

        $new_user = $this->auth->register($username, $email_address, $full_name, $phone);    

        return $new_user;
    }

    public function cek_agree($val){
        $this->form_validation->set_message('cek_agree', lang('users_register_agree_message'));
        return isset($val) ? TRUE : FALSE;
    }

    public function check_register_username($val)
    {
        $this->form_validation->set_message('check_register_username', lang('users_username_already_taken'));

        $check = $this->users_model->find_by('username', $val);
        if($check)
        {
            return FALSE;
        }

        return TRUE;
    }

    public function check_register_email($val)
    {
        $this->form_validation->set_message('check_register_email', lang('users_email_already_taken'));

        $check = $this->users_model->find_by('email', $val);
        if($check)
        {
            return FALSE;
        }

        return TRUE;
    }

    public function ajax_login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $remember = $this->input->post('remember');

        $this->form_validation->set_rules('username', 'lang:users_email','trim|required|max_length[100]');
        $this->form_validation->set_rules('password', 'lang:users_password','trim|required|max_length[100]');

        if($this->form_validation->run($this) === FALSE)
        {
            echo json_encode(array('type' => 'error', 'msg' => validation_errors()));
            die();
        }

        if(isset($_POST['login']))
        {
            echo $this->auth->ajax_login($username, $password, $remember);
        }
    }

    public function logout()
    {
    	$this->auth->logout();
    }

    public function forgot_password()
    {
        $email = $this->input->post('reset_email');

        if($email)
        {
            $reset_link = $this->auth->get_reset_link($email);
            if($reset_link)
            {
                //send reset link to email
                $this->load->library('email');
                $sender  = app_get_setting('email.sender');
                $subject = 'Forgot Password';
                $to      = $email;
                $message = "Dear {$reset_link['fullname']},<br>
                You recently requested to reset your password on ".base_url()."<br>
                Click this link below to reset your password:<br>
                <a href='".$reset_link['link']."'>{$reset_link['link']}</a>
                <br>
                <br>
                If you do not request a password reset, please ignore this email.<br>
                This password reset is only valid for the next 24 hours.
                <br>
                <br>
                Thanks.
                ";

                $message_body  = $this->load->view('users/email/_header.php', '', TRUE);
                $message_body .= $message;
                $message_body .= $this->load->view('users/email/_footer.php', '', TRUE);

                $this->email->from($sender, 'Digital Printing');
                $this->email->to($to);

                $this->email->subject($subject);
                $this->email->message($message_body);

                $reset = $this->email->send(); 
                if($reset)
                {
                    $this->template->set_message(lang('users_reset_link_sent'),'success');
                }
                else
                {
                    $this->template->set_message(lang('users_email_send_failed'),'error');
                }  
            }
            else
            {
                $this->template->set_message(lang('users_email_not_found'),'error');   
            }
        }

        $this->template->title('Forgot Password');
        $this->template->render('forgot_password');
    }

    public function reset_password($token = NULL)
    {
        if(isset($_POST['save']))
        {
            $this->form_validation->set_rules('new_password','New Password','required|trim|alpha_numeric');
            $this->form_validation->set_rules('confirm_new_password','Confirm New Password','required|trim|alpha_numeric|matches[new_password]');
            if($this->form_validation->run() === FALSE)
            {
                $this->template->set_message(validation_errors(),'error');
            }
            else
            {
                $dt_token       = explode("x", $token);
                $id_user        = $dt_token[0];
                $new_password   = $this->input->post('new_password');

                $reset = $this->auth->reset_password($id_user, $dt_token[1], $new_password);
                if($reset)
                {
                    $this->template->set_message(lang('users_reset_password_success'), 'success');
                }
                else
                {
                    $this->template->set_message(lang('users_reset_password_failed'), 'success');    
                }

                redirect('/');
            }
        }

        $this->template->title('Set New Password');
        $this->template->render('reset_password');
    }
}