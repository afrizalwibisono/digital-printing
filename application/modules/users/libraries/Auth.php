<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This library for authentication user 
 */
class Auth
{
	protected $ci;
	protected $user;
    /** @var string The name of the db table containing user tokens. */
    protected $cookiesTable = 'user_cookies';
    /** @var string The name of the cookie index holding autologin data. */
    protected $autoLoginIndex = 'autologin';
    /** @var string The separator used in the autologin data. */
    protected $autoLoginSeparator = '~';
    /**
     * @var string The date format used for users.last_login, login_attempts.time,
     * and user_cookies.created_on. Passed as the first argument of the PHP date()
     * function when handling any of these values.
     */
    protected $loginDateFormat = 'Y-m-d H:i:s';

	public function __construct()
	{
		$this->ci =& get_instance();
        $this->ci->load->library('session');
        $this->ci->lang->load('users/users');
		$this->ci->load->model(['users/users_model',
                                'users/groups_model',
                                'users/user_groups_model',
                                'users/konsumen_model'
                            ]);

        // Try to log the user in from session/cookie data.
        $this->autologin();

		$this->user = $this->ci->session->userdata('app_session');
	}

    public function generate_username($email_address = NULL)
    {
        if(!$email_address)
        {
            return FALSE;
        }

        $username_check = explode("@", $email_address);
        $username = $this->ci->db->escape_str($username_check[0]);

        $check = FALSE;
        $i = 0;
        while ($check !== FALSE) {
            $check = $this->ci->users_model->find_by('username', $username);
            $i++;
            if($check)
            {
                $username .= $i;
            }
        }

        return $username;
    }

	public function is_login()
	{
		return ($this->user) ? TRUE : FALSE;
	}

    public function user_id()
    {
        return $this->user['id_user'];
    }
    
    public function userdata()
    {
        if(!$this->is_login())
        {
            return NULL;
        }

        $userdata =  $this->ci->users_model->select(array("users.id_user","users.username","users.email","karyawan.idkaryawan","karyawan.nama as nm_karyawan","konsumen.idkonsumen","konsumen.nama as nm_konsumen"))
                                ->join("karyawan","users.id_user = karyawan.id_user","left")
                                ->join("konsumen","users.id_user = konsumen.id_user","left")
                                ->find($this->user_id());
        $user_groups = "";

        if($this->is_admin())
        {
            $user_groups = "Administrator";
        }
        else
        {
            $user_groups = $this->get_user_groups();
        }

        $userdata->groups = $user_groups;

        return $userdata;
    }

    public function role_id($user_id = NULL)
    {
        if(!$user_id)
        {
            $user_id = $this->user_id();
        }

        $role_data = $this->ci->user_groups_model->find_by('id_user', $user_id);
       
        if($role_data)
        {
            $role_id = $role_data->id_group;
        }
        else
        {
            $role_id = NULL;
        }

        return $role_id;
    }

    //Register
    public function register($username = '', $email_address = '', $full_name = '', $phone = '', $id_group = '')
    {
        if($username == '' || $email_address =='' || $full_name =='' || $phone == ''){
            $this->ci->template->set_message(lang('users_register_incomplete'), 'error');
            return FALSE;
        }
        //Check username
        $check = $this->ci->users_model->find_by('username', $username);
        if($check)
        {
            $this->ci->template->set_message(lang('users_username_already_taken'), 'error');
            return FALSE;
        }

        $check = $this->ci->users_model->find_by('email', $email_address);
        if($check)
        {
            $this->ci->template->set_message(lang('users_email_already_taken'), 'error');
            return FALSE;
        }

        // Generate a random string for the token.
        if (! function_exists('random_string')) {
            $this->ci->load->helper('string');
        }

        $random_string = random_string('alnum', 6);

        $cost = $this->get_server_cost();
        $options = [
                'cost' => $cost
            ];
        $password = password_hash($random_string, PASSWORD_BCRYPT, $options);

        $insert_data = array(
                        'username' => $username,
                        'password' => $password,
                        'email'    => $email_address,
                        'nm_lengkap' => $full_name,
                        'hp'       => $phone
                    );

        if(intval(app_get_setting('new.user.need.activation')) == 1)
        {
            $insert_data['st_aktif'] = 0;
        }

        $this->ci->db->trans_start();

        $new_user = $this->ci->users_model->insert($insert_data);
        
        if($new_user)
        {
            //Assign Group
            //Get Default user group
            if(!$id_group){
                $dt_group = $this->ci->groups_model->find_by(array('st_default' => 1));
                if($dt_group)
                {
                    $id_group = $dt_group->id_group;
                }
            }
            
            if($id_group)
            {
                $insert_group = array('id_user' => $new_user,
                                    'id_group' => $id_group
                                );
                
                $this->ci->user_groups_model->insert($insert_group);
            }

            //Save Data Konsumen
            if($id_group == 4){
                $dt_konsumen = [
                                'idkonsumen' => gen_primary('','konsumen','idkonsumen'),
                                'nama'       => $full_name,
                                'st'         => intval(app_get_setting('register.tipe.konsumen')), //Tipe Konsumen
                                'wa'         => $phone,
                                'email'      => $email_address,
                                'id_user'    => $new_user
                                ];

                $this->ci->konsumen_model->insert($dt_konsumen);
            }
            //Send Email to New User 
            $subject = lang('users_register_subject');
            $to      = $email_address;
            $data_msg = ['full_name'    => $full_name, 
                        'username'      => $username, 
                        'random_string' => $random_string, 
                        'email_address' => $email_address,
                        'phone'         => $phone,
                        'need_activation' => app_get_setting('new.user.need.activation')
                        ];

            $message = $this->ci->load->view('users/register/message', $data_msg, TRUE);
            $message_body  = $this->ci->load->view('users/email/_header.php', '', TRUE);
            $message_body .= $message;
            $message_body .= $this->ci->load->view('users/email/_footer.php', '', TRUE);
            //Load Email Library
            $this->ci->load->library('mailer');
            $mail = $this->ci->mailer->load();
            $mail->Subject = $subject;
            $mail->Body = $message_body;
            $mail->AddAddress($to);
            $user_mail_sending = $mail->Send();
            //End Sending email to new user

            //Send Email to web admin
            $subject = lang('users_register_subject');
            $to      = trim(app_get_setting('email.system.notification'));
            $message = $this->ci->load->view('users/register/admin_message', $data_msg, TRUE);

            $message_body  = $this->ci->load->view('users/email/_header.php', '', TRUE);
            $message_body .= $message;
            $message_body .= $this->ci->load->view('users/email/_footer.php', '', TRUE);
            //Clear All Recipients
            $mail->clearAllRecipients();
            $mail->Body = $message_body;
            $mail->AddAddress($to);

            $admin_mail_sending = $mail->Send();
        }

        $this->ci->db->trans_complete();

        return $new_user;
    }

	public function login($username = "", $password = "", $remember = FALSE)
	{
		if($this->is_login())
        {
            if($this->is_admin())
            {
                redirect('dashboard');
            }
            else
            {
                redirect('/');
            }
        }

		$user 	= $this->ci->users_model->find_by(array('username' => $username, 'email' => $username),'', 'or');

    	if(!$user)
    	{
    		$this->ci->template->set_message(lang('users_login_fail'), 'error');
    		return FALSE;
    	}

        if($user->deleted == 1)
        {
            $this->ci->template->set_message(lang('users_already_deleted'), 'error');
            return FALSE;
        }

    	if($user->st_aktif == 0)
    	{
    		$this->ci->template->set_message(lang('users_not_active'), 'error');
    		return FALSE;
    	}

    	if(password_verify($password, $user->password))
    	{
    		//Buat Session
    		$array = array();
    		foreach ($user as $key => $usr) {
    			$array[$key] = $usr;
    		}

    		$this->ci->session->set_userdata('app_session', $array);
            //Set User Data
            $this->user = $this->ci->session->userdata('app_session');
            //Set Cookies if remember selected
            if($remember)
            {
                $this->setAutologinCookie($user->id_user);
            }
            //Update Login Terakhir
            $ip_address = ($this->ci->input->ip_address()) == "::1" ? "127.0.0.1" : $this->ci->input->ip_address();
            $this->ci->users_model->update($this->user_id(), array('login_terakhir' => date('Y-m-d H:i:s'), 'ip' => $ip_address));

            $requested_page = $this->ci->session->userdata('requested_page');
            if($requested_page != '')
            {
                redirect($requested_page);
            }

    		redirect("/");
    	}

        $this->ci->template->set_message(lang('users_wrong_password'), 'error');
        return FALSE;
	}

    /**
     * Set the autologin cookie data.
     * @param integer $userId   The user's ID.
     * @param string  $oldToken The user's existing autologin token.
     *
     * @return boolean True if the data was set successfully in the cookie and database,
     * else false.
     */
    private function setAutologinCookie($userId, $oldToken = null)
    {
        // Generate a random string for the token.
        if (! function_exists('random_string')) {
            $this->ci->load->helper('string');
        }
        $token = random_string('alnum', 128);

        if (empty($oldToken)) {
            $this->ci->db->insert(
                $this->cookiesTable,
                array(
                    'user_id'    => $userId,
                    'token'      => $token,
                    'created_on' => $this->getLoginTimestamp(),
                )
            );
        } else {
            $this->ci->db->where('user_id', $userId)
                         ->where('token', $oldToken)
                         ->set('token', $token)
                         ->set('created_on', $this->getLoginTimestamp())
                         ->update($this->cookiesTable);
        }

        if ($this->ci->db->affected_rows()) {
            if (! function_exists('set_cookie')) {
                $this->ci->load->helper('cookie');
            }

            set_cookie(
                $this->autoLoginIndex,
                "{$userId}{$this->autoLoginSeparator}{$token}",
                app_get_setting('auth.remember_length')
            );

            return true;
        }

        return false;
    }

    /**
     * Gets a timestamp using $this->loginDateFormat and the system's configured
     * 'time_reference'.
     *
     * @param integer $time A UNIX timestamp.
     *
     * @return string A timestamp formatted according to $this->loginDateFormat.
     */
    protected function getLoginTimestamp($time = null)
    {
        if (empty($time)) {
            $time = time();
        }
        return strtolower($this->ci->config->item('time_reference')) == 'gmt' ?
            gmdate($this->loginDateFormat, $time) : date($this->loginDateFormat, $time);
    }

    //--------------------------------------------------------------------------
    // !AUTO-LOGIN
    //--------------------------------------------------------------------------

    /**
     * Attempt to log the user in based on an existing 'autologin' cookie.
     *
     * @return void
     */
    private function autologin()
    {
        $cookie = $this->getAutologinCookie(true);
        if (is_null($cookie)) {
            return;
        }

        // If the session exists, there's nothing more to do.
        if ($this->ci->session->userdata('app_session')) {
            return;
        }

        // Grab the current user info for the session.
        $user   = $this->ci->users_model->find($cookie->userId);

        // If no user was found, the session can't be created properly.
        if (! $user) {
            return;
        }

        //Buat Session
        $array = array();
        foreach ($user as $key => $usr) {
            $array[$key] = $usr;
        }

        $this->ci->session->set_userdata('app_session', $array);
        //Set User Data
        $this->user = $this->ci->session->userdata('app_session');
    }

    /**
     * Get the user ID and token from the cookie.
     *
     * @param  boolean $xssClean Pass the data through CI's xss_clean().
     * @param  boolean $checkDb  Verify that the cookie data matches the database.
     *
     * @return object The login cookie (with userId and token properties) or null
     * if the cookie was not found or did not match the database (when $checkDb
     * is true).
     */
    private function getAutologinCookie($xssClean = null, $checkDb = true)
    {
        if (! function_exists('get_cookie')) {
            $this->ci->load->helper('cookie');
        }

        $cookie = get_cookie($this->autoLoginIndex, $xssClean);
        if (! $cookie) {
            return null;
        }

        $loginCookie = new stdClass;
        list($loginCookie->userId, $loginCookie->token) = explode($this->autoLoginSeparator, $cookie);

        if ($checkDb) {
            // Try to pull a match for the cookie from the database.
            $query = $this->ci->db->where(array('user_id' => $loginCookie->userId, 'token' => $loginCookie->token))
                                  ->get($this->cookiesTable);
            if ($query->num_rows() != 1) {
                return null;
            }
        }

        return $loginCookie;
    }

    /**
     * Delete the autologin cookie for the current user.
     *
     * @return void
     */
    private function deleteAutologin()
    {
        $this->deleteAutologinCookie();
        $this->cleanupCookies();
    }

    /**
     * Remove old user tokens from the database.
     *
     * @return void
     */
    private function cleanupCookies()
    {
        // Perform a clean up of any autologins older than 2 months.
        $this->ci->db->where('created_on <', $this->getLoginTimestamp(strtotime('2 months ago')))
                     ->delete($this->cookiesTable);
    }

    /**
     * Delete the autologin data from the user's cookie.
     *
     * @param  boolean $removeDbEntry Remove the user's token from the database.
     *
     * @return void
     */
    private function deleteAutologinCookie($removeDbEntry = true)
    {
        if ($removeDbEntry) {
            // Grab the cookie to determine which row in the table to delete.
            $cookie = $this->getAutologinCookie(false, false);
            if (! is_null($cookie)) {
                // Now delete the cookie from the database.
                $this->ci->db->where('user_id', $cookie->userId)
                             ->where('token', $cookie->token)
                             ->delete($this->cookiesTable);
            }
        }

        if (! function_exists('delete_cookie')) {
            $this->ci->load->helper('cookie');
        }
        delete_cookie($this->autoLoginIndex);
    }

    public function ajax_login($username = "", $password = "", $remember = FALSE)
    {
        $user   = $this->ci->users_model->find_by(array('username' => $username,'email' => $username),'','or');
        $err = "";
        if(!$user)
        {
            $err = lang('users_login_fail');
            goto errlabel;
        }

        if($user->deleted == 1)
        {
            $err = lang('users_already_deleted');
            goto errlabel;
        }

        if($user->st_aktif == 0)
        {
            $err = lang('users_not_active');
            goto errlabel;
        }

        if(password_verify($password, $user->password))
        {
            //Buat Session
            $array = array();
            foreach ($user as $key => $usr) {
                $array[$key] = $usr;
            }

            $this->ci->session->set_userdata('app_session', $array);
            //Set User Data
            $this->user = $this->ci->session->userdata('app_session');
            //Set Cookies if remember selected
            if($remember)
            {
                $this->setAutologinCookie($user->id_user);
            }
            //Update Login Terakhir
            $ip_address = ($this->ci->input->ip_address()) == "::1" ? "127.0.0.1" : $this->ci->input->ip_address();
            $this->ci->users_model->update($this->user_id(), array('login_terakhir' => date('Y-m-d H:i:s'), 'ip' => $ip_address));

            $requested_page = $this->ci->session->userdata('requested_page');
        }
        else{
            $err = lang('users_wrong_password');
        }

        errlabel:
        
        if($err)
        {
            $return = array('type' => 'error', 'msg' => $err);
        }
        else
        {
            $return = array('type' => 'success', 'msg' => 'Login success','redirect' => $requested_page ? $requested_page : 'dashboard');
        }

        return json_encode($return);
    }


    public function get_reset_link($email = NULL)
    {
        if(!$email)
        {
            return FALSE;
        }

        $data = $this->ci->users_model->find_by(array('email' => $email));
        if(!$data)
        {
            return FALSE;
        }

        // Generate a random string for the token.
        if (! function_exists('random_string')) {
            $this->ci->load->helper('string');
        }

        $random_string = random_string('numeric', 25);
        $expired_date  = date("Y-m-d H:i:s", strtotime('+24 hours'));

        $user_id = $data->id_user;
        $result = $this->ci->users_model->update($user_id, array('reset_password_token' => $random_string, 'valid_reset' => $expired_date));

        $link       = base_url('forgot_password/'.$user_id.'x'.$random_string);
        $fullname   = $data->nm_lengkap;

        return array('fullname' => $fullname, 'link' => $link);
    }

    public function reset_password($id_user = NULL, $token = NULL, $new_password = NULL)
    {
        if(!$id_user || !$token || !$new_password)
        {
            $this->ci->template->set_message(lang('users_invalid_reset_parameter'),'error');
            redirect('/');
        }

        $data = $this->ci->users_model->find_by(array('id_user' => $id_user, 'reset_password_token' => $token, 'valid_reset >=' => date('Y-m-d H:i:s')));
        if(!$data)
        {
            $this->ci->template->set_message(lang('users_invalid_reset_link'),'error');
            redirect('/');
        }

        $cost = $this->get_server_cost();

        $options = [
            'cost' => $cost
        ];

        $password = password_hash($new_password, PASSWORD_BCRYPT, $options);

        $result = $this->ci->users_model->update($id_user, array('password' => $password, 'reset_password_token' => NULL, 'valid_reset' => NULL));

        return $result;
    }

    public function logout()
    {
        // Destroy the autologin information
        $this->deleteAutologin();

        $this->ci->session->sess_destroy();
        redirect('/');
    }

    public function is_admin()
    {
        $id = $this->user_id();

        $data = $this->ci->users_model->join('user_groups','users.id_user = user_groups.id_user')
                                    ->find_by(array('users.id_user' => $id, 'id_group' => 1));
                                    
        if($data)
        {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Fungsi ini untuk mendapatkan data user group
     * @param  integer $id_user     [id user]
     * @param  integer $return_type [0 = String dipisahkan dengan koma, 1 = Array ID, 2 = Records]
     * @return mixed                [dapat berupa string dan array, default string]
     */
    public function get_user_groups($id_user = 0, $return_type = 0)
    {
        $id = $this->user_id();
        if($id_user > 0)
        {
            $id = $id_user;
        }

        $groups = $this->ci->user_groups_model->select("user_groups.id_group, groups.nm_group")
                                    ->join('groups', 'user_groups.id_group = groups.id_group')
                                    ->order_by('nm_group', 'ASC')
                                    ->find_all_by(array('id_user' => $id));

        $return = "";
        $arr    = array();
        if($groups)
        {
            if($return_type == 1) //Array ID
            {
                foreach ($groups as $key => $gr) {
                    $arr[] = $gr->id_group;
                }

                $return = $arr;
            }
            elseif($return_type == 2) //Records
            {
                $return = $groups;
            }
            else // 0 String
            {
                foreach ($groups as $key => $gr) {
                    $arr[] = ucwords($gr->nm_group);
                }

                $return = implode(", ", $arr);
            }

            
        }

        return $return;
    }

    public function has_permission($nm_permission = "")
    {
        if($nm_permission == "")
        {
            return FALSE;
        }

        if($this->is_admin())
        {
            return TRUE;
        }

        $id = $this->user_id();

        $group_permissions = $this->ci->users_model->join('user_groups','users.id_user = user_groups.id_user')
                                        ->join('group_permissions','user_groups.id_group = group_permissions.id_group')
                                        ->join('permissions','group_permissions.id_permission = permissions.id_permission')
                                        ->find_by(array('nm_permission' => $nm_permission, 'users.id_user' => $id));
        if($group_permissions)
        {
            return TRUE;
        }

        $user_permissions = $this->ci->users_model->join('user_permissions','users.id_user = user_permissions.id_user')
                                        ->join('permissions','user_permissions.id_permission = permissions.id_permission')
                                        ->find_by(array('nm_permission' => $nm_permission, 'users.id_user' => $id));
        if($user_permissions)
        {
            return TRUE;
        }

        return FALSE;
    }

    public function has_permissions($nm_permission = array())
    {
        if(!is_array($nm_permission) || count($nm_permission) == 0)
        {
            return FALSE;
        }

        if($this->is_admin())
        {
            return TRUE;
        }

        $id = $this->user_id();

        $group_permissions = $this->ci->users_model->join('user_groups','users.id_user = user_groups.id_user')
                                        ->join('group_permissions','user_groups.id_group = group_permissions.id_group')
                                        ->join('permissions','group_permissions.id_permission = permissions.id_permission')
                                        ->where_in('nm_permission', $nm_permission)
                                        ->find_by(array('users.id_user' => $id));
        if($group_permissions)
        {
            return TRUE;
        }

        $user_permissions = $this->ci->users_model->join('user_permissions','users.id_user = user_permissions.id_user')
                                        ->join('permissions','user_permissions.id_permission = permissions.id_permission')
                                        ->where_in('nm_permission', $nm_permission)
                                        ->find_by(array('users.id_user' => $id));
        if($user_permissions)
        {
            return TRUE;
        }

        return FALSE;
    }

    public function has_id_permission($id_permission = "")
    {
        if($id_permission == "")
        {
            return FALSE;
        }

        if($this->is_admin())
        {
            return TRUE;
        }

        $id = $this->user_id();

        $group_permissions = $this->ci->users_model->join('user_groups','users.id_user = user_groups.id_user')
                                        ->join('group_permissions','user_groups.id_group = group_permissions.id_group')
                                        ->join('permissions','group_permissions.id_permission = permissions.id_permission')
                                        ->find_by(array('permissions.id_permission' => $id_permission, 'users.id_user' => $id));
        if($group_permissions)
        {
            return TRUE;
        }

        $user_permissions = $this->ci->users_model->join('user_permissions','users.id_user = user_permissions.id_user')
                                        ->join('permissions','user_permissions.id_permission = permissions.id_permission')
                                        ->find_by(array('permissions.id_permission' => $id_permission, 'users.id_user' => $id));
        if($user_permissions)
        {
            return TRUE;
        }

        return FALSE;
    }

    public function restrict($permission = null, $uri = null) // This function copied from bonfire with modification
    {
        // If user isn't logged in, redirect to the login page.
        if ($this->is_login() === false) {
            $this->ci->template->set_message(lang('users_must_login'), 'error');
            redirect('login');
        }

        // Check whether the user has the proper permissions.
        if (empty($permission) || $this->has_permission($permission)) {
            return true;
        }

        // If the user is logged in, but does not have permission...
        // If $uri is not set, get the previous page from the session.
        if (! $uri) {
            $uri      = $this->ci->session->userdata('previous_page');
            $req_page = $this->ci->session->userdata('requested_page');

            // If previous page and current page are the same, but the user no longer
            // has permission, redirect to site URL to prevent an infinite loop.
            if ($uri == $req_page) {
                $uri = site_url();
            }
        }

        // Inform the user of the lack of permission and redirect.
        $this->ci->template->set_message(lang('users_no_permission'), 'error');
        redirect($uri);
    }

    //Get Cost for server performance test
    public function get_server_cost()
    {
        /**
         * This code will benchmark your server to determine how high of a cost you can
         * afford. You want to set the highest cost that you can without slowing down
         * you server too much. 8-10 is a good baseline, and more is good if your servers
         * are fast enough. The code below aims for ≤ 50 milliseconds stretching time,
         * which is a good baseline for systems handling interactive logins.
         */
        $timeTarget = 0.05; // 50 milliseconds 

        $cost = 8;
        do {
            $cost++;
            $start = microtime(true);
            password_hash("test", PASSWORD_BCRYPT, ["cost" => $cost]);
            $end = microtime(true);
        } while (($end - $start) < $timeTarget);
        

        return $cost;
    }
}