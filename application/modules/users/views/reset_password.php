<div class="forgot-password-box">
	<div class="box panel-default">
        <div class="box-body">
            <div class="text-center">
              	<h3><i class="fa fa-unlock-alt fa-4x"></i></h3>
              	<h2 class="text-center">New Password</h2>
              	<p>Type your new password here.</p>
                <div class="box-body">
                	<?= form_open($this->uri->uri_string(), array('class'=>'form-horizontal', 'id'=>'frm_forgot_password','name'=>'frm_forgot_password')) ?>
                        <fieldset>
                            <div class="form-group">
                              <input type="password" class="form-control" name="new_password" id="new_password" placeholder="type new password" />
                            </div>
                            <div class="form-group">
                              <input type="password" class="form-control" name="confirm_new_password" id="confirm_new_password" placeholder="confirm new password" />
                            </div>
                          	<div class="form-group">
                          		<button type="submit" class="btn btn-primary btn-block" name="save">Save</button>
                          	</div>
                        </fieldset>
                  	<?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>