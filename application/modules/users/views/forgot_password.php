<div class="forgot-password-box">
	<div class="box panel-default">
        <div class="box-body">
            <div class="text-center">
              	<h3><i class="fa fa-lock fa-4x"></i></h3>
              	<h2 class="text-center">Forgot Password?</h2>
              	<p>You can reset your password here.</p>
                <div class="box-body">
                	<?= form_open($this->uri->uri_string(), array('class'=>'form-horizontal', 'id'=>'frm_forgot_password','name'=>'frm_forgot_password')) ?>
                        <fieldset>
                            <div class="form-group">
                                <div class="input-group">
                                  	<span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                                  	<input type="email" id="emailInput" name="reset_email" placeholder="enter your email address here" class="form-control" oninvalid="setCustomValidity('Please enter a valid email address!')" onchange="try{setCustomValidity('')}catch(e){}" required autofocus>
                                </div>
                            </div>
                          	<div class="form-group">
                          		<button type="submit" class="btn btn-primary btn-block" name="send">Send My Password</button>
                          	</div>
                        </fieldset>
                  	<?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>