<p>Selamat <?= $full_name ?>,
    <br>
    <?php if(intval($need_activation) == 1) : ?>
    Kamu telah berhasil mendaftar pada <a href="<?= site_url() ?>" target="_blank"><?= site_url() ?></a>, silahkan tunggu hingga admin kami mengaktifkan akun kamu. 
    <br>
    Ketika akun kamu telah aktif, kamu dapat masuk menggunakan informasi berikut:
    <?php else : ?>
    Kamu telah berhasil mendaftar pada <a href="<?= site_url() ?>" target="_blank"><?= site_url() ?></a>, sekarang akun kamu telah aktif.
    <br>
    Kamu dapat masuk dengan menggunakan informasi berikut:
    <?php endif; ?>
    <br>
    Username : <?= $username ?>
    <br>
    Password Sementara : <?= $random_string ?>
    <br>
    Untuk alasan keamanan, mohon ubah password kamu ketika telah berhasil masuk untuk pertama kali.
    <br>
    <br>
    Terima Kasih.
</p>