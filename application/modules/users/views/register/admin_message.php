<p>Hi Admin,
    <br>
    Ada pengguna baru yang mendaftar pada <a href="<?= site_url() ?>" target="_blank"><?= site_url() ?></a>.
    <br>
    Berikut adalah informasi pengguna tersebut:
    <br>
    Username : <?= $username ?>
    <br>
    Password Sementara : <?= $random_string ?>
    <br>
    Email : <?= $email_address ?>
    <br>
    No. Hp : <?= $phone ?>
    <?php if(intval($need_activation) == 1) : ?>
    <br>
    <br>
    <strong>Mohon segera aktifkan</strong>
    <?php endif; ?>
    <br>
    <br>
    Thank you.
</p>