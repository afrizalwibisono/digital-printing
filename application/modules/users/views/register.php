<div class="register-box">
  <div class="register-logo">
    <a href="<?= site_url() ?>"><img src="<?= base_url('assets/images/logo/'.$idt->logo) ?>" alt="<?= isset($idt->nm_perusahaan) ? $idt->nm_perusahaan : 'Belum Disetting' ?>"></a>
  </div>
  <!-- /.register-logo -->
  <div class="register-box-body">
    <p class="register-box-msg">Registrasi Pengguna Baru</p>

    <?= form_open($this->uri->uri_string(), array('id' => 'frm_register', 'name' => 'frm_register')) ?>
      <div class="form-group <?= form_error('full_name') ? 'has-error' : '' ?>">
        <input type="text" class="form-control" name="full_name" placeholder="<?= lang('users_nm_lengkap') ?>" value="<?= set_value('full_name') ?>" required autofocus>
      </div>
      <div class="form-group has-feedback <?= form_error('username') ? 'has-error' : '' ?>">
        <input type="text" class="form-control" name="username" placeholder="<?= lang('users_username') ?>" value="<?= set_value('username') ?>" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback <?= form_error('email_address') ? 'has-error' : '' ?>">
        <input type="email" class="form-control" name="email_address" placeholder="<?= lang('users_email') ?>" value="<?= set_value('email_address') ?>" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
       <div class="form-group has-feedback <?= form_error('phone') ? 'has-error' : '' ?>">
        <input type="text" class="form-control" name="phone" placeholder="<?= lang('users_hp') ?>" value="<?= set_value('phone') ?>" required>
        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="checkbox icheck register-agree">
            <label>
              <input type="checkbox" id="agree" name="agree" value=""> Saya menyetujui <a href="#">syarat dan ketentuan</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <button type="submit" class="btn btn-primary btn-block btn-flat" name="register">Submit</button>
        </div>
        <div class="col-xs-6">
          <a href="<?= site_url('/') ?>" class="btn btn-danger btn-block btn-flat">Batal</a>
        </div>
        <!-- /.col -->
      </div>
    <?= form_close() ?>
    <?php
        if (Template::message()) :
    ?>
    <br>
    <div class="row">
        <div class="col-md-12">
            <?= Template::message(); ?>
        </div>
    </div>
    <?php endif; ?>
  </div>
  <!-- /.register-box-body -->
</div>
<!-- /.register-box -->
<script type="text/javascript">
  $(document).ready(function(){
    $("body").addClass("register-page");
  });
</script>