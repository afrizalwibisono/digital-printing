<div class="box box-primary">
    <!-- form start -->
    <?= form_open_multipart($this->uri->uri_string(),array('id'=>'frm_users','name'=>'frm_users','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
    		<div class="row">
    			<div class="col-md-6">
    				<div class="form-group <?= form_error('username') ? ' has-error' : ''; ?>">
					    <label for="username" class="col-md-4 control-label"><?= lang('users_username') ?></label>
					    <div class="col-md-6">
					    	<input type="text" class="form-control" id="username" name="username" maxlength="45" value="<?= set_value('username', isset($data->username) ? $data->username : ''); ?>" autofocus />
					    </div>
				  	</div>
				  	<div class="form-group <?= form_error('password') ? ' has-error' : ''; ?>">
					    <label for="password" class="col-md-4 control-label"><?= lang('users_password') ?></label>
					    <div class="col-md-6">
					    	<input type="password" class="form-control" id="password" name="password" maxlength="100" value="<?= set_value('password') ?>" placeholder="<?= isset($data) ? lang('users_pass_placeholder') : '' ?>" />
					    </div>
				  	</div>
				  	<div class="form-group <?= form_error('re-password') ? ' has-error' : ''; ?>">
					    <label for="re-password" class="col-md-4 control-label"><?= lang('users_repassword') ?></label>
					    <div class="col-md-6">
					    	<input type="password" class="form-control" id="re-password" name="re-password" maxlength="100" value="<?= set_value('re-password') ?>" placeholder="<?= isset($data) ? lang('users_pass_placeholder') : '' ?>" />
					    </div>
				  	</div>
				  	<div class="form-group <?= form_error('email') ? ' has-error' : ''; ?>">
					    <label for="email" class="col-md-4 control-label"><?= lang('users_email') ?></label>
					    <div class="col-md-6">
					    	<input type="email" class="form-control" id="email" name="email" maxlength="100" value="<?= set_value('email', isset($data->email) ? $data->email : ''); ?>" <?= (in_array(3, $group_ids) || in_array(4, $group_ids)) ? '' : 'readonly=""' ?> />
					    </div>
				  	</div>
				  	<?php if(in_array(3, $group_ids) || in_array(4, $group_ids)): ?>
				  	<div class="form-group <?= form_error('nm_lengkap') ? ' has-error' : ''; ?>">
					    <label for="nm_lengkap" class="col-md-4 control-label"><?= lang('users_nm_lengkap') ?></label>
					    <div class="col-md-6">
					    	<input type="text" class="form-control" id="nm_lengkap" name="nm_lengkap" maxlength="100" value="<?= set_value('nm_lengkap', isset($data->nm_lengkap) ? $data->nm_lengkap : ''); ?>"  />
					    </div>
				  	</div>
				  	<div class="form-group <?= form_error('alamat') ? ' has-error' : ''; ?>">
					    <label for="alamat" class="col-md-4 control-label"><?= lang('users_alamat') ?></label>
					    <div class="col-md-8">
					    	<textarea class="form-control" id="alamat" name="alamat" maxlength="255"><?= set_value('alamat', isset($data->alamat) ? $data->alamat : ''); ?></textarea>
					    </div>
				  	</div>
				  	<div class="form-group <?= form_error('kota') ? ' has-error' : ''; ?>">
					    <label for="kota" class="col-md-4 control-label"><?= lang('users_kota') ?></label>
					    <div class="col-md-4">
					    	<input type="text" class="form-control" id="kota" name="kota" maxlength="20" value="<?= set_value('kota', isset($data->kota) ? $data->kota : ''); ?>"  />
					    </div>
				  	</div>
				  	<div class="form-group <?= form_error('hp') ? ' has-error' : ''; ?>">
					    <label for="hp" class="col-md-4 control-label"><?= lang('users_hp') ?></label>
					    <div class="col-md-4">
					    	<input type="text" class="form-control" id="hp" name="hp" maxlength="20" value="<?= set_value('hp', isset($data->hp) ? $data->hp : ''); ?>"  />
					    </div>
				  	</div>
				  	<?php endif; ?>
				  	<div class="form-group">
			            <label for="photo" class="col-md-4 control-label"><?= lang('users_photo') ?></label>
			            <div class="col-md-5">
			                <input type="file" id="photo" name="photo" accept="image/png, image/jpg, image/jpeg">
			                <div id="imgcontainer" style="width:70px;height:70px;border:1px solid #000000;text-align:center;float:left;margin-top:10px;">
			                    <img id="img-user" src="<?= isset($data->photo) && $data->photo!="" && file_exists('assets/images/users/'.$data->photo) ? base_url('assets/images/users/'.$data->photo) : base_url('assets/images/male-def.png') ?>" alt="preview" width="68px" height="68px">
			                </div>
			                <div class="clearfix"></div>
		                	<span class="help-block text-red">Allowed file extension : jpeg, jpg, png<br>and max. file size is 1MB</span>
			            </div>
			        </div>
    			</div>
    		</div>
	  	</div>
	  	<div class="box-footer">
	  		<button type="submit" name="save" class="btn btn-primary"><i class="fa fa-save"></i> <?= lang('users_btn_save') ?></button>
	  		<?php
            	echo ' atau ' . anchor('profile', lang('users_btn_cancel'));
            ?>
	  	</div>
	<?= form_close() ?>
</div><!-- /.box -->
<script type="text/javascript">
	$(document).ready(function(){
        //Load Foto
        $("#photo").change(function(){
            loadfoto(this,'img-user');
        });
    });
</script>