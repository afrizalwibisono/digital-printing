<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['users_new_title'] 	= 'New User';
$lang['users_edit_title'] 	= 'Edit User';
$lang['users_register_title'] 	= 'Register';
$lang['users_edit_profile'] = 'My Profile';
$lang['users_edit_perm_title'] 	= 'Edit User Permission';
$lang['users_manage_title'] = 'Users';
$lang['users_access_title'] = 'Permission';
$lang['users_full_access']  = 'Full Access';
$lang['users_facility_name'] = 'Module Name';
$lang['users_info'] 		 = 'User Information';
$lang['users_group_info'] 	 = 'Group Information';
$lang['users_group'] 	 	 = 'User Group';

$lang['users_no'] 			= '#';
$lang['users_username'] 	= 'Username';
$lang['users_password'] 	= 'Password';
$lang['users_repassword'] 	= 'Re-Password';
$lang['users_email'] 		= 'Email';
$lang['users_nm_lengkap'] 	= 'Nama Lengkap';
$lang['users_alamat'] 		= 'Alamat';
$lang['users_kota'] 		= 'Kota';
$lang['users_hp'] 			= 'No. Hp';
$lang['users_st_aktif'] 	= 'Status';
$lang['users_aktif'] 		= 'Aktif';
$lang['users_td_aktif'] 	= 'Tidak Aktif';
$lang['users_photo'] 		= 'Foto';

$lang['users_message'] 		= 'Pesan';

$lang['users_pass_placeholder'] = 'leave it blank to keep your password';

$lang['users_btn_new'] 		= 'New';
$lang['users_btn_save'] 	= 'Save';
$lang['users_btn_cancel'] 	= 'Cancel';
$lang['users_btn_delete'] 	= 'Delete';

$lang['users_create_success'] = 'New user created successfully';
$lang['users_create_fail'] 	  = 'New user creation failed :';
$lang['users_edit_success']	  = 'Update saved successfully';
$lang['users_edit_fail'] 	  = 'Update saving failed';
$lang['users_del_confirm'] 	  = 'Are you sure to delete selected users ?';
$lang['users_del_success'] 	  = 'Selected user deleted successfully';
$lang['users_del_fail'] 	  = 'Selected user deletion failed';
$lang['users_del_error'] 	  = 'Please, choose at least a user to be deleted';
$lang['users_invalid_id'] 	  = 'Invalid ID';
$lang['users_invalid_email']  = 'Alamat Email tidak valid / telah terdaftar';
$lang['users_username_already_taken']   = 'Username telah terdaftar';
$lang['users_email_already_taken']  	= 'Alamat email telah terdaftar';
$lang['users_register_incomplete']  	= 'Data registrasi tidak lengkap';
$lang['users_already_deleted'] = 'User has been deleted,<br> for more information please call Administrator';
$lang['users_permission_edit_success'] = 'User Access saved successfully';
$lang['users_permission_edit_fail'] = 'User Access saving failed';

$lang['users_login_fail'] 	  	= 'Login failed, username can not be found';
$lang['users_not_active'] 		= 'Username have been inactive';
$lang['users_wrong_password'] 	= 'You have entered a wrong password';
$lang['users_must_login'] 		= 'You must log in to open that page';
$lang['users_no_permission'] 	= 'You have not permission to open that page';
$lang['users_no_records_found'] = 'Data not found';
$lang['users_only_ajax'] 		= 'Only ajax request allowed';

$lang['users_email_not_found']   		= 'Email address not found';
$lang['users_invalid_reset_link']   	= 'Invalid password reset link or it has expired';
$lang['users_invalid_reset_parameter']  = 'Invalid parameter for reset password';
$lang['users_reset_link_sent']   		= 'We have sent a reset link to your email.<br>Please check your inbox or spam folder.';
$lang['users_email_send_failed']   		= 'Sorry, we failed to send reset link to your email.';
$lang['users_reset_password_success']   = 'Your password has been reset successfully';
$lang['users_reset_password_failed']    = 'Your password failed to reset';
//Register
$lang['users_register_success']   = 'Pendaftaran berhasil.<br>Kami telah mengirimkan informasi login ke email anda.<br>Silahkan periksa folder inbox/spam';
$lang['users_register_failed']    = 'Maaf, Pendaftaran gagal.<br>Mohon ulangi lagi';
$lang['users_register_subject']   = 'Registrasi Pengguna Baru';
$lang['users_register_agree']     = 'Pertetujuan Pengguna';
$lang['users_register_agree_message']   = 'Anda harus menyetujui syarat dan ketentuan sebelum mendaftar';