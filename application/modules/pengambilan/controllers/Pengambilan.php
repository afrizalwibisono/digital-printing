<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2018, Cokeshome
 * 
 */

class Pengambilan extends Admin_Controller {
    
    protected $viewPermission       = "Pengambilan.View";
    protected $addPermission        = "Pengambilan.Add";
    protected $managePermission     = "Pengambilan.Manage";

    protected $prefix_key           = "PB";

    public function __construct(){
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('pengambilan/pengambilan');
        $this->load->model([
                                'konsumen_model',
                                'order_model',
                                'order_detail_model',
                                'trans_model',
                                'kolekting_model',
                                'pengambilan_model',
                                'pengambilan_detail_model'
                            ]);

        $this->template->title(lang('title_data'));
		$this->template->page_icon('fa fa-list');
    }

    public function index(){

        $this->auth->restrict($this->viewPermission);

        if(isset($_POST['tgl1'])){

            $tgl1           = isset($_POST['tgl1']) ? $this->input->post("tgl1") : '';
            $tgl2           = isset($_POST['tgl2']) ? $this->input->post("tgl2") : '';
            $konsumen       = isset($_POST['konsumen']) ? $this->input->post("konsumen") : '';
            $judul_file     = isset($_POST['judul_file']) ? $this->input->post("judul_file") : '';
            $kd_barcode     = isset($_POST['kd_barcode']) ? $this->input->post("kd_barcode") : '';
            $kd_faktur      = isset($_POST['kd_faktur']) ? $this->input->post("kd_faktur") : '';

        }elseif (isset($_GET['tgl1'])) {
                
            $tgl1           = isset($_GET['tgl1']) ? $this->input->get("tgl1") : '';
            $tgl2           = isset($_GET['tgl2']) ? $this->input->get("tgl2") : '';
            $konsumen       = isset($_GET['konsumen']) ? $this->input->get("konsumen") : '';
            $judul_file     = isset($_GET['judul_file']) ? urldecode($this->input->get("judul_file")) : '';
            $kd_barcode     = isset($_GET['kd_barcode']) ? $this->input->get("kd_barcode") : '';
            $kd_faktur      = isset($_GET['kd_faktur']) ? $this->input->get("kd_faktur") : '';

        }else{

            $tgl1           = date("d/m/Y");
            $tgl2           = date("d/m/Y");
            $konsumen       = '';
            $judul_file     = '';
            $kd_barcode     = '';
            $kd_faktur      = '';

        }

        /* pengkondisian where */

        $where          = "pengambilan.deleted = 0 and transaksi.deleted = 0";
        $where_tambah   = "";

        if(strlen($tgl1)){

            $tgl1_pakai     = date_ymd($tgl1)." 00:00:00";
            $tgl2_pakai     = date_ymd($tgl2)." 23:59:00";

            $where_tambah   .= " and (`pengambilan`.`created_on` >= '{$tgl1_pakai}' and  `pengambilan`.`created_on` <= '{$tgl2_pakai}')";

        }

        if(strlen($konsumen)){

            $where_tambah   .= " and `order_produk`.id_konsumen = '{$konsumen}'";

        }

        if(strlen($judul_file)){

            $where_tambah   .= " and `order_produk`.nama_pekerjaan = '{$judul_file}'";            

        }

        if(strlen($kd_barcode)){

            $where_tambah   = " and `order_produk`.barcode = '{$kd_barcode}'";            

        }        

        if(strlen($kd_faktur)){

            $where_tambah   = " and `transaksi`.`no_faktur` = '{$kd_faktur}'";            

        }        

        $where  .= $where_tambah;

        $total  = $this->pengambilan_model->select("pengambilan.idpengambilan")
                        ->join("pengambilan_detail","pengambilan.idpengambilan = pengambilan_detail.idpengambilan","inner")
                        ->join("order_produk_detail","pengambilan_detail.id_detail_produk_order = order_produk_detail.id_detail_produk_order","inner")
                        ->join("`kategori`","`order_produk_detail`.`id_kategori` = `kategori`.`idkategori`","inner")
                        ->join("`produk`","`order_produk_detail`.`id_produk` = `produk`.`idproduk`","inner")
                        ->join("`konversi_satuan`","`order_produk_detail`.`id_satuan` = `konversi_satuan`.`id_konversi`","inner")
                        ->join("`satuan_terkecil`","`konversi_satuan`.`id_satuan_terkecil` = `satuan_terkecil`.`id_satuan_terkecil`","inner")
                        ->join("order_produk","order_produk.id_order = order_produk_detail.id_order","inner")
                        ->join("konsumen","konsumen.idkonsumen = order_produk.id_konsumen","inner")
                        ->join("transaksi","transaksi.id_order = order_produk.id_order","inner")
                        ->where($where)
                        ->count_all();

        // Pagination
        $this->load->library('pagination');
        
        $offset = $this->input->get('per_page');

        $limit  = $this->config->item('list_limit');

        $filter = "?tgl1={$tgl1}&tgl2={$tgl2}&konsumen={$konsumen}&judul_file=".urlencode($judul_file)."&kd_barcode={$kd_barcode}&kd_faktur={$kd_faktur}" ;

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);    

        $data   = $this->pengambilan_model
                        ->select("`pengambilan`.`idpengambilan`,
                                    date_format(`pengambilan`.`created_on`,'%d/%m/%Y %h:%i:%s') as waktu,
                                    `pengambilan`.`nm_pengambil`,
                                    `pengambilan`.`st`,
                                    `pengambilan`.`ket`,
                                    `pengambilan_detail`.`id_pengambilan_detail`,
                                    `order_produk_detail`.`nama_pekerjaan`,
                                    `kategori`.`nmkategori`,
                                    `produk`.`nm_produk`,
                                    `order_produk_detail`.`id_detail_produk_order`,
                                    `order_produk_detail`.`thumbnail`,
                                    `order_produk_detail`.`p`,
                                    `order_produk_detail`.`l`,
                                    CONCAT(konversi_satuan.satuan_besar,
                                            ' (',
                                            `konversi_satuan`.`jml_kecil`,
                                            ' ',
                                            `satuan_terkecil`.`alias`,
                                            ')') AS tampil2,
                                    `order_produk_detail`.`jml_cetak`,
                                    `order_produk_detail`.`sub_total`,
                                    IF(`order_produk_detail`.`st_finishing` = 0,
                                        'Finishing Standart',
                                        'Custom Finishing') AS st_finishing,
                                    `order_produk_detail`.`st_collecting`,
                                    `order_produk_detail`.`st_ambil`,
                                    `order_produk`.`no_transaksi`,
                                    CONCAT(IF(konsumen.panggilan IS NULL,
                                                '',
                                                konsumen.panggilan),
                                            ' ',
                                            `nama`,
                                            ' | ',
                                            IF(konsumen.st = 0,
                                                'konsumen',
                                                IF(konsumen.st = 1,
                                                    'reseller',
                                                    'instansi'))) AS nm,
                                    `transaksi`.`id_transaksi`,
                                    `transaksi`.`no_faktur`")
                        ->join("pengambilan_detail","pengambilan.idpengambilan = pengambilan_detail.idpengambilan","inner")
                        ->join("order_produk_detail","pengambilan_detail.id_detail_produk_order = order_produk_detail.id_detail_produk_order","inner")
                        ->join("`kategori`","`order_produk_detail`.`id_kategori` = `kategori`.`idkategori`","inner")
                        ->join("`produk`","`order_produk_detail`.`id_produk` = `produk`.`idproduk`","inner")
                        ->join("`konversi_satuan`","`order_produk_detail`.`id_satuan` = `konversi_satuan`.`id_konversi`","inner")
                        ->join("`satuan_terkecil`","`konversi_satuan`.`id_satuan_terkecil` = `satuan_terkecil`.`id_satuan_terkecil`","inner")
                        ->join("order_produk","order_produk.id_order = order_produk_detail.id_order","inner")
                        ->join("konsumen","konsumen.idkonsumen = order_produk.id_konsumen","inner")
                        ->join("transaksi","transaksi.id_order = order_produk.id_order","inner")
                        ->where($where)
                        ->order_by("pengambilan.created_on","ASC")
                        ->order_by("`order_produk_detail`.`nama_pekerjaan`","ASC")
                        ->limit($limit, $offset)
                        ->find_all();

        //echo $this->db->last_query();

        $data_konsumen  = $this->konsumen_model
                                ->select("`idkonsumen` as id, 
                                        CONCAT(IF(panggilan IS NULL, '', panggilan),
                                        ' ',
                                        `nama`,
                                        ' | ',
                                        IF(st = 0,
                                            'konsumen',
                                            IF(st = 1, 'reseller', 'instansi'))) AS nm")
                                ->where("deleted=0")
                                ->order_by("nama","asc")
                                ->find_all();


        $asset  =   [
                        "plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
                        "plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
                        "plugins/select2/dist/css/select2.min.css",
                        "plugins/select2/dist/js/select2.min.js",
                        "pengambilan/assets/js/index_pengambilan.js",
                        "pengambilan/assets/css/css_list.css"
                    ];

        add_assets($asset);

        $param_filter   =   [
                                'tgl1'          => $tgl1,
                                'tgl2'          => $tgl2,
                                'konsumen'      => $konsumen,
                                'judul_file'    => $judul_file,
                                'kd_barcode'    => $kd_barcode,
                                'kd_faktur'     => $kd_faktur
                            ];

        $this->template->set("numb",$offset+1);
        $this->template->set("data",$data);
        $this->template->set("pr",$param_filter);
        $this->template->set("dt_konsumen",$data_konsumen);
        $this->template->title(lang('title_data'));
        $this->template->set("toolbar_title", lang('title_data'));
        $this->template->set("page_title", lang('title_data'));
        $this->template->page_icon('fa fa-list');
        $this->template->render('index'); 
    }

    public function batal(){

        $this->template->set_message(lang("label_cancel_sukses"),"success");
        redirect("pengambilan/create");

    }    

    public function create(){

        $this->auth->restrict($this->addPermission);        

        if(isset($_POST["st_simpan"])){    
        //if(isset($_POST["simpan"])){    

            if($this->simpan()){

                $this->template->set_message(lang("label_simpan_sukses"),"success");
                redirect("pengambilan/create");

            }

        }

        $data_konsumen  = $this->konsumen_model
                                ->select("`idkonsumen` as id, 
                                        CONCAT(IF(panggilan IS NULL, '', panggilan),
                                        ' ',
                                        `nama`,
                                        ' | ',
                                        IF(st = 0,
                                            'konsumen',
                                            IF(st = 1, 'reseller', 'instansi'))) AS nm")
                                ->where("deleted=0")
                                ->order_by("nama","asc")
                                ->find_all();


        $asset  =   [
                        "plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
                        "plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
                        "plugins/select2/dist/css/select2.min.css",
                        "plugins/select2/dist/js/select2.min.js",
                        "pengambilan/assets/js/frm_pengambilan.js",
                        "pengambilan/assets/css/css_list.css"
                    ];

        add_assets($asset);

        $this->template->set("dt_konsumen",$data_konsumen);

        $this->template->title(lang('title_new'));
        $this->template->set("toolbar_title", lang('title_new'));
        $this->template->set("page_title", lang('title_new'));
        $this->template->page_icon('fa fa-check-square-o');
        $this->template->render('form_pengambilan'); 

    }

    private function simpan(){

        $aksi           = $this->input->post("pil-aksi");
        $checked        = $this->input->post("checked");
        $nm_pengambil   = $this->input->post("nm_pengambil");
        $ket            = $this->input->post("ket");

        $this->form_validation->set_rules("nm_pengambil","lang:capt-frm-nm-pengambil","required");

        if($this->form_validation->run() === false){

            Template::set_message(validation_errors(),'error');
            return false;

        }

        if(!isset($_POST['checked'])){

            $this->template->set_message(lang("konfirmasi-error-pil-update"),"error");
            return false;

        }

        $sql_all    = "";

        $this->db->trans_start();

            /* ..:: Simpan data pengembilan */

            $idprimary  = gen_primary("","pengambilan","idpengambilan");
            $arr_head   =   [
                                'idpengambilan' => $idprimary, 
                                'nm_pengambil'  => $nm_pengambil, 
                                'st'            => $aksi, 
                                'ket'           => $ket
                            ];

            $this->pengambilan_model->insert($arr_head);
            $sql_all    = $this->db->last_query();

            $arr_update = [];
            $arr_detail = [];

            foreach ($checked as $key => $dt) {
                
                $pecah          = explode("|", $dt);
                $idorder_detail = $pecah[1];

                $arr_detail[]   =   [
                                        'id_pengambilan_detail'     => gen_primary("","pengambilan_detail","id_pengambilan_detail"), 
                                        'idpengambilan'             => $idprimary, 
                                        'id_detail_produk_order'    => $idorder_detail
                                    ];

                

                $arr_update[]   =   [
                                        'id_detail_produk_order'    => $idorder_detail,   
                                        'st_ambil'                  => $aksi
                                    ];

            }

            $this->order_detail_model->update_batch($arr_update,'id_detail_produk_order');
            $sql_all    .= "\n\n".$this->db->last_query();    

            
            $this->pengambilan_detail_model->insert_batch($arr_detail);
            $sql_all    .= "\n\n".$this->db->last_query();

        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            $return     = false;
            $keterangan = "gagal simpan data pengambilan barang";
            $status     = 0;

        }else{

            $return     = true;
            $keterangan = "sukses simpan data pengambilan barang";
            $status     = 1;

        }

        $nm_hak_akses   = $this->addPermission; 
        $kode_universal = $idprimary;
        $jumlah         = 0;
        $sql            = $sql_all;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        return $return;

    }

    public function buka_data(){

        if(!$this->input->is_ajax_request()){
            redirect('pengambilan/create');
        }

        $konsumen       = isset($_POST['konsumen']) ? $this->input->post("konsumen") : '';
        $no_faktur      = isset($_POST['no_faktur']) ? $this->input->post("no_faktur") : '';
        $kode_barcode   = isset($_POST['barcode']) ? $this->input->post("barcode") : '';
        $aksi           = isset($_POST['aksi']) ? $this->input->post("aksi") : '';

        $st_ambil       = $aksi == 1 ? 0 : 1;

        $where_tambah   = "";

        $where          = "`transaksi`.`deleted` = 0
                            AND `transaksi`.`st_produksi` = 1 AND `order_produk_detail`.`st_ambil` = {$st_ambil}";

        if(strlen($konsumen) > 0){

            $where_tambah   .= " and `order_produk`.id_konsumen = '{$konsumen}'";

        }

        if(strlen($no_faktur) > 0){

            $where_tambah   .= " and transaksi.no_faktur = '{$no_faktur}'";            

        }        

        if(strlen($kode_barcode) > 0){

            $where_tambah   = " AND order_produk.barcode = '{$kode_barcode}'";

        }

        $where  .= $where_tambah;

        $data   =   $this->order_model
                    ->select(" `order_produk`.id_order as id,
                                `order_produk`.id_konsumen,
                                CONCAT(IF(panggilan IS NULL, '', panggilan),
                                        ' ',
                                        `nama`,
                                        ' | ',
                                        IF(st = 0,
                                            'konsumen',
                                            IF(st = 1, 'reseller', 'instansi'))) AS nm,
                                order_produk.no_transaksi,
                                transaksi.id_transaksi,
                                transaksi.no_faktur,
                                CONCAT(IF(panggilan IS NULL, '', panggilan),
                                        ' ',
                                        `nama`,
                                        ' | ',
                                        IF(st = 0,
                                            'konsumen',
                                            IF(st = 1, 'reseller', 'instansi'))) AS nm,
                                `order_produk_detail`.`nama_pekerjaan`,
                                `kategori`.`nmkategori`,
                                `produk`.`nm_produk`,
                                `order_produk_detail`.`id_detail_produk_order`,
                                `order_produk_detail`.`thumbnail`,
                                `order_produk_detail`.`p`,
                                `order_produk_detail`.`l`,
                                CONCAT(konversi_satuan.satuan_besar,
                                        ' (',
                                        konversi_satuan.jml_kecil,
                                        ' ',
                                        satuan_terkecil.alias,
                                        ')') AS tampil2,
                                `order_produk_detail`.`jml_cetak`,
                                `order_produk_detail`.`sub_total`,
                                IF(`order_produk_detail`.`st_finishing` = 0,
                                    'Finishing Standart',
                                    'Custom Finishing') AS st_finishing,
                                `order_produk_detail`.`st_collecting`,
                                `order_produk_detail`.`st_ambil`")
                    ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                    ->join("transaksi","order_produk.id_order = transaksi.id_order","inner")
                    ->join("order_produk_detail","order_produk.id_order = order_produk_detail.id_order","inner")
                    ->join("kategori","order_produk_detail.id_kategori = kategori.idkategori","inner")
                    ->join("produk","order_produk_detail.id_produk = produk.idproduk","inner")
                    ->join("konversi_satuan","order_produk_detail.id_satuan = konversi_satuan.id_konversi","inner")
                    ->join("satuan_terkecil","konversi_satuan.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","inner")
                    ->where($where)
                    ->order_by("`order_produk_detail`.`nama_pekerjaan`", "asc")
                    ->find_all();

        //echo $this->db->last_query();

        if(is_array($data) && count($data)){

            $konsumen   = strlen($no_faktur) > 0 ||  strlen($kode_barcode) > 0 ? $data[0]->nm : "";

            $st_ketemu  = 1;
            $tampil     = "";

            foreach ($data as $key => $dt) {
                
                $isi_acuan  = $dt->id."|".$dt->id_detail_produk_order."|".$dt->st_ambil."|".$key;
                $centang    = "";

                if($dt->st_collecting == 0){

                    $label  = "<span class='label label-warning'>".lang('isi-status-belum-collect')."</span>";

                }else{

                    $label          = "<span class='label label-success'>".lang('isi-status-sudah-collect')."</span>";
                    $centang        = "<input type='checkbox' name='checked[]' id='checked' value='".$isi_acuan."'>";

                }

                if($dt->st_ambil == 0){

                    $label_ambil = "<span class='label label-danger'>".lang('isi-status-belum-ambil')."</span>";

                }else{

                    $label_ambil = "<span class='label label-success'>".lang('isi-status-sudah-ambil')."</span>";

                }

                $tampil     .=  "<tr>".
                                    "<td class='text-center'>".
                                        $centang.
                                    "</td>".
                                    "<td class='text-center'>".
                                        ($key+1).
                                    "</td>".
                                    "<td class='text-center thumb-daftar'>".
                                        "<img src='".base_url().$dt->thumbnail ."' class='img-rounded' alt='".lang('capt-image-null')."'>".
                                    "</td>".
                                    "<td class='deskripsi'>".
                                        "<p class='nama-pekerjaan'>".ucwords($dt->nama_pekerjaan)."</p>".
                                        "<p>".ucwords($dt->nmkategori)." - ".ucwords($dt->nm_produk)."</p>".
                                        "<p>".$dt->st_finishing."</p>".
                                    "</td>".
                                    "<td class='deskripsi'>".
                                        "<p class='nama-pekerjaan'> No. Order    :".$dt->no_transaksi."</p>".
                                        "<p class='nama-pekerjaan'> No. Faktur   :".$dt->no_faktur."</p>".
                                        "<p>". ucwords($dt->nm) ."</p>".
                                    "</td>".
                                    "<td class='text-center'>".
                                        $dt->jml_cetak.
                                    "</td>".
                                    "<td class='text-center'>".$label."</td>".
                                    "<td class='text-center'>".$label_ambil."</td>".
                                "</tr>";

            }

        }else{

                $tampil     = "";
                $st_ketemu  = 0;
            
        }

        $hasil  = ['st'=>$st_ketemu,'isi'=>$tampil, 'konsumen'=>$konsumen];

        echo json_encode($hasil);

    }

}
