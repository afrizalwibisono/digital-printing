$(function(){
	
	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	$("#konsumen").select2({
        placeholder :"-- Pilih Konsumen --",
        allowClear  : true
  	});  

});

function cetak(obj,e){

	e.preventDefault();
	var id = $(obj).data("id");

	var url 	= baseurl+"collecting/cetak/"+id;

	window.open(url, '_blank', 'width=250,height=400,scrollbars=yes,menubar=no,status=yes,resizable=yes,screenx=100,screeny=100'); return false;

}