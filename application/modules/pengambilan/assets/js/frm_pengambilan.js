$(function(){

	cek_pilihan();

	$("#frm_pengambilan").find("input[name='pil-filter']").click(function(){

		cek_pilihan();

	});

	$("#konsumen").select2({

        placeholder :"-- Pilih Konsumen --",
        allowClear  : true

  	});  

	$("#cr_barcode").keydown(function(e){

		if(e.keyCode == 13){

			var barcode 	= $("#cr_barcode").val();
			var aksi 		= $("input[name='pil-aksi']:checked").val();
			var data 		= {barcode:barcode, aksi:aksi};


	        ambil_data(data);
	        e.preventDefault();

	    }

	});

	$("#filter").click(function(){

		var konsumen 	= $("#konsumen").val();
		var no_faktur	= $("#no_faktur").val();
		var aksi 		= $("input[name='pil-aksi']:checked").val();

		var data 		= {konsumen:konsumen, no_faktur:no_faktur, aksi:aksi};
		
		ambil_data(data);

	});


	$("#simpan").click(function(e){

		cek_centang();
		e.preventDefault();

	});

	$("#nm_sama").click(function(){

		if($(this).is(":checked")){

			var nm_konsumen 	= $("#nm_customer").val();

			$("#nm_pengambil").val(nm_konsumen);

		}

	})


	$("input[name='pil-aksi']").click(function(){

		var aksi = $("input[name='pil-aksi']:checked").val();

		if($("input[name='pil-filter']:checked").val() == 1){

			var barcode 	= $("#cr_barcode").val();
			var data 		= {barcode:barcode, aksi:aksi};

		}else{

			var konsumen 	= $("#konsumen").val();
			var no_faktur	= $("#no_faktur").val();
			var data 		= {konsumen:konsumen, no_faktur:no_faktur, aksi:aksi};

		}

		ambil_data(data);

	});

});

function cek_centang(){

	var baris 		= $("#daftar tr").length - 1;
	var terpilih 	= 0;

	if(baris <= 0){

		alertify.error("Cek kembali status pengambilan barang");

	}else{

		for (var i = 1; i <= baris ; i++) {
			
			var cek 	= $("#daftar tr:eq('"+i+"')").find("input[name='checked[]']").is(":checked");
			if(cek){
				terpilih++;
			}

		}

		if(terpilih == 0){

			alertify.error("Cek kembali status pengambilan barang");

		}else{


			if($("#nm_pengambil").val().length > 0){

				$("#frm_pengambilan").submit();
				$("#st_simpan").val("1");

			}else{

				alertify.error("Pastikan nama PIC yang mengambil barang sudah terisi");

			}	

		}

	}


}

function ambil_data(data){

	$.ajax({
				url 		: baseurl+"pengambilan/buka_data",
				type 		: "post",
				dataType 	: "json",
				data 		: data,
				success	 	: function(msg){

								var st 	= parseFloat(msg['st']);
								$("#daftar tbody tr").remove();

								if(st == 0){

									 alertify.error('Data tidak ditemukan');		
									 
								}else{

									var isi 	= msg['isi'];
									$("#daftar tbody").append(isi);

									$("#nm_customer").val(msg['konsumen']);

								}

							}

	});


}


function cek_pilihan(){

	var isi 	= parseFloat($("#frm_pengambilan").find("input[name='pil-filter']:checked").val());

	$("#cr_barcode").val('');
	$("#konsumen").val('').trigger('change');
	$("#no_faktur").val('');

	if(isi == 0){

		$("#area-manual").show(400);
		$("#area-barcode").hide(400);
		$("#tombol_filter").show(400);

	}else{

		$("#area-manual").hide(400);
		$("#area-barcode").show(400);
		$("#tombol_filter").hide(400);
		$("#cr_barcode").focus();
	}


}