<?php defined('BASEPATH')|| exit('No direct script access allowed');

$lang['title_data']								= 'History Pengambilan';
$lang['title_new']								= 'Pengambilan Barang';
$lang['title_view']								= 'Data History Pengambilan';

// ===================================================================================================================
/* --- Placeholder --- */

$lang['capt-plh-tgl1'] 							= 'Tanggal Awal';
$lang['capt-plh-tgl2'] 							= 'Tanggal Akhir';
$lang['capt-plh-barcode']						= 'Kode Barcode';

$lang['capt-plh-tbl-konsumen']					= 'Konsumen';
$lang['capt-plh-tbl-no-order']					= 'No. Order';
$lang['capt-plh-tbl-no-faktur']					= 'No. Faktur';

$lang['capt-plh-st-sudah-diambil']				= 'Sudah Diambil';
$lang['capt-plh-st-belum-diambil']				= 'Belum Diambil';

// ===================================================================================================================
/* --- View --- */


// ===================================================================================================================
/* --- Index --- */

$lang['capt-index-filter-range']				= 'Waktu';
$lang['capt-index-filter-barcode']				= 'Kode Barcode';
$lang['capt-index-filter-konsumen']				= 'Konsumen';
$lang['capt-index-filter-file']					= 'Judul File';
$lang['capt-index-filter-faktur']				= 'No. Faktur / Nota';
$lang['capt-index-filter-produk']				= 'Produk';

$lang['capt-index-table-deskripsi']				= 'Deskripsi';
$lang['capt-index-table-order']					= 'Order';
$lang['capt-index-table-jml']					= 'JML';
$lang['capt-index-table-status']				= 'Status';

$lang['capt-index-table-dibatalkan']			= 'Dibatalkan';
$lang['capt-index-table-diambil']				= 'Diambil';

// ===================================================================================================================
/* --- Form --- */

$lang['capt-frm-link-filter-manual']			= 'Filter Manual';
$lang['capt-frm-link-filter-barcode']			= 'Filter Barcode';
$lang['capt-frm-link-history']					= 'History';
$lang['capt-frm-konsumen']						= 'Konsumen';
$lang['capt-frm-no-faktur']						= 'No. Faktur';
$lang['capt-frm-status']						= 'Status';

$lang['capt-frm-link-aksi']						= 'Aksi / Proses';
	
	$lang['isi-aksi-pengambilan']				= 'Pengambilan';
	$lang['isi-aksi-pembatalan']				= 'Dibatalkan';

$lang['capt-frm-tbl-deskripsi']					= 'Deskripsi';
$lang['capt-frm-tbl-order']						= 'Order';
$lang['capt-frm-tbl-jml']						= 'Jml';
$lang['capt-frm-tbl-status']					= 'Status';
	
	$lang['isi-status-sudah-collect']			= 'Siap Ambil';
	$lang['isi-status-belum-collect']			= 'Menunggu';

$lang['capt-frm-tbl-status-ambil']				= 'Diambil';

	$lang['isi-status-sudah-ambil']				= 'Sudah';
	$lang['isi-status-belum-ambil']				= 'Belum';

$lang['capt-frm-nm-pengambil']					= 'Nama PIC';
$lang['capt-frm-nm-penjelas']					= '[Optional] sebagai informasi';
$lang['capt-frm-ket']							= 'Keterangan';


$lang['capt-image-null']						= 'Gambar Tidak Ditemukan';

// ===================================================================================================================
$lang['btn_create']								= 'Baru';
$lang['btn_delete']								= 'Hapus';
$lang['btn_filter']								= 'Filter';

// ===================================================================================================================
$lang['btn-save']								= "Simpan";
$lang['btn-batal']								= "Batal / Kembali";
$lang['btn-delete']								= "Hapus";
$lang['btn-filter']								= "Filter";

$lang['bf_or']									= "or";

// ===================================================================================================================
$lang['label_simpan_sukses'] 					= "Simpan Data Pengambilan Barang sukses";
$lang['label_cancel_sukses']					= "Data Pengambilan Barang telah berhasil dibatalkan";

// ===================================================================================================================
$lang['konfirmasi-tidak-ada-perubahan']			= "Tidak ada perubahan pada Data Pengambilan Barang";
$lang['konfirmasi-delete']						= "Apakah anda yakin akan menghapus Data Collecting terpilih ? ";
$lang['konfirmasi-data-tidak-ada']				= "Data tidak ditemukan";
$lang['konfirmasi-error-pil-delete']			= "Tidak ada data Collecting yang dipilih";
$lang['konfirmasi-error-pil-update']			= "Tidak ada data Produk yang dipilih";
$lang['konfirmasi-delete-sukses']				= "Delete data Pelunasan Collecting sukses";



