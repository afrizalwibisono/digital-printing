<?php
		$addPermission 	= has_permission("Pengambilan.Add");

?>


<?= form_open($this->uri->uri_string(), [
											'name'	=> 'frm_index',
											'id'	=> 'frm_index',
											'role'	=> 'form'

										]) ?>


<div class="box box-primary">
	
<div class="box-header">
	
	<div class="box-body form-horizontal">
		
		<div class="col-sm-6">			

			<div class="form-group" style="margin-bottom: 5px">
				<label for="tgl1" class="col-sm-4 control-label"><?= lang("capt-index-filter-range") ?></label>
				<div class="col-sm-7">
					<div class="input-group input-daterange" >
						<input type="text" name="tgl1" id="tgl1" class="form-control" readonly placeholder="<?= lang('capt-seacrh-order-tgl1') ?>" value = "<?= set_value('tgl1',isset($pr['tgl1']) ? $pr['tgl1'] : '') ?>" >
						<span class="input-group-addon">to</span>
						<input type="text" name="tgl2" id="tgl2" class="form-control" readonly placeholder="<?= lang('capt-seacrh-order-tgl2') ?>" value = "<?= set_value('tgl2',isset($pr['tgl2']) ? $pr['tgl2'] : '') ?>" >
					</div>
				</div>
			</div>

			<div class="form-group" style="margin-bottom: 5px">
				<label class="control-label col-sm-4" for="konsumen" ><?= lang('capt-index-filter-konsumen') ?></label>
				<div class="col-sm-7">
					<select class="form-control" name="konsumen" id="konsumen">
						<option></option>
						<?php 
								if(isset($dt_konsumen) && is_array($dt_konsumen) && count($dt_konsumen)):
									foreach ($dt_konsumen as $key => $isi) :
										
						?>

								<option value="<?= $isi->id ?>" <?= set_select('konsumen',$isi->id, isset($pr['konsumen']) && $pr['konsumen'] == $isi->id) ?> ><?= ucwords($isi->nm) ?></option>

						<?php
									endforeach;
								endif;
						?>
					</select>
				</div>
			</div>

			<div class="form-group" style="margin-bottom: 5px">
				<label class="control-label col-sm-4"><?= lang('capt-index-filter-file') ?></label>
				<div class="col-sm-7">
					<input type="text" name="judul_file" id="judul_file" class="form-control" value="<?= set_value("judul_file", isset($pr['judul_file']) ? $pr['judul_file'] : '' ) ?>">
				</div>

			</div>	

		</div>	

		<div class="col-sm-6">
			
			<div class="form-group" style="margin-bottom: 5px">
				<label class="control-label col-sm-4"><?= lang('capt-index-filter-barcode') ?></label>
				<div class="col-sm-7">
					<input type="text" name="kd_barcode" id="kd_barcode" class="form-control" value="<?= set_value("kd_barcode", isset($pr['kd_barcode']) ? $pr['kd_barcode'] : '' ) ?>" >
				</div>
			</div>				

			<div class="form-group" style="margin-bottom: 5px">
				<label class="control-label col-sm-4"><?= lang('capt-index-filter-faktur') ?></label>
				<div class="col-sm-7">
					<input type="text" name="kd_faktur" id="kd_faktur" class="form-control" value = "<?= set_value("kd_faktur", isset($pr['kd_faktur']) ? $pr['kd_faktur'] : '' ) ?>">
				</div>
			</div>				

		</div>	

	</div>

	<div class="box-footer">			

		<?php if($addPermission){ ?>

			<a href="<?= site_url('pengambilan/create') ?>" class='btn btn-success'>
				<?= lang('btn_create') ?>
			</a>	

		<?php } ?>

		<div class="pull-right">

			<button class="btn btn-primary"><?= lang("btn_filter") ?> <span class="fa fa-search"></span></button>
			
		</div>

			
	</div>

</div>	

<?php if(isset($data) && is_array($data) && count($data)): ?>

<div class="box-body table-responsive">
	
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th class="text-center" width="50">#</th>
				<th></th>
				<th class="text-center" colspan="2"><?= lang("capt-index-table-deskripsi") ?></th>
				<th class="text-center" ><?= lang("capt-index-table-order") ?></th>
				<th class="text-center"	><?= lang("capt-index-table-jml") ?></th>
				<th class="text-center" ><?= lang("capt-index-table-status") ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($data as $key => $isi) : ?>
			<tr>
				<td class="text-right" ><?= $numb ?></td>
				<td class="deskripsi">
					<p class='nama-pekerjaan'> <?= "Waktu : ". $isi->waktu ?> </p>
					<p><?= "PIC : ". ucwords($isi->nm_pengambil) ?></p>
				</td>
				<td class="text-center thumb-daftar">
								
					<img src="<?= base_url().$isi->thumbnail ?>" class="img-rounded" alt="<?= lang('capt-image-null') ?>">

				</td>
				<td class="deskripsi" style="width: 20%">
					<p class="nama-pekerjaan"><?= $isi->nama_pekerjaan ?></p>
					<p><?= $isi->nmkategori." - ".$isi->nm_produk ?></p>
					<p><?= $isi->st_finishing ?></p>
					<p>
						<span>
							<a target="_blank" href="<?= site_url('order_produk/view_detail/'.$isi->id_detail_produk_order) ?>" class="text-blue" data-toggle="tooltip" data-placement="left" title="Hapus Order" ><i class="fa fa-folder"></i></a>
						</span>
					</p>
				</td>
				<td class='deskripsi'>
                    <p class='nama-pekerjaan'> <?= "No. Order    : ". $isi->no_transaksi ?> </p>
                    <p class='nama-pekerjaan'> <?= "No. Faktur   : ".$isi->no_faktur ?> </p>
                    <p><?=  ucwords($isi->nm) ?></p>
                </td>
                <td class='text-center'> <?= $isi->jml_cetak ?> </td>
                <?php 
                		if($isi->st == 0){

		                    $label_ambil = "<span class='label label-danger'>".lang('capt-index-table-dibatalkan')."</span>";

		                }else{

		                    $label_ambil = "<span class='label label-success'>".lang('capt-index-table-diambil')."</span>";
		                }
                ?>
                <td class='text-center'><?= $label_ambil ?></td>
			</tr>
			<?php 
				
				$numb++;	
				endforeach; 
			?>
		</tbody>

	</table>

</div>	
<div class="box-footer"></div>	
<?php else: ?>

	<div class="alert alert-info" role="alert">
    	<p><i class="fa fa-warning"></i> &nbsp; <?= lang('konfirmasi-data-tidak-ada') ?></p>
	</div>

<?php endif; ?>

</div>

<?= form_close() ?>