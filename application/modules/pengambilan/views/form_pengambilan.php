<?php
	
		$addPermission 		= has_permission("Pengambilan.Add");
		$managePermission	= has_permission("Pengambilan.Manage");

?>

<?= form_open($this->uri->uri_string(),['name'=>'frm_pengambilan', 'id'=>'frm_pengambilan','role'=>'form']) ?>

<input type="hidden" name="nm_customer" id="nm_customer"> 
<!-- untuk menampung nama customer hasil search, hanya berlaku untuk search barcode n no_faktur / nota  -->

<div class="box box-primary">
	
	<div class="box-header with-border">
		
		<div class="row">
			
			<div class="col-sm-8">
			
				<div class="radio">
					
					<label>
						<input type="radio" name="pil-filter" id="pil-filter-barcode" value="1" checked>
						<?= lang("capt-frm-link-filter-barcode") ?>
					</label>
					
					<label>
						<input type="radio" name="pil-filter" id="pil-filter-manual" value="0">
						<?= lang('capt-frm-link-filter-manual') ?>
					</label>	
					
				</div>

			</div>

		</div>

		<div class="row" id="area-barcode">

			<div class="col-sm-4">
				
				<div class="form-group">

					<div class="input-group">
						<input type="text" name="cr_barcode" id="cr_barcode" class="form-control" placeholder="<?= lang('capt-plh-barcode') ?>">
						<div class="input-group-addon">
							<span class="fa fa-barcode"></span>
						</div>
					</div>	

				</div>

			</div>
			
		</div>

		
		<div class="row" id="area-manual">
			
			<div class="col-sm-4"> 
				<div class="form-group">
					<label class="control-label" style="margin-bottom: 0px"><?= lang('capt-frm-konsumen') ?></label>
					<select class="form-control" name="konsumen" id="konsumen">
						<option></option>
						<?php 
								if(isset($dt_konsumen) && is_array($dt_konsumen) && count($dt_konsumen)):
									foreach ($dt_konsumen as $key => $isi) :
										
						?>

								<option value="<?= $isi->id ?>" <?= set_select('konsumen',$isi->id, isset($konsumen) && $konsumen == $isi->id) ?> ><?= ucwords($isi->nm) ?></option>

						<?php
									endforeach;
								endif;
						?>
					</select>
				</div>
			</div>
			<div class="col-sm-4"> 
				<div class="form-group">
					<label class="control-label" style="margin-bottom: 0px" ><?= lang('capt-frm-no-faktur') ?></label>
					<input type="text" name="no_faktur" id="no_faktur" class="form-control">
				</div>
			</div>

		</div>

		<div class="row" > <!-- Area Button -->
			
			<div class="col-sm-12">

				<a href="<?= site_url('pengambilan') ?>" class="btn btn-default" title="<?= lang('capt-frm-link-history') ?>">
					<?= lang('capt-frm-link-history') ?> <span class="fa fa-book"></span>
				</a>

				<div class="pull-right" id="tombol_filter">

					<button class="btn btn-primary" type="button" name="filter" id="filter">
						<?= lang('btn_filter') ?> <span class="fa fa-search"></span>
					</button>

				</div>

			</div>

		</div>

	
	</div>

	<div class="box-body form-horizontal">
		
		<div class="radio">
			<label style="padding-left: 0px" class="text-danger"><?= lang("capt-frm-link-aksi") ?> : </label>
			<label>
				<input type="radio" name="pil-aksi" id="pil-ambil" value="1" checked>
				<?= lang("isi-aksi-pengambilan") ?>
			</label>
			<?php if($managePermission): ?>		
			<label>
				<input type="radio" name="pil-aksi" id="pil-batal" value="0">
				<?= lang("isi-aksi-pembatalan") ?>
			</label>	
			<?php endif; ?>
		</div>		

		<div class="table-responsive">

			<table class="table table-bordered" id="daftar">
				<thead>
					<tr class="success">
						<th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
						<th class="text-center" width="50">#</th>
						<th class="text-center" colspan="2"><?= lang("capt-frm-tbl-deskripsi") ?></th>
						<th class="text-center" ><?= lang("capt-frm-tbl-order") ?></th>
						<th class="text-center"><?= lang("capt-frm-tbl-jml") ?></th>
						<th class="text-center" ><?= lang("capt-frm-tbl-status") ?></th>
						<th class="text-center" ><?= lang("capt-frm-tbl-status-ambil") ?></th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
			
		</div>

		<div class="form-group">
			<label class="control-label col-sm-2" for="nm_pengambil"><?= lang('capt-frm-nm-pengambil') ?></label>
			<div class="col-sm-4">

				<div class="input-group">
					<div class="input-group-addon">
						<input type="checkbox" name="nm_sama" id="nm_sama">
					</div>
					<input type="text" name="nm_pengambil" id="nm_pengambil" class="form-control">
				</div>

				<small class="text-danger"><?= lang('capt-frm-nm-penjelas') ?></small>

			</div>

		</div>

		<div class="form-group">
			<label class="control-label col-sm-2" for="ket"><?= lang('capt-frm-ket') ?></label>
			<div class="col-sm-4">
				<textarea class="form-control" name="ket" id="ket"></textarea>
			</div>

		</div>		

		<div class="form-group">
			<div class="col-sm-offset-2 col-md-4">
				<input type="hidden" name="st_simpan" id="st_simpan" value="0">
				<button type="submit" name="simpan" id="simpan" class="btn btn-primary" >
    				<?= lang("btn-save") ?>
    			</button>
    			<?php echo lang("bf_or")." ".anchor('pengambilan/batal', lang('btn-batal')) ?>
			</div>
		</div>

	</div>

	
</div>

<?= form_close(); ?>