<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_laporan','name'=>'frm_laporan','class' => 'form-inline')) ?>
        <div class="box-header hidden-print">
            <div class="pull-left">
                <button type="button" id="cetak" class="btn btn-default" onclick="cetak_halaman()"><i class="fa fa-print"></i> <?= lang('cetak') ?></button>
            </div>
            <div class="pull-right">
                <div class="form-group">
                    <div class="input-daterange input-group" title="<?= lang('range_tgl') ?>" data-toggle="tooltip" data-placement="top">
                        <input type="text" style="width: 120px" class="form-control" name="tgl_awal" id="tgl_awal" value="<?= isset($period1) ? $period1 : '' ?>" placeholder="<?= lang('tgl_awal') ?>" readonly />
                        <span class="input-group-addon">to</span>
                        <input type="text" style="width: 120px" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?= isset($period2) ? $period2 : '' ?>" placeholder="<?= lang('tgl_akhir') ?>" readonly />
                    </div>
                </div>
                <div class="form-group">
                    <select name="st_konsumen" id="st_konsumen" class="form-control">
                        <option value="" <?= set_select('st_konsumen','',isset($st_konsumen) && $st_konsumen == '') ?> ><?= lang("lbl_isi_kategori_all") ?></option>
                        <option value="0" <?= set_select('st_konsumen','0',isset($st_konsumen) && $st_konsumen == '0') ?> ><?= lang("lbl_isi_kategori_0") ?></option>
                        <option value="1" <?= set_select('st_konsumen','1',isset($st_konsumen) && $st_konsumen == '1') ?>><?= lang("lbl_isi_kategori_1") ?></option>
                        <option value="2" <?= set_select('st_konsumen','2',isset($st_konsumen) && $st_konsumen == '2') ?> ><?= lang("lbl_isi_kategori_2") ?></option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="hidden" name="isi_konsumen" id="isi_konsumen" value="<?= set_value('konsumen', isset($konsumen) ? $konsumen : '') ?>">
                    <select name="konsumen" id="konsumen" class="form-control" style="min-width: 300px !important" >
                        <option value=""></option>
                    </select>
                </div>
                <form class="form-group">
                    <button class="btn btn-default"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>
    <?= form_close() ?>
    <?php if($idt) : ?>
    <div class="row header-laporan">
        <div class="col-md-12">
            <div class="col-md-1 laporan-logo">
                <img src="<?= base_url('assets/images/'.$idt->logo) ?>" alt="logo">
            </div>
            <div class="col-md-6 laporan-identitas">
                <address>
                    <h4><?= $idt->nm_perusahaan ?></h4>
                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?>
                </address>
            </div>
            <div class="col-md-5 text-right">
                <h3><?= lang("judul_piutang") ?></h3>
                <h5>Periode <?= $period1." - ".$period2 ?></h5>
            </div>
            <div class="clearfix"></div>
            <div class="laporan-garis"></div>
        </div>
    </div>
    <?php endif ?>
	<?php if (isset($data) && is_array($data) && count($data)) : ?>
    	
        <div class="box-body laporan-body">
            
            <h4><?= lang("lbl_resume_piutang") ?></h4>
            <table class="table-condensed table-summary">

                <?php 

                    $total = 0;
                    foreach ($data as $key => $hitung) {
                        $total += $hitung->sisa_tagihan;
                    }

                ?>

                <tr>
                    <td width="200"><?= lang('lbl_jml_nota_piutang') ?></td>
                    <td width="10">:</td>
                    <td><strong> <?= count($data) ?> </strong></td>
                </tr>

                <tr>
                    <td width="200"><?= lang('lbl_total_piutang') ?></td>
                    <td width="10">:</td>
                    <td><strong><?= number_format($total) ?> </strong></td>
                </tr>
            
            </table>

            <h4><?= lang('lbl_detail_piutang') ?></h4>
            
            <div class="table-responsive no-padding">
                <table class="table table-condensed table-bordered table-detail">
                    <thead>
                        <tr class="bg-success">
                            <th width="30" class="text-center">No.</th>
                            <th class="text-center"><?= lang("lbl_jdl_tbl_nonota_piutang") ?></th>
                            <th class="text-center"><?= lang("lbl_jdl_tbl_tgl_piutang") ?></th>
                            <th class="text-center"><?= lang("lbl_jdl_tbl_konsumen_piutang") ?></th>
                            <th class="text-center"><?= lang("lbl_jdl_tbl_total_piutang") ?></th>
                            <th class="text-center"><?= lang("lbl_jdl_tbl_bayar_piutang") ?></th>
                            <th class="text-center"><?= lang("lbl_jdl_tbl_sisa_piutang") ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $key => $isi) : ?>
                            <tr>
                                <td class="text-right">
                                    <?= str_pad($key+1, 2,"0",STR_PAD_LEFT) ?>
                                </td>
                                <td class="text-center">
                                    <?= $isi->no_faktur ?>
                                </td>
                                <td class="text-center">
                                    <?= $isi->waktu_tampil ?>
                                </td>
                                <td class="text-center">
                                    <?= $isi->nm." ; ". $isi->st_konsumen ?>
                                </td>
                                <td class="text-center">
                                    <?= number_format($isi->grand_total) ?>
                                </td>
                                <td class="text-right">
                                    <?= number_format($isi->total_sudah_bayar) ?>
                                </td>
                                <td class="text-right">
                                    <strong><?= number_format($isi->sisa_tagihan) ?></strong>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>




            <!-- <?php //foreach ($data as $key => $dt) : ?>

            <div class="table-responsive no-padding">
                <table class="table-condensed table-head">
                    <tr>
                        <td><?= lang("lbl_isi_dtkat_konsumen") ?></td>
                        <td>:</td>
                        <td><strong><?= $dt['konsumen']." ; ".$dt['st_konsumen'] ?></strong></td>
                    </tr>
                </table>
            </div>    

            <div class="table-responsive no-padding">
                <table class="table table-condensed table-bordered table-detail">
                    <thead>
                        <tr class="bg-success">
                            <th width="50"></th>
                            <?php foreach ($dt['dt_trans'] as $key => $isi) :?>
                                <th class="text-center"><?= $isi['nm_kategori'] ?></th>
                            <?php endforeach; ?>    
                        </tr>
                        <tr>
                            <th class="bg-success text-center">Total</th>
                            <?php foreach ($dt['dt_trans'] as $key => $isi) :?>
                                <td class="text-center"><?= number_format($isi['value']) ?></td>
                            <?php endforeach; ?>    
                        </tr>
                    </thead>
                </table>
            </div>

            <?php //endforeach; ?>
 -->
        </div><!-- end box-body -->

	<?php else: ?>
    <div class="alert alert-info" role="alert">
            <p><i class=fa fa-warning"></i> &nbsp; <?= lang('lbl_data_tidak_ditemukan') ?></p>
    </div>
    <?php endif;?>
</div><!-- /.box -->