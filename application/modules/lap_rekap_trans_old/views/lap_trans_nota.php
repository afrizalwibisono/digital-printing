<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),[

												'name' 	=> 'frm_tran_nota',
												'id' 	=> 'frm_tran_nota',
												'role'	=> 'form'
												//'class' => 'form-horizontal'
											]) ?>
		
	<div class="box-header hidden-print">
		
		<div class="row">
		
			<div class="col-sm-3">
				<div class="form-group">
					<label class="control-label" for="tgl1">Tgl. Trans</label>
					<div class="input-group input-daterange">
						<input type="text" name="tgl1" id="tgl1" class="form-control" placeholder="<?= lang("tgl_awal") ?>" value="<?= set_value('tgl1',isset($filter['tgl1']) ? $filter['tgl1'] : '' ) ?>" >
						<span class="input-group-addon">to</span>
						<input type="text" name="tgl2" id="tgl2" class="form-control" placeholder="<?= lang("tgl_akhir") ?>" value="<?= set_value('tgl2',isset($filter['tgl2']) ? $filter['tgl2'] : '' ) ?>">
					</div>
				</div>
			</div>

			<div class="col-sm-3">
				<div class="form-group">
					<label class="control-label" for="posisi">Lokasi Transaksi</label>
					<select class="form-control" name="posisi" id="posisi" >
						<option></option>
						<option value="1" <?= set_select('posisi',1,isset($filter['st_kasir']) && $filter['st_kasir'] === 1 ) ?> >Kasir</option>
						<option value="0" <?= set_select('posisi',0,isset($filter['st_kasir']) && $filter['st_kasir'] === 0 ) ?> >Accounting</option>
					</select>
				</div>
			</div>

			<div class="col-sm-3">
				<div class="form-group">
					<label class="control-label" for="jns_order">Jenis Order</label>
					<select class="form-control" name="jns_order" id="jns_order" style="min-width: 100px">
						<option></option>
						<option value="0" <?= set_select('jns_order','0',isset($filter['st_po']) && $filter['st_po'] === 0 ) ?> >Non PO</option>
						<option value="1" <?= set_select('jns_order','1',isset($filter['st_po']) && $filter['st_po'] === 1 ) ?> >PO</option>
					</select>	
					
				</div>
			</div>

			<div class="col-sm-3">
				<div class="form-group">
					<label class="control-label" for="st_lunas">Status Transaksi</label>	
					<select class="form-control" name="st_lunas" id="st_lunas" style="min-width: 100px">
						<option></option>
						<option value="0" <?= set_select('st_lunas','0',isset($filter['st_lunas']) && $filter['st_lunas'] === 0 ) ?> >Hutang</option>
						<option value="1" <?= set_select('st_lunas','1',isset($filter['st_lunas']) && $filter['st_lunas'] === 1 ) ?> >Lunas</option>
					</select>
				</div>
				
			</div>

		</div><!-- end row -->		

		<div class="row">
			
			<div class="col-sm-3">
				<div class="form-group">
					<label class="control-label" for="gol_konsumen">Gol. Konsumen</label>	
					<select class="form-control" name="gol_konsumen" id="gol_konsumen" >
						<option></option>
						<option value="0" <?= set_select('gol_konsumen','0',isset($filter['gol_kons']) && $filter['gol_kons'] === 0 ) ?> >Konsumen</option>
						<option value="1" <?= set_select('gol_konsumen','1',isset($filter['gol_kons']) && $filter['gol_kons'] === 1 ) ?> >Reseller</option>
						<option value="2" <?= set_select('gol_konsumen','2',isset($filter['gol_kons']) && $filter['gol_kons'] == '2' ) ?> >Instansi</option>
					</select>	
				</div>
			</div>			

			<div class="col-sm-3">
				<div class="form-group">
					<label class="control-label" for="konsumen">Konsumen</label>	
					<input type="hidden" name="id_konsumen_pilih" id="id_konsumen_pilih" value="<?= set_value('konsumen',isset($filter['konsumen']) ? $filter['konsumen'] : '' ) ?>" >
					<select class="form-control" name="konsumen" id="konsumen" >
						<option></option>
					</select>	
				</div>
			</div>	

			<div class="col-sm-6">
				<div class="form-group">
					<label class="control-label" for="pil_produk">Produk</label>	
					<?=
						form_dropdown('pil_produk[]',$isi_produk,$pil_produk,
                                    [   'id'        => "pil_produk",
                                        'multiple'  => "multiple",
                                        'class'     => "form-control select2-multi"
                                    ]);
					?>
				</div>	
			</div>

		</div>

		<!-- 
		<div class="pull-right">
			
			<div class="form-group">
				<div class="input-group input-daterange">
					<input type="text" name="tgl1" id="tgl1" style="width: 120px" class="form-control" placeholder="<?= lang("tgl_awal") ?>" value="<?= set_value('tgl1',isset($filter['tgl1']) ? $filter['tgl1'] : '' ) ?>" >
					<span class="input-group-addon">to</span>
					<input type="text" name="tgl2" id="tgl2" style="width: 120px" class="form-control" placeholder="<?= lang("tgl_akhir") ?>" value="<?= set_value('tgl2',isset($filter['tgl2']) ? $filter['tgl2'] : '' ) ?>">
				</div>
			</div>

			<div class="form-group">
				<select class="form-control" name="posisi" id="posisi" style="min-width: 120px">
					<option></option>
					<option value="1" <?= set_select('posisi',1,isset($filter['st_kasir']) && $filter['st_kasir'] === 1 ) ?> >Kasir</option>
					<option value="0" <?= set_select('posisi',0,isset($filter['st_kasir']) && $filter['st_kasir'] === 0 ) ?> >Accounting</option>
				</select>
			</div>

			<div class="form-group">
				<select class="form-control" name="jns_order" id="jns_order" style="min-width: 100px">
					<option></option>
					<option value="0" <?= set_select('jns_order','0',isset($filter['st_po']) && $filter['st_po'] === 0 ) ?> >Non PO</option>
					<option value="1" <?= set_select('jns_order','1',isset($filter['st_po']) && $filter['st_po'] === 1 ) ?> >PO</option>
				</select>
			</div>

			<div class="form-group">
				<select class="form-control" name="st_lunas" id="st_lunas" style="min-width: 100px">
					<option></option>
					<option value="0" <?= set_select('st_lunas','0',isset($filter['st_lunas']) && $filter['st_lunas'] === 0 ) ?> >Hutang</option>
					<option value="1" <?= set_select('st_lunas','1',isset($filter['st_lunas']) && $filter['st_lunas'] === 1 ) ?> >Lunas</option>
				</select>
			</div>

			<div class="form-group">
				<select class="form-control" name="gol_konsumen" id="gol_konsumen" style="min-width: 120px">
					<option></option>
					<option value="0" <?= set_select('gol_konsumen','0',isset($filter['gol_kons']) && $filter['gol_kons'] === 0 ) ?> >Konsumen</option>
					<option value="1" <?= set_select('gol_konsumen','1',isset($filter['gol_kons']) && $filter['gol_kons'] === 1 ) ?> >Reseller</option>
					<option value="2" <?= set_select('gol_konsumen','2',isset($filter['gol_kons']) && $filter['gol_kons'] == '2' ) ?> >Instansi</option>
				</select>
			</div>

			<div class="form-group">
				<input type="hidden" name="id_konsumen_pilih" id="id_konsumen_pilih" value="<?= set_value('konsumen',isset($filter['konsumen']) ? $filter['konsumen'] : '' ) ?>" >
				<select class="form-control" name="konsumen" id="konsumen" style="min-width: 200px">
					<option></option>
				</select>
			</div>

		</div>
	 	-->	

		<div class="clearfix"></div>
		
		<hr style="margin-top: 8px;margin-bottom: 5px">
		<div class="pull-left">
			<button type="button" id="cetak" class="btn btn-default" onclick="cetak_halaman()"><i class="fa fa-print"></i> Cetak</button>
		</div>
		<div class="pull-right">
			<button type="submit" id="cari" name="cari" class="btn btn-default"><i class="fa fa-search"></i> Filter</button>
		</div>

	</div>	
	<?= form_close() ?>

	<?php if($idt) : ?>
    <div class="row header-laporan">
        <div class="col-md-12">
            <div class="col-md-1 laporan-logo">
                <img src="<?= base_url('assets/images/'.$idt->logo) ?>" alt="logo">
            </div>
            <div class="col-md-6 laporan-identitas">
                <address>
                    <h4><?= $idt->nm_perusahaan ?></h4>
                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?>
                </address>
            </div>
            <div class="col-md-5 text-right">
                <h3><?= lang("trans-order-jdl-lap") ?></h3>
                <h5>Periode Cetak <strong><?= $filter["tgl1"]." - ".$filter["tgl2"] ?></strong></h5>
            </div>
            <div class="clearfix"></div>
            <div class="laporan-garis"></div>
        </div>
    </div>
    <?php endif ?>

    <?php if(isset($data) && is_array($data) && count($data)) : ?>
    <div class="box-body laporan-body">
    	
    	<h4><?= lang('trans-order-resume-jdl') ?></h4>
    	<table class="table-condensed table-summary">
    		<tbody>
    			<tr>
    				<td width="200"><?= lang('trans-order-resume-jml') ?></td>
    				<td width="10">:</td>
    				<td><strong><?= $jml_trans ?></strong> Kali</td>
    			</tr>	
    			<tr>
    				<td><?= lang('trans-order-resume-ttl') ?></td>
    				<td width="10">:</td>
    				<td><strong> Rp. <?= number_format($total_trans) ?></strong></td>
    			</tr>	
    			<tr>
    				<td><?= lang('trans-order-resume-piutang') ?></td>
    				<td width="10">:</td>
    				<td><strong> Rp. <?= number_format($total_piutang) ?></strong></td>
    			</tr>	
    		</tbody>
    	</table>

    	<?php if(isset($resume) && is_array($resume) && count($resume)): ?>		

    	<table class="table table-condensed table-bordered table-detail" >
    		<thead>
    			<tr class="success">
    				<th class="text-center" ><?= lang('trans-order-resume-kt') ?></th>
    				<th class="text-center" ><?= lang('trans-order-resume-ttlcetak') ?></th>
    				<th class="text-center" ><?= lang('trans-order-resume-ttldesign') ?></th>
    				<th class="text-center" ><?= lang('trans-order-resume-ttlfinish') ?></th>
    			</tr>
    		</thead>
    		<tbody>
    			<?php foreach ($resume as $key => $rs) : ?>
    			
    			<tr>
    				<td class="text-center success"><?= $rs['nm'] ?></td>
    				<td class="text-center"><?= number_format($rs['cetak']) ?></td>
    				<td class="text-center"><?= number_format($rs['design']) ?></td>
    				<td class="text-center"><?= number_format($rs['finishing']) ?></td>
    			</tr>

    			<?php endforeach; ?>
    		</tbody>
    	</table>

    	<?php endif; ?>

    	<h4><?= lang('trans-order-detail-jdl') ?></h4>
    	<?php foreach ($data as $key => $isi) : ?>
    	<div class="table-responsive no-padding">
	    	<table class="table-condensed table-head">
	    		<tbody>
	    			<tr>
	    				<td><?= lang('trans-order-head-nota') ?></td>
	    				<td>:</td>
	    				<td>
	    					<strong><?= $isi['no_nota'] ?></strong>
	    				</td>
	    				<td><?= lang('trans-order-head-konsumen') ?></td>
	    				<td>:</td>
	    				<td>
	    					<strong><?= $isi['konsumen']." | ".strtoupper($isi['gol_konsumen']) ?></strong>
	    				</td>
	    				<td><?= lang('trans-order-head-lokasi') ?></td>
	    				<td>:</td>
	    				<td>
	    					<?= $isi['st_kasir'] ?>
	    				</td>
	    			</tr>
	    			<tr>
	    				<td><?= lang('trans-order-head-nota-master') ?></td>
	    				<td>:</td>
	    				<td>
	    					<?php 
		    					if($isi['st_master'] == 0){
		    						echo $isi['no_nota_master'];
		    					}
	    					?>
	    				</td>
	    				<td><?= lang('trans-order-head-tempo') ?></td>
	    				<td>:</td>
	    				<td><?= $isi['tgl_tempo'] ?></td>
	      				<td><?= lang('trans-order-head-op') ?></td>
	    				<td>:</td>
	    				<td><?= $isi['op'] ?></td>
	    			</tr>
	    			<tr>
	    				<td><?= lang('trans-order-head-waktu') ?></td>
	    				<td>:</td>
	    				<td><?= $isi['waktu'] ?></td>
	    				<td><?= lang('trans-order-head-lunas') ?></td>
	    				<td>:</td>
	    				<td>
	    					<strong><?= $isi['st_lunas'] ?></strong>
	    				</td>
	    			</tr>

	    		</tbody>
	    	</table>
    	</div>
    	<div class="table-responsive no-padding">
    		<table class="table table-condensed table-bordered table-detail">
    			<thead>
    				<tr class="success">
    					<th class="text-center" width="10">#</th>
    					<th class="text-center"><?= lang('trans-order-tbl-kategori') ?></th>
    					<th class="text-center"><?= lang('trans-order-tbl-produk') ?></th>
    					<th class="text-center"><?= lang('trans-order-tbl-uk') ?></th>
    					<th class="text-center"><?= lang('trans-order-tbl-harga') ?></th>
    					<th class="text-center"><?= lang('trans-order-tbl-pot') ?></th>
    					<th class="text-center"><?= lang('trans-order-tbl-jml') ?></th>
    					<th class="text-center"><?= lang('trans-order-tbl-b_cetak') ?></th>
    					<th class="text-center"><?= lang('trans-order-tbl-b_design') ?></th>
    					<th class="text-center"><?= lang('trans-order-tbl-b_finish') ?></th>
    					<th class="text-center"><?= lang('trans-order-tbl-b_subtotal') ?></th>
    				</tr>
    			</thead>
    			<tbody>
    				<?php foreach ($isi['detail'] as $key_d => $dti) : ?>

    					<tr>
    						<td class="text-center"><?= ($key_d+1) ?></td>
    						<td class="text-center"><?= strtoupper($dti['kategori']) ?></td>
    						<td class="text-center"><?= strtoupper($dti['nm_produk']) ?></td>
    						<td class="text-center">
    							
    							<?php 

    								if($dti['tipe_produk'] == 1){

    									echo $dti['uk_cetak'];

    								}

    							?>

    						</td>
    						<td class="text-center"><?= $dti['harga'] ?></td>
    						<td class="text-center"><?= $dti['pot'] ?></td>
    						<td class="text-center"><?= $dti['jml_cetak'] ?></td>
    						<td class="text-center"><?= number_format($dti['biaya_cetak']) ?></td>
    						<td class="text-center"><?= number_format($dti['biaya_design']) ?></td>
    						<td class="text-center"><?= number_format($dti['biaya_finishing']) ?></td>
    						<td class="text-right"><strong><?= number_format($dti['subtotal']) ?></strong></td>
    					</tr>

    				<?php endforeach; ?>	
    			
    			
    				<tr class="border-top">
    					<td class="text-right" colspan="10"><strong><?= lang('trans-order-tbl-g_total') ?></strong></td>
    					<td class="text-right" ><strong><?= number_format($isi['gtotal']) ?></strong></td>
    				</tr>
    				<tr>
    					<td class="text-right" colspan="10"><strong><?= lang('trans-order-tbl-total_bayar') ?></strong></td>
    					<td class="text-right" ><strong><?= number_format($isi['bayar']) ?></strong></td>
    				</tr>
    				<tr>
    					<td class="text-right" colspan="10"><strong><?= lang('trans-order-tbl-kurang_bayar') ?></strong></td>
    					<td class="text-right" ><strong><?= number_format($isi['kurang_bayar']) ?></strong></td>
    				</tr>
    			</tbody>
    		</table>
    	</div>
    	<?php endforeach ?>

    	
    </div><!-- akhir dari box-body -->
    <?php else : ?>
	    <div class="alert alert-info" role="alert">
	            <p><i class=fa fa-warning"></i> &nbsp; <?= lang('lbl_data_tidak_ditemukan') ?></p>
	    </div>
	<?php endif ?>

</div>