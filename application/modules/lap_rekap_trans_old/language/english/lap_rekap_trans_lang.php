<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ==========================================================================

// ..:: Untuk Rekap A3 by Trans ::.. 

$lang['title-a3-trans']					= 'Laporan Cetak A3 by Transaksi';
$lang['title-a3-jdl-lap']				= 'Rekap Cetak A3 by Transaksi';

$lang['trans-a3-resume-jdl']			= 'Resume';
$lang['trans-a3-detail-jdl']			= 'Detail Transaksi';

$lang['trans-a3-head-nota']				= 'No. Nota';
$lang['trans-a3-head-nota-master']		= 'No. Nota-Ref';
$lang['trans-a3-head-waktu']			= 'Waktu Transaksi';
$lang['trans-a3-head-konsumen']			= 'Konsumen';
$lang['trans-a3-head-lokasi']			= 'Fasilitas';
$lang['trans-a3-head-op']				= 'Operator';

$lang['trans-a3-tbl-file']				= 'Nama File';
$lang['trans-a3-tbl-produk']			= 'Produk';
$lang['trans-a3-tbl-cetak']				= 'Bahan';
$lang['trans-a3-tbl-jml-order']			= 'Jml Order';
$lang['trans-a3-tbl-jml-a3']			= 'Jml Cetak A3';

// ..:: Untuk Report Transaksi Order ::..
$lang['title-trans-order']				= 'Laporan Rekap Transaksi Order';
$lang['trans-order-jdl-lap']			= 'Rekap Transaksi Order';

$lang['trans-order-resume-jdl']			= 'Resume Transaksi';
$lang['trans-order-resume-jml']			= 'Jumlah Transaksi';
$lang['trans-order-resume-ttl']			= 'Total Transaksi';
$lang['trans-order-resume-piutang']		= 'Total Piutang';

$lang['trans-order-resume-kt']			= 'Kategori';
$lang['trans-order-resume-ttlcetak']	= 'Biaya Cetak';
$lang['trans-order-resume-ttldesign']	= 'Biaya Design';
$lang['trans-order-resume-ttlfinish']	= 'Biaya Finishing';

$lang['trans-order-detail-jdl']			= 'Detail Transaksi';

$lang['trans-order-head-nota']			= 'No. Nota';
$lang['trans-order-head-nota-master']	= 'No. Nota-Ref';
$lang['trans-order-head-waktu']			= 'Waktu Transaksi';
$lang['trans-order-head-konsumen']		= 'Konsumen';
$lang['trans-order-head-tempo']			= 'Tgl Tempo';
$lang['trans-order-head-lunas']			= 'Status Nota';
$lang['trans-order-head-lokasi']		= 'Fasilitas';
$lang['trans-order-head-op']			= 'Operator';

$lang['trans-order-tbl-kategori']		= 'Kategori';
$lang['trans-order-tbl-produk']			= 'Produk';
$lang['trans-order-tbl-uk']				= 'Ukuran';
$lang['trans-order-tbl-harga']			= 'Harga';
$lang['trans-order-tbl-pot']			= 'Potongan';
$lang['trans-order-tbl-jml']			= 'Jml';
$lang['trans-order-tbl-b_cetak']		= 'Cetak(Rp)';
$lang['trans-order-tbl-b_design']		= 'Design(Rp)';
$lang['trans-order-tbl-b_finish']		= 'Finishing(Rp)';
$lang['trans-order-tbl-b_subtotal']		= 'Subtotal';

$lang['trans-order-tbl-g_total']		= 'Grand Total';
$lang['trans-order-tbl-total_bayar']	= 'Total Bayar';
$lang['trans-order-tbl-kurang_bayar']	= 'Kurang Bayar';

// ==========================================================================
// ..:: Untuk Report Penjualan Kategori ::..

$lang['title_jual_kategori']			= 'Laporan Rekap Transaksi Kategori';
$lang['judul_jual_kategori']			= 'Rekap Transaksi Kategori';

$lang['lbl_resume_kategori']			= 'Resume Transaksi Kategori';
$lang['lbl_detail_kategori']			= 'Detail Transaksi Kategori';

// ==========================================================================
// ..:: Untuk Report Penjualan Produk ::..

$lang['title_jual_produk']				= 'Laporan Rekap Transaksi Produk';
$lang['judul_jual_produk']				= 'Rekap Transaksi Produk';

$lang['lbl_resume_produk']				= 'Resume Transaksi';
$lang['lbl_detail_produk']				= 'Detail Transaksi Produk';

$lang['lbl_head_group_konsumen_produk']	= 'Konsumen';
$lang['lbl_head_group_kategori_produk']	= 'Kategori';

$lang['lbl_total_foot_group_produk']	= 'Total';

$lang['lbl_jdl_tbl_nm_produk']			= 'Produk';
$lang['lbl_jdl_tbl_kategori_produk']	= 'Kategori';
$lang['lbl_jdl_tbl_cetak_produk']		= '(n)Cetak';
$lang['lbl_jdl_tbl_total_produk']		= 'Total Transaksi';

// ==========================================================================
// ..:: Untuk Report Penjualan Kategori ::..

$lang['title_piutang']					= 'Laporan Rekap Piutang';
$lang['judul_piutang']					= 'Rekap Piutang';

$lang['lbl_resume_piutang']				= 'Resume Piutang';
$lang['lbl_detail_piutang']				= 'Detail Data Piutang';

$lang['lbl_jml_nota_piutang']			= 'Jumlah Nota';
$lang['lbl_total_piutang']				= 'Total Piutang';

$lang['lbl_jdl_tbl_nonota_piutang']		= 'No. Nota';
$lang['lbl_jdl_tbl_konsumen_piutang']	= 'Konsumen';
$lang['lbl_jdl_tbl_tgl_piutang']		= 'Tgl Trans';
$lang['lbl_jdl_tbl_total_piutang']		= 'Total Transaksi';
$lang['lbl_jdl_tbl_bayar_piutang']		= 'Bayar';
$lang['lbl_jdl_tbl_sisa_piutang']		= 'Piutang';

// ==========================================================================
// ..:: Untuk Report Kas Kasir ::..

$lang['title_kas_kasir']					= 'Laporan Rekap Closing Kas Kasir';
$lang['judul_kas_kasir']					= 'Rekap Closing Kas Kasir';

$lang['resume_kasir']						= 'Resume Kas Kasir';
$lang['ttl_cash_masuk_kasir']				= 'Cash Masuk';
$lang['ttl_cash_keluar_kasir']				= 'Cash Keluar';
$lang['ttl_debit_kasir']					= 'Debit';
$lang['ttl_transfer_kasir']					= 'Transfer';
$lang['ttl_deposit_kasir']					= 'Deposit';
$lang['ttl_closing_kasir']					= 'Closing Kasir';

$lang['jdl_detail_data']					= 'Data Kas Kasir';
$lang['lbl_jdl_tbl_tgl_kasir']				= 'Tanggal';
$lang['lbl_jdl_tbl_op_kasir']				= 'Kasir';
$lang['lbl_jdl_tbl_saldo_awal_kasir']		= 'Saldo Awal';
$lang['lbl_jdl_tbl_masuk_kasir']			= 'Kas Masuk';
$lang['lbl_jdl_tbl_keluar_kasir']			= 'Kas Keluar';
$lang['lbl_jdl_tbl_debit_kasir']			= 'Debit';
$lang['lbl_jdl_tbl_transfer_kasir']			= 'Transfer';
$lang['lbl_jdl_tbl_piutang_kasir']			= 'Piutang';
$lang['lbl_jdl_tbl_saldo_akhir_kasir']		= 'Saldo Akhir';
$lang['lbl_jdl_tbl_saldo_akhir_ref_kasir']	= 'Saldo Akhir Ref';

// ==========================================================================
// ..:: Umum ::..

$lang['tgl']							= 'Tanggal';
$lang['range']							= 'Range Tanggal';
$lang['tgl_awal']						= 'Tgl Awal';
$lang['tgl_akhir']						= 'Tgl Akhir';

$lang['lbl_isi_kategori_0']				= 'Umum / Konsumen Biasa';
$lang['lbl_isi_kategori_1']				= 'Reseller';
$lang['lbl_isi_kategori_2']				= 'Instansi / Perusahaan';
$lang['lbl_isi_kategori_all']			= '';

$lang['lbl_isi_dtkat_konsumen']			= 'Konsumen';

$lang['lbl_isi_grouping_konsumen']		= 'By Konsumen';
$lang['lbl_isi_grouping_produk']		= 'By Produk';

// ==========================================================================
$lang['lbl_data_tidak_ditemukan']		= 'Data Tidak Ditemukan';