<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for rekap data transaksi

*/
class Lap_rekap_trans extends Admin_Controller {

	protected $viewPermission_rekapKategory     = "Laporan Rekap Penjualan by Kategory.View";
    protected $viewPermission_rekapProduk       = "Laporan Rekap Penjualan by Produk.View";
    protected $viewPermission_rekapPiutang      = "Laporan Rekap Piutang.View";
    protected $viewPermission_rekapKasKasir     = "Laporan Rekap Kas Kasir.View";
    protected $viewPermission_rekapTransNota    = "Laporan Transaksi Order.View";
    protected $viewPermission_rekapA3Cetak      = "Rekap Cetak A3 by Transaksi.View";


    public function __construct(){

        parent::__construct();

        $this->lang->load('lap_rekap_trans/lap_rekap_trans');
        $this->load->model(array(
                                    "konsumen_model",
                                    "trans_model",
                                    "kategori_model",
                                    "satuan_model",
                                    "karyawan_model",
                                    "kas_kasir_model",
                                    "produk_model"

        						)
    						);

        //$this->template->title(lang('title'));
		$this->template->page_icon('fa fa-file-o');
    }

    public function index(){

        
    }

    function get_konsumen(){

        if(!$this->input->is_ajax_request()){

            redirect("lap_rekap_trans");

        }

        $st_konsumen    = $this->input->post("st");

        $data   = $this->konsumen_model->select("`idkonsumen` as id,
                                                CONCAT(`konsumen`.`panggilan`,
                                                ' ',
                                                `konsumen`.`nama`) AS nm")
                        ->where("st = {$st_konsumen}")
                        ->order_by("nm")
                        ->find_all();

        if(is_array($data) && count($data)){

            $isi    = ["st" => 1, "data" => $data];

        }else{

            $isi    = ["st" => 0, "data" => ""];

        }

        echo json_encode($isi);

    }

    public function cetak_a3_by_nota(){

        $this->auth->restrict($this->viewPermission_rekapA3Cetak);

        // data rekap penggunaan cetak A3 berdasarkan transaksi

        $where      = "`transaksi`.`deleted` = 0
                        AND `transaksi`.`st_history` = 0";

        $tgl1       = isset($_POST['tgl1']) ? $this->input->post('tgl1') : '';
        $tgl2       = isset($_POST['tgl2']) ? $this->input->post('tgl2') : '';

        $st_kasir   = isset($_POST['posisi']) ? $this->input->post('posisi') : '';
        $gol_kons   = isset($_POST['gol_konsumen']) ? $this->input->post('gol_konsumen') : '';        
        $konsumen   = isset($_POST['konsumen']) ? $this->input->post('konsumen') : '';                

        if(strlen($tgl1) > 0){

            $tgl1_cr    = date_ymd($tgl1). " 01:00:00";
            $tgl2_cr    = date_ymd($tgl2). " 23:59:00";        
             
        }else{

            $tgl1_cr    = date("Y-m-d"). " 01:00:00";
            $tgl2_cr    = date("Y-m-d"). " 23:59:00";

            $tgl1       = date("d/m/Y");
            $tgl2       = date("d/m/Y");

        }
        
        $where      .= " AND (transaksi.waktu >= '{$tgl1_cr}' AND transaksi.waktu <= '{$tgl2_cr}')";

        $filter     =   [
                            'tgl1'      => $tgl1,
                            'tgl2'      => $tgl2,
                            'st_kasir'  => $st_kasir,
                            'gol_kons'  => $gol_kons,
                            'konsumen'  => $konsumen
                        ];


        if(strlen($st_kasir) > 0){

            $where  .= " AND transaksi.st_kasir = {$st_kasir}";

        }

        if(strlen($konsumen) > 0){

            $where  .= " and `konsumen`.`nama` = '{$konsumen}'";

        }else{

            if(strlen($gol_kons) > 0){

                $where  .= " and `konsumen`.`st` = '{$gol_kons}'";

            }

        }
        
        $dt_trans       = $this->trans_model
                            ->select("
                                        `transaksi`.`id_transaksi`,
                                        `transaksi`.`st_kasir`,
                                        `transaksi`.`st_trans_master`,
                                        `users`.`nm_lengkap` AS `op`,
                                        DATE_FORMAT(`transaksi`.`waktu`, '%d/%m/%Y %H:%i:%s') AS waktu_trans,
                                        `transaksi`.`no_faktur`,
                                        `t_master`.`no_faktur` AS `no_faktur_master`,
                                        `konsumen`.`st` AS st_konsumen,
                                        CONCAT(IF(LENGTH(`konsumen`.`panggilan`) > 0,
                                                    `konsumen`.`panggilan`,
                                                    ''),
                                                ' ',
                                                `konsumen`.`nama`) AS nm_konsumen,
                                        IF(`konsumen`.`st` = 0,
                                            'Konsumen',
                                            IF(`konsumen`.`st` = 1,
                                                'Reseller',
                                                'Instansi')) AS gol_konsumen,
                                        `order_produk`.`no_transaksi`,
                                        `order_produk`.`id_konsumen`,
                                        DATE_FORMAT(`order_produk`.`tgl_order`, '%d/%m/%Y') AS tgl_order,
                                        `m_order_produk_detail`.`id_detail_produk_order`,
                                        `m_order_produk_detail`.`nama_pekerjaan`,
                                        `produk`.`nm_produk`,
                                        `produk`.`st_tipe` AS `st_tipe_produk`,
                                        `m_order_produk_detail`.`jml_cetak`,
                                        tbl_cetak_a3.idbarang,
                                        tbl_cetak_a3.nm_barang,
                                        (`m_order_produk_detail`.`jml_cetak` * tbl_cetak_a3.jml_cetak) AS cetak_a3
                                    ")
                            ->join("`users`","`transaksi`.`created_by` = `users`.`id_user`","inner")
                            ->join("(SELECT 
                                        `id_transaksi` AS id, `no_faktur`
                                    FROM
                                        transaksi
                                    WHERE
                                        deleted = 0) AS t_master","`t_master`.`id` = `transaksi`.`id_trans_ref_asli`","left")
                            ->join("`order_produk`","`transaksi`.`id_order` = `order_produk`.`id_order`","inner")
                            ->join("`konsumen`","`konsumen`.`idkonsumen` = `order_produk`.`id_konsumen`","inner")
                            ->join("`transaksi_detail`","`transaksi_detail`.`id_transaksi` = `transaksi`.`id_transaksi`","inner")
                            ->join("`m_order_produk_detail`","`m_order_produk_detail`.`id_detail_produk_order` = `transaksi_detail`.`id_detail_produk_order`","inner")
                            ->join("`produk`","`produk`.`idproduk` = `m_order_produk_detail`.`id_produk`","inner")
                            ->join("(SELECT 
                                            `produk`.`idproduk` AS id,
                                                `produk`.`nm_produk`,
                                                `barang`.`idbarang_bb` AS idbarang,
                                                `barang`.`nm_barang`,
                                                SUM(IF(`konversi_satuan`.`jml_kecil` IS NULL, 1, (`konversi_satuan`.`jml_kecil` * `produk_detail_penyusun`.`jml_pakai`)) * IF(tbl_bb_cetak.idbarang_bb IS NULL, 1, tbl_bb_cetak.ttl_pakai)) AS jml_cetak
                                        FROM
                                            produk
                                        INNER JOIN produk_detail_penyusun ON produk.idproduk = produk_detail_penyusun.idproduk
                                        INNER JOIN barang ON produk_detail_penyusun.idbarang_bb = barang.idbarang_bb
                                        LEFT JOIN konversi_satuan ON produk_detail_penyusun.id_konversi_jml = konversi_satuan.id_konversi
                                        LEFT JOIN (SELECT 
                                            produk.idproduk AS id,
                                                `produk_detail_penyusun`.`idbarang_bb`,
                                                IF(`konversi_satuan`.`jml_kecil` IS NULL, 1, (`konversi_satuan`.`jml_kecil` * `produk_detail_penyusun`.`jml_pakai`)) AS ttl_pakai
                                        FROM
                                            produk
                                        INNER JOIN produk_detail_penyusun ON produk.idproduk = produk_detail_penyusun.idproduk
                                        LEFT JOIN konversi_satuan ON produk_detail_penyusun.id_konversi_jml = konversi_satuan.id_konversi
                                        WHERE
                                            produk.idkategori = 1
                                                AND produk_detail_penyusun.deleted = 0
                                                AND produk_detail_penyusun.st_target_cetakan = 1) AS tbl_bb_cetak ON produk.idproduk = tbl_bb_cetak.id
                                        WHERE
                                            barang.st_cetak_a3 = 1
                                                AND produk_detail_penyusun.deleted = 0
                                        GROUP BY barang.idbarang_bb , produk.idproduk
                                        ORDER BY produk.idproduk) AS tbl_cetak_a3","m_order_produk_detail.id_produk = tbl_cetak_a3.id","inner")
                            ->where($where)
                            ->order_by("`transaksi`.`waktu`","asc")
                            ->order_by("`transaksi`.`id_transaksi`","asc")
                            ->order_by("`produk`.`nm_produk`","asc")
                            ->find_all();

        $dt_resume      = $this->trans_model
                            ->select("
                                        tbl_cetak_a3.idbarang,
                                        tbl_cetak_a3.nm_barang,
                                        sum(`m_order_produk_detail`.`jml_cetak` * tbl_cetak_a3.jml_cetak) AS cetak_a3
                                    ")
                            ->join("`users`","`transaksi`.`created_by` = `users`.`id_user`","inner")
                            ->join("(SELECT 
                                        `id_transaksi` AS id, `no_faktur`
                                    FROM
                                        transaksi
                                    WHERE
                                        deleted = 0) AS t_master","`t_master`.`id` = `transaksi`.`id_trans_ref_asli`","left")
                            ->join("`order_produk`","`transaksi`.`id_order` = `order_produk`.`id_order`","inner")
                            ->join("`konsumen`","`konsumen`.`idkonsumen` = `order_produk`.`id_konsumen`","inner")
                            ->join("`transaksi_detail`","`transaksi_detail`.`id_transaksi` = `transaksi`.`id_transaksi`","inner")
                            ->join("`m_order_produk_detail`","`m_order_produk_detail`.`id_detail_produk_order` = `transaksi_detail`.`id_detail_produk_order`","inner")
                            ->join("`produk`","`produk`.`idproduk` = `m_order_produk_detail`.`id_produk`","inner")
                            ->join("(SELECT 
                                            `produk`.`idproduk` AS id,
                                                `produk`.`nm_produk`,
                                                `barang`.`idbarang_bb` AS idbarang,
                                                `barang`.`nm_barang`,
                                                SUM(IF(`konversi_satuan`.`jml_kecil` IS NULL, 1, (`konversi_satuan`.`jml_kecil` * `produk_detail_penyusun`.`jml_pakai`)) * IF(tbl_bb_cetak.idbarang_bb IS NULL, 1, tbl_bb_cetak.ttl_pakai)) AS jml_cetak
                                        FROM
                                            produk
                                        INNER JOIN produk_detail_penyusun ON produk.idproduk = produk_detail_penyusun.idproduk
                                        INNER JOIN barang ON produk_detail_penyusun.idbarang_bb = barang.idbarang_bb
                                        LEFT JOIN konversi_satuan ON produk_detail_penyusun.id_konversi_jml = konversi_satuan.id_konversi
                                        LEFT JOIN (SELECT 
                                            produk.idproduk AS id,
                                                `produk_detail_penyusun`.`idbarang_bb`,
                                                IF(`konversi_satuan`.`jml_kecil` IS NULL, 1, (`konversi_satuan`.`jml_kecil` * `produk_detail_penyusun`.`jml_pakai`)) AS ttl_pakai
                                        FROM
                                            produk
                                        INNER JOIN produk_detail_penyusun ON produk.idproduk = produk_detail_penyusun.idproduk
                                        LEFT JOIN konversi_satuan ON produk_detail_penyusun.id_konversi_jml = konversi_satuan.id_konversi
                                        WHERE
                                            produk.idkategori = 1
                                                AND produk_detail_penyusun.deleted = 0
                                                AND produk_detail_penyusun.st_target_cetakan = 1) AS tbl_bb_cetak ON produk.idproduk = tbl_bb_cetak.id
                                        WHERE
                                            barang.st_cetak_a3 = 1
                                                AND produk_detail_penyusun.deleted = 0
                                        GROUP BY barang.idbarang_bb , produk.idproduk
                                        ORDER BY produk.idproduk) AS tbl_cetak_a3","m_order_produk_detail.id_produk = tbl_cetak_a3.id","inner")
                            ->where($where)
                            ->group_by("tbl_cetak_a3.idbarang")
                            ->find_all();

        //print_r($dt_resume);
        //echo $this->db->last_query();

        // ---- start susun array untuk menampilkan report

        $data           = [];
        $index_pakai    = 0;

        if(is_array($dt_trans) && count($dt_trans)){

            $id_pakai   = "";

            foreach ($dt_trans as $key => $isi) {
                
                if($id_pakai != $isi->id_transaksi){

                    $data[]     =   [
                                        'no_nota'           => $isi->no_faktur,
                                        'no_nota_master'    => $isi->no_faktur_master,
                                        'st_kasir'          => $isi->st_kasir == 0 ? 'Accounting / Admin' : 'Kasir',
                                        'st_master'         => $isi->st_trans_master,
                                        'waktu'             => $isi->waktu_trans,
                                        'konsumen'          => ucwords($isi->nm_konsumen),
                                        'gol_konsumen'      => $isi->gol_konsumen,
                                        'op'                => $isi->op,  
                                        'detail'            =>  [
                                                                    [
                                                                        "nm_file"       => $isi->nama_pekerjaan,
                                                                        "produk"        => $isi->nm_produk,
                                                                        "bahan_cetak"   => $isi->nm_barang,
                                                                        "jml_order"     => $isi->jml_cetak,
                                                                        "jml_cetak_a3"  => $isi->cetak_a3
                                                                    ]
                                                                ]
                                    ];

                    $id_pakai       = $isi->id_transaksi;

                    $index_pakai    = $key == 0 ? 0 : ($index_pakai + 1);
                
                }else{

                    $data[$index_pakai]['detail'][] =   [
                                                            "nm_file"       => $isi->nama_pekerjaan,
                                                            "produk"        => $isi->nm_produk,
                                                            "bahan_cetak"   => $isi->nm_barang,
                                                            "jml_order"     => $isi->jml_cetak,
                                                            "jml_cetak_a3"  => $isi->cetak_a3
                                                        ];

                }

            }

        }

        //print_r($data);

        // ---- end susun array untuk menampilkan report


        $asset  =   [
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'lap_rekap_trans/assets/js/lap_rekap_cetak_a3.js'
                    ]; 

        add_assets($asset);

        $this->template->set("dt_resume",$dt_resume);
        $this->template->set("data",$data);

        $this->template->set("filter",$filter);
        $this->template->set("toolbar_title", lang('title-a3-trans'));
        $this->template->title(lang('title-a3-trans'));
        $this->template->render('lap_cetak_a3'); 

    }

    public function trans_order_by_nota(){

        $this->auth->restrict($this->viewPermission_rekapTransNota);

        //data resume transaksi//

        $where  = "`transaksi`.`deleted` = 0
                    AND transaksi.st_history = 0 ";

        $tgl1       = isset($_POST['tgl1']) ? $this->input->post('tgl1') : '';
        $tgl2       = isset($_POST['tgl2']) ? $this->input->post('tgl2') : '';

        $st_kasir   = isset($_POST['posisi']) ? $this->input->post('posisi') : '';
        $st_po      = isset($_POST['jns_order']) ? $this->input->post('jns_order') : '';
        $st_lunas   = isset($_POST['st_lunas']) ? $this->input->post('st_lunas') : '';

        $gol_kons   = isset($_POST['gol_konsumen']) ? $this->input->post('gol_konsumen') : '';        
        $konsumen   = isset($_POST['konsumen']) ? $this->input->post('konsumen') : '';        

        $pil_produk = isset($_POST['pil_produk']) ? $this->input->post('pil_produk') : '';        

        //print_r($pil_produk);

        if(strlen($tgl1) > 0){

            $tgl1_cr    = date_ymd($tgl1). " 01:00:00";
            $tgl2_cr    = date_ymd($tgl2). " 23:59:00";        
             
        }else{

            $tgl1_cr    = date("Y-m-d"). " 01:00:00";
            $tgl2_cr    = date("Y-m-d"). " 23:59:00";

            $tgl1       = date("d/m/Y");
            $tgl2       = date("d/m/Y");

        }
        
        $where      .= " AND (transaksi.waktu >= '{$tgl1_cr}' AND transaksi.waktu <= '{$tgl2_cr}')";

        $filter     =   [
                            'tgl1'      => $tgl1,
                            'tgl2'      => $tgl2,
                            'st_kasir'  => $st_kasir,
                            'st_po'     => $st_po,
                            'st_lunas'  => $st_lunas,
                            'gol_kons'  => $gol_kons,
                            'konsumen'  => $konsumen
                        ];

        if(strlen($st_kasir) > 0){

            $where  .= " AND transaksi.st_kasir = {$st_kasir}";

        }

        if(strlen($st_po) > 0){        

            $where  .= " and order_produk.st_po = {$st_po}";

        }

        if(strlen($st_lunas) > 0){

            $where  .= " and transaksi.st_lunas = {$st_lunas}";

        }        

        if(strlen($konsumen) > 0){

            $where  .= " and `konsumen`.`idkonsumen` = '{$konsumen}'";

        }else{

            if(strlen($gol_kons) > 0){

                $where  .= " and `konsumen`.`st` = '{$gol_kons}'";

            }

        }



        //jika search produk yang dicari difungsikan

        if(count($pil_produk) && is_array($pil_produk)){

            $gabung     = "";

            foreach ($pil_produk as $key => $idp) {
                
                if($key == 0){

                    $gabung = "(";

                }

                $gabung     .= "m_order_produk_detail.id_produk = '{$idp}'";


                if($key == (count($pil_produk)-1)){

                    $gabung .= ")";

                }else{

                    $gabung .= " or ";

                }                


            }

            $where  .= " and ".$gabung;

            //start ambil data kode transaksi yang barangnya include
            $dt_idtrans = $this->trans_model
                                ->select("transaksi.id_transaksi as id")
                                ->join("order_produk","transaksi.id_order = order_produk.id_order","inner")
                                ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                                ->join("transaksi_detail","transaksi.id_transaksi = transaksi_detail.id_transaksi","inner")
                                ->join("m_order_produk_detail","transaksi_detail.id_detail_produk_order = m_order_produk_detail.id_detail_produk_order","inner")
                                ->where($where)
                                ->group_by("transaksi.id_transaksi")
                                ->find_all();

            $where  = "";

            if(is_array($dt_idtrans) && count($dt_idtrans)){
                    
                foreach ($dt_idtrans as $key => $dti) {
                    
                    $where  .=  "transaksi.id_transaksi = '{$dti->id}'";

                    if($key < count($dt_idtrans)-1){

                        $where  .= " or ";

                    }

                }

            }

            //echo $where;

        }



        $data_produk    = $this->produk_model
                                ->select("`produk`.`idproduk`,
                                            `produk`.`nm_produk`,
                                            kategori.nmkategori")
                                ->join("kategori","produk.idkategori = kategori.idkategori")
                                ->where("produk.deleted = 0
                                            AND produk.st_produk = 0")
                                ->order_by("produk.nm_produk","asc")
                                ->find_all();

        $arr_produk = [];

        if(is_array($data_produk) && count($data_produk)){

            foreach ($data_produk as $key => $dp) {
                
                $arr_produk[$dp->idproduk] = $dp->nm_produk." [".$dp->nmkategori."]";

            }

        }



        $data_trans = $this->trans_model
                        ->select("`transaksi`.`id_transaksi`,
                                    `users`.`nm_lengkap` as op,
                                    DATE_FORMAT(`transaksi`.`waktu`, '%d/%m/%Y %H:%i:%s') AS waktu_trans,
                                    `transaksi`.`no_faktur`,
                                    `t_master`.`no_faktur` AS `no_faktur_master`,
                                    `transaksi`.`st_kasir`,
                                    `transaksi`.`st_trans_master`,
                                    `transaksi`.`st_ref`,
                                    `transaksi`.`st_history`,
                                    `transaksi`.`id_trans_ref_by`,
                                    `transaksi`.`id_trans_ref_asli`,
                                    `transaksi`.`lama_tempo`,
                                    DATE_FORMAT(`transaksi`.`tgl_tempo`, '%d/%m/%Y') AS tgl_tempo,
                                    `transaksi`.`st_metode_bayar`,
                                    `transaksi`.`total` AS `total_all`,
                                    `transaksi`.`diskon` AS `diskon_all`,
                                    `transaksi`.`potongan` AS `pot_all`,
                                    `transaksi`.`pajak` AS `pajak_all`,
                                    `transaksi`.`grand_total`,
                                    t_bayar.total_bayar,
                                    (`transaksi`.`grand_total` - IF(t_bayar.total_bayar IS NULL,
                                        0,
                                        t_bayar.total_bayar)) AS sisa_piutang,
                                    `transaksi`.`st_lunas`,
                                    `transaksi`.`id_order`,
                                    `konsumen`.`st`,
                                    CONCAT(IF(LENGTH(`konsumen`.`panggilan`) > 0,
                                                `konsumen`.`panggilan`,
                                                ''),
                                            ' ',
                                            `konsumen`.`nama`) AS nm_konsumen,
                                    IF(`konsumen`.`st` = 0,
                                        'Konsumen',
                                        IF(`konsumen`.`st` = 1,
                                            'Reseller',
                                            'Instansi')) AS gol_konsumen,
                                    `order_produk`.`no_transaksi`,
                                    `order_produk`.`id_konsumen`,
                                    DATE_FORMAT(`order_produk`.`tgl_order`, '%d/%m/%Y') AS tgl_order,
                                    `order_produk`.`total_value_order`,
                                    `order_produk`.`st_revisi`,
                                    `order_produk`.`st_po`,
                                    `m_order_produk_detail`.`nama_pekerjaan`,
                                     `m_order_produk_detail`.`id_kategori`,
                                    `kategori`.`nmkategori`,
                                    `produk`.`nm_produk`,
                                    `produk`.`st_tipe` AS `st_tipe_produk`,
                                    `m_order_produk_detail`.`id_detail_produk_order`,
                                    `m_order_produk_detail`.`thumbnail`,
                                    `m_order_produk_detail`.`p`,
                                    `m_order_produk_detail`.`l`,
                                    `m_order_produk_detail`.`id_satuan` AS `id_satuan_uk`,
                                    konversi_satuan.satuan_besar,
                                    CONCAT(konversi_satuan.satuan_besar,
                                            ' (',
                                            `konversi_satuan`.`jml_kecil`,
                                            ' ',
                                            `satuan_terkecil`.`alias`,
                                            ')') AS tampil2,
                                    `m_order_produk_detail`.`st_urgent`,
                                    DATE_FORMAT(`m_order_produk_detail`.`tgl_permintaan_selesai`,
                                            '%d/%m/%Y') AS tgl_selesai,
                                    IF(`m_order_produk_detail`.`st_finishing` = 0,
                                        'Finishing Standart',
                                        'Custom Finishing') AS st_finishing,
                                    `m_order_produk_detail`.`jml_cetak`,
                                    `transaksi_detail`.`harga_by_satuan`,
                                    `transaksi_detail`.`satuan_harga_by`,
                                    `satuan_harga`.`satuan` AS `satuan_harga_kecil`,
                                    `transaksi_detail`.`biaya_cetak`,
                                    `transaksi_detail`.`biaya_design`,
                                    `transaksi_detail`.`biaya_finishing`,
                                    `transaksi_detail`.`diskon`,
                                    `transaksi_detail`.`pot_kecil`,
                                    `transaksi_detail`.`potongan`,
                                    `transaksi_detail`.`subtotal`,
                                    `transaksi_detail`.`st_ubahan`")
                        ->join("users","transaksi.created_by = users.id_user","inner")
                        ->join("`order_produk`","`transaksi`.`id_order` = `order_produk`.`id_order`","inner")
                        ->join("`konsumen`","`konsumen`.`idkonsumen` = `order_produk`.`id_konsumen`","inner")
                        ->join("`transaksi_detail`","`transaksi_detail`.`id_transaksi` = `transaksi`.`id_transaksi`","inner")
                        ->join("`m_order_produk_detail`","`m_order_produk_detail`.`id_detail_produk_order` = `transaksi_detail`.`id_detail_produk_order`","inner")
                        ->join("`kategori`","`kategori`.`idkategori` = `m_order_produk_detail`.`id_kategori`","inner")
                        ->join("`produk`","`produk`.`idproduk` = `m_order_produk_detail`.`id_produk`","inner")
                        ->join("`konversi_satuan`","`konversi_satuan`.`id_konversi` = `m_order_produk_detail`.`id_satuan`","inner")
                        ->join("`satuan_terkecil`","`konversi_satuan`.`id_satuan_terkecil` = `satuan_terkecil`.`id_satuan_terkecil`","inner")
                        ->join("(SELECT 
                                        `id_konversi` AS id, `satuan_besar` AS satuan
                                    FROM
                                        konversi_satuan
                                    WHERE
                                        deleted = 0) AS satuan_harga","`transaksi_detail`.`satuan_harga_by` = `satuan_harga`.`id`","inner")
                        ->join("(SELECT 
                                        `id_transaksi` AS id, `no_faktur`
                                    FROM
                                        transaksi
                                    WHERE
                                        deleted = 0) AS t_master","`t_master`.`id` = `transaksi`.`id_trans_ref_asli`","left")
                        ->join("(SELECT 
                                        `id_transaksi_master` AS id, SUM(`bayar`) AS total_bayar
                                    FROM
                                        bayar_trans_kasir
                                    WHERE
                                        deleted = 0
                                    GROUP BY id_transaksi_master) AS t_bayar","transaksi.id_trans_ref_asli = t_bayar.id","left")
                        ->where($where)
                        ->order_by("transaksi.waktu","asc")
                        ->order_by("transaksi.id_transaksi","asc")
                        ->order_by("`kategori`.`nmkategori`","asc")
                        ->order_by("produk.nm_produk","asc")
                        ->find_all();

        $dt_kategori    = $this->kategori_model
                                ->select("`idkategori` as id, `nmkategori` as nm")
                                ->where("deleted = 0")
                                ->find_all();

        $arr_kategori   = [];

        foreach ($dt_kategori as $key => $kt) {
            
            $arr_kategori[$kt->id]  = [
                                        'nm'        => $kt->nm,
                                        'cetak'     => 0,
                                        'finishing' => 0,
                                        'design'    => 0,
                                    ];

        }

        //echo $this->db->last_query();

        $jml_trans      = 0;
        $total_trans    = 0;
        $total_piutang  = 0;

        $data           = [];
        $detail         = [];

        $id_acuan       = "";
        $index_header   = 0;


        if(is_array($data_trans) && count($data_trans)){

            foreach ($data_trans as $key => $isi) {
                // ** Start proses penataan data laporan **
                $uk_cetak       = $isi->p."x".$isi->l."(".$isi->satuan_besar.")";
                $harga_kecil    = number_format($isi->harga_by_satuan)." /".$isi->satuan_harga_kecil;    
                $potongan       = number_format($isi->pot_kecil)." /".$isi->satuan_harga_kecil;    

                if($isi->id_transaksi != $id_acuan){ //kode transaksi sudah menjadi baru

                    $total_trans    += $isi->grand_total;
                    $total_piutang  += $isi->sisa_piutang;

                    $id_acuan       = $isi->id_transaksi;

                    $index_header   = $key == 0 ? 0 : ($index_header+1);

                    $data[]     =   [
                                        'no_nota'           => $isi->no_faktur,
                                        'no_nota_master'    => $isi->no_faktur_master,
                                        'st_kasir'          => $isi->st_kasir == 0 ? 'Accounting / Admin' : 'Kasir',
                                        'st_master'         => $isi->st_trans_master,
                                        'waktu'             => $isi->waktu_trans,
                                        'konsumen'          => ucwords($isi->nm_konsumen),
                                        'gol_konsumen'      => $isi->gol_konsumen,
                                        'tgl_tempo'         => $isi->tgl_tempo,
                                        'st_lunas'          => $isi->st_lunas == 0 ? 'Hutang' : 'Lunas',
                                        'gtotal'            => $isi->grand_total,
                                        'bayar'             => $isi->total_bayar,
                                        'kurang_bayar'      => $isi->sisa_piutang,
                                        'op'                => $isi->op,
                                        'detail'            =>  [
                                                                    [
                                                                        'kategori'          => $isi->nmkategori,
                                                                        'nm_produk'         => $isi->nm_produk,
                                                                        'tipe_produk'       => $isi->st_tipe_produk,
                                                                        'uk_cetak'          => $uk_cetak,
                                                                        'jml_cetak'         => $isi->jml_cetak,
                                                                        'harga'             => $harga_kecil,
                                                                        'pot'               => $potongan,
                                                                        'biaya_cetak'       => $isi->biaya_cetak,
                                                                        'biaya_design'      => $isi->biaya_design,
                                                                        'biaya_finishing'   => $isi->biaya_finishing,
                                                                        'subtotal'          => $isi->subtotal
                                                                    ]
                                                                ]
                                    ];  



                }else{

                    $data[$index_header]['detail'][]    =   [
                                                                'kategori'          => $isi->nmkategori,
                                                                'nm_produk'         => $isi->nm_produk,
                                                                'tipe_produk'       => $isi->st_tipe_produk,
                                                                'uk_cetak'          => $uk_cetak,
                                                                'jml_cetak'         => $isi->jml_cetak,
                                                                'harga'             => $harga_kecil,
                                                                'pot'               => $potongan,
                                                                'biaya_cetak'       => $isi->biaya_cetak,
                                                                'biaya_design'      => $isi->biaya_design,
                                                                'biaya_finishing'   => $isi->biaya_finishing,
                                                                'subtotal'          => $isi->subtotal
                                                            ];

                }

                // ** end proses penataan data laporan **

                // ** start proses penataan data resume **

                $kategori_cek   = $isi->id_kategori;
                $ttl_cetak      = $arr_kategori[$kategori_cek]['cetak']+$isi->biaya_cetak+$isi->potongan;
                $ttl_finishing  = $arr_kategori[$kategori_cek]['finishing']+$isi->biaya_finishing;
                $ttl_design     = $arr_kategori[$kategori_cek]['design']+$isi->biaya_design;

                //isi ke array

                $arr_kategori[$kategori_cek]['cetak']       =  $ttl_cetak;
                $arr_kategori[$kategori_cek]['design']      =  $ttl_design;   
                $arr_kategori[$kategori_cek]['finishing']   =  $ttl_finishing;   

                // ** end proses penataan data resume **

            }

        }

        // start remove resume kategori yang nilai cetaknya 0

        foreach ($arr_kategori as $key => $kt_resume) {
            
            if($kt_resume['cetak'] == 0 ){

                unset($arr_kategori[$key]);    

            }

        }

        // end remove resume kategori yang nilai cetaknya 0        

        //print_r($arr_kategori);
        //print_r($data);

        $jml_trans  = count($data);

        $asset  =   [
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'lap_rekap_trans/assets/js/lap_rekap_trans_nota.js',
                        'lap_rekap_trans/assets/css/lap_rekap_trans_nota.css'
                    ]; 

        add_assets($asset);


        $this->template->set("jml_trans",$jml_trans);
        $this->template->set("total_trans",$total_trans);
        $this->template->set("total_piutang",$total_piutang);

        $this->template->set("isi_produk", $arr_produk);
        $this->template->set("pil_produk", $pil_produk);

        $this->template->set("data",$data);
        $this->template->set("resume",$arr_kategori);//resume total transaksi, cetak , finishing, design

        $this->template->set("filter",$filter);
        $this->template->set("toolbar_title", lang('title-trans-order'));
        $this->template->title(lang('title-trans-order'));
        $this->template->render('lap_trans_nota'); 

    }

    public function penjualan_by_kategory(){

        $this->auth->restrict($this->viewPermission_rekapKategory);

        $tgl1   = isset($_POST['tgl_awal']) ? $this->input->post('tgl_awal') : date('d/m/Y');
        $tgl2   = isset($_POST['tgl_akhir']) ? $this->input->post('tgl_akhir') : date('d/m/Y');

        $nm_op  = isset($_POST['nm_op']) ? $this->input->post('nmop') : '';

        $tgl1_cr   = date_ymd($tgl1)." 01:00:00";
        $tgl2_cr   = date_ymd($tgl2)." 23:59:59";


        $st_konsumen    = isset($_POST['st_konsumen']) ? $this->input->post('st_konsumen') : '';
        $konsumen       = isset($_POST['konsumen']) ? $this->input->post('konsumen') : '';

        //buat wherenya

        $where  = "transaksi.deleted = 0
                    AND transaksi.st_history = 0
                    AND (`transaksi`.`waktu` >= '{$tgl1_cr}' and `transaksi`.`waktu` <= '{$tgl2_cr}')";

        if(strlen($st_konsumen) > 0){

            $where .= " AND `konsumen`.`st` = {$st_konsumen}";

        }

        if(strlen($konsumen) > 0){

            $where .= " AND `order_produk`.`id_konsumen` = '{$konsumen}'";

        }


        // data resume penjualan sesuai pencarian

        $dt_resume  = $this->trans_model
                            ->select("SUM(`transaksi_detail`.`subtotal`) AS total,
                                        `m_order_produk_detail`.`id_kategori`,
                                        `kategori`.`nmkategori`")
                            ->join("order_produk","transaksi.id_order = order_produk.id_order","inner")
                            ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                            ->join("transaksi_detail","transaksi.id_transaksi = transaksi_detail.id_transaksi","inner")
                            ->join("m_order_produk_detail","transaksi_detail.id_detail_produk_order = m_order_produk_detail.id_detail_produk_order","inner")
                            ->join("kategori","m_order_produk_detail.id_kategori = kategori.idkategori","inner")
                            ->where($where)
                            ->group_by("`m_order_produk_detail`.`id_kategori`")
                            ->find_all();


        // data detail penjualan kategori by konsumen
        $dt_detail  = $this->trans_model
                            ->select("`order_produk`.`id_konsumen`,
                                        CONCAT(`konsumen`.`panggilan`,
                                                ' ',
                                                `konsumen`.`nama`) AS nm,
                                        IF(`konsumen`.`st` = 0,
                                            'Umum',
                                            IF(`konsumen`.`st` = 1,
                                                'Reseller',
                                                'Instansi')) AS st_konsumen,
                                        `transaksi_detail`.`id_detail_produk_order`,
                                        COUNT(`transaksi_detail`.`id_detail_produk_order`) AS kali,
                                        SUM(`transaksi_detail`.`subtotal`) AS total,
                                        `m_order_produk_detail`.`id_kategori`,
                                        `kategori`.`nmkategori`")
                            ->join("order_produk","transaksi.id_order = order_produk.id_order","inner")
                            ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                            ->join("transaksi_detail","transaksi.id_transaksi = transaksi_detail.id_transaksi","inner")
                            ->join("m_order_produk_detail","transaksi_detail.id_detail_produk_order = m_order_produk_detail.id_detail_produk_order","inner")
                            ->join("kategori","m_order_produk_detail.id_kategori = kategori.idkategori","inner")
                            ->where($where)
                            ->group_by("nm")
                            ->group_by("`m_order_produk_detail`.`id_kategori`")
                            ->order_by("st")
                            ->order_by("nm")
                            ->order_by("total","DESC")
                            ->find_all();

        $data   = [];
        $dt_val = [];
        if(is_array($dt_detail) && count($dt_detail)){

            $id_konsumen_cek    = "";
            $n_index            = 0;

            foreach ($dt_detail as $key => $dt) {
                
                if($key > 0){

                    if($dt->id_konsumen == $id_konsumen_cek){

                        $data[$n_index]["dt_trans"][]   =   [
                                                                "nm_kategori"   => $dt->nmkategori,
                                                                "value"         => $dt->total       
                                                            ];

                    }

                }


                if($dt->id_konsumen <> $id_konsumen_cek){


                    $data[]   = [
                                    "konsumen"      =>  $dt->nm,
                                    "st_konsumen"   =>  $dt->st_konsumen,
                                    "dt_trans"      =>  [
                                                            [
                                                            "nm_kategori"   => $dt->nmkategori,
                                                            "value"         => $dt->total
                                                            ]
                                                        ]
                                ];

                    $id_konsumen_cek = $dt->id_konsumen;

                    $n_index    = $key == 0 ? 0 : $n_index + 1;

                }


            }

            //print_r($data);

        }


        $asset  =   [
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'lap_rekap_trans/assets/js/lap_rekap_trans_jual_kategori.js'
                    ];     

        add_assets($asset);

        $this->template->set("period1",$tgl1);
        $this->template->set("period2",$tgl2);   

        $this->template->set("st_konsumen",$st_konsumen);
        $this->template->set("konsumen",$konsumen);

        $this->template->set("resume",$dt_resume);
        $this->template->set("data",$data);

        $this->template->set("toolbar_title", lang('title_jual_kategori'));
        $this->template->title(lang('title_jual_kategori'));
        $this->template->render('lap_jual_kategori'); 


    }

    public function penjualan_by_produk(){

        $this->auth->restrict($this->viewPermission_rekapProduk);

        $tgl1       = isset($_POST['tgl_awal']) ? $this->input->post('tgl_awal') : date('d/m/Y');
        $tgl2       = isset($_POST['tgl_akhir']) ? $this->input->post('tgl_akhir') : date('d/m/Y');
        
        $pilih_kat  = isset($_POST['pil_kategori']) ? $this->input->post('pil_kategori') : ['0'];
        $grouping   = isset($_POST['grouping']) ? $this->input->post('grouping') : '1';

        $st_konsumen    = isset($_POST['st_konsumen']) ? $this->input->post('st_konsumen') : '';
        $konsumen       = isset($_POST['konsumen']) ? $this->input->post('konsumen') : '';

        $tgl1_cr    = date_ymd($tgl1)." 01:00:00";
        $tgl2_cr    = date_ymd($tgl2)." 23:59:59";


        // data kategori produk
        $dt_kat = $this->kategori_model->select("`idkategori` as id, `nmkategori` as nm")
                                    ->where("deleted = 0")
                                    ->order_by("nm")
                                    ->find_all();
        $arr_kat    = [];

        if(count($dt_kat)){

            foreach ($dt_kat as $key => $val) {
                $arr_kat[$val->id] = $val->nm;
            }

        }


        //buat wherenya 

        $where  = "transaksi.deleted = 0 
                    and transaksi.st_history = 0 
                    and (transaksi.waktu >= '{$tgl1_cr}' 
                    and transaksi.waktu <= '{$tgl2_cr}')";

        if($grouping == 0){ //jika berdasarkan konsumen

            if(strlen($st_konsumen) > 0){

                $where  .= " and konsumen.st = {$st_konsumen}";

            }


            if(strlen($konsumen) > 0){

                $where  .= " and `order_produk`.`id_konsumen` = '{$konsumen}'";

            }


        }else{ //jika berdasarkan produk

            if(count($pilih_kat)){

                foreach ($pilih_kat as $key => $kat) {
                    
                    if($kat > 0){

                        if($key == 0){
                        
                            $add_where  = " and `m_order_produk_detail`.`id_kategori` = {$kat}";
                        
                        }else{

                            $add_where  .= " or `m_order_produk_detail`.`id_kategori` = {$kat}";

                        }

                    }else{

                        $add_where = '';

                    }

                }

                $where  .= $add_where;

            }

        }

        
        // data resume
        $dt_resume  = $this->trans_model
                            ->select("SUM(`transaksi_detail`.`subtotal`) AS total,
                                        `m_order_produk_detail`.`id_kategori`,
                                        `kategori`.`nmkategori`")
                            ->join("order_produk","transaksi.id_order = order_produk.id_order","inner")
                            ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                            ->join("transaksi_detail","transaksi.id_transaksi = transaksi_detail.id_transaksi","inner")
                            ->join("m_order_produk_detail","transaksi_detail.id_detail_produk_order = m_order_produk_detail.id_detail_produk_order","inner")
                            ->join("kategori","m_order_produk_detail.id_kategori = kategori.idkategori","inner")
                            ->where($where)
                            ->group_by("`m_order_produk_detail`.`id_kategori`")
                            ->find_all();

        //ambil jml satuan untuk m2 dan pcs

        $dt_konv_satuan  = $this->satuan_model
                                ->select("`id_konversi`, `satuan_besar`, `jml_kecil`")
                                ->where("id_konversi = 8
                                            OR id_konversi = 3 
                                            AND deleted = 0")
                                ->order_by("id_konversi")
                                ->find_all();

        $dt_pcs     = [
                        'value' => $dt_konv_satuan[1]->jml_kecil, 
                        'satuan' => $dt_konv_satuan[1]->satuan_besar
                    ];

        $dt_m2      = [
                        'value' => $dt_konv_satuan[0]->jml_kecil, 
                        'satuan' => $dt_konv_satuan[0]->satuan_besar
                    ];

        // data transaksi produk

        $data = [];

        if($grouping == 0){ //berdasarkan konsumen

            $data_trans     = $this->trans_model
                                    ->select("`transaksi`.`id_transaksi`,
                                                `order_produk`.`id_konsumen`,
                                                CONCAT(`konsumen`.`panggilan`,
                                                        ' ',
                                                        `konsumen`.`nama`) AS nm,
                                                IF(konsumen.st = 0,
                                                    'Konsumen',
                                                    IF(konsumen.st = 1,
                                                        'Reseller',
                                                        'Instansi')) AS st_konsumen,
                                                `transaksi_detail`.`id_detail_produk_order`,
                                                `m_order_produk_detail`.`id_kategori`,
                                                `kategori`.`nmkategori`,
                                                `m_order_produk_detail`.`id_produk`,
                                                `produk`.`nm_produk`,
                                                `produk`.`st_tipe`,
                                                `m_order_produk_detail`.`p`,
                                                `m_order_produk_detail`.`l`,
                                                `m_order_produk_detail`.`id_satuan`,
                                                `konversi_satuan`.`satuan_besar`,
                                                `konversi_satuan`.`jml_kecil`,
                                                `m_order_produk_detail`.`jml_cetak`,
                                                COUNT(`transaksi_detail`.`id_detail_produk_order`) AS jml_kali,
                                                SUM(IF(`produk`.`st_tipe` = 1,
                                                    ((`m_order_produk_detail`.`p` * `konversi_satuan`.`jml_kecil`) * (`m_order_produk_detail`.`l` * `konversi_satuan`.`jml_kecil`)) * `m_order_produk_detail`.`jml_cetak`,
                                                    `m_order_produk_detail`.`jml_cetak`)) AS total_cetak,
                                                SUM(`transaksi_detail`.`subtotal`) AS total_trans")
                                    ->join("order_produk","transaksi.id_order = order_produk.id_order","inner")
                                    ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                                    ->join("transaksi_detail","transaksi.id_transaksi = transaksi_detail.id_transaksi","inner")
                                    ->join("m_order_produk_detail","transaksi_detail.id_detail_produk_order = m_order_produk_detail.id_detail_produk_order","inner")
                                    ->join("kategori","m_order_produk_detail.id_kategori = kategori.idkategori","inner")
                                    ->join("produk","m_order_produk_detail.id_produk = produk.idproduk","inner")
                                    ->join("konversi_satuan","m_order_produk_detail.id_satuan = konversi_satuan.id_konversi","inner")
                                    ->where($where)
                                    ->group_by("id_produk")
                                    ->group_by("id_konsumen")
                                    ->order_by("nm")
                                    ->order_by("total_trans","DESC")
                                    ->find_all();

            //echo $this->db->last_query();

            if(is_array($data_trans) && count($data_trans)){

                $id_konsumen_cek    = "";
                $n_index            = 0;

                foreach ($data_trans as $key => $dt) {
                    
                    if($dt->st_tipe == 0){//fix, berdasarkan jml cetak

                        $pembagi        = $dt_pcs['value'];
                        $satuan_tampil  = $dt_pcs['satuan'];

                    }else{

                        $pembagi        = $dt_m2['value'];
                        $satuan_tampil  = $dt_m2['satuan'];

                    }

                    if($key > 0){

                        if($id_konsumen_cek == $dt->id_konsumen){

                            $data[$n_index]['dt_detail'][]  = [
                                                                "nm_produk" => $dt->nm_produk,
                                                                "kategori"  => $dt->nmkategori,
                                                                "cetak"     => $dt->total_cetak / $pembagi,
                                                                "satuan"    => $satuan_tampil,
                                                                "ttl_trans" => $dt->total_trans
                                                            ];

                        }

                    }


                    if($id_konsumen_cek <> $dt->id_konsumen){

                        $id_konsumen_cek = $dt->id_konsumen;
                        $n_index = $key == 0 ? 0 : $n_index+=1;

                        $data[]   =   [
                                        "dt_header"     => $dt->nm." ; ".$dt->st_konsumen,
                                        "dt_detail"     =>  [
                                                                [
                                                                    "nm_produk" => $dt->nm_produk,
                                                                    "kategori"  => $dt->nmkategori,
                                                                    "cetak"     => $dt->total_cetak / $pembagi,
                                                                    "satuan"    => $satuan_tampil,
                                                                    "ttl_trans" => $dt->total_trans
                                                                ]
                                                            ]

                                    ];

                    }

                }

                

            }

        }else{ //berdasarkan produk

            $data_trans = $this->trans_model
                                ->select("`transaksi`.`id_transaksi`,
                                            `transaksi_detail`.`id_detail_produk_order`,
                                            `m_order_produk_detail`.`id_kategori`,
                                            `kategori`.`nmkategori`,
                                            `m_order_produk_detail`.`id_produk`,
                                            `produk`.`nm_produk`,
                                            `produk`.`st_tipe`,
                                            `m_order_produk_detail`.`p`,
                                            `m_order_produk_detail`.`l`,
                                            `m_order_produk_detail`.`id_satuan`,
                                            `konversi_satuan`.`satuan_besar`,
                                            `konversi_satuan`.`jml_kecil`,
                                            `m_order_produk_detail`.`jml_cetak`,
                                            COUNT(`transaksi_detail`.`id_detail_produk_order`) AS jml_kali,
                                            SUM(IF(`produk`.`st_tipe` = 1,
                                                ((`m_order_produk_detail`.`p` * `konversi_satuan`.`jml_kecil`) * (`m_order_produk_detail`.`l` * `konversi_satuan`.`jml_kecil`)) * `m_order_produk_detail`.`jml_cetak`,
                                                `m_order_produk_detail`.`jml_cetak`)) AS total_cetak,
                                            SUM(`transaksi_detail`.`subtotal`) AS total_trans")
                                ->join("transaksi_detail","transaksi.id_transaksi = transaksi_detail.id_transaksi","inner")     
                                ->join("m_order_produk_detail","transaksi_detail.id_detail_produk_order = m_order_produk_detail.id_detail_produk_order","inner")     
                                ->join("kategori","m_order_produk_detail.id_kategori = kategori.idkategori","inner")     
                                ->join("produk","m_order_produk_detail.id_produk = produk.idproduk","inner")     
                                ->join("konversi_satuan","m_order_produk_detail.id_satuan = konversi_satuan.id_konversi","inner")     
                                ->where($where)
                                ->group_by("id_produk")
                                ->order_by("id_kategori")
                                ->order_by("total_trans","DESC")
                                ->find_all();

            //print_r($data_trans);

            if(is_array($data_trans) && count($data_trans)){

                $id_cek     = "";
                $n_index    = 0;

                foreach ($data_trans as $key => $dt) {
                    
                    if($dt->st_tipe == 0){//fix, berdasarkan jml cetak

                        $pembagi        = $dt_pcs['value'];
                        $satuan_tampil  = $dt_pcs['satuan'];

                    }else{

                        $pembagi        = $dt_m2['value'];
                        $satuan_tampil  = $dt_m2['satuan'];

                    }

                    if($key > 0){

                        if($id_cek == $dt->id_kategori){

                            $data[$n_index]['dt_detail'][]  = [
                                                                "nm_produk" => $dt->nm_produk,
                                                                "kategori"  => $dt->nmkategori,
                                                                "cetak"     => $dt->total_cetak / $pembagi,
                                                                "satuan"    => $satuan_tampil,
                                                                "ttl_trans" => $dt->total_trans
                                                            ];

                        }

                    }


                    if($id_cek <> $dt->id_kategori){

                        $id_cek  = $dt->id_kategori;
                        $n_index = $key == 0 ? 0 : $n_index+=1;

                        //echo "<br>";
                        //echo $n_index." ".$id_cek;
                        //echo "<br>";

                        $data[]   = [
                                        "dt_header"     => $dt->nmkategori,
                                        "dt_detail"     =>  [
                                                                [
                                                                    "nm_produk" => $dt->nm_produk,
                                                                    "kategori"  => $dt->nmkategori,
                                                                    "cetak"     => $dt->total_cetak / $pembagi,
                                                                    "satuan"    => $satuan_tampil,
                                                                    "ttl_trans" => $dt->total_trans
                                                                ]
                                                            ]

                                    ];

                    }

                }

                

            }


        }

        $asset  =   [
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'lap_rekap_trans/assets/js/lap_rekap_trans_jual_produk.js'
                    ];     

        add_assets($asset);

        $this->template->set("data",$data);
        $this->template->set("resume", $dt_resume);

        $this->template->set("kategori",$arr_kat);
        $this->template->set("pil_kategori",$pilih_kat);
        $this->template->set("grouping",$grouping);

        $this->template->set("period1",$tgl1);
        $this->template->set("period2",$tgl2);   

        $this->template->set("st_konsumen",$st_konsumen);
        $this->template->set("konsumen",$konsumen);

        $this->template->set("toolbar_title", lang('title_jual_produk'));
        $this->template->title(lang('title_jual_produk'));
        $this->template->render('lap_jual_produk'); 


    }

    public function rekap_piutang(){

        $this->auth->restrict($this->viewPermission_rekapPiutang);

        $tgl1           = isset($_POST['tgl_awal']) ? $this->input->post('tgl_awal') : date('d/m/Y');
        $tgl2           = isset($_POST['tgl_akhir']) ? $this->input->post('tgl_akhir') : date('d/m/Y');
        $tgl1_cr        = date_ymd($tgl1)." 01:00:00";
        $tgl2_cr        = date_ymd($tgl2)." 23:59:59";
        $st_konsumen    = isset($_POST['st_konsumen']) ? $this->input->post('st_konsumen') : '';
        $konsumen       = isset($_POST['konsumen']) ? $this->input->post('konsumen') : '';

        $where  = "transaksi.deleted = 0
                    AND transaksi.st_history = 0
                    AND transaksi.st_lunas = 0
                    AND (transaksi.waktu >= '{$tgl1_cr}' and transaksi.waktu <= '{$tgl2_cr}')";

        if(strlen($st_konsumen) > 0){

            $where  .= " AND konsumen.st = {$st_konsumen}";

        }

        if(strlen($konsumen) > 0){

            $where  .= " AND `order_produk`.`id_konsumen` = '{$konsumen}'";

        }


        $data   = $this->trans_model
                        ->select("`transaksi`.`id_transaksi`,
                                    `transaksi`.`id_trans_ref_asli`,
                                    DATE_FORMAT(`transaksi`.`waktu`, '%d/%m/%Y %H:%i:%s') AS waktu_tampil,
                                    `transaksi`.`no_faktur`,
                                    `transaksi`.`grand_total`,
                                    `order_produk`.`id_konsumen`,
                                    CONCAT(`konsumen`.`panggilan`,
                                            ' ',
                                            `konsumen`.`nama`) AS nm,
                                    IF(konsumen.st = 0,
                                        'Konsumen',
                                        IF(konsumen.st = 1,
                                            'Reseller',
                                            'Instansi')) AS st_konsumen,
                                    IF(`bayar_trans_kasir`.`bayar` IS NULL,
                                        0,
                                        SUM(`bayar_trans_kasir`.`bayar`)) AS total_sudah_bayar,
                                    IF(`bayar_trans_kasir`.`bayar` IS NULL,
                                        `transaksi`.`grand_total`,
                                        `transaksi`.`grand_total` - SUM(`bayar_trans_kasir`.`bayar`)) AS sisa_tagihan")
                        ->join("order_produk","transaksi.id_order = order_produk.id_order","inner")
                        ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                        ->join("bayar_trans_kasir","transaksi.id_trans_ref_asli = bayar_trans_kasir.id_transaksi_master","left")
                        ->where($where)
                        ->group_by("id_transaksi")
                        ->order_by("transaksi.waktu")
                        ->find_all();

        //echo $this->db->last_query();

        $asset  =   [
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'lap_rekap_trans/assets/js/lap_rekap_trans_jual_kategori.js'
                    ];     

        add_assets($asset);

        $this->template->set("period1",$tgl1);
        $this->template->set("period2",$tgl2);   

        $this->template->set("data",$data);

        $this->template->set("st_konsumen",$st_konsumen);
        $this->template->set("konsumen",$konsumen);
        
        $this->template->set("toolbar_title", lang('title_piutang'));
        $this->template->title(lang('title_piutang'));
        $this->template->render('lap_piutang'); 
    }

    function rekap_kas_kasir(){

        $tgl1   = isset($_POST['tgl_awal']) ? $this->input->post('tgl_awal') : date('d/m/Y');
        $tgl2   = isset($_POST['tgl_akhir']) ? $this->input->post('tgl_akhir') : date('d/m/Y');
        $tgl1_cr    = date_ymd($tgl1)." 01:00:00";
        $tgl2_cr    = date_ymd($tgl2)." 23:59:59";

        $karyawan    = isset($_POST['karyawan']) ? $this->input->post('karyawan') : '';

        $dt_karyawan    = $this->karyawan_model
                            ->select("`idkaryawan` as id, nama as nm")
                            ->distinct()
                            ->join("users","karyawan.id_user = users.id_user","inner")
                            ->join("kas_kasir","users.id_user = kas_kasir.created_by","inner")
                            ->order_by("nama")
                            ->find_all();

        $where  = "kas_kasir.deleted = 0
                    AND kas_kasir.st_close = 1
                    AND(`kas_kasir`.`waktu_buat` >= '{$tgl1_cr}' and `kas_kasir`.`waktu_buat` <= '{$tgl2_cr}')";

        if(strlen($karyawan) > 0){

            $where .= "AND `karyawan`.`idkaryawan` = '{$karyawan}'";

        }

        $data   = $this->kas_kasir_model
                        ->select("`kas_kasir`.`idkas_kasir`,
                                    DATE_FORMAT(`kas_kasir`.`waktu_buat`, '%d/%m/%Y') AS tgl,
                                    `karyawan`.`nama`,
                                    `kas_kasir`.`saldo_awal`,
                                    `kas_kasir`.`total_masuk`,
                                    `kas_kasir`.`total_keluar`,
                                    `kas_kasir`.`total_debit`,
                                    `kas_kasir`.`total_transfer`,
                                    `kas_kasir`.`total_dp`,
                                    `kas_kasir`.`total_piutang`,
                                    `kas_kasir`.`saldo_akhir_system`,
                                    `kas_kasir`.`saldo_revisi`")
                        ->join("users","kas_kasir.created_by = users.id_user","inner")
                        ->join("karyawan","users.id_user = karyawan.id_user","inner")
                        ->where($where)
                        ->order_by("`kas_kasir`.`waktu_buat`")
                        ->find_all();

        //echo $this->db->last_query();

        $resume = $this->kas_kasir_model
                        ->select("SUM(`kas_kasir`.`saldo_awal`) AS ttl_saldo_awal,
                                    SUM(`kas_kasir`.`total_omset_nota`) AS ttl_omset,
                                    SUM(`kas_kasir`.`total_omset_nota_bb`) AS ttl_omset_bb,
                                    SUM(`kas_kasir`.`total_masuk`) AS ttl_cash_masuk,
                                    SUM(`kas_kasir`.`total_keluar`) AS ttl_cash_keluar,
                                    SUM(`kas_kasir`.`total_debit`) AS ttl_debit,
                                    SUM(`kas_kasir`.`total_transfer`) AS ttl_transfer,
                                    SUM(`kas_kasir`.`total_deposit`) AS ttl_deposit,
                                    SUM(`kas_kasir`.`saldo_akhir_system`),
                                    SUM(`kas_kasir`.`saldo_revisi`),
                                    SUM(IF(`kas_kasir`.`saldo_revisi` > 0,
                                        `kas_kasir`.`saldo_revisi`,
                                        `kas_kasir`.`saldo_akhir_system`)) AS ttl_final_closing")
                        ->join("users","kas_kasir.created_by = users.id_user","inner")
                        ->join("karyawan","users.id_user = karyawan.id_user","inner")
                        ->where($where)
                        ->order_by("`kas_kasir`.`waktu_buat`")
                        ->find_all();

        $asset  =   [
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'lap_rekap_trans/assets/js/lap_rekap_trans_kas_kasir.js'
                    ];     

        add_assets($asset);

        $this->template->set("period1",$tgl1);
        $this->template->set("period2",$tgl2);   
        $this->template->set("karyawan",$karyawan);

        $this->template->set("dt_karyawan",$dt_karyawan);

        $this->template->set("data",$data);
        $this->template->set("data_resume", $resume);
        
        $this->template->set("toolbar_title", lang('title_kas_kasir'));
        $this->template->title(lang('title_kas_kasir'));
        $this->template->render('lap_kas_kasir');


    }

}
	
?>