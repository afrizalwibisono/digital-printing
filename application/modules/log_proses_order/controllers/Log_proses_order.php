<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 */

class Log_proses_order extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "";
    
    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('log_proses_order/log_proses_order');
        $this->load->model(
                                [
                                    'log_proses_order/ket_proses_model',
                                    'log_proses_order/log_status_model',
                                    'log_proses_order/lokasi_model',
                                    'log_proses_order/order_model',
                                    'log_proses_order/order_detail_model'
                                ]    
                            );
        $this->template->title(lang('title'));
		$this->template->page_icon('fa fa-clock-o');
    }

    public function index($id){

        $this->auth->restrict($this->viewPermission);

        $assets =   [
                        'log_proses_order/assets/css/css_log_proses.css'
                    ];
        
        add_assets($assets);

        $this->template->set("toolbar_title", lang('title'));
        $this->template->title(lang('title'));
        
        $this->template->render('timeline_log_order'); 

    }

    public function view($id){

        $this->auth->restrict($this->viewPermission);

        // start ambil data log sesuai kode order

        //$id = "2019H05T30M1232111264"; //dummy

        $data   = $this->log_status_model
                        ->select("`log_status_proses`.`idlog_status_proses`,
                                    DATE_FORMAT(`log_status_proses`.created_on,
                                            '%d/%m/%Y %H:%i:%s') AS waktu_buat,
                                    `log_status_proses`.`id_detail_produk_order`,
                                    `log_status_proses`.`kode_universal`,
                                    `m_order_produk_detail`.`id_order`,
                                    `m_order_produk_detail`.`nama_pekerjaan`,
                                    `produk`.`nm_produk`,
                                    `produk`.`st_tipe` AS `st_tipe_produk`,
                                    `kategori`.`nmkategori`,
                                    `m_order_produk_detail`.`id_detail_produk_order`,
                                    `m_order_produk_detail`.`thumbnail`,
                                    `m_order_produk_detail`.`p`,
                                    `m_order_produk_detail`.`l`,
                                    `m_order_produk_detail`.`id_satuan` AS `id_satuan_uk`,
                                    konversi_satuan.satuan_besar,
                                    `m_order_produk_detail`.`st_urgent`,
                                    `m_order_produk_detail`.`jml_cetak` as jml_cetak_order,
                                    DATE_FORMAT(`m_order_produk_detail`.`tgl_permintaan_selesai`,
                                            '%d/%m/%Y') AS tgl_selesai,
                                    IF(`m_order_produk_detail`.`st_finishing` = 0,
                                        'Finishing Standart',
                                        'Custom Finishing') AS st_finishing,
                                    `log_status_proses`.`jml_cetak`,
                                    `ket_proses_order`.`ket` as ket_proses,
                                    `lokasi_proses`.`nm_lokasi`")
                        ->join("ket_proses_order","log_status_proses.idket_proses_order = ket_proses_order.idket_proses_order","inner")
                        ->join("lokasi_proses","log_status_proses.idlokasi_proses = lokasi_proses.idlokasi_proses","inner")
                        ->join("m_order_produk_detail","log_status_proses.id_detail_produk_order = m_order_produk_detail.id_detail_produk_order","inner")
                        ->join("produk","m_order_produk_detail.id_produk = produk.idproduk","inner")
                        ->join("konversi_satuan","m_order_produk_detail.id_satuan = konversi_satuan.id_konversi","inner")
                        ->join("kategori","produk.idkategori = kategori.idkategori","inner")
                        ->where("m_order_produk_detail.id_order = '$id'")
                        ->order_by("nmkategori","asc")
                        ->order_by("nm_produk","asc")
                        ->order_by("nama_pekerjaan","asc")
                        ->order_by("log_status_proses.created_on","desc")
                        ->find_all();
        
        $data_log   = [];

        if(is_array($data) && count($data)){

            $kode_univ_produk   = "";
            $key_ke             = 0;

            foreach ($data as $key => $isi) {
                
                if($isi->kode_universal != $kode_univ_produk){

                    $data_log[] =   [
                                        'nama_pekerjaan'    => $isi->nama_pekerjaan,
                                        'kategori'          => $isi->nmkategori,
                                        'produk'            => $isi->nm_produk,
                                        'st_tipe_produk'    => $isi->st_tipe_produk,
                                        'gambar'            => $isi->thumbnail,
                                        'ukuran'            => 'P: '.$isi->p.'x L: '.$isi->p.' '.$isi->satuan_besar,
                                        'jml'               => $isi->jml_cetak_order,
                                        'finishing'         => $isi->st_finishing,
                                        'log'               =>  [
                                                                    /*[
                                                                        'waktu'     => $isi->waktu_buat,
                                                                        'ket'       => $isi->ket_proses,
                                                                        'jml'       => $isi->jml_cetak,
                                                                        'lokasi'    => $isi->nm_lokasi
                                                                    ]    */                                        
                                                                ]
                                    ];

                    $kode_univ_produk = $isi->kode_universal;
                    $key_ke     = $key > 0 ? $key_ke+1 : $key_ke;

                }

                //if ($key > 0) {
                    
                    $data_log[$key_ke]['log'][] =   [
                                                        'waktu'     => $isi->waktu_buat,
                                                        'ket'       => $isi->ket_proses,
                                                        'jml'       => $isi->jml_cetak,
                                                        'lokasi'    => $isi->nm_lokasi
                                                    ];

                //}

                


            }

        }

        //print_r($data_log);

        // end ambil data log sesuai kode order

        $assets =   [
                        'log_proses_order/assets/css/css_log_proses.css'
                    ];
        
        add_assets($assets);

        $this->template->set("dt_log", $data_log);

        $this->template->set("toolbar_title", lang('title'));
        $this->template->title(lang('title'));
        
        $this->template->render('timeline_log_order'); 

    }

    public function set_data_log($id_lokasi = 0, $id_ket_proses = "", $id_detail_produk_order = "", $jumlah_proses = 0){

        /*
            Keterangan :
                
                $id_lokasi 
                    - Lokasi saat status dibuat.
                    - Kodenya Lokasi dapat dilihat di tabel "lokasi_proses"

                $id_ket_proses
                    - Keterangan dari proses yang sedang berlangsung.
                    - Agar keterangannya stabil maka semua keterangan disimpan di dalam tabel.
                    - Untuk kode keterangan status order dapat dilihat di tabel "ket_proses_order"

                $id_detail_produk_order
                    - Id detail order terbaru, bukan ID detail order master.

                $jumlah_proses
                    - Jumlah yang diproses saat proses

        */


        //ambil kode universalnya
        $kode_universal_detail_order    = "";

        $dt_detail_order    = $this->order_detail_model
                                ->select('`id_detail_produk_order`, `kode_universal`')
                                ->where("id_detail_produk_order = '{$id_detail_produk_order}' and deleted = 0")
                                ->find_all();

        if(is_array($dt_detail_order) && count($dt_detail_order)){

            $kode_universal_detail_order    = $dt_detail_order[0]->kode_universal;

        }else{

            return false;

        }

        $this->db->trans_start();

            $arr_isi    =   [ 
                                'idlog_status_proses'       => gen_primary("","log_status_proses","idlog_status_proses"),
                                'id_detail_produk_order'    => $id_detail_produk_order, 
                                'kode_universal'            => $kode_universal_detail_order, 
                                'idlokasi_proses'           => $id_lokasi, 
                                'idket_proses_order'        => $id_ket_proses, 
                                'jml_cetak'                 => $jumlah_proses
                            ];

            $this->log_status_model->insert($arr_isi);

        $this->db->trans_complete();

        if($this->db->trans_status() === false){

            return false;

        }else{

            return true;

        }


    }

   
}
?>