<?php if(is_array($dt_log) && count($dt_log)) : ?>

<div class="box box-primary">
    
    <div class="box-body table-responsive">
        
        <table class="tabel" > 
            
            <tbody>

                <?php foreach ($dt_log as $key => $isi) : ?>

                <tr>
                    <td class="text-center thumb-daftar" style = "width: 5%">
                        <img src="<?= base_url().$isi['gambar'] ?>" alt="" class="img-rounded">
                    </td>
                    <td class="deskripsi garis-batas" colspan="4">
                        <p class="nama-pekerjaan text-success"><?= $isi['nama_pekerjaan'] ?></p>
                        <p class="nama-pekerjaan text-info"><?= $isi['kategori'].' - '.$isi['produk'] ?></p>
                        <p class="nama-pekerjaan text-info">
                            
                            <?php 

                            if($isi['st_tipe_produk'] == 1){

                                echo 'Uk : '. $isi['ukuran'];

                            } 

                            ?>

                        </p>
                        <p class="nama-pekerjaan text-info"><?= $isi['finishing'] ?></p>
                        <p class="nama-pekerjaan text-info"><?= 'Jml Cetak : '.$isi['jml'] ?></p>
                    </td>
                </tr>

                    <?php foreach ($isi['log'] as $key => $log) : ?>                    

                        <tr>
                            <td></td>
                            <td><?= $log['waktu'] ?></td>
                            <td class="text-center"><?= $log['ket'] ?></td>
                            <td class="text-center"><?= $log['jml'] ?></td>
                            <td class="text-center"><?= strtoupper($log['lokasi']) ?></td>
                        </tr>

                    <?php endforeach; ?>

                <?php endforeach; ?>

                
            </tbody>

        </table>        

    </div>

</div>

<?php else: ?>

<div class="alert alert-info" role="alert">
    <p><i class="fa fa-warning"></i> &nbsp; <?= lang('konfirmasi-data-tidak-ada') ?></p>
</div>

<?php endif; ?>