<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ==============================================================================================
$lang['title']							= 'History Proses';

// ==============================================================================================
$lang['table-deskripsi']				= 'Deskripsi';

// ==============================================================================================
$lang['btn-filter']						= 'Filter';
$lang['btn-create']						= 'Baru';
$lang['btn-save']						= "Simpan";
$lang['btn-batal']						= "Batal / Kembali";
$lang['btn-reset_clear']				= "Batal / Reset";
$lang['btn-delete']						= "Hapus";
$lang['btn-delete-history']				= "Hapus Semua History Transaksi";
$lang['btn-hanya-cetak']				= "Hanya Cetak Nota";
$lang['btn-kembali']					= "Kembali";

$lang['bf_or']							= "or";

// =============================================================================================

$lang['konfirmasi-data-tidak-ada']		= "Data tidak ditemukan";



