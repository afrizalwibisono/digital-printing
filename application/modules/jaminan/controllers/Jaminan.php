<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2019, Cokeshome
 * 
 * This is controller for Jaminan
 */

class Jaminan extends Admin_Controller {

    protected $uploadFolder     = "./upload/jaminan";
    protected $viewPermission   = "Jaminan.View";
    protected $addPermission    = "Jaminan.Add";
    
    public function __construct()
    {
        parent::__construct();
        $this->auth->restrict($this->viewPermission);

        $this->lang->load('jaminan/jaminan');

        $this->load->model([
                        'jaminan/jaminan_model', 
                        'jaminan/konsumen_model'
                        ]);

        $this->template->title(lang('jm_title'));
        $this->template->page_icon('fa fa-archive');
    }

    public function index(){
        $this->auth->restrict($this->viewPermission);

        $search = $this->input->post('table_search') ? $this->input->post('table_search') : $this->input->get('search');
        $idc    = $this->input->post('idkonsumen') ? $this->input->post('idkonsumen') : $this->input->get('idc');

        $filter = "?search=".urlencode($search);
        $search2 = $this->db->escape_str($search);

        $where = "jaminan.deleted = 0 AND nama like '%$search2%'";
        if($idc){
            $filter .= "&idc=".urlencode($idc);
            $where  .= " AND jaminan.idkonsumen='".$idc."'";
        }

        $total = $this->konsumen_model
                    ->join("(SELECT 
                                    jaminan.idkonsumen, jaminan.saldo, jaminan.deleted
                                FROM
                                    jaminan
                                    join
                                    (SELECT 
                                    MAX(id_jaminan) as id_jaminan, idkonsumen
                                    FROM
                                        jaminan
                                    where jaminan.deleted = 0
                                    GROUP BY idkonsumen) as tsaldo on jaminan.id_jaminan=tsaldo.id_jaminan
                                WHERE
                                    deleted = 0) AS jaminan", "konsumen.idkonsumen=jaminan.idkonsumen")
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;
        // Pagination
        $this->load->library('pagination');

        $this->pagination->initialize($this->pager);

        $data = $this->konsumen_model
                    ->select("jaminan.*,
                                konsumen.panggilan,
                                konsumen.nama,
                                konsumen.st, 
                                IF(`konsumen`.`st` = 0,
                                'Konsumen',
                                IF(`konsumen`.`st` = 1,
                                    'Reseller',
                                    'Instansi')) AS st_konsumen,
                                jaminan.saldo
                                ")
                    ->join("(SELECT 
                                    jaminan.idkonsumen, jaminan.saldo, jaminan.deleted
                                FROM
                                    jaminan
                                    join
                                    (SELECT 
                                    MAX(id_jaminan) as id_jaminan, idkonsumen
                                    FROM
                                        jaminan
                                    where jaminan.deleted = 0
                                    GROUP BY idkonsumen) as tsaldo on jaminan.id_jaminan=tsaldo.id_jaminan
                                WHERE
                                    deleted = 0) AS jaminan", "konsumen.idkonsumen=jaminan.idkonsumen")
                    ->where($where)
                    ->order_by(['nama' => 'ASC'])
                    ->limit($limit, $offset)
                    ->find_all();

        $dt_konsumen    = $this->konsumen_model
                                ->select("`idkonsumen` as id, 
                                    CONCAT(IF(panggilan IS NULL, '', panggilan),
                                    ' ',
                                    `nama`,
                                    ' | ',
                                    IF(st = 0,
                                        'konsumen',
                                        IF(st = 1, 'reseller', 'instansi'))) AS nm")
                                ->where("deleted", 0)
                                ->order_by("nama","asc")
                                ->find_all();

        $assets = array('plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'jaminan/assets/js/jaminan_index.js'
                        );

        add_assets($assets);

        $this->template->set("data", $data);
        $this->template->set("dt_konsumen", $dt_konsumen);
        $this->template->set("idkonsumen", $idc);
        $this->template->set("search", $search);

        $this->template->title(lang('jm_title'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index');
    }

    public function create(){
        $this->auth->restrict($this->addPermission);

        if(isset($_POST['save'])){
            if($this->save_jaminan()){
              $this->template->set_message(lang("jm_create_success"), 'success');
              redirect('jaminan');
            }
        }

        $dt_konsumen    = $this->konsumen_model
                                ->select("`idkonsumen` as id, 
                                    CONCAT(IF(panggilan IS NULL, '', panggilan),
                                    ' ',
                                    `nama`,
                                    ' | ',
                                    IF(st = 0,
                                        'konsumen',
                                        IF(st = 1, 'reseller', 'instansi'))) AS nm")
                                ->where("deleted", 0)
                                ->order_by("nama","asc")
                                ->find_all();

        $assets = array('plugins/datetimepicker/bootstrap-datetimepicker.css',
                        'plugins/datetimepicker/bootstrap-datetimepicker.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'plugins/number/jquery.number.js',
                        'jaminan/assets/js/jaminan_create.js'
                        );

        add_assets($assets);

        $this->template->set("dt_konsumen", $dt_konsumen);

        $this->template->title(lang('jm_title_new'));
        $this->template->render('form');
    }

    public function penarikan($id_konsumen = ''){
        if(!$id_konsumen){
            redirect('jaminan');
        }

        if(isset($_POST['save'])){
            if($this->save_penarikan($id_konsumen)){
                $this->template->set_message(lang("jm_tarik_success"), 'success');
                redirect('jaminan');
            }
        }

        $dt_konsumen    = $this->konsumen_model
                                ->select("`idkonsumen` as id, 
                                    CONCAT(IF(panggilan IS NULL, '', panggilan),
                                    ' ',
                                    `nama`,
                                    ' | ',
                                    IF(st = 0,
                                        'konsumen',
                                        IF(st = 1, 'reseller', 'instansi'))) AS nm")
                                ->find($id_konsumen);
        if(!$dt_konsumen){
            $this->template->set_message(lang("jm_invalid_id"), 'error');
            redirect('jaminan');
        }

        $this->auth->restrict($this->addPermission);

        $assets = array('plugins/datetimepicker/bootstrap-datetimepicker.css',
                        'plugins/datetimepicker/bootstrap-datetimepicker.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'plugins/number/jquery.number.js',
                        'jaminan/assets/js/jaminan_penarikan.js'
                        );

        add_assets($assets);

        $saldo = $this->_get_saldo_jaminan($id_konsumen);

        $this->template->set("dt_konsumen", $dt_konsumen);
        $this->template->set("saldo", $saldo);

        $this->template->title(lang('jm_title_penarikan'));
        $this->template->render('penarikan');
    }

    public function detail($id_konsumen = '', $xlimit = 0, $pdf = 0){
        $this->auth->restrict($this->viewPermission);

        if(!$id_konsumen){
            $this->template->set_message(lang('jm_invalid_id'), 'error');
            redirect('jaminan');
        }

        $limit = $this->input->get('tampilkan');
        if(!$limit){
            $limit = 10;
        }

        if($limit > 1000){
            $limit = 1000;
        }

        if($xlimit){
            $limit = $xlimit;
        }

        $konsumen = $this->konsumen_model->select("idkonsumen, panggilan, nama, alamat, email, kota")->find($id_konsumen);
        $data = $this->jaminan_model
                            ->order_by('id_jaminan', 'DESC')
                            ->limit($limit)
                            ->find_all_by(['deleted' => 0, 'idkonsumen' => $id_konsumen]);
        //Identitas
        $identitas = $this->identitas_model->find(1);

        $assets = array('jaminan/assets/js/jaminan_detail.js');

        add_assets($assets);

        $this->template->set("konsumen", $konsumen);
        $this->template->set("data", $data);
        $this->template->set("idt", $identitas);
        $this->template->set("limit", $limit);

        $this->template->title(lang('jm_title_view'));

        if($pdf){
            $this->load->library('pdfmaker');

            $this->template->set_layout('pdf');
            $html = $this->template->render('pdf', [], true);

            $pdf = $this->pdfmaker->generate($html,'jaminan', false);
            return $pdf;
        }else{
            $this->template->render('detail');
        }
    }

    /**
     * Simpan transaksi jaminan
     * @param  string  $id_konsumen       Id primary key Konsumen
     * @param  integer $st_in             1 = In, 0 = Out
     * @param  integer $nilai             nilai jaminan yang masuk/keluar
     * @param  string  $ket               Keterangan tambahan
     * @param  integer $st_valid          0 = Tidak valid, 1 = Valid, 2 = Menunggu pengecekan ke bank (jika metode bayar == 2 [transfer]), hanya diisi jika st_in == 1
     * @param  datetime  $waktu           Waktu Transaksi, jika dikosongi maka akan dianggap waktu sekarang
     * @param  integer $metode_bayar      1 = Cash, 2 = Transfer
     * @param  string  $rekening_pengirim nomor rekening pengirim, diisi jika metode bayar == 2 [transfer]
     * @param  array  $input_file_resi    isi dengan name file input upload resi
     * @return boolean                     true jika sukses, false jika gagal
     */
    public function _simpan_jaminan($id_konsumen = '', $st_in = null, $nilai = 0, $ket = '', $st_valid = 1, $waktu = null, $metode_bayar = 1, $rekening_pengirim = '', $input_file_resi = null){
        if(!$id_konsumen || is_null($st_in) || $st_in === '' || doubleval($nilai) <= 0){
            return false;
        }
        
        if(!$waktu){
            $waktu = date('Y-m-d H:i:s');
        }

        //validate konsumen
        $cek_konsumen = $this->konsumen_model->find($id_konsumen);
        if(!$cek_konsumen){
            return false;
        }

        if($st_in == 1){ // Jaminan Masuk
            // Jika transfer maka wajib $rekening_pengirim dan resi
            if($metode_bayar == 2 && (!$rekening_pengirim || !$input_file_resi)){
                return false;
            }
            // Get Last Saldo
            $last_saldo = $this->_get_saldo_jaminan($id_konsumen);
            if(!$last_saldo){
                $last_saldo = 0;
            }

            $data = [
                'idkonsumen' => $id_konsumen,
                'st_in' => $st_in,
                'waktu' => $waktu,
                'metode_bayar' => $metode_bayar,
                'rekening_pengirim' => $rekening_pengirim,
                'nilai' => doubleval($nilai),
                'saldo' => $last_saldo + doubleval($nilai),
                'ket' => $ket,
                'st_valid' => $st_valid
                ];

            $id_jaminan = $this->jaminan_model->insert($data);
            if($id_jaminan && $_FILES[$input_file_resi]['name'] != ''){
                if (!is_dir($this->uploadFolder)){
                    mkdir($this->uploadFolder, 0777, TRUE);
                }
                // Upload Resi
                $this->load->library('upload');
                $config['upload_path']   = $this->uploadFolder;
                $config['allowed_types'] = 'jpeg|jpg|png';
                $config['max_size']      = '0'; // Max size unlimited, tapi akan diresize dibawah
                $config['file_ext_tolower']  = TRUE;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload($input_file_resi)){
                    return FALSE;               
                }else{
                    $upload_data = $this->upload->data();
                    $file_name   = $upload_data['file_name'];
                    //update jaminan
                    $this->jaminan_model->update($id_jaminan, ['file_resi' => $file_name]);

                    //Resize Photo
                    $this->load->library('image_lib');
                    $image_config = array(
                        'image_library' => 'imagemagick',
                        'library_path' => '/usr/bin',
                        'source_image' => $upload_data['full_path'],
                        'maintain_ratio' => TRUE,
                        'width' => 720,
                    );

                    $this->image_lib->initialize($image_config);
                    $this->image_lib->resize();
                    $this->image_lib->clear();
                }
            }

            // Update Status Konsumen jadi reseller
            $this->konsumen_model->update($id_konsumen, ['st' => 1]);

            return $id_jaminan ? true : false;
        }else{ // Jaminan keluar
            $saldo = $this->_get_saldo_jaminan($id_konsumen);
            if(!$saldo){
                $saldo = 0;
            }

            if($saldo == 0 || $saldo < $nilai){
                return false;
            }

            $nilai = doubleval($nilai);
            $saldo_akhir = $saldo - $nilai;

            // Insert Jaminan yang dipakai
            $dt_insert  = [
                            'idkonsumen' => $id_konsumen,
                            'st_in' => $st_in,
                            'waktu' => $waktu,
                            'nilai' => $nilai,
                            'saldo' => $saldo_akhir,
                            'ket' => $ket,
                            'st_valid' => 1
                            ];
            $insert = $this->jaminan_model->insert($dt_insert);
            // Update Status Konsumen jadi konsumen biasa
            $this->konsumen_model->update($id_konsumen, ['st' => 0]);

            return $insert ? true : false;
        }
    }

    private function _get_saldo_jaminan($id_konsumen = ''){
        if(!$id_konsumen){
            return false;
        }

        $dp_data = $this->jaminan_model->select('saldo')
                                        ->order_by('id_jaminan', 'DESC')
                                        ->limit(1)
                                        ->find_by(['idkonsumen' => $id_konsumen, 'st_valid' => 1, 'deleted' => 0]);
        if($dp_data){
            return doubleval($dp_data->saldo);
        }

        return false;
    }

    private function save_jaminan(){
        $idkonsumen = $this->input->post('idkonsumen');
        $nilai      = $this->input->post('nilai');
        $_POST['nilai'] = remove_comma($nilai); 
        $nilai          = $_POST['nilai'];
        $ket        = $this->input->post('ket');
        $waktu      = $this->input->post('waktu');
        $metode_bayar = $this->input->post('metode_bayar');
        $rekening_pengirim = $this->input->post('rekening_pengirim');
        $file_resi    = $this->input->post('file_resi');

        $this->form_validation->set_rules('idkonsumen','lang:jm_konsumen','callback_default_select');
        $this->form_validation->set_rules('nilai','lang:jm_nilai','required|greater_than[0]');
        $this->form_validation->set_rules('ket','lang:jm_ket','trim');
        $this->form_validation->set_rules('waktu','lang:jm_tanggal','trim|required');
        $this->form_validation->set_rules('metode_bayar','lang:jm_metode_bayar','callback_default_select');
        if($metode_bayar == 2){
            $this->form_validation->set_rules('rekening_pengirim','lang:jm_no_rek','trim|required');
            if (empty($_FILES['file_resi']['name'])){
                $this->form_validation->set_rules('file_resi','lang:jm_upload_resi','required');
            }
        }

        $this->form_validation->set_message('default_select', 'You should choose the "%s" field');

        if ($this->form_validation->run() === FALSE) {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        $waktu = date_ymd($waktu);

        $result = $this->_simpan_jaminan($idkonsumen, 1, $nilai, $ket, 1, $waktu, $metode_bayar, $rekening_pengirim, 'file_resi');
        if($result){
            // Send Email
            $this->send_email_saldo_jaminan($idkonsumen, 'New Jaminan', ['nominal' => $nilai, 'tanggal' => $waktu], 1);
        }

        return $result;
    }

    private function save_penarikan($id_konsumen = ''){
        $saldo = $this->_get_saldo_jaminan($id_konsumen);
        if(!$saldo){
            $saldo = 0;
        }

        $nilai = $saldo;
        $_POST['nilai'] = $nilai; 
        $ket        = $this->input->post('ket');

        $this->form_validation->set_rules('ket','lang:jm_ket','trim|required');

        if ($this->form_validation->run() === FALSE) {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        $result = $this->_simpan_jaminan($id_konsumen, 0, $nilai, $ket);
        if($result){
            // Send Email
            $this->send_email_saldo_jaminan($id_konsumen, 'Penarikan Jaminan', ['nominal' => $nilai, 'tanggal' => date('Y-m-d H:i:s')], 2);
        }

        return $result;
    }

    public function default_select($val)
    {
        return $val == "" ? FALSE : TRUE;
    }

    public function cek_konsumen_email(){
        if(!$this->input->is_ajax_request()){
            $this->template->set_message(lang('jm_only_ajax'), 'error');
            redirect('jaminan');
        }

        $idkonsumen = $this->input->post('idkonsumen');
        $cek = $this->konsumen_model->find($idkonsumen);
        if(!$cek){
            $return = ['type' => 'error', 'msg' => lang('jm_no_konsumen')];
        }

        if(!filter_var($cek->email, FILTER_VALIDATE_EMAIL)){
            $return = ['type' => 'error', 'msg' => lang('jm_email_not_valid')];
        }else{
            $return = ['type' => 'success', 'msg' => ''];
        }

        echo json_encode($return);
    }
    /**
     * $type => 1 = New Jaminan, 2 = Penarikan Jaminan, 3 = Rekening koran
     */
    public function send_email_saldo_jaminan($idkonsumen = '', $subject = 'Email Notif', $data = ['nominal' => 0, 'tanggal' => ''], $type = 1, $xlimit = 10){
        if(!$idkonsumen){
            if(!$this->input->is_ajax_request()){
                $this->template->set_message(lang('jm_only_ajax'), 'error');
                redirect('jaminan');
            }

            $subject = $this->input->post('subject');
        }

        $cek = $this->konsumen_model->find($idkonsumen);
        if(!$cek){
            $return = ['type' => 'error', 'msg' => lang('jm_no_konsumen')];
        }

        if(!filter_var($cek->email, FILTER_VALIDATE_EMAIL)){
            return false;
        }

        //Send Email to Customer
        $subject = $subject;
        $to      = $cek->email;
        $data_msg = ['full_name' => $cek->panggilan." ".$cek->nama,
                     'nominal'   => $data['nominal'],
                     'tanggal'   => $data['tanggal']
                    ];
        if($type == 1){
            $message = $this->load->view('jaminan/email/new_jaminan', $data_msg, TRUE);    
        }else if($type == 2){
            $message = $this->load->view('jaminan/email/penarikan_jaminan', $data_msg, TRUE);    
        }else{
            $message = $this->load->view('jaminan/email/rekening_koran_jaminan', $data_msg, TRUE); 
        }
        
        $message_body  = $this->load->view('jaminan/email/_header.php', '', TRUE);
        $message_body .= $message;
        $message_body .= $this->load->view('jaminan/email/_footer.php', '', TRUE);
        $pdf = $this->detail($idkonsumen, $xlimit, 1);
        //Load Email Library
        $this->load->library('mailer');
        $mail = $this->mailer->load();
        $mail->Subject = $subject;
        $mail->Body = $message_body;
        $mail->AddAddress($to);
        $mail->addStringAttachment($pdf, "Saldo Jaminan " . date("d-m-Y-H-i-s") . ".pdf");
        // $mail->SMTPDebug = 2;
        $sent = $mail->Send();
        
        if($sent !== false){
            return true;
        }

        return false;
    }

    public function send_rekening_koran(){
        if(!$this->input->is_ajax_request()){
            $this->template->set_message(lang('jm_only_ajax'), 'error');
            redirect('jaminan');
        }

        $idkonsumen = $this->input->post('idkonsumen');
        $limit      = $this->input->post('limit');

        $cek = $this->konsumen_model->find($idkonsumen);
        if(!$cek){
            $return = ['type' => 'error', 'msg' => lang('jm_invalid_id')];
        }

        $saldo = $this->_get_saldo_jaminan($idkonsumen);
        $waktu = date('Y-m-d H:i:s');
        // Send Email
        $sent = $this->send_email_saldo_jaminan($idkonsumen, 'Informasi Saldo Jaminan', ['nominal' => $saldo, 'tanggal' => $waktu], 3);
        if($sent !== false){
            $return = ['type' => 'success', 'msg' => lang('jm_email_sent')];
        }else{
            $return = ['type' => 'error', 'msg' => lang('jm_email_sent_failed')];
        }

        echo json_encode($return);
    }
}
