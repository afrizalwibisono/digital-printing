$(function(){
	//Konsumen
    $("#idkonsumen").select2({ placeholder : "-- Pilih Konsumen --", allowClear:true});
	//Load Foto
    $("#file_resi").change(function(){
        loadfoto(this,'img-resi');
    });
    //Format Harga
	$("input[name='nilai']").number(true, 0);
	// Metode Pembayaran
	var metode_bayar = $("#metode_bayar").val();
	if(metode_bayar != 2){
		$(".transfer").hide();
	}
	$("#metode_bayar").on("change", function(){
		if($(this).val() !=2){
			$(".transfer").hide();
		}else{
			$(".transfer").show();
			$("#rekening_pengirim")[0].focus();
		}
	});
});