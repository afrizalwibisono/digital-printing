$(function(){
	//Konsumen
    $("#idkonsumen").select2({ placeholder : "-- Pilih Konsumen --", allowClear:true});
    //Date Picker
    var maxDate = new Date();
	$('#waktu').datetimepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy hh:ii:ss",
	    minuteStep: 1,
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true,
	    endDate: maxDate
	});
	//Load Foto
    $("#file_resi").change(function(){
        loadfoto(this,'img-resi');
    });
    //Format Harga
	$("input[name='nilai']").number(true, 0);
	// Metode Pembayaran
	var metode_bayar = $("#metode_bayar").val();
	if(metode_bayar != 2){
		$(".transfer").hide();
	}
	$("#metode_bayar").on("change", function(){
		if($(this).val() !=2){
			$(".transfer").hide();
		}else{
			$(".transfer").show();
			$("#rekening_pengirim")[0].focus();
		}
	});

	// Validate Email before submit
	var bypass = false;
	$("#frm_jaminan").submit(function(e){
		if(!bypass){
			var idkonsumen = $("#idkonsumen").val();
			if(!idkonsumen){
				alertify.error('Silahkan pilih konsumen dulu');
				return false;
			}

			var respon;
			$.ajax({
				url: siteurl + 'jaminan/cek_konsumen_email',
				type: 'post',
				data: {idkonsumen: idkonsumen},
				dataType: 'json',
				async: false,
				success: function(res){
					respon = res;
				}
			});

			if(respon.type == 'error'){
				alertify.confirm(respon.msg, 
				function(){
					bypass = true;
					$("#save").trigger('click');
				},
				function(){

				}).set('labels', {ok:'Ya, Lanjutkan', cancel:'Batal'});
			}else{
				return true;
			}

			return false;
		}
	});
});