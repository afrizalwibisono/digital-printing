<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['jm_title'] 		= 'Jaminan';
$lang['jm_title_new'] 	= 'Jaminan Baru';
$lang['jm_title_penarikan'] = 'Penarikan Jaminan';
$lang['jm_title_view'] 	= 'Detail Transaksi Jaminan';

// form/table
$lang['jm_konsumen'] 	= 'Konsumen';
$lang['jm_tp_konsumen'] = 'Tipe Konsumen';
$lang['jm_tanggal'] 	= 'Tanggal';
$lang['jm_metode_bayar']= 'Metode Pembayaran';
$lang['jm_cash'] 		= 'Cash';
$lang['jm_transfer'] 	= 'Transfer';
$lang['jm_no_rek'] 		= 'No. Rekening Pengirim';
$lang['jm_upload_resi'] = 'Upload Bukti Transfer';
$lang['jm_resi'] 		= 'Bukti Transfer';
$lang['jm_nilai'] 		= 'Nominal Jaminan';
$lang['jm_nilai_penarikan'] = 'Nominal Penarikan';
$lang['jm_uang_masuk'] 	= 'Uang Masuk';
$lang['jm_uang_keluar'] = 'Uang Keluar';
$lang['jm_saldo'] 		= 'Saldo';
$lang['jm_saldo_akhir'] = 'Saldo Akhir';
$lang['jm_ket'] 	= 'Keterangan';
$lang['jm_status'] 	= 'Status Pembayaran';
$lang['jm_tdk_valid'] 	= 'Tidak Valid';
$lang['jm_valid'] 		= 'Valid';
$lang['jm_cek_bank'] 	= 'Menunggu Pengecekan';
$lang['jm_detail'] 	= 'Rincian Transaksi Jaminan';
$lang['jm_alamat'] 	= 'Alamat';
$lang['jm_tampilkan'] = 'Tampilkan';
$lang['jm_tgl_cetak'] = 'Tgl. Cetak';
$lang['jm_detail'] 	= 'Detail %s transaksi terakhir';
$lang['jm_email'] 	= 'Email';
$lang['jm_pilih'] 	= 'Pilih';

// button
$lang['jm_btn_new'] 	= 'Baru';
$lang['jm_btn_delete'] 	= 'Hapus';
$lang['jm_btn_save'] 	= 'Simpan';
$lang['jm_btn_cancel'] 	= 'Batal';
$lang['jm_btn_back'] 	= 'Kembali';
$lang['jm_or'] 			= 'atau';
$lang['jm_close'] 		= 'Tutup';

// messages
$lang['jm_del_error']		= 'Anda belum memilih jaminan yang akan dihapus.';
$lang['jm_del_failure']		= 'Tidak dapat menghapus jaminan: ';
$lang['jm_delete_confirm']	= 'Apakah anda yakin akan menghapus jaminan terpilih ?';
$lang['jm_cancel_confirm']	= 'Apakah anda yakin akan membatalkan transaksi Jaminan ?';
$lang['jm_deleted']			= 'Data Jaminan berhasil dihapus';
$lang['jm_no_records_found'] = 'Data tidak ditemukan.';
$lang['jm_no_konsumen'] 	= 'Data konsumen tidak ditemukan.';
$lang['jm_email_not_valid'] = 'Alamat email konsumen belum ada/tidak valid.
<br>Alamat email diperlukan untuk mengirimkan bukti setor jaminan kepada konsumen.
<br><br>Apakah anda ingin tetap melanjutkan ?';
$lang['jm_email_sent'] = 'Email telah terkirim';
$lang['jm_email_sent_failed'] = 'Email gagal dikirim';

$lang['jm_create_failure'] 	= 'Jaminan baru gagal disimpan: ';
$lang['jm_create_success'] 	= 'Jaminan baru berhasil disimpan';
$lang['jm_canceled'] 		= 'Jaminan telah berhasil dibatalkan';

$lang['jm_tarik_failure'] 	= 'Penarikan jaminan gagal disimpan: ';
$lang['jm_tarik_success'] 	= 'Penarikan jaminan berhasil disimpan';

$lang['jm_edit_success'] 	= 'Jaminan berhasil disimpan';
$lang['jm_invalid_id'] 		= 'ID Tidak Valid';

$lang['jm_only_ajax'] 		= 'Hanya ajax request yang diijinkan.';

$lang['jm_date_required'] 	= 'Silahkan pilih tanggal awal dan akhir.';
