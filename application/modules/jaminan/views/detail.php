<?php 
    $ENABLE_ADD     = has_permission('Jaminan.Add');
?>
<div class="box box-primary">
	<?php if($idt) : ?>
    <div class="row header-laporan visible-print">
        <div class="col-md-12">
            <div class="col-md-1 laporan-logo">
                <img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo">
            </div>
            <div class="col-md-6 laporan-identitas">
                <address>
                    <h4><?= $idt->nm_perusahaan ?></h4>
                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?>
                </address>
            </div>
            <div class="col-md-5 text-right">
                <h4>Jaminan Konsumen</h4>
            </div>
            <div class="clearfix"></div>
            <div class="laporan-garis"></div>
        </div>
    </div>
    <?php endif ?>
	<div class="box-header">
		<?= form_open($this->uri->uri_string(), ['class' => 'form-horizontal', 'id' => 'frm-detail', 'method' => 'get']) ?>
			<div class="form-group">
				<label for="nm_konsumen" class="control-label col-md-2 col-sm-2"><?= lang('jm_konsumen') ?></label>
				<div class="col-md-5 col-sm-5">
					<input type="text" class="form-control" value="<?= strtoupper($konsumen->panggilan." ".$konsumen->nama) ?>" readonly="">
				</div>
			</div>
			<div class="form-group">
				<label for="alamat" class="control-label col-md-2 col-sm-2"><?= lang('jm_alamat') ?></label>
				<div class="col-md-5 col-sm-5">
					<input type="text" class="form-control" value="<?= $konsumen->alamat ?>" readonly="">
				</div>
			</div>
			<?php if($konsumen->email) : ?>
			<div class="form-group">
				<label for="email" class="control-label col-md-2 col-sm-2"><?= lang('jm_email') ?></label>
				<div class="col-md-5 col-sm-5">
					<input type="text" class="form-control" value="<?= $konsumen->email ?>" readonly="">
				</div>
			</div>
			<?php endif; ?>
			<div class="form-group visible-print">
				<label for="tgl_cetak" class="control-label col-md-2 col-sm-2"><?= lang('jm_tgl_cetak') ?></label>
				<div class="col-md-5 col-sm-5">
					<input type="text" class="form-control" value="<?= date('d/m/Y H:i:s') ?>" readonly="">
				</div>
			</div>
			<div class="form-group hidden-print">
				<label for="periode" class="control-label col-md-2"><?= lang('jm_tampilkan') ?></label>
				<div class="col-md-5">
					<select class="form-control" name="tampilkan" id="tampilkan">
						<option value="10" <?= set_select('tampilkan', 10, isset($limit) && $limit == 10) ?>>10 Transaksi</option>
						<option value="100" <?= set_select('tampilkan', 100, isset($limit) && $limit == 100) ?>>100 Transaksi</option>
						<option value="500" <?= set_select('tampilkan', 500, isset($limit) && $limit == 500) ?>>500 Transaksi</option>
						<option value="1000" <?= set_select('tampilkan', 1000, isset($limit) && $limit == 1000) ?>>1.000 Transaksi</option>
					</select>
				</div>
			</div>
			<div class="form-group visible-print">
				<label for="" class="control-label col-md-2 col-sm-2"><?= lang('jm_saldo_akhir') ?></label>
				<div class="col-md-5 col-sm-5">
					<input type="text" class="form-control" value="Rp. <?= isset($data) ? number_format($data[0]->saldo) : '0' ?>" readonly="">
				</div>
			</div>
			<div class="form-group hidden-print">
				<div class="col-md-5 col-md-offset-2">
					<?php if($konsumen->email) : ?>
					<button type="button" id="send-email" class="btn btn-default" data-id="<?= $konsumen->idkonsumen; ?>"><i class="fa fa-send"></i> Send Email</button>
					<?php endif; ?>
					<button type="button" id="cetak" class="btn btn-default" onclick="cetak_halaman()"><i class="fa fa-print"></i> Cetak</button>
				</div>
			</div>
		<?= form_close(); ?>
	</div>
    <div class="box-body table-responsive laporan-body">
    	<h4><?= sprintf(lang('jm_detail'), $limit) ?></h4>
        <table class="table table-condensed table-bordered">
            <thead>
                <tr>
                    <th width="50">#</th>
                    <th class="text-center"> <?= lang('jm_tanggal') ?> </th>
                    <th class="text-center"> <?= lang('jm_ket') ?></th>
                    <th class='text-center'> <?= lang('jm_uang_masuk') ?> </th>
                    <th class='text-center'> <?= lang('jm_uang_keluar') ?> </th>
                    <th class='text-center'> <?= lang('jm_saldo') ?> </th>
                </tr>    
            </thead>            
            <tbody>
                <?php 
                    $num    = 0;
                    foreach ($data as $key => $isi):
                    $num++;
                ?>
                    <tr>
                        <td><?= $num ?></td>
                        <td><?= date('d/m/Y H:i:s', strtotime($isi->waktu)) ?></td>
                        <td>
                        	<a href="#" class="md-detail" 
                        	data-metode="<?= $isi->metode_bayar == 1 ? lang('jm_cash') : lang('jm_transfer') ?>"
                        	data-nilai="<?= number_format($isi->nilai) ?>"
                        	data-ket="<?= $isi->ket ?>"
                        	data-rekening="<?= $isi->rekening_pengirim ?>"
                        	data-resi="<?= $isi->metode_bayar == 2 && $isi->file_resi != '' ? base_url('upload/jaminan/'.$isi->file_resi) : '#' ?>" 
                        	data-toggle="modal" 
                        	data-target="#modal-detail" title="Klik untuk melihat detail"><?= $isi->ket ?></a>
                        </td>
                        <td class="text-right"><?= $isi->st_in == 1 ? number_format($isi->nilai) : '-' ?></td>
                        <td class="text-right text-red"><?= $isi->st_in == 0 ? number_format($isi->nilai) : '-' ?></td>
                        <td class="text-right"><?= number_format($isi->saldo) ?></td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
    <div class="box-footer">
        <div class="form-group">
	  		<div class="col-md-12">
	  			<?php echo anchor('jaminan', lang('jm_btn_back'), array('class' =>'btn btn-success')) ?>
	  		</div>
	  	</div>
    </div>
</div><!-- /.box -->
<div class="modal" id="modal-detail" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
	    <div class="modal-content">
	    	<?= form_open("",array('id'=>'frm_detail','name'=>'frm_detail','role'=>'form')) ?>
		      	<div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        	<h4 class="modal-title"><?= lang('jm_title_view') ?></h4>
		      	</div>
		      	<div class="modal-body">
		      		<div class="form-group">
		      			<label for="" class="control-label"><?= lang('jm_metode_bayar') ?></label>
		      			<input type="text" class="form-control" id="d-metode-bayar" value="" placeholder="" readonly="">
		      		</div>
		      		<div class="form-group">
		      			<label for="" class="control-label"><?= lang('jm_nilai') ?></label>
		      			<input type="text" class="form-control" id="d-nilai" value="" placeholder="" readonly="">
		      		</div>
		      		<div class="form-group">
		      			<label for="" class="control-label"><?= lang('jm_ket') ?></label>
		      			<textarea class="form-control" id="d-ket" readonly=""></textarea>
		      		</div>
		      		<div class="form-group div-transfer" style="display: none">
		      			<label for="" class="control-label"><?= lang('jm_no_rek') ?></label>
		      			<input type="text" class="form-control" id="d-no-rek" value="" placeholder="" readonly="">
		      		</div>
		      		<div class="form-group div-transfer" style="display: none">
		      			<label for="" class="control-label"><?= lang('jm_resi') ?></label>
		      			<div class="thumbnail">
		      				<img src="#" id="d-resi" alt="" class="img-thumbnail">
		      			</div>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
		      		<button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('jm_close') ?></button>
		      	</div>
	      	<?= form_close() ?>
	    </div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->