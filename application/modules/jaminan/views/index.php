<?php 
    $ENABLE_ADD     = has_permission('Jaminan.Add');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_list_jaminan','name'=>'frm_list_jaminan')) ?>
    <div class="box-header">
        <?php if($ENABLE_ADD): ?>
        <a class="btn btn-success" href="<?= site_url('jaminan/create') ?>"><?= lang('jm_btn_new') ?></a>
        <?php endif; ?>
        <div class="pull-right form-inline">
            <div class="form-group">
                <select class="form-control" name="idkonsumen" id="idkonsumen">
                    <option value=""></option>
                    <?php 
                        if($dt_konsumen):
                            foreach ($dt_konsumen as $key => $val):
                    ?>
                            <option value="<?= $val->id ?>"  <?= set_select('idkonsumen',$val->id, isset($idkonsumen) && $val->id == $idkonsumen ) ?> ><?= strtoupper($val->nm) ?></option>
                    <?php endforeach;endif;?>
                </select>
            </div>
            <div class="input-group">
                <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="Pencarian..." autofocus />
                <div class="input-group-btn">
                    <button class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>     
    </div>
    <?= form_close();?>  
    <?php if(isset($data) && is_array($data) && count($data)): ?>
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th width="50">#</th>
                    <th class="text-center"> <?= lang('jm_konsumen') ?> </th>
                    <th class="text-center"> <?= lang('jm_tp_konsumen') ?></th>
                    <th class='text-center'> <?= lang('jm_saldo') ?> </th>
                    <th width="80"></th>
                </tr>    
            </thead>            
            <tbody>
                <?php 
                    $num    = 0;
                    foreach ($data as $key => $isi):
                    $num++;
                ?>
                    <tr>
                        <td class="text-center"><?= $num ?></td>
                        <td class="text-center"><?= strtoupper($isi->panggilan." ".$isi->nama) ?></td>
                        <td class="text-center"><?= $isi->st_konsumen ?></td>
                        <td class="text-center"><?= number_format($isi->saldo) ?></td>
                        <td>
                            <a class="text-black" href="<?= site_url('jaminan/detail/'.$isi->idkonsumen) ?>" data-toggle="tooltip" data-placement="left" title="View Detail"><i class="fa fa-folder"></i></a>
                            &nbsp;|&nbsp;
                            <a class="text-black" href="<?= site_url('jaminan/penarikan/'.$isi->idkonsumen) ?>" data-toggle="tooltip" data-placement="left" title="Penarikan Dana"><i class="fa fa-money"></i></a>
                        </td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
    <div class="box-footer clearfix">
        <?= $this->pagination->create_links() ?> 
    </div>
    
    <?php else: ?>

    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('jm_no_records_found') ?></p>
    </div>
    
    <?php endif; ?>
</div><!-- /.box -->
