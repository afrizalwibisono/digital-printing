<div class="box box-primary" lang="en">
    <!--form start -->
    <?= form_open_multipart($this->uri->uri_string(),array('id'=>'frm_jaminan','name'=>'frm_jaminan','class'=>'form-horizontal'))?>
    <div class="box-body">
    	<div class="form-group <?= form_error('idkonsumen') ? ' has-error' : ''; ?>">
            <label for="idkonsumen" class="col-sm-2 control-label"><?= lang('jm_konsumen') ?></label>
            <div class="col-sm-3">
                <select class="form-control" name="idkonsumen" id="idkonsumen">
                    <option></option>
                    <?php 
                        if($dt_konsumen):
                            foreach ($dt_konsumen as $key => $val):
                    ?>
                            <option value="<?= $val->id ?>"  <?= set_select('idkonsumen',$val->id, isset($idkonsumen) && $val->id == $idkonsumen ) ?> ><?= strtoupper($val->nm) ?></option>
                    <?php endforeach;endif;?>
                </select>
            </div>
        </div>
        <div class="form-group <?= form_error('nilai') ? ' has-error' : ''; ?>">
            <label for="nilai" class="col-sm-2 control-label"><?= lang('jm_nilai') ?></label>
            <div class="col-sm-3">
                <div class="input-group">
	                <span class="input-group-addon">Rp</span>
	                <input type="text" class="form-control" name="nilai" value="<?= set_value('nilai') ?>">
	            </div>
            </div>
        </div>
        <div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
            <label for="ket" class="col-sm-2 control-label"><?= lang('jm_ket') ?></label>
            <div class="col-sm-4">
                <textarea class="form-control" id="ket" name="ket" maxlength="255"><?= set_value('ket'); ?></textarea>
            </div>
        </div>
        <div class="form-group <?= form_error('waktu') ? ' has-error' : ''; ?>">
            <label for="waktu" class="col-sm-2 control-label"><?= lang('jm_tanggal') ?></label>
            <div class="col-sm-3">
               	<input type="text" class="form-control" id="waktu" name="waktu" value="<?= set_value('waktu', date('d/m/Y H:i:s')) ?>">
            </div>
        </div>
        <div class="form-group <?= form_error('metode_bayar') ? ' has-error' : ''; ?>">
            <label for="metode_bayar" class="col-sm-2 control-label"><?= lang('jm_metode_bayar') ?></label>
            <div class="col-sm-3">
                <select class="form-control" name="metode_bayar" id="metode_bayar">
                    <option value="">--<?= lang('jm_pilih') ?>--</option>
                    <option value="1" <?= set_select('metode_bayar', 1) ?>><?= lang('jm_cash') ?></option>
                    <option value="2" <?= set_select('metode_bayar', 2) ?>><?= lang('jm_transfer') ?></option>
                </select>
            </div>
        </div>
        <div class="transfer form-group <?= form_error('rekening_pengirim') ? ' has-error' : ''; ?>">
            <label for="rekening_pengirim" class="col-sm-2 control-label"><?= lang('jm_no_rek') ?></label>
            <div class="col-sm-3">
                <input type="text" class="form-control" id="rekening_pengirim" name="rekening_pengirim" value="<?= set_value('rekening_pengirim') ?>">
            </div>
        </div>
        <div class="transfer form-group">
            <label for="file_resi" class="col-sm-2 control-label"><?= lang('jm_upload_resi') ?></label>
            <div class="col-sm-3">
                <input type="file" id="file_resi" name="file_resi" value="" accept="image/png, image/jpeg, image/jpg">
                <div id="imgcontainer" style="width:70px;height:70px;border:1px solid #000000;text-align:center;float:left;margin-top:10px;">
                    <img id="img-resi" src="#" alt="Bukti Transfer" width="68px" height="68px">
                </div>
                <div class="clearfix"></div>
                <span class="help-block text-red">Ext : jpeg, jpg, png</span>
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" name="save" id="save" class="btn btn-primary"><?= lang('jm_btn_save') ?></button>
                <?php
                echo lang('jm_or') . ' ' . anchor('jaminan', lang('jm_btn_cancel'));
                ?>
            </div>
        </div>
    </div>
    <?= form_close() ?>
</div>