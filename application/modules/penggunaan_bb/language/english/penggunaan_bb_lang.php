<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['penggunaan_bb_title_manage'] = 'Data Penggunaan Bahan Baku';
$lang['penggunaan_bb_title_view'] 	= 'View Detail Penggunaan Bahan Baku';
$lang['penggunaan_bb_title_create']	= 'Input Penggunaan Bahan Bahan Baku';

// form/table
$lang['penggunaan_bb_no'] 			= 'NO';
$lang['penggunaan_bb_id_penggunaan_bb']		= 'Kode penggunaan bahan baku';
$lang['penggunaan_bb_id_penggunaan_bb']	= 'Kode penggunaan bahan baku';
$lang['penggunaan_bb_nama_konsumen']	= 'Nama Konsumen';
$lang['penggunaan_bb_nama_file'] 	= 'Nama File';
$lang['penggunaan_bb_tgl_penggunaan']	= 'Tanggal Penggunaan';
$lang['penggunaan_bb_qty']		= 'Qty';
$lang['penggunaan_bb_act']		= 'Action';
$lang['penggunaan_bb_pilih_brg']	= 'Pilih Barang';
$lang['penggunaan_bb_barcode']			= 'Barcode';
$lang['penggunaan_bb_nm_barang']			= 'Nama Bahan Baku';
$lang['penggunaan_bb_konversi_satuan'] 	= 'Konversi Satuan';
$lang['penggunaan_bb_satuan']			= 'Satuan';
$lang['penggunaan_bb_scan_brg']			= 'Ketik / Scan Barcode Barang';
$lang['penggunaan_bb_nm_merk']			= 'Merek Barang';
$lang['penggunaan_bb_text_file']		= 'File Text';
$lang['penggunaan_bb_nm_mesin']         = 'Nama Mesin';
$lang['penggunaan_bb_bw_color']          = 'BW Color Counter';
$lang['penggunaan_bb_full_color']       = 'Full Color Counter';


$lang['penggunaan_bb_ket']					= 'Keterangan';
$lang['penggunaan_bb_nm_worksheet']	 = 'Nama Worksheet';
$lang['penggunaan_bb_created_by']	 = 'Dibuat Oleh';




$lang['penggunaan_bb_search']		= 'Keterangan';

$lang['penggunaan_bb_btn_save'] 	= 'Simpan';
$lang['penggunaan_bb_pilih_btn']	= 'Pilih';
$lang['penggunaan_bb_btn_cancel'] 	= 'Kembali';
$lang['penggunaan_bb_btn_delete']	= 'Hapus';
$lang['penggunaan_bb_btn_new']		= 'Baru';
$lang['penggunaan_bb_or']			= ' Atau ';
$lang['penggunaan_bb_cari_btn']     = 'Cari';
$lang['penggunaan_bb_cari_brg']     = 'Ketikan Nama Bahan Baku';

// messages
$lang['penggunaan_bb_del_error']		= 'Anda belum memilih data penggunaan bahan baku produksi yang akan dihapus.';
$lang['penggunaan_bb_del_failure']		= 'Tidak dapat menghapus penggunaan baha baku produksi: ';
$lang['penggunaan_bb_delete_confirm']	= 'Apakah anda yakin akan menghapus penggunaan bahan baku produksi terpilih ?';
$lang['penggunaan_bb_cancle_penggunaan_bb_success']= 'Cancel penggunaan bahan baku produksi  Berhasil';
$lang['penggunaan_bb_cancle_penggunaan_bb_failure']= 'Cancel penggunaan bahan baku produksi  Gagal';
$lang['penggunaan_bb_deleted']			= 'penggunaan_bb berhasil dihapus';
$lang['penggunaan_bb_no_records_found'] 	= 'Data tidak ditemukan.';
$lang['penggunaan_bb_no_item']			= 'Anda belum mengisi bahan baku penggunaan bahan baku';
$lang['penggunaan_bb_bw_color_minus']     = 'Counter BW Color kurang dari data sebelumnya';
$lang['penggunaan_bb_full_color_minus']     = 'Counter Full Color kurang dari data sebelumnya';

$lang['penggunaan_bb_create_failure'] 	= 'penggunaan baha baku produksi gagal disimpan';
$lang['penggunaan_bb_create_success'] 	= 'penggunaan baha baku produksi berhasil disimpan';

$lang['penggunaan_bb_edit_success'] 		= 'Penggunaan bahan baku produksi berhasil disimpan';
$lang['penggunaan_bb_invalid_id'] 		= 'ID Tidak Valid';