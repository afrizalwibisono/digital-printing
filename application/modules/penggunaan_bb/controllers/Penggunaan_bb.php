<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for penggunaan_bb
 */

class Penggunaan_bb extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission       = "Penggunaan Bahan Baku.View";
    protected $addPermission        = "Penggunaan Bahan Baku.Add";
    protected $managePermission     = "Penggunaan Bahan Baku.Manage";
    protected $deletePermission     = "Penggunaan Bahan Baku.Delete";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('penggunaan_bb/penggunaan_bb');
        $this->load->model(['penggunaan_bb_model','penggunaan_bb_detail_model',
                            'produk_detail_penyusun_model','bahan_baku_model',
                            'konversi_satuan/konversi_satuan_model',
                            'bom/produk_model',
                            'bahan_baku/bahan_baku_model',
                            'mesin_cetak/mesin_model',
                            'worksheet/worksheet_model']);

        $this->template->title(lang('penggunaan_bb_title_manage'));
		$this->template->page_icon('fa fa-address-card-o');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);
        $idworksheet = $this->uri->segment(3);
        if($idworksheet==1){
            $a3=true;
        }else{
            $a3= false;
        }
        if (isset($_POST['save']))
        {
            if ($this->save_penggunaan_bb())
            {
              $this->template->set_message(lang("penggunaan_bb_create_success"), 'success');
              redirect('penggunaan_bb/index/'.$idworksheet);
            }
        }
        if ($idworksheet){
            if (isset($_POST['delete']) && has_permission($this->deletePermission))
            {
                $checked = $this->input->post('checked');

                if (is_array($checked) && count($checked))
                {
                    $result = FALSE;
                    foreach ($checked as $pid)
                    {
                        $del = $this->penggunaan_bb_model->delete($pid);
                        if($del)
                            {
                                
                                $keterangan = "SUKSES, hapus data penggunaan_bb : ".$pid;
                                $status     = 1;
                            }
                            else
                            {
                                $keterangan = "GAGAL, hapus data penggunaan_bb : ".$pid;
                                $status     = 0;
                            } 

                        $nm_hak_akses   = $this->deletePermission; 
                        $kode_universal = $pid;
                        $jumlah         = 1;
                        $sql            = $this->db->last_query();

                        $hasil = simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                    }

                    if ($del)
                    {
                        $this->template->set_message(count($checked) .' '. lang('penggunaan_bb_deleted') .'.', 'success');
                    }
                    else
                    {
                        $this->template->set_message(lang('penggunaan_bb_del_failure'), 'error');
                    }
                }
                else
                {
                    $this->template->set_message(lang('penggunaan_bb_del_error'), 'error');
                }

                unset($_POST['delete']);
            }//end if
            // Pagination
            $this->load->library('pagination');

            if(isset($_POST['table_search']))
            {
                $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
                $st_selesai = $this->input->post('st_selesai');
                $tgl_awal  = $this->input->post('tgl_awal');
                $tgl_akhir = $this->input->post('tgl_akhir');
            }
            else
            {
                $search = isset($_GET['search'])?$this->input->get('search'):'';
                $st_selesai = $this->input->get('st_selesai');
                $tgl_awal  = $this->input->get('tgl_awal');
                $tgl_akhir = $this->input->get('tgl_akhir');
            }

            $filter = "?search=".urlencode($search);
            $search2 = $this->db->escape_str($search);

            $add_where = "";
            if($tgl_awal !='' && $tgl_akhir !='')
            {
                $filter    .= "&tgl_awal=".urlencode($tgl_awal)."&tgl_akhir=".urlencode($tgl_akhir);
                $add_where .= " AND (tgl_penggunaan >='".date_ymd($tgl_awal)."' AND tgl_penggunaan <='".date_ymd($tgl_akhir)."')";
            }else{
                $tgl_awal = date('d/m/Y');
                $tgl_akhir = date('d/m/Y');
                
                $filter   .= "&tgl_awal=".$tgl_awal."&tgl_akhir=".$tgl_akhir;
                $add_where .= " AND (tgl_penggunaan >='".date_ymd($tgl_awal)."' AND tgl_penggunaan <='".date_ymd($tgl_akhir)."')";
            }
            $user= $this->auth->userdata();
            $id_user = $user->id_user;
            
            $where = "`penggunaan_bb.deleted` = 0 $add_where 
                        AND penggunaan_bb.id_worksheet= $idworksheet
                        AND penggunaan_bb.ket LIKE '%$search2%' ESCAPE '!'";
            
            $total = $this->penggunaan_bb_model
                    ->join("users","users.id_user=penggunaan_bb.created_by","left")
                    ->join("worksheet","worksheet.id_worksheet=penggunaan_bb.id_worksheet","left")
                    ->join("mesin_cetak","penggunaan_bb.id_mesin=mesin_cetak.id_mesin","left")
                    ->where($where)
                    ->count_all();

            $offset = $this->input->get('per_page');

            $limit = $this->config->item('list_limit');
            // $limit = 3;

            $this->pager['base_url']            = current_url().$filter;
            $this->pager['total_rows']          = $total;
            $this->pager['per_page']            = $limit;
            $this->pager['page_query_string']   = TRUE;

            $this->pagination->initialize($this->pager);

            $data = $this->penggunaan_bb_model->select("penggunaan_bb.*,users.nm_lengkap,users.id_user, worksheet.nm_worksheet,nm_mesin")
                    ->join("users","users.id_user=penggunaan_bb.created_by","left")
                    ->join("worksheet","worksheet.id_worksheet=penggunaan_bb.id_worksheet","left")
                    ->join("mesin_cetak","penggunaan_bb.id_mesin=mesin_cetak.id_mesin","left")
                    ->where($where)
                    ->order_by('penggunaan_bb.created_on','ASC')
                    ->limit($limit, $offset)
                    ->find_all();

            $assets = array(
                            'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                            'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                            'plugins/number/jquery.number.js',
                            'plugins/select2/dist/css/select2.min.css',
                            'plugins/select2/dist/js/select2.min.js',
                            'penggunaan_bb/assets/js/penggunaan_bb.js'
                            );

            add_assets($assets);
            $data_worksheet = $this->worksheet_model->find($idworksheet);
            $nm_worksheet   = $data_worksheet->nm_worksheet;


            $this->template->set('results', $data);
            $this->template->set('idworksheet', $idworksheet);
            $this->template->set('tgl_awal', $tgl_awal);
            $this->template->set('tgl_akhir', $tgl_akhir);
            $this->template->set('search', $search);
            $this->template->set('a3', $a3);
            $this->template->set('id_user', $id_user);
            $this->template->set("numb", $offset+1);
            $this->template->title(lang('penggunaan_bb_title_manage')." ". $nm_worksheet);
            $this->template->render('index'); 
        }else{
            show_404();
        }
    }
   	public function create()
   	{
   		
  		$this->auth->restrict($this->managePermission);
                
        // $id = $this->uri->segment(3);
        $idworksheet = $this->uri->segment(3);

        if (empty($idworksheet))
        {
            $this->template->set_message(lang("penggunaan_bb_invalid_id"), 'error');
            redirect('penggunaan_bb/index/'.$idworksheet);
        }

        if (isset($_POST['save']))
        {
            if ($this->save_penggunaan_bb())
            {
                $this->template->set_message(lang("penggunaan_bb_create_success"), 'success');
                redirect('penggunaan_bb/index/'.$idworksheet);
            }

        }
        
        $assets = array(
                'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                'plugins/select2/dist/css/select2.min.css',
                'plugins/select2/dist/js/select2.min.js',
                'plugins/number/jquery.number.js',
                'penggunaan_bb/assets/js/penggunaan_bb.js',
                );

        add_assets($assets);
        $data_worksheet = $this->worksheet_model->find($idworksheet);
        $nm_worksheet   = $data_worksheet->nm_worksheet;
        $data_mesin = $this->mesin_model->find_all_by(['deleted' => 0,'idkategori'=>$idworksheet]);

        $this->template->title(lang('penggunaan_bb_title_create')." ". $nm_worksheet);
        $this->template->set('idworksheet', $idworksheet);
        $this->template->set('data_mesin', $data_mesin);
        $this->template->render('form');
        
   	}
    public function view()
    {
        
        $this->auth->restrict($this->managePermission);
                
        $id = $this->uri->segment(3);
        $idworksheet = $this->uri->segment(4);

        if (empty($id))
        {
            $this->template->set_message(lang("penggunaan_bb_invalid_id"), 'error');
            redirect('penggunaan_bb/index/'.$idworksheet);
        }

        if (isset($_POST['save']))
        {
            if ($this->save_penggunaan_bb())
            {
                $this->template->set_message(lang("penggunaan_bb_create_success"), 'success');
                redirect('penggunaan_bb/index/'.$idworksheet);
            }

        }
        // $estimasi_bb= array();
        // $penggunaan_bb = $this->penggunaan_bb_model->find($id);
        $data_penggunaan_bb = $this->penggunaan_bb_model->find_by(["idpenggunaan_bb"=>$id,"deleted"=>0]);
        $detail_penggunaan_bb  = $this->penggunaan_bb_model
                        ->select(["penggunaan_bb.*","barang.*","penggunaan_bb_detail.qty","penggunaan_bb_detail.real_qty","konversi_satuan.satuan_besar"])
                        ->join("penggunaan_bb_detail","penggunaan_bb.idpenggunaan_bb=penggunaan_bb_detail.idpenggunaan_bb","left")
                        ->join("barang","barang.idbarang_bb=penggunaan_bb_detail.idbarang_bb","left")
                        ->join("konversi_satuan","konversi_satuan.id_konversi = penggunaan_bb_detail.id_konversi","left")
                        ->where("penggunaan_bb.idpenggunaan_bb = '$id' AND penggunaan_bb.deleted=0")
                        ->order_by("nm_barang","ASC")
                        ->find_all();
        foreach ($detail_penggunaan_bb as $key => $record) {
            if ($record->st_cetak_a3 ==1) {
                $detail_penggunaan_bb[$key]->qty = $record->real_qty;
            }
        }
        
        $assets = array(
                'plugins/select2/dist/css/select2.min.css',
                'plugins/select2/dist/js/select2.min.js',
                'plugins/number/jquery.number.js',
                // 'penggunaan_bb/assets/js/penggunaan_bb.js',
                );

        add_assets($assets);
        $this->template->set('data_penggunaan_bb', $data_penggunaan_bb);
        $this->template->set('detail_penggunaan_bb', $detail_penggunaan_bb);
        $data_worksheet = $this->worksheet_model->find($idworksheet);
        $nm_worksheet   = $data_worksheet->nm_worksheet;

        $this->template->title(lang('penggunaan_bb_title_view')." ". $nm_worksheet);
        $this->template->render('view');
        
    }
    public function cancel_penggunaan_bb()
    {
        
        $this->auth->restrict($this->managePermission);
                
        $id = $this->uri->segment(3);
        $idworksheet = $this->uri->segment(4);

        if (empty($id))
        {
            $this->template->set_message(lang("penggunaan_bb_invalid_id"), 'error');
            redirect('penggunaan_bb/index/'.$idworksheet);
        }

        $data_tmp = $this->tmp_penggunaan_bb_model->find_by(["idpenggunaan_bb"=> $id, "deleted"=>0]);
        $query ="";
        if ($data_tmp) {
            // hapus data tmp penggunaan_bb
            $idtmp= $data_tmp->idtmp_penggunaan_bb;
            $del_tmp = $this->tmp_penggunaan_bb_model->delete($idtmp);
            $query .= $this->db->last_query();

            if ($del_tmp) {
                // update satus penggunaan_bb menjadi belum diambil;
                $update = $this->penggunaan_bb_model->update($id,["st_ambil"=>0,"waktu_ambil"=>NULL, "id_user"=>0]);
                $query .= "\n\n" .$this->db->last_query();

                if ($update) 
                {
                    $keterangan = "SUKSES, Update data penggunaan_bb dg id : ".$id;
                    $status     = 1;
                }
                else
                {
                    $keterangan = "GAGAL, Update data penggunaan_bb dg id : ".$id;
                    $status     = 0;
                }
                $nm_hak_akses   = $this->updatePermission; 
                $kode_universal = $id;
                $jumlah         = 1;
                $sql            = $query;

                $hasil = simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
            }

        }
        if ($update)
        {
            $this->template->set_message(lang("penggunaan_bb_cancle_penggunaan_bb_success"), 'success');
            redirect('penggunaan_bb/index/'.$idworksheet);
        }else{
            $this->template->set_message(lang("penggunaan_bb_cancle_penggunaan_bb_failure"), 'error');
            redirect('penggunaan_bb/index/'.$idworksheet);
        }
        
    }
    public function cari_barang()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            redirect('penggunaan_bb');
        }

        $cari = trim($this->db->escape_str($this->input->post('cr_barang')));

        $data = $this->bahan_baku_model->select(array("barang.*","satuan_terkecil.alias as satuan","nm_merk_bb"))
                                    ->join("satuan_terkecil","barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","left")
                                    ->join("merk_bb","barang.idmerk_bb=merk_bb.idmerk_bb","left")
                                    ->where("`barang.deleted` = 0
                                            AND (barcode like '%$cari%' OR nm_barang like '%$cari%') AND barang.st_fix = 0")
                                    ->order_by('nm_barang', 'ASC')
                                    ->find_all();
        if($data)
        {
            foreach ($data as $key => $dt) {
                if ($dt->st_potong_meteran ==1) {
                    $data[$key]->konversi = get_konversi(6,'',FALSE);
                }else{
                    $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
                }
            }
        }

        echo json_encode($data);
    }
    //Scan Barcode
    public function scan_barcode()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('kb_only_ajax'), 'error');
            redirect('kedatangan_barang');
        }

        $barcode = $this->input->post('barcode');

        if($barcode != "")
        {
            $data = $this->bahan_baku_model->select(array("barang.*","satuan_terkecil.alias as satuan"))
                                        ->join("satuan_terkecil","barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","left")
                                        ->find_all_by(array('barang.deleted' => 0, 'barcode' => $barcode));
            if($data)
            {
                foreach ($data as $key => $dt) {
                    $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
                }

                $return = array('type' => 'success', 'barcode' => $barcode, 'data' => $data);
            }
            else
            {
                $return = array('type' => 'error', 'barcode' => $barcode, 'data' => 0);
            }
        }
        else
        {
            $return = array('type' => 'error', 'barcode' => $barcode, 'data' => 0);
        }

        echo json_encode($return);
    }
    public function cancel()
    {
        $this->auth->restrict($this->addPermission);

        $this->session->unset_userdata('kedatangan');
        $this->template->set_message(lang('penggunaan_bb_canceled'), 'success');

        redirect('penggunaan_bb');
    }

   	protected function save_penggunaan_bb()
   	{
        if (isset($_POST['save']) && has_permission($this->addPermission))
        {
            $idpenggunaan_bb          = $this->input->post('idpenggunaan_bb');
            $id_mesin = $this->input->post('id_mesin');
            $ket            = $this->input->post('ket');
            $tgl_penggunaan = $this->input->post('tgl_penggunaan');
            $idworksheet    = $this->input->post('idworksheet');
            $text_file      = isset($_FILES['text_file'])?$_FILES['text_file']: null;
            $post_full_color = str_replace(",", "", $this->input->post('full_color'));
            $post_bw_color = str_replace(",", "", $this->input->post('bw_color'));

            $id_barang  = $this->input->post('id_barang');
            $qty        = $this->input->post('qty');
            $konversi   = $this->input->post('konversi');
            $cek_a3     = false;


            $this->form_validation->set_rules('ket','lang:penggunaan_bb_ket','trim');
            $this->form_validation->set_rules('id_mesin','lang:penggunaan_bb_id_mesin','trim');
            $where="";
            if($idworksheet==1){
                $where.= " `id_mesin` ='". $id_mesin."'";
            }

            $data_rekap = $this->penggunaan_bb_model
                                        ->order_by("created_on","ASC")
                                        ->where($where)
                                        ->find_all_by(['deleted'=>0, 'id_worksheet'=>$idworksheet]);
            if (isset($text_file) && $text_file['size'] > 0 ) {
                $return = $this->read_file($_FILES["text_file"]["tmp_name"], 'r');

                if ($return['fc'] !="" && $return['bw'] !="" && $return['tanggal'] !="") {
                    if (date_ymd($tgl_penggunaan) < date_ymd($return['tanggal'])) {
                        if (is_array($data_rekap)) {
                            $tgl_data="";
                            foreach ($data_rekap as $key => $record) {
                                $tgl_data = $record->tgl_penggunaan;
                            }
                            if ($tgl_data <= date_ymd($tgl_penggunaan)) {
                                $cek_a3 = true;       
                            }else{
                                $this->template->set_message("Tanggal Penggunaan bahan baku lebih kecil/sama dengan tanggal terakhir data dinputkan", 'error');
                                return false;   
                            }
                        }else{
                            $cek_a3= true;
                        }
                    }else{
                        $this->template->set_message("file date and data input date are not the same", 'error');
                        return false;  
                    }
                }else{
                    $this->template->set_message("please select the correct file", 'error');
                    return false;
                }
            }elseif($post_bw_color != '' && $post_full_color !=""){
                $cek_a3 = true;
                if(is_array($data_rekap)){
                    $tgl_data = date('Y/m/d', strtotime("-1 days"));
                    foreach ($data_rekap as $key => $record) {
                        $tgl_data = $record->tgl_penggunaan;
                    }
                    if ($tgl_data <= date_ymd($tgl_penggunaan)) {
                        $cek_a3 = true;       
                    }else{
                        $this->template->set_message("Tanggal Penggunaan bahan baku lebih kecil/sama dengan tanggal terakhir data dinputkan", 'error');
                        return false;   
                    }
                }
            }else{
                $cek_a3 = false;
                if(is_array($data_rekap)){
                    $tgl_data = date('Y/m/d', strtotime("-1 days"));
                    foreach ($data_rekap as $key => $record) {
                        $tgl_data = $record->tgl_penggunaan;
                    }
                    if ($tgl_data > date_ymd($tgl_penggunaan)) {
                        $this->template->set_message("Tanggal Penggunaan bahan baku lebih kecil/sama dengan tanggal terakhir data dinputkan", 'error');
                        return false;   
                    }
                    // if ($tgl_data <= date_ymd($tgl_penggunaan)) {
                    //     $cek_a3 = true;
                    // }else{
                    //     $this->template->set_message("Tanggal Penggunaan bahan baku lebih kecil/sama dengan tanggal terakhir data dinputkan", 'error');
                    //     return false;   
                    // }
                }
            }

            if ($this->form_validation->run() === FALSE) 
            {

                $this->template->set_message(validation_errors(), 'error');
                return FALSE;
            }

            //validasi
            if(count($id_barang) == 0)
            {
                $this->template->set_message(lang('penggunaan_bb_no_item'), 'error');
                return FALSE;
            }

            $ero_msg = array();
            $urut = 0;

            $data = $this->session->userdata('penggunaan_bb');
            //Start Transaction
            $this->db->trans_begin();

            //Simpan Ke table Kedatangan barang
            $data_penggunaan_bb = array(
                                    'idpenggunaan_bb'        => gen_primary('PROD'),
                                    'tgl_penggunaan'    => date_ymd($tgl_penggunaan),
                                    'ket'               => $ket, 
                                    'id_worksheet'      => $idworksheet,
                                    'id_mesin'          => $id_mesin, 
                                );

            $this->penggunaan_bb_model->insert($data_penggunaan_bb);
            $query = $this->db->last_query();

            //Insert Detail bb
            if ($cek_a3 == true) {

                $cetak_full_color = $this->bahan_baku_model->find_by(['deleted'=>0, 'id_mesin'=> $id_mesin, 'st_warna'=>1]);
                $cetak_bw_color = $this->bahan_baku_model->find_by(['deleted'=>0, 'id_mesin'=> $id_mesin, 'st_warna'=>0]);

                $data_full_color =  $this->penggunaan_bb_model
                                        ->join('penggunaan_bb_detail','penggunaan_bb.idpenggunaan_bb = penggunaan_bb_detail.idpenggunaan_bb','left')
                                        ->join('barang','barang.idbarang_bb=penggunaan_bb_detail.idbarang_bb','left')
                                        ->order_by('penggunaan_bb.created_on','ASC')
                                        ->find_all_by(['penggunaan_bb.deleted'=>0,'penggunaan_bb_detail.idbarang_bb'=>$cetak_full_color->idbarang_bb]);
                if ($data_full_color) {
                    foreach ($data_full_color as $key => $dt_full) {
                        $qty_awal = $dt_full->qty;
                        $hpp_full_color = $dt_full->hpp_manual;
                    }
                    
                    $jml_full_color = isset($return['fc']) ? ($return['fc'] - $qty_awal) : ($post_full_color - $qty_awal) ;
                    if($jml_full_color <0 ){
                        $this->template->set_message(lang('penggunaan_bb_full_color_minus'), 'error');
                        return FALSE;
                    }
                }else{
                    $hpp_full_color = 0;
                    $jml_full_color = 0;
                }
                $data_bw_color =  $this->penggunaan_bb_model
                                        ->join('penggunaan_bb_detail','penggunaan_bb.idpenggunaan_bb = penggunaan_bb_detail.idpenggunaan_bb','left')
                                        ->join('barang','barang.idbarang_bb=penggunaan_bb_detail.idbarang_bb','left')
                                        ->order_by('penggunaan_bb.created_on','ASC')
                                        ->find_all_by(['penggunaan_bb.deleted'=>0,'penggunaan_bb_detail.idbarang_bb'=> $cetak_bw_color->idbarang_bb]);
                if ($data_bw_color) {
                    foreach ($data_bw_color as $key => $dt_bw) {
                        $qty_awal = $dt_bw->qty;
                        $hpp_bw_color = $dt_bw->hpp_manual;
                    }
                    $jml_bw_color =  isset($return['bw']) ? ($return['bw'] - $qty_awal) : ($post_bw_color - $qty_awal);
                    if($jml_bw_color<0){
                        $this->template->set_message(lang('penggunaan_bb_bw_color_minus'), 'error');
                        return FALSE;
                    }
                }else{
                    $hpp_bw_color = 0;
                    $jml_bw_color = 0;   
                }


                $full_color[] =array('idpenggunaan_bb_detail' => gen_primary("DETPROD"),
                                    'idpenggunaan_bb'  => $data_penggunaan_bb['idpenggunaan_bb'],
                                    'idbarang_bb'  => $cetak_full_color->idbarang_bb,
                                    'qty'           => $post_full_color>0 ? $post_bw_color : $return['fc'],
                                    'id_konversi'   => 8,
                                    'real_qty'      => round($jml_full_color),
                                    'hpp_manual'    => $hpp_full_color,
                                    'total_hpp'     => ($jml_full_color * $hpp_full_color)/2);
                $bw_color[]   =array('idpenggunaan_bb_detail' => gen_primary("DETPROD"),
                                    'idpenggunaan_bb'  => $data_penggunaan_bb['idpenggunaan_bb'],
                                    'idbarang_bb'  => $cetak_bw_color->idbarang_bb,
                                    'qty'           => $post_bw_color>0 ? $post_full_color : $return['bw'],
                                    'id_konversi'   => 8,
                                    'real_qty'      => round($jml_bw_color),
                                    'hpp_manual'    => $hpp_bw_color,
                                    'total_hpp'     => ($jml_bw_color * $hpp_bw_color)/2);
                $add_array = array_merge($bw_color,$full_color);
            }
            foreach ($id_barang as $key => $br) {
                $dt_real_qty = hitung_ke_satuan_kecil($konversi[$key], floatval(remove_comma($qty[$key])));
                $real_qty  = $dt_real_qty['qty'];
                $hpp= get_hpp_bb_by_produksi([$br]);

                $data_det_bb[] = array('idpenggunaan_bb_detail' => gen_primary("DETPROD"),
                                        'idpenggunaan_bb'  => $data_penggunaan_bb['idpenggunaan_bb'],
                                        'idbarang_bb'  => $br,
                                        'qty'           => remove_comma($qty[$key]),
                                        'id_konversi'   => $konversi[$key],
                                        'real_qty'      => $real_qty,
                                        'hpp_manual'    => $hpp[0]->hpp_final,
                                        'total_hpp'     => $hpp[0]->hpp_final * $real_qty,
                                        );
            }
            if ($cek_a3== true) {
                $data_detail_bahan_baku_paki = array_merge($data_det_bb, $add_array);
            }else{
                $data_detail_bahan_baku_paki = $data_det_bb;
            }

            $this->penggunaan_bb_detail_model->insert_batch($data_detail_bahan_baku_paki);
            $query .= "\n\n".$this->db->last_query();

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();

                $keterangan = "GAGAL, tambah data kedatangan barang dengan ID ".$data['id_kedatangan'];
                $status     = 0;
                
                $return = FALSE;
            }
            else
            {
                $this->db->trans_commit();

                $keterangan = "SUKSES, tambah data kedatangan barang dengan ID : ".$data['id_kedatangan'];
                $status     = 1;
                
                $return = TRUE;
            }

            //Save Log
            $nm_hak_akses   = $this->addPermission; 
            $kode_universal = $data_penggunaan_bb['idpenggunaan_bb'];
            $jumlah         = 1;
            $sql            = $query;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

            if($return)
            {
                //Clear Session
                $this->session->unset_userdata('penggunaan_bb');
            }
            
            return $return;
        }
    }
    function read_file($filename){
        $fc = "";
        $bw = "";
        $tanggal="";
        $handle = @fopen($filename, "r");
        if ($handle) {
            while (($content = fgets($handle, 4096)) !== false) {
                $text = explode(" ",$content);

                if ($text[0] == "Full") {
                    if (count($text)== 47) {
                        $fc = floatval(end($text));
                    }
                }elseif ($text[0]=="B/W") {
                    $bw = floatval(end($text));
                }elseif ($text[0]== "Date/Time") {
                    $tanggal = $text[53];
                }
            }
            if (!feof($handle)) {
                echo "Error: unexpected fgets() fail\n";
            }
            fclose($handle);
        }
        $return = array('fc' => $fc,'bw' => $bw,'tanggal'=>$tanggal);
        return $return;
    }
    public function default_select($val)
    {
        return $val == "" ? FALSE : TRUE;
    }
}
?>