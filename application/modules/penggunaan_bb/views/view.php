<style type="text/css">
    .select2-container{ width: 100% !important; }
    .select2-container .select2-selection {
        font-size: 12px;
        max-height: 30px;
        padding-top: 3px;

        line-height: 1.33333;
    }
</style>

<?php
        $ENABLE_ADD     = has_permission('Produksi.Add'); 
?>
<div class="box box-primary">
    <?= form_open($this->uri->uri_string(),array('name'=>'frm_produksi','id'=>'frm_produksi','role'=>'form','class'=>'form-horizontal','enctype'=>'multipart/form-data')) ?>
    <div class="box-body">
        <div class="form-group">
            <label for="tgl_penggunaan" class="control-label col-sm-2"><?= lang('penggunaan_bb_tgl_penggunaan') ?></label>
            <div class="col-sm-2">
                <input type="text" name="tgl_penggunaan" id="tgl_penggunaan" class="form-control" value="<?= date('d M Y', strtotime($data_penggunaan_bb->tgl_penggunaan)) ?>" readonly>
                <input type="hidden" name="idworksheet" id="idworksheet" value="<?= $data_penggunaan_bb->id_worksheet ?>">
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered" id="tdet_bb" style="margin: 0px;padding: 0px;">
                <thead>
                    <tr>
                        <th width="50"><?= lang('penggunaan_bb_no') ?></th>
                        <th><?= lang('penggunaan_bb_barcode') ?></th>
                        <th><?= lang('penggunaan_bb_nm_barang') ?></th>
                        <th width="300"><?= lang('penggunaan_bb_qty') ?></th>
                        <th><?= lang('penggunaan_bb_satuan') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(isset($detail_penggunaan_bb)) : ?>
                        <?php 
                            $no = 1;
                            foreach($detail_penggunaan_bb as $key => $dt) : 
                        ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td>
                            <?= $dt->barcode ?>
                            <input type="hidden" name="id_barang[]" value="<?= $dt->idbarang_bb ?>" />
                        </td>
                        <td><?= $dt->nm_barang ?>
                        </td>
                        <td><?= number_format($dt->qty) ?></td>
                        <td>
                            <?= $dt->satuan_besar ?>
                        </td>
                    </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
        <div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
            <div class="col-sm-6">
                <label for="ket" class="control-label"><?= lang('penggunaan_bb_ket') ?></label>
                <textarea class="form-control" id="ket" name="ket" maxlength="255" disabled><?= $data_penggunaan_bb->ket ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <button type="submit" name="save" class="btn btn-primary"><?= lang('penggunaan_bb_btn_save') ?></button>
                <?php
                    echo " Atau " . ' ' . anchor('penggunaan_bb/index/'.$data_penggunaan_bb->id_worksheet, lang('penggunaan_bb_btn_cancel'));
                ?>
            </div>
        </div>
    </div>
    <?= form_close() ?>
    <!-- /.box-body -->
</div>
<!-- /.box -->
<script type="text/javascript" src="<?= base_url('assets/plugins/jquery-ui/jquery-ui.js') ?>"></script>