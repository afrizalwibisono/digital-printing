<?php 
    $ENABLE_ADD     = has_permission('Penggunaan Bahan Baku.Add');
    $ENABLE_MANAGE  = has_permission('Penggunaan Bahan Baku.Manage');
    $ENABLE_DELETE  = has_permission('Penggunaan Bahan Baku.Delete');
?>
<div class="box box-primary">
    <!--form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_produksi','name'=>'frm_produksi','role','class'=>'form-inline'))?>
    <div class="box-header">
        <?php if ($ENABLE_ADD): ?>
            <a href="<?= site_url('penggunaan_bb/create/'.$idworksheet) ?>" class="btn btn-success" title="<?= lang('penggunaan_bb_btn_new') ?>"><?= lang('penggunaan_bb_btn_new') ?></a>
            <?php endif;?>
        <div class="pull-right">
            <div class="form-group">
                <div class="input-group input-daterange">
                    <input type="text" name="tgl_awal" id="tgl_awal" class="form-control" readonly placeholder="<?= lang('penggunaan_bb_tgl_penggunaan') ?>" value = "<?= set_value('tgl_awal',isset($tgl_awal) ? $tgl_awal : '') ?>"  placeholder="Tanggal Awal Order">
                    <span class="input-group-addon">to</span>
                    <input type="text" name="tgl_akhir" id="tgl_akhir" class="form-control" readonly placeholder="<?= lang('penggunaan_bb_tgl_penggunaan') ?>" value = "<?= set_value('tgl_akhir',isset($tgl_akhir) ? $tgl_akhir : '') ?>"  placeholder="Tanggal Akhir Order">
                </div>
            </div>
            <div class="form-group">
                <input type="text" name="table_search" id="table_search" class="form-control" placeholder="<?= lang('penggunaan_bb_search') ?>" value="<?php echo isset($search) ? $search : ''; ?>">
            </div>
            <div class="form-group">
                <button class="btn btn-default" name="btn_cari" id="btn_cari">
                    <span class="fa fa-search"></span>
                </button>
            </div>
        </div>

    </div>
    <?php if($results) : ?>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="tdet_order" name="tdet_order">
                <thead>
                    <tr class="success">
                        <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
                        <th width="50">#</th>
                        <th style="vertical-align:middle; text-align: center;"><?= lang('penggunaan_bb_tgl_penggunaan') ?></th>
                        <th style="vertical-align:middle; text-align: center;"><?= lang('penggunaan_bb_nm_worksheet') ?></th>
                        <?php if($a3 == true):?>
                            <th style="vertical-align:middle; text-align: center;"><?= lang('penggunaan_bb_nm_mesin') ?></th>
                        <?php endif;?>
                        <th style="vertical-align:middle; text-align: center;"><?= lang('penggunaan_bb_created_by') ?></th>
                        <th style="vertical-align:middle; text-align: center;"><?= lang('penggunaan_bb_ket') ?></th>
                        <th style="vertical-align:middle;text-align: center;width: 70px; "><?= lang('penggunaan_bb_act') ?></th>
                    </tr>
                </thead>
                <tbody>
                            <?php 
                                foreach($results as $key => $record) :
                            ?>
                        <tr>
                            <td class="column-check">
                                <?php if ($record->id_user == $id_user):?>
                                <input type="checkbox" name="checked[]" value="<?= $record->idpenggunaan_bb ?>" />
                            <?php endif; ?>
                            </td>
                            <td><?= $numb++; ?></td>
                            <td><?= date("d M Y", strtotime($record->tgl_penggunaan)) ?></td>
                            <td><?= $record->nm_worksheet ?></td>
                            <?php if($a3==true):?>
                                    <td><?= $record->nm_mesin ?></td>
                            <?php endif;?>
                            <td><?= $record->nm_lengkap ?> </td>
                            <td><?= $record->ket?></td>
                            <td style="padding-right:20px;text-align: center;">
                                <?php if($ENABLE_MANAGE) : ?>
                                <a class="text-black" href="<?= site_url('penggunaan_bb/view/' . $record->idpenggunaan_bb .'/'.$idworksheet); ?>" data-toggle="tooltip" data-placement="left" title="View"><i class="fa fa-folder-open"></i></a>
                            <?php endif;?>
                            </td>
                        </tr>
                    <?php 
                        endforeach;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="box-footer clearfix">
        <?php if($ENABLE_DELETE) : ?>
        <input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('penggunaan_bb_btn_delete') ?>" onclick="return confirm('<?= (lang('penggunaan_bb_delete_confirm')); ?>')">
        <?php endif;
        echo $this->pagination->create_links(); 
        ?>
    </div>
    <?php else: ?>
     <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('penggunaan_bb_no_records_found') ?></p>
    </div>
    <?php
    endif;
    echo form_close() ?>
</div>
