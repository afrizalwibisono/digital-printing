<style type="text/css">
    .select2-container{ width: 100% !important; }
    .select2-container .select2-selection {
        font-size: 12px;
        max-height: 30px;
        padding-top: 3px;

        line-height: 1.33333;
    }
</style>

<?php
        $ENABLE_ADD     = has_permission('Produksi.Add'); 
?>
<div class="box box-primary">
    <?= form_open($this->uri->uri_string(),array('name'=>'frm_produksi','id'=>'frm_produksi','role'=>'form','class'=>'form-horizontal','enctype'=>'multipart/form-data')) ?>
    <div class="box-body">
        <?php
            if (isset($idworksheet) && $idworksheet== 1):
        ?>
            <div class="form-group">
                <label for="id_konsumen" class="control-label col-sm-2"><?= lang('penggunaan_bb_nm_mesin') ?></label>
                <div class="col-sm-2">
                    <select id="id_mesin" name="id_mesin" class="form-control" required>
                        <option></option>
                        <?php foreach ($data_mesin as $key => $record) : ?>
                        <!-- 1: by text -->
                        <!-- 0: manual -->
                        <option value="<?= $record->id_mesin; ?>" <?= set_select('id_mesin', $record->id_mesin, isset($data['id_mesin']) && $data['id_mesin'] == $record->id_mesin) ?> data-tipe ="<?= $record->st_rekap_cetak;?>"><?= $record->nm_mesin ?></option> 
                        <?php endforeach; ?>
                    </select>
                </div>
            </div> 
            <div id="txt-div">
                <div class="form-group">
                    <label for="text_file" class="control-label col-sm-2"><?= lang('penggunaan_bb_text_file') ?></label>
                    <div class="col-sm-2">
                        <input type="file" name="text_file" id="text_file" accept=".txt" required>
                    </div>
                </div>
            </div>
            <div id="manual-div">
                <div class="form-group">
                    <label for="bw_color" class="control-label col-sm-2"><?= lang('penggunaan_bb_bw_color') ?></label>
                    <div class="col-sm-2">
                        <input type="text" name="bw_color" id="bw_color" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="full_color" class="control-label col-sm-2"><?= lang('penggunaan_bb_full_color') ?></label>
                    <div class="col-sm-2">
                        <input type="text" name="full_color" id="full_color" required>
                    </div>
                </div>  
            </div>  
        
        <?php
            endif;
        ?>
        <div class="form-group">
            <label for="tgl_penggunaan" class="control-label col-sm-2"><?= lang('penggunaan_bb_tgl_penggunaan') ?></label>
            <div class="col-sm-2">
                <input type="text" name="tgl_penggunaan" id="tgl_penggunaan" class="form-control" readonly>
                <input type="hidden" name="idworksheet" id="idworksheet" value="<?= $idworksheet ?>">
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered" id="tdet_bb" style="margin: 0px;padding: 0px;">
                <thead>
                    <tr>
                        <th width="50"><?= lang('penggunaan_bb_no') ?></th>
                        <th><?= lang('penggunaan_bb_barcode') ?></th>
                        <th><?= lang('penggunaan_bb_nm_barang') ?></th>
                        <th width="300"><?= lang('penggunaan_bb_qty') ?></th>
                        <th><?= lang('penggunaan_bb_satuan') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(isset($data['items'])) : ?>
                        <?php 
                            $no = 1;
                            foreach($data['items'] as $key => $dt) : 
                        ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td>
                            <?= $dt['barcode'] ?>
                            <input type="hidden" name="id_barang[]" value="<?= $dt['id_barang'] ?>" />
                        </td>
                        <td><?= $dt['nm_barang'] ?>
                            <br><a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a>
                        </td>
                        <td><input type="number" min="1" name="qty[]" class="form-control" value="<?= $dt['qty'] ?>" /></td>
                        <td>
                            <select class='form-control' name='konversi[]' style="width: 150px;">
                                <?php 
                                    if($dt['konversi']) :
                                        foreach ($dt['konversi'] as $key => $kf) :
                                ?>
                                <option value="<?= $kf->id_konversi ?>" <?= (isset($kf->id_konversi) && $kf->id_konversi == $dt['id_konversi']) ? "selected" : "" ?>><?= $kf->satuan_besar." (".$kf->ket.")" ?></option>
                                <?php
                                        endforeach; 
                                    endif; 
                                ?>
                            </select>
                        </td>
                    </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6">
                            <div class="input-group" id="barcode_input">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-barang"><i class="fa fa-plus-square"></i></button>
                                </div>
                                <input type="text" class="form-control" id="barcode" name="barcode" placeholder="<?= lang('penggunaan_bb_scan_brg') ?>" autocomplete="off" autofocus />
                            </div>
                            <span class="help-block text-green" id="barcode_msg"></span>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
            <div class="col-sm-6">
                <label for="ket" class="control-label"><?= lang('penggunaan_bb_ket') ?></label>
                <textarea class="form-control" id="ket" name="ket" maxlength="255"><?php echo set_value('ket', isset($data['ket']) ? $data['ket'] : ''); ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <button type="submit" name="save" class="btn btn-primary"><?= lang('penggunaan_bb_btn_save') ?></button>
                <?php
                    echo " Atau " . ' ' . anchor('penggunaan_bb/index/'.$idworksheet, lang('penggunaan_bb_btn_cancel'));
                ?>
            </div>
        </div>
    </div>
    <?= form_close() ?>
    <!-- /.box-body -->
</div>
<!-- /.box -->
<div class="modal" id="modal-barang" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= form_open(site_url('produksi/add_item'),array('id'=>'frm_barang','name'=>'frm_barang','role'=>'form')) ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= lang('penggunaan_bb_pilih_brg') ?></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" id="cr_barang" name="cr_barang" placeholder="<?= lang('penggunaan_bb_cari_brg') ?>" />
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-primary" name="cari_btn" id="cari_btn"><i class="fa fa-search"></i> <?= lang('penggunaan_bb_cari_btn') ?></button>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="tbl_barang" class="table table-hover table-striped">
                            <thead>
                                <tr class="success">
                                    <th><input type="checkbox" id="ck_all" name="ck_all" /></th>
                                    <th><?= lang('penggunaan_bb_no') ?></th>
                                    <th><?= lang('penggunaan_bb_barcode') ?></th>
                                    <th><?= lang('penggunaan_bb_nm_barang') ?></th>
                                    <th><?= lang('penggunaan_bb_nm_merk') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="pilih_btn" name="pilih_btn"><?= lang('penggunaan_bb_pilih_btn') ?></button>
                </div>
            <?= form_close() ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" src="<?= base_url('assets/plugins/jquery-ui/jquery-ui.js') ?>"></script>