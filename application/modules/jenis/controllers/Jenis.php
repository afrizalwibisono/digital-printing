<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 * 
 * This is controller for Merk

 */

class Jenis extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewjenis   = "Jenis.View";
    protected $addjenis    = "Jenis.Add";
    protected $managejenis = "Jenis.Manage";
    protected $deletejenis = "Jenis.Delete";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewjenis);

        $this->lang->load('jenis/jenis');
        $this->load->model(array(
                                'Jenis_model'
                            ));

        $this->template->title(lang('jenis_title_manage'));
		$this->template->page_icon('fa fa-table');
    }

    public function index()
    {
        $this->auth->restrict($this->viewjenis);

        if (isset($_POST['delete']) && has_permission($this->deletejenis))
        {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                foreach ($checked as $pid)
                {
                    $result = $this->Jenis_model->delete($pid);
                }

                if ($result)
                {
                    $this->template->set_message(count($checked) .' '. lang('jenis_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('jenis_del_failure') . $this->Jenis_model->error, 'error');
                }
            }
            else
            {
                $this->template->set_message(lang('jenis_del_error') . $this->Jenis_model->error, 'error');
            }
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
        }

        $filter = "";
        if($search!="")
        {
            $filter = "?search=".$search;
        }

        $search2 = $this->db->escape_str($search);
        
        $where = "deleted = 0
                AND (`nmjenis_bb` LIKE '%$search2%' ESCAPE '!'
                OR `ket` LIKE '%$search2%' ESCAPE '!')";
                
        $total = $this->Jenis_model
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->Jenis_model
                    ->where($where)
                    ->order_by('nmjenis_bb','ASC')
                    ->limit($limit, $offset)->find_all();

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set("toolbar_title", lang('jenis_title_manage'));
        $this->template->title(lang('jenis_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

    public function create()
    {

        $this->auth->restrict($this->addjenis);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_jenis())
            {
              $this->template->set_message(lang("jenis_create_success"), 'success');
              redirect('jenis');
            }
        }

        $this->template->set("page_title", lang('jenis_title_new'));
        $this->template->render('jenis_form');
    }

    protected function save_jenis($type='insert', $id=0)
    {

        $this->form_validation->set_rules('nmjenis_bb','lang:jenis_name','required|trim|max_length[255]');
        $this->form_validation->set_rules('ket','lang:jenis_ket','trim');

        if ($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        unset($_POST['submit'], $_POST['save']);

        if ($type == 'insert')
        {
            $id = $this->Jenis_model->insert($_POST);

            if (is_numeric($id))
            {
                $return = TRUE;
            }
            else
            {
                $return = FALSE;
            }
        }
        else if ($type == 'update')
        {
            $return = $this->Jenis_model->update($id, $_POST);
        }

        return $return;
    }

    public function edit()
    {
        
        $this->auth->restrict($this->managejenis);
                
        $id = (int)$this->uri->segment(4);

        if (empty($id))
        {
            $this->template->set_message(lang("jenis_invalid_id"), 'error');
            redirect('jenis');
        }

        if (isset($_POST['save']))
        {
            if ($this->save_jenis('update', $id))
            {
                $this->template->set_message(lang("jenis_edit_success"), 'success');
            }

        }

        $data  = $this->Jenis_model->find_by(array('idjenis_bb' => $id));

        if(!$data)
        {
            $this->template->set_message(lang("jenis_invalid_id"), 'error');
            redirect('jenis');
        }
        
        $this->template->set('data', $data);
        $this->template->set('toolbar_title', lang("jenis_title_edit"));
        $this->template->set('page_title', lang("jenis_title_edit"));
        $this->template->render('jenis_form');
    }

}
?>