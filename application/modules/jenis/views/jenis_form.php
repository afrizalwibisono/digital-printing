<div class="box box-primary">
    <!--form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_jenis','name'=>'frm_jenis','role','class'=>'form-horizontal'))?>
    <div class="box-body">
        <div class="form-group <?= form_error('nmjenis_bb') ? ' has-error' : ''; ?>">
                <label for="nmjenis_bb" class="col-sm-2 control-label"><?= lang('jenis_name') ?></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="nmjenis_bb" name="nmjenis_bb" maxlength="255" value="<?php echo set_value('nmjenis_bb', isset($data->nmjenis_bb) ? $data->nmjenis_bb : ''); ?>" required autofocus>
                </div>
            </div>
            <div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
                <label for="ket" class="col-sm-2 control-label"><?= lang('jenis_ket') ?></label>
                <div class="col-sm-4">
                    <textarea class="form-control" id="ket" name="ket"><?php echo set_value('ket', isset($data->ket) ? $data->ket : ''); ?></textarea>
                </div>
            </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" name="save" class="btn btn-primary"><?= lang('jenis_btn_save') ?></button>
                <?php
                echo lang('bf_or') . ' ' . anchor('jenis', lang('jenis_btn_cancel'));
                ?>
            </div>
        </div>
    </div>
    <?= form_close() ?>
</div>