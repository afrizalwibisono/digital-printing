<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ============================
$lang['jenis_title_manage']			= 'Data Jenis';
$lang['jenis_title_new']			= 'Data Jenis Baru';
$lang['jenis_title_edit']			= 'Edit Data jenis';

// from/table
$lang['jenis_name']					= 'Nama jenis';
$lang['jenis_ket']					= 'Keterangan';


//button
$lang['jenis_btn_new']				= 'Baru';
$lang['jenis_btn_delete']			= 'Hapus';
$lang['jenis_btn_save']				= 'Simpan';
$lang['jenis_btn_cancel']			= 'Batal';

//messages
$lang['jenis_del_error']			= 'Anda belum memilih data jenis yang akan dihapus.';
$lang['jenis_del_failure']			= 'Tidak dapat menghapus data jenis';
$lang['jenis_delete_confirm']		= 'Apakah anda yakin akan menghapus data jenis terpilih?';
$lang['jenis_deleted']				= 'Data jenis berhasil dihapus';
$lang['jenis_no_records_found']		= 'Data tidak ditemukan.';

$lang['jenis_create_failure']		= 'Data jenis gagal disimpan';
$lang['jenis_create_success']		= 'Data jenis berhasil disimpan';

$lang['jenis_edit_success']			= 'Data jenis berhasil disimpan';
$lang['jenis_invalid_id']			= 'ID tidak Valid';

