<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 * 
 * This is controller for Merk

 */

class Kategori extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewKategori   = "Kategori.View";
    protected $addKategori    = "Kategori.Add";
    protected $manageKategori = "Kategori.Manage";
    protected $deleteKategori = "Kategori.Delete";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewKategori);

        $this->lang->load('kategori/kategori');
        $this->load->model(array(
                                'kategori_model',
                                'konversi_model'
                            ));

        $this->template->title(lang('kategori_title_manage'));
		$this->template->page_icon('fa fa-table');
    }

    public function index()
    {
        $this->auth->restrict($this->viewKategori);

        if (isset($_POST['delete']) && has_permission($this->deleteKategori)){
            
            if($this->delete()){

                $this->template->set_message(lang("kategori_deleted"),"success");
                redirect("kategori");

            }

        }

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
        }

        $filter = "";
        if($search!="")
        {
            $filter = "?search=".$search;
        }

        $search2 = $this->db->escape_str($search);
        
        $where = "deleted = 0
                AND (`nmkategori` LIKE '%$search2%' ESCAPE '!'
                OR `ket` LIKE '%$search2%' ESCAPE '!')";
                
        $total = $this->kategori_model
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->kategori_model
                    ->where($where)
                    ->order_by('nmkategori','ASC')
                    ->limit($limit, $offset)->find_all();

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set("toolbar_title", lang('kategori_title_manage'));
        $this->template->title(lang('kategori_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

    function delete(){

        if(!isset($_POST['checked'])){

            $this->template->set_message(lang("kategori_del_error"),"error");
            return false;

        }

        $id     = $this->input->post("checked");

        $sql    = "";

        $this->db->trans_start();

            foreach ($id as $key => $isi) {
                
                $this->kategori_model->delete($isi);
                $sql .= $this->db->last_query."\n\n";

            }

        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            $return         = false;
            $keterangan     = "Gagal hapus data kategori";
            $status         = 0;

        }else{

            $return         = true;
            $keterangan     = "Sukses hapus data kategori";
            $status         = 0;

        }

        $nm_hak_akses   = $this->deleteKategori; 
        $kode_universal = "-";
        $jumlah         = 0;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        return $return;

    }

    public function create()
    {

        $this->auth->restrict($this->addKategori);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_kategori())
            {
              $this->template->set_message(lang("kategori_create_success"), 'success');
              redirect('kategori');
            }
        }

        $data_satuan    = get_konversi_by_tipe(1,null);

        $asset  = array(
                            "plugins/select2/js/select2.js",
                            "plugins/select2/css/select2.css",
                            "plugins/number/jquery.number.js",
                            "kategori/assets/js/form_kategori.js"
                        );

        add_assets($asset);

        $this->template->set("satuan",$data_satuan);
        $this->template->set("page_title", lang('kategori_title_new'));
        $this->template->render('kategori_form');
    }

    public function cek_pil_dropdown($val){

        return $val == "" ? false : true;

    }

    protected function save_kategori($type='0', $id=0) // 0 = baru, 1 = edit
    {

        $this->form_validation->set_rules('nmkategori','lang:kategori_name','required|trim|max_length[255]');
        $this->form_validation->set_rules('ket','lang:kategori_ket','trim');

        if(str_replace(",", "",$_POST['dimensi_p']) > 0 || str_replace(",", "",$_POST['dimensi_l']) > 0 || strlen($_POST['satuan']) > 0 ){

            $this->form_validation->set_rules('dimensi_l','lang:kategori_l_dimensi','required');
            $this->form_validation->set_rules('dimensi_p','lang:kategori_p_dimensi','required');
            $this->form_validation->set_rules('satuan','lang:kategori_satuan','callback_cek_pil_dropdown');

        }

        $this->form_validation->set_message("cek_pil_dropdown", "You should choose the {field} field");

        if ($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        $nm_kategori    = $this->input->post('nmkategori');
        $ket            = $this->input->post('ket');
        $dimensi_p      = str_replace(",", "", $this->input->post('dimensi_p'));
        $dimensi_l      = str_replace(",", "", $this->input->post('dimensi_l'));
        $satuan         = $this->input->post('satuan') == "" ? 0 : $this->input->post('satuan') ;

        //ambil nilai konversi satuan terkecilnya
        $jml_asli_p     = 0;
        $jml_asli_l     = 0;

        if($satuan != "" && $dimensi_l > 0 && $dimensi_p > 0){

            $data       = $this->konversi_model->select('jml_kecil')->find($satuan);
            $jml_kecil  = $data->jml_kecil;

            $jml_asli_p = $dimensi_p * $jml_asli_p;
            $jml_asli_l = $dimensi_l * $jml_kecil;

        }

        $st_ukuran      = $jml_asli_l == 0 || $jml_asli_p == 0 ? 0 : 1;
        
        if($type == 0){

            $arr_save       = array(
                                        'nmkategori'    => $nm_kategori, 
                                        'ket'           => $ket, 
                                        'p'             => $jml_asli_p, 
                                        'l'             => $jml_asli_l, 
                                        'st_ukuran'     => $st_ukuran, 
                                        'p_tampil'      => $dimensi_p, 
                                        'l_tampil'      => $dimensi_l, 
                                        'id_konversi'   => $satuan
                                    );


            $sql    = "";

            $this->db->trans_start();

                $this->kategori_model->insert($arr_save);
                $sql    = $this->db->last_query();

            $this->db->trans_complete();

            if($this->db->trans_status() == false){

                $return         = false;
                $keterangan     = "Gagal simpan data kategori";
                $status         = 0;

            }else{

                $return         = true;
                $keterangan     = "SUKSES simpan data kategori";
                $status         = 0;

            }

            $nm_hak_akses   = $this->addPermission; 
            $kode_universal = "-";
            $jumlah         = 0;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        }else{ //edit



        }


        return $return;
    }


}
?>