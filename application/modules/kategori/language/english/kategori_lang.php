<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ============================
$lang['kategori_title_manage']			= 'Data Kategori';
$lang['kategori_title_new']				= 'Data Kategori Baru';
$lang['kategori_title_edit']			= 'Edit Data kategori';

// from/table
$lang['kategori_name']					= 'Nama kategori';
$lang['kategori_ket']					= 'Keterangan';
$lang['kategori_dimensi']				= 'Dimensi Fix';
$lang['kategori_p_dimensi']				= 'Panjang Dimensi Fix';
$lang['kategori_l_dimensi']				= 'Lebar Dimensi Fix';

$lang['kategori_satuan']				= 'Satuan Dimensi';

//button
$lang['kategori_btn_new']				= 'Baru';
$lang['kategori_btn_delete']			= 'Hapus';
$lang['kategori_btn_save']				= 'Simpan';
$lang['kategori_btn_cancel']			= 'Batal';
$lang['kategori_btn_or']				= 'or';

//messages
$lang['kategori_del_error']				= 'Anda belum memilih data kategori yang akan dihapus.';
$lang['kategori_del_failure']			= 'Tidak dapat menghapus data kategori';
$lang['kategori_delete_confirm']		= 'Apakah anda yakin akan menghapus data kategori terpilih?';
$lang['kategori_deleted']				= 'Data kategori berhasil dihapus';
$lang['kategori_no_records_found']		= 'Data tidak ditemukan.';

$lang['kategori_create_failure']		= 'Data kategori gagal disimpan';
$lang['kategori_create_success']		= 'Data kategori berhasil disimpan';

$lang['kategori_edit_success']			= 'Data kategori berhasil disimpan';
$lang['kategori_invalid_id']			= 'ID tidak Valid';

