<div class="box box-primary">
    <!--form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_kategori','name'=>'frm_kategori','role','class'=>'form-horizontal'))?>
    <div class="box-body">
        <div class="form-group <?= form_error('nmkategori') ? ' has-error' : ''; ?>">
            <label for="nmkategori" class="col-sm-2 control-label"><?= lang('kategori_name') ?></label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="nmkategori" name="nmkategori" maxlength="255" value="<?php echo set_value('nmkategori', isset($data->nmkategori) ? $data->nmkategori : ''); ?>" required autofocus>
            </div>
        </div>

        <div class="form-group <?= form_error('dimensi_p') || form_error('dimensi_p') ? 'has-error' : '' ?>">
            <label class="control-label col-sm-2" for="dimensi_p"><?= lang("kategori_dimensi") ?></label>
            <div class="col-sm-2">
                <div class="input-group">
                    <span class="input-group-addon">P</span>
                    <input type="text" name="dimensi_p" id="dimensi_p" class="form-control">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="input-group">
                    <span class="input-group-addon">L</span>
                    <input type="text" name="dimensi_l" id="dimensi_l" class="form-control">
                </div>
            </div>
        </div>

        <div class="form-group <?= form_error('satuan') ? 'has-error' : '' ?>">
            <label class="control-label col-sm-2" for="dimensi_p"><?= lang("kategori_satuan") ?></label>    
            <div class="col-sm-4">
                <select class="form-control" name="satuan" id="satuan">
                    <option></option>
                    <?php
                            if(isset($satuan) && is_array($satuan) && count($satuan)):
                                foreach ($satuan as $key => $isi) :
                                
                    ?>

                        <option value="<?= $isi->id_konversi ?>"><?= $isi->tampil2 ?></option>

                    <?php
                                endforeach;
                            endif;

                    ?>
                </select>
            </div>
        </div>            

        <div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
            <label for="ket" class="col-sm-2 control-label"><?= lang('kategori_ket') ?></label>
            <div class="col-sm-4">
                <textarea class="form-control" id="ket" name="ket"><?php echo set_value('ket', isset($data->ket) ? $data->ket : ''); ?></textarea>
            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" name="save" class="btn btn-primary"><?= lang('kategori_btn_save') ?></button>
                <?php
                echo lang('kategori_btn_or') . ' ' . anchor('kategori', lang('kategori_btn_cancel'));
                ?>
            </div>
        </div>
    </div>
    <?= form_close() ?>
</div>