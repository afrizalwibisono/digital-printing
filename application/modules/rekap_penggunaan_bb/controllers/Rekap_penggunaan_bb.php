<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for produksi
 */

class Rekap_penggunaan_bb extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission       = "Rekap Penggunaan Bahan Baku.View";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('rekap_penggunaan_bb/rekap_penggunaan_bb');
        $this->load->model(['produksi_model','produksi_detail_model',
                            'bahan_baku_model',
                            'tmp_produksi_model',
                            'tmp_produksi_detail_model',
                            'jenis/jenis_model',
                            'konversi_satuan/satuan_terkecil_model',
                            'merk/merk_model',
                            'kategori/kategori_model',
                            'stok_model',
                            'gudang/gudang_model',
                            'konversi_satuan/konversi_satuan_model',
                            'setting_upload/setting_upload_model',
                            'bom/produk_model']);

        $this->template->title(lang('produksi_title_manage'));
		$this->template->page_icon('fa fa-address-card-o');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);
        if(isset($_POST['download'])){

            if($this->cetak()){
            }
        };
        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search     = isset($_POST['table_search'])?$this->input->post('table_search'):'';
            $id_jenis   = $this->input->post('id_jenis');
            $id_kategori= $this->input->post('id_kategori');
            $id_merek   = $this->input->post('id_merek');
            $tgl_awal   = $this->input->post('tgl_awal');
            $tgl_akhir  = $this->input->post('tgl_akhir');
        }
        else
        {
            $search     = $this->input->get('search');
            $id_jenis   = $this->input->get('jenis');
            $id_kategori= $this->input->get('kategori');
            $id_merek   = $this->input->get('merek');
            $tgl_awal  = $this->input->get('tgl_awal');
            $tgl_akhir = $this->input->get('tgl_akhir');
        }

        $filter = "?search=".$search;

        $search2 = $this->db->escape_str($search);

        $addWhere = "";
        if($tgl_awal != '' && $tgl_akhir != '')
        {
            $filter   .= "&tgl_awal=".urlencode(date_ymd($tgl_awal))."&tgl_akhir=".urlencode(date_ymd($tgl_akhir));
            $addWhere .= " AND ( date(produksi.created_on) >='".date_ymd($tgl_awal)."' AND date(produksi.created_on) <='".date_ymd($tgl_akhir)."')";
        }
        else
        {
             $tgl_awal = date('d/m/Y');
            $tgl_akhir = date('d/m/Y');
            
            $filter   .= "&tgl_awal=".date_ymd($tgl_awal)."&tgl_akhir=".date_ymd($tgl_akhir);
            $addWhere .= " AND ( date(produksi.created_on) >='".date_ymd($tgl_awal)."' AND date(produksi.created_on) <='".date_ymd($tgl_akhir)."')";
        }
        if($id_jenis !="")
        {
            $addWhere .= "AND barang.idjenis_bb=".urldecode($id_jenis)." ";
            $filter   .= "&jenis=".urlencode($id_jenis);
        }
        if($id_kategori !="")
        {
            $addWhere .= "AND barang.idkategori=".urldecode($id_kategori)." ";
            $filter   .= "&kategori=".urlencode($id_kategori);
        }
        if($id_merek !="")
        {
            $addWhere .= "AND barang.idmerk_bb=".urldecode($id_merek)." ";
            $filter   .= "&merek=".urlencode($id_merek);
        }

        $where="produksi.deleted = 0
                    $addWhere
                    AND (`nm_barang` LIKE '%$search2%' ESCAPE '!'
                    OR `barcode` LIKE '%$search2%' ESCAPE '!' 
                    OR `nmjenis_bb` LIKE '%$search2%' ESCAPE '!'
                    OR `nmkategori` LIKE '%$search2%' ESCAPE '!'
                    OR `nm_merk_bb` LIKE '%$search2%' ESCAPE '!'
                    OR `ket_barang` LIKE '%$search2%' ESCAPE '!'
                   )";
        
        $total = $this->produksi_model
                    ->select(array("barang.*","jenis_bb.nmjenis_bb","merk_bb.nm_merk_bb","nmkategori","satuan_terkecil.alias as satuan_terkecil","SUM(real_qty) AS stok"))
                    ->join("produksi_detail","produksi_detail.idproduksi = produksi.idproduksi","left")
                    ->join("barang","barang.idbarang_bb = produksi_detail.idbarang_bb","left")
                    ->join("jenis_bb","barang.idjenis_bb = jenis_bb.idjenis_bb","left")
                    ->join("kategori","barang.idkategori = kategori.idkategori","left")
                    ->join("satuan_terkecil","barang.id_satuan_terkecil=satuan_terkecil.id_satuan_terkecil","left")
                    ->join("merk_bb","barang.idmerk_bb = merk_bb.idmerk_bb","left")
                    ->where($where)
                    ->group_by(array("barang.idbarang_bb"))
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->produksi_model
                    ->select(array("barang.*","jenis_bb.nmjenis_bb","merk_bb.nm_merk_bb","nmkategori","satuan_terkecil.alias as satuan_terkecil","SUM(real_qty) AS stok_real"))
                    ->join("produksi_detail","produksi_detail.idproduksi = produksi.idproduksi","left")
                    ->join("barang","barang.idbarang_bb = produksi_detail.idbarang_bb","left")
                    ->join("jenis_bb","barang.idjenis_bb = jenis_bb.idjenis_bb","left")
                    ->join("kategori","barang.idkategori = kategori.idkategori","left")
                    ->join("satuan_terkecil","barang.id_satuan_terkecil=satuan_terkecil.id_satuan_terkecil","left")
                    ->join("merk_bb","barang.idmerk_bb = merk_bb.idmerk_bb","left")
                    ->where($where)
                    ->group_by(array("barang.idbarang_bb"))
                    ->order_by('nm_barang','ASC')
                    ->limit($limit, $offset)
                    ->find_all();

        if($data)
        {
            foreach ($data as $key => $dt) {
                if ($dt->st_potong_meteran ==1) {
                    $data_real_p = hitung_ke_satuan_kecil($dt->id_konversi, floatval(remove_comma($dt->panjang_spek)));
                    $real_p = $data_real_p['qty'];
                    $data_real_l = hitung_ke_satuan_kecil($dt->id_konversi, floatval(remove_comma($dt->lebar_spek)));
                    $real_l = $data_real_l['qty'];
                    $spek = $real_p * $real_l;



                    $data[$key]->stok = $dt->stok_real / $spek;

                    // $data[$key]->stok = $dt->stok_real / $spek;
                    $data[$key]->stok = $dt->stok_real;
                    // $data[$key]->opt_select = get_konversi_select($dt->id_satuan_terkecil,2);
                    $data[$key]->opt_select = get_konversi_select(4,2);
                    // konversi barang asli
                    // $konversi_a = get_konversi($dt->id_satuan_terkecil);
                    // konversi pnajang/meteran
                    // $konversi_b = get_konversi(4,'',FALSE);
                    // $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
                    // $data[$key]->konversi = array_merge($konversi_a, $konversi_b);
                    $data[$key]->konversi = get_konversi(4,'',FALSE);;
                }else{
                    $data[$key]->stok = $dt->stok_real;
                    $data[$key]->opt_select = get_konversi_select($dt->id_satuan_terkecil,2);
                    $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
                }
            }
        }

        $jenis_bb   = $this->jenis_model->order_by('nmjenis_bb','ASC')->find_all_by('deleted', 0);
        $merek      = $this->merk_model->order_by('nm_merk_bb', 'ASC')->find_all_by('deleted', 0);
        $kategori   = $this->kategori_model->order_by('nmkategori', 'ASC')->find_all_by('deleted', 0);
        $gudang     = $this->gudang_model->order_by('nama_gudang', 'ASC')->find_all_by('deleted', 0);
        
        $assets = array(
                    'plugins/select2/dist/css/select2.min.css',
                    'plugins/select2/dist/js/select2.min.js',
                    'plugins/number/jquery.number.js',
                    'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                    'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                    'rekap_penggunaan_bb/assets/js/rekap_penggunaan_bb.js'
                    );

        add_assets($assets);

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('jenis', $jenis_bb);
        $this->template->set('merek', $merek);
        $this->template->set('kategori', $kategori);
        $this->template->set('gudang', $gudang);
        $this->template->set('tgl_awal', $tgl_awal);
        $this->template->set('tgl_akhir', $tgl_akhir);
        $this->template->set('id_jenis', $id_jenis);
        $this->template->set('idkategori', $id_kategori);
        $this->template->set('id_merek', $id_merek);

        $this->template->title(lang('stok_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

    function cetak(){

        $search     = isset($_POST['table_search'])?$this->input->post('table_search'):'';
        $id_jenis   = $this->input->post('id_jenis');
        $id_kategori= $this->input->post('id_kategori');
        $id_merek   = $this->input->post('id_merek');
        $tgl_awal   = $this->input->post('tgl_awal');
        $tgl_akhir  = $this->input->post('tgl_akhir');

        $addWhere = "";
        $search2 = $this->db->escape_str($search);
        if($tgl_awal != '' && $tgl_akhir != '')
        {
            $tgl_awal   = $this->input->post('tgl_awal');
            $tgl_akhir  = $this->input->post('tgl_akhir');
            $addWhere .= " AND ( date(produksi.created_on) >='".date_ymd($tgl_awal)."' AND date(produksi.created_on) <='".date_ymd($tgl_akhir)."')";            
        }
        else
        {
            $tgl_awal = date('Y-m-d');
            $tgl_akhir = date('Y-m-d');
            $addWhere .= " AND ( date(produksi.created_on) >='".date_ymd($tgl_awal)."' AND date(produksi.created_on) <='".date_ymd($tgl_akhir)."')";
        }
        if($id_jenis !="")
        {
            $addWhere .= "AND barang.idjenis_bb=".urldecode($id_jenis)." ";
        }
        if($id_kategori !="")
        {
            $addWhere .= "AND barang.idkategori=".urldecode($id_kategori)." ";
        }
        if($id_merek !="")
        {
            $addWhere .= "AND barang.idmerk_bb=".urldecode($id_merek)." ";
        }
        $where="produksi.deleted = 0
                    $addWhere
                    AND (`nm_barang` LIKE '%$search2%' ESCAPE '!'
                    OR `barcode` LIKE '%$search2%' ESCAPE '!' 
                    OR `nmjenis_bb` LIKE '%$search2%' ESCAPE '!'
                    OR `nmkategori` LIKE '%$search2%' ESCAPE '!'
                    OR `nm_merk_bb` LIKE '%$search2%' ESCAPE '!'
                    OR `ket_barang` LIKE '%$search2%' ESCAPE '!'
                   )";

        $data = $this->produksi_model
                    ->select(array("barang.*","jenis_bb.nmjenis_bb","merk_bb.nm_merk_bb","nmkategori","satuan_terkecil.alias as satuan_terkecil","SUM(real_qty) AS stok_real"))
                    ->join("produksi_detail","produksi_detail.idproduksi = produksi.idproduksi","left")
                    ->join("barang","barang.idbarang_bb = produksi_detail.idbarang_bb","left")
                    ->join("jenis_bb","barang.idjenis_bb = jenis_bb.idjenis_bb","left")
                    ->join("kategori","barang.idkategori = kategori.idkategori","left")
                    ->join("satuan_terkecil","barang.id_satuan_terkecil=satuan_terkecil.id_satuan_terkecil","left")
                    ->join("merk_bb","barang.idmerk_bb = merk_bb.idmerk_bb","left")
                    ->where($where)
                    ->group_by(array("barang.idbarang_bb"))
                    ->order_by('nm_barang','ASC')
                    ->find_all();
        if($data)
        {
            foreach ($data as $key => $dt) {
                if ($dt->st_potong_meteran ==1) {
                    $data_real_p = hitung_ke_satuan_kecil($dt->id_konversi, floatval(remove_comma($dt->panjang_spek)));
                    $real_p = $data_real_p['qty'];
                    $data_real_l = hitung_ke_satuan_kecil($dt->id_konversi, floatval(remove_comma($dt->lebar_spek)));
                    $real_l = $data_real_l['qty'];
                    $spek = $real_p * $real_l;

                    // $data[$key]->stok = $dt->stok_real / $spek;
                    $data[$key]->stok = $dt->stok_real/1000000;
                    $data[$key]->satuan_terkecil = "Meter Persegi";
                }else{
                    $data[$key]->stok = $dt->stok_real;
                }
                $data[$key]->opt_select = get_konversi_select($dt->id_satuan_terkecil,2);
                $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
            }
        }


        // $sql        = "SELECT 
        //                     `barang_keluar`.`no_faktur`,
        //                     `gudang`.`nama_gudang` AS gudang_asal,
        //                     date_format(`barang_keluar`.`tgl_keluar`,'%d-%m-%Y') as date,
        //                     `barang_keluar`.`no_req`,
        //                     g.nama_gudang AS gudang_target,
        //                     `barang`.`barcode`,
        //                     `barang`.`artikel`,
        //                     `merek`.`nm_merek`,
        //                     `detail_barang_keluar`.`qty`,
        //                     `detail_barang_keluar`.`id_konversi`,
        //                     `konversi_satuan`.`satuan_besar`,
        //                     (`detail_barang_keluar`.`sub_total` / `detail_barang_keluar`.`qty`) as hpp,
        //                     `detail_barang_keluar`.`sub_total`
        //                 FROM
        //                     barang_keluar
        //                         INNER JOIN
        //                     gudang ON barang_keluar.id_gudang = gudang.id_gudang
        //                         INNER JOIN
        //                     (SELECT 
        //                         id_gudang, nama_gudang
        //                     FROM
        //                         gudang
        //                     WHERE
        //                         deleted = 0) AS g ON barang_keluar.id_gudang_tujuan = g.id_gudang
        //                         INNER JOIN
        //                     detail_barang_keluar ON barang_keluar.no_faktur = detail_barang_keluar.no_faktur
        //                         INNER JOIN
        //                     konversi_satuan ON detail_barang_keluar.id_konversi = konversi_satuan.id_konversi
        //                         INNER JOIN
        //                     barang ON detail_barang_keluar.id_barang = barang.id_barang
        //                         INNER JOIN
        //                     merek ON barang.id_merek = merek.id_merek
        //                 WHERE ".$where." ORDER BY gudang.nama_gudang ASC , g.nama_gudang ASC , barang.nm_barang ASC";

        // $data   = $this->db->query($sql)->result();

        if(!is_array($data) || count($data)<1){

            $this->template->set_message("Data Kosong","error");
            return false;

        }

        // === buat file excelnya =====
        

        $this->load->library("phpexcell/PHPExcel");

        $objPHPExcel = new PHPExcel();

                // Set document properties
        $objPHPExcel->getProperties()->setCreator("COKEShome")
                                     ->setLastModifiedBy("COKEShome")
                                     ->setTitle("Report Penggunaan Bahan Baku By Produksi")
                                     ->setSubject("Report Penggunaan Bahan Baku By Produksi")
                                     ->setDescription("Menampilkan Data Bahan Baku")
                                     ->setKeywords("Bahan Baku")
                                     ->setCategory("rekap");

        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "No.");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, "Barcode");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, "Nama Barang");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, "kategori Barang");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, "Jenis Bahan Baku");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, "Merk Bahan Baku");
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, "JML Penggunaan");    
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 1, "Satuan");    

        foreach ($data as $key => $isi) {
            
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, ($key+2), ($key+1));
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, ($key+2), $isi->barcode);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, ($key+2), $isi->nm_barang);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, ($key+2), $isi->nmkategori);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, ($key+2), $isi->nmjenis_bb);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, ($key+2), $isi->nm_merk_bb);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, ($key+2), number_format($isi->stok,2));
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, ($key+2), $isi->satuan_terkecil);

            $objPHPExcel->getActiveSheet()->getStyle('B'.($key+2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $objPHPExcel->getActiveSheet()->getStyle('D'.($key+2).':E'.($key+2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


            $objPHPExcel->getActiveSheet()->getStyle('J'.($key+2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        }

        // === align center
        $objPHPExcel->getActiveSheet()->getStyle('A1:L1')
                                        ->getFill()
                                        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                                        ->getStartColor()->setARGB('E4E3E3');

        $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->setBold(true);
               
        foreach(range('A','K') as $columnID) {

                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }

        // Rename worksheet
        $tgl_proses     = date("dmYHis");
        $nmfile         = "Rekap Penggunaan Bahan Baku".$tgl_proses.".xlsx";
        $objPHPExcel->getActiveSheet()->setTitle('DataBahanBaku_'.$tgl_proses);


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$nmfile.'"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');

        exit;
        //return true;

    }
}
?>