<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['produksi_title_view'] 	= 'Detail Penggunaan Bahan Baku By Produksi';

// form/table
$lang['penggunaan_bb_range_tanggal']	= 'Range Tanggal';
$lang['penggunaan_bb_barcode']			= 'Barcode';
$lang['penggunaan_bb_nm_barang']		= 'Nama Barang';
$lang['penggunaan_bb_kategori']			= 'Kategori Bahan Baku';
$lang['penggunaan_bb_jenis']			= 'Janis Bahan Baku';
$lang['penggunaan_bb_merk']				= 'Merk Bahan Baku';
$lang['penggunaan_bb_satuan']			= 'Satuan';
$lang['penggunaan_bb_other_keyword']	= 'Keyword Lain';

$lang['penggunaan_bb_jml_pakai_produksi'] = 'JML Pakai Produksi';


$lang['penggunaan_bb_records_found']	= 'Data tidak ditemukan';