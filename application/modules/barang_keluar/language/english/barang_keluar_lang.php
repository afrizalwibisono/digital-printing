<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['barang_keluar_title_manage'] = 'Barang Keluar';
$lang['barang_keluar_title_new'] 	= 'Barang Keluar Baru';
$lang['barang_keluar_title_view'] 	= 'Detail Barang Keluar';
$lang['barang_keluar_laporan_title_view'] = 'Laporan Barang Keluar';

// form/table
$lang['barang_keluar_no_faktur'] = 'Nomor Faktur';
$lang['barang_keluar_tanggal'] 	= 'Tanggal';
$lang['barang_keluar_total'] 	= 'Total';
$lang['barang_keluar_asal'] 	= 'Gudang Asal';
$lang['barang_keluar_tujuan'] 	= 'Gudang Tujuan';
$lang['barang_keluar_pil_tujuan'] = '-- Pilih Tujuan --';
$lang['barang_keluar_no_req'] 	= 'Kode Permintaan';
$lang['barang_keluar_gudang'] 	= 'Gudang';
$lang['barang_keluar_ket'] 		= 'Keterangan Tambahan';
$lang['barang_keluar_surat_jalan'] = 'Surat Jalan';
$lang['barang_keluar_pengirim'] = 'Pengirim';

$lang['barang_keluar_opt_biasa'] 		= 'Keluar biasa';
$lang['barang_keluar_opt_kirim_gudang'] = 'Pengiriman barang ke gudang';

$lang['barang_keluar_no'] 		= '#';
$lang['barang_keluar_barcode'] 	= 'Barcode';
$lang['barang_keluar_nm_barang'] = 'Nama Barang';
$lang['barang_keluar_qty'] 		= 'Qty';
$lang['barang_keluar_stok'] 	= 'Stok';
$lang['barang_keluar_harga'] 	= 'Harga';
$lang['barang_keluar_satuan'] 	= 'Satuan';
$lang['barang_keluar_total'] 	= 'Total';
$lang['barang_keluar_gtotal'] 	= 'Grand Total : ';


$lang['barang_keluar_ringkasan'] 		= 'Ringkasan';
$lang['barang_keluar_detail'] 			= 'Detail Transaksi';
$lang['barang_keluar_total_tran_qty'] 	= 'Transaksi';
$lang['barang_keluar_total_tran'] 		= 'Total Seluruh Transaksi';
$lang['barang_keluar_kali'] 			= 'kali';

$lang['barang_keluar_operator'] 	= 'Operator';
$lang['barang_keluar_cetak'] 		= 'Cetak';

$lang['barang_keluar_range']  	 = 'Range tanggal';
$lang['barang_keluar_tgl_awal']  = 'Tanggal awal';
$lang['barang_keluar_tgl_akhir'] = 'Tanggal akhir';

$lang['barang_keluar_pilih_brg'] = 'Pilih Barang';
$lang['barang_keluar_scan_brg']  = 'Ketik / Scan Barcode Barang';
$lang['barang_keluar_cari_brg']  = 'Cari Barang ...';
$lang['barang_keluar_cari_btn']  = 'Cari';
$lang['barang_keluar_pilih_btn'] = 'Pilih';

// button
$lang['barang_keluar_btn_new'] 		= 'Baru';
$lang['barang_keluar_btn_delete'] 	= 'Hapus';
$lang['barang_keluar_btn_save'] 	= 'Simpan';
$lang['barang_keluar_btn_cancel'] 	= 'Batal';
$lang['barang_keluar_btn_back'] 	= 'Kembali';
$lang['barang_keluar_or'] 			= 'atau';

// messages
$lang['barang_keluar_del_error']		= 'Anda belum memilih faktur barang keluar yang akan dihapus.';
$lang['barang_keluar_del_failure']		= 'Tidak dapat menghapus faktur barang keluar: ';
$lang['barang_keluar_delete_confirm']	= 'Apakah anda yakin akan menghapus faktur barang keluar terpilih ?';
$lang['barang_keluar_cancel_confirm']	= 'Apakah anda yakin akan membatalkan transaksi barang keluar ?';
$lang['barang_keluar_deleted']			= 'Data Barang Keluar berhasil dihapus';
$lang['barang_keluar_no_records_found'] = 'Data tidak ditemukan.';

$lang['barang_keluar_create_failure'] 	= 'Barang Keluar baru gagal disimpan: ';
$lang['barang_keluar_create_success'] 	= 'Barang Keluar baru berhasil disimpan';
$lang['barang_keluar_canceled'] 		= 'Barang Keluar telah berhasil dibatalkan';

$lang['barang_keluar_edit_success'] 	= 'Barang Keluar berhasil disimpan';
$lang['barang_keluar_invalid_id'] 		= 'ID Tidak Valid';

$lang['barang_keluar_no_item'] 			= 'Anda belum menambahkan data barang yang akan dikeluarkan.';
$lang['barang_keluar_qty_nol'] 			= 'Qty baris ke: %s tidak boleh kurang atau sama dengan 0 (nol).';
$lang['barang_keluar_harga_nol'] 		= 'Harga baris ke: %s tidak boleh kurang atau sama dengan 0 (nol).';
$lang['barang_keluar_stok_minus'] 		= 'Maaf stok barang baris ke: %s tidak tidak mencukupi.';
$lang['barang_keluar_kode_telah_digunakan'] = 'Maaf, kode permintaan "%s" sudah pernah diproses sebelumnya.';

$lang['barang_keluar_barcode_found'] 	= 'Barcode : %s ditemukan.';
$lang['barang_keluar_barcode_not_found'] = 'Barcode : %s tidak ditemukan.';

$lang['barang_keluar_only_ajax'] 		= 'Hanya ajax request yang diijinkan.';

$lang['barang_keluar_date_required'] 	= 'Silahkan pilih tanggal awal dan akhir.';
$lang['barang_keluar_stok_not_enough'] 	= 'Maaf stok tidak mencukupi untuk barang : %s';
$lang['barang_keluar_gudang_sama'] 		= 'Maaf, gudang asal dan gudang tujuan tidak boleh sama.';