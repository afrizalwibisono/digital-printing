$(document).ready(function(){
	$("#id_gudang_tujuan, #id_gudang_asal").select2({placeholder : "-- Pilih Gudang --",allowClear : true});

	$("#ck_all").on("click", function(){
		if($(this).prop("checked")){
			$("input[name='item_code[]']").prop("checked", true);
		}else{
			$("input[name='item_code[]']").prop("checked", false);
		}
	});

	$("#cari_btn").on("click", function(){
		cari_barang();
	});

	$("#cr_barang").on("keydown", function(e){
		if(e.keyCode == 13){
			cari_barang();
			e.preventDefault();
		}
	});

	//Format Text
	$("input[name='harga[]']").number( true, 0 );

	//Scan Barcode
	$("input[name='barcode']").on("keydown", function(e){
		if(e.keyCode == 13){
			scan_barcode();
			e.preventDefault();
		}
	});

	//Add Items
	$("#pilih_btn").on("click", function(){
		add_items();
	});

	//Load Request on key enter
	$("#no_req").on("keydown", function(e){
		if(e.keyCode == 13){
			load_request();
			e.preventDefault();
		}
	});
	$("select[name='konversi[]']").select2();
	//Tanggal Keluar
	$('#tgl_keluar').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    todayHighlight: true
	});
});

//cari barang
function cari_barang(){
	var cari = $("#cr_barang").val();
	var id_gudang_asal 	= $("#id_gudang_asal").val();

	if(!id_gudang_asal && $("#id_gudang_asal").length > 0){
        alertify.error('Silahkan pilih gudang asal dulu.');
        return false;
	}

	$.ajax({
		url : baseurl+"barang_keluar/cari_barang",
		type : "post",
		dataType : "json",
		data : {cr_barang : cari, id_gudang_asal: id_gudang_asal},
		success : function(msg){
			$("table#tbl_barang tbody tr").remove();
			if(!msg){
				var $tr = $("<tr>"
							+"<td colspan='5' class='text-center'>Data ditemukan</td>"
							+"</tr>");

				$("table#tbl_barang tbody").append($tr);
			}else{

				var opt_select = "";
				$.each(msg, function(i,n){
					$.each(n['konversi'], function(l,m){
						var selected = "";
						if(m['selected'] == 1)
						{
							selected = "selected";
						}

						opt_select += '<option value="'+m['id_konversi']+'" '+selected+'>'+m['tampil2']+'</option>';
					});

					console.log(n['barcode']);

					var $tr = $("<tr data-konversi='"+opt_select+"'>"
								+"<td><input type='checkbox' name='item_code[]' value='"+n['idbarang_bb']+"'></td>"
								+"<td><input type='hidden' name='item_barcode[]' value='"+n['barcode']+"' />"+(i+1)+"</td>"
								+"<td><input type='hidden' name='item_id_satuan[]' value='"+n['id_satuan_terkecil']+"' />"+n['barcode']+"</td>"
								+"<td><input type='hidden' name='item_nm_satuan[]' value='"+n['satuan']+"' /><input type='hidden' name='item_nm_barang[]' value='"+n['nm_barang']+"' />"+n['nm_barang']+"</td>"
								+"<td>"+($.number(n['stok']))+" "+n['satuan']+"</td>"
								+"</tr>");

					$("table#tbl_barang tbody").append($tr);

					opt_select = "";
				});
				
			}
			
		}
	});
}

//Add item to list
function add_items()
{
	var $obj_ids   = $("#tbl_barang input[name='item_code[]']:checked");

	var ids_barang = $("#tbl_barang input[name='item_code[]']:checked").map(function(){
		return $(this).val();
	}).get();

	var ids_barcodes = [];
	var nms_barang = [];
	var ids_satuan = [];
	var nms_satuan = [];
	var options 	= [];

	$.each($obj_ids, function(){
		ids_barcodes.push($(this).closest("tr").find("input[name='item_barcode[]']").val());
		nms_barang.push($(this).closest("tr").find("input[name='item_nm_barang[]']").val());
		ids_satuan.push($(this).closest("tr").find("input[name='item_id_satuan[]']").val());
		nms_satuan.push($(this).closest("tr").find("input[name='item_nm_satuan[]']").val());
		options.push($(this).closest("tr").data("konversi"));
	});

	if(ids_barang.length == 0)
	{
        alertify.error('Silahkan pilih barang yang akan ditambahkan dulu');
        return false;
	}

	var pj_ids = ids_barang.length;

	for (var i = 0; i < pj_ids; i++) {
		
 		var $row = $("<tr>"
		  					+"<td></td>"
		  					+"<td>"
		  						+ids_barcodes[i]
		  						+"<input type='hidden' name='id_barang[]' value='"+ids_barang[i]+"' />"
		  						+"<input type='hidden' name='id_satuan[]' value='"+ids_satuan[i]+"' />"
		  					+"</td>"
		  					+"<td>"+nms_barang[i]+" - <a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>"
		  					+"<td><input type='number' min='1' name='qty[]' class='form-control' value='1' /></td>"
		  					+"<td><select class='form-control' name='konversi[]' style='width: 200px;'>"+options[i]+"</select></td>"
		  					+"<td align='right'></td>"
		  				+"</tr>");

		var pj = $("#tdet_keluar tr").length;
		var cek = false;

		if(pj > 2){
			for (var j = 0; j <= pj - 1; j++) {
				var kode = $("#tdet_keluar tr:eq("+(j+1)+")").find("input[name='id_barang[]']").val();

				if(kode != "" && kode == ids_barang[i]){
					var qty = parseFloat($("#tdet_keluar tr:eq("+(j+1)+")").find("input[name='qty[]']").val());

					$("#tdet_keluar tr:eq("+(j+1)+")").find("input[name='qty[]']").val(qty+1);
					
					cek = true;
					break;
				}

			}
			
		}

		if(cek == false){
			$("#tdet_keluar tbody").append($row);
		}
	}

	$("#modal-barang").modal("hide");
	$("#tbl_barang input[type='checkbox']").prop('checked', false);	
	$("select[name='konversi[]']").select2();
	//Buat Nomor
	buat_no();
}

//buat nomor
function buat_no(){
	$.each($("#tdet_keluar tbody tr"), function(i){
		$(this).find("td:first").text(i+1);
	});
}

//Scan Barcoce
function scan_barcode(){
	var barcode 		= $("#barcode").val().trim();
	var id_gudang_asal 	= $("#id_gudang_asal").val();

	if(!barcode){
		return false;
	}

	if(!id_gudang_asal){
        alertify.error('Silahkan pilih gudang asal dulu.');
        return false;
	}

	$.ajax({
		url : baseurl+"barang_keluar/scan_barcode",
		type : "post",
		dataType : "json",
		data : {barcode : barcode, id_gudang_asal: id_gudang_asal},
		success : function(msg){
			//clear barcode
			$("#barcode").val("");

			if(msg['type'] == 'success'){ //Sukses
				$("#barcode_input").removeClass("has-error").addClass("has-success");
				$("#barcode_msg").removeClass("has-red").addClass("text-green");
				$("#barcode_msg").text("Barcode : "+msg['barcode']+" ditemukan.");

				var addToList = function(){
					var pj = $("#tdet_keluar tr").length;
					var cek = false;
					//Cek jika item sudah ada pada daftar
					if(pj > 2){
						for (var j = 0; j <= pj - 1; j++) {
							var kode = $("#tdet_keluar tr:eq("+(j+1)+")").find("input[name='id_barang[]']").val();

							if(kode != "" && kode == msg['data'][0]['idbarang_bb']){
								var qty = parseFloat($("#tdet_keluar tr:eq("+(j+1)+")").find("input[name='qty[]']").val());

								$("#tdet_keluar tr:eq("+(j+1)+")").find("input[name='qty[]']").val(qty+1); //Trigger Change
								$("input[name='id_barang[]'][value='"+msg['data'][0]['idbarang_bb']+"']").closest("tr").find("input[name='qty[]']").trigger("change");
								
								cek = true;
								break;
							}

						}
						
					}

					if(cek == false){
						var opt_select = "";

						$.each(msg['data'][0]['konversi'], function(i,n){
							var selected = "";
							if(n['selected'] == 1)
							{
								selected = "selected";
							}

							opt_select += '<option value="'+n['id_konversi']+'" '+selected+'>'+n['tampil2']+'</option>';
						});

						var $row = $("<tr>"
				  					+"<td></td>"
				  					+"<td>"
				  						+msg['data'][0]['barcode']
				  						+"<input type='hidden' name='id_barang[]' value='"+msg['data'][0]['id_barang']+"' />"
				  						+"<input type='hidden' name='id_satuan[]' value='"+msg['data'][0]['id_satuan_terkecil']+"' />"
				  					+"</td>"
				  					+"<td>"+msg['data'][0]['nm_barang']+" - <a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>"
				  					+"<td><input type='number' min='1' name='qty[]' class='form-control' value='1' /></td>"
				  					+"<td><select class='form-control' name='konversi[]' style='width: 200px;'>"+opt_select+"</select></td>"
			  						+"<td align='right'></td>"
				  				+"</tr>");

						$("#tdet_keluar tbody").append($row);
					}

					$("#modal-barang").modal("hide");
					$("#tbl_barang input[type='checkbox']").prop('checked', false);
					$("select[name='konversi[]']").select2();
					//Buat Nomor
					buat_no();
				}

				addToList();

			}else{ //gagal
				$("#barcode_input").removeClass("has-success").addClass("has-error");
				$("#barcode_msg").removeClass("text-green").addClass("text-red");
				$("#barcode_msg").text("Barcode : "+msg['barcode']+" tidak ditemukan.");
			}
		}
	});
}

function load_request(){
	var kode_req = $("#no_req").val();

	if(kode_req == "")
	{
        alertify.error('Silahkan ketik Kode Permintaan kemudian tekan Enter');
        return false;
	}

	$.ajax({
		url : baseurl+"barang_keluar/load_request",
		type : "post",
		dataType : "json",
		data : {kode : kode_req},
		success : function(msg){
			//Remove data on list first
			$("#tdet_keluar tbody tr").remove();
			
			if(msg['tipe'] == 'success'){
				//Load data
				$("#id_gudang_tujuan").val(msg['data']['id_gudang']);
				$("#id_gudang_tujuan").prop("disabled", true).trigger("change");;

				if(msg['data']['detail']){
					$.each(msg['data']['detail'], function(i,n){

						var opt_select = "";
						$.each(n['konversi'], function(l,m){
							var selected = "";
							if(m['id_konversi'] == n['id_konversi'])
							{
								selected = "selected";
							}

							opt_select += '<option value="'+m['id_konversi']+'" '+selected+'>'+m['tampil2']+'</option>';
						});

						var $row = $("<tr>"
				  					+"<td></td>"
				  					+"<td>"
				  						+n['barcode']
				  						+"<input type='hidden' name='id_barang[]' value='"+n['id_barang']+"' />"
				  						+"<input type='hidden' name='id_satuan[]' value='"+n['id_satuan_terkecil']+"' />"
				  					+"</td>"
				  					+"<td>"+n['nm_barang']+" - <a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>"
				  					+"<td><input type='number' min='1' name='qty[]' class='form-control' value='"+n['qty']+"' /></td>"
				  					+"<td><select class='form-control' name='konversi[]' style='width: 200px'>"+opt_select+"</select></td>"
			  						+"<td align='right'>"+($.number(n['sub_total']))+"</td>"
				  				+"</tr>");

						$("#tdet_keluar tbody").append($row);
					});

					$("select[name='konversi[]']").select2();

					//Gtotal
					$("#lblgtotal").text($.number(msg['data']['total']));

					//Buat Nomor
					buat_no();
				}
			}
			else
			{
				alertify.error(msg['text']);
		        return false;
			}
		}
	});
}

//Remove Item
function remove_item(obj){
	$(obj).closest("tr").fadeTo("slow",0.2, function(){
		$(this).remove();
	});
}