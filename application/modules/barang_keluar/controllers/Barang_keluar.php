<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 * 
 * This is controller for Barang keluar
 */

class Barang_keluar extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Barang Keluar.View";
    protected $addPermission    = "Barang Keluar.Add";
    protected $managePermission = "Barang Keluar.Manage";
    protected $deletePermission = "Barang Keluar.Delete";
    protected $laporanPermission = "Laporan Barang Keluar.View";

    protected $prefixKey        = "FK";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('barang_keluar/barang_keluar');
        $this->load->model(array('barang_keluar/barang_keluar_model',
                                'barang_keluar/barang_keluar_detail_model',
                                'barang_keluar/barang_keluar_detail_hpp_model',
                                'bahan_baku/bahan_baku_model',
                                'stok_model',
                                'identitas_model',
                                'gudang/gudang_model',
                                'gudang_order/gudang_order_model',
                                'gudang_order/gudang_order_detail_model',
                                ));

        $this->template->title(lang('barang_keluar_title_manage'));
		$this->template->page_icon('fa fa-list-ul');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if (isset($_POST['delete']) && has_permission($this->deletePermission))
        {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                foreach ($checked as $pid)
                {
                    //Hapus Barang Keluar
                    $total          = 0;
                    $dt_keluar      = $this->barang_keluar_model->find($pid);
                    $total          = $dt_keluar->gtotal;
                    $del_keluar     = $this->barang_keluar_model->delete_where(array('no_faktur' => $pid));
                    
                    $query_hapus    = "";
                    $query_hapus    .= $this->db->last_query();

                    $del_hpp    = FALSE;
                    if($del_keluar)
                    {
                        $det_keluar_hpp = $this->barang_keluar_detail_hpp_model->find_all_by('no_faktur', $pid);
                        //Hapus Detail Barang Keluar HPP
                        $del_hpp = $this->barang_keluar_detail_hpp_model->delete_where(array('no_faktur' => $pid));
                        
                        $query_hapus .= "\n\n".$this->db->last_query();
                        //Kembalikan Stok
                        if($det_keluar_hpp)
                        {
                            foreach ($det_keluar_hpp as $key => $dkh) {
                                $this->stok_model->update_stok_by_kd_stok($dkh->id_barang, $dkh->id_stok, $dkh->qty, 1);
                            }
                        }

                        if($del_hpp)
                        {
                            //Update Status Outlet Order kembali ke 0
                            if($dt_keluar->no_req != "")
                            {
                                $this->gudang_order_model->update($dt_keluar->no_req, array('st_req' => 0));
                            }
                            
                            $keterangan = "SUKSES, hapus data barang keluar dengan ID : ".$pid;
                            $status     = 1;
                        }
                        else
                        {
                            $keterangan = "GAGAL, hapus data barang keluar dengan ID : ".$pid;
                            $status     = 0;
                        } 
                    }
                    else
                    {
                        $keterangan = "GAGAL, hapus data barang keluar dengan ID : ".$pid.", error : ".$this->barang_keluar_detail_model->error;
                        $status     = 0;
                    }

                    $nm_hak_akses   = $this->deletePermission; 
                    $kode_universal = $pid;
                    $jumlah         = $total;
                    $sql            = $query_hapus;

                    $hasil = simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                }

                if ($del_keluar && $del_hpp)
                {
                    $this->template->set_message(count($checked) .' '. lang('barang_keluar_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('barang_keluar_del_failure') . (isset($err) ? $err." " : "") . $this->barang_keluar_model->error, 'error');
                }
            }
            else
            {
                $this->template->set_message(lang('barang_keluar_del_error').$this->barang_keluar_model->error, 'error');
            }

            unset($_POST['delete']);
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
            $tgl_awal  = $this->input->post('tgl_awal');
            $tgl_akhir = $this->input->post('tgl_akhir');
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
            $tgl_awal  = $this->input->get('tgl_awal');
            $tgl_akhir = $this->input->get('tgl_akhir');
        }

        $filter = "?search=".urlencode($search);
        $search2 = $this->db->escape_str($search);

        $add_where = "";
        if($tgl_awal !='' && $tgl_akhir !='')
        {
            $filter    .= "&tgl_awal=".urlencode(date_ymd($tgl_awal))."&tgl_akhir=".urlencode(date_ymd($tgl_akhir));
            $add_where .= " AND (barang_keluar.tgl_keluar >='".date_ymd($tgl_awal)."' AND barang_keluar.tgl_keluar <='".date_ymd($tgl_akhir)."')";
        }
        
        $where = "`barang_keluar.deleted` = 0 $add_where
                AND (`no_faktur` LIKE '%$search2%' ESCAPE '!'
                OR `no_req` LIKE '%$search2%' ESCAPE '!'
                OR `gtotal` LIKE '%$search2%' ESCAPE '!'
                OR `nama_gudang` LIKE '%$search2%' ESCAPE '!'
                OR `barang_keluar`.`ket` LIKE '%$search2%' ESCAPE '!')";
        
        $total = $this->barang_keluar_model
                    ->join("gudang","barang_keluar.id_gudang_tujuan = gudang.id_gudang","left")
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->barang_keluar_model
                    ->select("*, barang_keluar.ket as ket")
                    ->join("gudang","barang_keluar.id_gudang_tujuan = gudang.id_gudang","left")
                    ->where($where)
                    ->order_by(['barang_keluar.created_on' => 'DESC', 'barang_keluar.tgl_keluar' => 'DESC'])
                    ->limit($limit, $offset)->find_all();

        $assets = array(
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        "barang_keluar/assets/js/barang_keluar_index.js"
                        );

        add_assets($assets);

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set("toolbar_title", lang('barang_keluar_title_manage'));
        $this->template->title(lang('barang_keluar_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

   	//Create Barang Keluar
   	public function create()
   	{

        $this->auth->restrict($this->addPermission);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_barang_keluar())
            {
              $this->template->set_message(lang("barang_keluar_create_success"), 'success');
              redirect('barang_keluar');
            }
        }

        //cek session data
        $data = $this->session->userdata('br_keluar');
        if(!$data)
        {
            $data = array('br_keluar' => array(
                                        'no_faktur' => gen_primary($this->prefixKey),
                                        'items'     => array(),
                                        'gtotal'    => 0,
                                        'id_gudang_asal'    => 0,
                                        'id_gudang_tujuan'  => 0,
                                        'tgl_keluar'=> '',
                                        'no_req'    => '',
                                        'ket'       => ''
                                        ));

            $this->session->set_userdata($data);
            $data = $this->session->userdata('br_keluar');
        }

        $gudang      = $this->gudang_model->order_by("nama_gudang","ASC")->find_all_by(["deleted" => 0]);

        $this->template->set('data', $data);
        $this->template->set('gudang', $gudang);

        $assets = array('plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'plugins/number/jquery.number.js',
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        "barang_keluar/assets/js/barang_keluar.js"
                        );

        add_assets($assets);

        $this->template->title(lang('barang_keluar_title_new'));
		$this->template->render('barang_keluar_form');
   	}

    //View Detail
    public function view($no_faktur = '')
    {
        $this->auth->restrict($this->viewPermission);

        if (!$no_faktur)
        {
            $this->template->set_message(lang("barang_keluar_invalid_id"), 'error');
            redirect('barang_keluar');
        }

        $data  = $this->barang_keluar_model->select("barang_keluar.*, barang_keluar.ket as ket, g1.nama_gudang as gudang_asal, g2.nama_gudang as gudang_tujuan")
                                        ->join("gudang as g1","barang_keluar.id_gudang = g1.id_gudang","left")
                                        ->join("gudang as g2","barang_keluar.id_gudang_tujuan = g2.id_gudang","left")
                                        ->find($no_faktur);
                                        
        $detail = $this->barang_keluar_detail_model->select("barang_keluar_detail.*, konversi_satuan.satuan_besar as satuan, barcode, nm_barang")
                                            ->join('barang','barang_keluar_detail.id_barang = barang.idbarang_bb')
                                            ->join('konversi_satuan','barang_keluar_detail.id_konversi = konversi_satuan.id_konversi',"left")
                                            ->order_by("nm_barang","ASC")
                                            ->find_all_by('no_faktur', $no_faktur);

        $this->template->set('no_faktur', $no_faktur);
        $this->template->set('data', $data);
        $this->template->set('detail', $detail);

        $this->template->title(lang('barang_keluar_title_view'));
        $this->template->render('view_barang_keluar');
    }

    public function cetak_surat_jalan($no_faktur = "")
    {
        $this->auth->restrict($this->addPermission);

        if (!$no_faktur)
        {
            echo lang("barang_keluar_invalid_id");
            return FALSE;
        }

        $data  = $this->barang_keluar_model->select("*, barang_keluar.ket as ket")
                                        ->join("gudang","barang_keluar.id_gudang_tujuan = gudang.id_gudang","left")
                                        ->find($no_faktur);
                                        
        $detail = $this->barang_keluar_detail_model->select("barang_keluar_detail.*, konversi_satuan.satuan_besar as satuan, barcode, nm_barang")
                                            ->join('barang','barang_keluar_detail.id_barang = barang.idbarang_bb')
                                            ->join('konversi_satuan','barang_keluar_detail.id_konversi = konversi_satuan.id_konversi',"left")
                                            ->order_by("nm_barang","ASC")
                                            ->find_all_by('no_faktur', $no_faktur);

        //Identitas
        $identitas    = $this->identitas_model->find(1);
        $idt_pengirim = $this->auth->userdata();

        $this->template->set('no_faktur', $no_faktur);
        $this->template->set('data', $data);
        $this->template->set('detail', $detail);
        $this->template->set('idt', $identitas);
        $this->template->set('pengirim', $idt_pengirim);

        $this->template->set_layout('cetak');
        $this->template->title(lang('barang_keluar_surat_jalan'));
        $this->template->render('surat_jalan');
    }

    //Cancel Barang masuk
    public function cancel()
    {
        $this->auth->restrict($this->addPermission);

        $this->session->unset_userdata('br_keluar');
        $this->template->set_message(lang('barang_keluar_canceled'), 'success');

        redirect('barang_keluar');
    }

    public function cari_barang()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('barang_keluar_only_ajax'), 'error');
            redirect('barang_keluar');
        }

        $id_gudang_asal = $this->input->post('id_gudang_asal');
        $cari = $this->db->escape_str($this->input->post('cr_barang'));

        $data = $this->bahan_baku_model->select(array("barang.idbarang_bb","barang.barcode","barang.nm_barang","barang.id_satuan_terkecil","satuan_terkecil.alias as satuan","sum(stok_update) as stok"))
                                    ->join("satuan_terkecil","barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","left")
                                    ->join("stok","barang.idbarang_bb = stok.id_barang")
                                    ->where("`barang.deleted` = 0 AND `stok`.`deleted` = 0 AND stok.id_gudang = '$id_gudang_asal' 
                                            AND (barcode like '%$cari%' OR nm_barang like '%$cari%')")
                                    ->order_by('nm_barang', 'ASC')
                                    ->group_by(array("barang.idbarang_bb"))
                                    ->having("stok >", 0)
                                    ->find_all();
        if($data)
        {
            foreach ($data as $key => $dt) {
                $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
            }
        }

        echo json_encode($data);
    }

    public function scan_barcode()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('barang_keluar_only_ajax'), 'error');
            redirect('barang_keluar');
        }

        $barcode        = $this->input->post('barcode');
        $id_gudang_asal = $this->input->post('id_gudang_asal');

        if($barcode != "")
        {
            $data = $this->bahan_baku_model->select(array("barang.*","satuan_terkecil.alias as satuan","sum(stok_update) as stok"))
                                    ->join("satuan_terkecil","barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","left")
                                    ->join("stok","barang.idbarang_bb = stok.id_barang")
                                    ->where("`barang.deleted` = 0 AND `stok`.`deleted` = 0 AND stok.id_gudang = '$id_gudang_asal' 
                                            AND barcode='$barcode'")
                                    ->order_by('nm_barang', 'ASC')
                                    ->group_by(array("barang.idbarang_bb"))
                                    ->having("stok >", 0)
                                    ->find_all();
            if($data)
            {
                foreach ($data as $key => $dt) {
                    $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
                }

                $return = array('type' => 'success', 'barcode' => $barcode, 'data' => $data);
            }
            else
            {
                $return = array('type' => 'error', 'barcode' => $barcode, 'data' => 0);
            }
        }
        else
        {
            $return = array('type' => 'error', 'barcode' => $barcode, 'data' => 0);
        }

        echo json_encode($return);
    }

    protected function cek_stok($id_barang = "", $qty = 0, $id_gudang_asal = '')
    {
        $cek_stok = $this->stok_model->order_by("created_on", "desc")
                                ->find_all_by(array('deleted'   => 0, 
                                                    'id_barang' => $id_barang, 
                                                    'stok_update >' => 0,
                                                    'id_gudang' => $id_gudang_asal
                                                    ));
        
        if(!$cek_stok)
        {
            return FALSE;
        }

        //Hitung stok
        $stok = 0;
        foreach ($cek_stok as $key0 => $ck) {
            $stok += $ck->stok_update;
        }

        if($stok < $qty)
        {
           return FALSE;
        }

        return TRUE;
    }

    protected function get_total_brg_keluar($id_barang = "", $qty = 0, $id_gudang_asal = '')
    {
        if($id_barang == "" || $qty == 0)
        {
            return 0;
        }

        $data = $this->stok_model->order_by("created_on", "desc")
                                ->find_all_by(array('deleted'   => 0, 
                                                    'id_barang' => $id_barang, 
                                                    'stok_update >' => 0,
                                                    'id_gudang' => $id_gudang_asal    
                                                    ));
        if(!$data)
        {
            return 0;
        }

        //Hitung total
        $total = 0;
        foreach ($data as $key => $dt) {
            if($dt->stok_update >= $qty)
            {
                $total += $qty * $dt->hpp;
                $data[$key]->stok_update -= $qty;
                $qty = 0;
                break;
            }
            else if($dt->stok_update < $qty)
            {
                $total += $dt->stok_update * $dt->hpp;
                $qty -= $dt->stok_update;
                $data[$key]->stok_update = 0;
            }
        }

        return $total;
    }

    public function load_request()
    {
        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('barang_keluar_only_ajax'), 'error');
            redirect('barang_keluar');
        }

        $kode = $this->input->post('kode');
        $kode = $this->db->escape_str($kode);

        //Cek apakah sudah pernah dikeluarkan sebelumnya
        $cek = $this->barang_keluar_model->find_by(array('no_req' => $kode));
        if($cek)
        {
            $result = array('tipe' => 'error', 'text' => 'Maaf, Kode Permintaan yang anda masukkan sudah pernah diproses.', 'data' => '');
            echo json_encode($result);
            return FALSE;
        }

        $data = $this->gudang_order_model->find($kode);
        if($data)
        {
            $detail = $this->gudang_order_detail_model->select("gudang_order_detail.*, konversi_satuan.satuan_besar as satuan, barcode, nm_barang, barang.id_satuan_terkecil")
                                            ->join('barang','gudang_order_detail.id_barang = barang.idbarang_bb',"left")
                                            ->join('konversi_satuan','gudang_order_detail.id_konversi = konversi_satuan.id_konversi',"left")
                                            ->order_by('nm_barang','ASC')
                                            ->find_all_by('no_req', $kode);
            if($detail)
            {
                $gtotal = 0;
                foreach ($detail as $key => $dt) {
                    $detail[$key]->konversi = get_konversi($dt->id_satuan_terkecil);

                    //Get real qty
                    $dt_real_qty = hitung_ke_satuan_kecil($dt->id_konversi, $dt->qty);
                    $real_qty    = $dt_real_qty['qty'];

                    $total       = $this->get_total_brg_keluar($dt->id_barang, $real_qty, $data->id_gudang);
                    $gtotal     += $total;
                    //Overwrite sub_total
                    $detail[$key]->sub_total = $total;
                }
                //Overwrite total
                $data->total = $gtotal;
            }

            $data->detail = $detail;

            $result = array('tipe' => 'success', 'text' => '', 'data' => $data);
        }
        else
        {
            $result = array('tipe' => 'error', 'text' => 'Kode Permintaan tidak ditemukan.', 'data' => '');
        }

        echo json_encode($result);
    }

    private function save_temp_data()
    {
        $no_faktur          = $this->input->post('no_faktur');
        $no_req             = $this->input->post('no_req');
        $id_gudang_asal     = $this->input->post('id_gudang_asal');
        $id_gudang_tujuan   = $this->input->post('id_gudang_tujuan');
        $tgl_keluar         = $this->input->post('tgl_keluar');
        $ket                = $this->input->post('ket');

        //Detail item
        $id_barang = $this->input->post('id_barang');
        $id_satuan = $this->input->post('id_satuan');
        $qty       = $this->input->post('qty');
        $konversi  = $this->input->post('konversi');

        $data = $this->session->userdata('br_keluar');
        $data['no_faktur']          = $no_faktur;
        $data['id_gudang_asal']     = $id_gudang_asal;
        $data['id_gudang_tujuan']   = $id_gudang_tujuan;
        $data['tgl_keluar']         = $tgl_keluar;
        $data['no_req']             = $no_req;
        $data['ket']                = $ket;

        $dt_item = array();
        $gtotal  = 0;
        if($id_barang)
        {
            foreach ($id_barang as $key => $br) {
                $dt_barang  = $this->bahan_baku_model->find($br);
                $total      = 0;
                if($dt_barang)
                {
                    /** Hitung Total */
                    //Get real qty
                    $dt_real_qty = hitung_ke_satuan_kecil($konversi[$key], floatval($qty[$key]));
                    $real_qty    = $dt_real_qty['qty'];

                    $total       = $this->get_total_brg_keluar($br, $real_qty, $id_gudang_asal);
                    $gtotal     += $total;

                    $dt_item[]   = array(
                                    'id_barang'   => $br,
                                    'barcode'     => $dt_barang->barcode,
                                    'nm_barang'   => $dt_barang->nm_barang,
                                    'qty'         => $qty[$key],
                                    'id_satuan'   => $id_satuan[$key],
                                    'id_konversi' => $konversi[$key],
                                    'konversi'    => get_konversi($id_satuan[$key]),
                                    'total'       => $total
                                    );
                }
            }
        }

        $data['gtotal'] = $gtotal;
        $data['items']  = $dt_item;
        //Set Session
        $this->session->set_userdata('br_keluar', $data);
    }
    
   	protected function save_barang_keluar()
   	{
        $no_faktur          = $this->input->post('no_faktur');
        $no_req             = $this->input->post('no_req');
        $id_gudang_asal     = $this->input->post('id_gudang_asal');
        $id_gudang_tujuan   = $this->input->post('id_gudang_tujuan');
        $tgl_keluar         = $this->input->post('tgl_keluar');
        $ket                = $this->input->post('ket');

        //Detail item
        $id_barang = $this->input->post('id_barang');
        $id_satuan = $this->input->post('id_satuan');
        $qty       = $this->input->post('qty');
        $konversi  = $this->input->post('konversi');
        //validasi
        if(count($id_barang) == 0)
        {
            //Save Session
            $this->save_temp_data();
            $this->template->set_message(lang('barang_keluar_no_item'), 'error');
            return FALSE;
        }

        if($id_gudang_asal == $id_gudang_tujuan){
            $this->template->set_message(lang('barang_keluar_gudang_sama'), 'error');
            return FALSE;
        }

        $ero_msg = array();
        $urut = 0;
        foreach ($id_barang as $key => $it) {
            $urut ++;
            if($qty[$key] <= 0)
            {
                $ero_msg[] = sprintf(lang('barang_keluar_qty_nol'), $urut);
            }

            if(!$this->cek_stok($it, $qty[$key], $id_gudang_asal))
            {
                $ero_msg[] = sprintf(lang('barang_keluar_stok_minus'), $urut);
            }
        }

        if($ero_msg)
        {
            //Save Session
            $this->save_temp_data();

            $ero_msg = implode("<br>", $ero_msg);
            $this->template->set_message($ero_msg, 'error');
            return FALSE;
        }

        $this->form_validation->set_rules('no_faktur','lang:barang_keluar_no_faktur','required');
        $this->form_validation->set_rules('tgl_keluar','lang:barang_keluar_tanggal','required|trim');
        if(isset($_POST['id_gudang_asal'])){
            $this->form_validation->set_rules('id_gudang_asal','lang:barang_keluar_asal','callback_default_select');    
        }
        
        if(!isset($_POST['no_req']) || $_POST['no_req'] == "")
        {
            $this->form_validation->set_rules('id_gudang_tujuan','lang:barang_keluar_tujuan','callback_default_select');
        }
        else
        {
            //Validasi kode permintaan
            $cek_brg_keluar = $this->barang_keluar_model->find_by(array('no_req' => $no_req));
            if($cek_brg_keluar) //Jika kode sudah pernah terpakai sebelumnya
            {
                $this->template->set_message(sprintf(lang('barang_keluar_kode_telah_digunakan'), $no_req),'error');
                return FALSE;
            }

            $cek_kode = $this->gudang_order_model->find($no_req);
            if($cek_kode)
            {
                $_POST['id_gudang_tujuan'] = $cek_kode->id_gudang;
                $id_gudang_tujuan          = $cek_kode->id_gudang;    
            }
        }

        $this->form_validation->set_message('default_select', 'You should choose the "%s" field');
        

        if ($this->form_validation->run() === FALSE) 
        {
            //Save Session
            $this->save_temp_data();

            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        //Start Transaction
        $this->db->trans_start();
        //Hitung Gtotal
        $gtotal = 0;
        foreach ($id_barang as $key => $br) {
            //Get real qty
            $dt_real_qty = hitung_ke_satuan_kecil($konversi[$key], floatval($qty[$key]));
            $real_qty    = $dt_real_qty['qty'];

            $total       = $this->get_total_brg_keluar($br, $real_qty, $id_gudang_asal);
            $gtotal     += $total;
        }
        //Simpan Ke table Barang Keluar
        $data_brg_keluar = array('no_faktur'    => $no_faktur,
                                'id_gudang'     => $id_gudang_asal,
                                'tgl_keluar'    => date_ymd($tgl_keluar), 
                                'gtotal'        => $gtotal, 
                                'no_req'        => $no_req,
                                'id_gudang_tujuan' => $id_gudang_tujuan,
                                'ket'           => $ket);

        $this->barang_keluar_model->insert($data_brg_keluar);
        $query_barang_keluar = $this->db->last_query();

        if($no_req != "")
        {
            //Update Status Outlet Order
            $this->gudang_order_model->update($no_req, array('st_req' => 1));
        }

        //Check sukses atau tidak
        $st_brg_keluar = $this->barang_keluar_model->count_by('no_faktur', $no_faktur);
        if($st_brg_keluar)
        {
            //Insert Detail Barang Keluar
            $data_det_keluar = array();
            $data_stok_keluar = array();
            foreach ($id_barang as $key => $br) {
                //Get real qty
                $dt_real_qty = hitung_ke_satuan_kecil($konversi[$key], floatval($qty[$key]));
                $real_qty    = $dt_real_qty['qty'];

                $total       = $this->get_total_brg_keluar($br, $real_qty, $id_gudang_asal);

                $data_det_keluar[] = array('id_detail_barang_keluar' => gen_primary(),
                                        'no_faktur' => $no_faktur,
                                        'id_barang' => $br,
                                        'qty'       => $qty[$key],
                                        'id_konversi' => $konversi[$key],
                                        'sub_total' => $total
                                        );
            }

            $this->barang_keluar_detail_model->insert_batch($data_det_keluar);
            $query_barang_keluar .= "\n\n".$this->db->last_query();

            $st_det_keluar = $this->barang_keluar_detail_model->count_by('no_faktur', $no_faktur);
            if($st_det_keluar == count($data_det_keluar))
            {
                $st_det_keluar = TRUE;
                foreach ($data_det_keluar as $key => $ddt) {
                    //Data Barang
                    $dt_barang  = $this->bahan_baku_model->find($ddt['id_barang']);
                    //Update Stok barang
                    //Get real qty
                    $dt_real_qty = hitung_ke_satuan_kecil($ddt['id_konversi'], $ddt['qty']);
                    $real_qty    = $dt_real_qty['qty'];

                    $stok_keluar = $this->stok_model->update_stok_by_kd_barang($ddt['id_barang'], $real_qty, 0, $id_gudang_asal);
                    $query_barang_keluar .= "\n\n".$this->db->last_query();
                    //Insert Table Detail Keluar HPP
                    $data_kel_hpp = array();
                    foreach ($stok_keluar as $key => $stk) {
                        $data_kel_hpp[] = array(
                                        'id_detail_barang_keluar_hpp' => gen_primary(),
                                        'no_faktur' => $ddt['no_faktur'],
                                        'id_barang' => $ddt['id_barang'],
                                        'qty'       => $stk['jml'],
                                        'hpp'       => $stk['hpp'],
                                        'id_satuan_terkecil' => $dt_barang->id_satuan_terkecil,
                                        'id_stok'   => $stk['id_stok']
                                        );
                    }

                    $this->barang_keluar_detail_hpp_model->insert_batch($data_kel_hpp);
                    $query_barang_keluar .= "\n\n".$this->db->last_query();
                }
            }
            else
            {
                $st_det_keluar = FALSE;
                //Delete Barang keluar
                $this->barang_keluar_detail_model->delete_where(array('no_faktur' => $no_faktur));
                $this->barang_keluar_model->soft_delete(false)->delete($no_faktur);
                $this->barang_keluar_detail_hpp_model->delete_where(array('no_faktur' => $no_faktur));
                //Kembalikan stok
                foreach ($data_det_keluar as $key => $ddt) {
                    //Get real qty
                    $dt_real_qty = hitung_ke_satuan_kecil($ddt['id_konversi'], $ddt['qty']);
                    $real_qty    = $dt_real_qty['qty'];

                    $this->stok_model->update_stok_by_kd_barang($ddt['id_barang'], $real_qty, 1, $id_gudang_asal);
                }
                
                $this->template->set_message(lang('barang_keluar_create_failure'),'error');
            }
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $keterangan = "GAGAL, tambah data barang keluar dengan ID ".$no_faktur;
            $status     = 0;

            $return = FALSE;
        }
        else
        {
            $keterangan = "SUKSES, tambah data barang keluar dengan ID : ".$no_faktur;
            $status     = 1;
            
            $return = TRUE;
        }

        //Save Log
        $nm_hak_akses   = $this->addPermission; 
        $kode_universal = $no_faktur;
        $jumlah         = $gtotal;
        $sql            = $query_barang_keluar;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        if($return)
        {
            //Clear Session
            $this->session->unset_userdata('br_keluar');
        }
        
        return $return;
   	}

    public function default_select($val)
    {
        return $val == "" ? FALSE : TRUE;
    }

    public function laporan()
    {
        $this->auth->restrict($this->laporanPermission);

        $this->form_validation->set_rules('tgl_awal', 'lang:barang_keluar_tgl_awal', 'required|trim');
        $this->form_validation->set_rules('tgl_akhir', 'lang:barang_keluar_tgl_akhir', 'required|trim');
        
        if ($this->form_validation->run() === FALSE) {
            $this->template->set_message(validation_errors(), 'error');
        }

        if(isset($_POST['table_search'])) {
            $search     = $this->input->post('table_search');
            $tgl_awal   = isset($_POST['tgl_awal']) ? date_ymd($this->input->post('tgl_awal')) : '';
            $tgl_akhir  = isset($_POST['tgl_awal']) ? date_ymd($this->input->post('tgl_akhir')) : '';
        } else {
            $search     = "";
            $tgl_awal   = "";
            $tgl_akhir  = "";
        }

        $search2 = $this->db->escape_str($search);

        $where = "barang_keluar.deleted = 0 AND (barang_keluar.tgl_keluar >='".$tgl_awal."' AND barang_keluar.tgl_keluar <='".$tgl_akhir."') AND nm_barang like '%$search2%'";

        $data = $this->barang_keluar_model->select(array("barang_keluar.*","users.nm_lengkap as operator","gudang.nama_gudang as nm_gudang_asal", "gudang_tujuan.nama_gudang as nm_gudang_tujuan"))
                                        ->join('barang_keluar_detail','barang_keluar.no_faktur = barang_keluar_detail.no_faktur')
                                        ->join('barang','barang_keluar_detail.id_barang = barang.idbarang_bb')
                                        ->join('users','barang_keluar.created_by = users.id_user',"left")
                                        ->join('gudang','barang_keluar.id_gudang = gudang.id_gudang')
                                        ->join('gudang as gudang_tujuan','barang_keluar.id_gudang_tujuan = gudang_tujuan.id_gudang')
                                        ->where($where)
                                        ->group_by("barang_keluar.no_faktur")
                                        ->order_by("barang_keluar.created_on","DESC")
                                        ->find_all();
        if($data)
        {
            foreach ($data as $key => $dt) {
                $detail = $this->barang_keluar_detail_model->select(array("barang_keluar_detail.*","barcode","nm_barang","satuan_besar as satuan"))
                                                        ->join('barang','barang_keluar_detail.id_barang = barang.idbarang_bb')
                                                        ->join("konversi_satuan","barang_keluar_detail.id_konversi = konversi_satuan.id_konversi","left")
                                                        ->where("no_faktur", $dt->no_faktur)
                                                        ->order_by("nm_barang","ASC")
                                                        ->find_all();
                $data[$key]->detail = $detail;
            }
        }

        //Identitas
        $identitas = $this->identitas_model->find(1);

        $assets = array(
                    'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                    'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                    'barang_keluar/assets/js/laporan.js'
                    );

        add_assets($assets);

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('idt', $identitas);
        $this->template->set('period1', $this->input->post('tgl_awal'));
        $this->template->set('period2', $this->input->post('tgl_akhir'));

        $this->template->title(lang('barang_keluar_laporan_title_view'));
        $this->template->render('laporan');
    }

}
?>