<div class="box box-default">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_surat_jalan','name'=>'frm_surat_jalan','role'=>'form','class'=>'form-horizontal')) ?>
    	<?php if($idt) : ?>
	    <div class="row header-laporan">
	        <div class="col-md-12">
	            <div class="col-md-1 laporan-logo">
	                <img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo">
	            </div>
	            <div class="col-md-6 laporan-identitas">
	                <address>
	                	<h3 class="no-margin">Surat Jalan</h3>
	                    <h4><?= $idt->nm_perusahaan ?></h4>
	                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
	                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
	                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?><br>
	                </address>
	            </div>
	            <div class="clearfix"></div>
	            <div class="laporan-garis"></div>
	        </div>
	    </div>
	    <?php endif ?>
    	<div class="box-body">
    		<table class="table-condensed table-summary">
                <tr>
                    <td width="120"><?= lang('barang_keluar_no_faktur') ?></td>
                    <td width="10">:</td>
                    <td><strong><?= $no_faktur ?></strong></td>
                    <td width="120"><?= lang('barang_keluar_tujuan') ?></td>
                    <td width="10">:</td>
                    <td><?= $data->nama_gudang ?></td>
                </tr>
                <tr>
                    <td width="120"><?= lang('barang_keluar_no_req') ?></td>
                    <td width="10">:</td>
                    <td><strong><?= ($data->no_req != "") ? strtoupper($data->no_req) : "-" ?></strong></td>
                	<td width="120">Dicetak Pada</td>
                    <td width="10">:</td>
                    <td><?= date('d/m/Y H:i:s') ?></td>
                </tr>
            </table>
            <h5>Detail Barang</h5>
		  	<div class="table-responsive">
		  		<table class="table table-condensed table-bordered table-detail">
		  			<thead>
		  				<tr class="success">
			  				<th width="50"><?= lang('barang_keluar_no') ?></th>
			  				<th><?= lang('barang_keluar_nm_barang') ?></th>
			  				<th><?= lang('barang_keluar_qty') ?></th>
			  				<th><?= lang('barang_keluar_satuan') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($detail) : ?>
		  					<?php foreach($detail as $key => $dt) : ?>
		  				<tr>
		  					<td><?= $key+1 ?></td>
		  					<td><?= $dt->nm_barang ?></td>
		  					<td><?= number_format($dt->qty) ?></td>
		  					<td><?= $dt->satuan ?></td>
		  				</tr>
		  					<?php endforeach; ?>
		  				<?php endif; ?>
		  			</tbody>
		  		</table>
		  	</div>
		  	<div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
			    <div class="col-sm-6">
			    	<label for="ket" class="control-label"><?= lang('barang_keluar_ket') ?></label>
			    	<p><?= $data->ket ?></p>
			    </div>
		  	</div>
		  	<br>
		  	<br>
		  	<div class="pull-right" style="margin-right: 100px">
		  		<?= lang('barang_keluar_pengirim') ?>,
		  		<br>
			  	<br>
			  	<br>
			  	<?= $pengirim->nm_karyawan ?>
			  	<br>
			  	<?= $data->nama_gudang ?>	
		  	</div>
	  	</div>
	<?= form_close() ?>
</div><!-- /.box -->
<script type="text/javascript">
	$(document).ready(function(){
		window.print();
	});
</script>