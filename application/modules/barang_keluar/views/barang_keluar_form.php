<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_barang_keluar','name'=>'frm_barang_keluar','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="form-group <?= form_error('no_faktur') ? ' has-error' : ''; ?>">
			    <div class="col-md-3">
			    	<label for="no_faktur" class="control-label"><?= lang('barang_keluar_no_faktur') ?></label>
			    	<input type="text" class="form-control" id="no_faktur" name="no_faktur" maxlength="100" value="<?= $data['no_faktur'] ?>" readonly>
			    </div>
			    <div class="col-md-2 req <?= form_error('no_req') ? ' has-error' : ''; ?>">
			    	<label for="no_req" class="control-label"><?= lang('barang_keluar_no_req') ?></label>
			    	<input type="text" class="form-control" id="no_req" name="no_req" value="<?= $data['no_req'] ?>" placeholder="Ketik kode kemudian tekan Enter">
			    </div>
			    <div class="col-md-3 <?= form_error('id_gudang_asal') ? ' has-error' : ''; ?>">
			    	<label for="id_gudang_asal" class="control-label"><?= lang('barang_keluar_asal') ?></label>
			    	<select name="id_gudang_asal" id="id_gudang_asal" class="form-control">
			    		<option></option>
			    		<?php foreach ($gudang as $key => $gd) : ?>
			    		<option value="<?= $gd->id_gudang ?>" <?= set_select('id_gudang_asal', $gd->id_gudang, isset($data['id_gudang_asal']) &&  $data['id_gudang_asal'] == $gd->id_gudang) ?>><?= ucwords($gd->nama_gudang) ?></option>
			    		<?php endforeach ?>
			    	</select>
			    </div>
			    <div class="col-md-2 <?= form_error('id_gudang_tujuan') ? ' has-error' : ''; ?>">
			    	<label for="id_gudang_tujuan" class="control-label"><?= lang('barang_keluar_tujuan') ?></label>
			    	<select name="id_gudang_tujuan" id="id_gudang_tujuan" class="form-control">
			    		<option></option>
			    		<?php foreach ($gudang as $key => $gd) : ?>
			    		<option value="<?= $gd->id_gudang ?>" <?= set_select('id_gudang_tujuan', $gd->id_gudang, isset($data['id_gudang_tujuan']) &&  $data['id_gudang_tujuan'] == $gd->id_gudang) ?>><?= ucwords($gd->nama_gudang) ?></option>
			    		<?php endforeach ?>
			    	</select>
			    </div>
			    <div class="col-md-2 <?= form_error('tgl_keluar') ? ' has-error' : ''; ?>">
			    	<label for="tgl_keluar" class="control-label"><?= lang('barang_keluar_tanggal') ?></label>
			    	<input type="text" class="form-control" id="tgl_keluar" name="tgl_keluar" maxlength="100" value="<?= $data['tgl_keluar'] ?>" readonly>
			    </div>
		  	</div>
		  	<div class="table-responsive">
		  		<table class="table table-bordered" id="tdet_keluar">
		  			<thead>
		  				<tr class="success">
			  				<th width="50"><?= lang('barang_keluar_no') ?></th>
			  				<th><?= lang('barang_keluar_barcode') ?></th>
			  				<th><?= lang('barang_keluar_nm_barang') ?></th>
			  				<th><?= lang('barang_keluar_qty') ?></th>
			  				<th><?= lang('barang_keluar_satuan') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($data['items']) : ?>
		  					<?php 
		  						$no = 1;
		  						foreach($data['items'] as $key => $dt) : 
		  					?>
		  				<tr>
		  					<td><?= $no++ ?></td>
		  					<td>
		  						<?= $dt['barcode'] ?>
		  						<input type="hidden" name="id_barang[]" value="<?= $dt['id_barang'] ?>" />
		  						<input type='hidden' name='id_satuan[]' value="<?= $dt['id_satuan'] ?>" />
		  					</td>
		  					<td><?= $dt['nm_barang'] ?> - <a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>
		  					<td><input type="number" min="1" name="qty[]" class="form-control" value="<?= $dt['qty'] ?>" data-qty="<?= $dt['qty'] ?>" /></td>
		  					<td>
		  						<select class='form-control' name='konversi[]' style='width: 150px;'>
		  							<?php 
		  								if($dt['konversi']) :
		  									foreach ($dt['konversi'] as $key => $kf) :
		  							?>
		  							<option value="<?= $kf->id_konversi ?>" <?= (isset($kf->id_konversi) && $kf->id_konversi == $dt['id_konversi']) ? "selected" : "" ?>><?= $kf->satuan_besar." (".$kf->ket.")" ?></option>
		  							<?php
		  									endforeach; 
		  								endif; 
		  							?>
		  						</select>
		  					</td>
		  				</tr>
		  					<?php endforeach; ?>
		  				<?php endif; ?>
		  			</tbody>
		  			<tfoot>
		  				<tr>
		  					<td colspan="5">
		  						<div class="input-group" id="barcode_input">
		  							<div class="input-group-btn">
		  								<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-barang"><i class="fa fa-plus-square"></i></button>
		  							</div>
		  							<input type="text" class="form-control" id="barcode" name="barcode" placeholder="<?= lang('barang_keluar_scan_brg') ?>" autocomplete="off" autofocus />
		  						</div>
		  						<span class="help-block text-green" id="barcode_msg"></span>
		  					</td>
		  				</tr>
		  			</tfoot>
		  		</table>
		  	</div>
		  	<div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
			    <div class="col-sm-6">
			    	<label for="ket" class="control-label"><?= lang('barang_keluar_ket') ?></label>
			    	<textarea class="form-control" id="ket" name="ket" maxlength="255"><?php echo set_value('ket', isset($data['ket']) ? $data['ket'] : ''); ?></textarea>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <div class="col-md-12">
			    	<input type="hidden" name="type" value="1" />
			      	<button type="submit" name="save" class="btn btn-primary"><?= lang('barang_keluar_btn_save') ?></button>
			    	<?php
	                	echo lang('barang_keluar_or') . ' ' . anchor('barang_keluar/cancel', lang('barang_keluar_btn_cancel'), array("onclick" => "return confirm('".lang('barang_keluar_cancel_confirm')."')"));
	                ?>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->
<div class="modal" id="modal-barang" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
	    <div class="modal-content">
	    	<?= form_open("",array('id'=>'frm_barang','name'=>'frm_barang','role'=>'form')) ?>
		      	<div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        	<h4 class="modal-title"><?= lang('barang_keluar_pilih_brg') ?></h4>
		      	</div>
		      	<div class="modal-body">
		      		<div class="form-group">
		      			<div class="input-group">
		      				<input type="text" class="form-control" id="cr_barang" name="cr_barang" placeholder="<?= lang('barang_keluar_cari_brg') ?>" />
		      				<div class="input-group-btn">
		      					<button type="button" class="btn btn-primary" name="cari_btn" id="cari_btn"><i class="fa fa-search"></i> <?= lang('barang_keluar_cari_btn') ?></button>
		      				</div>
		      			</div>
		      		</div>
		        	<div class="table-responsive">
		        		<table id="tbl_barang" class="table table-hover table-striped">
		        			<thead>
		        				<tr class="success">
			        				<th><input type="checkbox" id="ck_all" name="ck_all" /></th>
			        				<th><?= lang('barang_keluar_no') ?></th>
			        				<th><?= lang('barang_keluar_barcode') ?></th>
			        				<th><?= lang('barang_keluar_nm_barang') ?></th>
			        				<th><?= lang('barang_keluar_stok') ?></th>
			        			</tr>
		        			</thead>
		        			<tbody>
		        				
		        			</tbody>
		        		</table>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
		      		<button type="button" class="btn btn-primary" id="pilih_btn" name="pilih_btn"><?= lang('barang_keluar_pilih_btn') ?></button>
		      	</div>
	      	<?= form_close() ?>
	    </div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->