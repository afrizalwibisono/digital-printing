<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_barang_keluar','name'=>'frm_barang_keluar','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="form-group <?= form_error('no_faktur') ? ' has-error' : ''; ?>">
			    <div class="col-md-3">
			    	<label for="no_faktur" class="control-label"><?= lang('barang_keluar_no_faktur') ?></label>
			    	<input type="text" class="form-control" id="no_faktur" name="no_faktur" maxlength="100" value="<?= $no_faktur ?>" autofocus readonly>
			    </div>
			    <div class="col-md-2 req">
			    	<label for="no_req" class="control-label"><?= lang('barang_keluar_no_req') ?></label>
			    	<input type="text" class="form-control" id="no_req" name="no_req" value="<?= ($data->no_req != "") ? strtoupper($data->no_req) : "-" ?>" placeholder="Ketik kode kemudian tekan Enter" disabled />
			    </div>
			    <div class="col-md-3">
			    	<label for="id_gudang" class="control-label"><?= lang('barang_keluar_asal') ?></label>
			    	<input type="text" name="id_gudang" id="id_gudang" class="form-control" value="<?= ucwords($data->gudang_asal) ?>" disabled />
			    </div>
			    <div class="col-md-2">
			    	<label for="id_gudang_tujuan" class="control-label"><?= lang('barang_keluar_tujuan') ?></label>
			    	<input type="text" name="id_gudang_tujuan" id="id_gudang_tujuan" class="form-control" value="<?= ucwords($data->gudang_tujuan) ?>" disabled />
			    </div>
			    <div class="col-md-2 <?= form_error('tgl_keluar') ? ' has-error' : ''; ?>">
			    	<label for="tgl_keluar" class="control-label"><?= lang('barang_keluar_tanggal') ?></label>
			    	<input type="text" class="form-control" id="tgl_keluar" name="tgl_keluar" maxlength="100" value="<?= date('d/m/Y', strtotime($data->tgl_keluar)) ?>" readonly>
			    </div>
		  	</div>
		  	<div class="table-responsive">
		  		<table class="table table-bordered" id="tdet_masuk">
		  			<thead>
		  				<tr class="success">
			  				<th width="50"><?= lang('barang_keluar_no') ?></th>
			  				<th><?= lang('barang_keluar_barcode') ?></th>
			  				<th><?= lang('barang_keluar_nm_barang') ?></th>
			  				<th><?= lang('barang_keluar_qty') ?></th>
			  				<th><?= lang('barang_keluar_satuan') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($detail) : ?>
		  					<?php foreach($detail as $key => $dt) : ?>
		  				<tr>
		  					<td><?= $key+1 ?></td>
		  					<td><?= $dt->barcode ?></td>
		  					<td><?= $dt->nm_barang ?></td>
		  					<td><?= number_format($dt->qty) ?></td>
		  					<td><?= $dt->satuan ?></td>
		  				</tr>
		  					<?php endforeach; ?>
		  				<?php endif; ?>
		  			</tbody>
		  		</table>
		  	</div>
		  	<div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
			    <div class="col-sm-6">
			    	<label for="ket" class="control-label"><?= lang('barang_keluar_ket') ?></label>
			    	<textarea class="form-control" id="ket" name="ket" maxlength="255" readonly=""><?= $data->ket ?></textarea>
			    </div>
		  	</div>
		  	<div class="form-group">
		  		<div class="col-md-12">
		  			<?php echo anchor('barang_keluar', lang('barang_keluar_btn_back'), array('class' =>'btn btn-success')) ?>
		  		</div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->