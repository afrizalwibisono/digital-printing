<?php 
    $ENABLE_ADD     = has_permission('Barang Keluar.Add');
    $ENABLE_MANAGE  = has_permission('Barang Keluar.Manage');
    $ENABLE_DELETE  = has_permission('Barang Keluar.Delete');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_barang_keluar','name'=>'frm_barang_keluar')) ?>
        <div class="box-header">
            <div class="form-group">
                <?php if ($ENABLE_ADD) : ?>
	  	        <a href="<?= site_url('barang_keluar/create') ?>" class="btn btn-success" title="<?= lang('barang_keluar_btn_new') ?>"><?= lang('barang_keluar_btn_new') ?></a>
                <?php endif; ?>
                <div class="pull-right form-inline">
                    <div class="input-daterange input-group" data-toggle="tooltip" data-placement="top" title="<?= lang('barang_keluar_range') ?>">
                        <input type="text" class="form-control" name="tgl_awal" id="tgl_awal" value="" placeholder="<?= lang('barang_keluar_tgl_awal') ?>" />
                        <span class="input-group-addon">to</span>
                        <input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" value="" placeholder="<?= lang('barang_keluar_tgl_akhir') ?>" />
                    </div>
                    <div class="input-group">
                        <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="Search" autofocus />
                        <div class="input-group-btn">
                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
	<div class="box-body table-responsive no-padding">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
                        <th width="50">#</th>
                        <th><?= lang('barang_keluar_no_faktur') ?></th>
                        <th><?= lang('barang_keluar_tanggal') ?></th>
                        <th><?= lang('barang_keluar_no_req') ?></th>
                        <th><?= lang('barang_keluar_tujuan') ?></th>
                        <th><?= lang('barang_keluar_ket') ?></th>
                        <?php if($ENABLE_MANAGE) : ?>
                        <th width="80"></th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $atts = array(
                                'width'       => 744,
                                'height'      => 600,
                                'scrollbars'  => 'yes',
                                'status'      => 'yes',
                                'resizable'   => 'yes',
                                'screenx'     => 100,
                                'screeny'     => 100,
                                'window_name' => '_blank',
                                'class'       => 'text-black',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'left',
                                'title'       => 'Cetak Surat Jalan'        
                        );

                        foreach ($results as $record) : ?>
                    <tr>
                        <td class="column-check"><input type="checkbox" name="checked[]" value="<?= $record->no_faktur ?>" /></td>
                        <td><?= $numb; ?></td>
                        <td><?= $record->no_faktur ?></td>
                        <td><?= date('d/m/Y', strtotime($record->tgl_keluar)) ?></td>
                        <td><?= ($record->no_req != "") ? strtoupper($record->no_req) : "-" ?></td>
                        <td><?= ucwords($record->nama_gudang) ?></td>
                        <td><?= $record->ket ?></td>
                        <?php if($ENABLE_MANAGE) : ?>
                        <td style="padding-right:20px">
                            <?= anchor_popup('barang_keluar/cetak_surat_jalan/'.$record->no_faktur, "<i class='fa fa-print'></i>", $atts); ?>
                            &nbsp;|&nbsp;
                            <a class="text-black" href="<?= site_url('barang_keluar/view/' . $record->no_faktur); ?>" data-toggle="tooltip" data-placement="left" title="View Detail"><i class="fa fa-folder"></i></a>
                        </td>
                        <?php endif; ?>
                    </tr>
                    <?php $numb++; endforeach; ?>
                </tbody>
	  </table>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">
		<?php if($ENABLE_DELETE) : ?>
		<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('barang_keluar_btn_delete') ?>" onclick="return confirm('<?= (lang('barang_keluar_delete_confirm')); ?>')">
		<?php endif;
		echo $this->pagination->create_links(); 
		?>
	</div>
	<?php else: ?>
     <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('barang_keluar_no_records_found') ?></p>
    </div>
    <?php
	endif;
	echo form_close(); 
	?>
</div><!-- /.box -->