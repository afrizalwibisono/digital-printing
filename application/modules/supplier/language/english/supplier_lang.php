<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ============================
$lang['supplier_title_manage']			= 'Data Supplier';
$lang['supplier_title_new']				= 'Data Supplier Baru';
$lang['supplier_title_edit']			= 'Edit Data Supplier';

// from/table
$lang['supplier_name']					= 'Nama Supplier';
$lang['supplier_ket']					= 'Keterangan';
$lang['alamat']							= 'Alamat';
$lang['kota']							= 'Kota';
$lang['telp']							= 'No. Telp';
$lang['fax']							= 'Faximile';
$lang['whatsapp']						= 'WhatsApp';
$lang['email']							= 'Email';
$lang['npwp_badan']						= 'NPWP Badan';
$lang['nm_npwp']						= 'Nama Badan Usaha sesuai NPWP Badan';
$lang['alamat_npwp']					= 'Alamat Usaha sesuai NPWP Badan';


//button
$lang['supplier_btn_new']				= 'Baru';
$lang['supplier_btn_delete']			= 'Hapus';
$lang['supplier_btn_save']				= 'Simpan';
$lang['supplier_btn_cancel']			= 'Batal';

//messages
$lang['supplier_del_error']				= 'Anda belum memilih data Supplier yang akan dihapus.';
$lang['supplier_del_failure']			= 'Tidak dapat menghapus data Supplier';
$lang['supplier_delete_confirm']		= 'Apakah anda yakin akan menghapus data Supplier terpilih?';
$lang['supplier_deleted']				= 'Data Supplier berhasil dihapus';
$lang['supplier_no_records_found']		= 'Data tidak ditemukan.';

$lang['supplier_create_failure']		= 'Data Supplier gagal disimpan';
$lang['supplier_create_success']		= 'Data Supplier berhasil disimpan';

$lang['supplier_edit_success']			= 'Data Supplier berhasil disimpan';
$lang['supplier_invalid_id']			= 'ID tidak Valid';

