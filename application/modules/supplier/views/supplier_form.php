<div class="box box-primary">
    <!--form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_supplier','name'=>'frm_supplier','role','class'=>'form-horizontal'))?>
    <div class="box-body">
            <div class="form-group <?= form_error('nm_supplier') ? ' has-error' : ''; ?>">
                <label for="nm_supplier" class="col-sm-3 control-label"><?= lang('supplier_name') ?></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="nm_supplier" name="nm_supplier" maxlength="255" value="<?php echo set_value('nm_supplier', isset($data->nm_supplier) ? $data->nm_supplier : ''); ?>" required autofocus>
                </div>
            </div>
            <div class="form-group <?= form_error('alamat') ? ' has-error' : ''; ?>">
                <label for="alamat" class="col-sm-3 control-label"><?= lang('alamat') ?></label>
                <div class="col-sm-6">
                    <textarea class="form-control" id="alamat" name="alamat"><?php echo set_value('alamat', isset($data->alamat) ? $data->alamat : ''); ?></textarea>
                </div>
            </div>
            <div class="form-group <?= form_error('kota') ? ' has-error' : ''; ?>">
                <label for="kota" class="col-sm-3 control-label"><?= lang('kota') ?></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="kota" name="kota" maxlength="45" value="<?php echo set_value('kota', isset($data->kota) ? $data->kota : ''); ?>" required autofocus>
                </div>
            </div>
            <div class="form-group <?= form_error('telp') ? ' has-error' : ''; ?>">
                <label for="telp" class="col-sm-3 control-label"><?= lang('telp') ?></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="telp" name="telp" maxlength="45" value="<?php echo set_value('telp', isset($data->telp) ? $data->telp : ''); ?>" required autofocus>
                </div>
            </div>
            <div class="form-group <?= form_error('fax') ? ' has-error' : ''; ?>">
                <label for="fax" class="col-sm-3 control-label"><?= lang('fax') ?></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="fax" name="fax" maxlength="45" value="<?php echo set_value('fax', isset($data->fax) ? $data->fax : ''); ?>" required autofocus>
                </div>
            </div>
            <div class="form-group <?= form_error('wa') ? ' has-error' : ''; ?>">
                <label for="wa" class="col-sm-3 control-label"><?= lang('whatsapp') ?></label>
                <div class="col-sm-6">
                    <input type="number" class="form-control" id="wa" name="wa" maxlength="45" value="<?php echo set_value('wa', isset($data->wa) ? $data->wa : ''); ?>" required autofocus>
                </div>
            </div>
            <div class="form-group <?= form_error('email') ? ' has-error' : ''; ?>">
                <label for="email" class="col-sm-3 control-label"><?= lang('email') ?></label>
                <div class="col-sm-6">
                    <input type="email" class="form-control" id="email" name="email" maxlength="255" value="<?php echo set_value('email', isset($data->email) ? $data->email : ''); ?>" required autofocus>
                </div>
            </div>
            <div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
                <label for="ket" class="col-sm-3 control-label"><?= lang('supplier_ket') ?></label>
                <div class="col-sm-6">
                    <textarea class="form-control" id="ket" name="ket"><?php echo set_value('ket', isset($data->ket) ? $data->ket : ''); ?></textarea>
                </div>
            </div>

            <hr>

            <div class="form-group <?= form_error('npwp_badan') ? ' has-error' : ''; ?>">
                <label for="npwp_badan" class="col-sm-3 control-label"><?= lang('npwp_badan') ?></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="npwp_badan" name="npwp_badan" maxlength="255" value="<?php echo set_value('npwp_badan', isset($data->npwp_badan) ? $data->npwp_badan : ''); ?>" required autofocus>
                </div>
            </div>
            <div class="form-group <?= form_error('nm_npwp') ? ' has-error' : ''; ?>">
                <label for="nm_npwp" class="col-sm-3 control-label"><?= lang('nm_npwp') ?></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="nm_npwp" name="nm_npwp" maxlength="255" value="<?php echo set_value('nm_npwp', isset($data->nm_npwp) ? $data->nm_npwp : ''); ?>" required autofocus>
                </div>
            </div>
            <div class="form-group <?= form_error('alamat_npwp') ? ' has-error' : ''; ?>">
                <label for="alamat_npwp" class="col-sm-3 control-label"><?= lang('alamat_npwp') ?></label>
                <div class="col-sm-6">
                    <textarea class="form-control" id="alamat_npwp" name="alamat_npwp"><?php echo set_value('alamat_npwp', isset($data->alamat_npwp) ? $data->alamat_npwp : ''); ?></textarea>
                </div>
            </div>
        <div class="form-group">
            <div class="col-sm-offset-10 col-sm-10">
                <button type="submit" name="save" class="btn btn-primary"><?= lang('supplier_btn_save') ?></button>
                <?php
                echo lang('bf_or') . ' ' . anchor('supplier', lang('supplier_btn_cancel'));
                ?>
            </div>
        </div>
    </div>
    <?= form_close() ?>
</div>