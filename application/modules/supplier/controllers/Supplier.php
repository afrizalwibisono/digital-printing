<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 * 
 * This is controller for Supplier.

 */

class Supplier extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewsupplier   = "Supplier.View";
    protected $addsupplier    = "Supplier.Add";
    protected $managesupplier = "Supplier.Manage";
    protected $deletesupplier = "Supplier.Delete";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewsupplier);

        $this->lang->load('supplier/supplier');
        $this->load->model(array(
                                'Supplier_model'
                            ));

        $this->template->title(lang('supplier_title_manage'));
		$this->template->page_icon('fa fa-table');
    }

    public function index()
    {
        $this->auth->restrict($this->viewsupplier);

        if (isset($_POST['delete']) && has_permission($this->deletesupplier))
        {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                foreach ($checked as $pid)
                {
                    $result = $this->Supplier_model->delete($pid);
                }

                if ($result)
                {
                    $this->template->set_message(count($checked) .' '. lang('supplier_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('supplier_del_failure') . $this->Supplier_model->error, 'error');
                }
            }
            else
            {
                $this->template->set_message(lang('supplier_del_error') . $this->Supplier_model->error, 'error');
            }
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
        }

        $filter = "";
        if($search!="")
        {
            $filter = "?search=".$search;
        }

        $search2 = $this->db->escape_str($search);
        
        $where = "deleted = 0
                AND (`nm_supplier` LIKE '%$search2%' ESCAPE '!'
                OR `ket` LIKE '%$search2%' ESCAPE '!')";
                
        $total = $this->Supplier_model
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->Supplier_model
                    ->where($where)
                    ->order_by('nm_supplier','ASC')
                    ->limit($limit, $offset)->find_all();

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set("toolbar_title", lang('supplier_title_manage'));
        $this->template->title(lang('supplier_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

    public function create()
    {

        $this->auth->restrict($this->addsupplier);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_supplier())
            {
              $this->template->set_message(lang("supplier_create_success"), 'success');
              redirect('supplier');
            }
        }

        $this->template->set("page_title", lang('supplier_title_new'));
        $this->template->render('supplier_form');
    }

    protected function save_supplier($type='insert', $id=0)
    {

        $this->form_validation->set_rules('nm_supplier','lang:supplier_name','required|trim|max_length[255]');
        $this->form_validation->set_rules('alamat','lang:alamat','required|trim|max_length[255]');
        $this->form_validation->set_rules('kota','lang:kota','required|trim|max_length[45]');
        $this->form_validation->set_rules('telp','lang:telp','required|trim|max_length[45]');
        $this->form_validation->set_rules('fax','lang:fax','required|trim|max_length[45]');
        $this->form_validation->set_rules('wa','lang:whatsapp','required|trim|max_length[45]');
        $this->form_validation->set_rules('email','lang:email','required|trim|max_length[100]');
        $this->form_validation->set_rules('ket','lang:supplier_ket','trim|max_length[255]');

        if ($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        unset($_POST['submit'], $_POST['save']);

        if ($type == 'insert')
        {
            $_POST['idsupplier'] = gen_primary('spl','supplier','idsupplier');
            $id = $this->Supplier_model->insert($_POST);

            if (is_numeric($id))
            {
                $return = TRUE;
            }
            else
            {
                $return = FALSE;
            }
        }
        else if ($type == 'update')
        {
            $return = $this->Supplier_model->update($id, $_POST);
        }

        return $return;
    }

    public function edit()
    {
        
        $this->auth->restrict($this->managesupplier);        
        
        $id = $this->uri->segment(3);

        if (empty($id))
        {
            $this->template->set_message(lang("supplier_invalid_id"), 'error');
            redirect('supplier');
        }

        if (isset($_POST['save']))
        {
            if ($this->save_supplier('update', $id))
            {
                $this->template->set_message(lang("supplier_edit_success"), 'success');
            }

        }

        $data  = $this->Supplier_model->find_by(array('idsupplier' => $id));

        if(!$data)
        {
            $this->template->set_message(lang("supplier_invalid_id"), 'error');
            redirect('supplier');
        }
        
        $this->template->set('data', $data);
        $this->template->set('toolbar_title', lang("supplier_title_edit"));
        $this->template->set('page_title', lang("supplier_title_edit"));
        $this->template->render('supplier_form');
    }

}
?>