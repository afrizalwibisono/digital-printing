$(function(){

	get_satuan_min();

	inputan_min_order();

	get_kategori_bahan_baku();

	get_bahan_baku();

	kondisikan_status_hitung_bahan_baku();

	kondisikan_inputan_pakai_bahan();

	$("#jml_min, #min_p, #min_l, #jml_bahan, #p_bahan, #l_bahan").number(true);

	$("#kategori").select2({
        placeholder :"-- Pilih Kategori Produk --",
        allowClear  : true
  	});

	$("#satuan_min").select2({
        placeholder :"-- Pilih Satuan Hitung --",
        allowClear  : true
  	});  	

	$("#kategori_bahan").select2({
        placeholder :"-- Pilih Kategori Bahan Baku --",
        allowClear  : true
  	});  	

	$("#jenis_bahan").select2({
        placeholder :"-- Pilih Jenis Bahan Baku --",
        allowClear  : true
  	});  	

	$("#nm_bahan").select2({
        placeholder :"-- Pilih Bahan Baku --",
        allowClear  : true
  	});  		

  	$("#satuan_bahan").select2({
        placeholder :"-- Pilih Satuan Bahan Baku --",
        allowClear  : true
  	});  	
	
	$("#kategori").change(function(){

		get_kategori_bahan_baku();
		$("#daftar_bahan tbody tr").remove();
		kosongkan_bahan();

	});

  	$("#st_tipe").change(function(){

  		get_satuan_min();

  		if($("#st_order").val()==1){
  			
  			inputan_min_order();
  		}

  	});

  	$("#st_order").change(function(){

  		inputan_min_order();

  	});

  	$("#kategori_bahan").change(function(){

  		get_bahan_baku();

  	});

  	$("#jenis_bahan").change(function(){

  		get_bahan_baku();

  	});

  	$("#nm_bahan").change(function(){

  		kondisikan_status_hitung_bahan_baku();
  		get_satuan_bahan();

  	});

  	$("#cr_hitung_bahan").change(function(){

  		kondisikan_inputan_pakai_bahan();


  	});

  	$("#pakai_bahan").click(function(){

  		insert_daftar();

  	});

  	$("#btl_insert_bahan").click(function(){

  		kosongkan_bahan();

  	});

});

function kosongkan_bahan(){

	$("#kategori_bahan").val('').trigger("change");
	$("#jenis_bahan").val('').trigger("change");
	$("#nm_bahan option").remove();
	$("#cr_hitung_bahan option").remove();
	$("#satuan_bahan option").remove();

	$("#inputan_hitung_normal").hide(400);
	$("#inputan_hitung_dimensi").hide(400);
	$("#inputan_hitung_satuan").hide(400);

	$("#ket_bahan").val('');
	$("#jml_bahan").val('');
	$("#p_bahan").val('');
	$("#l_bahan").val('');

	$("#id_barang").val('');
	$("#isi_cr_hitung").val('');
	$("#isi_satuan_bahan").val('');

}

function remove_item(obj){

	var id 	= $(obj).closest("tr").find("input[name='dft_bahan_iddetail[]']").val();

	$(obj).closest("tr").animate({backgroundColor:'red'}, 1000).fadeOut(1000,function() {
		$(obj).closest("tr").remove();
	});   

}

function view_item(obj){

	var id_kategori 	= $(obj).closest("tr").find("input[name='dft_bahan_kategori[]']").val();
	var id_jenis 		= $(obj).closest("tr").find("input[name='dft_bahan_jenis[]']").val();
	var id_barang 		= $(obj).closest("tr").find("input[name='dft_bahan_id[]']").val();
	var cr_hitung 		= $(obj).closest("tr").find("input[name='dft_bahan_crhitung[]']").val();
	var id_satuan 		= $(obj).closest("tr").find("input[name='dft_bahan_satuan[]']").val();
	var ket 			= $(obj).closest("tr").find("input[name='dft_bahan_ket[]']").val();
	var jml 			= $(obj).closest("tr").find("input[name='dft_bahan_jml[]']").val();
	var p 				= $(obj).closest("tr").find("input[name='dft_bahan_p[]']").val();
	var l 				= $(obj).closest("tr").find("input[name='dft_bahan_l[]']").val();

	$("#id_barang").val(id_barang);
	$("#isi_cr_hitung").val(cr_hitung);
	$("#isi_satuan_bahan").val(id_satuan);

	$("#kategori_bahan").val(id_kategori).trigger("change");
	$("#jenis_bahan").val(id_jenis).trigger("change");

	$("#ket_bahan").val(ket);
	$("#jml_bahan").val(jml);
	$("#p_bahan").val(p);
	$("#l_bahan").val(l);

}

function get_satuan_bahan(){

	$("#satuan_bahan option").remove();
	var nm_bahan 	= $("#nm_bahan").val();
	var isi_pilih 	= $("#isi_satuan_bahan").val();

	if(nm_bahan){

		var id_satuan_terkecil 	= $("#nm_bahan").find("option:selected").data("satuan");
		var st_pakai 	 		= $("#nm_bahan").find("option:selected").data("pakai");

		var data 				= {id_satuan : id_satuan_terkecil, st_pakai : st_pakai};

		$.ajax({

					url 		: baseurl+"bom/finishing/get_satuan_bahan",
					type 		: "post",
					data 		: data,
					dataType 	: "json",
					success	 	: function(msg){

										if(msg){

											var isi 		= "<option></option>";
											var selected 	= "";
											$.each(msg,function(i,n){

												selected 	= isi_pilih == n['id_konversi'] ? 'selected' : '';
												isi 	+= 	"<option value='"+n['id_konversi']+"'"+ selected +">"+ n['tampil2'] +"</option>";

											});

											$("#satuan_bahan").append(isi);
										}

									}

		});

	}


}

function insert_daftar(){

	/*
		Jika ada bahan baku yang sama, maka data pada row yang sama akan dihapus, dan digantikan inputan yang baru
	*/

	var kategori 		= $("#kategori_bahan").val();
	var jenis 			= $("#jenis_bahan").val();
	var nm_bahan 		= $("#nm_bahan").val();
	var cr_hitung 		= $("#cr_hitung_bahan").val();
	var satuan_bahan 	= cr_hitung == 2 ? 0 : $("#satuan_bahan").val();
	var ket 			= $("#ket_bahan").val();
	var isi_jml 		= cr_hitung == 0 ? $("#jml_bahan").val() : 0;
	var isi_p_bahan		= cr_hitung == 1 ? $("#p_bahan").val() : 0;
	var isi_l_bahan		= cr_hitung == 1 ? $("#l_bahan").val() : 0;

	var nama_jenis		= $("#jenis_bahan option:selected").text();
	var nama_bahan		= $("#nm_bahan option:selected").text();
	var nama_cr			= $("#cr_hitung_bahan option:selected").text();

	var st_insert 	= false;

	if(kategori && jenis && nm_bahan && cr_hitung && ket){

		if(cr_hitung==0 && isi_jml){

			st_insert = true;

		}else if(cr_hitung==1 && isi_p_bahan && isi_l_bahan){

			st_insert = true;

		}else if(cr_hitung==2){

			st_insert = true;

		}else{

			st_insert = false;

		}

	}else{

		st_insert 	= false;

	}


	if(st_insert){ //jika true

		var jml_rec 	= $("#daftar_bahan tr").length;
		var ketemu 		= false;
		var row_ke 		= 0;

		var id_cari 	= "";

		if(jml_rec>1){ // 1 header , 1 inputan data

			for (var i = 0; i <= jml_rec-1 ; i++) {


				var id_list_bahan 	= $("#daftar_bahan tr:eq("+ (i+1) +")").find("input[name='dft_bahan_id[]']").val();
				if(nm_bahan == id_list_bahan){

					ketemu = true;
					break;

				}

			}

		}

		if(!ketemu){

			var isi 	= 	"<tr>"+
								"<td>"+
									"<input type='hidden' name = 'dft_bahan_iddetail[]' value='' >"+
									"<input type='hidden' name = 'dft_bahan_kategori[]' value='"+ kategori + "' >"+
									"<input type='hidden' name = 'dft_bahan_jenis[]' 	value='"+ jenis + "' >"+
									"<input type='hidden' name = 'dft_bahan_id[]' 		value='"+ nm_bahan + "' >"+
									"<input type='hidden' name = 'dft_bahan_crhitung[]' value='"+ cr_hitung + "' >"+
									"<input type='hidden' name = 'dft_bahan_ket[]' 		value='"+ ket + "' >"+
									"<input type='hidden' name = 'dft_bahan_jml[]' 		value='"+ isi_jml + "' >"+
									"<input type='hidden' name = 'dft_bahan_p[]' 		value='"+ isi_p_bahan + "' >"+
									"<input type='hidden' name = 'dft_bahan_l[]' 		value='"+ isi_l_bahan + "' >"+
									"<input type='hidden' name = 'dft_bahan_satuan[]' 	value='"+ satuan_bahan + "' >"+
									"<input type='hidden' name = 'dft_bahan_nm_jenis[]'	value='"+ nama_jenis + "' >"+
									"<input type='hidden' name = 'dft_bahan_nm_bahan[]'	value='"+ nama_bahan + "' >"+
									"<input type='hidden' name = 'dft_bahan_nm_cr[]'	value='"+ nama_cr + "' >"+
									nama_jenis+
								"</td>"+
								"<td>"+ nama_bahan +"</td>"+
								"<td>"+ nama_cr +"</td>"+
								"<td>"+
									"<a href='#del_daftar_bahan' onclick='remove_item(this)'><span class='fa fa-remove text-red'></span></a> "+
									" <a href='#view_daftar_bahan' onclick='view_item(this)'><span class='fa fa-folder text-black'></span></a>"+
								"</td>"+
							"</tr>";

			$("#daftar_bahan tbody").append(isi);

			kosongkan_bahan();

		}else{

			alertify.error('Bahan baku yang sama telah terdaftar');			

		}

	}else{

		alertify.warning('Cek kembali inputan penggunaan bahan baku');

	}


}

function kondisikan_inputan_pakai_bahan(){

	var st_hitung 	= $("#cr_hitung_bahan").val();
	if(st_hitung){

		if(st_hitung == 0){

			$("#inputan_hitung_normal").show(400);
			$("#inputan_hitung_dimensi").hide(400);
			$("#inputan_hitung_satuan").show(400);

		}else if(st_hitung == 1){

			$("#inputan_hitung_normal").hide(400);
			$("#inputan_hitung_dimensi").show(400);
			$("#inputan_hitung_satuan").show(400);

		}else{

			$("#inputan_hitung_normal").hide(400);
			$("#inputan_hitung_dimensi").hide(400);
			$("#inputan_hitung_satuan").hide(400);

		}

	}else{

		$("#inputan_hitung_normal").hide(400);
		$("#inputan_hitung_dimensi").hide(400);
		$("#inputan_hitung_satuan").hide(400);

	}

}


function kondisikan_status_hitung_bahan_baku(){

	var idbahan_baku 	= $("#nm_bahan").val();
	var st_potongan 	= $("#nm_bahan").find("option:selected").data("pakai");
	var isi_pilih	 	= $("#isi_cr_hitung").val();

	if(idbahan_baku){

		$("#cr_hitung_bahan option").remove();

		if(st_potongan == 0){

			var isi 	= 	"<option value='0' selected>Berdasarkan Jumlah (PCS, BOX, ROLL, dll)</option>"+
                			"<option value='2'>Sesuaikan Dengan Order</option>"

			$("#cr_hitung_bahan").append(isi);

		}else{

			var isi 	= 	"<option value='1' selected>Berdasarkan Dimensi P x L</option>"+
                			"<option value='2'>Sesuaikan Dengan Order</option>"

			$("#cr_hitung_bahan").append(isi);


		}

		if(isi_pilih){

			$("#cr_hitung_bahan").val(isi_pilih).trigger("change");

		}


		kondisikan_inputan_pakai_bahan();

	}

}

function get_bahan_baku(){

	var id_kategori 	= $("#kategori_bahan").val();
	var id_jenis 		= $("#jenis_bahan").val();
	var id_barang 		= $("#id_barang").val();

	$("#nm_bahan option").remove();

	if(id_kategori && id_jenis){

		$.ajax({
					url 		: baseurl + "bom/finishing/get_bahan_baku",
					type 		: "post",
					data 		: {idkategori : id_kategori, idjenis : id_jenis},
					dataType 	: "json",
					success 	: function(msg){

										if(msg){

											var isi = "<option></option>";
											var selected = "";
											$.each(msg,function(i,n){

												selected = id_barang == n['id'] ? 'selected' : '';
												isi 	+= 	"<option value='"+n['id']+"' data-satuan='"+ n['id_satuan_terkecil'] +"' data-pakai ='"+n['st_potongan']+"'" + selected + " >"+n['nm']+"</option>";

											});

											$("#nm_bahan").append(isi);

											kondisikan_status_hitung_bahan_baku();
											get_satuan_bahan();

										}

									}
		});

	}

}

function get_kategori_bahan_baku(){

	var kategori 	= $("#kategori").val();
	var data 		= {id_kategori : kategori};

	$("#kategori_bahan option").remove();

	$.ajax({
				url 		: baseurl + "bom/finishing/get_kategori_bahan_baku",
				type 		: "post",
				data 		: data,
				dataType 	: "json",
				success 	: function(msg){

									

									if(msg){

										var isi = "<option></option>";

										$.each(msg,function(i,n){

											isi 	+= 	"<option value='"+n['id']+"' >"+n['nm']+"</option>";

										});

										$("#kategori_bahan").append(isi);

									}

								}

	});

}

function inputan_min_order(){

	var st_pakai_min_order 	= $("#st_order").val();
	var st_tipe 			= $("#st_tipe").val();

	if(st_pakai_min_order == 0){

		$("#garis_min_order").hide(400);
		$("#st_tipe_normal").hide(400);
		$("#st_tipe_dimensi").hide(400);
		$("#satuan_min_order").hide(400);

	}else{

		$("#garis_min_order").show(400);
		$("#satuan_min_order").show(400);

		if(st_tipe == 0){

			$("#st_tipe_normal").show(400);
			$("#st_tipe_dimensi").hide(400);
			
		}else{

			$("#st_tipe_normal").hide(400);
			$("#st_tipe_dimensi").show(400);

		}

	}

}

function get_satuan_min(){

	var st 	= $("#st_tipe").val();

	$("#satuan_min option").remove();

	$.ajax({
				url 		: baseurl+"bom/finishing/get_satuan_minimal_order",
				type 		: "post",
				data		: {st : st},
				dataType 	: "json",
				success 	: function(msg){

								if(msg){

									var isi 	= "<option></option>";
									var isi_pil = $("#isi_satuan_min").val();

									$.each(msg,function(i,n){

										var terpilih 	= isi_pil == n['id_konversi'] ? 'selected' : '';
										isi 	+= 	"<option value='"+n['id_konversi']+"' "+ terpilih +" >"+n['tampil2']+"</option>";

									});

									$("#satuan_min").append(isi);

								}

							}

	});

}