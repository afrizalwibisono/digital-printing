<?php
	
		$disable 	= isset($lock) && $lock == 1 ? 'disabled' : '';

?>


<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array("name"=>"frm_baru","id"=>"frm_baru","role"=>"form","class"=>"form-horizontal")) ?>
	<input type="hidden" name="id_primary" value="<?= isset($dt['id_primary']) ? $dt['id_primary'] : '' ?>">
	<div class="box-body">
		
		<div class="form-group <?= form_error('barcode') ? 'has-error' : '' ?>">
			<label class="control-label col-sm-2" for="barcode"><?= lang("capt-barcode") ?></label>
			<div class="col-sm-4">
				<input type="text" name="barcode" <?= $disable ?> id="barcode" class="form-control" value="<?= set_value("barcode", isset($dt['barcode']) ? $dt['barcode'] : '')?>" />
			</div>
		</div>

		<div class="form-group <?= form_error('nm_produk') ? 'has-error' : '' ?>">
			<label class="control-label col-sm-2" for="nm_produk"><?= lang("capt-nm-produk") ?></label>
			<div class="col-sm-4">
				<input type="text" <?= $disable ?> name="nm_produk" id="nm_produk" class="form-control" value="<?= set_value("nm_produk", isset($dt['nm_produk']) ? $dt['nm_produk'] : '')?>" />
			</div>
		</div>		

		<div class="form-group <?= form_error('kategori') ? 'has-error' : '' ?>">
			<label class="control-label col-sm-2" for="kategori"><?= lang("capt-kategori") ?></label>
			<div class="col-sm-4">
				<select name="kategori" id="kategori" class="form-control" <?= $disable ?> >
					<option></option>
					<?php 
							if(isset($kategori) && is_array($kategori) && count($kategori)):
								foreach ($kategori as $key => $isi):
					?>
									<option value="<?=  $isi->id ?>" <?= set_select("kategori",$isi->id,isset($dt['kategori']) && $dt['kategori'] == $isi->id  ) ?> ><?= strtoupper($isi->nm) ?></option>
					<?php
								endforeach;
							endif;
					?>
				</select>
			</div>
		</div>				

		<div class="form-group <?= form_error('ket') ? 'has-error' : '' ?>">
			<label class="control-label col-sm-2" for="ket"><?= lang("capt-ket") ?></label>
			<div class="col-sm-4">
				<textarea <?= $disable ?> name="ket" id="ket" class="form-control" ><?= set_value("ket", isset($dt['ket']) ? $dt['ket'] : '' ) ?></textarea>
			</div>
		</div>		

		<div class="form-group <?= form_error('worksheet') ? 'has-error' : '' ?>">
			<label class="control-label col-sm-2" for="worksheet"><?= lang("capt-worksheet") ?></label>
			<div class="col-sm-4">
				<input type="hidden" name="isi_worksheet" id="isi_worksheet" value="<?= set_value("worksheet",isset($dt['isi_worksheet']) ? $dt['isi_worksheet'] : '') ?>" >
				<span id="worksheet" class="form-control"><?= isset($dt['worksheet']) ? $dt['worksheet'] : '' ?></span>
			</div>
		</div>				


		<div class="form-group <?= form_error('st_tipe') ? 'has-error' : '' ?>">
			<label class="control-label col-sm-2" for="st_tipe"><?= lang("capt-st-tipe") ?></label>
			<div class="col-sm-4">
				<select name="st_tipe" id="st_tipe" class="form-control" <?= $disable ?> >
					<option value="0" <?= set_select("st_tipe","0",isset($dt['st_tipe']) && $dt['st_tipe'] == 0) ?> ><?= lang("isi-st-tipe-normal") ?></option>
					<option value="1" <?= set_select("st_tipe","1",isset($dt['st_tipe']) && $dt['st_tipe'] == 1) ?>><?= lang("isi-st-tipe-dimensi") ?></option>
				</select>
			</div>
		</div>						

		<div class="form-group <?= form_error('st_order') ? 'has-error' : '' ?>" style="display: none" >
			<label class="control-label col-sm-2" for="st_order"><?= lang("capt-st-order") ?></label>
			<div class="col-sm-4">
				<select name="st_order" id="st_order" class="form-control" <?= $disable ?> >
					<option value="0" <?= set_select("st_order","0",isset($dt['st_order']) && $dt['st_order'] == 0) ?> ><?= lang("isi-st-order-tidak") ?></option>
					<option value="1" <?= set_select("st_order","1",isset($dt['st_order']) && $dt['st_order'] == 1) ?> ><?= lang("isi-st-order-pakai") ?></option>
				</select>
			</div>
		</div>						

		<hr id="garis_min_order">

		<div class="form-group <?= form_error('jml_min') ? 'has-error' : '' ?>" id="st_tipe_normal">
			<label class="control-label col-sm-2" for="jml_min"><?= lang("capt-jml-min") ?></label>
			<div class="col-sm-4">
				<input type="text" name="jml_min" id="jml_min" class="form-control" <?= $disable ?> value = "<?= set_value("jml_min",isset($dt['jml_min']) ? $dt['jml_min'] : '' ) ?>" />
			</div>
		</div>		

		<div class="form-group <?= form_error('min_p') || form_error('min_l') ? 'has-error' : '' ?>" id="st_tipe_dimensi">
			<label class="control-label col-sm-2" for="min_p"><?= lang("capt-ukuran-min") ?></label>
			<div class="col-sm-2 ">
				<div class="input-group">
					<span class="input-group-addon"><strong>P</strong></span>
					<input type="text" name="min_p" id="min_p" <?= $disable ?> class="form-control" value = "<?= set_value("min_p",isset($dt['p_min']) ? $dt['p_min'] : '' ) ?>">
				</div>
			</div>
			<div class="col-sm-2">
				<div class="input-group">
					<span class="input-group-addon"><strong>L</strong></span>
					<input type="text" name="min_l" id="min_l" <?= $disable ?> class="form-control" value = "<?= set_value("min_l",isset($dt['l_min']) ? $dt['l_min'] : '' ) ?>">
				</div>
			</div>

		</div>		

		<div class="form-group <?= form_error('satuan_min') ? 'has-error' : '' ?>" id="satuan_min_order">
			<label class="control-label col-sm-2" for="satuan_min"><?= lang("capt-satuan") ?></label>
			<div class="col-sm-4">
				<input type="hidden" name="isi_satuan_min" id="isi_satuan_min" <?= $disable ?> value="<?= set_value("satuan_min",isset($dt['satuan_min']) ? $dt['satuan_min'] : '') ?>" />
				<select name="satuan_min" id="satuan_min" class="form-control">
					<option></option>
				</select>
			</div>
		</div>						

		<!-- Tabs inputan data bahan baku penyusun -->

		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#bahanbaku" data-toggle="tab" aria-expanded="true"><?= lang("capt-tabs-bahanbaku") ?></a>
				</li>
			</ul>
			<div class="tab-content">
				<div id="bahanbaku" class="tab-pane active">
					
                	<div class="form-group">
                		<label class="control-label col-sm-3" for="kategori_bahan" ><?= lang("capt-kategori-bahan") ?></label>
                		<div class="col-sm-4">
                			<select class="form-control" name="kategori_bahan" id="kategori_bahan">
                				<option></option>
                			</select>
                		</div>
                	</div>
              		
              		<div class="form-group">
                		<label class="control-label col-sm-3" for="jenis_bahan" ><?= lang("capt-jenis-bahan") ?></label>
                		<div class="col-sm-4">
                			<select class="form-control" name="jenis_bahan" id="jenis_bahan">
                				<option></option>
                				<?php 
										if(isset($jenis) && is_array($jenis) && count($jenis)):
											foreach ($jenis as $key => $isi):
								?>
												<option value="<?=  $isi->id ?>" <?= set_select("jenis",$isi->id) ?> ><?= strtoupper($isi->nm) ?></option>
								<?php
											endforeach;
										endif;
								?>
                			</select>
                		</div>
                	</div>

                	<div class="form-group">
                		<label class="control-label col-sm-3" for="nm_bahan" ><?= lang("capt-nm-bahan") ?></label>
                		<div class="col-sm-4">
                			<input type="hidden" name="id_barang" id="id_barang">
                			<select class="form-control" name="nm_bahan" id="nm_bahan">
                				<option></option>
                			</select>
                		</div>
                	</div>

                	<div class="form-group">
                		<label class="control-label col-sm-3" for="cr_hitung_bahan" ><?= lang("capt-cr-pakai-bahan") ?></label>
                		<div class="col-sm-4">
                			<input type="hidden" name="isi_cr_hitung" id="isi_cr_hitung">
                			<select class="form-control" name="cr_hitung_bahan" id="cr_hitung_bahan">
                				<!-- <option value="0"><?= lang("isi-pakai-bahan-normal") ?></option>
                				<option value="1"><?= lang("isi-pakai-bahan-dimensi") ?></option>
                				<option value="2"><?= lang("isi-pakai-bahan-order") ?></option> -->
                			</select>
                		</div>
                	</div>

                	<hr>

                	<div class="form-group" id="inputan_hitung_normal">
						<label class="control-label col-sm-3" for="jml_bahan"><?= lang("capt-jml-bahan") ?></label>
						<div class="col-sm-4">
							<input type="text" name="jml_bahan" id="jml_bahan" class="form-control" />
						</div>
					</div>		

					<div class="form-group" id="inputan_hitung_dimensi">
						<label class="control-label col-sm-3" for="p_bahan"><?= lang("capt-ukuran-bahan") ?></label>
						<div class="col-sm-2 ">
							<div class="input-group">
								<span class="input-group-addon"><strong>P</strong></span>
								<input type="text" name="p_bahan" id="p_bahan" class="form-control">
							</div>
						</div>
						<div class="col-sm-2">
							<div class="input-group">
								<span class="input-group-addon"><strong>L</strong></span>
								<input type="text" name="l_bahan" id="l_bahan" class="form-control">
							</div>
						</div>

					</div>		

					<div class="form-group" id="inputan_hitung_satuan">
						<label class="control-label col-sm-3" for="satuan_bahan"><?= lang("capt-satuan") ?></label>
						<div class="col-sm-4">
							<input type="hidden" name="isi_satuan_bahan" id="isi_satuan_bahan">
							<select name="satuan_bahan" id="satuan_bahan" class="form-control">
								<option></option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-3" for="ket_bahan"><?= lang("capt-ket") ?></label>
						<div class="col-sm-4">
							<textarea name="ket_bahan" id="ket_bahan" class="form-control" ></textarea>
						</div>
					</div>	

					

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-4">
							<button type="button" name="pakai_bahan" id="pakai_bahan" class="btn btn-success" <?= $disable ?> >
								<span class="fa fa-plus"></span> <?= lang("btn-insert_daftar") ?>
							</button>
							<?= lang("or") ?> <a href="#batal_insert" id="btl_insert_bahan"><?= lang("btn-batal") ?></a>


						</div>
					</div>	

					

					<div class="table-responsive">
						<table class="table table-bordered" id="daftar_bahan">
							<thead>
								<tr class="success">
									<th><?= lang("capt-jenis-bahan") ?></th>
									<th><?= lang("capt-nm-bahan") ?></th>
									<th><?= lang("capt-cr-pakai-bahan") ?></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php
										if(isset($dt['data_bahan']) && is_array($dt['data_bahan']) && count($dt['data_bahan'])):
											foreach ($dt['data_bahan'] as $key => $isi) :
								?>
									<tr>
										<td>
											<input type='hidden' name = 'dft_bahan_iddetail[]' 	value="<?= $isi['dft_bahan_iddetail'] ?>" >
											<input type='hidden' name = 'dft_bahan_kategori[]' 	value="<?= $isi['dft_bahan_kategori'] ?>" >
											<input type='hidden' name = 'dft_bahan_jenis[]' 	value="<?= $isi['dft_bahan_jenis'] ?>" >
											<input type='hidden' name = 'dft_bahan_id[]' 		value="<?= $isi['dft_bahan_id'] ?>" >
											<input type='hidden' name = 'dft_bahan_crhitung[]' 	value="<?= $isi['dft_bahan_crhitung'] ?>" >
											<input type='hidden' name = 'dft_bahan_ket[]' 		value="<?= $isi['dft_bahan_ket'] ?>" >
											<input type='hidden' name = 'dft_bahan_jml[]' 		value="<?= $isi['dft_bahan_jml'] ?>" >
											<input type='hidden' name = 'dft_bahan_p[]' 		value="<?= $isi['dft_bahan_p'] ?>" >
											<input type='hidden' name = 'dft_bahan_l[]' 		value="<?= $isi['dft_bahan_l'] ?>" >
											<input type='hidden' name = 'dft_bahan_satuan[]' 	value="<?= $isi['dft_bahan_satuan'] ?>" >
											<input type='hidden' name = 'dft_bahan_nm_jenis[]'	value="<?= $isi['dft_bahan_nm_jenis'] ?>" >
											<input type='hidden' name = 'dft_bahan_nm_bahan[]'	value="<?= $isi['dft_bahan_nm_bahan'] ?>" >
											<input type='hidden' name = 'dft_bahan_nm_cr[]'		value="<?= $isi['dft_bahan_nm_cr'] ?>" >
											<?= strtoupper($isi['dft_bahan_nm_jenis']) ?>
										</td>
										<td><?= strtoupper($isi['dft_bahan_nm_bahan']) ?></td>
										<td><?= strtoupper($isi['dft_bahan_nm_cr']) ?></td>
										<td>
											<a href='#del_daftar_bahan' onclick='remove_item(this)'><span class='fa fa-remove text-red'></span></a>
											<a href='#view_daftar_bahan' onclick='view_item(this)'><span class='fa fa-folder text-black'></span></a>
										</td>
									</tr>
								<?php
											endforeach;
										endif;
								?>
							</tbody>
						</table>
					</div>

				</div><!-- akhir id="bahanbaku" -->
			</div><!-- akhir class="tab-content" -->
		</div><!-- akhir class="nav-tabs-custom" -->

		<div class="form-group">
			
			<div class="col-sm-offset-2 col-sm-4">
				
				<button class="btn btn-primary" type="submit" name="simpan" <?= $disable ?> >
					<?= lang('btn-save-finishing') ?>
				</button>
				<?php
	                echo lang('or') . ' ' . anchor('bom/finishing/cancel', lang('btn-batal'));
	            ?>

			</div>

		</div>

	</div>

	<?= form_close(); ?>
</div>

