<?php 
   
    $ENABLE_ADD     = has_permission('Finishing.Add'); 
    $ENABLE_MANAGE  = has_permission('Finishing.Manage'); 
    $ENABLE_DELETE  = has_permission('Finishing.Delete'); 
   
?> 

<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array("name"=>"frm_index", "id"=>"frm_index","role"=>"form")) ?>
	
	<div class="box-header">
		<?php if($ENABLE_ADD): ?>
			<a href="<?= site_url("bom/finishing/create") ?>" class="btn btn-success" title="<?= lang("btn_create") ?>" ><?= lang("btn_create") ?></a>
		<?php endif; ?>

		<div class="pull-right form-inline">
			
			<div class="form-group">
				
				<select name="kategori" id="kategori" class="form-control" style="min-width: 250px">
					<option></option>
					<?php 
							if(isset($kategori) && is_array($kategori) && count($kategori)):
								foreach ($kategori as $key => $isi):
					?>
									<option value="<?=  $isi->id ?>" <?= set_select("kategori",$isi->id,isset($kategori_search) && $kategori_search == $isi->id  ) ?> ><?= strtoupper($isi->nm) ?></option>
					<?php
								endforeach;
							endif;
					?>
				</select>
				
			</div>

			<div class="form-group">
				<div class="input-group">
					<input type="text" name="search" id="search" class="form-control" value="<?= set_value("search",isset($search)? $search : '') ?>">	
					<div class="input-group-btn">
						<button name="cari" class="btn btn-devault">
							<i class="fa fa-search"></i>
						</button>
					</div>
				</div>
			</div>

		</div>

	</div>

	<?php

		if(isset($data) && is_array($data) && count($data)):

	?>
	<div class="box-body table-responsive no-padding">
		
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
	                <th width="50">#</th>
	                <th class="text-center"> <?= lang("capt-barcode") ?> </th>
	                <th class="text-center"> <?= lang("capt-kategori") ?> </th>
	                <th > <?= lang("capt-nm-produk") ?> </th>
	                <th class="text-center"> <?= lang("capt-st-order") ?> </th>
	                <th class="text-center"> <?= lang("capt-worksheet") ?> </th>
	                <th > <?= lang("capt-ket") ?> </th>
	                <th></th>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($data as $key => $isi) :	
				?>
						<tr>
							<td>
								<input type="checkbox" name="checked[]" value="<?= $isi->id ?>">
							</td>
							<td><?= $numb ?></td>
							<td class="text-center"><?= strtoupper($isi->barcode) ?></td>
							<td class="text-center"><?= strtoupper($isi->nmkategori) ?></td>
							<td><?= strtoupper($isi->nm_produk) ?></td>
							<td class="text-center"><?= strtoupper($isi->st_min_order) ?></td>
							<td class="text-center"><?= strtoupper($isi->nm_worksheet) ?></td>
							<td><?= strtoupper($isi->ket) ?></td>
							
	                        <td style="padding-right:20px">
	                        	<?php if($ENABLE_MANAGE) : ?>
		                        	<a class="text-black" href="<?= site_url('bom/finishing/view/' . $isi->id); ?>" data-toggle="tooltip" data-placement="left" title="Edit Data"><i class="fa fa-pencil"></i></a>
	                        	<?php endif; ?>
	                        	&nbsp	
	                        	<a class="text-black" href="<?= site_url('bom/finishing/lock_view/' . $isi->id); ?>" data-toggle="tooltip" data-placement="left" title="Lihat Data"><i class="fa fa-folder"></i></a>
	                        </td>
	                        
	                        
	                        
						</tr>
				<?php
						$numb++;
					endforeach;
				?>
			</tbody>

		</table>


	</div>	

	<div class="box-footer clearfix">
		<?php if($ENABLE_DELETE) : ?>
			<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('btn-delete') ?>" onclick="return confirm('<?= lang('finishing-konfirmasi-delete'); ?>')">
		<?php endif;
			echo $this->pagination->create_links(); 
		?>
		
	</div>

	<?php else: ?>

		<div class="alert alert-info" role="alert">
        	<p><i class="fa fa-warning"></i> &nbsp; <?= lang('konfirmasi-data-tidak-ada') ?></p>
    	</div>

	<?php 

		endif;
		form_close(); 
	?>
</div>