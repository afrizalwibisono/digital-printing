<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for bom finishing

*/
class Finishing extends Admin_Controller {

	/**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Finishing.View";
    protected $addPermission    = "Finishing.Add";
    protected $managePermission = "Finishing.Manage";
    protected $deletePermission = "Finishing.Delete";

    protected $prefixKey        = "Pfsh-";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('bom/bom');
        $this->load->model(array(
        							'bahan_baku_model',
                                    'kategori_model',
                                    'produk_detail_penyusun_model',
                                    'produk_model',
                                    'worksheet_model',
                                    'jenis_model'
        						)
    						);

        $this->template->title(lang('finishing-title-index'));
		$this->template->page_icon('fa fa-list');
    }


    public function index(){

        $this->auth->restrict($this->viewPermission);

        if(isset($_POST['delete']) && has_permission($this->deletePermission)){

            if($this->delete()){

                $this->template->set_message(lang("finishing-konfirmasi-delete-sukses"),"success");

            }

        }

        if(isset($_POST['search'])){

            $search     = isset($_POST['search']) ? $this->input->post("search") : '';
            $kategori   = isset($_POST['kategori']) ? $this->input->post("kategori") : '';

        }else{

            $search     = isset($_GET['search']) ? $this->input->get("search") : '';
            $kategori   = isset($_GET['kategori']) ? $this->input->get("kategori") : '';

        }

        $filter     = "?search=".$search;
        $search     = $this->db->escape_str($search);

        $where      = "produk.deleted = 0 and produk.st_produk = 1 and (produk.barcode like '%$search%' or produk.nm_produk like '%$search%') ";

        if($kategori){

            $filter .= "&kategori=".$kategori;
            $where  .= " and produk.idkategori = $kategori";

        }

        $total  = $this->produk_model->join("kategori","produk.idkategori = kategori.idkategori","inner")
                                    ->join("worksheet","produk.id_worksheet = worksheet.id_worksheet","inner")
                                    ->where($where)
                                    ->count_all();

        $this->load->library('pagination'); //librari pagination

        $offset = $this->input->get('per_page'); // start record

        $limit = $this->config->item('list_limit'); //jml tiap halaman

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);


        $data   = $this->produk_model->select("produk.idproduk as id,
                                                `produk`.`barcode`,
                                                `produk`.`nm_produk`,
                                                `kategori`.`nmkategori`,
                                                `produk`.`ket`,
                                                IF(`produk`.`st_minimal_order` = 0,
                                                    'Tidak',
                                                    'Ya') AS st_min_order,
                                                `worksheet`.`nm_worksheet`")
                                    ->join("kategori","produk.idkategori = kategori.idkategori","inner")
                                    ->join("worksheet","produk.id_worksheet = worksheet.id_worksheet","inner")
                                    ->where($where)
                                    ->order_by("`kategori`.`nmkategori`","asc")
                                    ->order_by("nm_produk","asc")
                                    ->limit($limit,$offset)
                                    ->find_all();


        $data_kategori  = $this->kategori_model->select("`idkategori` as id, `nmkategori` as nm")
                                                ->where("deleted=0")
                                                ->order_by("nmkategori","asc")
                                                ->find_all();

        $assets     = array(
                                "plugins/select2/js/select2.js",
                                "plugins/select2/css/select2.css",
                                "bom/assets/js/index_finishing.js"
                            );

        add_assets($assets);

        $this->template->set("numb", $offset+1);
        
        $this->template->set("data",$data);
        $this->template->set("kategori",$data_kategori);
    	$this->template->set("toolbar_title", lang('finishing-title-index'));
        $this->template->set("page_title", lang('finishing-title-index'));
        $this->template->render('index_finishing'); 

    }


    public function delete(){

        if(isset($_POST['checked'])){

            $sql    = "";

            $this->db->trans_start();

                foreach ($this->input->post('checked') as $key => $id) {
                    
                    $this->produk_model->delete($id);
                    $sql    .= $this->db->last_query()."\n\n";

                }

            $this->db->trans_complete();

            if($this->db->trans_status() == false){

                $return     = false;
                $keterangan = "GAGAL delete data finishing";
                $status     = 0;

            }else{

                $return     = true;
                $keterangan = "SUKSES delete data finishing";
                $status     = 1;

            }

            $nm_hak_akses   = $this->deletePermission; 
            $kode_universal = "-";
            $jumlah         = 0;
            
            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

            return $return;

        }else{

            $this->template->set_message(lang("finishing-konfirmasi-error-pil-delete"),"error");
            return false;

        }

    }

    public function cancel(){

        $this->session->unset_userdata("frm_finishing");
        $this->template->set_message(lang('finishing-label_cancel_sukses'), 'success');

        redirect("bom/finishing");

    }

    public function create(){

        $this->auth->restrict($this->addPermission);

        if(isset($_POST['simpan'])){

            if($this->save(null)){
                
                $this->template->set_message(lang("finishing-label_simpan_sukses"),"success");
                redirect("bom/finishing");

            }

        }

        $data_sess  = $this->session->userdata("frm_finishing");
        if(!$data_sess){

            $this->set_session(1,0,null);

            $data_sess = $this->session->userdata("frm_finishing"); 

        }

        $data_kategori  = $this->kategori_model->select("`idkategori` as id, `nmkategori` as nm")
                                                ->where("deleted=0")
                                                ->order_by("nmkategori","asc")
                                                ->find_all();

        $data_jenis     = $this->jenis_model->select("`idjenis_bb` as id, `nmjenis_bb` as nm")
                                                ->where("deleted=0")
                                                ->order_by("nmjenis_bb","asc")
                                                ->find_all();                                                

        $assets     = array(
                                "plugins/select2/js/select2.js",
                                "plugins/select2/css/select2.css",
                                "plugins/number/jquery.number.js",
                                "bom/assets/js/frm_finishing.js"

                            );

        add_assets($assets);

        $this->template->set("dt",$data_sess);

        $this->template->set("kategori",$data_kategori);
        $this->template->set("jenis",$data_jenis);
        $this->template->set("toolbar_title", lang('finishing-title-new'));
        $this->template->set("page_title", lang('finishing-title-new'));
        $this->template->title(lang('finishing-title-new'));
        $this->template->page_icon('fa fa-file');
        $this->template->render('form_finishing'); 

    }

    public function lock_view($id){

        $this->auth->restrict($this->viewPermission);

        $data_sess  = $this->session->userdata("frm_finishing");
        if(!$data_sess){

            $this->set_session(0,1,$id);
            $data_sess = $this->session->userdata("frm_finishing"); 

        }

        $this->session->unset_userdata("frm_finishing");

        $data_kategori  = $this->kategori_model->select("`idkategori` as id, `nmkategori` as nm")
                                                ->where("deleted=0")
                                                ->order_by("nmkategori","asc")
                                                ->find_all();

        $data_jenis     = $this->jenis_model->select("`idjenis_bb` as id, `nmjenis_bb` as nm")
                                                ->where("deleted=0")
                                                ->order_by("nmjenis_bb","asc")
                                                ->find_all();                                                

        $assets     = array(
                                "plugins/select2/js/select2.js",
                                "plugins/select2/css/select2.css",
                                "plugins/number/jquery.number.js",
                                "bom/assets/js/frm_finishing.js"

                            );

        add_assets($assets);

        $this->template->set("dt",$data_sess);

        $this->template->set("lock",1);
        $this->template->set("kategori",$data_kategori);
        $this->template->set("jenis",$data_jenis);
        $this->template->set("toolbar_title", lang('finishing-title-view'));
        $this->template->set("page_title", lang('finishing-title-view'));
        $this->template->title(lang('finishing-title-view'));
        $this->template->page_icon('fa fa-file');
        $this->template->render('form_finishing'); 


    }

    public function view($id){

        $this->auth->restrict($this->managePermission);

        if(isset($_POST['simpan'])){

            if($this->save($id)){

                $this->template->set_message(lang("finishing-label_edit_sukses"),"success");

            }

        }

        $data_sess  = $this->session->userdata("frm_finishing");
        if(!$data_sess){

            $this->set_session(0,1,$id);
            $data_sess = $this->session->userdata("frm_finishing"); 

        }

        $this->session->unset_userdata("frm_finishing");

        $data_kategori  = $this->kategori_model->select("`idkategori` as id, `nmkategori` as nm")
                                                ->where("deleted=0")
                                                ->order_by("nmkategori","asc")
                                                ->find_all();

        $data_jenis     = $this->jenis_model->select("`idjenis_bb` as id, `nmjenis_bb` as nm")
                                                ->where("deleted=0")
                                                ->order_by("nmjenis_bb","asc")
                                                ->find_all();                                                

        $assets     = array(
                                "plugins/select2/js/select2.js",
                                "plugins/select2/css/select2.css",
                                "plugins/number/jquery.number.js",
                                "bom/assets/js/frm_finishing.js"

                            );

        add_assets($assets);

        $this->template->set("dt",$data_sess);

        $this->template->set("kategori",$data_kategori);
        $this->template->set("jenis",$data_jenis);
        $this->template->set("toolbar_title", lang('finishing-title-view'));
        $this->template->set("page_title", lang('finishing-title-view'));
        $this->template->title(lang('finishing-title-view'));
        $this->template->page_icon('fa fa-file');
        $this->template->render('form_finishing'); 


    }

    public function cek_pil_dropdown($val){

        return $val == "" ? false : true;

    }

    protected function cek_barcode($kode_barcode = "",$id = ""){

        if(strlen($kode_barcode) > 0){

            $where  = "deleted = 0 and barcode = '$kode_barcode' ";

            if(strlen($id) > 0){

                $where .= " and idproduk <> '$id'";

            }

            $data   = $this->produk_model->where($where)
                                        ->count_all();

            return $data > 0 ? false : true;

        }else{

            return true;
        }

    }

    public function save($id = null){

        

        // status minimal order dan tipe penggunaannya
        if($_POST['st_order'] == 1){

            $this->form_validation->set_rules("satuan_min","lang:capt-satuan","callback_cek_pil_dropdown");

            if($_POST['st_tipe'] == 0){

                $this->form_validation->set_rules("jml_min","lang:capt-jml-min","required");                

            }else{

                $this->form_validation->set_rules("min_p","lang:capt-ukuran-min","required");                
                $this->form_validation->set_rules("min_l","lang:capt-ukuran-min","required");                

            }

        }

        $this->form_validation->set_rules("nm_produk","lang:capt-nm-produk","required");
        $this->form_validation->set_rules("kategori","lang:capt-kategori","callback_cek_pil_dropdown");
        $this->form_validation->set_rules("isi_worksheet","lang:capt-worksheet","required");

        $this->form_validation->set_message("cek_pil_dropdown", "You should choose the {field} field");

        

        if($this->form_validation->run() === false){


            $this->set_session(0,0,null);
            Template::set_message(validation_errors(),'error');
            return false;

        }else{



            //cek barcode
            $barcode    = $this->input->post("barcode");
            if(!$this->cek_barcode($barcode, $id)){

                if(!$id) $this->set_session(0,0,null);
                
                $this->template->set_message(lang("konfirmasi-barcode-ditemukan"),"error");
                return false;

            }

            //cek inputan bahan baku
            //bahan baku yang dipakai boleh kosong
            /*if(!isset($_POST['dft_bahan_iddetail'])){

                $this->set_session(0,0,null);
                $this->template->set_message(lang("konfirmasi-bahan_kosong"),"error");
                return false;
            }*/

        }

        $barcode                = $this->input->post("barcode");
        $nm_produk              = $this->input->post("nm_produk");
        $kategori               = $this->input->post("kategori");
        $ket                    = $this->input->post("ket");
        $st_tipe                = $this->input->post("st_tipe");
        $st_order               = $this->input->post("st_order");
        $jml_min                = str_replace(",","",$this->input->post("jml_min"));
        $p_min                  = str_replace(",","",$this->input->post("min_p"));
        $l_min                  = str_replace(",","",$this->input->post("min_l"));
        $satuan_min             = $this->input->post("satuan_min");

        $dft_bahan_iddetail     = $this->input->post("dft_bahan_iddetail");
        $dft_bahan_kategori     = $this->input->post("dft_bahan_kategori");
        $dft_bahan_jenis        = $this->input->post("dft_bahan_jenis");
        $dft_bahan_id           = $this->input->post("dft_bahan_id");
        $dft_bahan_crhitung     = $this->input->post("dft_bahan_crhitung");
        $dft_bahan_ket          = $this->input->post("dft_bahan_ket");
        $dft_bahan_jml          = $this->input->post("dft_bahan_jml");
        $dft_bahan_p            = $this->input->post("dft_bahan_p");
        $dft_bahan_l            = $this->input->post("dft_bahan_l");
        $dft_bahan_satuan       = $this->input->post("dft_bahan_satuan"); 

        if(!$id){
            
            //buat array simpan header produk
            $kode_primary           = gen_primary($this->prefixKey,"produk","idproduk");

            $arr_header     = array(
                                    'idproduk'              => $kode_primary, 
                                    'barcode'               => $barcode, 
                                    'nm_produk'             => $nm_produk,  
                                    'idkategori'            => $kategori, 
                                    'st_produk'             => 1, 
                                    'st_fix'                => 0, 
                                    'ket'                   => $ket, 
                                    'st_minimal_order'      => $st_order, 
                                    'st_tipe'               => $st_tipe, 
                                    'jml_minimal'           => ($st_tipe == 0 && $st_order == 1) ? $jml_min : 0, 
                                    'id_konversi_jml'       => ($st_tipe == 0 && $st_order == 1) ? $satuan_min : 0, 
                                    'panjang_minimal'       => ($st_tipe == 1 && $st_order == 1) ? $p_min : 0, 
                                    'lebar_minimal'         => ($st_tipe == 1 && $st_order == 1) ? $l_min : 0, 
                                    'id_konversi_ukuran'    => ($st_tipe == 1 && $st_order == 1) ? $satuan_min : 0, 
                                    'id_worksheet'          => 5
                                );

            //buat array untuk detailnya
            $arr_detail     = array();

            if(isset($_POST['dft_bahan_iddetail'])){

                foreach ($dft_bahan_iddetail as $key => $value) {
                    
                    $arr_detail[]   = array(
                                            'idproduk_detail_penyusun'  => $kode_primary.$key, 
                                            'idproduk'                  => $kode_primary, 
                                            'idjenis_bb'                => $dft_bahan_jenis[$key], 
                                            'idbarang_bb'               => $dft_bahan_id[$key], 
                                            'st_tipe'                   => $dft_bahan_crhitung[$key], 
                                            'jml_pakai'                 => $dft_bahan_crhitung[$key] == 0 ? $dft_bahan_jml[$key] : 0, 
                                            'id_konversi_jml'           => $dft_bahan_crhitung[$key] == 0 ? $dft_bahan_satuan[$key] : 0, 
                                            'panjang'                   => $dft_bahan_crhitung[$key] == 1 ? $dft_bahan_p[$key] : 0, 
                                            'lebar'                     => $dft_bahan_crhitung[$key] == 1 ? $dft_bahan_l[$key] : 0, 
                                            'id_konversi_panjang'       => $dft_bahan_crhitung[$key] == 1 ? $dft_bahan_satuan[$key] : 0, 
                                            'ket'                       => $dft_bahan_ket[$key] 
                                            );


                }
            }

            $sql    = "";
            $this->db->trans_start();

                $this->produk_model->insert($arr_header);
                $sql    = $this->db->last_query();

                if(isset($_POST['dft_bahan_iddetail'])){ //jika ada bahan baku yang dipakai                
                    $this->produk_detail_penyusun_model->insert_batch($arr_detail);
                    $sql    .= "\n\n".$this->db->last_query();
                }

            $this->db->trans_complete();

            if($this->db->trans_status() == false){

                $return         = false;
                $keterangan     = "GAGAL, tambah data produk";
                $status         = 0;

            }else{

                $return         = true;
                $keterangan     = "SUKSES, tambah data produk";
                $status         = 1;

            }
            
            $nm_hak_akses   = $this->addPermission; 
            $kode_universal = $kode_primary;
            $jumlah         = 0;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

            if($return){

                $this->session->unset_userdata("frm_finishing");

            }

            return $return;

        }else{//jika edit

            $id_primary = $id;

            $arr_header             = array(
                                                'barcode'               => $barcode, 
                                                'nm_produk'             => $nm_produk,  
                                                'idkategori'            => $kategori, 
                                                'st_produk'             => 1, 
                                                'st_fix'                => 0, 
                                                'ket'                   => $ket, 
                                                'st_minimal_order'      => $st_order, 
                                                'st_tipe'               => $st_tipe, 
                                                'jml_minimal'           => ($st_tipe == 0 && $st_order == 1) ? $jml_min : 0, 
                                                'id_konversi_jml'       => ($st_tipe == 0 && $st_order == 1) ? $satuan_min : 0, 
                                                'panjang_minimal'       => ($st_tipe == 1 && $st_order == 1) ? $p_min : 0, 
                                                'lebar_minimal'         => ($st_tipe == 1 && $st_order == 1) ? $l_min : 0, 
                                                'id_konversi_ukuran'    => ($st_tipe == 1 && $st_order == 1) ? $satuan_min : 0, 
                                                'id_worksheet'          => 5
                                            );

            //buat array untuk detailnya
            $arr_detail     = array();
            $st_del_all     = false;
            $st_del_beda    = false;

            if(isset($_POST['dft_bahan_iddetail'])){

                foreach ($dft_bahan_iddetail as $key => $value) {
                    
                    if(strlen($value)<=0){ //hanya untuk yang id detailnya kosong

                        $arr_detail[]   = array(
                                                    'idproduk_detail_penyusun'  => gen_primary("ED-".$this->prefixKey,"produk","idproduk"), 
                                                    'idproduk'                  => $id_primary, 
                                                    'idjenis_bb'                => $dft_bahan_jenis[$key], 
                                                    'idbarang_bb'               => $dft_bahan_id[$key], 
                                                    'st_tipe'                   => $dft_bahan_crhitung[$key], 
                                                    'jml_pakai'                 => $dft_bahan_crhitung[$key] == 0 ? $dft_bahan_jml[$key] : 0, 
                                                    'id_konversi_jml'           => $dft_bahan_crhitung[$key] == 0 ? $dft_bahan_satuan[$key] : 0, 
                                                    'panjang'                   => $dft_bahan_crhitung[$key] == 1 ? $dft_bahan_p[$key] : 0, 
                                                    'lebar'                     => $dft_bahan_crhitung[$key] == 1 ? $dft_bahan_l[$key] : 0, 
                                                    'id_konversi_panjang'       => $dft_bahan_crhitung[$key] == 1 ? $dft_bahan_satuan[$key] : 0, 
                                                    'ket'                       => $dft_bahan_ket[$key] 
                                                );

                    }

                }



                $data_bahan     = $this->produk_detail_penyusun_model->select("`idproduk_detail_penyusun` as id")
                                                                    ->where("deleted = 0 AND idproduk = '$id_primary'")
                                                                    ->find_all();

                /* Bandingkan kategori yang ada di session dengan yang ada pada inputan 
                        Jika pada inputan berbeda dengan session maka "Hapus semua detail Bahan Baku dan Finishingnya"
                        Jika tidak berbeda maka nanti akan dikondisikan pada inputan tabel detail bahan bakunya
                */


                $where_del_all_penyusun     = "";
                $kategori_data              = $this->produk_model->select("`idkategori`")->find($id_primary);
                
                if($kategori != $kategori_data->idkategori){

                    $st_del_all     = true;
                    $where_del_all_penyusun = "idproduk = '$id_primary'";

                }else{

                    $arr_id_detail_sess  =  array();

                    foreach ($data_bahan as $key => $isi) {
                            
                        $arr_id_detail_sess[]    = $isi->id;

                    }

                    $hasil_id_del   = array_diff($arr_id_detail_sess, $dft_bahan_iddetail);
                    if(count($hasil_id_del) > 0) $st_del_beda = true;               

                }

            }

            $sql    = "";
            $this->db->trans_start();

                if($st_del_all){

                    $this->produk_detail_penyusun_model->delete_where($where_del_all_penyusun);
                    $sql    .= $this->db->last_query();

                }

                if($st_del_beda){

                    foreach ($hasil_id_del as $key => $id) {
                        
                        $this->produk_detail_penyusun_model->delete($id);
                        $sql    .= "\n\n".$this->db->last_query();

                    }

                }

                $this->produk_model->update($id_primary,$arr_header);
                $sql    .= "\n\n".$this->db->last_query();

                if(count($arr_detail)){

                    $this->produk_detail_penyusun_model->insert_batch($arr_detail);
                    $sql    .= "\n\n".$this->db->last_query();

                }

            $this->db->trans_complete();

            if($this->db->trans_status() == false){

                $return         = false;
                $keterangan     = "GAGAL, edit data produk";
                $status         = 0;

            }else{

                $return         = true;
                $keterangan     = "SUKSES, edit data produk";
                $status         = 1;

            }
            
            $nm_hak_akses   = $this->managePermission; 
            $kode_universal = $id_primary;
            $jumlah         = 0;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

            if($return){

                $this->session->unset_userdata("frm_finishing");

            }

            return $return;

        }


    }

    protected function set_session($st_new = 1, $st_view = 0,$id = null){

        if($st_new == 1 && $st_view == 0 && strlen($id)<=0){ //inialisasi kosongan

            $arr_sess   = array(
                                    "st_view"       => 0,
                                    "id_primary"    => "",
                                    "barcode"       => "",
                                    "nm_produk"     => "",
                                    "kategori"      => "",
                                    "keterangan"    => "",
                                    "isi_worksheet" => 5,
                                    "worksheet"     => "FINISHING",
                                    "st_tipe"       => 0,
                                    "st_order"      => 0,
                                    "satuan_min"    => "",
                                    "jml_min"       => 0,
                                    "p_min"         => 0,
                                    "l_min"         => 0,
                                    "data_bahan"    => array()
                                );

            $this->session->set_userdata("frm_finishing",$arr_sess);

        }else if($st_new == 0 && $st_view == 0 && strlen($id)<=0){ //diisi dari inputan form

            // inputan header

            $barcode        = $this->input->post("barcode");
            $nm_produk      = $this->input->post("nm_produk");
            $kategori       = $this->input->post("kategori");
            $ket            = $this->input->post("ket");
            $st_tipe        = $this->input->post("st_tipe");
            $st_order       = $this->input->post("st_order");
            $jml_min        = $this->input->post("jml_min");
            $p_min          = $this->input->post("min_p");
            $l_min          = $this->input->post("min_l");
            $satuan_min     = $this->input->post("satuan_min");

            $arr_detail     = array();
            // inputan detail bahan

            if(isset($_POST['dft_bahan_id'])){

                $id_detail_bahan        = $this->input->post("dft_bahan_iddetail");
                $dft_bahan_kategori     = $this->input->post("dft_bahan_kategori");
                $dft_bahan_jenis        = $this->input->post("dft_bahan_jenis");
                $dft_bahan_id           = $this->input->post("dft_bahan_id");
                $dft_bahan_crhitung     = $this->input->post("dft_bahan_crhitung");
                $dft_bahan_ket          = $this->input->post("dft_bahan_ket");
                $dft_bahan_jml          = $this->input->post("dft_bahan_jml");
                $dft_bahan_p            = $this->input->post("dft_bahan_p");
                $dft_bahan_l            = $this->input->post("dft_bahan_l");
                $dft_bahan_satuan       = $this->input->post("dft_bahan_satuan");
                $dft_bahan_nm_jenis     = $this->input->post("dft_bahan_nm_jenis");
                $dft_bahan_nm_bahan     = $this->input->post("dft_bahan_nm_bahan");
                $dft_bahan_nm_cr        = $this->input->post("dft_bahan_nm_cr");

                

                foreach ($id_detail_bahan as $key => $isi) {
                    
                    $arr_detail[]   = array(
                                                "dft_bahan_iddetail"    => $isi,
                                                "dft_bahan_kategori"    => $dft_bahan_kategori[$key],
                                                "dft_bahan_jenis"       => $dft_bahan_jenis[$key],
                                                "dft_bahan_id"          => $dft_bahan_id[$key],
                                                "dft_bahan_crhitung"    => $dft_bahan_crhitung[$key],
                                                "dft_bahan_ket"         => $dft_bahan_ket[$key],
                                                "dft_bahan_jml"         => $dft_bahan_jml[$key],
                                                "dft_bahan_p"           => $dft_bahan_p[$key],
                                                "dft_bahan_l"           => $dft_bahan_l[$key],
                                                "dft_bahan_satuan"      => $dft_bahan_satuan[$key],
                                                "dft_bahan_nm_jenis"    => $dft_bahan_nm_jenis[$key],
                                                "dft_bahan_nm_bahan"    => $dft_bahan_nm_bahan[$key],
                                                "dft_bahan_nm_cr"       => $dft_bahan_nm_cr[$key]

                                            );

                }

            }

            // susun semua arraynya
            $arr_sess   = array(
                                    "st_view"       => 0,
                                    "id_primary"    => "",
                                    "barcode"       => $barcode,
                                    "nm_produk"     => $nm_produk,
                                    "kategori"      => $kategori,
                                    "keterangan"    => $ket,
                                    "isi_worksheet" => 5,
                                    "worksheet"     => "FINISHING",
                                    "st_tipe"       => $st_tipe,
                                    "st_order"      => $st_order,
                                    "satuan_min"    => $satuan_min,
                                    "jml_min"       => $jml_min,
                                    "p_min"         => $p_min,
                                    "l_min"         => $l_min,
                                    "data_bahan"    => $arr_detail

                                );

            $this->session->set_userdata("frm_finishing", $arr_sess);



        }else if($st_new == 0 && $st_view == 1 && strlen($id)>0){ //diisi dari database

            /*$where  = "produk.deleted = 0 AND st_produk = 1 AND produk.idproduk = '$id' and produk_detail_penyusun.deleted = 0";

            $data   = $this->produk_model->select("`produk`.`idproduk`,
                                                    `produk`.`barcode`,
                                                    `produk`.`nm_produk`,
                                                    `produk`.`idkategori`,
                                                    `produk`.`ket`,
                                                    `produk`.`st_minimal_order`,
                                                    `produk`.`st_tipe` as st_pakai,
                                                    `produk`.`jml_minimal`,
                                                    `produk`.`id_konversi_jml`,
                                                    `produk`.`panjang_minimal`,
                                                    `produk`.`lebar_minimal`,
                                                    `produk`.`id_konversi_ukuran`,
                                                    `produk`.`id_worksheet`,
                                                    worksheet.nm_worksheet,
                                                    `produk_detail_penyusun`.`idproduk_detail_penyusun`,
                                                    `produk_detail_penyusun`.`idjenis_bb`,
                                                    `barang`.`idkategori` as bahan_kategori,
                                                    `produk_detail_penyusun`.`idbarang_bb`,
                                                    `produk_detail_penyusun`.`st_tipe`,
                                                    `produk_detail_penyusun`.`jml_pakai`,
                                                    `produk_detail_penyusun`.`id_konversi_jml` as bahan_satuan_jml,
                                                    `produk_detail_penyusun`.`panjang`,
                                                    `produk_detail_penyusun`.`lebar`,
                                                    `produk_detail_penyusun`.`id_konversi_panjang` as bahan_satuan_panjang,
                                                    `produk_detail_penyusun`.`ket` as bahan_ket,
                                                    `jenis_bb`.`nmjenis_bb`,
                                                    `barang`.`nm_barang`,
                                                    IF(`produk_detail_penyusun`.`st_tipe` = 0,
                                                        'Berdasarkan Jumlah (PCS, BOX, ROLL, dll)',
                                                        IF(`produk_detail_penyusun`.`st_tipe` = 1,
                                                            'Berdasarkan Dimensi P x L',
                                                            'Sesuaikan Dengan Order')) AS nm_sttipe")
                                        ->join("worksheet","produk.id_worksheet = worksheet.id_worksheet","inner")
                                        ->join("produk_detail_penyusun","produk.idproduk = produk_detail_penyusun.idproduk","inner")
                                        ->join("barang","produk_detail_penyusun.idbarang_bb = barang.idbarang_bb","inner")
                                        ->join("jenis_bb","produk_detail_penyusun.idjenis_bb = jenis_bb.idjenis_bb","inner") 
                                        ->where($where)
                                        ->group_by("`barang`.`nm_barang`","asc")
                                        ->find_all();
*/
            $dt_head    = $this->produk_model->select("
                                                        `produk`.`idproduk`,
                                                        `produk`.`barcode`,
                                                        `produk`.`nm_produk`,
                                                        `produk`.`idkategori`,
                                                        `produk`.`ket`,
                                                        `produk`.`st_minimal_order`,
                                                        `produk`.`st_tipe` AS `st_pakai`,
                                                        `produk`.`jml_minimal`,
                                                        `produk`.`id_konversi_jml`,
                                                        `produk`.`panjang_minimal`,
                                                        `produk`.`lebar_minimal`,
                                                        `produk`.`id_konversi_ukuran`,
                                                        `produk`.`id_worksheet`,
                                                        `worksheet`.`nm_worksheet`
                                                        ")
                                            ->join("`worksheet`","`produk`.`id_worksheet` = `worksheet`.`id_worksheet`","inner")
                                            ->find($id);


            $where  = "produk_detail_penyusun.idproduk = '$id' and produk_detail_penyusun.deleted = 0";

            $dt_detail  = $this->produk_detail_penyusun_model
                                ->select("
                                            `produk_detail_penyusun`.`idproduk_detail_penyusun`,
                                            `produk_detail_penyusun`.`idjenis_bb`,
                                            `barang`.`idkategori` AS `bahan_kategori`,
                                            `produk_detail_penyusun`.`idbarang_bb`,
                                            `produk_detail_penyusun`.`st_tipe`,
                                            `produk_detail_penyusun`.`jml_pakai`,
                                            `produk_detail_penyusun`.`id_konversi_jml` AS `bahan_satuan_jml`,
                                            `produk_detail_penyusun`.`panjang`,
                                            `produk_detail_penyusun`.`lebar`,
                                            `produk_detail_penyusun`.`id_konversi_panjang` AS `bahan_satuan_panjang`,
                                            `produk_detail_penyusun`.`ket` AS `bahan_ket`,
                                            `jenis_bb`.`nmjenis_bb`,
                                            `barang`.`nm_barang`,
                                            IF(`produk_detail_penyusun`.`st_tipe` = 0,
                                                'Berdasarkan Jumlah (PCS, `BOX`, `ROLL`, dll)',
                                                IF(`produk_detail_penyusun`.`st_tipe` = 1,
                                                    'Berdasarkan Dimensi P x L',
                                                    'Sesuaikan Dengan Order')) AS nm_sttipe
                                        ")
                                ->join("`barang`","`produk_detail_penyusun`.`idbarang_bb` = `barang`.`idbarang_bb`","inner")
                                ->join("`jenis_bb`","`produk_detail_penyusun`.`idjenis_bb` = `jenis_bb`.`idjenis_bb`","inner")
                                ->where($where)
                                ->order_by("`barang`.`nm_barang`")
                                ->find_all();


            $arr_detail     = array();

            if(is_array($dt_detail) && count($dt_detail)){

                foreach ($dt_detail as $key => $isi) {
                    
                    //buat array detail bahan baku penyusun
                    $arr_detail[]   = array(

                                                "dft_bahan_iddetail"    => $isi->idproduk_detail_penyusun,
                                                "dft_bahan_kategori"    => $isi->bahan_kategori,
                                                "dft_bahan_jenis"       => $isi->idjenis_bb,
                                                "dft_bahan_id"          => $isi->idbarang_bb,
                                                "dft_bahan_crhitung"    => $isi->st_tipe,
                                                "dft_bahan_ket"         => $isi->bahan_ket,
                                                "dft_bahan_jml"         => $isi->jml_pakai,
                                                "dft_bahan_p"           => $isi->panjang,
                                                "dft_bahan_l"           => $isi->lebar,
                                                "dft_bahan_satuan"      => $isi->st_tipe == 0 ? $isi->bahan_satuan_jml : $isi->bahan_satuan_panjang,
                                                "dft_bahan_nm_jenis"    => $isi->nmjenis_bb,
                                                "dft_bahan_nm_bahan"    => $isi->nm_barang,
                                                "dft_bahan_nm_cr"       => $isi->nm_sttipe


                                            );


                }
            }

            //buar array sessionnya

            $arr_sess   = array(
                                    "st_view"       => 1,
                                    "id_primary"    => $dt_head->idproduk,
                                    "barcode"       => $dt_head->barcode,
                                    "nm_produk"     => $dt_head->nm_produk,
                                    "kategori"      => $dt_head->idkategori,
                                    "keterangan"    => $dt_head->ket,
                                    "isi_worksheet" => $dt_head->id_worksheet,
                                    "worksheet"     => $dt_head->nm_worksheet,
                                    "st_tipe"       => $dt_head->st_pakai,
                                    "st_order"      => $dt_head->st_minimal_order,
                                    "satuan_min"    => $dt_head->st_pakai == 0 ? $dt_head->id_konversi_jml : $dt_head->id_konversi_ukuran,
                                    "jml_min"       => $dt_head->jml_minimal,
                                    "p_min"         => $dt_head->panjang_minimal,
                                    "l_min"         => $dt_head->lebar_minimal,
                                    "data_bahan"    => $arr_detail

                                );

            $this->session->set_userdata("frm_finishing", $arr_sess);

        }    

    }


    function get_satuan_bahan(){

        if(!$this->input->is_ajax_request()){

            redirect('bom/finishing');

        } 
        
        $id_satuan_terkecil     = $this->input->post("id_satuan");
        $st_tipe                = $this->input->post("st_pakai");

        if($st_tipe == 0){
        
            $data_satuan            = get_konversi($id_satuan_terkecil,"",TRUE);
        
        }else{

            $data_satuan            = get_konversi_by_tipe(1,null);                

        }

        echo json_encode($data_satuan);

    }

    function get_satuan_minimal_order(){

        if(!$this->input->is_ajax_request()){

            redirect('bom/finishing');

        }

        $st     = $this->input->post("st");

        if($st == 0){

            $satuan     = get_konversi_by_tipe(0,null);

        }else{

            $satuan     = get_konversi_by_tipe(1,null);    

        }


        echo json_encode($satuan);

    }

    function get_kategori_bahan_baku(){

        if(!$this->input->is_ajax_request()){

            redirect('bom/finishing');

        }

        $id_kategori    = $this->input->post('id_kategori');
        $where          = "deleted = 0";

        if($id_kategori!=5){

            $where .= " and idkategori = 5 or idkategori = $id_kategori";

        }

        $data   = $this->kategori_model->select("`idkategori` as id, ucase(`nmkategori`) as nm")
                                        ->where($where)
                                        ->order_by("nmkategori","asc")
                                        ->find_all();

        echo json_encode($data);


    }

    function get_bahan_baku(){

        $idjenis        = $this->input->post("idjenis");
        $idkategori     = $this->input->post("idkategori");
        $where          = "deleted = 0 and `idkategori` = $idkategori AND `idjenis_bb` = $idjenis";

        $data   = $this->bahan_baku_model->select("`idbarang_bb` as id, ucase(`nm_barang`) as nm, `id_satuan_terkecil`, `st_potong_meteran` as st_potongan")
                                        ->where($where)
                                        ->order_by("nm_barang","asc")
                                        ->find_all();

        echo json_encode($data);

    }


}	