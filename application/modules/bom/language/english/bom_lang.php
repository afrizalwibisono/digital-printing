<?php defined('BASEPATH')|| exit('No direct script access allowed');


// ============= Title  ==============================================================
$lang['finishing-title-index'] 					= "Data Finishing";
$lang['finishing-title-new'] 					= "Data Finishing Baru";
$lang['finishing-title-view'] 					= "Data Finishing Edit";

$lang['produk-title-index'] 					= "Data Produk";
$lang['produk-title-new'] 						= "Data Produk Baru";
$lang['produk-title-view'] 						= "Data Produk Edit";

// ====================================================================================


// ====================================================================================
$lang['btn_create']								= 'Baru';
$lang['btn_delete']								= 'Hapus';

// ====================================================================================
$lang['capt-st-worksheet']						= 'Worksheet';
$lang['capt-barcode']							= 'Barcode';
$lang['capt-nm-produk']							= 'Nama Produk';
$lang['capt-kategori']							= 'Kategori';
$lang['capt-ket']								= 'Keterangan';
$lang['capt-st-tipe']							= 'Metode Hitung Order';
$lang['capt-st-order']							= 'Minimal Order';
$lang['capt-worksheet']							= 'Worksheet';
$lang['capt-jml-min']							= 'Jml Min Order';
$lang['capt-jml-bahan']							= 'Jml Pakai';
$lang['capt-ukuran-min']						= 'Ukuran Min Order';
$lang['capt-ukuran-bahan']						= 'Ukuran Pakai';
$lang['capt-satuan']							= 'Satuan';
$lang['capt-kategori-bahan']					= 'Kategori Bahan Baku';
$lang['capt-jenis-bahan']						= 'Jenis Bahan Baku';
$lang['capt-nm-bahan']							= 'Bahan Baku';
$lang['capt-target-cetak']						= 'Status Bahan Baku';
$lang['capt-cr-pakai-bahan']					= 'Cara Hitung Pemakaian';
$lang['capt-finishing-pakai']					= 'Finishing';
$lang['capt-finishing-set']						= 'SET';

$lang['capt-tabs-bahanbaku']					= 'Bahan Baku Penyusun';
$lang['capt-tabs-finishing']					= 'Finishing Penyusun';

$lang['isi-target-cetak-cetak']					= 'Media Cetak A3';
$lang['isi-target-cetak-normal']				= 'Bukan Media Cetak A3';

$lang['isi-pakai-bahan-normal']					= 'Berdasarkan Jumlah (PCS, BOX, ROLL, dll)';
$lang['isi-pakai-bahan-dimensi']				= 'Berdasarkan Dimensi P x L';
$lang['isi-pakai-bahan-order']					= 'Sesuaikan Dengan Order';

$lang['isi-st-order-pakai']						= 'Menggunakan Minimal Order';
$lang['isi-st-order-tidak']						= 'Tanpa Minimal Order';

$lang['isi-st-tipe-normal']						= 'Berdasarkan Jumlah';
$lang['isi-st-tipe-dimensi']					= 'Berdasarkan Ukuran Cetak';

// ====================================================================================
$lang['btn-save-finishing']						= "Simpan Semua Data Finishing";
$lang['btn-save-produk']						= "Simpan Semua Data Produk";
$lang['btn-batal']								= "Batal";
$lang['btn-delete']								= "Hapus";
$lang['btn-insert_daftar']						= "Tambahkan";


$lang['or']										= "atau";
// =====================================================================================
$lang['plc-wajib-isi']							= "Wajib Diisi";
 
// =====================================================================================
$lang['finishing-label_simpan_sukses']			= "Simpan Data Finishing baru sukses";
$lang['finishing-label_cancel_sukses']			= "Data Finishing telah berhasil dibatalkan";
$lang['finishing-label_edit_sukses'] 			= "Edit Data Finishing baru sukses";

$lang['produk-label_simpan_sukses']				= "Simpan Data Produk baru sukses";
$lang['produk-label_cancel_sukses']				= "Data Produk telah berhasil dibatalkan";
$lang['produk-label_edit_sukses'] 				= "Edit Data Produk baru sukses";

// ======================================================================================
$lang['konfirmasi-data-tidak-ada']				= "Data tidak ditemukan";
$lang['konfirmasi-barcode-ditemukan']			= "Barcode yang sama telah terdaftar dalam system";
$lang['konfirmasi-bahan_kosong']				= "Minimal ada 1(satu) Bahan baku penyusun Produk";

$lang['finishing-konfirmasi-delete']			= "Apakah anda yakin akan menghapus Data Finishing terpilih ?";
$lang['finishing-konfirmasi-error-pil-delete']	= "Tidak ada Data Finishing yang dipilih";
$lang['finishing-konfirmasi-delete-sukses']		= "Delete Data Finishing sukses";

$lang['produk-konfirmasi-delete']				= "Apakah anda yakin akan menghapus Data Produk terpilih ?";
$lang['produk-konfirmasi-error-pil-delete']		= "Tidak ada Data Produk yang dipilih";
$lang['produk-konfirmasi-delete-sukses']		= "Delete Data Produk sukses";



