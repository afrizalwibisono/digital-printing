<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for Kedatangan Barang
 */

class Kedatangan_barang extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Kedatangan Barang.View";
    protected $addPermission    = "Kedatangan Barang.Add";
    protected $managePermission = "Kedatangan Barang.Manage";
    protected $deletePermission = "Kedatangan Barang.Delete";
    protected $laporanPermission = "Laporan.Kedatangan Barang.View";

    protected $prefixKey        = "FKB";
    protected $prefixKeyStok    = "STK";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('kedatangan_barang/kedatangan_barang');
        $this->load->model(array('kedatangan_barang/kedatangan_barang_model',
                                'kedatangan_barang/kedatangan_detail_model',
                                'bahan_baku/bahan_baku_model',
                                'stok_model',
                                'identitas_model',
                                'kedatangan_barang/gudang_model',
                                'kedatangan_barang/supplier_model',
                                'kedatangan_barang/pembelian_model'
                            ));

        $this->template->title(lang('kb_title_manage'));
		$this->template->page_icon('fa fa-truck');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if (isset($_POST['delete']) && has_permission($this->deletePermission))
        {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                $this->db->trans_start();
                foreach ($checked as $pid)
                {
                    //Cek apakah masih bisa dihapus
                    $cek         = $this->cek_hapus($pid);
                    $query_hapus = "";
                    $total       = 0;
                    if($cek)
                    {
                        $kedatangan       = $this->kedatangan_barang_model->find($pid);

                        $dt_supplier      = $this->supplier_model->find($kedatangan->id_supplier);
                        $data_stok_datang = $this->stok_model->find_all_by(['no_faktur' => $pid, 'deleted' => 0]);
                        foreach ($data_stok_datang as $key => $val) {
                            //Save Alur Stok
                            simpan_alur_stok($kedatangan->id_gudang, $val->id_barang, $val->stok_datang, $val->hpp, 0, 'Penghapusan Kedatangan Barang dari supplier "'.$dt_supplier->nm_supplier.'" dengan kode faktur: '.$kedatangan->no_nota, $key+1);
                        }
                        //Hapus Stok
                        $this->stok_model->delete_where(array('no_faktur' => $pid));
                        $query_hapus .= $this->db->last_query();

                        $result      = $this->kedatangan_barang_model->delete($pid);
                        $query_hapus .= $this->db->last_query();

                        if($result)
                        {
                            $keterangan = "SUKSES, hapus data kedatangan barang dengan No. Nota /Faktur : ".$kedatangan->no_nota;
                            $status     = 1;
                        }
                        else
                        {
                            $keterangan = "GAGAL, hapus data kedatangan barang dengan No. Nota /Faktur : ".$kedatangan->no_nota;
                            $status     = 0;
                        } 
                    }
                    else
                    {
                        $keterangan = "GAGAL, hapus data kedatangan barang dengan ID : ".$pid." karena telah dilakukan pembelian";
                        $status     = 0;
                    }

                    $nm_hak_akses   = $this->deletePermission; 
                    $kode_universal = $pid;
                    $jumlah         = $kedatangan->gtotal;
                    $sql            = $query_hapus;

                    $hasil = simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                }

                $this->db->trans_complete();

                if ($result)
                {
                    $this->template->set_message(count($checked) .' '. lang('kb_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('kb_del_failure') . (isset($err) ? $err." " : "") . $this->kedatangan_barang_model->error, 'error');
                }
            }
            else
            {
                $this->template->set_message(lang('kb_del_error').$this->kedatangan_barang_model->error, 'error');
            }

            unset($_POST['delete']);
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search     = isset($_POST['table_search'])?$this->input->post('table_search'):'';
            $tgl_awal   = isset($_POST['tgl_awal']) ? date_ymd($this->input->post('tgl_awal')) : '';
            $tgl_akhir  = isset($_POST['tgl_awal']) ? date_ymd($this->input->post('tgl_akhir')) : '';
        }
        else
        {
            $search     = isset($_GET['search'])?$this->input->get('search'):'';
            $tgl_awal   = isset($_GET['tgl_awal']) ? date_ymd($this->input->get('tgl_awal')) : '';
            $tgl_akhir  = isset($_GET['tgl_awal']) ? date_ymd($this->input->get('tgl_akhir')) : '';
        }

        $filter = "";
        if($search!="")
        {
            $filter = "?search=".$search;
        }

        $search2 = $this->db->escape_str($search);

        $add_where = "";
        
        if($tgl_awal != '' && $tgl_akhir != '')
        {
            $filter    .= "&tgl_awal=".$tgl_awal."&tgl_akhir=".$tgl_akhir;
            $add_where .= " AND ( date(tgl_kedatangan) >='".$tgl_awal."' AND date(tgl_kedatangan) <='".$tgl_akhir."')";
        }
        else
        {
            $tgl_awal  = date('Y-m-01');
            $tgl_akhir = date('Y-m-d');
            
            $filter    .= "&tgl_awal=".$tgl_awal."&tgl_akhir=".$tgl_akhir;
            $add_where .= " AND ( date(tgl_kedatangan) >='".$tgl_awal."' AND date(tgl_kedatangan) <='".$tgl_akhir."')";
        }
        
        $where = "`kedatangan_barang.deleted` = 0 $add_where
                AND (`no_nota` LIKE '%$search2%' ESCAPE '!'
                OR `nm_supplier` LIKE '%$search2%' ESCAPE '!'
                OR `kedatangan_barang`.`ket` LIKE '%$search2%' ESCAPE '!')";
        
        $total = $this->kedatangan_barang_model
                    ->join("supplier","kedatangan_barang.id_supplier = supplier.idsupplier","left")
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->kedatangan_barang_model
                    ->select("kedatangan_barang.*, gudang.nama_gudang, nm_lengkap, nm_supplier")
                    ->join("supplier","kedatangan_barang.id_supplier = supplier.idsupplier","left")
                    ->join("gudang","kedatangan_barang.id_gudang = gudang.id_gudang","left")
                    ->join("users","kedatangan_barang.created_by = users.id_user","left")
                    ->where($where)
                    ->order_by('created_on','DESC')
                    ->limit($limit, $offset)->find_all();

        $assets = array('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'kedatangan_barang/assets/js/kedatangan_index.js'
                        );

        add_assets($assets);

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('tgl_awal', $tgl_awal);
        $this->template->set('tgl_akhir', $tgl_akhir);
        $this->template->set("toolbar_title", lang('kb_title_manage'));
        $this->template->set("page_title", lang('kb_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

    protected function cek_hapus($id_kedatangan = '')
    {
        if($id_kedatangan == '')
        {
            return FALSE;
        }
        
        $data = $this->pembelian_model->find_by(['id_kedatangan' => $id_kedatangan, 'deleted' => 0]);
        if(!$data)
        {
            return TRUE;
        }

        return FALSE;
    }

   	//Create Kedatangan Barang
   	public function create()
   	{
        $this->auth->restrict($this->addPermission);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_kedatangan_barang())
            {
              $this->template->set_message(lang("kb_create_success"), 'success');
              redirect('kedatangan_barang');
            }
        }

        //cek session data
        $data = $this->session->userdata('kedatangan');
        if(!$data)
        {
            $data = array('kedatangan' => array(
                                        'id_kedatangan'  => gen_primary($this->prefixKey),
                                        'no_nota'        => '',
                                        'id_gudang'      => '',
                                        'idsupplier'    => '',
                                        'tgl_kedatangan' => '',
                                        'items'          => array(),
                                        'ket'            => '',
                                        'gtotal'         => 0
                                        ));

            $this->session->set_userdata($data);
            $data = $this->session->userdata('kedatangan');
        }

        $gudang     = $this->gudang_model->order_by("nama_gudang","ASC")->find_all_by("deleted", 0);
        $supplier   = $this->supplier_model->order_by("nm_supplier","ASC")->find_all_by("deleted", 0);

        $this->template->set('data', $data);
        $this->template->set('gudang', $gudang);
        $this->template->set('supplier', $supplier);

        $assets = array(
                        "plugins/select2/dist/css/select2.min.css",
                        "plugins/select2/dist/js/select2.min.js",
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        "plugins/number/jquery.number.js",
                        "kedatangan_barang/assets/js/kedatangan_barang.js"
                        );

        add_assets($assets);

        $this->template->title(lang('kb_title_new'));
		$this->template->render('kedatangan_form');
   	}

    //Scan Barcode
    public function scan_barcode()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('kb_only_ajax'), 'error');
            redirect('kedatangan_barang');
        }

        $barcode = $this->input->post('barcode');

        if($barcode != "")
        {
            $data = $this->bahan_baku_model->select(array("barang.*","satuan_terkecil.alias as satuan"))
                                        ->join("satuan_terkecil","barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","left")
                                        ->find_all_by(array('barang.deleted' => 0, 'barcode' => $barcode));
            if($data)
            {
                foreach ($data as $key => $dt) {
                    $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
                }

                $return = array('type' => 'success', 'barcode' => $barcode, 'data' => $data);
            }
            else
            {
                $return = array('type' => 'error', 'barcode' => $barcode, 'data' => 0);
            }
        }
        else
        {
            $return = array('type' => 'error', 'barcode' => $barcode, 'data' => 0);
        }

        echo json_encode($return);
    }

    //View Detail
    public function view($id_kedatangan = '')
    {
        $this->auth->restrict($this->viewPermission);

        if (!$id_kedatangan)
        {
            $this->template->set_message(lang("kb_invalid_id"), 'error');
            redirect('kedatangan_barang');
        }
        
        $data  = $this->kedatangan_barang_model->select("kedatangan_barang.*, nama_gudang, nm_supplier")
                                    ->join("gudang","kedatangan_barang.id_gudang = gudang.id_gudang","left")
                                    ->join("supplier","kedatangan_barang.id_supplier = supplier.idsupplier","left")
                                    ->find_by('id_kedatangan', $id_kedatangan);

        $detail = $this->kedatangan_detail_model->select("kedatangan_detail.*, konversi_satuan.satuan_besar as satuan, barcode, nm_barang")
                                            ->join('barang','kedatangan_detail.id_barang = barang.idbarang_bb')
                                            ->join('konversi_satuan','kedatangan_detail.id_konversi = konversi_satuan.id_konversi',"left")
                                            ->order_by('nm_barang', 'ASC')
                                            ->find_all_by('id_kedatangan', $id_kedatangan);

        $this->template->set('id_kedatangan', $id_kedatangan);
        $this->template->set('data', $data);
        $this->template->set('detail', $detail);

        $this->template->title(lang('kb_title_view'));
        $this->template->render('view_kedatangan');
    }

    //Cancel Kedatangan barang
    public function cancel()
    {
        $this->auth->restrict($this->addPermission);

        $this->session->unset_userdata('kedatangan');
        $this->template->set_message(lang('kb_canceled'), 'success');

        redirect('kedatangan_barang');
    }

    public function cari_barang()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            redirect('kedatangan_barang');
        }

        $cari = trim($this->db->escape_str($this->input->post('cr_barang')));

        $data = $this->bahan_baku_model->select(array("barang.*","satuan_terkecil.alias as satuan","nm_merk_bb"))
                                    ->join("satuan_terkecil","barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","left")
                                    ->join("merk_bb","barang.idmerk_bb=merk_bb.idmerk_bb","left")
                                    ->where("`barang.deleted` = 0
                                            AND (barcode like '%$cari%' OR nm_barang like '%$cari%')")
                                    ->order_by('nm_barang', 'ASC')
                                    ->find_all();
        if($data)
        {
            foreach ($data as $key => $dt) {
                $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
            }
        }

        echo json_encode($data);
    }

   	protected function save_kedatangan_barang()
   	{
        $no_nota          = $this->input->post('no_nota');
        $id_supplier      = $this->input->post('id_supplier');
        $id_gudang        = $this->input->post('id_gudang');
        $tgl_kedatangan   = $this->input->post('tgl_kedatangan');
        $ket              = $this->input->post('ket');

        $id_barang  = $this->input->post('id_barang');
        $qty        = $this->input->post('qty');
        $konversi   = $this->input->post('konversi');
        $harga      = $this->input->post('harga');
        $bonus      = $this->input->post('bonus');

        $this->form_validation->set_rules('no_nota','lang:kb_no_nota','required|callback_cek_nota|trim');
        $this->form_validation->set_rules('id_supplier','lang:kb_supplier','trim');
        $this->form_validation->set_rules('id_gudang','lang:kb_gudang','callback_default_select');    
        $this->form_validation->set_rules('tgl_kedatangan','lang:kb_tgl_kedatangan','required|trim');
        $this->form_validation->set_rules('ket','lang:kb_ket','trim');
        $this->form_validation->set_message('default_select', 'You should choose the "%s" field');
        $this->form_validation->set_message('cek_nota', lang('kb_no_nota').' telah diinputkan sebelumnya');

        if ($this->form_validation->run() === FALSE) 
        {
            //Save Session
            $this->save_session();

            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        //validasi
        if(count($id_barang) == 0)
        {
            $this->template->set_message(lang('kb_no_item'), 'error');
            return FALSE;
        }

        $ero_msg = array();
        $urut = 0;
        foreach ($qty as $key => $it) {
            $urut ++;
            if($it <= 0)
            {
                $ero_msg[] = sprintf(lang('kb_qty_nol'), $urut);
            }
        }

        if($ero_msg)
        {
            //Save Session
            $this->save_session();

            $ero_msg = implode("<br>", $ero_msg);
            $this->template->set_message($ero_msg, 'error');
            return FALSE;
        }

        $data = $this->session->userdata('kedatangan');
        //Start Transaction
        $this->db->trans_begin();

        // $gtotal = $this->hitung_gtotal();
        $gtotal = 0;

        //Simpan Ke table Kedatangan barang
        $data_kedatangan = array(
                                'id_kedatangan' => $data['id_kedatangan'],
                                'no_nota'       => $no_nota, 
                                'id_supplier'   => $id_supplier, 
                                'id_gudang'     => $id_gudang,
                                'tgl_kedatangan' => date_ymd($tgl_kedatangan),
                                'gtotal'        => $gtotal,
                                'ket'           => $ket
                            );

        $this->kedatangan_barang_model->insert($data_kedatangan);
        $query_kedatangan_barang = $this->db->last_query();

        //Check sukses atau tidak
        $cek_masuk = $this->kedatangan_barang_model->find_by('id_kedatangan', $data['id_kedatangan']);

        if($cek_masuk)
        {
            //Insert Detail Kedatangan Barang
            $data_det_datang = array();
            $data_stok_datang = array();
            foreach ($id_barang as $key => $br) {
                $sub_total = 0;

                $data_det_datang[] = array('id_kedatangan_detail'   => gen_primary(),
                                        'id_kedatangan'             => $data['id_kedatangan'],
                                        'id_barang'                 => $br,
                                        'qty'                       => remove_comma($qty[$key]),
                                        'id_konversi'               => $konversi[$key],
                                        'harga'                     => 0,
                                        'subtotal'                  => $sub_total,
                                        'st_bonus'                  => $bonus[$key]
                                        );

                $dt_barang = $this->bahan_baku_model->find($br);

                $dt_real_qty = hitung_ke_satuan_kecil($konversi[$key], floatval(remove_comma($qty[$key])));
                $real_qty  = $dt_real_qty['qty'];

                $hpp = 0;

                $data_stok_datang[] = array('id_stok' => gen_primary($this->prefixKeyStok),
                                        'no_faktur'  => $data['id_kedatangan'],
                                        'id_gudang'  => $id_gudang,
                                        'id_barang'  => $br,
                                        'stok_datang' => $real_qty,
                                        'stok_update' => $real_qty,
                                        'id_satuan_terkecil' => $dt_barang->id_satuan_terkecil,
                                        'hpp'         => $hpp,
                                        'st_bonus'    => $bonus[$key]
                                        );
            }

            $this->kedatangan_detail_model->insert_batch($data_det_datang);
            $query_kedatangan_barang .= "\n\n".$this->db->last_query();

            $st_det_datang = $this->kedatangan_detail_model->count_by('id_kedatangan', $data['id_kedatangan']);
            if($st_det_datang == count($data_det_datang))
            {
                $st_det_datang = TRUE;

                $this->stok_model->insert_batch($data_stok_datang);
                $query_kedatangan_barang .= "\n\n".$this->db->last_query();
                //Cek Status
                $st_stok_datang = $this->stok_model->count_by('no_faktur', $data['id_kedatangan']);
                if($st_stok_datang == count($data_stok_datang))
                {
                    $st_stok_datang = TRUE;

                    $dt_supplier   = $this->supplier_model->find($id_supplier);
                    foreach ($data_stok_datang as $key => $val) {
                        //Save Alur Stok
                        simpan_alur_stok($id_gudang, $val['id_barang'], $val['stok_datang'], $val['hpp'], 1, 'Kedatangan Barang dari supplier "'.$dt_supplier->nm_supplier.'" dengan kode faktur: '.$no_nota, $key+1, date_ymd($tgl_kedatangan));
                    }
                }
                else
                {
                    $st_stok_datang = FALSE;
                }
            }
            else
            {
                $st_det_datang = FALSE;
            }
        }

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();

            $keterangan = "GAGAL, tambah data kedatangan barang dengan ID ".$data['id_kedatangan'];
            $status     = 0;
            
            $return = FALSE;
        }
        else
        {
            $this->db->trans_commit();

            $keterangan = "SUKSES, tambah data kedatangan barang dengan ID : ".$data['id_kedatangan'];
            $status     = 1;
            
            $return = TRUE;
        }

        //Save Log
        $nm_hak_akses   = $this->addPermission; 
        $kode_universal = $data['id_kedatangan'];
        $jumlah         = 1;
        $sql            = $query_kedatangan_barang;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        if($return)
        {
            //Clear Session
            $this->session->unset_userdata('kedatangan');
        }
        
        return $return;
   	}

    private function hitung_total($konversi = NULL,  $qty = NULL, $harga = NULL)
    {
        if(!$konversi || !$qty || !$harga)
        {
            return 0;
        }

        //Get real qty
        $dt_real_qty        = hitung_ke_satuan_kecil($konversi, floatval($qty));
        $real_qty           = $dt_real_qty['qty'];
        //harga
        $hpp                = floatval(remove_comma($harga));
        if($real_qty != $qty)
        {
            $hpp = floatval(remove_comma($harga))/$real_qty;
        }

        $total = $real_qty * $hpp;

        return $total;
    }

    private function hitung_gtotal()
    {
        $id_barang  = $this->input->post('id_barang');
        $qty        = $this->input->post('qty');
        $konversi   = $this->input->post('konversi');
        $harga      = $this->input->post('harga');

        $gtotal = 0;
        foreach ($id_barang as $key => $br) {
            //gtotal
            $gtotal     += $this->hitung_total($konversi[$key], $qty[$key], $harga[$key]);
        }

        return $gtotal;
    }

    private function save_session()
    {
        $no_nota        = $this->input->post('no_nota');
        $id_supplier    = $this->input->post('id_supplier');
        $id_gudang      = $this->input->post('id_gudang');
        $tgl_kedatangan = $this->input->post('tgl_kedatangan');
        $ket            = $this->input->post('ket');

        $id_barang  = $this->input->post('id_barang');
        $qty        = $this->input->post('qty');
        $konversi   = $this->input->post('konversi');
        $harga      = $this->input->post('harga');
        $bonus      = $this->input->post('bonus');

        $items  = array();
        $gtotal = 0;
        if($id_barang)
        {
            foreach ($id_barang as $key => $br) {
                $dt_barang = $this->bahan_baku_model->find($br);

                // $subtotal = $this->hitung_total($konversi[$key], $qty[$key], $harga[$key]);
                $subtotal = 0;

                $items[] = array(
                            'id_barang' => $br,
                            'barcode'   => $dt_barang->barcode,
                            'nm_barang' => $dt_barang->nm_barang,
                            'qty'       => remove_comma($qty[$key]),
                            'id_konversi'  => $konversi[$key],
                            'konversi'  => get_konversi($dt_barang->id_satuan_terkecil),
                            // 'harga'     => remove_comma($harga[$key]),
                            'harga'     => 0,
                            'subtotal'  => $subtotal,
                            'bonus'     => $bonus[$key]
                            );

                $gtotal += $subtotal;
            }    
        }

        $data = $this->session->userdata('kedatangan');

        $data['no_nota']     = $no_nota;
        $data['idsupplier']  = $id_supplier;
        $data['id_gudang']   = $id_gudang;
        $data['tgl_kedatangan'] = $tgl_kedatangan;
        $data['items']  = $items;
        $data['ket']    = $ket;
        $data['gtotal'] = $gtotal;

        $this->session->set_userdata('kedatangan', $data);
        $data = $this->session->userdata('kedatangan');
    }

    public function default_select($val)
    {
        return $val == "" ? FALSE : TRUE;
    }

    public function cek_nota($val){
        $cek = $this->kedatangan_barang_model->find_by(['no_nota' => $val, 'deleted' => 0]);

        return $cek ? FALSE : TRUE;
    }

    public function laporan()
    {
        $this->auth->restrict($this->laporanPermission);

        $this->form_validation->set_rules('tgl_awal', 'lang:kb_tgl_awal', 'required|trim');
        $this->form_validation->set_rules('tgl_akhir', 'lang:kb_tgl_akhir', 'required|trim');
        
        if ($this->form_validation->run() === FALSE) {
            $this->template->set_message(validation_errors(), 'error');
        }

        if(isset($_POST['table_search'])) {
            $search     = $this->input->post('table_search');
            $tgl_awal   = isset($_POST['tgl_awal']) ? date_ymd($this->input->post('tgl_awal')) : '';
            $tgl_akhir  = isset($_POST['tgl_awal']) ? date_ymd($this->input->post('tgl_akhir')) : '';
        } else {
            $search     = "";
            $tgl_awal   = "";
            $tgl_akhir  = "";
        }

        $search2 = $this->db->escape_str($search);

        $where = "kedatangan_barang.deleted = 0 AND (kedatangan_barang.created_on >='".$tgl_awal."' AND kedatangan_barang.created_on <='".$tgl_akhir."') AND nm_barang like '%$search2%'";

        $data = $this->kedatangan_barang_model->select(array("kedatangan_barang.*","users.display_name as operator"))
                                        ->join('detail_kedatangan_barang','kedatangan_barang.no_nota = detail_kedatangan_barang.no_nota')
                                        ->join('barang','detail_kedatangan_barang.id_barang = barang.id_barang')
                                        ->join('users','kedatangan_barang.created_by = users.id',"left")
                                        ->where($where)
                                        ->group_by("kedatangan_barang.no_nota")
                                        ->order_by("kedatangan_barang.created_on","DESC")
                                        ->find_all();
        if($data)
        {
            foreach ($data as $key => $dt) {
                $detail = $this->kedatangan_detail_model->select(array("detail_kedatangan_barang.*","barcode","nm_barang","satuan.singkatan as satuan"))
                                                        ->join('barang','detail_kedatangan_barang.id_barang = barang.id_barang')
                                                        ->join("satuan","detail_kedatangan_barang.id_satuan = satuan.id_satuan","left")
                                                        ->where("no_nota", $dt->no_nota)
                                                        ->order_by("nm_barang","ASC")
                                                        ->find_all();
                $data[$key]->detail = $detail;
            }
        }

        //Identitas
        $identitas = $this->identitas_model->find(1);

        Assets::add_css('plugins/datepicker/bootstrap-datepicker3.css');
        Assets::add_js('plugins/datepicker/bootstrap-datepicker.min.js');
        Assets::add_module_js('kedatangan_barang','laporan.js');

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('idt', $identitas);
        $this->template->set('period1', $this->input->post('tgl_awal'));
        $this->template->set('period2', $this->input->post('tgl_akhir'));
        $this->template->set("toolbar_title", lang('kb_laporan_title_view'));
        $this->template->set("page_title", lang('kb_laporan_title_view'));
        $this->template->render();
    }

}
?>