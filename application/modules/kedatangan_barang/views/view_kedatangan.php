<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_kedatangan_barang','name'=>'frm_kedatangan_barang','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="form-group <?= form_error('no_nota') ? ' has-error' : ''; ?>">
			    <div class="col-md-3">
			    	<label for="no_nota" class="control-label"><?= lang('kb_no_nota') ?></label>
			    	<input type="text" class="form-control" id="no_nota" name="no_nota" maxlength="100" value="<?= $data->no_nota ?>" autofocus readonly>
			    </div>
			    <div class="col-md-3">
		  			<label for="id_supplier" class="control-label"><?= lang('kb_supplier') ?></label>
		  			<input type="text" name="id_supplier" class="form-control" value="<?= $data->nm_supplier ?>" readonly>
		  		</div>
		  		<div class="col-md-3">
		  			<label for="id_gudang" class="control-label"><?= lang('kb_gudang') ?></label>	
		  			<input type="text" name="id_gudang" class="form-control" value="<?= ucwords($data->nama_gudang) ?>" readonly>
		  		</div>
		  		<div class="col-md-3">
			    	<label for="tgl_kedatangan" class="control-label"><?= lang('kb_tgl_kedatangan') ?></label>
			    	<input type="text" class="form-control" id="tgl_kedatangan" name="tgl_kedatangan" value="<?= date('d/m/Y', strtotime($data->tgl_kedatangan)) ?>" readonly>
			    </div>
		  	</div>
		  	<div class="table-responsive">
		  		<table class="table table-bordered" id="tdet_masuk">
		  			<thead>
		  				<tr class="success">
			  				<th width="50"><?= lang('kb_no') ?></th>
			  				<th><?= lang('kb_barcode') ?></th>
			  				<th><?= lang('kb_nm_barang') ?></th>
			  				<th><?= lang('kb_qty') ?></th>
			  				<th><?= lang('kb_satuan') ?></th>
			  				<th><?= lang('kb_status') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($detail) : ?>
		  					<?php foreach($detail as $key => $dt) : ?>
		  				<tr>
		  					<td><?= $key+1 ?></td>
		  					<td><?= $dt->barcode ?></td>
		  					<td><?= $dt->nm_barang ?></td>
		  					<td><?= number_format($dt->qty) ?></td>
		  					<td><?= $dt->satuan ?></td>
		  					<td><?= $dt->st_bonus == 1 ? lang('kb_bonus') : lang('kb_bukan_bonus') ?></td>
		  				</tr>
		  					<?php endforeach; ?>
		  				<?php endif; ?>
		  			</tbody>
		  		</table>
		  	</div>
		  	<div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
			    <div class="col-sm-6">
			    	<label for="ket" class="control-label"><?= lang('kb_ket') ?></label>
			    	<textarea class="form-control" id="ket" name="ket" maxlength="255" readonly=""><?= $data->ket ?></textarea>
			    </div>
		  	</div>
		  	<div class="form-group">
		  		<div class="col-md-12">
		  			<?php echo anchor('kedatangan_barang', lang('kb_btn_back'), array('class' =>'btn btn-success')) ?>
		  		</div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->