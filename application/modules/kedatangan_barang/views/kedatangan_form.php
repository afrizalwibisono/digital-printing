<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_kedatangan','name'=>'frm_kedatangan','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="form-group <?= form_error('no_nota') ? ' has-error' : ''; ?>">
			    <div class="col-md-3">
			    	<label for="no_nota" class="control-label"><?= lang('kb_no_nota') ?></label>
			    	<input type="text" class="form-control" id="no_nota" name="no_nota" maxlength="100" value="<?php echo set_value('no_nota', isset($data['no_nota']) ? $data['no_nota'] : ''); ?>" autofocus>
			    </div>
			    <div class="col-md-3">
			    	<label for="id_supplier" class="control-label"><?= lang('kb_supplier') ?></label>
			    	<select name="id_supplier" id="id_supplier" class="form-control">
			    		<option></option>
			    		<?php foreach ($supplier as $key => $sp) : ?>
			    		<option value="<?= $sp->idsupplier ?>" <?= set_select('id_supplier', $sp->idsupplier, isset($data['idsupplier']) &&  $data['idsupplier'] == $sp->idsupplier) ?>><?= ucwords($sp->nm_supplier) ?></option>
			    		<?php endforeach ?>
			    	</select>
			    </div>
			    <div class="col-md-3">
			    	<label for="id_gudang" class="control-label"><?= lang('kb_gudang') ?></label>
			    	<select name="id_gudang" id="id_gudang" class="form-control">
			    		<option></option>
			    		<?php foreach ($gudang as $key => $gd) : ?>
			    		<option value="<?= $gd->id_gudang ?>" <?= set_select('id_gudang', $gd->id_gudang, isset($data['id_gudang']) &&  $data['id_gudang'] == $gd->id_gudang) ?>><?= ucwords($gd->nama_gudang) ?></option>
			    		<?php endforeach ?>
			    	</select>
			    </div>
				<div class="col-md-3">
			    	<label for="tgl_kedatangan" class="control-label"><?= lang('kb_tgl_kedatangan') ?></label>
			    	<input type="text" class="form-control" id="tgl_kedatangan" name="tgl_kedatangan" value="<?= set_value('tgl_kedatangan', isset($data['tgl_kedatangan']) ? $data['tgl_kedatangan'] : '') ?>">
			    </div>
		  	</div>
		  	<div class="table-responsive">
		  		<table class="table table-bordered" id="t_det_kedatangan">
		  			<thead>
		  				<tr>
			  				<th width="50"><?= lang('kb_no') ?></th>
			  				<th><?= lang('kb_barcode') ?></th>
			  				<th><?= lang('kb_nm_barang') ?></th>
			  				<th><?= lang('kb_qty') ?></th>
			  				<th><?= lang('kb_satuan') ?></th>
			  				<th><?= lang('kb_status') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
	  					<?php if($data['items']) : ?>
		  					<?php 
		  						$no = 1;
		  						foreach($data['items'] as $key => $dt) : 
		  					?>
		  				<tr>
		  					<td><?= $no++ ?></td>
		  					<td>
		  						<?= $dt['barcode'] ?>
		  						<input type="hidden" name="id_barang[]" value="<?= $dt['id_barang'] ?>" />
		  					</td>
		  					<td><?= $dt['nm_barang'] ?>
		  						<br><a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a>
		  					</td>
		  					<td><input type="number" min="1" name="qty[]" class="form-control" value="<?= $dt['qty'] ?>" /></td>
		  					<td>
		  						<select class='form-control' name='konversi[]' style="width: 150px;">
		  							<?php 
		  								if($dt['konversi']) :
		  									foreach ($dt['konversi'] as $key => $kf) :
		  							?>
		  							<option value="<?= $kf->id_konversi ?>" <?= (isset($kf->id_konversi) && $kf->id_konversi == $dt['id_konversi']) ? "selected" : "" ?>><?= $kf->satuan_besar." (".$kf->ket.")" ?></option>
		  							<?php
		  									endforeach; 
		  								endif; 
		  							?>
		  						</select>
		  					</td>
		  					<td>
		  						<select name="bonus[]" class="form-control" style="width: 150px;">
		  							<option value="0" <?= $dt['bonus'] == 0 ? 'selected' : '' ?>><?= lang('kb_bukan_bonus') ?></option>
		  							<option value="1" <?= $dt['bonus'] == 1 ? 'selected' : '' ?>><?= lang('kb_bonus') ?></option>
		  						</select>
		  					</td>
		  				</tr>
		  					<?php endforeach; ?>
		  				<?php endif; ?>
		  			</tbody>
		  			<tfoot>
		  				<tr>
		  					<td colspan="6">
		  						<div class="input-group" id="barcode_input">
		  							<div class="input-group-btn">
		  								<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-barang"><i class="fa fa-plus-square"></i></button>
		  							</div>
		  							<input type="text" class="form-control" id="barcode" name="barcode" placeholder="<?= lang('kb_scan_brg') ?>" autocomplete="off" autofocus />
		  						</div>
		  						<span class="help-block text-green" id="barcode_msg"></span>
		  					</td>
		  				</tr>
		  			</tfoot>
		  		</table>
		  	</div>
		  	<div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
			    <div class="col-sm-6">
			    	<label for="ket" class="control-label"><?= lang('kb_ket') ?></label>
			    	<textarea class="form-control" id="ket" name="ket" maxlength="255"><?php echo set_value('ket', isset($data['ket']) ? $data['ket'] : ''); ?></textarea>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <div class="col-md-12">
			      	<button type="submit" name="save" class="btn btn-primary"><?= lang('kb_btn_save') ?></button>
			    	<?php
	                	echo lang('kb_atau') . ' ' . anchor('kedatangan_barang/cancel', lang('kb_btn_cancel'), array("onclick" => "return confirm('".lang('kb_cancel_confirm')."')"));
	                ?>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->
<div class="modal" id="modal-barang" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
	    <div class="modal-content">
	    	<?= form_open(site_url('kedatangan_barang/add_item'),array('id'=>'frm_barang','name'=>'frm_barang','role'=>'form')) ?>
		      	<div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        	<h4 class="modal-title"><?= lang('kb_pilih_brg') ?></h4>
		      	</div>
		      	<div class="modal-body">
		      		<div class="form-group">
		      			<div class="input-group">
		      				<input type="text" class="form-control" id="cr_barang" name="cr_barang" placeholder="<?= lang('kb_cari_brg') ?>" />
		      				<div class="input-group-btn">
		      					<button type="button" class="btn btn-primary" name="cari_btn" id="cari_btn"><i class="fa fa-search"></i> <?= lang('kb_cari_btn') ?></button>
		      				</div>
		      			</div>
		      		</div>
		        	<div class="table-responsive">
		        		<table id="tbl_barang" class="table table-hover table-striped">
		        			<thead>
		        				<tr class="success">
			        				<th><input type="checkbox" id="ck_all" name="ck_all" /></th>
			        				<th><?= lang('kb_no') ?></th>
			        				<th><?= lang('kb_barcode') ?></th>
			        				<th><?= lang('kb_nm_barang') ?></th>
			        				<th><?= lang('kb_nm_merk') ?></th>
			        			</tr>
		        			</thead>
		        			<tbody>
		        				
		        			</tbody>
		        		</table>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
		      		<div class="checkbox pull-left">
		      			<label for="set_as_bonus">
			      			<input type="checkbox" id="set_as_bonus" value="1" /> <?= lang('kb_as_bonus') ?>
			      		</label>	 
		      		</div>
		      		<button type="button" class="btn btn-primary" id="pilih_btn" name="pilih_btn"><?= lang('kb_pilih_btn') ?></button>
		      	</div>
	      	<?= form_close() ?>
	    </div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" src="<?= base_url('assets/plugins/jquery-ui/jquery-ui.js') ?>"></script>