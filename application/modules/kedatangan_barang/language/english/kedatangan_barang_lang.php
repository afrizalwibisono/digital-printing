<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['kb_title_manage'] 	= 'Kedatangan Barang';
$lang['kb_title_new'] 		= 'Kedatangan Barang Baru';
$lang['kb_title_view'] 		= 'Detail Kedatangan Barang';
$lang['kb_laporan_title_view'] = 'Laporan Kedatangan Barang';

// form/table
$lang['kb_no_nota'] 	= 'No. Nota/Faktur Surat jalan';
$lang['kb_supplier'] 	= 'Supplier';
$lang['kb_tgl_kedatangan'] = 'Tanggal Kedatangan';
$lang['kb_gudang'] 		= 'Gudang';
$lang['kb_dibuat_oleh'] = 'Dibuat Oleh';
$lang['kb_tanggal'] 	= 'Tanggal';
$lang['kb_ket'] 		= 'Keterangan';

$lang['kb_no'] 			= '#';
$lang['kb_barcode'] 	= 'Barcode';
$lang['kb_nm_barang'] 	= 'Nama Barang';
$lang['kb_nm_merk']  	= 'Merek';
$lang['kb_qty'] 		= 'Qty';
$lang['kb_harga'] 		= 'Harga';
$lang['kb_satuan'] 		= 'Satuan';
$lang['kb_status'] 		= 'Status';
$lang['kb_subtotal'] 	= 'Sub Total';
$lang['kb_gtotal'] 		= 'Grand Total';
$lang['kb_bonus'] 		= 'Bonus';
$lang['kb_bukan_bonus'] = 'Bukan Bonus';
$lang['kb_as_bonus'] 	= 'Set Sebagai Bonus';
$lang['kb_range_tgl'] 	= 'Range Tanggal';
$lang['kb_tgl_awal']  	= 'Tanggal awal';
$lang['kb_tgl_akhir'] 	= 'Tanggal akhir';


$lang['kb_ringkasan'] 		= 'Ringkasan';
$lang['kb_detail'] 			= 'Detail Transaksi';
$lang['kb_total_tran_qty'] 	= 'Transaksi';
$lang['kb_total_tran'] 		= 'Total Seluruh Transaksi';
$lang['kb_kali'] 			= 'kali';

$lang['kb_operator'] 	= 'Operator';
$lang['kb_cetak'] 		= 'Cetak';

$lang['kb_tgl_awal'] 	= 'Tanggal awal';
$lang['kb_tgl_akhir'] 	= 'Tanggal akhir';

$lang['kb_pilih_brg'] = 'Pilih Barang';
$lang['kb_scan_brg']  = 'Ketik / Scan Barcode Barang';
$lang['kb_cari_brg']  = 'Cari Barang ...';
$lang['kb_cari_btn']  = 'Cari';
$lang['kb_pilih_btn'] = 'Pilih';

// button
$lang['kb_btn_new'] 		= 'Baru';
$lang['kb_btn_delete'] 		= 'Hapus';
$lang['kb_btn_save'] 		= 'Simpan';
$lang['kb_btn_cancel'] 		= 'Batal';
$lang['kb_btn_back'] 		= 'Kembali';
$lang['kb_atau'] 			= 'atau';

// messages
$lang['kb_del_error']		= 'Anda belum memilih data yang akan dihapus.';
$lang['kb_del_failure']		= 'Tidak dapat menghapus data terpilih: ';
$lang['kb_delete_confirm']	= 'Apakah anda yakin akan menghapus data terpilih ?';
$lang['kb_cancel_confirm']	= 'Apakah anda yakin akan membatalkan kedatangan barang ?';
$lang['kb_deleted']			= 'Data Kedatangan Barang berhasil dihapus';
$lang['kb_no_records_found']  = 'Data tidak ditemukan.';

$lang['kb_create_failure'] 	= 'Kedatangan Barang baru gagal disimpan: ';
$lang['kb_create_success'] 	= 'Kedatangan Barang baru berhasil disimpan';
$lang['kb_canceled'] 		= 'Kedatangan Barang telah berhasil dibatalkan';

$lang['kb_edit_success'] 	= 'Kedatangan Barang berhasil disimpan';
$lang['kb_invalid_id'] 		= 'ID Tidak Valid';

$lang['kb_no_item'] 		= 'Anda belum menambahkan data barang yang akan dimasukkan.';
$lang['kb_qty_nol'] 		= 'Qty baris ke: %s tidak boleh kurang atau sama dengan 0 (nol).';
$lang['kb_harga_nol'] 		= 'Harga baris ke: %s tidak boleh kurang atau sama dengan 0 (nol).';

$lang['kb_barcode_found'] 	  = 'Barcode : %s ditemukan.';
$lang['kb_barcode_not_found'] = 'Barcode : %s tidak ditemukan.';

$lang['kb_only_ajax'] 		= 'Hanya ajax request yang diijinkan.';

$lang['kb_date_required'] 	= 'Silahkan pilih tanggal awal dan akhir.';