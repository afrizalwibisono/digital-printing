$(document).ready(function(){
	$("#ck_all").on("click", function(){
		if($(this).prop("checked")){
			$("input[name='item_code[]']").prop("checked", true);
		}else{
			$("input[name='item_code[]']").prop("checked", false);
		}
	});

	$("#cari_btn").on("click", function(){
		cari_barang();
	});

	$("#cr_barang").on("keydown", function(e){
		if(e.keyCode == 13){
			cari_barang();
			e.preventDefault();
		}
	});

	//Scan Barcode
	$("input[name='barcode']").on("keydown", function(e){
		if(e.keyCode == 13){
			scan_barcode();
			e.preventDefault();
		}
	});
	//Pilih Item
	$("#pilih_btn").on("click", function(){
		add_items();
	});
	//Gudang
	$("#id_gudang").select2({placeholder : '-- Pilih gudang --', allowClear : true});
	//Supplier
	$("#id_supplier").select2({placeholder : '-- Pilih supplier --', allowClear : true});
	//Date Picker
	$('#tgl_kedatangan').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});
	//Konversi
	$("select[name='konversi[]']").select2();
	//Bonus
	$("select[name='bonus[]']").select2();
});

//cari barang
function cari_barang(){
	var cari = $("#cr_barang").val();

	$.ajax({
		url : baseurl+"kedatangan_barang/cari_barang",
		type : "post",
		dataType : "json",
		data : {cr_barang : cari},
		success : function(msg){
			$("table#tbl_barang tbody tr").remove();
			if(!msg){
				var $tr = $("<tr>"
							+"<td colspan='5' class='text-center'>Data ditemukan</td>"
							+"</tr>");

				$("table#tbl_barang tbody").append($tr);
			}else{

				var opt_select = "";
				$.each(msg, function(i,n){
					$.each(n['konversi'], function(l,m){
						var selected = "";
						if(m['selected'] == 1)
						{
							selected = "selected";
						}

						opt_select += '<option value="'+m['id_konversi']+'" '+selected+'>'+m['tampil2']+'</option>';
					});

					var $tr = $("<tr data-konversi='"+opt_select+"'>"
								+"<td><input type='checkbox' name='item_code[]' value='"+n['idbarang_bb']+"'></td>"
								+"<td><input type='hidden' name='item_barcode[]' value='"+(!n['barcode'] ? '' : n['barcode'])+"' />"+(i+1)+"</td>"
								+"<td><input type='hidden' name='item_id_satuan[]' value='"+n['id_satuan_terkecil']+"' />"+(!n['barcode'] ? '' : n['barcode'])+"</td>"
								+"<td><input type='hidden' name='item_nm_barang[]' value='"+n['nm_barang']+"' />"+n['nm_barang']+"</td>"
								+"<td><input type='hidden' name='item_nm_satuan[]' value='"+n['satuan']+"' />"+n['nm_merk_bb']+"</td>"
								+"</tr>");

					$("table#tbl_barang tbody").append($tr);

					opt_select = "";
				});
				
			}
		}
	});
}

//Add item to list
function add_items(){
	var $obj_ids   = $("#tbl_barang input[name='item_code[]']:checked");

	var ids_barang = $("#tbl_barang input[name='item_code[]']:checked").map(function(){
		return $(this).val();
	}).get();

	var st_bonus  = $("#set_as_bonus").prop('checked');

	var ids_barcodes = [];
	var nms_barang = [];
	var options    = [];

	$.each($obj_ids, function(){
		ids_barcodes.push($(this).closest("tr").find("input[name='item_barcode[]']").val());
		nms_barang.push($(this).closest("tr").find("input[name='item_nm_barang[]']").val());
		options.push($(this).closest("tr").data("konversi"));
	});

	if(ids_barang.length == 0)
	{
        alertify.error('Silahkan pilih barang yang akan ditambahkan dulu');
        return false;
	}

	var pj_ids = ids_barang.length;

	for (var i = 0; i < pj_ids; i++) {
		var $row = $("<tr>"
		  					+"<td></td>"
		  					+"<td>"
		  						+ids_barcodes[i]
		  						+"<input type='hidden' name='id_barang[]' value='"+ids_barang[i]+"' />"
		  					+"</td>"
		  					+"<td>"+nms_barang[i]+"<br><a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>"
		  					+"<td><input type='number' min='1' name='qty[]' class='form-control' value='1' /></td>"
		  					+"<td><select class='form-control' name='konversi[]' style='width: 150px'>"+options[i]+"</select></td>"
		  					+"<td><select name='bonus[]' class='form-control' style='width: 150px'><option value='0' "+(st_bonus == false ? 'selected' : '')+">Bukan Bonus</option><option value='1' "+(st_bonus ? 'selected' : '')+">Bonus</option>></select></td>"
		  				+"</tr>");

		var pj = $("#t_det_kedatangan tr").length;
		var cek = false;

		if(pj > 2 && st_bonus == false){
			for (var j = 0; j <= pj - 1; j++) {
				var kode 	 = $("#t_det_kedatangan tr:eq("+(j+1)+")").find("input[name='id_barang[]']").val();
				var is_bonus = $("#t_det_kedatangan tr:eq("+(j+1)+")").find("select[name='bonus[]']").val();
				
				if(kode != "" && kode == ids_barang[i] && is_bonus == 0 ){
					var qty = parseFloat($("#t_det_kedatangan tr:eq("+(j+1)+")").find("input[name='qty[]']").val());

					$("#t_det_kedatangan tr:eq("+(j+1)+")").find("input[name='qty[]']").val(qty+1).trigger("change");
					
					cek = true;
					break;
				}

			}
			
		}

		if(cek == false){
			$("#t_det_kedatangan tbody").append($row);
		}
	}
	
	$("#modal-barang").modal("hide");
	$("#tbl_barang input[type='checkbox']").prop('checked', false);
	$("#set_as_bonus").prop('checked', false);	
	//Konversi
	$("select[name='konversi[]']").select2();
	//Bonus
	$("select[name='bonus[]']").select2();

	//Buat Nomor
	buat_no();
}

//buat nomor
function buat_no(){
	$.each($("#t_det_kedatangan tbody tr"), function(i){
		$(this).find("td:first").text(i+1);
	});
}

//Scan Barcoce
function scan_barcode(){
	var barcode = $("#barcode").val().trim();

	if(!barcode){
		return false;
	}

	$.ajax({
		url : baseurl+"kedatangan_barang/scan_barcode",
		type : "post",
		dataType : "json",
		data : {barcode : barcode, type: 0},
		success : function(msg){
			//clear barcode
			$("#barcode").val("");

			if(msg['type'] == 'success'){ //Sukses
				$("#barcode_input").removeClass("has-error").addClass("has-success");
				$("#barcode_msg").removeClass("has-red").addClass("text-green");
				$("#barcode_msg").text("Barcode : "+msg['barcode']+" ditemukan.");

				var addToList = function(){
					var pj = $("#t_det_kedatangan tr").length;
					var cek = false;
					//Cek jika item sudah ada pada daftar
					if(pj > 2){
						for (var j = 0; j <= pj - 1; j++) {
							var kode 	 = $("#t_det_kedatangan tr:eq("+(j+1)+")").find("input[name='id_barang[]']").val();
							var st_bonus = $("#t_det_kedatangan tr:eq("+(j+1)+")").find("select[name='bonus[]']").val();

							if(kode != "" && kode == msg['data'][0]['idbarang_bb'] && st_bonus == 0){
								var qty = parseFloat($("#t_det_kedatangan tr:eq("+(j+1)+")").find("input[name='qty[]']").val());

								$("#t_det_kedatangan tr:eq("+(j+1)+")").find("input[name='qty[]']").val(qty+1).trigger("change"); //Trigger Change
								
								cek = true;
								break;
							}

						}
						
					}

					if(cek == false){
						var opt_select = "";

						$.each(msg['data'][0]['konversi'], function(i,n){
							var selected = "";
							if(n['selected'] == 1)
							{
								selected = "selected";
							}

							opt_select += '<option value="'+n['id_konversi']+'" '+selected+' data-jmlkecil="'+n['jml_kecil']+'">'+n['tampil2']+'</option>';
						});

						var $row = $("<tr>"
				  					+"<td></td>"
				  					+"<td>"
				  						+msg['data'][0]['barcode']
				  						+"<input type='hidden' name='id_barang[]' value='"+msg['data'][0]['idbarang_bb']+"' />"
				  					+"</td>"
				  					+"<td>"+msg['data'][0]['nm_barang']+"<br><a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>"
				  					+"<td><input type='number' min='1' name='qty[]' class='form-control' value='1' /></td>"
				  					+"<td><select class='form-control' name='konversi[]' style='width: 150px'>"+opt_select+"</select></td>"
				  					+"<td><select name='bonus[]' class='form-control' style='width: 150px'><option value='0' selected=''>Bukan Bonus</option><option value='1'>Bonus</option>></select></td>"
				  				+"</tr>");

						$("#t_det_kedatangan tbody").append($row);

						buat_no();

						//Konversi
						$("select[name='konversi[]']").select2();
						//Bonus
						$("select[name='bonus[]']").select2();
					}
				}

				addToList();
				
			}else if(msg['type'] == 'error'){ //gagal
				$("#barcode_input").removeClass("has-success").addClass("has-error");
				$("#barcode_msg").removeClass("text-green").addClass("text-red");
				$("#barcode_msg").text("Barcode : "+msg['barcode']+" tidak ditemukan.");
			}
		}
	});
}

function pilih_barcode(obj){
	var id_barang = $(obj).closest("tr").find("input[name='item_code[]']").val();
	var barcode = $(obj).closest("tr").find("input[name='item_barcode[]']").val();
	var nm_barang = $(obj).closest("tr").find("input[name='item_nm_barang[]']").val();
	var options = $(obj).closest("tr").data("konversi");

	var $row = $("<tr>"
  					+"<td></td>"
  					+"<td>"
  						+barcode
  						+"<input type='hidden' name='id_barang[]' value='"+id_barang+"' />"
  					+"</td>"
  					+"<td>"+nm_barang+"<br><a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>"
  					+"<td><input type='number' min='1' name='qty[]' class='form-control' value='1' /></td>"
  					+"<td><select class='form-control' name='konversi[]' style='width: 150px'>"+options+"</select></td>"
  					+"<td><select name='bonus[]' class='form-control' style='width: 150px'><option value='0' selected=''>Bukan Bonus</option><option value='1'>Bonus</option>></select></td>"
  				+"</tr>");

	var pj = $("#t_det_kedatangan tr").length;
	var cek = false;

	if(pj > 2){
		for (var j = 0; j <= pj - 1; j++) {
			var kode = $("#t_det_kedatangan tr:eq("+(j+1)+")").find("input[name='id_barang[]']").val();

			if(kode != "" && kode == id_barang){
				var qty = parseFloat($("#t_det_kedatangan tr:eq("+(j+1)+")").find("input[name='qty[]']").val());

				$("#t_det_kedatangan tr:eq("+(j+1)+")").find("input[name='qty[]']").val(qty+1).trigger("change");
				
				cek = true;
				break;
			}
		}
	}

	if(cek == false){
		$("#t_det_kedatangan tbody").append($row);
		buat_no();
	}

	//Konversi
	$("select[name='konversi[]']").select2();
	//Bonus
	$("select[name='bonus[]']").select2();

	//Format number
	$("input[name='harga[]']").number(true, 0);
	$("#modal-barcode").modal("hide");
	$("#barcode").focus();
}

//Remove Item
function remove_item(obj){
	$(obj).closest("tr").animate({backgroundColor:'red'}, 1000).fadeOut(1000,function() {
		$(this).remove();
	});
}

//Hitung total dan g total
function hitung_total(obj){
	var id_barang = $(obj).closest("tr").find("input[name='id_barang[]']").val();
	var qty 	= parseFloat($(obj).closest("tr").find("input[name='qty[]']").val());
	var harga 	= parseFloat($(obj).closest("tr").find("input[name='harga[]']").val());

	if(isNaN(qty)){
		qty = 0;
	}

	if(isNaN(harga)){
		harga = 0;
	}

	//Hitung total
	var total 	= qty * harga;
	total 		= $.number(total);
	//Update total
	$(obj).closest("tr").find("td:last").text(total);
	//Hitung Gtotal
	hitung_gtotal();
}

function hitung_gtotal(){
	//Hitung gtotal
	var gtotal = 0;
	$.each($("#t_det_kedatangan tbody tr"), function(i){
		var td_total = parseFloat($("#t_det_kedatangan tbody tr:eq("+(i)+") td:last").text().replace(/,/g,""));
		gtotal += td_total;
	});
	
	$("#lblgtotal").text($.number(gtotal));
}