<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ============================
$lang['merk_title_manage']			= 'Data Merk';
$lang['merk_title_new']				= 'Data Merk Baru';
$lang['merk_title_edit']			= 'Edit Data Merk';

// from/table
$lang['merk_name']					= 'Nama Merk';
$lang['merk_ket']					= 'Keterangan';


//button
$lang['merk_btn_new']				= 'Baru';
$lang['merk_btn_delete']			= 'Hapus';
$lang['merk_btn_save']				= 'Simpan';
$lang['merk_btn_cancel']			= 'Batal';

//messages
$lang['merk_del_error']				= 'Anda belum memilih data merk yang akan dihapus.';
$lang['merk_del_failure']			= 'Tidak dapat menghapus data merk';
$lang['merk_delete_confirm']		= 'Apakah anda yakin akan menghapus data merk terpilih?';
$lang['merk_deleted']				= 'Merk berhasil dihaspus';
$lang['merk_no_records_found']		= 'Data tidak ditemukan.';

$lang['merk_create_failure']		= 'Data merk gagal disimpan';
$lang['merk_create_success']		= 'Data merk berhasil disimpan';

$lang['merk_edit_success']			= 'Data merk berhasil disimpan';
$lang['merk_invalid_id']			= 'ID tidak Valid';

