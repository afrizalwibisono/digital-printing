<div class="box box-primary">
    <!--form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_merk','name'=>'frm_merk','role','class'=>'form-horizontal'))?>
    <div class="box-body">
        <div class="form-group <?= form_error('nm_merk_bb') ? ' has-error' : ''; ?>">
                <label for="nm_merk_bb" class="col-sm-2 control-label"><?= lang('merk_name') ?></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="nm_merk_bb" name="nm_merk_bb" maxlength="255" value="<?php echo set_value('nm_merk_bb', isset($data->nm_merk_bb) ? $data->nm_merk_bb : ''); ?>" required autofocus>
                </div>
            </div>
            <div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
                <label for="ket" class="col-sm-2 control-label"><?= lang('merk_ket') ?></label>
                <div class="col-sm-4">
                    <textarea class="form-control" id="ket" name="ket"><?php echo set_value('ket', isset($data->ket) ? $data->ket : ''); ?></textarea>
                </div>
            </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" name="save" class="btn btn-primary"><?= lang('merk_btn_save') ?></button>
                <?php
                echo lang('bf_or') . ' ' . anchor('merk', lang('merk_btn_cancel'));
                ?>
            </div>
        </div>
    </div>
    <?= form_close() ?>
</div>