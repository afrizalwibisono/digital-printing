<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_retur_beli','name'=>'frm_retur_beli','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="form-group">
			    <div class="col-md-3">
			    	<label for="id_retur" class="control-label"><?= lang('retur_beli_kd_retur') ?></label>
			    	<input type="text" class="form-control" id="id_retur" name="id_retur" maxlength="100" value="<?= $data->kd_retur_beli ?>" readonly>
			    </div>
			    <div class="col-md-2">
			    	<label for="tgl_retur" class="control-label"><?= lang('retur_beli_tanggal') ?></label>
			    	<input type="text" class="form-control" id="tgl_retur" name="tgl_retur" maxlength="10" value="<?= date('d/m/Y', strtotime($data->tgl_retur)) ?>" readonly>
			    </div>
			    <div class="col-md-3">
			    	<label for="id_supplier" class="control-label"><?= lang('retur_beli_id_supplier') ?></label>
			    	<input type="text" name="id_supplier" id="id_supplier" class="form-control" value="<?= ucwords($data->nm_supplier) ?>" readonly />
			    </div>
			    <div class="col-md-3">
			    	<label for="id_gudang" class="control-label"><?= lang('retur_beli_gudang') ?></label>
			    	<input type="text" name="id_gudang" id="id_gudang" class="form-control" value="<?= ucwords($data->nama_gudang) ?>" readonly />
			    </div>
		  	</div>
		  	<div class="table-responsive">
		  		<table class="table table-bordered" id="tdet_masuk">
		  			<thead>
		  				<tr class="success">
			  				<th width="50"><?= lang('retur_beli_no') ?></th>
			  				<th><?= lang('retur_beli_barcode') ?></th>
			  				<th><?= lang('retur_beli_nm_barang') ?></th>
			  				<th><?= lang('retur_beli_qty') ?></th>
			  				<th><?= lang('retur_beli_satuan') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($detail) : ?>
		  					<?php foreach($detail as $key => $dt) : ?>
		  				<tr>
		  					<td><?= $key+1 ?></td>
		  					<td><?= $dt->barcode ?></td>
		  					<td><?= $dt->nm_barang ?></td>
		  					<td><?= number_format($dt->qty) ?></td>
		  					<td><?= $dt->satuan ?></td>
		  				</tr>
		  					<?php endforeach; ?>
		  				<?php endif; ?>
		  			</tbody>
		  		</table>
		  	</div>
		  	<div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
			    <div class="col-sm-6">
			    	<label for="ket" class="control-label"><?= lang('retur_beli_ket') ?></label>
			    	<textarea class="form-control" id="ket" name="ket" maxlength="255" readonly=""><?= $data->ket ?></textarea>
			    </div>
		  	</div>
		  	<div class="form-group">
		  		<div class="col-md-12">
		  			<?php echo anchor('retur_pembelian', lang('retur_beli_btn_back'), array('class' =>'btn btn-success')) ?>
		  		</div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->