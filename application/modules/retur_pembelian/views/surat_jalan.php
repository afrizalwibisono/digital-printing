<div class="box box-default">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_surat_jalan','name'=>'frm_surat_jalan','role'=>'form','class'=>'form-horizontal')) ?>
    	<?php if($idt) : ?>
	    <div class="row header-laporan">
	        <div class="col-md-12">
	            <div class="col-md-1 laporan-logo">
	                <img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo">
	            </div>
	            <div class="col-md-6 laporan-identitas">
	                <address>
	                	<h3 class="no-margin">Surat Jalan Retur</h3>
	                    <h4><?= $idt->nm_perusahaan ?></h4>
	                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
	                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
	                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?><br>
	                </address>
	            </div>
	            <div class="clearfix"></div>
	            <div class="laporan-garis"></div>
	        </div>
	    </div>
	    <?php endif ?>
    	<div class="box-body">
    		<table class="table-condensed table-summary">
                <tr>
                    <td width="120"><?= lang('retur_beli_kd_retur') ?></td>
                    <td width="10">:</td>
                    <td><strong><?= $data->kd_retur_beli ?></strong></td>
                    <td width="120"><?= lang('retur_beli_id_supplier') ?></td>
                    <td width="10">:</td>
                    <td><?= $data->nm_supplier ?></td>
                </tr>
                <tr>
                    <td width="120"><?= lang('retur_beli_tanggal') ?></td>
                    <td width="10">:</td>
                    <td><?= date('d/m/Y', strtotime($data->tgl_retur)) ?></td>
                	<td width="120"><?= lang('retur_beli_gudang') ?></td>
                    <td width="10">:</td>
                    <td><?= $data->nama_gudang ?></td>
                </tr>
            </table>
            <h5>Detail Barang</h5>
		  	<div class="table-responsive">
		  		<table class="table table-condensed table-bordered table-detail">
		  			<thead>
		  				<tr class="success">
			  				<th width="50"><?= lang('retur_beli_no') ?></th>
			  				<th><?= lang('retur_beli_nm_barang') ?></th>
			  				<th><?= lang('retur_beli_qty') ?></th>
			  				<th><?= lang('retur_beli_satuan') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($detail) : ?>
		  					<?php foreach($detail as $key => $dt) : ?>
		  				<tr>
		  					<td><?= $key+1 ?></td>
		  					<td><?= $dt->nm_barang ?></td>
		  					<td><?= number_format($dt->qty) ?></td>
		  					<td><?= $dt->satuan ?></td>
		  				</tr>
		  					<?php endforeach; ?>
		  				<?php endif; ?>
		  			</tbody>
		  		</table>
		  	</div>
		  	<div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
			    <div class="col-sm-6">
			    	<label for="ket" class="control-label"><?= lang('retur_beli_ket') ?></label>
			    	<p><?= $data->ket ?></p>
			    </div>
		  	</div>
		  	<br>
		  	<br>
		  	<div class="pull-right" style="margin-right: 100px">
		  		<?= lang('retur_beli_pengirim') ?>,
		  		<br>
			  	<br>
			  	<br>
			  	<?= $pengirim->nm_karyawan ?>
			  	<br>
			  	<?= $data->nama_gudang ?>	
		  	</div>
	  	</div>
	<?= form_close() ?>
</div><!-- /.box -->
<script type="text/javascript">
	$(document).ready(function(){
		window.print();
	});
</script>