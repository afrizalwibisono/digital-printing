<?php 
    $ENABLE_ADD     = has_permission('Retur Pembelian.Add');
    $ENABLE_MANAGE  = has_permission('Retur Pembelian.Manage');
    $ENABLE_DELETE  = has_permission('Retur Pembelian.Delete');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_retur_beli','name'=>'frm_retur_beli')) ?>
        <div class="box-header">
            <div class="form-group">
                <?php if ($ENABLE_ADD) : ?>
	  	        <a href="<?= site_url('retur_pembelian/create') ?>" class="btn btn-success" title="<?= lang('retur_beli_btn_new') ?>"><?= lang('retur_beli_btn_new') ?></a>
                <?php endif; ?>
                <div class="pull-right form-inline">
                    <div class="input-daterange input-group" data-toggle="tooltip" data-placement="top" title="<?= lang('retur_beli_range') ?>">
                        <input type="text" class="form-control" name="tgl_awal" id="tgl_awal" value="" placeholder="<?= lang('retur_beli_tgl_awal') ?>" />
                        <span class="input-group-addon">to</span>
                        <input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" value="" placeholder="<?= lang('retur_beli_tgl_akhir') ?>" />
                    </div>
                    <div class="input-group">
                        <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="Search" autofocus />
                        <div class="input-group-btn">
                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
	<div class="box-body table-responsive no-padding">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
                        <th width="50">#</th>
                        <th><?= lang('retur_beli_kd_retur') ?></th>
                        <th><?= lang('retur_beli_tanggal') ?></th>
                        <th><?= lang('retur_beli_gudang') ?></th>
                        <th><?= lang('retur_beli_id_supplier') ?></th>
                        <th><?= lang('retur_beli_ket') ?></th>
                        <?php if($ENABLE_MANAGE) : ?>
                        <th width="80"></th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $atts = array(
                                'width'       => 744,
                                'height'      => 600,
                                'scrollbars'  => 'yes',
                                'status'      => 'yes',
                                'resizable'   => 'yes',
                                'screenx'     => 100,
                                'screeny'     => 100,
                                'window_name' => '_blank',
                                'class'       => 'text-black',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'left',
                                'title'       => 'Cetak Surat Jalan'        
                        );

                        foreach ($results as $record) : ?>
                    <tr>
                        <td class="column-check"><input type="checkbox" name="checked[]" value="<?= $record->id_retur ?>" /></td>
                        <td><?= $numb; ?></td>
                        <td><?= $record->kd_retur_beli ?></td>
                        <td><?= date('d/m/Y', strtotime($record->tgl_retur)) ?></td>
                        <td><?= ucwords($record->nama_gudang) ?></td>
                        <td><?= ucwords($record->nm_supplier) ?></td>
                        <td><?= $record->ket ?></td>
                        <?php if($ENABLE_MANAGE) : ?>
                        <td style="padding-right:20px">
                            <?= anchor_popup('retur_pembelian/cetak_surat_jalan/'.$record->id_retur, "<i class='fa fa-print'></i>", $atts); ?>
                            &nbsp;|&nbsp;
                            <a class="text-black" href="<?= site_url('retur_pembelian/view/' . $record->id_retur); ?>" data-toggle="tooltip" data-placement="left" title="View Detail"><i class="fa fa-folder"></i></a>
                        </td>
                        <?php endif; ?>
                    </tr>
                    <?php $numb++; endforeach; ?>
                </tbody>
	  </table>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">
		<?php if($ENABLE_DELETE) : ?>
		<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('retur_beli_btn_delete') ?>" onclick="return confirm('<?= (lang('retur_beli_delete_confirm')); ?>')">
		<?php endif;
		echo $this->pagination->create_links(); 
		?>
	</div>
	<?php else: ?>
     <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('retur_beli_no_records_found') ?></p>
    </div>
    <?php
	endif;
	form_close(); 
	?>
</div><!-- /.box -->