<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_retur_beli','name'=>'frm_retur_beli','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="form-group">
			    <div class="col-md-3 <?= form_error('id_retur') ? ' has-error' : ''; ?>">
			    	<label for="id_retur" class="control-label"><?= lang('retur_beli_kd_retur') ?></label>
			    	<input type="text" class="form-control" id="id_retur" name="id_retur" maxlength="100" value="<?= $data['kd_retur_beli'] ?>" readonly>
			    </div>
			    <div class="col-md-2 <?= form_error('tgl_retur') ? ' has-error' : ''; ?>">
			    	<label for="tgl_retur" class="control-label"><?= lang('retur_beli_tanggal') ?></label>
			    	<input type="text" class="form-control" id="tgl_retur" name="tgl_retur" maxlength="10" value="<?= set_value('tgl_retur', isset($data['tgl_retur']) ? $data['tgl_retur'] : '') ?>" placeholder="dd/mm/yyy">
			    </div>
			    <div class="col-md-3 <?= form_error('id_supplier') ? ' has-error' : ''; ?>">
			    	<label for="id_supplier" class="control-label"><?= lang('retur_beli_id_supplier') ?></label>
			    	<select name="id_supplier" id="id_supplier" class="form-control">
			    		<option></option>
			    		<?php foreach ($supplier as $key => $sp) : ?>
			    		<option value="<?= $sp->idsupplier ?>" <?= set_select('id_supplier', $sp->idsupplier, isset($data['id_supplier']) &&  $data['id_supplier'] == $sp->idsupplier) ?>><?= ucwords($sp->nm_supplier) ?></option>
			    		<?php endforeach ?>
			    	</select>
			    </div>
			    <div class="col-md-3 <?= form_error('id_gudang') ? ' has-error' : ''; ?>">
			    	<label for="id_gudang" class="control-label"><?= lang('retur_beli_gudang') ?></label>
			    	<select name="id_gudang" id="id_gudang" class="form-control">
			    		<option></option>
			    		<?php foreach ($gudang as $key => $gd) : ?>
			    		<option value="<?= $gd->id_gudang ?>" <?= set_select('id_gudang', $gd->id_gudang, isset($data['id_gudang']) &&  $data['id_gudang'] == $gd->id_gudang) ?>><?= ucwords($gd->nama_gudang) ?></option>
			    		<?php endforeach ?>
			    	</select>
			    </div>
		  	</div>
		  	<div class="table-responsive">
		  		<table class="table table-bordered" id="tdet_retur">
		  			<thead>
		  				<tr class="success">
			  				<th width="50"><?= lang('retur_beli_no') ?></th>
			  				<th><?= lang('retur_beli_barcode') ?></th>
			  				<th><?= lang('retur_beli_nm_barang') ?></th>
			  				<th><?= lang('retur_beli_qty') ?></th>
			  				<th><?= lang('retur_beli_satuan') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($data['items']) : ?>
		  					<?php 
		  						$no = 1;
		  						foreach($data['items'] as $key => $dt) : 
		  					?>
		  				<tr>
		  					<td><?= $no++ ?></td>
		  					<td>
		  						<?= $dt['barcode'] ?>
		  						<input type="hidden" name="id_barang[]" value="<?= $dt['id_barang'] ?>" />
		  						<input type='hidden' name='id_satuan[]' value="<?= $dt['id_satuan'] ?>" />
		  					</td>
		  					<td><?= $dt['nm_barang'] ?> - <a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>
		  					<td><input type="number" min="1" name="qty[]" class="form-control" value="<?= $dt['qty'] ?>" data-qty="<?= $dt['qty'] ?>" /></td>
		  					<td>
		  						<select class='form-control' name='konversi[]' style="width: 200px">
		  							<?php 
		  								if($dt['konversi']) :
		  									foreach ($dt['konversi'] as $key => $kf) :
		  							?>
		  							<option value="<?= $kf->id_konversi ?>" <?= (isset($kf->id_konversi) && $kf->id_konversi == $dt['id_konversi']) ? "selected" : "" ?>><?= $kf->tampil2 ?></option>
		  							<?php
		  									endforeach; 
		  								endif; 
		  							?>
		  						</select>
		  					</td>
		  				</tr>
		  					<?php endforeach; ?>
		  				<?php endif; ?>
		  			</tbody>
		  			<tfoot>
		  				<tr>
		  					<td colspan="5">
		  						<div class="input-group" id="barcode_input">
		  							<div class="input-group-btn">
		  								<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-barang"><i class="fa fa-plus-square"></i></button>
		  							</div>
		  							<input type="text" class="form-control" id="barcode" name="barcode" placeholder="<?= lang('retur_beli_scan_brg') ?>" autocomplete="off" autofocus />
		  						</div>
		  						<span class="help-block text-green" id="barcode_msg"></span>
		  					</td>
		  				</tr>
		  			</tfoot>
		  		</table>
		  	</div>
		  	<div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
			    <div class="col-sm-6">
			    	<label for="ket" class="control-label"><?= lang('retur_beli_ket') ?></label>
			    	<textarea class="form-control" id="ket" name="ket" maxlength="255"><?php echo set_value('ket', isset($data['ket']) ? $data['ket'] : ''); ?></textarea>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <div class="col-md-12">
			      	<button type="submit" name="save" class="btn btn-primary"><?= lang('retur_beli_btn_save') ?></button>
			    	<?php
	                	echo lang('retur_beli_or') . ' ' . anchor('retur_pembelian/cancel', lang('retur_beli_btn_cancel'), array("onclick" => "return confirm('".lang('retur_beli_cancel_confirm')."')"));
	                ?>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->
<div class="modal" id="modal-barang" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
	    <div class="modal-content">
	    	<?= form_open("",array('id'=>'frm_barang','name'=>'frm_barang','role'=>'form')) ?>
		      	<div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        	<h4 class="modal-title"><?= lang('retur_beli_pilih_brg') ?></h4>
		      	</div>
		      	<div class="modal-body">
		      		<div class="form-group">
		      			<div class="input-group">
		      				<input type="text" class="form-control" id="cr_barang" name="cr_barang" placeholder="<?= lang('retur_beli_cari_brg') ?>" />
		      				<div class="input-group-btn">
		      					<button type="button" class="btn btn-primary" name="cari_btn" id="cari_btn"><i class="fa fa-search"></i> <?= lang('retur_beli_cari_btn') ?></button>
		      				</div>
		      			</div>
		      		</div>
		        	<div class="table-responsive">
		        		<table id="tbl_barang" class="table table-hover table-striped">
		        			<thead>
		        				<tr class="success">
			        				<th><input type="checkbox" id="ck_all" name="ck_all" /></th>
			        				<th><?= lang('retur_beli_no') ?></th>
			        				<th><?= lang('retur_beli_barcode') ?></th>
			        				<th><?= lang('retur_beli_nm_barang') ?></th>
			        				<th><?= lang('retur_beli_stok') ?></th>
			        			</tr>
		        			</thead>
		        			<tbody>
		        				
		        			</tbody>
		        		</table>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
		      		<button type="button" class="btn btn-primary" id="pilih_btn" name="pilih_btn"><?= lang('retur_beli_pilih_btn') ?></button>
		      	</div>
	      	<?= form_close() ?>
	    </div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
