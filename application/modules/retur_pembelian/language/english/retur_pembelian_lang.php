<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['retur_beli_title_manage'] = 'Retur Pembelian';
$lang['retur_beli_title_new'] 	 = 'Retur Pembelian Baru';
$lang['retur_beli_title_view'] 	 = 'Detail Retur Pembelian';
$lang['retur_beli_laporan_title_view'] = 'Laporan Retur Pembelian';

// form/table
$lang['retur_beli_kd_retur'] 	= 'Kode Retur';
$lang['retur_beli_tanggal']  	= 'Tanggal Retur';
$lang['retur_beli_id_supplier'] = 'Supplier';
$lang['retur_beli_pil_tujuan'] 	= '-- Pilih Supplier --';
$lang['retur_beli_gudang'] 		= 'Gudang';
$lang['retur_beli_ket'] 		= 'Keterangan Tambahan';
$lang['retur_beli_surat_jalan'] = 'Surat Jalan';
$lang['retur_beli_pengirim'] 	= 'Pengirim';

$lang['retur_beli_no'] 			= '#';
$lang['retur_beli_barcode'] 	= 'Barcode';
$lang['retur_beli_artikel'] 	= 'Artikel';
$lang['retur_beli_nm_barang'] 	= 'Nama Barang';
$lang['retur_beli_qty'] 		= 'Qty';
$lang['retur_beli_stok'] 	= 'Stok';
$lang['retur_beli_harga'] 	= 'Harga';
$lang['retur_beli_satuan'] 	= 'Satuan';
$lang['retur_beli_total'] 	= 'Total';
$lang['retur_beli_gtotal'] 	= 'Grand Total : ';


$lang['retur_beli_ringkasan'] 		= 'Ringkasan';
$lang['retur_beli_detail'] 			= 'Detail Transaksi';
$lang['retur_beli_total_tran_qty'] 	= 'Transaksi';
$lang['retur_beli_total_tran'] 		= 'Total Seluruh Transaksi';
$lang['retur_beli_kali'] 			= 'kali';

$lang['retur_beli_operator'] 	= 'Operator';
$lang['retur_beli_cetak'] 		= 'Cetak';

$lang['retur_beli_range']  	  = 'Range tanggal';
$lang['retur_beli_tgl_awal']  = 'Tanggal awal';
$lang['retur_beli_tgl_akhir'] = 'Tanggal akhir';

$lang['retur_beli_pilih_brg'] = 'Pilih Barang';
$lang['retur_beli_scan_brg']  = 'Ketik / Scan Barcode Barang';
$lang['retur_beli_cari_brg']  = 'Cari Barang ...';
$lang['retur_beli_cari_btn']  = 'Cari';
$lang['retur_beli_pilih_btn'] = 'Pilih';

// button
$lang['retur_beli_btn_new'] 	= 'Baru';
$lang['retur_beli_btn_delete'] 	= 'Hapus';
$lang['retur_beli_btn_save'] 	= 'Simpan';
$lang['retur_beli_btn_cancel'] 	= 'Batal';
$lang['retur_beli_btn_back'] 	= 'Kembali';
$lang['retur_beli_or'] 			= 'atau';

// messages
$lang['retur_beli_del_error']		= 'Anda belum memilih retur pembelian yang akan dihapus.';
$lang['retur_beli_del_failure']		= 'Tidak dapat menghapus retur pembelian: ';
$lang['retur_beli_delete_confirm']	= 'Apakah anda yakin akan menghapus retur pembelian terpilih ?';
$lang['retur_beli_cancel_confirm']	= 'Apakah anda yakin akan membatalkan retur pembelian ?';
$lang['retur_beli_deleted']			= 'Data Barang retur pembelian berhasil dihapus';
$lang['retur_beli_no_records_found'] = 'Data tidak ditemukan.';

$lang['retur_beli_create_failure'] 	= 'Retur Pembelian baru gagal disimpan: ';
$lang['retur_beli_create_success'] 	= 'Retur Pembelian baru berhasil disimpan';
$lang['retur_beli_canceled'] 		= 'Retur Pembelian telah berhasil dibatalkan';

$lang['retur_beli_edit_success'] 	= 'Retur Pembelian berhasil disimpan';
$lang['retur_beli_invalid_id'] 		= 'ID Tidak Valid';

$lang['retur_beli_no_item'] 		= 'Anda belum menambahkan data barang yang akan diretur.';
$lang['retur_beli_qty_nol'] 		= 'Qty baris ke: %s tidak boleh kurang atau sama dengan 0 (nol).';
$lang['retur_beli_harga_nol'] 		= 'Harga baris ke: %s tidak boleh kurang atau sama dengan 0 (nol).';
$lang['retur_beli_stok_minus'] 		= 'Maaf stok barang baris ke: %s tidak mencukupi.';

$lang['retur_beli_barcode_found'] 	= 'Barcode : %s ditemukan.';
$lang['retur_beli_barcode_not_found'] = 'Barcode : %s tidak ditemukan.';

$lang['retur_beli_only_ajax'] 		= 'Hanya ajax request yang diijinkan.';

$lang['retur_beli_date_required'] 	= 'Silahkan pilih tanggal awal dan akhir.';
$lang['retur_beli_stok_not_enough'] = 'Maaf stok tidak mencukupi untuk barang : %s';