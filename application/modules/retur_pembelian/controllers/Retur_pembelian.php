<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2018, Cokeshome
 * 
 * This is controller for Retur Pembelian
 */

class Retur_pembelian extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Retur Pembelian.View";
    protected $addPermission    = "Retur Pembelian.Add";
    protected $managePermission = "Retur Pembelian.Manage";
    protected $deletePermission = "Retur Pembelian.Delete";
    protected $laporanPermission = "Laporan Retur Pembelian.View";

    protected $prefixKey        = "RB";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('retur_pembelian/retur_pembelian');
        $this->load->model(array('retur_pembelian/retur_pembelian_model',
                                'retur_pembelian/retur_pembelian_detail_model',
                                'retur_pembelian/retur_pembelian_detail_hpp_model',
                                'retur_pembelian/supplier_model',
                                'retur_pembelian/gudang_model',
                                'bahan_baku/bahan_baku_model',
                                'stok_model',
                                'identitas_model',
                                'konversi_satuan/konversi_satuan_model',
                                ));

        $this->template->title(lang('retur_beli_title_manage'));
		$this->template->page_icon('fa fa-share-square');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if (isset($_POST['delete']) && has_permission($this->deletePermission))
        {
            $this->db->trans_start();
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                foreach ($checked as $pid)
                {
                    //Hapus Retur Pembelian
                    $total          = 0;
                    $dt_retur       = $this->retur_pembelian_model->find($pid);
                    $total          = $dt_retur->total;
                    $del_retur     = $this->retur_pembelian_model->delete_where(array('id_retur' => $pid));
                    
                    $query_hapus    = "";
                    $query_hapus    .= $this->db->last_query();

                    $del_hpp    = FALSE;
                    if($del_retur)
                    {
                        $det_retur_hpp = $this->retur_pembelian_detail_hpp_model
                                                        ->select('retur_pembelian_detail_hpp.*')
                                                        ->join('retur_pembelian_detail','retur_pembelian_detail_hpp.id_detail_retur=retur_pembelian_detail.id_detail_retur')
                                                        ->find_all_by('id_retur', $pid);
                        //Kembalikan Stok
                        if($det_retur_hpp)
                        {
                            foreach ($det_retur_hpp as $key => $drh) {
                                $this->retur_pembelian_detail_hpp_model->delete_where(array('id_detail_retur' => $drh->id_detail_retur));
                                $query_hapus .= "\n\n".$this->db->last_query();
                                
                                $this->stok_model->update_stok_by_kd_stok($drh->id_barang, $drh->id_stok, $drh->qty, 1);
                                $query_hapus .= "\n\n".$this->db->last_query();
                            }
                        }

                        if($del_hpp)
                        {
                            $keterangan = "SUKSES, hapus data retur pembelian dengan ID : ".$pid;
                            $status     = 1;
                        }
                        else
                        {
                            $keterangan = "GAGAL, hapus data retur pembelian dengan ID : ".$pid;
                            $status     = 0;
                        } 
                    }
                    else
                    {
                        $keterangan = "GAGAL, hapus data retur pembelian dengan ID : ".$pid.", error : ".$this->retur_pembelian_detail_model->error;
                        $status     = 0;
                    }

                    $nm_hak_akses   = $this->deletePermission; 
                    $kode_universal = $pid;
                    $jumlah         = $total;
                    $sql            = $query_hapus;

                    $hasil = simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                }

                if ($del_retur)
                {
                    $this->template->set_message(count($checked) .' '. lang('retur_beli_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('retur_beli_del_failure') . (isset($err) ? $err." " : "") . $this->retur_pembelian_model->error, 'error');
                }
            }
            else
            {
                $this->template->set_message(lang('retur_beli_del_error').$this->retur_pembelian_model->error, 'error');
            }
            $this->db->trans_complete();

            unset($_POST['delete']);
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
            $tgl_awal  = $this->input->post('tgl_awal');
            $tgl_akhir = $this->input->post('tgl_akhir');
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
            $tgl_awal  = $this->input->get('tgl_awal');
            $tgl_akhir = $this->input->get('tgl_akhir');
        }

        $filter = "?search=".urlencode($search);
        $search2 = $this->db->escape_str($search);

        $add_where = "";
        if($tgl_awal !='' && $tgl_akhir !='')
        {
            $filter    .= "&tgl_awal=".urlencode(date_ymd($tgl_awal))."&tgl_akhir=".urlencode(date_ymd($tgl_akhir));
            $add_where .= " AND (retur_pembelian.tgl_retur >='".date_ymd($tgl_awal)."' AND retur_pembelian.tgl_retur <='".date_ymd($tgl_akhir)."')";
        }
        
        $where = "`retur_pembelian.deleted` = 0 $add_where
                AND (`id_retur` LIKE '%$search2%' ESCAPE '!'
                OR `kd_retur_beli` LIKE '%$search2%' ESCAPE '!'
                OR `total` LIKE '%$search2%' ESCAPE '!'
                OR `nama_gudang` LIKE '%$search2%' ESCAPE '!'
                OR `retur_pembelian`.`ket` LIKE '%$search2%' ESCAPE '!')";
        
        $total = $this->retur_pembelian_model
                    ->join("gudang","retur_pembelian.id_gudang = gudang.id_gudang","left")
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->retur_pembelian_model
                    ->select("retur_pembelian.*, nama_gudang, nm_supplier")
                    ->join("gudang","retur_pembelian.id_gudang = gudang.id_gudang","left")
                    ->join("supplier","retur_pembelian.id_supplier = supplier.idsupplier","left")
                    ->where($where)
                    ->order_by('retur_pembelian.created_on','DESC')
                    ->limit($limit, $offset)->find_all();

        $assets = array(
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        "retur_pembelian/assets/js/retur_pembelian_index.js"
                        );

        add_assets($assets);

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set("toolbar_title", lang('retur_beli_title_manage'));
        $this->template->title(lang('retur_beli_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

   	//Create Retur Pembelian
   	public function create()
   	{

        $this->auth->restrict($this->addPermission);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_retur())
            {
              $this->template->set_message(lang("retur_beli_create_success"), 'success');
              redirect('retur_pembelian');
            }
        }

        //cek session data
        $data = $this->session->userdata('rt_beli');
        if(!$data)
        {
            $data = array('rt_beli' => array(
                                        'id_retur' => gen_primary($this->prefixKey),
                                        'kd_retur_beli' => $this->generate_kode_retur(),
                                        'items'     => array(),
                                        'total'     => 0,
                                        'id_gudang' => '',
                                        'id_supplier' => '',
                                        'tgl_retur' => '',
                                        'ket'       => ''
                                        ));

            $this->session->set_userdata($data);
            $data = $this->session->userdata('rt_beli');
        }

        $gudang = $this->gudang_model->order_by("nama_gudang","ASC")->find_all_by(array("deleted" => 0));
        $supplier = $this->supplier_model->order_by("nm_supplier","ASC")->find_all_by(["deleted" => 0]);

        $this->template->set('data', $data);
        $this->template->set('gudang', $gudang);
        $this->template->set('supplier', $supplier);

        $assets = array('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'plugins/number/jquery.number.js',
                        "retur_pembelian/assets/js/retur_pembelian.js"
                        );

        add_assets($assets);

        $this->template->title(lang('retur_beli_title_new'));
		$this->template->render('retur_pembelian_form');
   	}

    //View Detail
    public function view($id_retur = '')
    {
        $this->auth->restrict($this->viewPermission);

        if (!$id_retur)
        {
            $this->template->set_message(lang("retur_beli_invalid_id"), 'error');
            redirect('retur_pembelian');
        }

        $data  = $this->retur_pembelian_model->select("retur_pembelian.*, nama_gudang, nm_supplier")
                                        ->join("gudang","retur_pembelian.id_gudang = gudang.id_gudang","left")
                                        ->join("supplier","retur_pembelian.id_supplier = supplier.idsupplier","left")
                                        ->find($id_retur);
                                        
        $detail = $this->retur_pembelian_detail_model->select("retur_pembelian_detail.*, konversi_satuan.satuan_besar as satuan, barcode, nm_barang")
                                            ->join('barang','retur_pembelian_detail.id_barang = barang.idbarang_bb')
                                            ->join('konversi_satuan','retur_pembelian_detail.id_konversi = konversi_satuan.id_konversi',"left")
                                            ->order_by("nm_barang","ASC")
                                            ->find_all_by('id_retur', $id_retur);

        $this->template->set('id_retur', $id_retur);
        $this->template->set('data', $data);
        $this->template->set('detail', $detail);

        $this->template->title(lang('retur_beli_title_view'));
        $this->template->render('view_retur_pembelian');
    }

    public function cetak_surat_jalan($id_retur = "")
    {
        $this->auth->restrict($this->addPermission);

        if (!$id_retur)
        {
            echo lang("retur_beli_invalid_id");
            return FALSE;
        }

        $data  = $this->retur_pembelian_model->select("retur_pembelian.*, nama_gudang, nm_supplier")
                                        ->join("gudang","retur_pembelian.id_gudang = gudang.id_gudang","left")
                                        ->join("supplier","retur_pembelian.id_supplier = supplier.idsupplier","left")
                                        ->find($id_retur);
                                        
        $detail = $this->retur_pembelian_detail_model->select("retur_pembelian_detail.*, konversi_satuan.satuan_besar as satuan, barcode, nm_barang")
                                            ->join('barang','retur_pembelian_detail.id_barang = barang.idbarang_bb')
                                            ->join('konversi_satuan','retur_pembelian_detail.id_konversi = konversi_satuan.id_konversi',"left")
                                            ->order_by("nm_barang","ASC")
                                            ->find_all_by('id_retur', $id_retur);

        //Identitas
        $identitas    = $this->identitas_model->find(1);
        $idt_pengirim = $this->auth->userdata();

        $this->template->set('id_retur', $id_retur);
        $this->template->set('data', $data);
        $this->template->set('detail', $detail);
        $this->template->set('idt', $identitas);
        $this->template->set('pengirim', $idt_pengirim);

        $this->template->set_layout('cetak');
        $this->template->title(lang('retur_beli_surat_jalan'));
        $this->template->render('surat_jalan');
    }

    //Cancel Barang masuk
    public function cancel()
    {
        $this->auth->restrict($this->addPermission);

        $this->session->unset_userdata('rt_beli');
        $this->template->set_message(lang('retur_beli_canceled'), 'success');

        redirect('retur_pembelian');
    }

    public function cari_barang()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('retur_beli_only_ajax'), 'error');
            redirect('retur_pembelian');
        }

        $id_gudang = $this->input->post('id_gudang');

        $cari = $this->db->escape_str($this->input->post('cr_barang'));

        $data = $this->bahan_baku_model->select(array("barang.*","satuan_terkecil.alias as satuan","sum(stok_update) as stok"))
                                    ->join("satuan_terkecil","barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","left")
                                    ->join("stok","barang.idbarang_bb = stok.id_barang")
                                    ->where("`barang.deleted` = 0 AND `stok`.`deleted` = 0 AND stok.id_gudang = '$id_gudang'  
                                            AND (barcode like '%$cari%' OR nm_barang like '%$cari%')")
                                    ->order_by('nm_barang', 'ASC')
                                    ->group_by(array("barang.idbarang_bb"))
                                    ->having("stok >", 0)
                                    ->find_all();
        if($data)
        {
            foreach ($data as $key => $dt) {
                $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
            }
        }

        echo json_encode($data);
    }

    public function scan_barcode()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('retur_beli_only_ajax'), 'error');
            redirect('retur_pembelian');
        }

        $barcode = $this->input->post('barcode');
        $id_gudang = $this->input->post('id_gudang');

        if($barcode != "")
        {
            $data = $this->bahan_baku_model->select(array("barang.*","satuan_terkecil.alias as satuan","sum(stok_update) as stok"))
                                    ->join("satuan_terkecil","barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","left")
                                    ->join("stok","barang.idbarang_bb = stok.id_barang")
                                    ->where("`barang.deleted` = 0 AND `stok`.`deleted` = 0 AND stok.id_gudang = '$id_gudang' 
                                            AND barcode='$barcode' AND `stok`.`st_bonus` = 0")
                                    ->order_by('nm_barang', 'ASC')
                                    ->group_by(array("barang.idbarang_bb"))
                                    ->having("stok >", 0)
                                    ->find_all();
            if($data)
            {
                foreach ($data as $key => $dt) {
                    $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
                }

                $return = array('type' => 'success', 'barcode' => $barcode, 'data' => $data);
            }
            else
            {
                $return = array('type' => 'error', 'barcode' => $barcode, 'data' => 0);
            }
        }
        else
        {
            $return = array('type' => 'error', 'barcode' => $barcode, 'data' => 0);
        }

        echo json_encode($return);
    }

    protected function cek_stok($id_barang = "", $qty = 0, $id_gudang = "")
    {
        $cek_stok = $this->stok_model->order_by("created_on", "desc")
                                ->find_all_by(array('deleted'   => 0, 
                                                    'id_barang' => $id_barang, 
                                                    'stok_update >' => 0,
                                                    'id_gudang' => $id_gudang
                                                    ));
        
        if(!$cek_stok)
        {
            return FALSE;
        }

        //Hitung stok
        $stok = 0;
        foreach ($cek_stok as $key0 => $ck) {
            $stok += $ck->stok_update;
        }

        if($stok < $qty)
        {
           return FALSE;
        }

        return TRUE;
    }

    public function hitung_total_ajax()
    {
        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('retur_beli_only_ajax'), 'error');
            redirect('retur_pembelian');
        }

        $id_barang = $this->input->post('id_barang');
        $qty       = $this->input->post('qty');
        $konversi  = $this->input->post('konversi');

        $return = array(
                    'gtotal' => 0,
                    'detail' => array()
                    );

        $gtotal = 0;
        if($id_barang)
        {
            foreach ($id_barang as $key => $br) {
                //Get real qty
                $dt_real_qty = hitung_ke_satuan_kecil($konversi[$key], floatval($qty[$key]));
                $real_qty    = $dt_real_qty['qty'];

                $hpp         = $this->stok_model->get_hpp($br);
                $total       = $real_qty * $hpp;
                $gtotal     += $total;

                $return['detail'][] = array('id_barang' => $br,'harga' => $hpp,'total' => $total);
            }
        }
        
        $return['gtotal'] = $gtotal;

        echo json_encode($return);        
    }

    private function save_temp_data()
    {
        $id_retur          = $this->input->post('id_retur');
        $tgl_retur         = $this->input->post('tgl_retur');
        $id_gudang         = $this->input->post('id_gudang');
        $id_supplier       = $this->input->post('id_supplier');
        $ket               = $this->input->post('ket');

        //Detail item
        $id_barang = $this->input->post('id_barang');
        $id_satuan = $this->input->post('id_satuan');
        $qty       = $this->input->post('qty');
        $harga     = $this->input->post('harga');
        $konversi  = $this->input->post('konversi');

        $data = $this->session->userdata('rt_beli');
        $data['kd_retur_beli'] = $id_retur;
        $data['tgl_retur']  = $tgl_retur;
        $data['id_gudang']  = $id_gudang;
        $data['id_supplier']= $id_supplier;
        $data['ket']        = $ket;

        $dt_item = array();
        $gtotal  = 0;
        if($id_barang)
        {
            foreach ($id_barang as $key => $br) {
                $dt_barang  = $this->bahan_baku_model->find($br);
                $total      = 0;
                if($dt_barang)
                {
                    /** Hitung Total */
                    //Get real qty
                    $dt_real_qty = hitung_ke_satuan_kecil($konversi[$key], floatval($qty[$key]));
                    $real_qty    = $dt_real_qty['qty'];

                    // $hpp         = $this->stok_model->get_hpp($br);
                    // $total       = $real_qty * $hpp;
                    // $gtotal     += $total;
                    $hpp         = 0;
                    $total       = 0;

                    $dt_item[]   = array(
                                    'id_barang'   => $br,
                                    'barcode'     => $dt_barang->barcode,
                                    'nm_barang'   => $dt_barang->nm_barang,
                                    'qty'         => $qty[$key],
                                    'id_satuan'   => $id_satuan[$key],
                                    'id_konversi' => $konversi[$key],
                                    'konversi'    => get_konversi($id_satuan[$key]),
                                    'harga'       => $hpp,
                                    'total'       => $total
                                    );
                }
            }
        }

        $data['gtotal'] = $gtotal;
        $data['items']  = $dt_item;
        //Set Session
        $this->session->set_userdata('rt_beli', $data);
    }
    
   	protected function save_retur()
   	{
        $id_retur           = $this->input->post('id_retur');
        $tgl_retur          = $this->input->post('tgl_retur');
        $id_gudang          = $this->input->post('id_gudang');
        $id_supplier        = $this->input->post('id_supplier');
        $ket                = $this->input->post('ket');

        //Detail item
        $id_barang = $this->input->post('id_barang');
        $id_satuan = $this->input->post('id_satuan');
        $qty       = $this->input->post('qty');
        $harga     = $this->input->post('harga');
        $konversi  = $this->input->post('konversi');

        //validasi
        if(count($id_barang) == 0)
        {
            //Save Session
            $this->save_temp_data();
            $this->template->set_message(lang('retur_beli_no_item'), 'error');
            return FALSE;
        }

        $ero_msg = array();
        $urut = 0;
        foreach ($id_barang as $key => $it) {
            $urut ++;
            if($qty[$key] <= 0)
            {
                $ero_msg[] = sprintf(lang('retur_beli_qty_nol'), $urut);
            }

            // if($harga[$key] <= 0)
            // {
            //     $ero_msg[] = sprintf(lang('retur_beli_harga_nol'), $urut);
            // }

            if(!$this->cek_stok($it, $qty[$key], $id_gudang))
            {
                $ero_msg[] = sprintf(lang('retur_beli_stok_minus'), $urut);
            }
        }

        if($ero_msg)
        {
            //Save Session
            $this->save_temp_data();

            $ero_msg = implode("<br>", $ero_msg);
            $this->template->set_message($ero_msg, 'error');
            return FALSE;
        }

        $this->form_validation->set_rules('id_retur','lang:retur_beli_kd_retur','required');
        $this->form_validation->set_rules('tgl_retur','lang:retur_beli_tanggal','required|trim');    
        $this->form_validation->set_rules('id_supplier','lang:retur_beli_id_supplier','callback_default_select');    
        $this->form_validation->set_rules('id_gudang','lang:retur_beli_gudang','callback_default_select');    
        $this->form_validation->set_message('default_select', 'You should choose the "%s" field');
        
        if ($this->form_validation->run() === FALSE) 
        {
            //Save Session
            $this->save_temp_data();

            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        //Start Transaction
        $this->db->trans_start();
        //Hitung Gtotal
        $gtotal = 0;
        foreach ($id_barang as $key => $br) {
            //Get real qty
            $dt_real_qty = hitung_ke_satuan_kecil($konversi[$key], floatval($qty[$key]));
            $real_qty    = $dt_real_qty['qty'];

            $hpp         = $this->stok_model->get_hpp($br);
            $total       = $real_qty * $hpp;
            $gtotal     += $total;

        }

        $dt_session = $this->session->userdata('rt_beli');
        //Simpan Ke table Retur Pembelian
        $data_brg_retur = array('id_retur'      => $dt_session['id_retur'],
                                'kd_retur_beli' => $id_retur,
                                'tgl_retur'     => date_ymd($tgl_retur),
                                'id_supplier'   => $id_supplier,
                                'id_gudang'     => $id_gudang,
                                'total'         => $gtotal, 
                                'ket'           => $ket
                            );

        $this->retur_pembelian_model->insert($data_brg_retur);
        $query_barang_retur = $this->db->last_query();

        //Check sukses atau tidak
        $st_brg_retur = $this->retur_pembelian_model->find_by('id_retur', $dt_session['id_retur']);
        if($st_brg_retur)
        {
            //Insert Detail Retur Pembelian
            $data_det_retur = array();
            $data_stok_keluar = array();
            foreach ($id_barang as $key => $br) {
                //Get real qty
                $dt_real_qty = hitung_ke_satuan_kecil($konversi[$key], floatval($qty[$key]));
                $real_qty    = $dt_real_qty['qty'];

                $hpp         = $this->stok_model->get_hpp($br);
                $total       = $real_qty * $hpp;

                $data_det_retur[] = array('id_detail_retur' => gen_primary(),
                                        'id_retur'  => $dt_session['id_retur'],
                                        'id_barang' => $br,
                                        'qty'       => $qty[$key],
                                        'harga'     => $hpp,
                                        'id_konversi'   => $konversi[$key],
                                        'sub_total'     => $total,
                                        'st_pakai_potong' => 0
                                        );
            }

            $this->retur_pembelian_detail_model->insert_batch($data_det_retur);
            $query_barang_retur .= "\n\n".$this->db->last_query();

            $st_det_retur = $this->retur_pembelian_detail_model->count_by('id_retur', $dt_session['id_retur']);
            if($st_det_retur == count($data_det_retur))
            {
                $st_det_retur = TRUE;
                foreach ($data_det_retur as $key => $ddr) {
                    //Data Barang
                    $dt_barang  = $this->bahan_baku_model->find($ddr['id_barang']);
                    //Update Stok barang
                    //Get real qty
                    $dt_real_qty = hitung_ke_satuan_kecil($ddr['id_konversi'], $ddr['qty']);
                    $real_qty    = $dt_real_qty['qty'];

                    $stok_keluar = $this->stok_model->update_stok_by_kd_barang($ddr['id_barang'], $real_qty, 0, $id_gudang);
                    $query_barang_retur .= "\n\n".$this->db->last_query();
                    //Insert Table Detail Retur HPP
                    $data_kel_hpp = array();
                    foreach ($stok_keluar as $key => $stk) {
                        $data_kel_hpp[] = array(
                                        'id_detail_retur_hpp' => gen_primary(),
                                        'id_detail_retur'     => $ddr['id_detail_retur'],
                                        'id_barang' => $ddr['id_barang'],
                                        'qty'       => $stk['jml'],
                                        'hpp'       => $stk['hpp'],
                                        'id_satuan_terkecil' => $dt_barang->id_satuan_terkecil,
                                        'id_stok'   => $stk['id_stok']
                                        );
                    }

                    $this->retur_pembelian_detail_hpp_model->insert_batch($data_kel_hpp);
                    $query_barang_retur .= "\n\n".$this->db->last_query();
                }
            }
            else
            {
                $st_det_retur = FALSE;
                $this->template->set_message(lang('retur_beli_create_failure'),'error');
            }
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $keterangan = "GAGAL, tambah data retur pembelian dengan Kode Retur ".$id_retur;
            $status     = 0;

            $return = FALSE;
        }
        else
        {
            $keterangan = "SUKSES, tambah data retur pembelian dengan Kode Retur : ".$id_retur;
            $status     = 1;
            
            $return = TRUE;
        }

        //Save Log
        $nm_hak_akses   = $this->addPermission; 
        $kode_universal = $id_retur;
        $jumlah         = $gtotal;
        $sql            = $query_barang_retur;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        if($return)
        {
            //Clear Session
            $this->session->unset_userdata('rt_beli');
        }
        
        return $return;
   	}

    public function default_select($val)
    {
        return $val == "" ? FALSE : TRUE;
    }

    public function generate_kode_retur(){
        $awal       = "RET";
        $delimiter  = "/";
        $type       = "B";
        $inc_delimiter = "-";
        $tgl        = $this->input->post('tgl') ? date_ymd($this->input->post('tgl')) : date('Y-m-d');
        $format_tgl = date('Y/m', strtotime($tgl));

        $last_retur = $this->retur_pembelian_model->select("kd_retur_beli")
                                                ->order_by("created_on","DESC")
                                                ->limit(1)
                                                ->find_by("date_format(tgl_retur,'%Y/%m')", $format_tgl);
                                                
        if($last_retur && $last_retur->kd_retur_beli !=''){
            $last_kode = $last_retur->kd_retur_beli;
            $tmp_kode  = explode("-", $last_kode);
            $increment = intval($tmp_kode[1]) + 1;
        }else{
            $increment = 1;
        }

        $kode = $awal.$delimiter.$type.$delimiter.$format_tgl.$inc_delimiter.(str_pad($increment, 5, "0", STR_PAD_LEFT));

        if($this->input->is_ajax_request()){
            $arr = ['type' => 'success', 'kode' => $kode];
            echo json_encode($arr);
        }

        return $kode;
    }

}
?>