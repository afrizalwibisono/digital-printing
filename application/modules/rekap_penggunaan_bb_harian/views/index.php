<div class="box box-primary">
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_barang','name'=>'frm_barang','role','class'=>'form-horizontal')) ?>
    <div class="box-header hidden-print">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-sm-4"><?= lang('rekap_pengunaan_bb_harian_range_tanggal') ?></label>
                <div class="col-sm-8">
                    <div class="input-daterange input-group"> 
                        <input type="text" class="form-control" name="tgl_awal" id="tgl_awal" value="<?= isset($tgl_awal) && $tgl_awal !='' ? $tgl_awal : '' ?>" placeholder="<?= lang('rekap_pengunaan_bb_harian_tgl_awal') ?>" required /> 
                        <span class="input-group-addon text-black">to</span> 
                        <input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?= isset($tgl_akhir) && $tgl_akhir !='' ? $tgl_akhir : '' ?>" placeholder="<?= lang('rekap_pengunaan_bb_harian_tgl_akhir') ?>" required /> 
                    </div>  
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4"><?=lang('rekap_pengunaan_bb_harian_kategori')?></label>
                <div class="col-sm-8">
                    <select id="id_kategori" name="id_kategori" class="form-control">
                        <option></option>
                        <?php foreach ($kategori as $kat) : ?>
                        <option value="<?= $kat->idkategori;?>" <?= set_select('id_kategori', $kat->idkategori, isset($id_kategori) && $id_kategori == $kat->idkategori)?>> <?= $kat->nmkategori;?> </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="id_jenis"><?= lang('rekap_pengunaan_bb_harian_jenis') ?></label>
                <div class="col-sm-8">
                    <select id="id_jenis" name="id_jenis" class="form-control" style="min-width: 150px;">
                        <option></option>
                        <?php foreach ($jenis as $jn) : ?>
                        <option value="<?= $jn->idjenis_bb;?>" <?= set_select('id_jenis', $jn->idjenis_bb, isset($id_jenis) && $id_jenis == $jn->idjenis_bb)?>> <?= $jn->nmjenis_bb;?> </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-sm-4"><?= lang('rekap_pengunaan_bb_harian_merk')?></label>
                <div class="col-sm-8">
                    <select id="id_merek" name="id_merek" class="form-control" style="min-width:100%;">
                        <option></option>
                        <?php foreach ($merek as $mr) : ?>
                        <option value="<?= $mr->idmerk_bb;?>" <?= set_select('id_merek', $mr->idmerk_bb, isset($id_merek) && $id_merek == $mr->idmerk_bb)?>> <?= $mr->nm_merk_bb;?> </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4"><?= lang('rekap_pengunaan_bb_harian_other_keyword')?></label>
                <div class="col-sm-8">
                    <input type="text" name="table_search" value="<?php echo isset($search) ? $search:''; ?>" class="form-control pull-right" placeholder="Search" autofocus>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="border-top: solid 1px silver; padding-top: 5px; padding-bottom: 5px;">
            <div class="pull-left">
                <button type="button" name="cetak" id="cetak" class="btn btn-default" onclick="cetak_halaman()">
                    <span class="fa fa-print"></span> Cetak
                </button>
            </div>
            <div class="pull-right">
                <button class="btn btn-info" name="search" id="search">Filter <i class="fa fa-search"></i></button> 
            </div>                                
        </div>
    </div>
    <?php if($idt) : ?>
    <div class="row header-laporan">
        <div class="col-md-12">
            <div class="col-md-1 laporan-logo">
                <img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo">
            </div>
            <div class="col-md-6 laporan-identitas">
                <address>
                    <h4><?= $idt->nm_perusahaan ?></h4>
                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?>
                </address>
            </div>
            <div class="col-md-5 text-right">
                <h5>Rekap Penggunaan Bahan Baku Harian</h5>
                <h5>Periode <?= $tgl_awal." - ".$tgl_akhir ?></h5>
            </div>
            <div class="clearfix"></div>
            <div class="laporan-garis"></div>
        </div>
    </div>
    <?php endif ?>
    <?php if (isset($results) && is_array($results) && count($results)) : ?>
    <div class="box-body" style="font-size: 12px;">
        <table class="table table-striped" id="tbarang">
            <thead>
                <tr class="success">
                    <th width="50">#</th>
                    <th style="display: none"><?= lang('rekap_pengunaan_bb_harian_barcode') ?></th>
                    <th><?= lang('rekap_pengunaan_bb_harian_nm_barang') ?></th>
                    <th><?= lang('rekap_pengunaan_bb_harian_kategori') ?></th>
                    <th><?= lang('rekap_pengunaan_bb_harian_jenis') ?></th>
                    <th><?= lang('rekap_pengunaan_bb_harian_merk') ?></th>
                    <th><?= lang('rekap_pengunaan_bb_harian_jml_pakai_produksi') ?></th>
                    <th><?= lang('rekap_pengunaan_bb_harian_satuan') ?></th>
                    <th><?= lang('rekap_pengunaan_bb_harian_roll') ?></th>
                    <th><?= "HPP" ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($results as $record) : ?>
                <tr>
                    <td><?= $numb; ?></td>
                    <td style="display: none"><?= $record->barcode ?></td>
                    <td><?= $record->nm_barang ?></td>
                    <td><?= $record->nmkategori ?></td>
                    <td><?= $record->nmjenis_bb ?></td>
                    <td><?= $record->nm_merk_bb ?></td>
                    <td>
                        <input type="hidden" name="stok[]" value="<?= $record->stok ?>">
                        <label class="lbl-stok"><?= number_format($record->stok) ?></label>
                        </td>
                    <td>
                        <select class="form-control" name="konversi[]" style="display: none;">
                            <?php 
                                if($record->konversi) :
                                    $tampil="";
                                    foreach ($record->konversi as $key2 => $kf) :
                                        if ($kf->selected == 1) {
                                            $tampil =reset(explode("(", $kf->tampil2)) ;
                                        }
                            ?>

                            <option value="<?= $kf->id_konversi ?>" <?= ($kf->selected == 1) ? "selected='selected'" : "" ?> data-jml-kecil="<?= $kf->jml_kecil ?>" data-jml-besar="<?= $kf->jml_besar ?>" data-tipe ="<?= $kf->st_tipe;?>"><?= $kf->tampil2 ?></option>
                            <?php
                                    endforeach; 
                                endif; 
                            ?>
                        </select>
                        <?= $tampil ?>
                    </td>
                    <td>
                        <?= isset($record->qty_spek)? number_format($record->qty_spek, 2, '.', '')." ". $record->satuan_terkecil:"-"; ?>
                    </td>
                    <td><?= number_format($record->total, 2) ?></td>
                </tr>
                <?php $numb++; endforeach; ?>
            </tbody>
      </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        <?php
        echo $this->pagination->create_links(); 
        ?>
    </div>
    <?php else: ?>
     <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('rekap_pengunaan_bb_harian_records_found') ?></p>
    </div>
    <?php
    endif;
    echo form_close(); 
    ?>  
</div><!-- /.box-->