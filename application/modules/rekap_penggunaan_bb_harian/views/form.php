<style type="text/css">
    .select2-container{ width: 100% !important; }
    .select2-container .select2-selection {
        font-size: 12px;
        max-height: 30px;
        padding-top: 3px;

        line-height: 1.33333;
    }
</style>

<?php
        $ENABLE_ADD     = has_permission('Produksi.Add'); 
?>
<?php if(isset($estimasi_bb) && is_array($estimasi_bb) && count($estimasi_bb)):  ?>
<div class="box box-primary box-solid collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title">Estimasi Bahan Baku</h3>
        <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
        </button>
        </div>
    <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body table-responsive" style="display: none;">
        <table class="table table-striped table-hover" id="tdet_estimasi">
            <thead>
                <tr class="success">
                    <th width="50">NO</th> 
                    <th style="vertical-align:middle; text-align: center;"><?= lang('produksi_nm_barang') ?></th>
                    <th style="vertical-align:middle; text-align: center;"><?= lang('produksi_qty') ?></th>
                    <th width="200px" style="vertical-align:middle;text-align: center;"><?= lang('produksi_konversi_satuan') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if($estimasi_bb) : ?>
                        <?php 
                            $no = 1;
                            foreach($estimasi_bb as $key => $record) :
                        ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td> <?= $record['nm_barang'] ?></td>
                        <td> 
                            <input type="hidden" name="stok[]" value="<?= $record['qty'] ?>">
                            <label class="lbl-stok"><?= number_format($record['qty']) ?></label> 
                        </td>
                        <td>
                            <select name="konversi[]" class="form-control">
                                <?php foreach ($record['konversi'] as $key => $record) : ?>
                                <option value="<?= $record->id_konversi; ?>" <?= ($record->selected == 1) ? "selected='selected'" : "" ?> data-jml-kecil ="<?= $record->jml_kecil;?>" data-jml-besar="<?= $record->jml_besar;?>" data-tipe ="<?= $record->st_tipe;?>" <?= set_select('konversi', $record->id_konversi, isset($data_detail->konversi) && $data_detail->konversi == $record->id_konversi) ?>><?= $record->satuan_besar.' ('.$record->tampil.')'?></option> 
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                <?php 
                    endforeach;
                endif;
                ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
<?php endif ?>
    <?= form_open($this->uri->uri_string(),array('name'=>'frm_produksi','id'=>'frm_produksi','role'=>'form','class'=>'form-horizontal')) ?>
    <!-- Tabs inputan data bahan baku penyusun -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs bg-info">
                <li class="active">
                    <a href="#bahanbaku" data-toggle="tab" aria-expanded="true"><strong><?= lang("produksi_input_bb") ?></strong></a>
                </li>
                <li>
                    <a href="#spkdetail" data-toggle="tab" aria-expanded="true"><strong><?= lang("produksi_spk_detail") ?></strong></a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="bahanbaku" class="tab-pane active">
                    <input type="hidden" name="idspk" value="<?= $spk->idspk ?>">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tdet_bb">
                            <thead>
                                <tr>
                                    <th width="50"><?= lang('produksi_no') ?></th>
                                    <th><?= lang('produksi_barcode') ?></th>
                                    <th><?= lang('produksi_nm_barang') ?></th>
                                    <th width="300"><?= lang('produksi_qty') ?></th>
                                    <th><?= lang('produksi_satuan') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(isset($data['items'])) : ?>
                                    <?php 
                                        $no = 1;
                                        foreach($data['items'] as $key => $dt) : 
                                    ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td>
                                        <?= $dt['barcode'] ?>
                                        <input type="hidden" name="id_barang[]" value="<?= $dt['id_barang'] ?>" />
                                    </td>
                                    <td><?= $dt['nm_barang'] ?>
                                        <br><a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a>
                                    </td>
                                    <td><input type="number" min="1" name="qty[]" class="form-control" value="<?= $dt['qty'] ?>" /></td>
                                    <td>
                                        <select class='form-control' name='konversi[]' style="width: 150px;">
                                            <?php 
                                                if($dt['konversi']) :
                                                    foreach ($dt['konversi'] as $key => $kf) :
                                            ?>
                                            <option value="<?= $kf->id_konversi ?>" <?= (isset($kf->id_konversi) && $kf->id_konversi == $dt['id_konversi']) ? "selected" : "" ?>><?= $kf->satuan_besar." (".$kf->ket.")" ?></option>
                                            <?php
                                                    endforeach; 
                                                endif; 
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <div class="input-group" id="barcode_input">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-barang"><i class="fa fa-plus-square"></i></button>
                                            </div>
                                            <input type="text" class="form-control" id="barcode" name="barcode" placeholder="<?= lang('produksi_scan_brg') ?>" autocomplete="off" autofocus />
                                        </div>
                                        <span class="help-block text-green" id="barcode_msg"></span>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="form-group <?= form_error('next_target') ? ' has-error' : ''; ?>">
                        <div class="col-sm-6">
                            <label for="next_target" class="control-label"><?= lang('produksi_next_target') ?></label>
                            <select id="next_target" name="next_target" class="form-control">
                                <option value="0"  <?= set_select("next_target","0",isset($data['next_target']) && $data['next_target'] == 0) ?> ><?= lang('produksi_selesai')?></option>
                                <option value="1"  <?= set_select("next_target","1",isset($data['next_target']) && $data['next_target'] == 1) ?> ><?= lang('produksi_finishing')?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
                        <div class="col-sm-6">
                            <label for="ket" class="control-label"><?= lang('produksi_ket') ?></label>
                            <textarea class="form-control" id="ket" name="ket" maxlength="255"><?php echo set_value('ket', isset($data['ket']) ? $data['ket'] : ''); ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <button type="submit" name="save" class="btn btn-primary"><?= lang('produksi_btn_save') ?></button>
                            <?php
                                echo lang('produksi_atau') . ' ' . anchor('produksi/cancel', lang('produksi_btn_cancel'), array("onclick" => "return confirm('".lang('produksi_cancel_confirm')."')"));
                            ?>
                        </div>
                    </div>
                </div><!-- akhir id="bahanbaku" -->
                <div id="spkdetail" class="tab-pane">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group <?= form_error('produksi_nama_pekerjaan') ? ' has-error' : ''; ?>">
                                <label for="nama_pekerjaan" class="col-sm-5 control-label"><?= lang('produksi_nama_pekerjaan') ?></label>
                                <div class="col-sm-7">
                                    <span class="form-control" id="view_nama_pekerjaan"><?= $data_detail->nama_pekerjaan ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= form_error('id_kategori') ? ' has-error' : ''; ?>">
                                <label for="id_kategori" class="col-sm-5 control-label"><?= lang('produksi_id_kategori') ?></label>
                                <div class="col-sm-7">
                                    <span class="form-control" id="view_id_kategori_produk"><?= $data_detail->nmkategori ?></span>
                                </div>
                            </div>
                            <div class="form-group <?= form_error('id_produk') ? ' has-error' : ''; ?>">
                                <label for="id_produk" class="col-sm-5 control-label"><?= lang('produksi_id_produk') ?></label>
                                <div class="col-sm-7">
                                    <span class="form-control" id="view_id_produk"><?= $data_detail->nm_produk?></span>
                                </div>
                            </div>
                            <?php
                                if ($data_detail->target_server==0) {
                                    $src_img = $setting_upload->url.'/'.$data_detail->gambar;
                                }else{  
                                    $src_img = base_url($data_detail->gambar);
                                }
                            ?>
                            <div class="form-group <?= form_error('gambar') ? ' has-error' : ''; ?>">
                                <label for="gambar" class="col-sm-5 control-label"><?= lang('produksi_gambar_cetak') ?></label>
                                <div class="col-sm-7">
                                    <img width="100" height="100" src="<?= $src_img?>" id="view_img" name="view_img">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group <?= form_error('ukuran_cetak') ? ' has-error' : ''; ?>">
                                <label for="ukuran_cetak" class="col-sm-5 control-label"><?= lang('produksi_ukuran_cetak') ?></label>
                                <div class="col-sm-7">
                                    <span class="form-control" id="view_p">
                                        <?=
                                            $data_detail->p."x".$data_detail->l." ".$data_detail->satuan_panjang;
                                        ?>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group <?= form_error('jml_order') ? ' has-error' : ''; ?>">
                                <label for="jml_order" class="col-sm-5 control-label"><?= lang('produksi_jml_order') ?></label>
                                <div class="col-sm-7">
                                    <span class="form-control" id="view_jml_order">
                                        <?= $data_detail->jml_cetak." ".$data_detail->satuan_order?>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group <?= form_error('note_produk') ? ' has-error' : ''; ?>">
                                <label for="note_produk" class="col-sm-5 control-label"><?= lang('produksi_note_produk') ?></label>
                                <div class="col-sm-7">
                                    <textarea id="view_note_produk" disabled class="form-control"><?= $data_detail->catatan?></textarea>
                                </div>
                            </div>
                            <div class="form-group <?= form_error('st_finishing') ? ' has-error' : ''; ?>">
                                <label for="st_finishing" class="col-sm-5 control-label"><?= lang('produksi_st_finishing') ?></label>
                                <div class="col-sm-7">
                                    <span class="form-control" id="view_st_finishing"><?= $data_detail->st_finishing==0 ? 'Finishing Standart':'Finishing Custom';?></span>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <?php if (isset($detail_finishing)):?>
                    <div class="col-md-12" id="detail-hide">
                        <!-- view detail finishing -->
                        <p style="margin-top: 15px; margin-bottom: 3px;">
                            <span class="label label-warning" style="font-size: 12px"><?= lang('order_produk_list_finishing') ?></span>
                        </p>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="view_tdet_finishing" name="view_tdet_finishing">
                                <thead>
                                    <tr class="success">
                                        <th style="vertical-align:middle;"><?= lang('produksi_no') ?></th>
                                        <th style="vertical-align:middle;"><?= lang('produksi_nm_finishing') ?></th>
                                        <th style="vertical-align: middle;"><?= lang('produksi_note_finishing')?></th>
                                        <th width="150px" style="vertical-align:middle;" class="text-center"><?= lang('produksi_jml_pakai_finishing') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no=0; foreach ($detail_finishing as $key => $record):?>
                                        <tr>
                                            <td><?= $no+=1; ?></td>
                                            <td><?= $record->nm_produk ?></td>
                                            <td><?= $record->catatan?></td>
                                            <td><?= $record->jml ." Set"?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php endif;?>
                
                <div class="form-group <?= form_error('produksi_nama_pekerjaan') ? ' has-error' : ''; ?>">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                </div>         

                </div><!-- akhir id="finishing_pakai" -->

            </div><!-- akhir class="tab-content" -->
        </div><!-- akhir class="nav-tabs-custom" -->      
<?= form_close() ?>
<div class="modal" id="modal-barang" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= form_open(site_url('produksi/add_item'),array('id'=>'frm_barang','name'=>'frm_barang','role'=>'form')) ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= lang('produksi_pilih_brg') ?></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" id="cr_barang" name="cr_barang" placeholder="<?= lang('produksi_cari_brg') ?>" />
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-primary" name="cari_btn" id="cari_btn"><i class="fa fa-search"></i> <?= lang('produksi_cari_btn') ?></button>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="tbl_barang" class="table table-hover table-striped">
                            <thead>
                                <tr class="success">
                                    <th><input type="checkbox" id="ck_all" name="ck_all" /></th>
                                    <th><?= lang('produksi_no') ?></th>
                                    <th><?= lang('produksi_barcode') ?></th>
                                    <th><?= lang('produksi_nm_barang') ?></th>
                                    <th><?= lang('produksi_nm_merk') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="pilih_btn" name="pilih_btn"><?= lang('produksi_pilih_btn') ?></button>
                </div>
            <?= form_close() ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" src="<?= base_url('assets/plugins/jquery-ui/jquery-ui.js') ?>"></script>