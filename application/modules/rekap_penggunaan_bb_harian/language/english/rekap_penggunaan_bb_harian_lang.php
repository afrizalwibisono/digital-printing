<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['rekap_penggunaan_bb_harian_title_view'] 	= 'Data Penggunaan Bahan Baku Harian';

// form/table
$lang['rekap_pengunaan_bb_harian_range_tanggal']	= 'Range Tanggal';
$lang['rekap_pengunaan_bb_harian_barcode']			= 'Barcode';
$lang['rekap_pengunaan_bb_harian_nm_barang']		= 'Nama Barang';
$lang['rekap_pengunaan_bb_harian_kategori']			= 'Kategori Bahan Baku';
$lang['rekap_pengunaan_bb_harian_jenis']			= 'Janis Bahan Baku';
$lang['rekap_pengunaan_bb_harian_merk']				= 'Merk Bahan Baku';
$lang['rekap_pengunaan_bb_harian_satuan']			= 'Satuan';
$lang['rekap_pengunaan_bb_harian_other_keyword']	= 'Keyword Lain';
$lang['rekap_pengunaan_bb_harian_roll']				= 'Roll';

$lang['rekap_pengunaan_bb_harian_jml_pakai_produksi'] = 'JML Pakai Produksi';


$lang['rekap_pengunaan_bb_harian_records_found']	= 'Data tidak ditemukan';