<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['module_config'] = array(
	'description'	=> 'Manajemen data identitas perusahaan',
	'author'		=> 'Cokeshome',
	'name'			=> 'Perusahaan',
	'version'		=> '0.1'
);