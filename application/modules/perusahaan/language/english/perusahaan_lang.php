<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['perusahaan_title_manage'] 	= 'Setting Identitas Perusahaan';

// form/table
$lang['perusahaan_name'] 			= 'Nama Perusahaan';
$lang['perusahaan_alias'] 			= 'Alias';
$lang['perusahaan_addr'] 			= 'Alamat';
$lang['perusahaan_kota'] 			= 'Kota';
$lang['perusahaan_telp'] 			= 'Telp';
$lang['perusahaan_fax'] 			= 'Fax';
$lang['perusahaan_email'] 			= 'Email';
$lang['perusahaan_website'] 		= 'Website';
$lang['perusahaan_logo'] 			= 'Logo';
$lang['perusahaan_favicon'] 		= 'Favicon';
$lang['perusahaan_report'] 			= 'Report';

$lang['perusahaan_btn_save'] 		= "Simpan";

$lang['perusahaan_edit_success'] 	= 'Data identitas perusahaan berhasil disimpan';
$lang['perusahaan_edit_failed'] 	= 'Data identitas perusahaan gagal disimpan';
$lang['perusahaan_invalid_id'] 		= 'ID Tidak Valid';