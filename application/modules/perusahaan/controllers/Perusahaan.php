<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for Perusahaan
 */

class Perusahaan extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Perusahaan.View";
    protected $uploadFolder     = "./assets/images/logo";
    protected $uploadFolder2     = "./assets/images/favicon";
    protected $uploadFolder3    = "./assets/images/report";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('perusahaan/perusahaan');
        $this->load->model(array('identitas_model'));

        $this->template->title(lang('perusahaan_title_manage'));
		$this->template->page_icon('fa fa-building');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if(isset($_POST['save']))
        {
            if($this->save_perusahaan())
            {
                $this->template->set_message(lang('perusahaan_edit_success'), 'success');
                redirect('perusahaan', 'refresh');
            }
        }

        $idt        = $this->identitas_model->find(1);

        $this->template->set('data', $idt);

        $this->template->render('index'); 
    }

   	protected function save_perusahaan()
   	{
        $data = $this->identitas_model->find(1);

        $this->form_validation->set_rules('nm_perusahaan','lang:perusahaan_name','required|trim|max_length[100]');
        $this->form_validation->set_rules('alias','lang:perusahaan_alias','required|trim|max_length[3]');
        $this->form_validation->set_rules('alamat','lang:perusahaan_addr','required|trim|max_length[255]');
        $this->form_validation->set_rules('kota','lang:perusahaan_kota','trim|max_length[45]');
        $this->form_validation->set_rules('no_telp','lang:perusahaan_telp','required|trim|max_length[15]');
        $this->form_validation->set_rules('fax','lang:perusahaan_fax','trim|max_length[15]');
        $this->form_validation->set_rules('email','lang:perusahaan_email','trim|valid_email|max_length[45]');
        $this->form_validation->set_rules('website','lang:perusahaan_website','trim|max_length[45]');
        if(!$_FILES['logo']['name'] && !file_exists($this->uploadFolder."/".$data->logo))
        {
            $this->form_validation->set_rules('logo','lang:perusahaan_logo','required');
        }

        if(!$_FILES['favicon']['name'] && !file_exists($this->uploadFolder2."/".$data->favicon))
        {
            $this->form_validation->set_rules('favicon','lang:perusahaan_favicon','required');
        }

        if(!$_FILES['report']['name'] && !file_exists($this->uploadFolder3."/".$data->report))
        {
            $this->form_validation->set_rules('report','lang:perusahaan_report','required');
        }

        if ($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        unset($_POST['save']);

        $logo       = $_FILES['logo']['name'];
        $filex1     = explode(".", $logo);
        $ext        = end($filex1);
        $logo_name  = "logo.".$ext;

        $favicon    = $_FILES['favicon']['name'];
        $filex2     = explode(".", $favicon);
        $ext2       = end($filex2);
        $favicon_name  = "favicon.".$ext2;

        $report     = $_FILES['report']['name'];
        $filex3     = explode(".", $report);
        $ext3       = end($filex3);
        $report_name  = "report.".$ext3;

        $update_data = array();

        $this->load->library('upload');

        if($logo!=""){

            @unlink($this->uploadFolder."/".$logo_name);

            $config['upload_path']   = $this->uploadFolder;
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_size']      = '0';
            $config['max_width']     = '2000'; 
            $config['max_height']    = '2000';
            $config['file_ext_tolower']  = TRUE;
            $config['file_name']     = "logo";
            $config['overwrite']     = TRUE;

            $this->upload->initialize($config);

            if ( ! $this->upload->do_upload("logo")){
                
                $this->template->set_message($this->upload->display_errors(), 'error');
                return FALSE;               
            }

            $update_data['logo'] = $logo_name;
        }

        /*---Favicon---*/
        if($favicon!=""){

            @unlink($this->uploadFolder2."/".$favicon_name);

            $config['upload_path']   = $this->uploadFolder2;
            $config['allowed_types'] = 'jpeg|jpg|png|ico';
            $config['max_size']      = '0';
            $config['max_width']     = '32'; 
            $config['max_height']    = '32';
            $config['file_ext_tolower']  = TRUE;
            $config['file_name']     = "favicon";
            $config['overwrite']     = TRUE;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload("favicon")){
                $this->template->set_message($this->upload->display_errors(), 'error');
                return FALSE;         
            }

            $update_data['favicon'] = $favicon_name;  
        }

        /*---report---*/
        if($report!=""){

            @unlink($this->uploadFolder3."/".$report_name);

            $config['upload_path']   = $this->uploadFolder3;
            $config['allowed_types'] = 'jpeg|jpg|png';
            $config['max_size']      = '0';
            $config['max_width']     = '200'; 
            $config['max_height']    = '200';
            $config['file_ext_tolower']  = TRUE;
            $config['file_name']     = "report";
            $config['overwrite']     = TRUE;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload("report")){
                $this->template->set_message($this->upload->display_errors(), 'error');
                return FALSE;         
            }

            $update_data['report'] = $report_name;  
        }

        $update_data['nm_perusahaan']   = $this->input->post('nm_perusahaan');
        $update_data['alias']           = $this->input->post('alias');
        $update_data['alamat']          = $this->input->post('alamat');
        $update_data['kota']            = $this->input->post('kota');
        $update_data['no_telp']         = $this->input->post('no_telp');
        $update_data['fax']             = $this->input->post('fax');
        $update_data['email']           = $this->input->post('email');
        $update_data['website']         = prep_url($this->input->post('website'));

        $idt = $this->identitas_model->update(1, $update_data);

        if(!$idt)
        {
            $this->template->set_message(lang('perusahaan_edit_failed'), 'error');
        }

        return $idt;
   	}
}
