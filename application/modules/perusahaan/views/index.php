<div class="box box-primary">
    <!-- form start -->
    <?= form_open_multipart($this->uri->uri_string(),array('id'=>'frm_perusahaan','name'=>'frm_perusahaan','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
    		<div class="col-md-6">
    			<div class="form-group <?= form_error('nm_perusahaan') ? ' has-error' : ''; ?>">
				    <label for="nm_perusahaan" class="col-md-4 control-label"><?= lang('perusahaan_name') ?></label>
				    <div class="col-md-8">
				    	<input type="text" class="form-control" id="nm_perusahaan" name="nm_perusahaan" maxlength="45" value="<?php echo set_value('nm_perusahaan', isset($data->nm_perusahaan) ? $data->nm_perusahaan : ''); ?>" required autofocus>
				    </div>
			  	</div>
			  	<div class="form-group <?= form_error('alias') ? ' has-error' : ''; ?>">
				    <label for="alias" class="col-md-4 control-label"><?= lang('perusahaan_alias') ?></label>
				    <div class="col-md-2">
				    	<input type="text" class="form-control" id="alias" name="alias" maxlength="3" value="<?php echo set_value('alias', isset($data->alias) ? $data->alias : ''); ?>" required>
				    </div>
			  	</div>
			  	<div class="form-group <?= form_error('alamat') ? ' has-error' : ''; ?>">
				    <label for="alamat" class="col-md-4 control-label"><?= lang('perusahaan_addr') ?></label>
				    <div class="col-sm-8">
				    	<textarea class="form-control" id="alamat" name="alamat" maxlength="255" required=""><?php echo set_value('alamat', isset($data->alamat) ? $data->alamat : ''); ?></textarea>
				    </div>
			  	</div>
			  	<div class="form-group <?= form_error('kota') ? ' has-error' : ''; ?>">
				    <label for="kota" class="col-md-4 control-label"><?= lang('perusahaan_kota') ?></label>
				    <div class="col-md-6">
				    	<input type="text" class="form-control" id="kota" name="kota" maxlength="15" value="<?php echo set_value('kota', isset($data->kota) ? $data->kota : ''); ?>" required="">
				    </div>
			  	</div>
			  	<div class="form-group <?= form_error('no_telp') ? ' has-error' : ''; ?>">
				    <label for="no_telp" class="col-md-4 control-label"><?= lang('perusahaan_telp') ?></label>
				    <div class="col-md-5">
				    	<input type="text" class="form-control" id="no_telp" name="no_telp" maxlength="15" value="<?php echo set_value('no_telp', isset($data->no_telp) ? $data->no_telp : ''); ?>">
				    </div>
			  	</div>
			  	<div class="form-group <?= form_error('fax') ? ' has-error' : ''; ?>">
				    <label for="fax" class="col-md-4 control-label"><?= lang('perusahaan_fax') ?></label>
				    <div class="col-md-5">
				    	<input type="text" class="form-control" id="fax" name="fax" maxlength="15" value="<?php echo set_value('fax', isset($data->fax) ? $data->fax : ''); ?>">
				    </div>
			  	</div>
			  	<div class="form-group <?= form_error('email') ? ' has-error' : ''; ?>">
				    <label for="email" class="col-md-4 control-label"><?= lang('perusahaan_email') ?></label>
				    <div class="col-md-6">
				    	<input type="text" class="form-control" id="email" name="email" maxlength="45" value="<?php echo set_value('email', isset($data->email) ? $data->email : ''); ?>">
				    </div>
			  	</div>
			  	<div class="form-group <?= form_error('website') ? ' has-error' : ''; ?>">
				    <label for="website" class="col-md-4 control-label"><?= lang('perusahaan_website') ?></label>
				    <div class="col-md-6">
				    	<input type="text" class="form-control" id="website" name="website" maxlength="45" value="<?php echo set_value('website', isset($data->website) ? $data->website : ''); ?>" required="">
				    </div>
			  	</div>
			  	<div class="form-group">
		            <label for="logo" class="col-md-4 control-label"><?= lang('perusahaan_logo') ?></label>
		            <div class="col-md-5">
		                <input type="file" id="logo" name="logo" accept="image/png, image/jpg, image/jpeg">
		                <div id="imgcontainer" style="width:70px;height:70px;border:1px solid #000000;text-align:center;float:left;margin-top:10px;">
		                    <img id="img-logo" src="<?= isset($data->logo) && $data->logo!="" && file_exists('assets/images/logo/'.$data->logo) ? base_url('assets/images/logo/'.$data->logo) : '' ?>" alt="Logo" width="68px" height="68px">
		                </div>
		                <div class="clearfix"></div>
		                <span class="help-block text-red">Dimensi logo : 200px X 200px (WxH),<br>Ext : jpeg, jpg, png</span>
		            </div>
		        </div>
		        <div class="form-group">
		            <label for="favicon" class="col-md-4 control-label"><?= lang('perusahaan_favicon') ?></label>
		            <div class="col-md-5">
		                <input type="file" id="favicon" name="favicon" accept="image/png, image/jpg, image/jpeg, image/x-icon">
		                <div id="imgcontainer" style="width:70px;height:70px;border:1px solid #000000;text-align:center;float:left;margin-top:10px;">
		                    <img id="img-favicon" src="<?= isset($data->favicon) && $data->favicon!="" && file_exists('assets/images/favicon/'.$data->favicon) ? base_url('assets/images/favicon/'.$data->favicon) : '' ?>" alt="Favicon" width="68px" height="68px">
		                </div>
		                <div class="clearfix"></div>
		                <span class="help-block text-red">Dimensi favicon : 32px X 32px (WxH),<br>Ext : jpeg, jpg, png, ico</span>
		            </div>
		        </div>
		        <div class="form-group">
		            <label for="report" class="col-md-4 control-label"><?= lang('perusahaan_report') ?></label>
		            <div class="col-md-5">
		                <input type="file" id="report" name="report" accept="image/png, image/jpg, image/jpeg">
		                <div id="imgcontainer" style="width:70px;height:70px;border:1px solid #000000;text-align:center;float:left;margin-top:10px;">
		                    <img id="img-report" src="<?= isset($data->report) && $data->report!="" && file_exists('assets/images/report/'.$data->report) ? base_url('assets/images/report/'.$data->report) : '' ?>" alt="Report" width="68px" height="68px">
		                </div>
		                <div class="clearfix"></div>
		                <span class="help-block text-red">Dimensi report : 200px X 200px (WxH),<br>Ext : jpeg, jpg, png</span>
		            </div>
		        </div>
		        <div class="form-group">
		        	<div class="col-md-2 col-md-offset-4">
		        		<button type="submit" name="save" class="btn btn-primary"><i class="fa fa-save"></i> <?= lang('perusahaan_btn_save') ?></button>
		        	</div>
		        </div>
    		</div>
    		<div class="col-md-6">
			  	
    		</div>
	  	</div>
		<div class="box-footer">
		  	
		</div>
	<?= form_close() ?>
</div><!-- /.box -->
<script type="text/javascript">
	$(document).ready(function(){
        //Load Foto
        $("#logo").change(function(){
            loadfoto(this,'img-logo');
        });

        $("#favicon").change(function(){
            loadfoto(this,'img-favicon');
        });

        $("#report").change(function(){
            loadfoto(this,'img-report');
        });
    });
</script>