<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_laporan','name'=>'frm_laporan','class' => 'form-inline')) ?>
        <div class="box-header hidden-print">
            <div class="pull-left">
                <button type="button" id="cetak" class="btn btn-default" onclick="cetak_halaman()"><i class="fa fa-print"></i> <?= lang('barang_masuk_cetak') ?></button>
            </div>
            <div class="pull-right">
                <div class="form-group">
                    <div class="input-daterange input-group">
                        <input type="text" class="form-control" name="tgl_awal" id="tgl_awal" value="<?= isset($period1) ? $period1 : '' ?>" placeholder="<?= lang('barang_masuk_tgl_awal') ?>" required />
                        <span class="input-group-addon">to</span>
                        <input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?= isset($period2) ? $period2 : '' ?>" placeholder="<?= lang('barang_masuk_tgl_akhir') ?>" required />
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="<?= lang('barang_masuk_nm_barang') ?>" autofocus />
                        <div class="input-group-btn">
                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?= form_close() ?>
    <?php if($idt) : ?>
    <div class="row header-laporan">
        <div class="col-md-12">
            <div class="col-md-1 laporan-logo">
                <img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo">
            </div>
            <div class="col-md-6 laporan-identitas">
                <address>
                    <h4><?= $idt->nm_perusahaan ?></h4>
                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?>
                </address>
            </div>
            <div class="col-md-5 text-right">
                <h3>Laporan Barang Masuk</h3>
                <h5>Periode <?= $period1." - ".$period2 ?></h5>
            </div>
            <div class="clearfix"></div>
            <div class="laporan-garis"></div>
        </div>
    </div>
    <?php endif ?>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
    	<div class="box-body laporan-body">
            <?php
                $total_tran = 0;
                foreach ($results as $keySummary => $rs) {
                    $total_tran += $rs->total;
                }
            ?>
            <h4><?= lang('barang_masuk_ringkasan') ?></h4>

            <table class="table-condensed table-summary">
                <tr>
                    <td width="200"><?= lang('barang_masuk_total_tran_qty') ?></td>
                    <td width="10">:</td>
                    <td><strong><?= number_format($keySummary+1) ?> <?= lang('barang_masuk_kali') ?></strong></td>
                </tr>
                <tr>
                    <td width="200"><?= lang('barang_masuk_total_tran') ?></td>
                    <td width="10">:</td>
                    <td><strong>Rp. <?= number_format($total_tran) ?></strong></td>
                </tr>
            </table>

            <h4><?= lang('barang_masuk_detail') ?></h4>

            <?php foreach ($results as $key => $dt): ?>
                <div class="table-responsive no-padding">
                    <table class="table-condensed table-head">
                        <tr>
                            <td><?= lang('barang_masuk_no_faktur') ?></td>
                            <td>:</td>
                            <td><strong><?= $dt->no_faktur ?></strong></td>
                            <td><?= lang('barang_masuk_operator') ?></td>
                            <td>:</td>
                            <td><?= ucwords($dt->operator) ?></td>
                            <td><?= lang('barang_masuk_ket') ?></td>
                            <td>:</td>
                            <td><?= $dt->ket ?></td>
                        </tr>
                        <tr>
                            <td><?= lang('barang_masuk_tanggal') ?></td>
                            <td>:</td>
                            <td><?= date('d F Y H:i:s', strtotime($dt->created_on)) ?></td>
                            <td><?= lang('barang_masuk_gudang') ?></td>
                            <td>:</td>
                            <td><?= ucwords($dt->nama_gudang) ?></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </div>
                <div class="table-responsive no-padding">
                    <table class="table table-condensed table-bordered table-detail">
                        <thead>
                            <tr class="bg-success">
                                <th width="50">#</th>
                                <th><?= lang('barang_masuk_barcode') ?></th>
                                <th><?= lang('barang_masuk_nm_barang') ?></th>
                                <th><?= lang('barang_masuk_qty') ?></th>
                                <th><?= lang('barang_masuk_satuan') ?></th>
                                <th class="text-right"><?= lang('barang_masuk_harga') ?></th>
                                <th class="text-right"><?= lang('barang_masuk_total') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $total = 0;
                                foreach ($dt->detail as $key2 => $record) : 
                            ?>
                            <tr>
                                <td><?= $key2+1 ?></td>
                                <td><?= $record->barcode ?></td>
                                <td><?= $record->nm_barang ?></td>
                                <td><?= number_format($record->qty) ?></td>
                                <td><?= $record->satuan ?></td>
                                <td align="right"><?= number_format($record->harga) ?></td>
                                <td align="right"><strong><?= number_format($record->sub_total) ?></strong></td>
                            </tr>
                            <?php $total += $record->sub_total; 
                                endforeach;
                            ?>
                            <tr class="border-top">
                                <td colspan="6" align="right"><strong><?= lang('barang_masuk_gtotal') ?></strong></td>
                                <td align="right"><strong>Rp. <?= number_format($total) ?></strong></td>
                            </tr>
                        </tbody>
        	       </table>
               </div>
          <?php endforeach ?>
    	</div><!-- /.box-body -->
        
	<?php else: ?>
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('barang_masuk_no_records_found') ?></p>
    </div>
    <?php endif;?>
</div><!-- /.box -->