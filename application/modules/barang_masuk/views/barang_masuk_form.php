<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_barang_masuk','name'=>'frm_barang_masuk','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="form-group">
			    <div class="col-md-3 <?= form_error('no_faktur') || form_error('kd_retur_jual') ? ' has-error' : ''; ?>">
			    	<label for="no_faktur" class="control-label"><?= lang('barang_masuk_no_faktur') ?></label>
			    	<input type="text" class="form-control" id="no_faktur" name="no_faktur" maxlength="100" value="<?= $data['no_faktur'] ?>" readonly>
			    </div>
			    <div class="col-md-3 <?= form_error('id_gudang_tujuan') ? ' has-error' : ''; ?>">
			    	<label for="id_gudang_tujuan" class="control-label"><?= lang('barang_masuk_gudang') ?></label>
			    	<select name="id_gudang_tujuan" id="id_gudang_tujuan" class="form-control">
			    		<option></option>
			    		<?php foreach ($gudang as $key => $gd) : ?>
			    		<option value="<?= $gd->id_gudang ?>" <?= set_select('id_gudang_tujuan', $gd->id_gudang, isset($data['id_gudang_tujuan']) &&  $data['id_gudang_tujuan'] == $gd->id_gudang) ?>><?= ucwords($gd->nama_gudang) ?></option>
			    		<?php endforeach ?>
			    	</select>
			    </div>
			    <div class="col-md-3 <?= form_error('no_faktur_keluar') ? ' has-error' : ''; ?> masuk-biasa">
			    	<label for="no_faktur_keluar" class="control-label"><?= lang('barang_masuk_no_faktur_keluar') ?></label>
			    	<input type="text" class="form-control" id="no_faktur_keluar" name="no_faktur_keluar" maxlength="100" value="<?= set_value('no_faktur_keluar') ?>" placeholder="Ketik dan tekan Enter" autofocus>
			    </div>
			    <div class="col-md-3 <?= form_error('tgl_masuk') ? ' has-error' : ''; ?>">
			    	<label for="tgl_masuk" class="control-label"><?= lang('barang_masuk_tanggal') ?></label>
			    	<input type="text" class="form-control" id="tgl_masuk" name="tgl_masuk" maxlength="10" value="<?= $data['tgl_masuk'] ?>" placeholder="dd/mm/yyyy" readonly>
			    </div>
		  	</div>
		  	<div class="table-responsive">
		  		<table class="table table-bordered" id="tdet_masuk">
		  			<thead>
		  				<tr>
			  				<th width="50"><?= lang('barang_masuk_no') ?></th>
			  				<th><?= lang('barang_masuk_barcode') ?></th>
			  				<th><?= lang('barang_masuk_nm_barang') ?></th>
			  				<th><?= lang('barang_masuk_qty') ?></th>
			  				<th><?= lang('barang_masuk_satuan') ?></th>
			  				<?php if($data['no_faktur_keluar'] == ''): ?>
			  				<th class="brkeluar"><?= lang('barang_masuk_harga') ?></th>
			  				<th class="brkeluar"><?= lang('barang_masuk_total') ?></th>
			  				<?php endif; ?>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($data['items']) : ?>
		  					<?php 
		  						$no = 1;
		  						foreach($data['items'] as $key => $dt) : 
		  					?>
		  				<tr>
		  					<td><?= $no++ ?></td>
		  					<td>
		  						<?= $dt['barcode'] ?>
		  						<input type="hidden" name="id_barang[]" value="<?= $dt['id_barang'] ?>" />
		  					</td>
		  					<td><?= $dt['nm_barang'] ?> - <a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>
		  					<td><input type="number" min="1" name="qty[]" class="form-control" value="<?= $dt['qty'] ?>" <?= $data['no_faktur_keluar'] != '' ? 'readonly=""' : '' ?> /></td>
		  					<td>
		  						<select class='form-control' name='konversi[]' style="width: 100%" <?= $data['no_faktur_keluar'] != '' ? 'disabled=""' : '' ?>>
		  							<?php 
		  								if($dt['konversi']) :
		  									foreach ($dt['konversi'] as $key => $kf) :
		  							?>
		  							<option value="<?= $kf->id_konversi ?>" <?= (isset($kf->id_konversi) && $kf->id_konversi == $dt['id_konversi']) ? "selected" : "" ?> data-jmlkecil="<?= $kf->jml_kecil ?>"><?= $kf->tampil2 ?></option>
		  							<?php
		  									endforeach; 
		  								endif; 
		  							?>
		  						</select>
		  					</td>
		  					<?php if($data['no_faktur_keluar'] == ''): ?>
		  					<td><input type="text" name="harga[]" class="form-control" value="<?= number_format($dt['harga']) ?>" /></td>
		  					<td align="right"><?= number_format($dt['total']) ?></td>
		  					<?php endif; ?>	
		  				</tr>
		  					<?php endforeach; ?>
		  				<?php endif; ?>
		  			</tbody>
		  			<?php if($data['no_faktur_keluar'] == ''): ?>
		  			<tfoot>
		  				<tr>
		  					<td colspan="6">
		  						<div class="input-group" id="barcode_input">
		  							<div class="input-group-btn">
		  								<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-barang"><i class="fa fa-plus-square"></i></button>
		  							</div>
		  							<input type="text" class="form-control" id="barcode" name="barcode" placeholder="<?= lang('barang_masuk_scan_brg') ?>" autocomplete="off" autofocus />
		  						</div>
		  						<span class="help-block text-green" id="barcode_msg"></span>
		  					</td>
		  				</tr>
		  				<tr class="info">
		  					<td colspan="6" align="right"><strong><?= lang('barang_masuk_gtotal') ?></strong></td>
		  					<td align="right"><strong>Rp. <label id="lblgtotal"><?= number_format($data['gtotal']) ?></label></strong></td>
		  				</tr>
		  			</tfoot>
		  			<?php endif; ?>
		  		</table>
		  	</div>
		  	<div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
			    <div class="col-sm-6">
			    	<label for="ket" class="control-label"><?= lang('barang_masuk_ket') ?></label>
			    	<textarea class="form-control" id="ket" name="ket" maxlength="255"><?php echo set_value('ket', isset($data['ket']) ? $data['ket'] : ''); ?></textarea>
			    </div>
		  	</div>
		  	<div class="form-group">
			    <div class="col-md-12">
			      	<button type="submit" name="save" class="btn btn-primary"><?= lang('barang_masuk_btn_save') ?></button>
			    	<?php
	                	echo lang('barang_masuk_or') . ' ' . anchor('barang_masuk/cancel', lang('barang_masuk_btn_cancel'), array("onclick" => "return confirm('".lang('barang_masuk_cancel_confirm')."')"));
	                ?>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->
<div class="modal" id="modal-barang" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
	    <div class="modal-content">
	    	<?= form_open(site_url('barang_masuk/add_item'),array('id'=>'frm_barang','name'=>'frm_barang','role'=>'form')) ?>
		      	<div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        	<h4 class="modal-title"><?= lang('barang_masuk_pilih_brg') ?></h4>
		      	</div>
		      	<div class="modal-body">
		      		<div class="form-group">
		      			<div class="input-group">
		      				<input type="text" class="form-control" id="cr_barang" name="cr_barang" placeholder="<?= lang('barang_masuk_cari_brg') ?>" />
		      				<div class="input-group-btn">
		      					<button type="button" class="btn btn-primary" name="cari_btn" id="cari_btn"><i class="fa fa-search"></i> <?= lang('barang_masuk_cari_btn') ?></button>
		      				</div>
		      			</div>
		      		</div>
		        	<div class="table-responsive">
		        		<table id="tbl_barang" class="table table-hover table-striped">
		        			<thead>
		        				<tr class="success">
			        				<th><input type="checkbox" id="ck_all" name="ck_all" /></th>
			        				<th><?= lang('barang_masuk_no') ?></th>
			        				<th><?= lang('barang_masuk_barcode') ?></th>
			        				<th><?= lang('barang_masuk_nm_barang') ?></th>
			        			</tr>
		        			</thead>
		        			<tbody>
		        				
		        			</tbody>
		        		</table>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
		      		<button type="button" class="btn btn-primary" id="pilih_btn" name="pilih_btn"><?= lang('barang_masuk_pilih_btn') ?></button>
		      	</div>
	      	<?= form_close() ?>
	    </div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->