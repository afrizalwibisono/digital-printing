<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_barang_masuk','name'=>'frm_barang_masuk','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="form-group">
			    <div class="col-md-3">
			    	<label for="no_faktur" class="control-label"><?= lang('barang_masuk_no_faktur') ?></label>
			    	<input type="text" class="form-control" id="no_faktur" name="no_faktur" maxlength="100" value="<?= $no_faktur ?>" autofocus readonly>
			    </div>
		  		<div class="col-md-3">
		  			<label for="id_gudang_tujuan" class="control-label"><?= lang('barang_masuk_gudang') ?></label>	
		  			<input type="text" name="id_gudang_tujuan" class="form-control" value="<?= ucwords($data->nama_gudang) ?>" readonly>
		  		</div>
		  		<div class="col-md-3">
		  			<label for="no_faktur_keluar" class="control-label"><?= lang('barang_masuk_no_faktur_keluar') ?></label>	
		  			<input type="text" name="no_faktur_keluar" class="form-control" value="<?= $data->no_faktur_keluar ?>" readonly>
		  		</div>
		  		<div class="col-md-3">
			    	<label for="tgl_masuk" class="control-label"><?= lang('barang_masuk_tanggal') ?></label>
			    	<input type="text" class="form-control" id="tgl_masuk" name="tgl_masuk" maxlength="10" value="<?= date('d/m/Y', strtotime($data->tgl_masuk)) ?>" placeholder="dd/mm/yyyy" readonly>
			    </div>
		  	</div>
		  	<div class="table-responsive">
		  		<table class="table table-bordered" id="tdet_masuk">
		  			<thead>
		  				<tr class="success">
			  				<th width="50"><?= lang('barang_masuk_no') ?></th>
			  				<th><?= lang('barang_masuk_barcode') ?></th>
			  				<th><?= lang('barang_masuk_nm_barang') ?></th>
			  				<th><?= lang('barang_masuk_qty') ?></th>
			  				<th><?= lang('barang_masuk_satuan') ?></th>
			  				<th><?= lang('barang_masuk_harga') ?></th>
			  				<th><?= lang('barang_masuk_total') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($detail) : ?>
		  					<?php foreach($detail as $key => $dt) : ?>
		  				<tr>
		  					<td><?= $key+1 ?></td>
		  					<td><?= $dt->barcode ?></td>
		  					<td><?= $dt->nm_barang ?></td>
		  					<td><?= number_format($dt->qty) ?></td>
		  					<td><?= $dt->satuan ?></td>
		  					<td><?= number_format($dt->harga) ?></td>
		  					<td align='right'><?= number_format($dt->sub_total) ?></td>
		  				</tr>
		  					<?php endforeach; ?>
		  				<?php endif; ?>
		  			</tbody>
		  			<tfoot>
		  				<tr class="info">
		  					<td colspan="6" align="right"><strong><?= lang('barang_masuk_gtotal') ?></strong></td>
		  					<td align="right"><strong>Rp. <label id="lblgtotal"><?= number_format($data->total) ?></label></strong></td>
		  				</tr>
		  			</tfoot>
		  		</table>
		  	</div>
		  	<div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
			    <div class="col-sm-7">
			    	<label for="ket" class="control-label"><?= lang('barang_masuk_ket') ?></label>
			    	<textarea class="form-control" id="ket" name="ket" maxlength="255" readonly=""><?= $data->ket ?></textarea>
			    </div>
		  	</div>
		  	<div class="form-group">
		  		<div class="col-md-12">
		  			<?php echo anchor('barang_masuk', lang('barang_masuk_btn_back'), array('class' =>'btn btn-success')) ?>
		  		</div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->