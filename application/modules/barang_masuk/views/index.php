<?php 
    $ENABLE_ADD     = has_permission('Barang Masuk.Add');
    $ENABLE_MANAGE  = has_permission('Barang Masuk.Manage');
    $ENABLE_DELETE  = has_permission('Barang Masuk.Delete');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_barang_masuk','name'=>'frm_barang_masuk')) ?>
        <div class="box-header">
            <div class="form-group">
                <?php if ($ENABLE_ADD) : ?>
	  	        <a href="<?= site_url('barang_masuk/create') ?>" class="btn btn-success" title="<?= lang('barang_masuk_btn_new') ?>"><?= lang('barang_masuk_btn_new') ?></a>
                <?php endif; ?>
                <div class="pull-right form-inline">
                    <div class="input-daterange input-group" data-toggle="tooltip" data-placement="top" title="<?= lang('barang_masuk_range') ?>">
                        <input type="text" class="form-control" name="tgl_awal" id="tgl_awal" value="<?= $tgl_awal != '' ? $tgl_awal : '' ?>" placeholder="<?= lang('barang_masuk_tgl_awal') ?>" />
                        <span class="input-group-addon">to</span>
                        <input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?= $tgl_akhir != '' ? $tgl_akhir : '' ?>" placeholder="<?= lang('barang_masuk_tgl_akhir') ?>" />
                    </div>
                    <div class="input-group">
                        <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="Search" autofocus />
                        <div class="input-group-btn">
                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
	<div class="box-body table-responsive no-padding">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
                        <th width="50">#</th>
                        <th><?= lang('barang_masuk_no_faktur') ?></th>
                        <th><?= lang('barang_masuk_tanggal') ?></th>
                        <th><?= lang('barang_masuk_gudang') ?></th>
                        <th><?= lang('barang_masuk_dibuat_oleh') ?></th>
                        <th><?= lang('barang_masuk_ket') ?></th>
                        <?php if($ENABLE_MANAGE) : ?>
                        <th width="25"></th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($results as $record) : ?>
                    <tr>
                        <td class="column-check"><input type="checkbox" name="checked[]" value="<?= $record->no_faktur ?>" /></td>
                        <td><?= $numb; ?></td>
                        <td><?= $record->no_faktur ?></td>
                        <td><?= date('d/m/Y', strtotime($record->tgl_masuk)) ?></td>
                        <td><?= $record->nama_gudang ?></td>
                        <td><?= $record->nm_lengkap ?></td>
                        <td><?= $record->ket ?></td>
                        <?php if($ENABLE_MANAGE) : ?>
                        <td style="padding-right:20px"><a class="text-black" href="<?= site_url('barang_masuk/view/' . $record->no_faktur); ?>" data-toggle="tooltip" data-placement="left" title="View Detail"><i class="fa fa-folder"></i></a></td>
                        <?php endif; ?>
                    </tr>
                    <?php $numb++; endforeach; ?>
                </tbody>
	  </table>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">
		<?php if($ENABLE_DELETE) : ?>
		<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('barang_masuk_btn_delete') ?>" onclick="return confirm('<?= (lang('barang_masuk_delete_confirm')); ?>')">
		<?php endif;
		echo $this->pagination->create_links(); 
		?>
	</div>
	<?php else: ?>
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('barang_masuk_no_records_found') ?></p>
    </div>
    <?php
	endif;
	echo form_close(); 
	?>
</div><!-- /.box -->