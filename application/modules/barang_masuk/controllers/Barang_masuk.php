<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for Barang masuk
 */

class Barang_masuk extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Barang Masuk.View";
    protected $addPermission    = "Barang Masuk.Add";
    protected $managePermission = "Barang Masuk.Manage";
    protected $deletePermission = "Barang Masuk.Delete";
    protected $laporanPermission = "Laporan Barang Masuk.View";

    protected $prefixKey        = "FM";
    protected $prefixKeyStok    = "STK";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('barang_masuk/barang_masuk');
        $this->load->model(array('barang_masuk/barang_masuk_model',
                                'barang_masuk/barang_masuk_detail_model',
                                'bahan_baku/bahan_baku_model',
                                'barang_keluar/barang_keluar_model',
                                'barang_keluar/barang_keluar_detail_model',
                                'barang_keluar/barang_keluar_detail_hpp_model',
                                'stok_model',
                                'identitas_model',
                                'gudang/gudang_model',
                            ));

        $this->template->title(lang('barang_masuk_title_manage'));
		$this->template->page_icon('fa fa-cube');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if (isset($_POST['delete']) && has_permission($this->deletePermission))
        {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked))
            {
                $result = FALSE;
                foreach ($checked as $pid)
                {
                    //Cek apakah masih bisa dihapus
                    $cek         = $this->cek_hapus($pid);
                    $query_hapus = "";
                    $total       = 0;
                    if($cek)
                    {
                        $this->stok_model->delete_where(array('no_faktur' => $pid));
                        $query_hapus .= $this->db->last_query();

                        $brg_masuk  = $this->barang_masuk_model->find($pid);
                        $total      = $brg_masuk->total;
                        $result     = $this->barang_masuk_model->delete($pid);
                        $query_hapus .= $this->db->last_query();

                        if($result)
                        {
                            $keterangan = "SUKSES, hapus data barang masuk dengan ID : ".$pid;
                            $status     = 1;
                        }
                        else
                        {
                            $keterangan = "GAGAL, hapus data barang masuk dengan ID : ".$pid;
                            $status     = 0;
                        } 
                    }
                    else
                    {
                        $err        = "karena stok sudah berubah";
                        $keterangan = "GAGAL, hapus data barang masuk dengan ID : ".$pid." karena stok sudah berubah";
                        $status     = 0;
                    }

                    $nm_hak_akses   = $this->deletePermission; 
                    $kode_universal = $pid;
                    $jumlah         = $total;
                    $sql            = $query_hapus;

                    $hasil = simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                }

                if ($result)
                {
                    $this->template->set_message(count($checked) .' '. lang('barang_masuk_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('barang_masuk_del_failure') . (isset($err) ? $err." " : "") . $this->barang_masuk_model->error, 'error');
                }
            }
            else
            {
                $this->template->set_message(lang('barang_masuk_del_error').$this->barang_masuk_model->error, 'error');
            }

            unset($_POST['delete']);
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
            $tgl_awal  = $this->input->post('tgl_awal');
            $tgl_akhir = $this->input->post('tgl_akhir');
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
            $tgl_awal  = $this->input->get('tgl_awal');
            $tgl_akhir = $this->input->get('tgl_akhir');
        }

        $filter = "?search=".urlencode($search);
        $search2 = $this->db->escape_str($search);

        $add_where = "";
        if($tgl_awal !='' && $tgl_akhir !='')
        {
            $filter    .= "&tgl_awal=".urlencode(date_ymd($tgl_awal))."&tgl_akhir=".urlencode(date_ymd($tgl_akhir));
            $add_where .= " AND (barang_masuk.tgl_masuk >='".date_ymd($tgl_awal)."' AND barang_masuk.tgl_masuk <='".date_ymd($tgl_akhir)."')";
        }
        
        $where = "`barang_masuk.deleted` = 0 $add_where
                AND (`no_faktur` LIKE '%$search2%' ESCAPE '!'
                OR `nama_gudang` LIKE '%$search2%' ESCAPE '!'
                OR `total` LIKE '%$search2%' ESCAPE '!'
                OR `barang_masuk`.`ket` LIKE '%$search2%' ESCAPE '!')";
        
        $total = $this->barang_masuk_model
                    ->join("gudang","barang_masuk.id_gudang = gudang.id_gudang","left")
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->barang_masuk_model
                    ->select("barang_masuk.*, gudang.nama_gudang, karyawan.nama as nm_lengkap")
                    ->join("gudang","barang_masuk.id_gudang = gudang.id_gudang","left")
                    ->join("users","barang_masuk.created_by = users.id_user","left")
                    ->join("karyawan","users.id_user = karyawan.id_user","left")
                    ->where($where)
                    ->order_by(['created_on' => 'DESC', 'tgl_masuk' => 'DESC'])
                    ->limit($limit, $offset)->find_all();

        $assets = array(
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'barang_masuk/assets/js/barang_masuk_index.js'
                        );

        add_assets($assets);

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('tgl_awal', $tgl_awal);
        $this->template->set('tgl_akhir', $tgl_akhir);
        $this->template->set("toolbar_title", lang('barang_masuk_title_manage'));
        $this->template->set("page_title", lang('barang_masuk_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

    protected function cek_hapus($no_faktur = '')
    {
        if($no_faktur == '')
        {
            return FALSE;
        }
        
        $data = $this->stok_model->find_all_by('no_faktur', $no_faktur);
        if(!$data)
        {
            return TRUE;
        }

        $st_bisa = TRUE;
        foreach ($data as $key => $dt) {
            if($dt->stok_datang != $dt->stok_update)
            {
                $st_bisa = FALSE;
                break;
            }
        }

        return $st_bisa;
    }

   	//Create Barang Masuk
   	public function create()
   	{
        $this->auth->restrict($this->addPermission);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_barang_masuk())
            {
              $this->template->set_message(lang("barang_masuk_create_success"), 'success');
              redirect('barang_masuk');
            }
        }

        //cek session data
        $data = $this->session->userdata('br_masuk');
        if(!$data)
        {
            $data = array('br_masuk' => array(
                                        'no_faktur'        => gen_primary($this->prefixKey),
                                        'no_faktur_keluar' => '',
                                        'tgl_masuk'        => '',
                                        'id_gudang_tujuan' => '',
                                        'items'            => array(),
                                        'gtotal'           => 0,
                                        'ket'              => ''
                                        ));

            $this->session->set_userdata($data);
            $data = $this->session->userdata('br_masuk');
        }

        $gudang     = $this->gudang_model->order_by("nama_gudang","ASC")->find_all_by("deleted", 0);

        $this->template->set('data', $data);
        $this->template->set('gudang', $gudang);

        $assets = array(
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        "plugins/number/jquery.number.js",
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'barang_masuk/assets/js/barang_masuk.js'
                        );

        add_assets($assets);

        $this->template->title(lang('barang_masuk_title_new'));
		$this->template->render('barang_masuk_form');
   	}

    //Scan Barcode
    public function scan_barcode()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('barang_masuk_only_ajax'), 'error');
            redirect('barang_masuk');
        }

        $barcode = $this->input->post('barcode');

        if($barcode != "")
        {
            $data = $this->bahan_baku_model->select(array("barang.*","satuan_terkecil.alias as satuan"))
                                        ->join("satuan_terkecil","barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","left")
                                        ->find_all_by(array('barang.deleted' => 0, 'barcode' => $barcode));
            if($data)
            {
                foreach ($data as $key => $dt) {
                    $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
                }
                
                $return = array('type' => 'success', 'barcode' => $barcode, 'data' => $data);
            }
            else
            {
                $return = array('type' => 'error', 'barcode' => $barcode, 'data' => 0);
            }
        }
        else
        {
            $return = array('type' => 'error', 'barcode' => $barcode, 'data' => 0);
        }

        echo json_encode($return);
    }

    public function load_barang_keluar()
    {
        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('barang_masuk_only_ajax'), 'error');
            redirect('barang_keluar');
        }

        $kode = $this->input->post('kode');
        $kode = $this->db->escape_str($kode);
        //Cek apakah sudah pernah dikeluarkan sebelumny
        $cek = $this->barang_masuk_model->find_by(['no_faktur_keluar' => $kode,'deleted' => 0]);
        if($cek)
        {
            $result = array('tipe' => 'error', 'text' => 'Maaf, No. Faktur Surat Jalan yang anda masukkan sudah pernah diproses.', 'data' => '');
            echo json_encode($result);
            return FALSE;
        }

        $data = $this->barang_keluar_model->find_by(['no_faktur' => $kode, 'deleted' => 0]);
        if($data)
        {
            $detail = $this->barang_keluar_detail_model->select("barang_keluar_detail.*, konversi_satuan.satuan_besar as satuan, barcode, nm_barang, barang.id_satuan_terkecil")
                                            ->join('barang','barang_keluar_detail.id_barang = barang.idbarang_bb',"left")
                                            ->join('konversi_satuan','barang_keluar_detail.id_konversi = konversi_satuan.id_konversi',"left")
                                            ->order_by('nm_barang','ASC')
                                            ->find_all_by('no_faktur', $kode);
            if($detail)
            {
                $gtotal = 0;
                foreach ($detail as $key => $dt) {
                    $detail[$key]->konversi = get_konversi($dt->id_satuan_terkecil);

                    $gtotal     += $dt->sub_total;
                }
                //Overwrite total
                $data->total = $gtotal;
            }

            $data->detail = $detail;

            $result = array('tipe' => 'success', 'text' => '', 'data' => $data);
        }
        else
        {
            $result = array('tipe' => 'error', 'text' => 'No. Faktur surat jalan tidak ditemukan.', 'data' => '');
        }

        echo json_encode($result);
    }

    //View Detail
    public function view($no_faktur = '')
    {
        $this->auth->restrict($this->viewPermission);

        if (!$no_faktur)
        {
            $this->template->set_message(lang("barang_masuk_invalid_id"), 'error');
            redirect('barang_masuk');
        }
        
        $data  = $this->barang_masuk_model->select("barang_masuk.*, nama_gudang, gudang.tipe as tipe_gudang")
                                    ->join("gudang","barang_masuk.id_gudang = gudang.id_gudang","left")
                                    ->find_by('no_faktur', $no_faktur);

        $detail = $this->barang_masuk_detail_model->select("barang_masuk_detail.*, konversi_satuan.satuan_besar as satuan, barcode, nm_barang")
                                            ->join('barang','barang_masuk_detail.id_barang = barang.idbarang_bb')
                                            ->join('konversi_satuan','barang_masuk_detail.id_konversi = konversi_satuan.id_konversi',"left")
                                            ->order_by('nm_barang','ASC')
                                            ->find_all_by('no_faktur', $no_faktur);

        $this->template->set('no_faktur', $no_faktur);
        $this->template->set('data', $data);
        $this->template->set('detail', $detail);

        $this->template->title(lang('barang_masuk_title_view'));
        $this->template->render('view_barang_masuk');
    }

    //Cancel Barang masuk
    public function cancel()
    {
        $this->auth->restrict($this->addPermission);

        $this->session->unset_userdata('br_masuk');
        $this->template->set_message(lang('barang_masuk_canceled'), 'success');

        redirect('barang_masuk');
    }

    public function cari_barang()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            redirect('barang_masuk');
        }

        $cari = $this->db->escape_str($this->input->post('cr_barang'));

        $data = $this->bahan_baku_model->select(array("barang.*","satuan_terkecil.alias as satuan"))
                                    ->join("satuan_terkecil","barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","left")
                                    ->where("`barang.deleted` = 0
                                            AND (barcode like '%$cari%' OR nm_barang like '%$cari%')")
                                    ->order_by('nm_barang', 'ASC')
                                    ->find_all();
        if($data)
        {
            foreach ($data as $key => $dt) {
                $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
            }
        }

        echo json_encode($data);
    }

   	protected function save_barang_masuk()
   	{
        $no_faktur          = $this->input->post('no_faktur');
        $no_faktur_keluar   = $this->input->post('no_faktur_keluar');
        $tgl_masuk          = $this->input->post('tgl_masuk');
        $id_gudang_tujuan   = $this->input->post('id_gudang_tujuan');
        $ket                = $this->input->post('ket');

        $id_barang  = $this->input->post('id_barang');
        $qty        = $this->input->post('qty');
        $konversi   = $this->input->post('konversi');
        $harga      = $this->input->post('harga');

        //Cek apakah sudah pernah dimasukan sebelumnya
        if($no_faktur_keluar != "")
        {
            $cek = $this->barang_masuk_model->find_by(array('no_faktur_keluar' => $no_faktur_keluar, 'deleted' => 0));
            if($cek)
            {
                $this->template->set_message(lang('barang_masuk_sudah_pernah'),'error');
                return FALSE;
            }
        }

        //Jika faktur keluar/surat jalan intijaya ada maka overwrite detail barang
        if($no_faktur_keluar != "")
        {
            $cek_faktur = $this->barang_keluar_model->find_by(array('no_faktur' => $no_faktur_keluar, 'deleted' => 0));
        
            if(!$cek_faktur)
            {
                $this->template->set_message(lang('barang_masuk_invalid_faktur_keluar'), 'error');
                return FALSE;
            }

            $_POST['id_gudang_tujuan'] = $cek_faktur->id_gudang_tujuan;
            $id_gudang_tujuan          = $cek_faktur->id_gudang_tujuan;

            //Detail Barang Keluar
            $det_keluar     = $this->barang_keluar_detail_model->find_all_by(array('no_faktur' => $no_faktur_keluar));
            $det_keluar_hpp = $this->barang_keluar_detail_hpp_model->find_all_by(array('no_faktur' => $no_faktur_keluar));
            
            //Overwrite detail barang
            if($det_keluar)
            {
                $id_barang  = array();
                $qty        = array();
                $konversi   = array();
                $harga      = array();
                foreach ($det_keluar as $key => $dk) {
                    $id_barang[] = $dk->id_barang;
                    $qty[]       = $dk->qty;
                    $konversi[]  = $dk->id_konversi;
                    $harga[]     = $dk->sub_total/$dk->qty; 
                }

                $_POST['id_barang'] = $id_barang;
                $_POST['qty']       = $qty;
                $_POST['konversi']  = $konversi;
                $_POST['harga']     = $harga;
            }
        }

        $this->form_validation->set_rules('no_faktur','lang:barang_masuk_no_faktur','required|trim');
        $this->form_validation->set_rules('no_faktur_keluar','lang:barang_masuk_no_faktur_keluar','trim');    
        $this->form_validation->set_rules('tgl_masuk','lang:barang_masuk_tanggal','trim|required');    
        $this->form_validation->set_rules('id_gudang_tujuan','lang:barang_masuk_gudang','callback_default_select');    
        $this->form_validation->set_rules('ket','lang:barang_masuk_ket','trim');
        $this->form_validation->set_message('default_select', 'You should choose the "%s" field');

        if ($this->form_validation->run() === FALSE) 
        {
            //Save Session
            $this->save_session();

            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        //validasi
        if(count($id_barang) == 0)
        {
            //Save Session
            $this->save_session();
            $this->template->set_message(lang('barang_masuk_no_item'), 'error');
            return FALSE;
        }

        $ero_msg = array();
        $urut = 0;
        foreach ($qty as $key => $it) {
            $urut ++;
            if($it <= 0)
            {
                $ero_msg[] = sprintf(lang('barang_masuk_qty_nol'), $urut);
            }

            // if($harga[$key] <= 0)
            // {
            //     $ero_msg[] = sprintf(lang('barang_masuk_harga_nol'), $urut);
            // }
        }

        if($ero_msg)
        {
            $ero_msg = implode("<br>", $ero_msg);
            $this->template->set_message($ero_msg, 'error');
            return FALSE;
        }

        $gtotal = $this->hitung_gtotal();
        //Start Transaction
        $this->db->trans_start();

        //Simpan Ke table Barang masuk
        $data_brg_masuk = array('no_faktur' => $no_faktur, 
                            'id_gudang'     => $id_gudang_tujuan,
                            'no_faktur_keluar' => $no_faktur_keluar, 
                            'tgl_masuk'     => date_ymd($tgl_masuk), 
                            'total'         => $gtotal, 
                            'ket'           => $ket
                            );

        $this->barang_masuk_model->insert($data_brg_masuk);
        $query_barang_masuk = $this->db->last_query();

        //Check sukses atau tidak
        $st_brg_masuk = $this->barang_masuk_model->count_by('no_faktur', $no_faktur);

        if($st_brg_masuk)
        {
            //Insert Detail Barang Masuk
            $data_det_masuk = array();
            $data_stok_masuk = array();
            foreach ($id_barang as $key => $br) {
                $sub_total = isset($harga[$key]) ? $this->hitung_total($konversi[$key], $qty[$key], $harga[$key]) : 0;

                $data_det_masuk[] = array(
                                            'id_detail_barang_masuk' => gen_primary(),
                                            'no_faktur' => $no_faktur,
                                            'id_barang' => $br,
                                            'qty'       => remove_comma($qty[$key]),
                                            'harga'     => isset($harga[$key]) ? remove_comma($harga[$key]) : 0,
                                            'id_konversi' => $konversi[$key],
                                            'sub_total' => $sub_total
                                        );

                $dt_barang = $this->bahan_baku_model->find($br);

                $dt_real_qty = hitung_ke_satuan_kecil($konversi[$key], floatval(remove_comma($qty[$key])));
                $real_qty  = $dt_real_qty['qty'];

                $hpp       = isset($harga[$key]) ? floatval(remove_comma($harga[$key])) : 0;

                if($real_qty != floatval(remove_comma($qty[$key])))
                {
                    $hpp = $sub_total / $real_qty;
                }

                $data_stok_masuk[] = array('id_stok' => gen_primary($this->prefixKeyStok),
                                        'no_faktur'  => $no_faktur,
                                        'id_gudang'  => $id_gudang_tujuan,
                                        'id_barang'  => $br,
                                        'stok_datang' => $real_qty,
                                        'stok_update' => $real_qty,
                                        'id_satuan_terkecil' => $dt_barang->id_satuan_terkecil,
                                        'hpp'         => $hpp
                                        );
            }

            $this->barang_masuk_detail_model->insert_batch($data_det_masuk);
            $query_barang_masuk .= "\n\n".$this->db->last_query();

            $st_det_masuk = $this->barang_masuk_detail_model->count_by('no_faktur', $no_faktur);
            if($st_det_masuk == count($data_det_masuk))
            {
                $st_det_masuk = TRUE;
                //Insert Stok
                if($no_faktur_keluar != ""){ 
                    //Jika ada faktur keluar maka ambil data dari stok keluar hpp
                    $data_stok_masuk = [];
                    foreach ($det_keluar_hpp as $key => $dkh) {
                        $data_stok_masuk[] = array('id_stok' => gen_primary($this->prefixKeyStok),
                                                    'no_faktur'  => $no_faktur,
                                                    'id_gudang'  => $id_gudang_tujuan,
                                                    'id_barang'  => $dkh->id_barang,
                                                    'stok_datang' => $dkh->qty,
                                                    'stok_update' => $dkh->qty,
                                                    'id_satuan_terkecil' => $dkh->id_satuan_terkecil,
                                                    'hpp'         => $dkh->hpp
                                                    );
                    }
                }
                //Cek Tipe Gudang dan ST Potong Meteran
                $dt_gudang   = $this->gudang_model->find($id_gudang_tujuan);
                $tipe_gudang = $dt_gudang->tipe;
                $data_stok_masuk2 = [];
                if($tipe_gudang == 2){ //Jika Gudang Produksi
                    foreach ($data_stok_masuk as $key => $val) {
                        //Cek ST Potong Meteran
                        $dt_item = $this->bahan_baku_model->find($val['id_barang']);
                        $st_potong_meteran = $dt_item->st_potong_meteran;
                       
                        $dt_panjang = hitung_ke_satuan_kecil($dt_item->id_konversi, floatval($dt_item->panjang_spek));
                        $panjang    = $dt_panjang['qty'];
                        $dt_lebar   = hitung_ke_satuan_kecil($dt_item->id_konversi, floatval($dt_item->lebar_spek));
                        $lebar      = $dt_lebar['qty'];

                        if($st_potong_meteran == 1){
                            for ($i=0; $i < $val['stok_datang']; $i++) { 
                                $luas = $panjang * $lebar;
                                $data_stok_masuk2[] = array('id_stok' => gen_primary($this->prefixKeyStok),
                                                        'no_faktur'  => $no_faktur,
                                                        'id_gudang'  => $id_gudang_tujuan,
                                                        'id_barang'  => $val['id_barang'],
                                                        'stok_datang' => 1,
                                                        'stok_update' => 1,
                                                        'luas_datang' => $luas,
                                                        'luas_update' => $luas,
                                                        'id_satuan_terkecil' => $val['id_satuan_terkecil'],
                                                        'hpp'         => $val['hpp']
                                                        );
                            }
                        }else{
                            $data_stok_masuk2[] = array('id_stok' => gen_primary($this->prefixKeyStok),
                                                        'no_faktur'  => $no_faktur,
                                                        'id_gudang'  => $id_gudang_tujuan,
                                                        'id_barang'  => $val['id_barang'],
                                                        'stok_datang' => $val['stok_datang'],
                                                        'stok_update' => $val['stok_update'],
                                                        'luas_datang' => NULL,
                                                        'luas_update' => NULL,
                                                        'id_satuan_terkecil' => $val['id_satuan_terkecil'],
                                                        'hpp'         => $val['hpp']
                                                        );
                        }
                    }
                }

                // Insert Stok
                if($data_stok_masuk2){
                    $this->stok_model->insert_batch($data_stok_masuk2);    
                }else{
                    $this->stok_model->insert_batch($data_stok_masuk);    
                }
                $query_barang_masuk .= "\n\n".$this->db->last_query();

                //Cek Status
                $st_stok_masuk = $this->stok_model->count_by('no_faktur', $no_faktur);
                if($st_stok_masuk == count($data_stok_masuk) || $st_stok_masuk == count($data_stok_masuk2))
                {
                    $st_stok_masuk = TRUE;
                }
                else
                {
                    $st_stok_masuk = FALSE;
                    //Delete Barang masuk
                    $this->barang_masuk_detail_model->delete_where(array('no_faktur' => $no_faktur));
                    $this->barang_masuk_model->soft_delete(false)->delete($no_faktur);
                    $this->stok_model->delete_where(array('no_faktur' => $no_faktur));
                }
            }
            else
            {
                $st_det_masuk = FALSE;
                //Delete Barang masuk
                $this->barang_masuk_detail_model->delete_where(array('no_faktur' => $no_faktur));
                $this->barang_masuk_model->soft_delete(false)->delete($no_faktur);

                $this->template->set_message(lang('barang_masuk_create_failure'),'error');
            }
        }
        
        $this->db->trans_complete();

        //Save Log
        if ($st_brg_masuk > 0 && $st_det_masuk == TRUE && $st_stok_masuk == TRUE)
        {
            $keterangan = "SUKSES, tambah data barang masuk dengan ID : ".$no_faktur;
            $status     = 1;
            
            $return = TRUE;
        }
        else
        {
            $keterangan = "GAGAL, tambah data barang masuk dengan ID ".$no_faktur;
            $status     = 0;
            
            $return = FALSE;
        }

        //Save Log
        $nm_hak_akses   = $this->addPermission; 
        $kode_universal = $no_faktur;
        $jumlah         = $gtotal;
        $sql            = $query_barang_masuk;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        if($return)
        {
            //Clear Session
            $this->session->unset_userdata('br_masuk');
        }
        
        return $return;
   	}

    private function save_session()
    {
        $no_faktur          = $this->input->post('no_faktur');
        $no_faktur_keluar   = $this->input->post('no_faktur_keluar');
        $tgl_masuk          = $this->input->post('tgl_masuk');
        $id_gudang_tujuan   = $this->input->post('id_gudang_tujuan');
        $ket                = $this->input->post('ket');

        $id_barang  = $this->input->post('id_barang');
        $qty        = $this->input->post('qty');
        $konversi   = $this->input->post('konversi');
        $harga      = $this->input->post('harga');

        $items  = array();
        $gtotal = 0;
        if($id_barang)
        {
            foreach ($id_barang as $key => $br) {
                $dt_barang = $this->bahan_baku_model->find($br);
                $total = $this->hitung_total($konversi[$key], $qty[$key], $harga[$key]);

                $items[] = array(
                            'id_barang' => $br,
                            'barcode'   => $dt_barang->barcode,
                            'nm_barang' => $dt_barang->nm_barang,
                            'qty'       => remove_comma($qty[$key]),
                            'id_konversi'  => $konversi[$key],
                            'konversi'  => get_konversi($dt_barang->id_satuan_terkecil),
                            'harga'     => remove_comma($harga[$key]),
                            'total'     => $total
                            );

                //gtotal
                $gtotal     += $total;
            }    
        }

        $dt_session = array(
                        'no_faktur'        => $no_faktur,
                        'no_faktur_keluar' => $no_faktur_keluar,
                        'tgl_masuk'        => $tgl_masuk,
                        'id_gudang_tujuan' => $id_gudang_tujuan,
                        'items'            => $items,
                        'gtotal'           => $gtotal,
                        'ket'              => $ket
                        );

        $this->session->set_userdata('br_masuk', $dt_session);
        $data = $this->session->userdata('br_masuk');
    }

    private function hitung_total($konversi = NULL,  $qty = NULL, $harga = NULL)
    {
        if(!$konversi || !$qty || !$harga)
        {
            return 0;
        }

        //Get real qty
        $dt_real_qty        = hitung_ke_satuan_kecil($konversi, floatval($qty));
        $real_qty           = $dt_real_qty['qty'];
        //harga
        $hpp                = floatval(remove_comma($harga));
        if($real_qty != $qty)
        {
            $hpp = floatval(remove_comma($harga)) * floatval($qty) / $real_qty;
        }

        $total = $real_qty * $hpp;

        return $total;
    }

    private function hitung_gtotal()
    {
        $id_barang  = $this->input->post('id_barang');
        $qty        = $this->input->post('qty');
        $konversi   = $this->input->post('konversi');
        $harga      = $this->input->post('harga');

        $gtotal = 0;
        foreach ($id_barang as $key => $br) {
            //gtotal
            $gtotal     += $this->hitung_total($konversi[$key], $qty[$key], $harga[$key]);
        }

        return $gtotal;
    }

    public function default_select($val)
    {
        return $val == "" ? FALSE : TRUE;
    }

    public function laporan()
    {
        $this->auth->restrict($this->laporanPermission);

        $this->form_validation->set_rules('tgl_awal', 'lang:barang_masuk_tgl_awal', 'required|trim');
        $this->form_validation->set_rules('tgl_akhir', 'lang:barang_masuk_tgl_akhir', 'required|trim');
        
        if ($this->form_validation->run() === FALSE) {
            $this->template->set_message(validation_errors(), 'error');
        }

        if(isset($_POST['table_search'])) {
            $search     = $this->input->post('table_search');
            $tgl_awal   = isset($_POST['tgl_awal']) ? date_ymd($this->input->post('tgl_awal')) : '';
            $tgl_akhir  = isset($_POST['tgl_awal']) ? date_ymd($this->input->post('tgl_akhir')) : '';
        } else {
            $search     = "";
            $tgl_awal   = "";
            $tgl_akhir  = "";
        }

        $search2 = $this->db->escape_str($search);

        $where = "barang_masuk.deleted = 0 
                AND (barang_masuk.tgl_masuk >='".$tgl_awal."' AND barang_masuk.tgl_masuk <='".$tgl_akhir."') 
                AND nm_barang like '%$search2%' 
                AND nama_gudang like '%$search%' 
                AND nm_lengkap like '%$search%'";

        $data = $this->barang_masuk_model->select(array("barang_masuk.*","users.nm_lengkap as operator","nama_gudang"))
                                        ->join('barang_masuk_detail','barang_masuk.no_faktur = barang_masuk_detail.no_faktur')
                                        ->join('barang','barang_masuk_detail.id_barang = barang.idbarang_bb')
                                        ->join('gudang','barang_masuk.id_gudang=gudang.id_gudang','left')
                                        ->join('users','barang_masuk.created_by = users.id_user',"left")
                                        ->where($where)
                                        ->group_by("barang_masuk.no_faktur")
                                        ->order_by("barang_masuk.created_on","DESC")
                                        ->find_all();
        if($data)
        {
            foreach ($data as $key => $dt) {
                $detail = $this->barang_masuk_detail_model->select("barang_masuk_detail.*, konversi_satuan.satuan_besar as satuan, barcode, nm_barang")
                                            ->join('barang','barang_masuk_detail.id_barang = barang.idbarang_bb')
                                            ->join('konversi_satuan','barang_masuk_detail.id_konversi = konversi_satuan.id_konversi',"left")
                                            ->find_all_by('no_faktur', $dt->no_faktur);

                $data[$key]->detail = $detail;
            }
        }

        //Identitas
        $identitas = $this->identitas_model->find(1);
        $assets = array(
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        "barang_masuk/assets/js/laporan.js"
                        );

        add_assets($assets);

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('idt', $identitas);
        $this->template->set('period1', $this->input->post('tgl_awal'));
        $this->template->set('period2', $this->input->post('tgl_akhir'));
        $this->template->title(lang('barang_masuk_laporan_title_view'));
        $this->template->page_icon('fa fa-bar-chart-o');
        $this->template->render('laporan');
    }

}
?>