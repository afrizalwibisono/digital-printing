<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['barang_masuk_title_manage'] 	= 'Barang Masuk';
$lang['barang_masuk_title_new'] 	= 'Barang Masuk Baru';
$lang['barang_masuk_title_view'] 	= 'Detail Barang Masuk';
$lang['barang_masuk_laporan_title_view'] = 'Laporan Barang Masuk';

// form/table
$lang['barang_masuk_no_faktur'] = 'No. Faktur Barang Masuk';
$lang['barang_masuk_no_faktur_keluar'] = 'No. Faktur Surat Jalan';
$lang['barang_masuk_no_nota'] 	= 'No. Nota';
$lang['barang_masuk_customer'] 	= 'Customer';
$lang['barang_masuk_gudang'] 	= 'Gudang';
$lang['barang_masuk_dibuat_oleh'] = 'Dibuat Oleh';
$lang['barang_masuk_tanggal'] 	= 'Tanggal';
$lang['barang_masuk_total'] 	= 'Total';
$lang['barang_masuk_ket'] 		= 'Keterangan';

$lang['barang_masuk_no'] 		= '#';
$lang['barang_masuk_barcode'] 	= 'Barcode';
$lang['barang_masuk_nm_barang'] = 'Nama Barang';
$lang['barang_masuk_qty'] 		= 'Qty';
$lang['barang_masuk_harga'] 	= 'Harga(Rp.)';
$lang['barang_masuk_satuan'] 	= 'Satuan';
$lang['barang_masuk_total'] 	= 'Total';
$lang['barang_masuk_gtotal'] 	= 'Grand Total : ';


$lang['barang_masuk_ringkasan'] 		= 'Ringkasan';
$lang['barang_masuk_detail'] 			= 'Detail Transaksi';
$lang['barang_masuk_total_tran_qty'] 	= 'Transaksi';
$lang['barang_masuk_total_tran'] 		= 'Total Seluruh Transaksi';
$lang['barang_masuk_kali'] 				= 'kali';

$lang['barang_masuk_operator'] 	= 'Operator';
$lang['barang_masuk_cetak'] 	= 'Cetak';

$lang['barang_masuk_range'] 	= 'Range tanggal';
$lang['barang_masuk_tgl_awal'] 	= 'Tanggal awal';
$lang['barang_masuk_tgl_akhir'] = 'Tanggal akhir';

$lang['barang_masuk_pilih_brg'] = 'Pilih Barang';
$lang['barang_masuk_scan_brg']  = 'Ketik / Scan Barcode Barang';
$lang['barang_masuk_cari_brg']  = 'Cari Barang ...';
$lang['barang_masuk_cari_btn']  = 'Cari';
$lang['barang_masuk_pilih_btn'] = 'Pilih';

// button
$lang['barang_masuk_btn_new'] 		= 'Baru';
$lang['barang_masuk_btn_delete'] 	= 'Hapus';
$lang['barang_masuk_btn_save'] 		= 'Simpan';
$lang['barang_masuk_btn_cancel'] 	= 'Batal';
$lang['barang_masuk_btn_back'] 		= 'Kembali';
$lang['barang_masuk_or'] 			= 'atau';

// messages
$lang['barang_masuk_del_error']			= 'Anda belum memilih faktur barang masuk yang akan dihapus.';
$lang['barang_masuk_del_failure']		= 'Tidak dapat menghapus faktur barang masuk: ';
$lang['barang_masuk_delete_confirm']	= 'Apakah anda yakin akan menghapus faktur barang masuk terpilih ?';
$lang['barang_masuk_cancel_confirm']	= 'Apakah anda yakin akan membatalkan transaksi barang masuk ?';
$lang['barang_masuk_deleted']			= 'Data Barang Masuk berhasil dihapus';
$lang['barang_masuk_no_records_found'] 	= 'Data tidak ditemukan.';
$lang['barang_masuk_err_faktur_keluar'] = 'Silahkan masukkan No. Faktur Surat Jalan.';
$lang['barang_masuk_invalid_faktur_keluar'] = 'No. Faktur Surat Jalan tidak valid';

$lang['barang_masuk_create_failure'] 	= 'Barang Masuk baru gagal disimpan: ';
$lang['barang_masuk_create_success'] 	= 'Barang Masuk baru berhasil disimpan';
$lang['barang_masuk_canceled'] 			= 'Barang Masuk telah berhasil dibatalkan';

$lang['barang_masuk_edit_success'] 		= 'Barang Masuk berhasil disimpan';
$lang['barang_masuk_invalid_id'] 		= 'ID Tidak Valid';

$lang['barang_masuk_no_item'] 			= 'Anda belum menambahkan data barang yang akan dimasukkan.';
$lang['barang_masuk_qty_nol'] 			= 'Qty baris ke: %s tidak boleh kurang atau sama dengan 0 (nol).';
$lang['barang_masuk_harga_nol'] 		= 'Harga baris ke: %s tidak boleh kurang atau sama dengan 0 (nol).';

$lang['barang_masuk_barcode_found'] 	= 'Barcode : %s ditemukan.';
$lang['barang_masuk_barcode_not_found'] = 'Barcode : %s tidak ditemukan.';

$lang['barang_masuk_only_ajax'] 		= 'Hanya ajax request yang diijinkan.';

$lang['barang_masuk_date_required'] 	= 'Silahkan pilih tanggal awal dan akhir.';
$lang['barang_masuk_sudah_pernah'] 	    = 'Maaf, No. Faktur Surat Jalan yang anda masukkan sudah pernah diproses.';