$(document).ready(function(){
	$("#ck_all").on("click", function(){
		if($(this).prop("checked")){
			$("input[name='item_code[]']").prop("checked", true);
		}else{
			$("input[name='item_code[]']").prop("checked", false);
		}
	});

	$("#cari_btn").on("click", function(){
		cari_barang();
	});

	$("#cr_barang").on("keydown", function(e){
		if(e.keyCode == 13){
			cari_barang();
			e.preventDefault();
		}
	});
	//Scan Barcode
	$("input[name='barcode']").on("keydown", function(e){
		if(e.keyCode == 13){
			scan_barcode();
			e.preventDefault();
		}
	});
	//Pilih Item
	$("#pilih_btn").on("click", function(){
		add_items();
	});
	//gudang Tujuan
	$("#id_gudang_tujuan").select2({placeholder : '-- Pilih gudang --', allowClear : true});
	//Load barang keluar
	$("#no_faktur_keluar").on("keydown", function(e){
		if(e.keyCode == 13){
			load_barang_keluar();
			e.preventDefault();
		}
	});

	$("select[name='konversi[]']").select2();
	//Tanggal Masuk
	$('#tgl_masuk').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    todayHighlight: true
	});

	$("button[name='save']").on('click', function(e){
		e.preventDefault();
		var id_gudang = $("#id_gudang_tujuan").val();
		if(!id_gudang){
	        alertify.error('Silahkan pilih gudang dulu.');
	        return false;
		}else{
			$("button[name='save']").off("click");
			$("button[name='save']").click();
		}
	});
});

//cari barang
function cari_barang(){
	var cari = $("#cr_barang").val();

	$.ajax({
		url : baseurl+"barang_masuk/cari_barang",
		type : "post",
		dataType : "json",
		data : {cr_barang : cari},
		success : function(msg){
			$("table#tbl_barang tbody tr").remove();
			if(!msg){
				var $tr = $("<tr>"
							+"<td colspan='4' class='text-center'>Data tidak ditemukan</td>"
							+"</tr>");

				$("table#tbl_barang tbody").append($tr);
			}else{

				var opt_select = "";
				$.each(msg, function(i,n){
					$.each(n['konversi'], function(l,m){
						var selected = "";
						if(m['selected'] == 1)
						{
							selected = "selected";
						}

						opt_select += '<option value="'+m['id_konversi']+'" '+selected+'>'+m['tampil2']+'</option>';
					});

					var $tr = $("<tr data-konversi='"+opt_select+"'>"
								+"<td><input type='checkbox' name='item_code[]' value='"+n['idbarang_bb']+"'></td>"
								+"<td><input type='hidden' name='item_barcode[]' value='"+n['barcode']+"' />"+(i+1)+"</td>"
								+"<td><input type='hidden' name='item_id_satuan[]' value='"+n['id_satuan_terkecil']+"' />"+n['barcode']+"</td>"
								+"<td><input type='hidden' name='item_nm_satuan[]' value='"+n['satuan']+"' /><input type='hidden' name='item_nm_barang[]' value='"+n['nm_barang']+"' />"+n['nm_barang']+"</td>"
								+"</tr>");

					$("table#tbl_barang tbody").append($tr);

					opt_select = "";
				});
				
			}
		}
	});
}

//Add item to list
function add_items(){
	var $obj_ids   = $("#tbl_barang input[name='item_code[]']:checked");

	var ids_barang = $("#tbl_barang input[name='item_code[]']:checked").map(function(){
		return $(this).val();
	}).get();

	var ids_barcodes = [];
	var nms_barang = [];
	var options    = [];

	$.each($obj_ids, function(){
		ids_barcodes.push($(this).closest("tr").find("input[name='item_barcode[]']").val());
		nms_barang.push($(this).closest("tr").find("input[name='item_nm_barang[]']").val());
		options.push($(this).closest("tr").data("konversi"));
	});

	if(ids_barang.length == 0)
	{
        alertify.error('Silahkan pilih barang yang akan ditambahkan dulu');
        return false;
	}

	var pj_ids = ids_barang.length;

	for (var i = 0; i < pj_ids; i++) {
		var $row = $("<tr>"
		  					+"<td></td>"
		  					+"<td>"
		  						+ids_barcodes[i]
		  						+"<input type='hidden' name='id_barang[]' value='"+ids_barang[i]+"' />"
		  					+"</td>"
		  					+"<td>"+nms_barang[i]+"<br><a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>"
		  					+"<td><input type='number' min='1' name='qty[]' onchange='hitung_total()' class='form-control' value='1' /></td>"
		  					+"<td><select class='form-control' name='konversi[]' style='width: 200px;'>"+options[i]+"</select></td>"
		  					+"<td><input type='text' name='harga[]' onchange='hitung_total()' class='form-control' value='0' /></td>"
		  					+"<td class='text-right'>0</td>"
		  				+"</tr>");

		var pj = $("#tdet_masuk tr").length;
		var cek = false;

		if(pj > 3){
			for (var j = 0; j <= pj - 2; j++) {
				var kode = $("#tdet_masuk tr:eq("+(j+1)+")").find("input[name='id_barang[]']").val();

				if(kode != "" && kode == ids_barang[i]){
					var qty = parseFloat($("#tdet_masuk tr:eq("+(j+1)+")").find("input[name='qty[]']").val());

					$("#tdet_masuk tr:eq("+(j+1)+")").find("input[name='qty[]']").val(qty+1).trigger("change");
					
					cek = true;
					break;
				}

			}
			
		}

		if(cek == false){
			$("#tdet_masuk tbody").append($row);
		}

		//Format number
		$("input[name='harga[]']").number(true, 0);

		$("#modal-barang").modal("hide");
		$("#tbl_barang input[type='checkbox']").prop('checked', false);	
	}

	$("#barcode").focus();
	$("select[name='konversi[]']").select2();

	hitung_total();

	//Buat Nomor
	buat_no();
}

//buat nomor
function buat_no(){
	$.each($("#tdet_masuk tbody tr"), function(i){
		$(this).find("td:first").text(i+1);
	});
}

//Hitung total dan g total
function hitung_total(){
	var qty = 0,
		harga = 0,
		total = 0,
		gtotal = 0;
		
	$.each($("#tdet_masuk tbody tr"), function(i, n){
		qty 	= parseFloat($(n).find("input[name='qty[]']").val());
		harga 	= parseFloat($(n).find("input[name='harga[]']").val());

		if(isNaN(qty)){
			qty = 0;
		}

		if(isNaN(harga)){
			harga = 0;
		}
		//Hitung total
		total 	= qty * harga;
		gtotal += total;
		//Update total
		total 	= $.number(total);
		$(n).find("td:last").text(total);
	});

	$("#lblgtotal").text($.number(gtotal));
}
//Scan Barcoce
function scan_barcode(){
	var barcode = $("#barcode").val().trim();

	if(!barcode){
		return false;
	}

	$.ajax({
		url : baseurl+"barang_masuk/scan_barcode",
		type : "post",
		dataType : "json",
		data : {barcode : barcode, type: 0},
		success : function(msg){
			//clear barcode
			$("#barcode").val("");

			if(msg['type'] == 'success'){ //Sukses
				$("#barcode_input").removeClass("has-error").addClass("has-success");
				$("#barcode_msg").removeClass("has-red").addClass("text-green");
				$("#barcode_msg").text("Barcode : "+msg['barcode']+" ditemukan.");

				var addToList = function(){
					var pj = $("#tdet_masuk tr").length;
					var cek = false;
					//Cek jika item sudah ada pada daftar
					if(pj > 3){
						for (var j = 0; j <= pj - 2; j++) {
							var kode = $("#tdet_masuk tr:eq("+(j+1)+")").find("input[name='id_barang[]']").val();

							if(kode != "" && kode == msg['data'][0]['id_barang']){
								var qty = parseFloat($("#tdet_masuk tr:eq("+(j+1)+")").find("input[name='qty[]']").val());

								$("#tdet_masuk tr:eq("+(j+1)+")").find("input[name='qty[]']").val(qty+1).trigger("change"); //Trigger Change
								
								cek = true;
								break;
							}

						}
					}

					if(cek == false){
						var opt_select = "";

						$.each(msg['data'][0]['konversi'], function(i,n){
							var selected = "";
							if(n['selected'] == 1)
							{
								selected = "selected";
							}

							opt_select += '<option value="'+n['id_konversi']+'" '+selected+' data-jmlkecil="'+n['jml_kecil']+'">'+n['tampil2']+'</option>';
						});

						var $row = $("<tr>"
				  					+"<td></td>"
				  					+"<td>"
				  						+msg['data'][0]['barcode']+(msg['data'][0]['artikel_pembeda'] ? '-'+msg['data'][0]['artikel_pembeda'] : '')
				  						+"<input type='hidden' name='id_barang[]' value='"+msg['data'][0]['id_barang']+"' />"
				  					+"</td>"
				  					+"<td>"+msg['data'][0]['nm_barang']+"<br><a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>"
				  					+"<td><input type='number' min='1' name='qty[]' onchange='hitung_total()' class='form-control' value='1' /></td>"
				  					+"<td><select class='form-control' name='konversi[]' style='width: 200px;'>"+opt_select+"</select></td>"
				  					+"<td><input type='text' name='harga[]' onchange='hitung_total()' class='form-control' value='0' /></td>"
				  					+"<td align='right'>0</td>"
				  				+"</tr>");

						$("#tdet_masuk tbody").append($row);

						buat_no();
					}
					$("select[name='konversi[]']").select2();
				}

				//Show Modal pilih barcode
				if(typeof msg['data'] == 'object' && msg['data'].length > 1)
				{
					var pembeda = prompt('Masukkan Kode Pembeda');
					
					if(pembeda === null){
						return;
					}

					$.each(msg['data'], function(i,n){
						if(n['artikel_pembeda'].toLowerCase() == pembeda.toLowerCase()){
							
							msg['data'] = [];
							msg['data'].push(n);

							return false;
						}
					});
				}

				addToList();
			}else if(msg['type'] == 'error'){ //gagal
				$("#barcode_input").removeClass("has-success").addClass("has-error");
				$("#barcode_msg").removeClass("text-green").addClass("text-red");
				$("#barcode_msg").text("Barcode : "+msg['barcode']+" tidak ditemukan.");
			}
		}
	});
}

//Remove Item
function remove_item(obj){
	$(obj).closest("tr").fadeTo("slow",0.2, function(){
		$(this).remove();
		hitung_total();
	});
}

//Load barang keluar
function load_barang_keluar(){
	var faktur_keluar = $("#no_faktur_keluar").val();
	
	$.ajax({
		url : baseurl+"barang_masuk/load_barang_keluar",
		type : "post",
		data : {kode : faktur_keluar},
		dataType : "json",
		success : function(msg){
			if(msg['tipe'] == "success"){
				//Clear First
				$("#tdet_masuk tbody tr").remove();

				$.each(msg['data']['detail'], function(i,n){
					
					var opt_select = "";
					$.each(n['konversi'], function(l,m){
						var selected = "";
						if(m['id_konversi'] == n['id_konversi'])
						{
							selected = "selected";
						}

						opt_select += '<option value="'+m['id_konversi']+'" '+selected+'>'+m['tampil2']+'</option>';
					});

					var $row = $("<tr>"
			  					+"<td></td>"
			  					+"<td>"+n['barcode']+"</td>"
			  					+"<td>"+n['nm_barang']+"</td>"
			  					+"<td><input type='number' min='1' name='qty[]' class='form-control' value='"+n['qty']+"' readonly /></td>"
			  					+"<td>"
			  						+"<select class='form-control' name='konversi[]' disabled>"
			  							+opt_select
			  						+"</select>"
			  					+"</td>"
			  				+"</tr>");
					//Hide barcode dan gtotal
					$("#tdet_masuk tfoot, .brkeluar").hide();

					$("#tdet_masuk tbody").append($row);

					opt_select = "";

				});

				$("#id_gudang_tujuan").val(msg['data']['id_gudang_tujuan']).trigger("change");
				$("#id_gudang_tujuan").select2({placeholder : '-- Pilih gudang --', allowClear : true, disabled: true});	
				//Buat Nomor
				buat_no();
			}
			else
			{
				alertify.error(msg['text']);
		        return false;
			}
		}
	});
}