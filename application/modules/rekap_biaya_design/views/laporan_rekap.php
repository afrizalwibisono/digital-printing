<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_laporan','name'=>'frm_laporan','class' => 'form-inline')) ?>
        <div class="box-header hidden-print">
            <div class="pull-left">
                <button type="button" id="cetak" class="btn btn-default" onclick="cetak_halaman()"><i class="fa fa-print"></i> <?= lang('cetak') ?></button>
            </div>
            <div class="pull-right">
                <div class="form-group">
                    <div class="input-daterange input-group" title="<?= lang('range_tgl') ?>" data-toggle="tooltip" data-placement="top">
                        <input type="text" style="width: 120px" class="form-control" name="tgl_awal" id="tgl_awal" value="<?= isset($period1) ? $period1 : '' ?>" placeholder="<?= lang('tgl_awal') ?>" />
                        <span class="input-group-addon">to</span>
                        <input type="text" style="width: 120px" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?= isset($period2) ? $period2 : '' ?>" placeholder="<?= lang('tgl_akhir') ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="nm_op" value="<?php echo isset($nmop) ? $nmop : ''; ?>" class="form-control pull-right" placeholder="Nama OP" autofocus />
                        <div class="input-group-btn">
                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?= form_close() ?>
    <?php if($idt) : ?>
    <div class="row header-laporan">
        <div class="col-md-12">
            <div class="col-md-1 laporan-logo">
                <img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo">
            </div>
            <div class="col-md-6 laporan-identitas">
                <address>
                    <h4><?= $idt->nm_perusahaan ?></h4>
                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?>
                </address>
            </div>
            <div class="col-md-5 text-right">
                <h5><?= lang("judul_lap") ?></h5>
                <h5>Periode <?= $period1." - ".$period2 ?></h5>
            </div>
            <div class="clearfix"></div>
            <div class="laporan-garis"></div>
        </div>
    </div>
    <?php endif ?>
	<?php if (isset($data) && is_array($data) && count($data)) : ?>
    	<div class="box-body">
            
            <div class="table-responsive no-padding">
                <table class="table table-condensed table-bordered table-detail">
                    <thead>
                        <tr class="bg-success">
                            <th width="30" class="text-center">#</th>
                            <th><?= lang('tbl_un') ?></th>
                            <th><?= lang('tbl_nama') ?></th>
                            <th class="text-center" ><?= lang('tbl_rek_design') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                            $num = 1;
                        foreach ($data as $key => $dt): ?>
                        <tr>
                            <td class="text-right"><?= $num ?></td>
                            <td><?= $dt->username ?></td>
                            <td><?= ucwords($dt->nm_lengkap) ?></td>
                            <td class="text-center"><?= number_format($dt->total) ?></td>
                        </tr>
                    <?php $num++;endforeach ?>                  
                    </tbody>
    	        </table>
            </div>
          
    	</div><!-- /.box-body -->
        
	<?php else: ?>
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('lbl_data_tidak_ditemukan') ?></p>
    </div>
    <?php endif;?>
</div><!-- /.box -->