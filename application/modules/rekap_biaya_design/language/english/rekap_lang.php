<?php defined('BASEPATH')|| exit('No direct script access allowed');


// ==========================================================================
$lang['title']							= 'Laporan Rekap Biaya Design';
$lang['judul_lap']						= 'Rekap Biaya Design';

// ==========================================================================
$lang['tgl_awal']						= 'Tgl Awal';
$lang['tgl_akhir']						= 'Tgl Akhir';

$lang['tbl_un']							= 'UserName';
$lang['tbl_nama']						= 'Nama Asli';
$lang['tbl_rek_design']					= 'Total Biaya Design';

// ==========================================================================
$lang['lbl_data_tidak_ditemukan']		= 'Data Tidak Ditemukan';