<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for kasir

*/
class Rekap_biaya_design extends Admin_Controller {

	protected $viewPermission   		= "Laporan.Rekap Biaya Design.View";

    public function __construct(){

        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('rekap_biaya_design/rekap');
        $this->load->model(array(
        							"order_detail_model"
        						)
    						);

        $this->template->title(lang('title'));
		$this->template->page_icon('fa fa-file-o');
    }

    public function index(){

        $this->auth->restrict($this->viewPermission);

        $tgl1   = isset($_POST['tgl_awal']) ? $this->input->post('tgl_awal') : date('d/m/Y');
        $tgl2   = isset($_POST['tgl_akhir']) ? $this->input->post('tgl_akhir') : date('d/m/Y');

        $nm_op  = isset($_POST['nm_op']) ? $this->input->post('nmop') : '';

        $tgl1_cr   = date_ymd($tgl1)." 01:00:00";
        $tgl2_cr   = date_ymd($tgl2)." 23:59:59";


        $where  = "m_order_produk_detail.deleted = 0 
                    and 
                    (m_order_produk_detail.created_on >= '{$tgl1_cr}' and m_order_produk_detail.created_on <= '{$tgl2_cr}')";

        if(strlen($nm_op) > 0){

            $nm_op_cr  = "and `users`.`nm_lengkap` like '%{'$nm_op'}%'";

            $where .= $nm_op_cr;

        }


        $data   = $this->order_detail_model
                        ->select('users.username,
                                    `users`.`nm_lengkap`,
                                    SUM(`m_order_produk_detail`.`biaya_design`) AS total')
                        ->join("(SELECT 
                                        `id_detail_produk_order` AS id, `created_by` AS id_user
                                    FROM
                                        `m_order_produk_detail`) AS t_asli","m_order_produk_detail.kode_universal = t_asli.id","inner")
                        ->join("users","t_asli.id_user = `users`.`id_user`","inner")
                        ->where($where)
                        ->group_by("t_asli.id_user")
                        ->order_by("`users`.`nm_lengkap`")
                        ->find_all();

        //echo $this->db->last_query();

        $asset  =   [
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'rekap_biaya_design/assets/js/laporan.js'
                    ];

        add_assets($asset);

        $this->template->set("period1",$tgl1);
        $this->template->set("period2",$tgl2);

        $this->template->set("nmop",$nm_op);

        $this->template->set("data",$data);

        $this->template->set("toolbar_title", lang('title'));
        $this->template->title(lang('title'));
        $this->template->render('laporan_rekap'); 

    }

}
	
?>