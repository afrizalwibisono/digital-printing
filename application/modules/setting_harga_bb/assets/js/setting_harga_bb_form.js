var list_diskon = [];
if(json_diskon && json_diskon.length > 0){
	list_diskon = json_diskon;
}

$(function(){
	$("#add_to_list").on('click', function(){
		add_to_list();
	});

	$("#harga").number(true);
	$("#margin").number(true);

	$('#hpp').on('change', function(){
		if(this.value !== ''){
			hitung_harga_by_margin();
		}else{
			$("#harga").val(0);
		}
	});

	$("#tipe_margin").on('change', hitung_harga_by_margin);
	$("#margin").on('keyup', function(){
		if(Number((this.value).replace(/,/g,"")) > 0){
			hitung_harga_by_margin();
		}else{
			$("#harga").val(0);
		}	
	});

	$("#harga").on("keyup", hitung_margin);
});

function add_to_list(){
	var id_gol = $("#golongan_member").val();
	var nm_gol = $("#golongan_member option:selected").text();
	var range1 = $("#range1").val();
	var range2 = $("#range2").val();
	var diskon = $("#diskon").val();

	if(id_gol === ''){
		alertify.error('Silahkan pilih '+ $('label[for="golongan_member"]').text());
		return false;
	}

	if(!Number(range1) || !Number(range2)){
		alertify.error('Silahkan masukkan '+ $('label[for="range1"]').text()+' dan pastikan hanya berisi angka');
		return false;
	}

	if(!Number(diskon)){
		alertify.error('Silahkan masukkan '+ $('label[for="diskon"]').text() + ' dan pastikan hanya berisi angka');
		return false;
	}

	// Cek apakah sudah ada dalam list
	var cek = list_diskon.filter(function(el, index, arr){
		if(id_gol == el.gol && ((range1 >= el.range.a && range1 <= el.range.b) || (range2 >= el.range.a && range2 <= el.range.b))){
			return el;
		}
	}, id_gol, range1, diskon);

	if(cek.length == 0){
		list_diskon.push({gol: Number(id_gol), nm_gol: nm_gol, range: {a: Number(range1), b: Number(range2)}, diskon: Number(diskon)});	
		// Set Value to List
		$("#tbl-diskon tbody tr").remove();
		list_diskon.map(function(val, i){
			var $row = $('<tr>\
							<td>'+(i+1)+'</td>\
							<td>\
								<input type="hidden" name="id_gol[]" value="'+ val.gol +'">\
								<input type="hidden" name="range1[]" value="'+ val.range.a +'">\
								<input type="hidden" name="range2[]" value="'+ val.range.b +'">\
								<input type="hidden" name="diskon[]" value="'+ val.diskon +'">\
								'+val.nm_gol+'\
							</td>\
							<td>'+val.range.a+' s/d '+val.range.b+'</td>\
							<td>'+val.diskon+'%</td>\
							<td><a href="#_" onclick="remove_list(this)"><i class="fa fa-trash text-red"></i> Hapus</a></td>\
						</tr>');
			$("#tbl-diskon tbody").append($row);
		});
		// Clear Input
		$("#golongan_member").val('');
		$("#range1").val('');
		$("#range2").val('');
		$("#diskon").val('');

		alertify.success('Data berhasil ditambahkan');
		$("#modal-form").modal('hide');

		return true;
	}else{
		alertify.error('Maaf, Golongan dan Range Jumlah yang ada masukkan sudah ada dalam daftar');
		return false;
	}
}

function remove_list(obj){
	var conf = confirm('Apakah anda yakin akan menghapus data ini ?');
	if(conf){
		$(obj).closest("tr").remove();
	}
}

function hitung_harga_by_margin(){
	var hpp = $("#hpp").val(),
		tipe_margin = $("#tipe_margin").val(),
		margin = $("#margin").val().replace(/,/g,""),
		harga = $("#harga").val();
	if(hpp === ''){
		alertify.error('Silahkan pilih hpp dulu');
		return false;
	}

	if(!Number(margin) || parseInt(margin) < 0){
		if(harga != ''){
			alertify.error('Margin tidak boleh lebih kecil dari 0');
		}
		
		return false;
	}

	var harga = 0,
		margin_value = 0;

	if(tipe_margin == 1){ // Prosentase
		margin_value = Number(margin) * Number(hpp) / 100;
		harga = Number(hpp) + margin_value;
	}else{ // Angka rupiah
		harga = Number(hpp) + Number(margin);
	}

	$("#harga").val(harga);

	return harga;
}

function hitung_margin(){
	var harga = $("#harga").val().replace(/,/g,""),
		margin_tipe = $("#tipe_margin").val(),
		hpp = $("#hpp").val();

	if(hpp === ''){
		alertify.error('Silahkan pilih hpp dulu');
		return false;
	}

	if(!Number(harga)){
		harga = 0;
	}

	var margin = 0;
	if(margin_tipe == 1){ //Prosentase
		margin = (Number(harga) - Number(hpp))/Number(hpp) * 100;
	}else{ // Angka rupiah
		margin = Number(harga) - Number(hpp);
	}

	$("#margin").val(margin);

	return margin;
}