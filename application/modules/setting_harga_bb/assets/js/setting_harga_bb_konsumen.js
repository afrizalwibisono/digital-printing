var deleted_harga = [];

$(function(){
	$("#add_to_list").on('click', function(){
		add_to_list();
	});

	$("#harga").number(true);
	$("#margin").number(true);

	$('#hpp').on('change', function(){
		if(this.value !== ''){
			hitung_harga_by_margin();
		}else{
			$("#harga").val(0);
		}
	});

	$("#tipe_margin").on('change', hitung_harga_by_margin);
	$("#margin").on('keyup', function(){
		if(Number((this.value).replace(/,/g,"")) >= 0){
			hitung_harga_by_margin();
		}else{
			$("#harga").val(0);
		}	
	});

	$("#harga").on("keyup", hitung_margin);

	$("#id_konsumen").select2({
		placeholder: '-- Pilih Konsumen --',
		dropdownParent: $("#modal-form"),
		ajax: {
		    url: siteurl+"setting_harga_bb/get_konsumen",
		    type: "post",
		    dataType: "json",
		    data: function (params) {
		      	var query = {
			        search: params.term,
			        page: params.page || 0,
			        per_page: 30 // limit to show
			    }

		      	return query;
		    },
		    processResults: function (data, params) {
			    params.page = params.page || 0;

			    var arr = [];
			    data.map(function(n, i){
			    	arr.push({id: n.id, text: n.nm, telp: (n.wa !='' && n.wa !='-' ? n.wa : (n.telp !='' && n.telp !='-' ? n.telp: ''))});
			    });

			    return {
			        results: arr,
			        pagination: {
			            more: (params.page * 10) < data.length
			        }
			    };
			}
		},
		templateResult: dataFormatResult,
		templateSelection: dataFormatSelection,
		escapeMarkup: function(m) {
		    return m;
		}
	});

	// Clear modal form
	$("#modal-form").on("shown.bs.modal", function(){
		$("#id_konsumen").val("").trigger("change");
		$("#hpp").val("");
		$("#tipe_margin").val(1);
		$("#margin").val("");
		$("#harga").val("");
	});
	//Append deleted harga
	$("#frm_harga_bb").on("submit", function(e){
		deleted_harga.map(function(n, i){
			$("#frm_harga_bb").append($("<input>").attr({"type": "hidden","name": "deleted_harga[]"}).val(n));
		});
	});
});

function dataFormatResult(data) {
	var markup = data.text +"<br>"+ data.telp;
	return markup;
}

function dataFormatSelection(data) {
	return data.text + (data.telp ? ' [ '+data.telp+' ]' : '');
}

function add_to_list(){
	var idkonsumen = $("#id_konsumen").val(),
		skon = $("#id_konsumen").select2('data'),
		nm_konsumen = skon[0].text+(skon[0].telp ? ' [ '+skon[0].telp+' ]' : ''),
		hpp = $("#hpp").val(),
		tipe_margin = $("#tipe_margin").val(),
		margin = $("#margin").val().replace(/,/g,""),
		harga = $("#harga").val();

	if(idkonsumen === ''){
		alertify.error('Silahkan pilih '+ $('label[for="id_konsumen"]').text());
		return false;
	}

	if(hpp === ''){
		alertify.error('Silahkan pilih '+ $('label[for="hpp"]').text());
		return false;
	}

	if(harga == 0){
		alertify.error('Harga tidak boleh sama dengan 0');
		return false;
	}

	var margin_value = 0;

	if(tipe_margin == 1){ // Prosentase
		margin_value = Number(margin) * Number(hpp) / 100;
	}else{
		margin_value = margin;
	}

	// Cek apakah sudah ada dalam list
	var cek = $("#tbl-harga input[name='id_konsumen[]'][value='"+idkonsumen+"']").length;

	if(cek == 0){
		// Set Value to List
		var $row = $('<tr>\
						<td></td>\
						<td>\
							<input type="hidden" name="id_harga_bb_konsumen[]" value="">\
							<input type="hidden" name="id_konsumen[]" value="'+ idkonsumen +'">\
							<input type="hidden" name="hpp[]" value="'+ hpp +'">\
							<input type="hidden" name="tipe_margin[]" value="'+ tipe_margin +'">\
							<input type="hidden" name="margin[]" value="'+ margin +'">\
							<input type="hidden" name="harga[]" value="'+ harga +'">\
							'+nm_konsumen+'\
						</td>\
						<td>'+$.number(hpp)+'</td>\
						<td>'+$.number(margin_value)+'</td>\
						<td>'+$.number(harga)+'</td>\
						<td><a href="#_" onclick="remove_list(this)"><i class="fa fa-trash text-red"></i> Hapus</a></td>\
					</tr>');
		$("#tbl-harga tbody").append($row);

		buat_no();
		
		alertify.success('Data berhasil ditambahkan');
		$("#modal-form").modal('hide');

		return true;
	}else{
		alertify.error('Maaf, Konsumen yang ada masukkan sudah ada dalam daftar');
		return false;
	}
}

function remove_list(obj){
	var conf = confirm('Apakah anda yakin akan menghapus data ini ?');
	if(conf){
		var id_harga 	= $(obj).closest("tr").find("input[name='id_harga_bb_konsumen[]']").val();
		if(id_harga){
			deleted_harga.push(id_harga);
		}

		$(obj).closest("tr").remove();
	}
}

function hitung_harga_by_margin(){
	var hpp = $("#hpp").val(),
		tipe_margin = $("#tipe_margin").val(),
		margin = $("#margin").val().replace(/,/g,""),
		harga = $("#harga").val();
	if(hpp === ''){
		alertify.error('Silahkan pilih hpp dulu');
		return false;
	}

	if(parseInt(margin) < 0){
		$("#margin").val(0);
		if(harga != ''){
			alertify.error('Margin tidak boleh lebih kecil dari 0');
		}
		
		return false;
	}

	var harga = 0,
		margin_value = 0;

	if(tipe_margin == 1){ // Prosentase
		margin_value = Number(margin) * Number(hpp) / 100;
		harga = Number(hpp) + margin_value;
	}else{ // Angka rupiah
		harga = Number(hpp) + Number(margin);
	}

	$("#harga").val(harga);

	return harga;
}

function hitung_margin(){
	var harga = $("#harga").val().replace(/,/g,""),
		margin_tipe = $("#tipe_margin").val(),
		hpp = $("#hpp").val();

	if(hpp === ''){
		alertify.error('Silahkan pilih hpp dulu');
		return false;
	}

	if(!Number(harga)){
		harga = 0;
	}

	var margin = 0;
	if(margin_tipe == 1){ //Prosentase
		margin = (Number(harga) - Number(hpp))/Number(hpp) * 100;
	}else{ // Angka rupiah
		margin = Number(harga) - Number(hpp);
	}

	$("#margin").val(margin);

	return margin;
}

function buat_no(){
	$("#tbl-harga tbody tr").map(function(i, el){
		$(this).find("td:first").text(i+1);
	});
}