<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2019, Cokeshome
 * 
 * This is controller for Bahan Baku

 */

class Setting_harga_bb extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Setting Harga BB.View";
    protected $managePermission = "Setting Harga BB.Manage";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('setting_harga_bb/setting_harga_bb');
        $this->load->model([
                            'bahan_baku_model',
                            'kategori_model',
                            'stok_model',
                            'setting_harga_bb_model',
                            'setting_harga_bb_konsumen_model',
                            'setting_harga_bb_diskon_model',
                            'konsumen_model',
                            'gol_harga_model',
                        ]);
        $this->template->title(lang('title_manage'));
		$this->template->page_icon('fa fa-table');
    }

    public function index(){
        
        $this->auth->restrict($this->viewPermission);

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
            $idkategori_search   = $this->input->post('idkategori_search');
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
            $idkategori_search   = $this->input->get('kategori');
        }

        $filter = "?search=".$search;

        $search2 = $this->db->escape_str($search);
        
        $addWhere = "";

        if($idkategori_search !=""){

            $addWhere .= "AND barang.idkategori= $idkategori_search ";
            $filter   .= "&kategori=".urldecode($idkategori_search);

        }
        
        $where  = "barang.deleted = 0 $addWhere
                    AND (`nm_barang` like '%$search2%' or `ket_barang` LIKE '%$search2%' ESCAPE '!')";

        
        $total  = $this->bahan_baku_model
                        ->where($where)
                        ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->bahan_baku_model->select("`barang`.`idbarang_bb`,
                                                    `barang`.`st_fix`,
                                                    `barang`.`nm_barang`,
                                                    `merk_bb`.`nm_merk_bb`,
                                                    `jenis_bb`.`nmjenis_bb`,
                                                    `kategori`.`nmkategori`,
                                                    `satuan_terkecil`.`alias`,
                                                    `harga`")
                        ->join('kategori',"barang.idkategori=kategori.idkategori","left")
                        ->join('jenis_bb',"barang.idjenis_bb=jenis_bb.idjenis_bb","left")
                        ->join('merk_bb',"barang.idmerk_bb=merk_bb.idmerk_bb","left")
                        ->join('satuan_terkecil',"barang.id_satuan_terkecil=satuan_terkecil.id_satuan_terkecil","left")
                        ->join('setting_harga_bb','barang.idbarang_bb=setting_harga_bb.idbarang_bb','left')
                        ->where($where)
                        ->order_by('`barang`.`idkategori`','ASC')
                        ->order_by('nm_barang','ASC')
                        ->limit($limit, $offset)->find_all();

        $assets = array('plugins/select2/dist/js/select2.js', 
                        'plugins/select2/dist/css/select2.css',
                        'setting_harga_bb/assets/js/setting_harga_bb_index.js',
                        'plugins/number/jquery.number.js'
                        );
        
        add_assets($assets);

        $data_kategori  = $this->kategori_model->where("deleted",0)
                                                ->order_by("nmkategori","ASC")
                                                ->find_all();
        
        $this->template->set('data_kategori', $data_kategori);

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('kategori', $idkategori_search);

        $this->template->set("toolbar_title", lang('title_manage'));
        $this->template->title(lang('title_manage'));
        $this->template->set("numb", $offset+1);
        
        $this->template->render('index'); 
    }

    public function edit($id){

        $this->auth->restrict($this->managePermission);

        if (isset($_POST['save'])){
            if ($this->save($id)){
                $this->template->set_message(lang("edit_success"), 'success');
            }
        }

        $data_barang    = $this->bahan_baku_model->select("`idbarang_bb`,
                                                            `barcode`,
                                                            `barang`.`idkategori`,
                                                            `barang`.`idjenis_bb`,
                                                            `barang`.`idmerk_bb`,
                                                            `nm_barang`,
                                                            `id_satuan_terkecil`,
                                                            `ket_barang`,
                                                            `st_potong_meteran`,
                                                            `panjang_spek`,
                                                            `lebar_spek`,
                                                            `barang`.`id_konversi`,
                                                            `ket_spek`,
                                                            st_hpp_manual,
                                                            hpp_manual,
                                                            nmkategori,
                                                            nm_merk_bb,
                                                            nmjenis_bb")
                                                ->join('kategori','barang.idkategori=kategori.idkategori','left')
                                                ->join('merk_bb','barang.idmerk_bb=merk_bb.idmerk_bb','left')
                                                ->join('jenis_bb','barang.idjenis_bb=jenis_bb.idjenis_bb','left')
                                                ->find($id);
        $list_hpp = $this->get_list_hpp($id);
        $dt_harga = $this->setting_harga_bb_model->find_by(['idbarang_bb' => $id, 'deleted' => 0]);
        $dt_diskon = [];
        if($dt_harga){
            $dt_diskon= $this->setting_harga_bb_diskon_model->select("idsetting_harga_bb_diskon, idsetting_harga_bb, id_golongan_member, 
                                                                range1, range2, diskon, nm_gol as nm_golongan")
                                                        ->join('gol_harga','setting_harga_bb_diskon.id_golongan_member=gol_harga.id_gol','left')
                                                        ->find_all_by(['idsetting_harga_bb' => $dt_harga->idsetting_harga_bb, 'setting_harga_bb_diskon.deleted' => 0]);
        }
        
        $arr_diskon = [];
        if($dt_diskon){
            foreach ($dt_diskon as $key => $val) {
                $arr_diskon[] = [
                                  'gol'     => intval($val->id_golongan_member),
                                  'nm_gol'  => $val->nm_golongan,
                                  'range'   => ['a' => $val->range1+0, 'b' => $val->range2+0],
                                  'diskon'  => $val->diskon+0
                                ];
            }
        }

        $gol_harga = $this->gol_harga_model
                            ->order_by('nm_gol', 'ASC')
                            ->find_all_by(['deleted' => 0]);

        $assets = array('plugins/select2/dist/js/select2.js', 
                        'plugins/select2/dist/css/select2.css',
                        'plugins/number/jquery.number.js',
                        'setting_harga_bb/assets/js/setting_harga_bb_form.js'
                        );
        
        add_assets($assets);

        $this->template->set("dt", $data_barang);
        $this->template->set("hrg", $dt_harga);
        $this->template->set("list_diskon", $dt_diskon);
        $this->template->set("json_diskon", json_encode($arr_diskon));
        $this->template->set("list_hpp", $list_hpp);
        $this->template->set("gol_harga", $gol_harga);
        $this->template->title(lang('title_edit'));

        $this->template->render('setting_harga_bb_form');
    }

    public function harga_konsumen($id){

        $this->auth->restrict($this->managePermission);

        if (isset($_POST['save'])){
            if ($this->save_harga_konsumen($id)){
                $this->template->set_message(lang("edit_success"), 'success');
            }
        }

        $data_barang    = $this->bahan_baku_model->select("`idbarang_bb`,
                                                            `barcode`,
                                                            `barang`.`idkategori`,
                                                            `barang`.`idjenis_bb`,
                                                            `barang`.`idmerk_bb`,
                                                            `nm_barang`,
                                                            `id_satuan_terkecil`,
                                                            `ket_barang`,
                                                            `st_potong_meteran`,
                                                            `panjang_spek`,
                                                            `lebar_spek`,
                                                            `barang`.`id_konversi`,
                                                            `ket_spek`,
                                                            st_hpp_manual,
                                                            hpp_manual,
                                                            nmkategori,
                                                            nm_merk_bb,
                                                            nmjenis_bb")
                                                ->join('kategori','barang.idkategori=kategori.idkategori','left')
                                                ->join('merk_bb','barang.idmerk_bb=merk_bb.idmerk_bb','left')
                                                ->join('jenis_bb','barang.idjenis_bb=jenis_bb.idjenis_bb','left')
                                                ->find($id);
        $list_hpp = $this->get_list_hpp($id);

        $list_harga = $this->setting_harga_bb_konsumen_model->select(['id_harga_bb_konsumen',
                            'setting_harga_bb_konsumen.idkonsumen',
                            'hpp','tipe_margin','margin_persen','margin_angka','margin','harga',
                            "CONCAT(`konsumen`.`panggilan`,' ',`konsumen`.`nama`) AS nm_konsumen",
                            ])
                            ->join('konsumen','setting_harga_bb_konsumen.idkonsumen=konsumen.idkonsumen')
                            ->where("setting_harga_bb_konsumen.deleted", 0)
                            ->where("setting_harga_bb_konsumen.idbarang_bb", $id)
                            ->find_all();

        $assets = array('plugins/select2/dist/js/select2.js', 
                        'plugins/select2/dist/css/select2.css',
                        'plugins/number/jquery.number.js',
                        'setting_harga_bb/assets/js/setting_harga_bb_konsumen.js'
                        );
        
        add_assets($assets);

        $this->template->set("dt", $data_barang);
        $this->template->set("list_hpp", $list_hpp);
        $this->template->set("list_harga", $list_harga);
        $this->template->title(lang('title_edit_konsumen'));

        $this->template->render('setting_harga_bb_konsumen');
    }

    public function get_konsumen(){
        if(!$this->input->is_ajax_request()){
            echo json_encode(["results"=> [],"pagination" => ["more" => false]]);
            die();
        }

        $search = $this->input->post("search");
        $per_page   = $this->input->post("per_page");
        $page   = $this->input->post("page");
        $start  = (intval($page)-1)*$per_page;

        $dt_konsumen    = $this->konsumen_model
                                ->select("`idkonsumen` as id, 
                                    CONCAT(IF(panggilan IS NULL, '', panggilan),
                                    ' ',
                                    `nama`,
                                    ' | ',
                                    IF(st = 0,
                                        'konsumen',
                                        IF(st = 1, 'reseller', 'instansi'))) AS nm,
                                    wa, telp")
                                ->where("deleted", 0)
                                ->like("nama", $search)
                                ->or_like("wa", $search)
                                ->or_like("telp", $search)
                                ->order_by("nama","asc")
                                ->limit($per_page, $page)
                                ->find_all();

        if($dt_konsumen){
            echo json_encode($dt_konsumen);
            die();
        }

        echo json_encode([]);
        
    }

    private function get_list_hpp($id_barang)
    {
        if(!$id_barang)
        {
            return [];
        }

       
        $dt_hpp = $this->stok_model->select('hpp')
                            ->group_by('hpp')
                            ->order_by("hpp","DESC")
                            ->find_all_by(array('id_barang' => $id_barang, 'stok_update >' => 0, 'st_bonus' => 0));

        return $dt_hpp;
    }

    protected function save($id = ''){
        if(!$id){
            $this->template->set_message(lang("invalid_id"), 'error');
            return false;
        }

        $_POST['harga'] = remove_comma($_POST['harga']);
        $_POST['margin'] = remove_comma($_POST['margin']);

        $this->form_validation->set_rules('hpp','lang:hpp','callback_validate_hpp');
        $this->form_validation->set_rules('margin','lang:margin','required|numeric');
        $this->form_validation->set_rules('harga','lang:harga','greater_than_equal_to[0]');

        if($this->form_validation->run() == false){
            $this->template->set_message(validation_errors(), 'error');
            return false;
        }

        $hpp         = $this->input->post('hpp');
        $tipe_margin = $this->input->post('tipe_margin');
        $margin      = $this->input->post('margin');
        // Diskon
        $id_gol = $this->input->post('id_gol');
        $range1 = $this->input->post('range1');
        $range2 = $this->input->post('range2');
        $diskon = $this->input->post('diskon');

        // Hitung Ulang Harga
        $harga        = 0;
        $margin_value = 0;
        if($tipe_margin == 1){ // Prosentase
            $margin_value = $margin * $hpp / 100;
            $harga = $hpp + $margin_value;
        }else{
            $margin_value = $margin;
            $harga = $hpp + $margin_value;
        }

        $insert_data = [
                        'idbarang_bb' => $id,
                        'hpp'         => $hpp,
                        'tipe_margin' => $tipe_margin,
                        'margin'      => $margin_value,
                        'harga'       => $harga,
                        ];
        
        if($tipe_margin == 1){ // Prosentase
            $insert_data['margin_persen'] = $margin;
            $insert_data['margin_angka'] = NULL;
        }else{
            $insert_data['margin_angka'] = $margin;
            $insert_data['margin_persen'] = NULL;
        }

        $this->db->trans_start();
        // Save Harga
        $sql = '';
        $cek = $this->setting_harga_bb_model->find_by(['idbarang_bb' => $id]);
        if($cek){
            // Update Harga
            $result = $this->setting_harga_bb_model->update($cek->idsetting_harga_bb, $insert_data);
            $sql .= $this->db->last_query();
            // Delete All diskon sebelumnya
            $this->setting_harga_bb_diskon_model->delete_where(['idsetting_harga_bb' => $cek->idsetting_harga_bb]);
            $sql .= $this->db->last_query();
            // Insert Diskon Ulang
            if($id_gol){
                foreach ($id_gol as $key => $val) {
                    $data_diskon = [
                                    'idsetting_harga_bb' => $cek->idsetting_harga_bb,
                                    'id_golongan_member' => $val,
                                    'range1'             => $range1[$key],
                                    'range2'             => $range2[$key],
                                    'diskon'             => $diskon[$key],
                                    ];

                    $this->setting_harga_bb_diskon_model->insert($data_diskon);
                    $sql .= $this->db->last_query();
                }
            }
        }else{
            // Insert Harga
            $id_setting_harga_bb = $this->setting_harga_bb_model->insert($insert_data);
            $sql .= $this->db->last_query();
            // Insert Diskon
            if($id_gol){
                foreach ($id_gol as $key => $val) {
                    $data_diskon = [
                                    'idsetting_harga_bb' => $id_setting_harga_bb,
                                    'id_golongan_member' => $val,
                                    'range1'             => $range1[$key],
                                    'range2'             => $range2[$key],
                                    'diskon'             => $diskon[$key],
                                    ];

                    $this->setting_harga_bb_diskon_model->insert($data_diskon);
                    $sql .= $this->db->last_query();
                }
            }
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->template->set_message($this->setting_harga_bb_model->error."\n\n".$this->setting_harga_bb_diskon_model->error, 'error');
            
            $keterangan = "GAGAL, Setting harga bahan baku dengan ID barang, : ".$insert_data['idbarang_bb'];
            $status     = 0;

            $return = FALSE;
        }
        else
        {
            $keterangan = "SUKSES, Setting harga bahan baku dengan ID barang, : ".$insert_data['idbarang_bb'];
            $status     = 1;

            $return = TRUE;
        }

        //Save Log
        $nm_hak_akses   = $this->managePermission; 
        $kode_universal = $cek ? $cek->idsetting_harga_bb : $id_setting_harga_bb;
        $jumlah         = $harga;
        $sql            = $sql;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        return $return;
    }

    protected function save_harga_konsumen($id = ''){
        if(!$id){
            $this->template->set_message(lang("invalid_id"), 'error');
            return false;
        }

        $id_harga_bb_konsumen = $this->input->post('id_harga_bb_konsumen');
        $id_konsumen = $this->input->post('id_konsumen');
        $hpp         = $this->input->post('hpp');
        $tipe_margin = $this->input->post('tipe_margin');
        $margin      = $this->input->post('margin');
        $harga       = $this->input->post('harga');
        $deleted_harga = $this->input->post('deleted_harga');

        $insert_data = [];
        if($id_konsumen){
            foreach ($id_konsumen as $key => $val) {
                if($id_harga_bb_konsumen[$key] == ''){ // Jika inputan baru
                    //Hitung ulang harga
                    if($tipe_margin[$key] == 1){ // Prosentase
                        $margin_value = $margin[$key] * $hpp[$key] / 100;
                        $harga = $hpp[$key] + $margin_value;
                    }else{
                        $margin_value = $margin[$key];
                        $harga = $hpp[$key] + $margin_value;
                    }

                    $insert_data[] = [
                        'idbarang_bb' => $id,
                        'idkonsumen'  => $val,
                        'hpp'         => $hpp[$key],
                        'tipe_margin' => $tipe_margin[$key],
                        'margin_persen' => $tipe_margin[$key] == 1 ? $margin[$key] : NULL,
                        'margin_angka'  => $tipe_margin[$key] == 2 ? $margin_value : NULL,
                        'margin'      => $margin_value,
                        'harga'       => $harga,
                    ];
                }
            }
        }

        $this->db->trans_start();
        // Save Harga
        $sql = '';
        if(is_array($deleted_harga) && count($deleted_harga)){
            //delete harga
            foreach ($deleted_harga as $key => $val) {
                $this->setting_harga_bb_konsumen_model->delete($val);
                $sql .= $this->db->last_query();
            }
        }
        // Insert harga baru
        if($insert_data){
            $this->setting_harga_bb_konsumen_model->insert_batch($insert_data);
            $sql .= $this->db->last_query();
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->template->set_message($this->setting_harga_bb_konsumen_model->error, 'error');
            
            $keterangan = "GAGAL, Setting harga bahan baku per konsumen dengan ID barang, : ".$id;
            $status     = 0;

            $return = FALSE;
        }
        else
        {
            $keterangan = "SUKSES, Setting harga bahan baku per konsumen dengan ID barang, : ".$id;
            $status     = 1;

            $return = TRUE;
        }

        //Save Log
        $nm_hak_akses   = $this->managePermission; 
        $kode_universal = $id;
        $jumlah         = 0;
        $sql            = $sql;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        return $return;
    }

    public function validate_hpp($val){
        $this->form_validation->set_message('validate_hpp', 'You should choose the "%s" field');
        if($val === ''){
            return false;
        }

        return true;
    }
}