<div class="box box-primary">
    <!--form start -->
<?= form_open($this->uri->uri_string(),array('id'=>'frm_bahan_baku','name' => 'frm_bahan_baku','role'=>'form','class'=>'form-horizontal'))?>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="nm_barang" class="col-sm-4 control-label"><?= lang('nama') ?></label>
                    <div class="col-sm-8">
                        <span class="form-control" id="nm_barang" ><?= strtoupper($dt->nm_barang) ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="kategori" class="col-sm-4 control-label"><?= lang('idkategori') ?></label>
                    <div class="col-sm-8">
                        <span class="form-control" id="kategori" ><?= strtoupper($dt->nmkategori) ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="merek" class="col-sm-4 control-label"><?= lang('idmerk_bb') ?></label>
                    <div class="col-sm-8">
                        <span class="form-control" id="merek" ><?= strtoupper($dt->nm_merk_bb) ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="jenis" class="col-sm-4 control-label"><?= lang('idjenis_bb') ?></label>
                    <div class="col-sm-8">
                        <span class="form-control" id="jenis" ><?= strtoupper($dt->nmjenis_bb) ?></span>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <label for="hpp" class="col-sm-4 control-label"><?= lang('hpp') ?></label>
                    <div class="col-sm-8">
                        <select id="hpp" name="hpp" class="form-control">
                            <option value="" <?= set_select('hpp', '', isset($hrg->hpp) && $hrg->hpp == '') ?>>-- <?= lang('pilih_hpp') ?> --</option>
                            <?php 
                                if(is_array($list_hpp)) :
                                foreach ($list_hpp as $key => $hpp) : ?>
                            <option value="<?= $hpp->hpp ?>" <?= set_select('hpp', $hpp->hpp, isset($hrg->hpp) && $hrg->hpp == $hpp->hpp) ?>><?= number_format($hpp->hpp) ?></option>
                            <?php endforeach;endif; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tipe_margin" class="col-sm-4 control-label"><?= lang('margin') ?></label>
                    <div class="col-sm-8 form-inline">
                        <select id="tipe_margin" name="tipe_margin" class="form-control">
                            <option value="1" <?= set_select('tipe_margin', 1, isset($hrg->tipe_margin) && $hrg->tipe_margin == 1) ?>>%</option>
                            <option value="2" <?= set_select('tipe_margin', 2, isset($hrg->tipe_margin) && $hrg->tipe_margin == 2) ?>>Rp</option>
                        </select>
                        <input type="text" class="form-control" id="margin" name="margin" value="<?= set_value('margin', isset($hrg->tipe_margin) ? ($hrg->tipe_margin == 1 ? $hrg->margin_persen : $hrg->margin_angka) : '' ) ?>" placeholder="nilai margin">
                    </div>
                </div>
                <div class="form-group">
                    <label for="harga" class="col-sm-4 control-label"><?= lang('harga') ?></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="harga" name="harga" value="<?= set_value('harga', isset($hrg->harga) ? $hrg->harga : '') ?>" placeholder="">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-offset-2 col-sm-10 table-responsive">
                <h4><?= lang('pengaturan_diskon_label') ?><button type="button" class="btn btn-sm btn-success" style="margin-left: 25px" id="add-diskon" data-toggle="modal" data-target="#modal-form"><i class="fa fa-plus"></i> <?= lang('btn_new') ?></button></h4>    
                <table class="table" id="tbl-diskon">
                    <thead>
                        <tr class="success">
                            <th>#</th>
                            <th><?= lang('gol_member') ?></th>
                            <th><?= lang('range_qty') ?></th>
                            <th><?= lang('diskon') ?></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if($list_diskon): 
                            foreach ($list_diskon as $key => $val) :
                        ?>
                        <tr>
                            <td><?= $key+1 ?></td>
                            <td>
                                <input type="hidden" name="id_gol[]" value="<?= $val->id_golongan_member ?>">
                                <input type="hidden" name="range1[]" value="<?= $val->range1 ?>">
                                <input type="hidden" name="range2[]" value="<?= $val->range2 ?>">
                                <input type="hidden" name="diskon[]" value="<?= $val->diskon ?>">
                                <?= $val->nm_golongan ?>
                            </td>
                            <td><?= $val->range1.' s/d '.$val->range2 ?></td>
                            <td><?= $val->diskon ?>%</td>
                            <td><a href="#_" onclick="remove_list(this)"><i class="fa fa-trash text-red"></i> Hapus</a></td>
                        </tr>
                        <?php endforeach;endif; ?>
                    </tbody>
                </table>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" name="save" class="btn btn-primary"><?= lang('btn_save') ?></button>
                        <?= lang('bf_or') . ' ' . anchor('setting_harga_bb', lang('btn_cancel')); ?>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
<?= form_close() ?>
</div>

<div class="modal" id="modal-form" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= form_open("",array('id'=>'frm_form','name'=>'frm_form','role'=>'form', 'class' => 'form-horizontal')) ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= lang('pengaturan_diskon_label') ?></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="golongan_member" class="col-sm-4 control-label"><?= lang('gol_member') ?></label>
                        <div class="col-sm-8">
                            <select id="golongan_member" name="golongan_member" class="form-control">
                                <option value="" selected="">-- <?= lang('plih_golongan') ?> --</option>
                                <?php if($gol_harga): 
                                    foreach ($gol_harga as $key => $val) :
                                ?>
                                <option value="<?= $val->id_gol ?>"><?= ucwords($val->nm_gol) ?></option>
                                <?php endforeach;endif; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="range1"><?= lang("range_qty") ?></label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" name="range1" id="range1" class="form-control" />
                                <span class="input-group-addon">s/d</span>
                                <input type="text" name="range2" id="range2" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="diskon" class="col-sm-4 control-label"><?= lang('diskon') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="diskon" name="diskon" value="" placeholder="%">
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="add_to_list" name="add_to_list"><?= lang('btn_add_list') ?></button>
                </div>
            <?= form_close() ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    var json_diskon = <?= $json_diskon ?>;
</script>