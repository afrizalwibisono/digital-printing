<?php
    $ENABLE_MANAGE = has_permission('Setting Harga BB.Manage');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_index','name'=>'frm_index'))?>
	<div class="box-header">
		<div class="form-group">
            <div class="pull-right form-inline"> <!-- style="margin-top: 5px;" -->
                <div class="form-group">
                    <select id="idkategori_search" name="idkategori_search" class="form-control" style="min-width: 160px;">
                        <option></option>
                        <?php foreach ($data_kategori as $key => $value) : ?>
                        <option value="<?= $value->idkategori; ?>" <?= set_select('idkategori', $value->idkategori, isset($kategori) && $kategori == $value->idkategori) ?>><?= $value->nmkategori ?></option> 
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="input-group">
                    <input type="text" name="table_search" value="<?php echo isset($search) ? $search:''; ?>" class="form-control pull-right" placeholder="Search" autofocus>
                    <div class="input-group-btn">
                        <button class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                </div>  
            </div>
		</div>
	</div>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
	<div class="box-body table-responsive no-padding">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th width="50">#</th>
                        <th><?= lang('nama') ?></th>
                        <th><?= lang('idkategori') ?></th>
                        <th><?= lang('idjenis_bb') ?></th>
                        <th><?= lang('idmerk_bb') ?></th>
                        <th><?= lang('id_satuan_terkecil') ?></th>
                        <th><?= lang('harga') ?></th>
                        <?php if($ENABLE_MANAGE) : ?>
                        <th></th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($results as $record) : ?>
                    <tr>
                        <td><?= $numb; ?></td>
                        <td><?= $record->nm_barang ?></td>
                        <td><?= $record->nmkategori ?></td>
                       	<td><?= $record->nmjenis_bb ?></td>
                        <td><?= $record->nm_merk_bb ?></td>
                        <td><?= $record->alias ?></td>
                        <td><?= number_format($record->harga) ?></td>
                        <td style="padding-right:20px">
                            <?php if($ENABLE_MANAGE) : ?>
                                <a class="text-black" href="<?= site_url('setting_harga_bb/edit/' . $record->idbarang_bb); ?>" data-toggle="tooltip" data-placement="left" title="Edit Harga"><i class="fa fa-money"></i></a>
                                &nbsp;|&nbsp;
                                <a class="text-black" href="<?= site_url('setting_harga_bb/harga_konsumen/' . $record->idbarang_bb); ?>" data-toggle="tooltip" data-placement="left" title="Edit Harga Khusus Per Konsumen"><i class="fa fa-user"></i></a>
                            <?php endif; ?>
                        </td>
                        
                    </tr>
                    <?php $numb++; endforeach; ?>
                </tbody>
	  </table>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">		
		<?php
		echo $this->pagination->create_links(); 
		?>
	</div>
	<?php else: ?>
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('no_records_found') ?></p>
    </div>
    <?php
	endif;
	echo form_close(); 
	?>	
</div><!-- /.box-->