<div class="box box-primary">
    <!--form start -->
<?= form_open($this->uri->uri_string(),array('id'=>'frm_harga_bb','name' => 'frm_harga_bb','role'=>'form','class'=>'form-horizontal'))?>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="nm_barang" class="col-sm-4 control-label"><?= lang('nama') ?></label>
                    <div class="col-sm-8">
                        <span class="form-control" id="nm_barang" ><?= strtoupper($dt->nm_barang) ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="kategori" class="col-sm-4 control-label"><?= lang('idkategori') ?></label>
                    <div class="col-sm-8">
                        <span class="form-control" id="kategori" ><?= strtoupper($dt->nmkategori) ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="merek" class="col-sm-4 control-label"><?= lang('idmerk_bb') ?></label>
                    <div class="col-sm-8">
                        <span class="form-control" id="merek" ><?= strtoupper($dt->nm_merk_bb) ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="jenis" class="col-sm-4 control-label"><?= lang('idjenis_bb') ?></label>
                    <div class="col-sm-8">
                        <span class="form-control" id="jenis" ><?= strtoupper($dt->nmjenis_bb) ?></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-offset-2 col-sm-10 table-responsive">
                <h4><?= lang('pengaturan_harga_label_konsumen') ?><button type="button" class="btn btn-sm btn-success" style="margin-left: 25px" id="add-diskon" data-toggle="modal" data-target="#modal-form"><i class="fa fa-plus"></i> <?= lang('btn_new') ?></button></h4>    
                <table class="table" id="tbl-harga">
                    <thead>
                        <tr class="success">
                            <th>#</th>
                            <th><?= lang('nm_konsumen') ?></th>
                            <th><?= lang('hpp') ?></th>
                            <th><?= lang('margin') ?></th>
                            <th><?= lang('harga') ?></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if($list_harga): 
                            foreach ($list_harga as $key => $val) :
                        ?>
                        <tr>
                            <td><?= $key+1 ?></td>
                            <td>
                                <?= $val->nm_konsumen ?>
                                <input type="hidden" name="id_harga_bb_konsumen[]" value="<?= $val->id_harga_bb_konsumen ?>">
                                <input type="hidden" name="id_konsumen[]" value="<?= $val->idkonsumen ?>">
                                <input type="hidden" name="hpp[]" value="<?= $val->hpp ?>">
                                <input type="hidden" name="tipe_margin[]" value="<?= $val->tipe_margin ?>">
                                <input type="hidden" name="margin[]" value="<?= $val->margin ?>">
                                <input type="hidden" name="harga[]" value="<?= $val->harga ?>">
                            </td>
                            <td><?= $val->hpp ?></td>
                            <td><?= number_format($val->margin) ?></td>
                            <td><?= number_format($val->harga) ?></td>
                            <td><a href="#_" onclick="remove_list(this)"><i class="fa fa-trash text-red"></i> Hapus</a></td>
                        </tr>
                        <?php endforeach;endif; ?>
                    </tbody>
                </table>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" name="save" id="save" class="btn btn-primary"><?= lang('btn_save') ?></button>
                        <?= lang('bf_or') . ' ' . anchor('setting_harga_bb', lang('btn_cancel')); ?>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
<?= form_close() ?>
</div>

<div class="modal" id="modal-form" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?= form_open("",array('id'=>'frm_form','name'=>'frm_form','role'=>'form', 'class' => 'form-horizontal')) ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?= lang('pengaturan_harga_label_konsumen') ?></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="id_konsumen" class="col-sm-4 control-label"><?= lang('nm_konsumen') ?></label>
                        <div class="col-sm-8">
                            <select id="id_konsumen" name="id_konsumen" class="form-control" style="width: 100%">
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="hpp" class="col-sm-4 control-label"><?= lang('hpp') ?></label>
                        <div class="col-sm-8">
                            <select id="hpp" name="hpp" class="form-control">
                                <option value="">-- <?= lang('pilih_hpp') ?> --</option>
                                <?php 
                                    if(is_array($list_hpp)) :
                                    foreach ($list_hpp as $key => $hpp) : ?>
                                    <option value="<?= $hpp->hpp ?>"><?= number_format($hpp->hpp) ?></option>
                                <?php endforeach;endif; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tipe_margin" class="col-sm-4 control-label"><?= lang('margin') ?></label>
                        <div class="col-sm-8 form-inline">
                            <select id="tipe_margin" name="tipe_margin" class="form-control">
                                <option value="1">%</option>
                                <option value="2">Rp</option>
                            </select>
                            <input type="text" class="form-control" id="margin" name="margin" value="" placeholder="nilai margin">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="harga" class="col-sm-4 control-label"><?= lang('harga') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="harga" name="harga" value="" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="add_to_list" name="add_to_list"><?= lang('btn_add_list') ?></button>
                </div>
            <?= form_close() ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->