<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ============================
$lang['title_manage']		= 'Setting Harga Bahan Baku';
$lang['title_edit']			= 'Edit Harga Bahan Baku';
$lang['title_edit_konsumen']= 'Edit Harga Bahan Baku Per Konsumen';

// from/table
$lang['idkategori']						= 'Kategori';
$lang['nama']							= 'Nama Barang';
$lang['idjenis_bb']						= 'Jenis Barang';
$lang['idmerk_bb']						= 'Merk Barang';
$lang['id_satuan_terkecil']				= 'Satuan';
$lang['hpp']							= 'HPP';
$lang['pilih_hpp']						= 'Pilih HPP';
$lang['margin']							= 'Margin';
$lang['harga']							= 'Harga';
$lang['pengaturan_diskon_label']		= 'Pengaturan Diskon Per Gol. Member';
$lang['pengaturan_harga_label_konsumen'] = 'Pengaturan Harga Per Konsumen';
$lang['gol_member']						= 'Gol. Member';
$lang['range_qty']						= 'Range Jumlah';
$lang['diskon']							= 'Diskon';
$lang['konsumen_biasa']					= 'Konsumen Biasa';
$lang['reseller']						= 'Reseller';
$lang['instansi']						= 'Instansi/Perusahaan';
$lang['plih_golongan']					= 'Pilih Gol. Member';
$lang['nm_konsumen']					= 'Nama Konsumen';

//button
$lang['btn_new']			= 'Baru';
$lang['btn_delete']			= 'Hapus';
$lang['btn_save']			= 'Simpan';
$lang['btn_cancel']			= 'Batal';
$lang['bf_or']				= 'or';
$lang['btn_add_list']		= 'Tambahkan';

//messages
$lang['del_error']			= 'Anda belum memilih data harga bahan baku yang akan dihapus.';
$lang['del_failure']		= 'Tidak dapat menghapus data harga bahan baku';
$lang['delete_confirm']		= 'Apakah anda yakin akan menghapus data harga bahan baku terpilih?';
$lang['deleted']			= 'Data harga bahan baku berhasil dihapus';
$lang['fail_deleted']		= 'Data harga bahan baku tidak dapat dihapus';
$lang['no_records_found']	= 'Data tidak ditemukan.';

$lang['create_failure']		= 'Data harga bahan baku gagal disimpan';
$lang['create_success']		= 'Data harga bahan baku berhasil disimpan';

$lang['edit_success']		= 'Data harga bahan baku berhasil disimpan';
$lang['invalid_id']			= 'ID tidak Valid';
$lang['no_konsumen']		= 'Anda belum menambahkan konsumen kedalam daftar';
