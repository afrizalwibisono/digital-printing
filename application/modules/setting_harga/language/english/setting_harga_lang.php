<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ============================
$lang['setting_harga_title_manage']		= 'Data Setting Harga';
$lang['setting_harga_title_new']		= 'Data Setting Harga Baru';
$lang['setting_harga_title_view']		= 'Data Setting Harga';
$lang['setting_harga_title_clonning']	= 'Clonning Harga';

// from/table
$lang['clonning_by']					= 'Target Data';

$lang['st_produk']						= 'Tipe';
$lang['isi_st_produk_produk']			= 'Produk';
$lang['isi_st_produk_finishing']		= 'Finishing';


$lang['idproduk']						= 'Produk';
$lang['kategori_produk']				= 'Kategori Produk';
$lang['idkategori_harga']				= 'Kategori Harga';

$lang['gol_harga_sumber']				= 'Gol. Harga Sumber';
$lang['gol_harga_tujuan']				= 'Gol. Harga Tujuan';

$lang['produk_bom']						= 'B.O.M / Produk';

$lang['st_range_harga']					= 'Status Range Harga';
$lang['isi_st_range_harga_range']		= 'Menggunakan Range Harga';
$lang['isi_st_range_harga_fix']			= 'Tidak Menggunakan Range Harga';

//===========================================================================
$lang['judul-tabel-type']				= "Tipe";
$lang['judul-tabel-kategori']			= "Kategori";
$lang['judul-tabel-kategori_harga']		= "Kategori Harga";
$lang['judul-tabel-produk']				= "Produk";

//===========================================================================
$lang['jml_no_range']					= "Jumlah";
$lang['ukuran']							= "Ukuran";
$lang['range']							= "Range Jumlah";
$lang['range1']							= "Range Awal";
$lang['range2']							= "Range Akhir";
$lang['satuan']							= "Satuan";
$lang['harga']							= "Harga";
$lang['harga_by']						= "Harga Berlaku Untuk";


//button
$lang['setting_harga_btn_new']			= 'Baru';
$lang['setting_harga_btn_clone']		= 'Clonning';
$lang['setting_harga_btn_delete']		= 'Hapus';
$lang['setting_harga_btn_save']			= 'Simpan';
$lang['setting_harga_btn_proses']		= 'Proses Clonning Harga';
$lang['setting_harga_btn_cancel']		= 'Batal';
$lang['setting_harga_btn_cancel_back']	= 'Batal / Kembali';
$lang['setting_harga_btn_add']			= 'Tambah ke daftar';
$lang['or_range']						= 'atau';

$lang['btn-delete']						= 'Hapus';


//messages
$lang['setting_harga_create_success']		= 'Data Setting Harga berhasil disimpan';
$lang['setting_harga_gagal_range_harga']	= 'Cek kembali data range harga';
$lang['warning_clonning']					= 'Clonning / Duplikasi Harga B.O.M akan secara otomatis menimpa data Harga B.O.M dengan Golongan Harga yang sama';


//konfirmasi
$lang['konfirmasi_edit_sukses']				= "Edit Data Setting Harga telah berhasil disimpan";
$lang['konfirmasi_cancel_sukses']			= "Data Setting Harga telah berhasil dibatalkan";
$lang['konfirmasi-data-tidak-ada']			= "Data tidak ditemukan";
$lang['konfirmasi-delete']					= "Apakah anda yakin akan menghapus Setting Harga terpilih ?";
$lang['konfirmasi-error-pil-delete']		= "Tidak ada data Setting Harga yang dipilih";
$lang['konfirmasi-delete-sukses']			= "Delete data Setting Harga sukses";
$lang['konfirmasi-simpan-gagal-ketemu']		= "Setting Harga dengan Produk dan Kategori Harga yang sama telah terdaftar dalam system";
$lang['konfirmasi-simpan-gagal-satuan']		= "Satuan hitung harga tidak sesuai dengan status Pemakaian Produk<br>Untuk pemakaian Normal, harus menggunakan satuan barang. Misalkan PCS, Box dll.<br>Untuk pemakaian Potongan, harus menggunakan satuan panjang. Misalkan mm, PCS, M";

$lang['konfirmasi-produk-kosong']			= "Anda belum memilih Produk yang akan di Clonning Harganya";
$lang['konfirmasi-kategori-kosong']			= "Anda belum memilih kategori BOM yang akan di Clonning Harganya";
$lang['konfirmasi-kategorisumber-kosong']	= "Data sumber clonning Harga tidak ditemukan, pastikan terlebih dahulu sumber data harganya belum dihapus atau sudah dibuat.";

$lang['konfirmasi-sukses-clonning'] 		= "Clonning data setting harga sukses";