<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 * 
 * This is controller for Setting_harga

 */

class Setting_harga extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Setting_harga.View";
    protected $addPermission    = "Setting_harga.Add";
    protected $managePermission = "Setting_harga.Manage";
    protected $deletePermission = "Setting_harga.Delete";

    protected $prefix           = "SH";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('setting_harga/setting_harga');
        $this->load->model(array(
                                    'produk_model',
                                    'produk_detail_penyusun_model',
                                    'setting_harga_model',
                                    'setting_harga_detail_model',
                                    'kategori_model',
                                    'kategori_harga_model',
                                    'konversi_model'
                                )
                            );

        $this->template->title(lang('setting_harga_title_manage'));
		$this->template->page_icon('fa fa-money');
    }

    public function index(){

        $this->auth->restrict($this->viewPermission);

        if(isset($_POST['delete']) && has_permission($this->deletePermission)){

            if($this->delete()){

                $this->template->set_message(lang("konfirmasi-delete-sukses"),"success");

            }

        }

        if(isset($_POST['type'])){

            $type               = isset($_POST['type']) ? $this->input->post("type") : '';
            $kategori           = isset($_POST['kategori']) ? $this->input->post("kategori") : '';
            $kategori_harga     = isset($_POST['kategori_harga']) ? $this->input->post("kategori_harga") : '';
            $produk             = isset($_POST['produk']) ? $this->input->post("produk") : '';

        }else{

            $type               = isset($_GET['type']) ? $this->input->get("type") : '';
            $kategori           = isset($_GET['kategori']) ? $this->input->get("kategori") : '';
            $kategori_harga     = isset($_GET['kategori_harga']) ? $this->input->get("kategori_harga") : '';
            $produk             = isset($_GET['produk']) ? $this->input->get("produk") : '';

        }

        $this->load->library('pagination'); //librari pagination

        $filter     = "?sh";
        $where      = "setting_harga.deleted = 0 ";

        

        if(strlen($type) > 0){

            $where  .= " and setting_harga.st_produk = $type";
            $filter .= "&type=".$type ;

        }

        if($kategori && strlen($kategori)>0){

            $where  .= " and `kategori`.`idkategori` = $kategori";
            $filter .= "&kategori=".$kategori;

        }

        if($kategori_harga && strlen($kategori_harga)>0){

            $where  .= " and `setting_harga`.`idkategori_harga` = $kategori_harga";
            $filter .= "&kategori_harga=".$kategori_harga;

        }

        if($produk && strlen($produk)){

            $where  .= " and `setting_harga`.`idproduk` = '$produk'";
            $filter .= "&produk=".$produk;

        }


        $total   = $this->setting_harga_model->select(`setting_harga`.`idsettingharga`)
                                                ->join("produk","setting_harga.idproduk = produk.idproduk","inner")
                                                ->join("kategori","produk.idkategori = kategori.idkategori","inner")
                                                ->join("gol_harga","setting_harga.idkategori_harga = gol_harga.id_gol","inner")
                                                ->where($where)
                                                ->count_all();

        $offset = $this->input->get('per_page'); // start record

        $limit = $this->config->item('list_limit'); //jml tiap halaman

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);


        $data   = $this->setting_harga_model->select("`setting_harga`.`idsettingharga` as id,
                                                IF(`setting_harga`.`st_produk` = 0,
                                                    'Produk',
                                                    'Finishing') AS st_produk,
                                                `kategori`.`nmkategori` AS kategori,
                                                `produk`.`nm_produk` AS produk,
                                                gol_harga.nm_gol AS kategori_harga")
                                        ->join("produk","setting_harga.idproduk = produk.idproduk","inner")
                                        ->join("kategori","produk.idkategori = kategori.idkategori","inner")
                                        ->join("gol_harga","setting_harga.idkategori_harga = gol_harga.id_gol","inner")
                                        ->where($where)
                                        ->order_by("setting_harga.st_produk","asc")
                                        ->order_by("kategori.nmkategori","asc")
                                        ->order_by("produk.nm_produk","asc")
                                        ->order_by("gol_harga.nm_gol","asc")
                                        ->limit($limit, $offset)
                                        ->find_all();


        $data_kategori  = $this->kategori_model->select("`idkategori` as id, `nmkategori` as nm")
                                                ->where("deleted=0")
                                                ->order_by("nmkategori","asc")
                                                ->find_all();

        $data_kategori_harga    = $this->kategori_harga_model->select("`id_gol` as id, `nm_gol` as nm")
                                                            ->where("deleted=0")
                                                            ->order_by("nm_gol","asc")
                                                            ->find_all();

        $asset  =   [
                        'plugins/select2/dist/css/select2.min.css',
                        'plugins/select2/dist/js/select2.min.js',
                        'setting_harga/assets/js/index_set_harga.js'
                    ];

        add_assets($asset);
        
        $this->template->set("numb", $offset+1);

        $this->template->set("cr_type", $type);
        $this->template->set("cr_kategori", $kategori);
        $this->template->set("cr_kategori_harga", $kategori_harga);
        $this->template->set("cr_produk", $produk);

        $this->template->set("data",$data);
        $this->template->set("kategori",$data_kategori);
        $this->template->set("kategori_harga",$data_kategori_harga);
        $this->template->set("toolbar_title", lang('setting_harga_title_manage'));
        $this->template->title(lang('setting_harga_title_manage'));
        $this->template->render('index'); 
    }

    public function delete_row(){

        if(!$this->input->is_ajax_request()){

            redirect('setting_harga');

        }

        $id     = $this->input->post('id');

        $sql    = "";
        $this->db->trans_start();

            $this->setting_harga_detail_model->delete($id);
            $sql    = $this->db->last_query();

        $this->db->trans_complete();

        $return     = 0; // 0 gagal, 1 sukses

        if($this->db->trans_status() == false){

            $return     = 0;
            $keterangan = "gagal delete data range harga, id = ".$id;
            $status     = 0;

        }else{

            $return     = 1;
            $keterangan = "sukses delete data range harga, id = ".$id;
            $status     = 1;

        }

        $nm_hak_akses   = $this->deletePermission; 
        $kode_universal = $id;
        $jumlah         = 0;
        
        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        echo $return;


    }

    public function delete(){

        if(isset($_POST['checked'])){

            $id         = $this->input->post("checked");
            $sql_all    = "";

            $this->db->trans_start();

            foreach ($id as $key => $isi) {
                
                $this->setting_harga_model->delete($isi);
                $sql_all    .= $this->db->last_query()."\n\n";

            }

            $this->db->trans_complete();

            if($this->db->trans_status() == false){

                $return     = false;
                $keterangan = "Gagal delete setting harga";
                $status     = 0;

            }else{

                $return     = true;
                $keterangan = "Sukses delete setting harga";
                $status     = 1;

            }

            $nm_hak_akses   = $this->deletePermission; 
            $kode_universal = "-";
            $jumlah         = 0;
            $sql            = $sql_all;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);


        }else{

            $this->template->set_message(lang("konfirmasi-error-pil-delete"),"error");
            return false;

        }

        return $return;

    }

    public function cancel(){

        $this->session->unset_userdata("frm_setting_harga");
        $this->template->set_message(lang('konfirmasi_cancel_sukses'), 'success');

        redirect("setting_harga");

    }

    private function simpan_clonning(){

        // ** aturan **
        // saat clonning, delete terlebih dahulu data BOM yang sama dengan gol. tujuan

        $this->form_validation->set_rules("gol_harga_sumber","lang:gol_harga_sumber","callback_cek_pil_dropdown");
        $this->form_validation->set_rules("gol_harga_tujuan","lang:gol_harga_tujuan","callback_cek_pil_dropdown");

        $this->form_validation->set_message("cek_pil_dropdown", "You should choose the {field} field");

        if($this->form_validation->run() === false){

            Template::set_message(validation_errors(),'error');
            return false;

        }


        $target_data        = isset($_POST['target_data']) ? $this->input->post('target_data') : '';
        $status_bom         = isset($_POST['status_bom']) ? $this->input->post('status_bom') : '';
        $st_pilih_bom       = isset($_POST['st_pilih_bom']) ? $this->input->post('st_pilih_bom') : '';
        $pil_kategori       = isset($_POST['pil_kategori']) ? $this->input->post('pil_kategori') : '';
        $pil_produk         = isset($_POST['pil_produk']) ? $this->input->post('pil_produk') : '';
        $gol_harga_sumber   = isset($_POST['gol_harga_sumber']) ? $this->input->post('gol_harga_sumber') : '';
        $gol_harga_tujuan   = isset($_POST['gol_harga_tujuan']) ? $this->input->post('gol_harga_tujuan') : '';
        
        // start ambil id barang dari harga sumber 

        // ---- susun where data sumber ----

        $where_asal = "setting_harga.deleted = 0 and setting_harga.idkategori_harga = {$gol_harga_sumber}";

        if($status_bom > 0){

            if($status_bom == 1){

                $where_asal .= " and `setting_harga`.`st_produk` = 0 ";

            }else{

                $where_asal .= " and `setting_harga`.`st_produk` = 1 ";

            }

        }

        switch ($target_data) {
            case 1: //Kategori Produk
                
                if(count($pil_kategori) > 0){

                    foreach ($pil_kategori as $key => $kat) {
                        
                        if($key == 0){

                            $where_asal .= " and ( ";

                        }else{

                            $where_asal .= " or ";

                        }

                        $where_asal     .= " `produk`.`idkategori` = $kat ";

                        if($key == (count($pil_kategori)-1) ){

                            $where_asal .= " ) ";

                        }

                    }

                }else{

                    $this->template->set_message(lang('konfirmasi-kategori-kosong'),'error');
                    return false;
                }

                break;
            case 2: //B.O.M

                if($st_pilih_bom = 1){

                    if(count($pil_produk) > 0){

                        foreach ($pil_produk as $key => $pro) {
                            
                            if($key == 0){

                                $where_asal .= " and ( ";

                            }else{

                                $where_asal .= " or ";

                            }

                            $where_asal     .= " `setting_harga`.`idproduk` = '{$pro}' ";

                            if($key == (count($pil_produk)-1) ){

                                $where_asal .= " ) ";

                            }

                        }

                    }else{

                        $this->template->set_message(lang('konfirmasi-produk-kosong'),'error');
                        return false;
                               
                    }

                }
                                
                break;
            case 3: //Golongan Harga

                break;    
            
        }

        $dt_setting_harga   = $this->setting_harga_model
                                    ->select('`setting_harga`.`idproduk`, setting_harga.idsettingharga')
                                    ->join('produk','`produk`.`idproduk` = `setting_harga`.`idproduk`','inner')
                                    ->where($where_asal)
                                    ->find_all();

        //end ambil id barang dari harga sumber  

        
        
        if(!is_array($dt_setting_harga) && !count($dt_setting_harga)){

            $this->template->set_message(lang('konfirmasi-kategorisumber-kosong'),'error');
            return false;

        }

        $user       = $this->auth->userdata();
        $id_user    = $user->id_user;

        $this->db->trans_start();

            $wh_clone   = "setting_harga.deleted = 0 and setting_harga.idkategori_harga = {$gol_harga_sumber}";

            foreach ($dt_setting_harga as $key => $dt) {
                
                //start delete produk di gol harga sumber

                $wh_delete  = "`setting_harga`.`idkategori_harga` = {$gol_harga_tujuan}
                                AND `setting_harga`.`idproduk` = '{$dt->idproduk}'";

                $this->setting_harga_model->delete_where($wh_delete);

                //end delete produk di gol harga sumber

                // clone headnya.
                $primarynew     = gen_primary("cln","setting_harga","idsettingharga")."-".$key;
                $waktu_simpan   = date('Y-m-d H:i:s');
                $sql_head       = "insert into setting_harga(`idsettingharga`, `st_produk`, `idproduk`, `idkategori_harga`, `st_hitung`, `st_range_harga`, `qty`, `p`, `l`, `id_konversi`, `qty_hitung`, `harga`, `harga_by`, `id_konversi_harga_by`, `qty_harga_by`, `created_on`, `created_by`, `deleted`)
                                    SELECT
                                        '{$primarynew}',
                                        `st_produk`,
                                        `idproduk`,
                                        {$gol_harga_tujuan},
                                        `st_hitung`,
                                        `st_range_harga`,
                                        `qty`,
                                        `p`,
                                        `l`,
                                        `id_konversi`,
                                        `qty_hitung`,
                                        `harga`,
                                        `harga_by`,
                                        `id_konversi_harga_by`,
                                        `qty_harga_by`,
                                        '{$waktu_simpan}',
                                        {$id_user},
                                        0
                                    FROM
                                        `setting_harga`
                                    WHERE
                                        `setting_harga`.idsettingharga = '{$dt->idsettingharga}'";

                $this->db->query($sql_head);

                $dt_setHarga_detail     = $this->setting_harga_detail_model
                                                ->select("`idsettingharga`,
                                                            `range_a`,
                                                            `range_b`,
                                                            `range_a_p`,
                                                            `range_a_l`,
                                                            `range_b_p`,
                                                            `range_b_l`,
                                                            `id_konversi`,
                                                            `range_a_qty`,
                                                            `range_b_qty`,
                                                            `harga`,
                                                            `harga_by`,
                                                            `id_konversi_harga_by`,
                                                            `qty_harga_by`")
                                                ->where("`idsettingharga` = '{$dt->idsettingharga}'")
                                                ->find_all();

                if(count($dt_setHarga_detail) && is_array($dt_setHarga_detail)){
                    
                    $arr_isi    = [];

                    foreach ($dt_setHarga_detail as $keyd => $det) {
                        
                        $primarynew_det = gen_primary('cln'.$keyd,'setting_harga_detail','idsettingharga_detail');

                        $arr_isi[]  =   [
                                            'idsettingharga_detail' => $primarynew_det, 
                                            'idsettingharga'        => $primarynew, 
                                            'range_a'               => $det->range_a, 
                                            'range_b'               => $det->range_b, 
                                            'range_a_p'             => $det->range_a_p, 
                                            'range_a_l'             => $det->range_a_l, 
                                            'range_b_p'             => $det->range_b_p, 
                                            'range_b_l'             => $det->range_b_l, 
                                            'id_konversi'           => $det->id_konversi, 
                                            'range_a_qty'           => $det->range_a_qty, 
                                            'range_b_qty'           => $det->range_b_qty, 
                                            'harga'                 => $det->harga, 
                                            'harga_by'              => $det->harga_by, 
                                            'id_konversi_harga_by'  => $det->id_konversi_harga_by, 
                                            'qty_harga_by'          => $det->qty_harga_by
                                        ];

                    }

                    $this->setting_harga_detail_model->insert_batch($arr_isi);
                
                }
                

            }

            
        $this->db->trans_complete();

        if($this->db->trans_status() == true){

            return true;

        }else{

            return false;

        }


    }

    public function clonning(){
        //clonning harga untuk menjadi harga baru di golongan lain

        if(isset($_POST['simpan']) && has_permission($this->addPermission)){

            $simpan     = $this->simpan_clonning();

            if($simpan){

                $this->template->set_message(lang('konfirmasi-sukses-clonning'),'success');
                redirect('setting_harga');

            }

        }


        // ** Start Data Kategori **

        $data_kategori  = $this->kategori_model
                                ->select("`idkategori` as id, `nmkategori` as nm")
                                ->where("deleted=0")
                                ->order_by("nmkategori","asc")
                                ->find_all();

        $arr_kategori = [];

        if(is_array($data_kategori) && count($data_kategori)){

            foreach ($data_kategori as $key => $kat) {
                
                $arr_kategori[$kat->id] = strtoupper($kat->nm);

            }

        }                                
        // ** End Data Kategori **

        // ** Start Data Golongan Harga **
        $data_kategori_harga    = $this->kategori_harga_model
                                        ->select("`id_gol` as id, `nm_gol` as nm")
                                        ->where("deleted=0")
                                        ->order_by("nm_gol","asc")
                                        ->find_all();


        // ** End Data Golongan Harga **

        // ** Data Produk **                                                    
        /*$data_produk    = $this->produk_model
                                ->select("`produk`.`idproduk`,
                                            `produk`.`nm_produk`,
                                            kategori.nmkategori")
                                ->join("kategori","produk.idkategori = kategori.idkategori")
                                ->where("produk.deleted = 0")
                                ->order_by("produk.nm_produk","asc")
                                ->find_all();
        */
        $arr_produk = [];

        /*if(is_array($data_produk) && count($data_produk)){

            foreach ($data_produk as $key => $dp) {
                
                $arr_produk[$dp->idproduk] = $dp->nm_produk." [".$dp->nmkategori."]";

            }

        }*/


        $asset  =   [
                        'plugins/select2/dist/css/select2.min.css',
                        'plugins/select2/dist/js/select2.min.js',
                        'setting_harga/assets/js/frm_clone.js',
                        'setting_harga/assets/css/setting_harga_clonning.css'
                    ];

        add_assets($asset);

        $this->template->set("data_kategori",$arr_kategori);
        $this->template->set("data_kategori_harga",$data_kategori_harga);
        $this->template->set("data_produk",$arr_produk);

        $this->template->title(lang('setting_harga_title_clonning'));
        $this->template->set("toolbar_title", lang('setting_harga_title_clonning'));
        $this->template->render('form_clonning_harga');

    }

    public function get_data_bom(){

        if(!$this->input->is_ajax_request()){

            redirect("setting_harga");

        }

        $st_bom     = $this->input->post('st');

        $where      = "produk.deleted = 0 and st_tmp = 0";

        if($st_bom == 1){

            $where  .= " and produk.st_produk = 0 ";

        }else if($st_bom == 2){

            $where  .= " and produk.st_produk = 1 ";            

        }


        // ** Data Produk **                                                    
        $data_produk    = $this->produk_model
                                ->select("`produk`.`idproduk` as id,
                                            CONCAT(`produk`.`nm_produk`,' [',kategori.nmkategori,']') as nm_tampil")
                                ->join("kategori","produk.idkategori = kategori.idkategori")
                                ->where($where)
                                ->order_by("produk.nm_produk","asc")
                                ->find_all();
        
        $data   = ['st' => 0, 'data' => null];

        if(is_array($data_produk) && count($data_produk)){

            $data   = ['st'=>1,'data'=>$data_produk];

        }

        echo json_encode($data);

    }


    public function create(){

        $this->auth->restrict($this->addPermission);

        if(isset($_POST['simpan']) && has_permission($this->addPermission)){

            if($this->save()){

                $this->template->set_message(lang("setting_harga_create_success"),"success");
                redirect("setting_harga");
            }

        }

        $data   = $this->session->userdata("frm_setting_harga");

        if(!$data){

            $this->set_session(1,0,"");
            $data   = $this->session->userdata("frm_setting_harga");

        }

        $data_kategori  = $this->kategori_model->select("`idkategori` as id, `nmkategori` as nm")
                                                ->where("deleted=0")
                                                ->order_by("nmkategori","asc")
                                                ->find_all();

        $data_kategori_harga    = $this->kategori_harga_model->select("`id_gol` as id, `nm_gol` as nm")
                                                            ->where("deleted=0")
                                                            ->order_by("nm_gol","asc")
                                                            ->find_all();

        $asset  =   [
                        'plugins/select2/dist/css/select2.min.css',
                        'plugins/select2/dist/js/select2.min.js',
                        'plugins/number/jquery.number.js',
                        'setting_harga/assets/js/frm_set_harga.js'
                    ];

        add_assets($asset);

        $this->template->set("dt",$data);
        $this->template->set("kategori",$data_kategori);
        $this->template->set("kategori_harga",$data_kategori_harga);
        $this->template->set("toolbar_title", lang('setting_harga_title_new'));
        $this->template->title(lang('setting_harga_title_new'));
        $this->template->render('form_setting_harga');    

    }

    function cek_pil_dropdown($val){

        return $val == "" ? false : true;

    }

    function save(){

        $this->form_validation->set_rules("kategori","lang:kategori_produk","callback_cek_pil_dropdown");
        $this->form_validation->set_rules("produk","lang:idproduk","callback_cek_pil_dropdown");
        $this->form_validation->set_rules("kategori_harga","lang:idkategori_harga","callback_cek_pil_dropdown");

        if($_POST['st_range'] == 1){

            $this->form_validation->set_rules("satuan","lang:satuan","callback_cek_pil_dropdown");
            $this->form_validation->set_rules("harga","lang:harga","required");            

            $this->form_validation->set_rules("harga_by","lang:harga_by","required");            
            $this->form_validation->set_rules("satuan_harga_by","lang:satuan","callback_cek_pil_dropdown");            
                
        }


        $this->form_validation->set_message("cek_pil_dropdown", "You should choose the {field} field");

        if($this->form_validation->run() === false){

            $this->set_session(0,0,"");
            Template::set_message(validation_errors(),'error');
            return false;

        }else{

            //=== cek range harga ================================================================
            if($_POST['st_range'] == 0){            

                if(!isset($_POST['dft_id_detail'])){

                    $this->set_session(0,0,"");
                    $this->template->set_message(lang("setting_harga_gagal_range_harga"),"error");
                    return false;                    

                }

            }

            //=== cek Produk + Kategori Harga =====================================================
            $produk         = $this->input->post("produk");    
            $kategori_harga = $this->input->post("kategori_harga");

            $where          = "deleted = 0 and idproduk = '$produk' and idkategori_harga = $kategori_harga";

            $total_ketemu   = $this->setting_harga_model->where($where)
                                                        ->count_all();

            if($total_ketemu>0){

                $this->set_session(0,0,"");
                $this->template->set_message(lang("konfirmasi-simpan-gagal-ketemu"),"error");
                return false;

            }


        }

        // ---- buat array insert header
        $tipe_produk        = $this->input->post("type");
        $kategori           = $this->input->post("kategori");
        $produk             = $this->input->post("produk");
        $kategori_harga     = $this->input->post("kategori_harga");
        $st_range           = $this->input->post("st_range");
        $satuan             = $this->input->post("satuan");
        $jml_fix            = $this->input->post("jml");                
        $harga              = str_replace(",","", $this->input->post("harga"));
        $harga_by           = $this->input->post("harga_by");
        $harga_by_satuan    = $this->input->post("satuan_harga_by");

        $total_jml_kecil    = hitung_ke_satuan_kecil($satuan,$jml_fix);
        $total_by_kecil     = hitung_ke_satuan_kecil($harga_by_satuan,$harga_by);

        $primary        = gen_primary($this->prefix,"setting_harga","idsettingharga");    

        if($st_range == 1){

            $arr_save_header    = array(
                                            'idsettingharga'        => $primary,
                                            'st_produk'             => $tipe_produk, 
                                            'idproduk'              => $produk, 
                                            'idkategori_harga'      => $kategori_harga, 
                                            'st_range_harga'        => $st_range, 
                                            'qty'                   => $jml_fix, 
                                            'id_konversi'           => $satuan, 
                                            'harga'                 => $harga,
                                            'qty_hitung'            => $total_jml_kecil['qty'], 
                                            'harga_by'              => $harga_by, 
                                            'id_konversi_harga_by'  => $harga_by_satuan, 
                                            'qty_harga_by'          => $total_by_kecil['qty']

                                    );

        }else{

            $arr_save_header    = array(
                                            'idsettingharga'    => $primary,
                                            'st_produk'         => $tipe_produk, 
                                            'idproduk'          => $produk, 
                                            'idkategori_harga'  => $kategori_harga, 
                                            'st_range_harga'    => $st_range
                                            
                                    );


            if(isset($_POST['dft_id_detail'])){

                $r1_qty             = $this->input->post("dft_r1_qty");
                $r2_qty             = $this->input->post("dft_r2_qty");
                $range1_real        = $this->input->post("dft_range1_real");
                $range2_real        = $this->input->post("dft_range2_real");
                $id_konversi        = $this->input->post("dft_idkonv");
                $satuan_tampil      = $this->input->post("dft_satuan");
                $harga              = $this->input->post("dft_harga");
                $harga_by_dft       = $this->input->post("dft_hargaby");
                $harga_by_idkonv    = $this->input->post("dft_hargaby_idkonv");
                $harga_by_terkecil  = $this->input->post("dft_hargaby_terkecil");

                $arr_detail     = array();

                foreach ( $_POST['dft_id_detail'] as $key => $isi) {
                    
                    $arr_detail[]   =   [
                                                
                                            'idsettingharga_detail'     => gen_primary("","setting_harga_detail","idsettingharga_detail"), 
                                            'idsettingharga'            => $primary, 
                                            'range_a_qty'               => str_replace(",","",$r1_qty[$key]), 
                                            'range_b_qty'               => str_replace(",","",$r2_qty[$key]),
                                            'range_a'					=> str_replace(",","",$range1_real[$key]), 	
                                            'range_b'					=> str_replace(",","",$range2_real[$key]),
                                            'id_konversi'               => $id_konversi[$key], 
                                            'harga'                     => str_replace(",","",$harga[$key]),
                                            'harga_by'                  => str_replace(",","",$harga_by_dft[$key]), 
                                            'id_konversi_harga_by'      => $harga_by_idkonv[$key], 
                                            'qty_harga_by'              => $harga_by_terkecil[$key]

                                        ];

                }

            }    

        }

        // ==== simpan ke database =====
        $return     = false;

        $this->db->trans_start();

            $this->setting_harga_model->insert($arr_save_header);
            $sql    = $this->db->last_query();

            if($st_range == 0){

                $this->setting_harga_detail_model->insert_batch($arr_detail);
                $sql    .= "\n\n".$this->db->last_query();

            }

        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            $return     = false;
            $keterangan = "Gagal tambah data setting harga, untuk produk".$produk ;
            $status     = 0;

        }else{

            $return     = true;
            $keterangan = "Sukses tambah data setting harga, untuk produk".$produk ;
            $status     = 1;

        }

        $nm_hak_akses   = $this->addPermission; 
        $kode_universal = $primary;
        $jumlah         = 0;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        if($return){

            $this->session->unset_userdata("frm_setting_harga");

        }

        return $return;
    }


    function get_satuan(){

        if(!$this->input->is_ajax_request()){
            redirect('setting_harga');
        }

        $tipe   = $this->input->post("tipe");

        if($tipe == 0){

            $satuan     = get_konversi($id_satuan_terkecil = 1, $id_konversi = "", $auto_select = FALSE);

        }else{

            $satuan     = get_konversi($id_satuan_terkecil = 6, $id_konversi = "", $auto_select = FALSE);

        }

        echo json_encode($satuan);


    }


    function get_produk(){

        if(!$this->input->is_ajax_request()){
            redirect('setting_harga');
        }

        $id         = $this->input->post("idkategori");
        $st_produk  = $this->input->post("st_produk");

        $data   = $this->produk_model->select("`idproduk` as id, ucase(`nm_produk`) as nm, `st_tipe` as st")
                                        ->where("deleted=0 and idkategori = {$id} and st_produk = {$st_produk} and st_tmp = 0")
                                        ->order_by("nm_produk", "asc")
                                        ->find_all();

        echo json_encode($data);

    }

    function set_session($st_kosong = 1,$st_lihat = 0, $id = ""){

        /* 
            $st_kosong  = 1, berarti array baru pertama dibuat 
            $st_lihat   = 1, jika menggunakan untuk melihat data, diikuti dengan $id 

        */

        $arr_sess = array();

        if($st_kosong == 1 && $st_lihat == 0 && strlen($id) <= 0){

            $arr_sess   =   array(

                                    'st_proses'         => 0, // 0 = input baru ; 1 = View / edit
                                    'id_setting_harga'  => "",
                                    'tipe_produk'       => 0,
                                    'kategori'          => "",
                                    'produk'            => "",
                                    'kategori_harga'    => "",
                                    'st_range'          => 0,
                                    'jml_fix'           => 1,
                                    'satuan'            => "",
                                    'harga'             => "",
                                    'harga_by'          => 1,
                                    'satuan_by'         => "",
                                    'daftar'            => array()

                            );

        }else if($st_kosong == 0 && $st_lihat == 0 && strlen($id) <= 0){ // isi session dari inputan

            $tipe_produk    = $this->input->post("type");
            $kategori       = $this->input->post("kategori");
            $produk         = $this->input->post("produk");
            $kategori_harga = $this->input->post("kategori_harga");
            $st_range       = $this->input->post("st_range");
            $jml_fix        = $this->input->post("jml");
            $satuan         = $this->input->post("satuan");
            $harga          = str_replace(",", "",$this->input->post("harga"));
            $harga_by       = $this->input->post("harga_by");
            $satuan_by      = $this->input->post("satuan_harga_by");

            $array_daftar   = array();

            if(isset($_POST['dft_id_detail'])){

                $r1_qty             = $this->input->post("dft_r1_qty");
                $r2_qty             = $this->input->post("dft_r2_qty");
                $range1_real        = $this->input->post("dft_range1_real");
                $range2_real        = $this->input->post("dft_range2_real");
                $id_konversi        = $this->input->post("dft_idkonv");
                $satuan_tampil      = $this->input->post("dft_satuan");
                $hargadft           = $this->input->post("dft_harga");
                $harga_by_dft       = $this->input->post("dft_hargaby");
                $harga_by_idkonv    = $this->input->post("dft_hargaby_idkonv");
                $harga_by_satuan    = $this->input->post("dft_hargaby_satuan");
                $harga_by_terkecil  = $this->input->post("dft_hargaby_terkecil");
                $id_detail          = $this->input->post("dft_id_detail");

                foreach ($id_detail as $key => $isi) {
                    
                    $array_daftar[] =   [
                                            "id_detail_setting"     => $isi,
                                            "dft_r1_qty"            => $r1_qty[$key],
                                            "dft_r2_qty"            => $r2_qty[$key],
                                            "dft_range1_real"       => $range1_real[$key],
                                            "dft_range2_real"       => $range2_real[$key],
                                            "dft_idkonv"            => $id_konversi[$key],
                                            "dft_satuan"            => $satuan_tampil[$key],
                                            "dft_harga"             => $hargadft[$key],
                                            "dft_hargaby"           => $harga_by_dft[$key],
                                            "dft_hargaby_idkonv"    => $harga_by_idkonv[$key],
                                            "dft_hargaby_satuan"    => $harga_by_satuan[$key],
                                            "dft_hargaby_terkecil"  => $harga_by_terkecil[$key]
                                        ];

                }

            }

            // susun arraynya

            $arr_sess   =   array(

                                    'st_proses'         => 0, // 0 = input baru ; 1 = View / edit
                                    'id_setting_harga'  => "",
                                    'tipe_produk'       => $tipe_produk,
                                    'kategori'          => $kategori,
                                    'produk'            => $produk,
                                    'kategori_harga'    => $kategori_harga,
                                    'st_range'          => $st_range,
                                    'jml_fix'           => $jml_fix,
                                    'satuan'            => $satuan,
                                    'harga'             => $harga,
                                    'harga_by'          => $harga_by,
                                    'satuan_by'         => $satuan_by,
                                    'daftar'            => $array_daftar

                            );



        }else if($st_kosong == 0 && $st_lihat == 1 && strlen($id) > 0){ // isi session dari tabel

            $data   = $this->setting_harga_model->select("`setting_harga`.`idsettingharga` as id,
                                                            `setting_harga`.`st_produk`,
                                                            `produk`.`idkategori`,
                                                            `setting_harga`.`idproduk`,
                                                            `setting_harga`.`idkategori_harga`,
                                                            `setting_harga`.`st_range_harga`,
                                                            `setting_harga`.`qty`,
                                                            `setting_harga`.`id_konversi`,
                                                            `setting_harga`.`harga`,
                                                            `setting_harga`.`harga_by`, 
                                                            `setting_harga`.`id_konversi_harga_by`
                                                            ")
                                                ->join("produk","setting_harga.idproduk = produk.idproduk","inner")
                                                ->where("`setting_harga`.`idsettingharga` = '$id'")
                                                ->find_all();

            $arr_detail = array();

            if($data[0]->st_range_harga == 0){

                $data_detail    = $this->setting_harga_detail_model
                                        ->select(" `setting_harga_detail`.`idsettingharga_detail` as id,
                                                    `setting_harga_detail`.`range_a`,
                                                    `setting_harga_detail`.`range_b`,
                                                    `setting_harga_detail`.`range_a_qty`, 
                                                    `setting_harga_detail`.`range_b_qty`,
                                                    `setting_harga_detail`.`id_konversi`,
                                                    CONCAT(`konversi_satuan`.satuan_besar,
                                                            ' (',
                                                            `konversi_satuan`.jml_kecil,
                                                            ' ',
                                                            `satuan_terkecil`.alias,
                                                            ')') AS tampil_satuan,
                                                    `setting_harga_detail`.`harga`,
                                                    `setting_harga_detail`.`harga_by`, 
                                                    `setting_harga_detail`.`id_konversi_harga_by`,`setting_harga_detail`.`qty_harga_by`,
                                                    konv.tampil_satuan_by
                                                    ")
                                        ->join("konversi_satuan","setting_harga_detail.id_konversi = konversi_satuan.id_konversi","inner")
                                        ->join("satuan_terkecil","konversi_satuan.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","inner")
                                        ->join("(SELECT 
                                                    `konversi_satuan`.`id_konversi` AS id,
                                                    `konversi_satuan`.satuan_besar AS tampil_satuan_by
                                                FROM
                                                    konversi_satuan
                                                INNER JOIN satuan_terkecil ON konversi_satuan.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil) AS konv","setting_harga_detail.id_konversi_harga_by = konv.id","inner")
                                        ->where("`setting_harga_detail`.`idsettingharga` = '$id' and `setting_harga_detail`.`deleted`=0")
                                        ->order_by("`setting_harga_detail`.`range_a`", "asc")
                                        ->find_all();

                //susun array detailnya isinya
                $arr_detail = [];

                if(is_array($data_detail) && count($data_detail)){

                    foreach ($data_detail as $key => $isi) {
                        
                        $arr_detail[]   =   [
                                                "id_detail_setting"     => $isi->id,
                                                "dft_r1_qty"            => $isi->range_a_qty,
                                                "dft_r2_qty"            => $isi->range_b_qty,
                                                "dft_range1_real"       => $isi->range_a,
                                                "dft_range2_real"       => $isi->range_b,
                                                "dft_idkonv"            => $isi->id_konversi,
                                                "dft_satuan"            => $isi->tampil_satuan,
                                                "dft_harga"             => $isi->harga,
                                                "dft_hargaby"           => $isi->harga_by,
                                                "dft_hargaby_idkonv"    => $isi->id_konversi_harga_by,
                                                "dft_hargaby_satuan"    => $isi->tampil_satuan_by,
                                                "dft_hargaby_terkecil"  => $isi->qty_harga_by
                                            ];

                    }
                }

            }

            //--- susun semua arraynya -----
            $arr_sess   =   array(

                                    'st_proses'         => 1, // 0 = input baru ; 1 = View / edit
                                    'id_setting_harga'  => $data[0]->id,
                                    'tipe_produk'       => $data[0]->st_produk,
                                    'kategori'          => $data[0]->idkategori,
                                    'produk'            => $data[0]->idproduk,
                                    'kategori_harga'    => $data[0]->idkategori_harga,
                                    'st_range'          => $data[0]->st_range_harga,
                                    'jml_fix'           => $data[0]->qty,
                                    'satuan'            => $data[0]->id_konversi,
                                    'harga'             => $data[0]->harga,
                                    'harga_by'          => $data[0]->harga_by,
                                    'satuan_by'         => $data[0]->id_konversi_harga_by,
                                    'daftar'            => $arr_detail

                            );


        }

        $this->session->set_userdata('frm_setting_harga',$arr_sess);

    }
  
    public function view($id){

        $this->auth->restrict($this->managePermission);

        if(isset($_POST['simpan'])){

            if($this->save_manage($id)){

                $this->template->set_message(lang("konfirmasi_edit_sukses"),"success");
                redirect('setting_harga');

            }

        }

        $data   = $this->session->userdata("frm_setting_harga");

        if(!$data){

            $this->set_session(0,1,$id);
            $data   = $this->session->userdata("frm_setting_harga");

        }

        $this->session->unset_userdata("frm_setting_harga");

        $data_kategori  = $this->kategori_model->select("`idkategori` as id, `nmkategori` as nm")
                                                ->where("deleted=0")
                                                ->order_by("nmkategori","asc")
                                                ->find_all();

        $data_kategori_harga    = $this->kategori_harga_model->select("`id_gol` as id, `nm_gol` as nm")
                                                            ->where("deleted=0")
                                                            ->order_by("nm_gol","asc")
                                                            ->find_all();

        $asset  =   [
                        'plugins/select2/dist/css/select2.min.css',
                        'plugins/select2/dist/js/select2.min.js',
                        'plugins/number/jquery.number.js',
                        'setting_harga/assets/js/frm_set_harga.js'
                    ];

        add_assets($asset);

        $this->template->set("dt",$data);
        $this->template->set("kategori",$data_kategori);
        $this->template->set("kategori_harga",$data_kategori_harga);
        $this->template->set("toolbar_title", lang('setting_harga_title_view'));
        $this->template->title(lang('setting_harga_title_view'));
        $this->template->render('form_setting_harga');    

    }

    public function save_manage($id){


        //---- cek validasi inputan edit -----

        if($_POST['st_range'] == 1){ //jika range Fix

            $this->form_validation->set_rules("satuan","lang:satuan","callback_cek_pil_dropdown"); 
            $this->form_validation->set_rules("harga","lang:harga","required");
            $this->form_validation->set_rules("satuan_harga_by","lang:satuan","callback_cek_pil_dropdown");            

            $this->form_validation->set_message("cek_pil_dropdown", "You should choose the {field} field");

            if($this->form_validation->run() === false){

                $this->set_session(0,0,"");
                Template::set_message(validation_errors(),'error');
                return false;                

            }

            //$jml_fix    = $this->input->post("jml");
            $satuan             = $this->input->post("satuan");
            $harga              = str_replace(",", "", $this->input->post("harga"));

            $jml_fix            = $this->input->post("jml");
            $harga_by           = $this->input->post("harga_by");
            $harga_by_satuan    = $this->input->post("satuan_harga_by");

            $total_jml_kecil    = hitung_ke_satuan_kecil($satuan,$jml_fix);
            $total_by_kecil     = hitung_ke_satuan_kecil($harga_by_satuan,$harga_by);

            
            // buat arraynya
            $arr_isi_edit   =   [
                                    'qty'                   => $jml_fix, 
                                    'id_konversi'           => $satuan, 
                                    'harga'                 => $harga,
                                    'qty_hitung'            => $total_jml_kecil['qty'], 
                                    'harga_by'              => $harga_by, 
                                    'id_konversi_harga_by'  => $harga_by_satuan, 
                                    'qty_harga_by'          => $total_by_kecil['qty']
                                ];

        }else{// menggunakan range harga

            if($_POST['st_range'] == 0){            

                if(!isset($_POST['dft_id_detail'])){

                    $this->set_session(0,0,"");
                    $this->template->set_message(lang("setting_harga_gagal_range_harga"),"error");
                    return false;                    

                }

            }

            
            $r1_qty             = $this->input->post("dft_r1_qty");
            $r2_qty             = $this->input->post("dft_r2_qty");
            $range1_real        = $this->input->post("dft_range1_real");
            $range2_real        = $this->input->post("dft_range2_real");
            $id_konversi        = $this->input->post("dft_idkonv");
            $satuan_tampil      = $this->input->post("dft_satuan");
            $harga              = $this->input->post("dft_harga");
            $harga_by_dft       = $this->input->post("dft_hargaby");
            $harga_by_idkonv    = $this->input->post("dft_hargaby_idkonv");
            $harga_by_terkecil  = $this->input->post("dft_hargaby_terkecil");
            $id_detail          = $this->input->post("dft_id_detail");

            $arr_isi_detail = array();

            foreach ($id_detail as $key => $isi) {
                
                if(strlen($isi)<=0){//diambil hanya yang tidak ada id detailnya

                    //buat arraynya
                    $arr_isi_detail[]   =   [
                        
                            'idsettingharga_detail' => gen_primary("","setting_harga_detail","idsettingharga_detail"), 
                            'idsettingharga'        => $id, 
                            'range_a_qty'           => str_replace(",","",$r1_qty[$key]), 
                            'range_b_qty'           => str_replace(",","",$r2_qty[$key]),
                            'range_a'               => str_replace(",","",$range1_real[$key]),  
                            'range_b'               => str_replace(",","",$range2_real[$key]),
                            'id_konversi'           => $id_konversi[$key], 
                            'harga'                 => str_replace(",","",$harga[$key]),
                            'harga_by'              => str_replace(",","",$harga_by_dft[$key]), 
                            'id_konversi_harga_by'  => $harga_by_idkonv[$key], 
                            'qty_harga_by'          => $harga_by_terkecil[$key]
    
                                            ];

                }

            }


        }

        $sql_all = "";

        $this->db->trans_start();

            if($_POST['st_range'] == 1){

                $this->setting_harga_model->update($id, $arr_isi_edit);
                $sql_all    = $this->db->last_query();

            }else{

                if(count($arr_isi_detail) > 0){

                    $this->setting_harga_detail_model->insert_batch($arr_isi_detail);
                    $sql_all    = $this->db->last_query();

                }

            }

        $this->db->trans_complete();
        
        if($this->db->trans_status()==false){

            $return     = false;
            $status     = 0;
            $keterangan = "Gagal, update data setting harga, id = ".$id;

        }else{

            $return     = true;
            $status     = 1;
            $keterangan = "Sukses, update data setting harga, id = ".$id;

        }

        $nm_hak_akses   = $this->managePermission; 
        $kode_universal = $id;
        $jumlah         = 0;
        $sql            = $sql_all;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        if($return){

            $this->session->unset_userdata("frm_setting_harga");

        }

        return $return;

    }

}

?>