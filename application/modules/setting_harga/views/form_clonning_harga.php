<?php
		$ENABLE_DELETE  = has_permission('Setting_harga.Delete');
?>

<?= form_open($this->uri->uri_string(),	[
											'name' 	=> 'frm_clonning_harga',
											'id'	=> 'frm_clonning_harga',
											'role'	=> 'form',
											'class' => 'form-horizontal'
										]) ?>

<div class="box box-primary">
	
	<div class="box-body">
		
		<div class="form-group">
			<label class="control-label col-sm-2" for="target_data"><?= lang('clonning_by') ?></label>
			<div class="col-sm-2">
				<select name="target_data" id="target_data" class="form-control">
					<option></option>
					<option value="1">Kategori Produk</option>
					<option value="2">B.O.M</option>
					<option value="3" selected="" >Golongan Harga</option>
				</select>
			</div>
			<div class="col-sm-4">
				<select name="status_bom" id="status_bom" class="form-control">
					<option value="0">Semua Data B.O.M</option>
					<option value="1">B.O.M Produk</option>
					<option value="2">B.O.M Finishing</option>
				</select>
			</div>
			<div class="col-sm-2">
				<select name="st_pilih_bom" id="st_pilih_bom" class="form-control">
					<option value="0">Pilih Semua</option>
					<option value="1">Pilih Sebagian</option>
				</select>
			</div>
		</div>

		<div class="form-group" id="area-kategori-produk">
			<label class="control-label col-sm-2" for="pil_kategori"><?= lang('kategori_produk') ?></label>
			<div class="col-sm-6">
				<?=
				form_dropdown('pil_kategori[]',$data_kategori,[],
                                    [   'id'        => "pil_kategori",
                                        'multiple'  => "multiple",
                                        'class'     => "form-control select2-multi"
                                    ]);
                ?>
			</div>
		</div>

		<div class="form-group" id="area-produk">
			<label class="control-label col-sm-2" for="pil_produk"><?= lang('produk_bom') ?></label>
			<div class="col-sm-8">
				<select class="form-control select2-multi" name = "pil_produk[]" id="pil_produk" multiple="multiple" ></select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-2" for="gol_harga_sumber"><?= lang('gol_harga_sumber') ?></label>
			<div class="col-sm-2">
				<select name="gol_harga_sumber" id="gol_harga_sumber" class="form-control">
					<option selected=""></option>
					<?php 
							if(is_array($data_kategori_harga) && count($data_kategori_harga)):

								foreach ($data_kategori_harga as $key => $gol_sumber):
											
					?>

					<option value="<?= $gol_sumber->id ?>">
						<?= $gol_sumber->nm ?>
					</option>

					<?php 
								endforeach;
							endif;
					?>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-2" for="gol_harga_tujuan"><?= lang('gol_harga_tujuan') ?></label>
			<div class="col-sm-2">
				<select name="gol_harga_tujuan" id="gol_harga_tujuan" class="form-control">
					<option selected=""></option>
					<?php 
							if(is_array($data_kategori_harga) && count($data_kategori_harga)):

								foreach ($data_kategori_harga as $key => $gol_tujuan):
											
					?>

					<option value="<?= $gol_tujuan->id ?>">
						<?= $gol_tujuan->nm ?>
					</option>

					<?php 
								endforeach;
							endif;
					?>
				</select>
			</div>
		</div>

		<div class="form-group">
    		<div class="col-sm-offset-2 col-sm-4">
    			<button type="submit" name="simpan" class="btn btn-primary">
    				<?= lang("setting_harga_btn_proses") ?>
    			</button>
    			<?php echo lang("or_range")." ".anchor('setting_harga', lang('setting_harga_btn_cancel_back')) ?>
    		</div>
    	</div>

	</div>

</div>
<div class="callout callout-danger" ><?= lang('warning_clonning') ?></div>

<?= form_close(); ?>