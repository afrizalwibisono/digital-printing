<?php
	$ENABLE_ADD		= has_permission('Setting_harga.Add');
	$ENABLE_MANAGE	= has_permission('Setting_harga.Manage');
	$ENABLE_DELETE	= has_permission('Setting_harga.Delete');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array("name"=>"form_index", "id"=>"form_index", "role"=>"form")) ?>
	<div class="box-header">
		<div class="form-group">
			
			<?php if ($ENABLE_ADD): ?>

				<a href="<?= site_url('setting_harga/create') ?>" class="btn btn-success" title="<?= lang('setting_harga_btn_new') ?>"><?= lang('setting_harga_btn_new') ?></a>
				<a href="<?= site_url('setting_harga/clonning') ?>" class="btn btn-primary" title="<?= lang('setting_harga_btn_clone') ?>"><?= lang('setting_harga_btn_clone') ?></a>

			<?php endif;?>
			
			<div class="pull-right form-inline">

				<div class="form-group">
					<select class="form-control" name="type" id="type" style="min-width: 120px">
	    				<option value="0" <?= set_select("type","0",isset($cr_type) && $cr_type == 0 ) ?> ><?= lang("isi_st_produk_produk") ?></option>
	    				<option value="1" <?= set_select("type","1",isset($cr_type) && $cr_type == 1 ) ?> ><?= lang("isi_st_produk_finishing") ?></option>
    				</select>
				</div>

				<div class="form-group">

					<select class="form-control" name="kategori" id="kategori" style="min-width: 200px">
	    				<option></option>
	    				<?php 
	    					if(isset($kategori) && is_array($kategori) && count($kategori)): 
	    						foreach ($kategori as $key => $isi):
	    				?>
	    					<option value="<?= $isi->id ?>" <?= set_select("kategori",$isi->id, isset($cr_kategori) && $cr_kategori == $isi->id) ?> ><?= strtoupper($isi->nm) ?></option>
	    				<?php 
	    						endforeach;
	    					endif;
	    				?>	
    				</select>	
					
				</div>

				<div class="form-group">

					<select class="form-control" name="kategori_harga" id="kategori_harga" style="min-width: 200px">
	    				<option></option>
	    				<?php 
	    					if(isset($kategori_harga) && is_array($kategori_harga) && count($kategori_harga)): 
	    						foreach ($kategori_harga as $key => $isi):
	    				?>
	    					<option value="<?= $isi->id ?>" <?= set_select("kategori_harga",$isi->id, isset($cr_kategori_harga) && $cr_kategori_harga == $isi->id ) ?> ><?= strtoupper($isi->nm) ?></option>
	    				<?php 
	    						endforeach;
	    					endif;
	    				?>	
    				</select>	
					
				</div>

				<div class="form-group">
					
					<input type="hidden" name="id_produk_terpilih" id="id_produk_terpilih" value="<?= set_value("produk", $cr_produk) ?>">
	    			<select class="form-control" name="produk" id="produk" style="min-width: 220px">
	    				<option></option>
	    			</select>	

				</div>

				<div class="form-group">
					<button class="btn btn-default"><i class="fa fa-search"></i> </button>
				</div>

				
			</div>

		</div>
	</div>
	
	<?php if (isset($data) && is_array($data) && count($data)) : ?>
		<div class="box-body table-responsive no-padding">
	            <table class="table table-hover table-striped">
	                <thead>
	                    <tr>
	                        <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
	                        <th width="50">#</th>
	                        <th><?= lang('judul-tabel-type') ?></th>
	                        <th><?= lang('judul-tabel-kategori') ?></th>
	                        <th><?= lang('judul-tabel-produk') ?></th>
	                        <th class="text-center"><?= lang('judul-tabel-kategori_harga') ?></th>
	                        <th></th>
	                    </tr>
	                </thead>
	                <tbody>
	                    <?php 
	                    		foreach ($data as $key => $isi) :
	                    			
	                    ?>

	                    			<tr>
	                    				<td>
	                    					<input type="checkbox" name="checked[]" value="<?= $isi->id ?>">
	                    				</td>	
	                    				<td><?= $numb ?></td>
	                    				<td><?= $isi->st_produk ?></td>
	                    				<td><?= $isi->kategori ?></td>
	                    				<td><?= ucwords($isi->produk) ?></td>
	                    				<td class="text-center" ><?= ucwords($isi->kategori_harga) ?></td>
	                    				 <?php if($ENABLE_MANAGE) : ?>
				                        <td style="padding-right:20px">
				                        	<a class="text-black" href="<?= site_url('setting_harga/view/' . $isi->id); ?>" data-toggle="tooltip" data-placement="left" title="Edit Data"><i class="fa fa-pencil"></i></a>
				                        </td>
				                        <?php endif; ?>
	                    			</tr>

	                    <?php
	                    			$numb++;
	                    		endforeach;
	                    ?>
	                </tbody>
		  </table>
		</div><!-- /.box-body -->
		<div class="box-footer clearfix">
			<?php if($ENABLE_DELETE) : ?>
			<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('btn-delete') ?>" onclick="return confirm('<?= lang('konfirmasi-delete'); ?>')">
			<?php endif;
			echo $this->pagination->create_links(); 
			?>
		</div>
	<?php else: ?>
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('konfirmasi-data-tidak-ada') ?></p>
    </div>
    <?php
	endif;
	echo form_close(); 
	?>	
</div><!-- /.box-->