<?php
        
        $ENABLE_DELETE  = has_permission('Setting_harga.Delete');
        $disable        = $dt['st_proses'] == 1 ? 'disabled' : '';
?>

<div class="box box-primary">
    <!--form start -->
    <?= form_open($this->uri->uri_string(), [
                                                "name"          => "frm_baru",
                                                "id"            => "frm_baru",
                                                "role"          => "form",
                                                "class"         => "form-horizontal",
                                                "data-status"   => $dt['st_proses']
                                            ]) ?>


    <div class="box-body">

    	<div class="form-group <?= form_error('type') ? 'has-error' : '' ?>">
    		<label class="control-label col-sm-2" for="type"><?= lang("st_produk") ?></label>
    		<div class="col-sm-4">
    			<select class="form-control" name="type" id="type" <?= $disable ?> >
    				<option value="0" <?= set_select("type","0",isset($dt['tipe_produk']) && $dt['tipe_produk'] == 0) ?> ><?= lang("isi_st_produk_produk") ?></option>
    				<option value="1" <?= set_select("type","1",isset($dt['tipe_produk']) && $dt['tipe_produk'] == 1) ?>><?= lang("isi_st_produk_finishing") ?></option>
    			</select>	
    		</div>
    		
    	</div>

    	<div class="form-group <?= form_error('kategori') ? 'has-error' : '' ?>">
    		<label class="control-label col-sm-2" for="kategori"><?= lang("kategori_produk") ?></label>
    		<div class="col-sm-4">
    			<select class="form-control" name="kategori" id="kategori" <?= $disable ?> >
    				<option></option>
    				<?php 
    					if(isset($kategori) && is_array($kategori) && count($kategori)): 
    						foreach ($kategori as $key => $isi):
    				?>
    					<option value="<?= $isi->id ?>" <?= set_select("kategori",$isi->id, isset($dt['kategori']) && $dt['kategori'] == $isi->id) ?> ><?= strtoupper($isi->nm) ?></option>
    				<?php 
    						endforeach;
    					endif;
    				?>	
    			</select>	
    		</div>
    		
    	</div>

		<div class="form-group <?= form_error('produk') ? 'has-error' : '' ?>">
    		<label class="control-label col-sm-2" for="produk"><?= lang("idproduk") ?></label>
    		<div class="col-sm-4">
    			<input type="hidden" name="id_produk_terpilih" id="id_produk_terpilih" value="<?= set_value("produk", $dt['produk']) ?>">
    			<select class="form-control" name="produk" id="produk" <?= $disable ?> >
    				<option></option>
    			</select>	
    		</div>
    		
    	</div>

    	<div class="form-group <?= form_error('kategori_harga') ? 'has-error' : '' ?>">
    		<label class="control-label col-sm-2" for="kategori_harga"><?= lang("idkategori_harga") ?></label>
    		<div class="col-sm-4">
    			<select class="form-control" name="kategori_harga" id="kategori_harga" <?= $disable ?> >
    				<option></option>
    				<?php 
    					if(isset($kategori_harga) && is_array($kategori_harga) && count($kategori_harga)): 
    						foreach ($kategori_harga as $key => $isi):
    				?>
    					<option value="<?= $isi->id ?>" <?= set_select("kategori_harga",$isi->id, isset($dt['kategori_harga']) && $dt['kategori_harga'] == $isi->id ) ?> ><?= strtoupper($isi->nm) ?></option>
    				<?php 
    						endforeach;
    					endif;
    				?>	
    			</select>	
    		</div>
    		
    	</div>

    	<div class="form-group">
    		<label class="control-label col-sm-2" for="st_range"><?= lang("st_range_harga") ?></label>
    		<div class="col-sm-4">
                <?php if($dt['st_proses'] == 1): ?>
                    <input type="hidden" name="st_range" value="<?= $dt['st_range'] ?>">
                <?php endif ?>    
    			<select class="form-control" name="st_range" id="st_range" <?= $disable ?> >
    				<option value="0" <?= set_select("st_range","0", isset($dt['st_range']) && $dt['st_range'] == 0)?> ><?= lang("isi_st_range_harga_range") ?></option>
    				<option value="1" <?= set_select("st_range","1", isset($dt['st_range']) && $dt['st_range'] == 1)?> ><?= lang("isi_st_range_harga_fix") ?></option>
    			</select>	
    		</div>
    	</div>

    	<div class="form-group" id="jml_no_range">
    		<label class="control-label col-sm-2" for="jml"><?= lang("jml_no_range") ?></label>
    		<div class="col-sm-4">
    			<span class="form-control"><?= $dt['jml_fix'] ?></span>
    			<input type="hidden" name="jml" id="jml" value="<?= $dt['jml_fix'] ?>">
    		</div>
    	</div>

    	<div id="jml_range">
			<div class="form-group">
	    		<label class="control-label col-sm-2" for="range1"><?= lang("range") ?></label>
	    		<div class="col-sm-4">
	    			<div class="input-group">
	    				<input type="text" name="range1" id="range1" class="form-control" />
	    				<span class="input-group-addon">s/d</span>
	    				<input type="text" name="range2" id="range2" class="form-control" />
	    			</div>
    			</div>
    		</div>	  		
    	</div>

    	<div class="form-group">
    		<label class="control-label col-sm-2" for="satuan"><?= lang("satuan") ?></label>
    		<div class="col-sm-4">
    			<input type="hidden" name="id_satuan_terpilih" id="id_satuan_terpilih" value="<?= set_value("satuan", $dt['satuan']) ?>">
    			<select class="form-control" name="satuan" id="satuan">
    			</select>	
    		</div>
    	</div>

    	<div class="form-group <?= form_error('harga') ? 'has-error' : '' ?> ">
    		<label class="control-label col-sm-2" for="harga"><?= lang("harga") ?></label>

    		<div class="col-sm-4">
    			<input type="text" name="harga" id="harga" class="form-control text-right" value="<?= set_value("harga", $dt['harga']) ?>">
    		</div>
    	</div>

        <div class="form-group <?= form_error('harga_by') ? 'has-error' : '' ?> ">
            <label class="control-label col-sm-2" for="harga_by"><?= lang("harga_by") ?></label>

            <div class="col-sm-4">
                
                <div class="col-sm-4" style="padding: 0px 0px 0px 0px">

                <input type="text" name="harga_by" id="harga_by" class="form-control text-right" value="<?= set_value("harga_by", $dt['harga_by']) ?>">
                
                </div>

                <div class="col-sm-8" style="padding: 0px 0px 0px 10px">
                    <input type="hidden" name="id_satuan_harga_by_terpilih" id="id_satuan_harga_by_terpilih" value="<?= set_value("satuan_harga_by", $dt['satuan_by']) ?>">

                    <select class="form-control" name="satuan_harga_by" id="satuan_harga_by">
                    </select>   
                </div>    

            </div>

            

        </div>        

    	<div id="jml_range2">
	    	<div class="form-group">
	    		<div class="col-sm-offset-2 col-sm-4">
	    			<button type="button" class="btn btn-success" id="btn_insert">
	    				<span class="fa fa-plus"></span> <?= lang("setting_harga_btn_add") ?>
	    			</button>
	    			<?= lang("or_range") ?> <a href="#btn_batal" id="btl_range"><?= lang("setting_harga_btn_cancel") ?></a>
	    		</div>
	    	</div>

	    	<div class="table-responsive" id="daftar_harga">
	    		<table class="table table-bordered">
	    			<thead>
	    				<tr class="success">
	    					<th class="text-center"><?= lang("range") ?></th>
	    					<th class="text-center"><?= lang("satuan") ?></th>
	    					<th class="text-center"><?= lang("harga") ?></th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    				<?php 
	    						if(isset($dt['daftar']) && is_array($dt['daftar']) && count($dt['daftar'])):
	    							foreach ($dt['daftar'] as $key => $isi):
	    								
	    				?>
	    						<tr>
                            		<td class="text-center">
                                        <input type="hidden" name="dft_id_detail[]" value ="<?= $isi['id_detail_setting'] ?>">
                            			<input type="hidden" name="dft_r1_qty[]" value ="<?= $isi['dft_r1_qty'] ?>">
                                        <input type="hidden" name="dft_r2_qty[]" value ="<?= $isi['dft_r2_qty'] ?>">
                                        <input type="hidden" name="dft_range1_real[]" value ="<?= $isi['dft_range1_real'] ?>">
                            			<input type="hidden" name="dft_range2_real[]" value ="<?= $isi['dft_range2_real'] ?>">
                            			<input type="hidden" name="dft_idkonv[]" value ="<?= $isi['dft_idkonv'] ?>">
                            			<input type="hidden" name="dft_satuan[]" value ="<?= $isi['dft_satuan'] ?>">
                            			<input type="hidden" name="dft_harga[]" value ="<?= $isi['dft_harga'] ?>">
                                        <input type="hidden" name="dft_hargaby[]" value = "<?= $isi['dft_hargaby'] ?>">
                                        <input type="hidden" name="dft_hargaby_idkonv[]" value ="<?= $isi['dft_hargaby_idkonv'] ?>">
                                        <input type="hidden" name="dft_hargaby_satuan[]" value ="<?= $isi['dft_hargaby_satuan'] ?>">
                                        <input type="hidden" name="dft_hargaby_terkecil[]" value ="<?= $isi['dft_hargaby_terkecil'] ?>">
                            			<?php 

                                            $tampil = number_format($isi['dft_r1_qty'])." s/d ".number_format($isi['dft_r2_qty']);

                                            echo $tampil;

                                        ?> 

                            			<?php if($ENABLE_DELETE) : ?>
                                            <div class="pull-right">
                                                <a href="#hapus" onclick="remove_item(this)"><i class="fa fa-remove text-red"></i> hapus</a>
                                            </div>
                                        <?php endif ?>
                            		</td>
									<td class="text-center"><?= $isi['dft_satuan'] ?></td>
									<td class="text-center"><?= number_format($isi['dft_harga'])." / ".$isi['dft_hargaby_satuan']  ?></td>
								</tr>
	    				<?php
	    							endforeach;
	    						endif;
	    				?>
	    			</tbody>
	    		</table>
	    	</div>
    	</div>

    	

    	<div class="form-group">
    		<div class="col-sm-offset-2 col-sm-4">
    			<button type="submit" name="simpan" class="btn btn-primary">
    				<?= lang("setting_harga_btn_save") ?>
    			</button>
    			<?php echo lang("or_range")." ".anchor('setting_harga/cancel', lang('setting_harga_btn_cancel_back')) ?>
    		</div>
    	</div>

    </div>
    <?= form_close() ?>
        
</div>
