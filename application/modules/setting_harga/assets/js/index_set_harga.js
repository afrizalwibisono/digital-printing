$(function(){

  get_produk();

	$("#kategori").select2({
        placeholder :"Kategori Produk",
        allowClear  : true
  	});

	$("#produk").select2({
	    placeholder :"Produk",
	    allowClear  : true
	});

	$("#kategori_harga").select2({
	    placeholder :"Kategori Harga",
	    allowClear  : true
	});

	$("#type").change(function(){

		get_produk();		

	});

	$("#kategori").change(function(){

		get_produk();

	});


});

function get_produk(){

	if(!$("#kategori").val()){

		$("#produk option").remove();		
    return false;

	}

	var id_terpilih 	= $("#id_produk_terpilih").val();

	var data  = {
		      			idkategori 	: $("#kategori").val(),
                st_produk 	: $("#type").val()
				      };

	$.ajax({
          
          url         : baseurl+"setting_harga/get_produk",
          type        : "post",
          data        : data,
          dataType    : "json",
          success     : function(msg){

          				        $("#produk option").remove();

                          if(msg.length > 0){

                            	var isi     = "<option></option>";
                              var select  = "";

                            	$.each(msg,function(i,n){

                            		select    = n['id'] == id_terpilih ? 'selected' : '';
                              	isi += "<option value ='"+ n['id'] +"' data-type = '"+ n['st'] +"'" + select + ">"+n['nm']+"</option>";

                            	});

                            	$("#produk").append(isi);

                          }

                        }


	});
}