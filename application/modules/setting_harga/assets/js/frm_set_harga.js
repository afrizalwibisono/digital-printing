$(function(){

	inputan_range();
	get_produk();
	get_satuan_pakai();

	$("#kategori").select2({
        placeholder :"-- Pilih Kategori Produk --",
        allowClear  : true
  	});

	$("#produk").select2({
	    placeholder :"-- Pilih Produk --",
	    allowClear  : true
	});

	$("#kategori_harga").select2({
	    placeholder :"-- Pilih Kategori Harga --",
	    allowClear  : true
	});

	$("#satuan, #satuan_harga_by").select2({
	    placeholder :"-- Pilih Satuan --",
	    allowClear  : true
	});

	$("#harga, #range1, #range2").number(true);
	$("#l, #p, #range1_l, #range1_p, #range2_l, #range2_p").number(true,2);

	$("#type").change(function(){

		$("#kategori").val('').trigger('change');

	});	
	

	$("#kategori").change(function(){

		get_produk();
		$("#satuan option").remove();
		$("#satuan_harga_by option").remove();
		//$("#daftar_harga tbody tr").remove();
		clear_table_range();

	});

	$("#produk").change(function(){

		get_satuan_pakai();
		inputan_range();
		//$("#daftar_harga tbody tr").remove();
		clear_table_range();

	});	

	$("#st_range").change(function(){

		inputan_range();
		//$("#daftar_harga tbody tr").remove();
		clear_table_range();

	});

	$("#btl_range").click(function(){

		bersihkan_inputan_range();		

	});

	$("#btn_insert").click(function(){

		insert_daftar();

	});

	$("#satuan").change(function(){

		pilih_satuan();		

	});
	

});

function clear_table_range(){

	var status_proses 	= $("#frm_baru").data('status');
	
	if(status_proses == 0){

		$("#daftar_harga tbody tr").remove();

	}

}

function pilih_satuan(){

	var status_proses 	= $("#frm_baru").data('status');

	if(status_proses == 0 ){

		var satuan_pilih 	= $("#satuan").val();

		$("#id_satuan_harga_by_terpilih").val(satuan_pilih);
		$("#satuan_harga_by").val(satuan_pilih).trigger("change");

	}

}

function remove_item(obj){

	var id 	= $(obj).closest("tr").find("input[name='dft_id_detail[]']").val();

	if(id.length > 0){

		alertify.confirm("Data range harga ini akan langsung dihapus dari system.\nApakah akan dilanjutkan?",
			function(){ //jika jawaban ok
						$.ajax({
									url 		: baseurl+"setting_harga/delete_row",
									type 		: "post",
									data 		: {id:id},
									success 	: function(msg){

														if(msg==1){
															
															$(obj).closest("tr").animate({backgroundColor:'red'}, 1000).fadeOut(1000,function() {
	    														$(obj).closest("tr").remove();
															});    

														}

													}
						});		
						
		  	},
		  	function(){ //jika jawaban cancel
		    	alertify.error('Hapus dibatalkan');
		});


	}else{

		$(obj).closest("tr").animate({backgroundColor:'red'}, 1000).fadeOut(1000,function() {
			$(obj).closest("tr").remove();
		});    

	}

}

function bersihkan_inputan_range(){

	$("#frm_baru input[name='range1']").val("");
	$("#frm_baru input[name='range2']").val("");
	$("#frm_baru input[name='harga']").val("");

	//$("#satuan").val('').trigger("change");

}

function insert_daftar(){

	var range1 						= parseFloat($("#range1").val());
	var range2 						= parseFloat($("#range2").val());

	var harga 						= $("#harga").val();
	var idsatuan 					= $("#satuan").val();
	var satuan 						= $("#satuan option:selected").text();
	var jml_satuan_kecil 			= parseFloat($("#satuan").find("option:selected").data("jmlkecil"));

	var harga_by 					= $("#harga_by").val();
	var id_satuan_harga_by 			= $("#satuan_harga_by").val();
	var jml_satuan_kecil_harga_by 	= parseFloat($("#satuan_harga_by").find("option:selected").data("jmlkecil"));
	var harga_by_terkecil 			= harga_by*jml_satuan_kecil_harga_by;

	var satuan_by 					= $("#satuan_harga_by").find("option:selected").text();
	satuan_by 						= satuan_by.split(" ");
	satuan_by 						= satuan_by[0];

	//var type 				= $("#produk").find("option:selected").data("type");

	var range1_real 		= 0;
	var range2_real 		= 0;

	var r1_tampil 			= 0;
	var r2_tampil 			= 0;

	range1_real 		= range1 * jml_satuan_kecil;
	range2_real 		= range2 * jml_satuan_kecil;

	r1_tampil 			= $.number(range1,2);
	r2_tampil 			= $.number(range2,2);


	//console.log("inputan : "+ range1_real +" ; "+range2_real );

	if(harga > 0 && idsatuan.length > 0 && range2_real > range1_real){

		var jml_inputan 	= $("#daftar_harga tr").length;
		var ketemu 			= false;

		if(jml_inputan > 1){

			for (var i = 1; i <= jml_inputan-1; i++) {
			
				var dft_range1 = parseFloat($("#daftar_harga tr:eq("+ (i) +")").find("input[name='dft_range1_real[]']").val());
				var dft_range2 = parseFloat($("#daftar_harga tr:eq("+ (i) +")").find("input[name='dft_range2_real[]']").val());

				console.log("daftar : "+ dft_range1+" ; "+dft_range2);

				if(
					
					(dft_range1 <= range2_real || dft_range1 <= range1_real)  
					&& 
					(dft_range2 > range1_real || dft_range2 >= range2_real)

				){

					ketemu = true;
					break;

				}

			}

		}

		
		if(ketemu == true){

			 alertify.error('Range ukuran telah terdaftar');				

		}else{

			var isi 	= 	"<tr>"+
								"<td class='text-center'>"+
									"<input type='hidden' name='dft_id_detail[]' 		value =''>"+
									"<input type='hidden' name='dft_r1_qty[]' 			value ='"+ (isNaN(range1) ? 0 : range1) +"'>"+
									"<input type='hidden' name='dft_r2_qty[]' 			value ='"+ (isNaN(range2) ? 0 : range2) +"'>"+
									"<input type='hidden' name='dft_range1_real[]' 		value ='"+ range1_real +"'>"+
									"<input type='hidden' name='dft_range2_real[]' 		value ='"+ range2_real +"'>"+
									"<input type='hidden' name='dft_idkonv[]' 			value ='"+ idsatuan +"'>"+
									"<input type='hidden' name='dft_satuan[]' 			value ='"+ satuan +"'>"+
									"<input type='hidden' name='dft_harga[]' 			value ='"+ harga +"'>"+
									"<input type='hidden' name='dft_hargaby[]' 			value ='"+ harga_by +"'>"+
									"<input type='hidden' name='dft_hargaby_idkonv[]' 	value ='"+ id_satuan_harga_by +"'>"+
									"<input type='hidden' name='dft_hargaby_satuan[]' 	value ='"+ satuan_by +"'>"+
									"<input type='hidden' name='dft_hargaby_terkecil[]'	value ='"+ harga_by_terkecil +"'>"+

									r1_tampil +" s/d " + r2_tampil + 
									"<div class='pull-right'><a href='#hapus' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></div>"+
								"</td>" +
								"<td class='text-center'>"+
									satuan +
								"</td>" +
								"<td class='text-center'>"+
									$.number(harga) + " / " + harga_by + " " + satuan_by +
								"</td>" +
							"</tr>";

			$("#daftar_harga tbody").append(isi);
			bersihkan_inputan_range();

		}

	}else{

		//pesan error	
		alertify.error('Cek kembali inputan range ukuran dan harga');

	}


	
}


function inputan_range(){

	var st_range 	= $("#st_range").val();
	var produk 		= $("#produk").val();

	if(produk.length > 0){

		var tipe 	= $("#produk").find("option:selected").data("type");
		if(st_range == 1){

			$("#jml_range").hide(400);
			$("#jml_range2").hide(400);
			$("#jml_no_range").show(400);
			$("#harga_by").prop("readonly","true");
			
		}else{

			$("#jml_no_range").hide(400);		
			$("#jml_range").show(400);			
			$("#jml_range2").show(400);
			$("#harga_by").removeAttr("readonly");

		}

	}else{

		$("#jml_range").hide(400);
		$("#jml_range2").hide(400);
		$("#jml_no_range").hide(400);
		
		get_satuan_pakai();

	}

}

function get_satuan_pakai(){

	//alert($("#produk").val());

	if(!$("#produk").val()){

		$("#satuan option").remove();		

	}else{

		var id_terpilih 			= $("#id_satuan_terpilih").val();
		var id_terpilih_harga_by 	= $("#id_satuan_harga_by_terpilih").val(); 

		var type 	= $("#produk").find("option:selected").data("type");

		var data 	= {tipe : type};

		$.ajax({
					url 		: baseurl+"setting_harga/get_satuan",
					type 		: "post",
					data 		: data,
					dataType 	: "json",
					success 	: function(msg){

										$("#satuan option").remove();
										$("#satuan_harga_by option").remove();

										if(msg.length>0){

	                              			var isi   		= "<option></option>";
	                              			var selected 	= "";

	                              			var isi_by 		= "<option></option>";
	                              			var selected_by	= "";	

	                              			$.each(msg,function(i,n){

	                              				selected 	= n['id_konversi'] == id_terpilih ? 'selected' : '';
	                              				selected_by	= n['id_konversi'] == id_terpilih_harga_by ? 'selected' : '';

	                              				var tmpl 	= n['tampil2'];
	                              				tmpl 		= tmpl.replace(/\b<sup>2<\u002Fsup>/g,"&sup2");
	                              				
	                              				isi 	+= "<option value='"+ n['id_konversi'] +"' data-jmlkecil='" + n['jml_kecil'] + "'" + selected + ">"+tmpl+"</option>";
	                              				isi_by 	+= "<option value='"+ n['id_konversi'] +"' data-jmlkecil='" + n['jml_kecil'] + "'" + selected_by + ">"+tmpl+"</option>";

	                              			});

	                              			$("#satuan").append(isi);
	                              			$("#satuan_harga_by").append(isi_by);

										}


									}

		});

	}


}

function get_produk(){

	if(!$("#kategori").val()){

		$("#produk option").remove();		

	}

	var id_terpilih 	= $("#id_produk_terpilih").val();
  	var data  = {
  					idkategori 	: $("#kategori").val(),
  					st_produk 	: $("#type").val()
  				};

  	$.ajax({
            
            url         : baseurl+"setting_harga/get_produk",
            type        : "post",
            data        : data,
            dataType    : "json",
            success     : function(msg){

            				$("#produk option").remove();

                            if(msg.length > 0){

                              	var isi   = "<option></option>";

                              	$.each(msg,function(i,n){

                              		
                                	isi += "<option value ='"+ n['id'] +
                                			"' data-type = '"+ n['st'] +
                                			"' data-lebar = '" + n['lebar'] + "'>"+n['nm']+"</option>";

                              	});

                              	$("#produk").append(isi);

                              	if(id_terpilih.length>0){

							  		$("#produk").val(id_terpilih).trigger("change");

							  	}

                            }

                          }


  	});


  	

}