$(function(){

	$("#target_data").select2({
		placeholder :"-- Sumber Data --",
        allowClear  : true
  	});

  	$("#pil_kategori").select2({
        allowClear  : true
  	});

  	$("#pil_produk").select2({
        allowClear  : true
  	});

  	$("#gol_harga_sumber").select2({
  		placeholder :"Gol. Harga Sumber",
        allowClear  : true
  	});

  	$("#gol_harga_tujuan").select2({
  		placeholder :"Gol. Harga Tujuan",
        allowClear  : true
  	});

  	$("#status_bom").select2();

  	tampil_inputan();

	$("#target_data").change(function(){

		tampil_inputan();

	});  	

	$("#st_pilih_bom").change(function(){

		tampil_inputan();

	});	

	$("#status_bom").change(function(){

		isi_select_bom();

	});

});

function isi_select_bom(){

	var isi_sumber_data 	= parseInt($("#target_data").val()); 
	var st_muncul 			= parseInt($("#st_pilih_bom").val()); 
	var st 					= $("#status_bom").val();

	if(isi_sumber_data == 2 && st_muncul == 1){ 

		var data 	= {st : st};

		$.ajax({
					url 		: baseurl + "setting_harga/get_data_bom",
					type 		: "post",
					dataType 	: "json",
					data 		: data,
					success 	: function(msg){

									var st 	= parseInt(msg['st']);

									if(st == 0){

										alertify.error("Data Produk Tidak Ditemukan");

									}else{

										$("#pil_produk option").remove();

										var isi = "";

										$.each(msg['data'],function(i,n){

											isi 	+= "<option value='" + n['id'] + "'>" + n['nm_tampil'] + "</option>";

										});

										$("#pil_produk").append(isi);

									}	

								}

		})

	}

}

function tampil_inputan(){

	var isi_sumber_data 	= parseInt($("#target_data").val());

	switch(isi_sumber_data){
		case 1://kategori

			$("#area-kategori-produk").show(400);
			$("#area-produk").hide(400);
			$("#st_pilih_bom").hide(400);

			break;
		case 2: //bom

			var isi_st 	= parseInt($("#st_pilih_bom").val());
			if(isi_st == 1){

				$("#area-produk").show(400);
				isi_select_bom();
					
			}else{

				$("#area-produk").hide(400);
				$("#pil_produk option").remove();

			}

			$("#area-kategori-produk").hide(400);
			$("#st_pilih_bom").show(400);

			break;
		case 3: //golongan harga

			$("#area-kategori-produk").hide(400);
			$("#area-produk").hide(400);
			$("#st_pilih_bom").hide(400);

			break;
			
	}

}