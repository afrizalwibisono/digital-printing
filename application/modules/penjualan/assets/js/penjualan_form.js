$(function(){
	$("#ck_all").on("click", function(){
		if($(this).prop("checked")){
			$("input[name='item_code[]']").prop("checked", true);
		}else{
			$("input[name='item_code[]']").prop("checked", false);
		}
	});

	$("#idkonsumen").select2({
        placeholder :"-- Konsumen --",
        allowClear  : true
  	});
  	//Tanggal Transaksi
	$('#tgl_tran').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    todayHighlight: true
	}).on('changeDate', function(e) {
		var minDate = new Date(e.date.valueOf()),
			maxDate = new Date(e.date.valueOf()+(1000 * 60 * 60 * 24 * 3));

        $('#tgl_tempo').datepicker('setStartDate', minDate);
        $('#tgl_tempo').datepicker('setEndDate', maxDate);

        $('#tgl_tempo').datepicker('setDate', maxDate);
    });
	// Tanggal Tempo
	$('#tgl_tempo').datepicker({
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    todayHighlight: true,
	    startDate: $('#tgl_tran').datepicker('getDate'),
	    endDate: new Date($('#tgl_tran').datepicker('getDate').valueOf()+(1000 * 60 * 60 * 24 * 3))
	});

	// Cari Barang
	$("#cari_btn").on("click", function(){
		cari_barang();
	});

	$("#cr_barang").on("keydown", function(e){
		if(e.keyCode == 13){
			cari_barang();
			e.preventDefault();
		}
	});
	//Add Items
	$("#pilih_btn").on("click", function(){
		add_items();
	});
	// Validate konsumen
	$("#barcode_input").on('click', function(){
		var idkonsumen = $("#idkonsumen").val();
		if(!idkonsumen){
			alertify.error('Silahkan pilih konsumen dulu');
			return false;
		}
	});
	//Scan Barcode
	$("input[name='barcode']").on("keydown", function(e){
		if(e.keyCode == 13){

			var idkonsumen = $("#idkonsumen").val();
			if(!idkonsumen){
				alertify.error('Silahkan pilih konsumen dulu');
				return false;
			}

			scan_barcode();
			e.preventDefault();
		}
	});
	// Update Harga
	$("body").on("change", "input[name='qty[]'], select[name='konversi[]']", update_harga);

	$("#metode_bayar").change(function(){
		cek_lampiran();
	});

	cek_lampiran();

	$("#file_att").change(function(){
		readURL(this);
		$("#nm_file").text(this.value.split('\\').pop());
		$("#tampil_lampiran").show(400);
	});

	$("#dp, #bayar").number(true, 0);

	$("#dp").on("keyup", function(){
		var gtotal = Number($("#gtotal").data('gtotal')),
			dp = Number($(this).val().replace(/,/g,"")),
			kurang_bayar = 0;

		if(dp > gtotal && gtotal > 0){
			alertify.error('Nilai DP tidak boleh melebihi Grand Total');
		}

		kurang_bayar = gtotal - dp;
		if(kurang_bayar > 0 && dp > 0){
			$("#kurang-bayar").text($.number(kurang_bayar));	

			$('#tgl_tempo').datepicker('setDate', new Date($('#tgl_tran').datepicker('getDate').valueOf()+(1000 * 60 * 60 * 24 * 3)));
		}else{
			$("#kurang-bayar").text('');	
			$('#tgl_tempo').val('');
		}
	});

	$("#bayar").on('keyup', hitung_kembalian);
	//Update Harga
	if($("#tjual tbody tr").length > 0){
		update_harga();	
	}
});

function cek_lampiran(){
	if($("#metode_bayar").val() == 3){
		$("#lampiran_transfer").show(400);
	}else{
		$("#lampiran_transfer").hide(400);
		$("#nm_file").text("");
		$("#thumb").prop("src","");
		$("#tampil_lampiran").hide(400);
		$("#file_att").val("");
	}
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var tempImage1 = new Image();
        reader.onload = function (e) {
            $('#thumb').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

//cari barang
function cari_barang(){
	var cari = $("#cr_barang").val();
	var id_gudang_asal 	= $("#id_gudang_asal").val();

	if(!id_gudang_asal && $("#id_gudang_asal").length > 0){
        alertify.error('Silahkan pilih gudang asal dulu.');
        return false;
	}

	$.ajax({
		url : siteurl+"penjualan/cari_barang",
		type : "post",
		dataType : "json",
		data : {cr_barang : cari, id_gudang_asal: id_gudang_asal},
		success : function(msg){
			$("table#tbl_barang tbody tr").remove();
			if(!msg){
				var $tr = $("<tr>"
							+"<td colspan='5' class='text-center'>Data ditemukan</td>"
							+"</tr>");

				$("table#tbl_barang tbody").append($tr);
			}else{

				var opt_select = "";
				$.each(msg, function(i,n){
					$.each(n['konversi'], function(l,m){
						var selected = "";
						if(m['selected'] == 1)
						{
							selected = "selected";
						}

						opt_select += '<option data-qty="'+m['jml_kecil']+'" value="'+m['id_konversi']+'" '+selected+'>'+m['tampil2']+'</option>';
					});

					var $tr = $("<tr data-konversi='"+opt_select+"'>"
								+"<td><input type='checkbox' name='item_code[]' value='"+n['idbarang_bb']+"'></td>"
								+"<td><input type='hidden' name='item_barcode[]' value='"+n['barcode']+"' />"+(i+1)+"</td>"
								+"<td><input type='hidden' name='item_id_satuan[]' value='"+n['id_satuan_terkecil']+"' />"+n['barcode']+"</td>"
								+"<td><input type='hidden' name='item_nm_satuan[]' value='"+n['satuan']+"' /><input type='hidden' name='item_nm_barang[]' value='"+n['nm_barang']+"' />"+n['nm_barang']+"</td>"
								+"<td>"+($.number(n['stok']))+" "+n['satuan']+"</td>"
								+"</tr>");

					$("table#tbl_barang tbody").append($tr);

					opt_select = "";
				});
				
			}
			
		}
	});
}

//Add item to list
function add_items()
{
	var $obj_ids   = $("#tbl_barang input[name='item_code[]']:checked");

	var ids_barang = $("#tbl_barang input[name='item_code[]']:checked").map(function(){
		return $(this).val();
	}).get();

	var ids_barcodes = [];
	var nms_barang = [];
	var ids_satuan = [];
	var nms_satuan = [];
	var options 	= [];

	$.each($obj_ids, function(){
		ids_barcodes.push($(this).closest("tr").find("input[name='item_barcode[]']").val());
		nms_barang.push($(this).closest("tr").find("input[name='item_nm_barang[]']").val());
		nms_satuan.push($(this).closest("tr").find("input[name='item_nm_satuan[]']").val());
		options.push($(this).closest("tr").data("konversi"));
	});

	if(ids_barang.length == 0)
	{
        alertify.error('Silahkan pilih barang yang akan ditambahkan dulu');
        return false;
	}

	if($("#tjual-placeholder").length){
		$("#tjual-placeholder").remove();
	}

	var pj_ids = ids_barang.length;

	for (var i = 0; i < pj_ids; i++) {
		
 		var $row = $("<tr>"
		  					+"<td></td>"
		  					+"<td>"
		  						+ids_barcodes[i]
		  						+"<input type='hidden' name='id_barang[]' value='"+ids_barang[i]+"' />"
		  					+"</td>"
		  					+"<td>"+nms_barang[i]+" - <a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>"
		  					+"<td><input type='number' min='1' name='qty[]' class='form-control' value='1' /></td>"
		  					+"<td><select class='form-control' name='konversi[]' style='width: 200px;'>"+options[i]+"</select></td>"
		  					+"<td align='right'><span class='harga'></span></td>"
		  					+"<td align='right'><span class='diskon'></span>%</td>"
		  					+"<td align='right'><span class='sub_total'></span></td>"
		  				+"</tr>");

		var pj = $("#tjual tbody tr").length;
		var cek = false;

		if(pj > 0){
			for (var j = 0; j <= pj; j++) {
				var kode = $("#tjual tbody tr:eq("+(j)+")").find("input[name='id_barang[]']").val();

				if(kode != "" && kode == ids_barang[i]){
					var qty = parseFloat($("#tjual tbody tr:eq("+(j)+")").find("input[name='qty[]']").val());

					$("#tjual tbody tr:eq("+(j)+")").find("input[name='qty[]']").val(qty+1);
					
					cek = true;
					break;
				}

			}
			
		}

		if(cek == false){
			$("#tjual tbody").append($row);
		}
	}
	// Update Harga
	update_harga();

	$("#modal-barang").modal("hide");
	$("#tbl_barang input[type='checkbox']").prop('checked', false);	
	$("select[name='konversi[]']").select2();
	//Buat Nomor
	buat_no();

	alertify.success('Item terpilih telah ditambahkan');
}

//Scan Barcoce
function scan_barcode(){
	var barcode 		= $("#barcode").val().trim();

	if(!barcode){
		return false;
	}

	$.ajax({
		url : siteurl+"penjualan/scan_barcode",
		type : "post",
		dataType : "json",
		data : {barcode : barcode},
		success : function(msg){
			//clear barcode
			$("#barcode").val("");

			if(msg['type'] == 'success'){ //Sukses
				$("#barcode_input").removeClass("has-error").addClass("has-success");
				$("#barcode_msg").removeClass("has-red").addClass("text-green");
				$("#barcode_msg").text("Barcode : "+msg['barcode']+" ditemukan.");

				var addToList = function(){
					if($("#tjual-placeholder").length){
						$("#tjual-placeholder").remove();
					}

					var pj = $("#tjual tbody tr").length;
					var cek = false;
					//Cek jika item sudah ada pada daftar
					if(pj > 0){
						for (var j = 0; j <= pj; j++) {
							var kode = $("#tjual tbody tr:eq("+(j)+")").find("input[name='id_barang[]']").val();

							if(kode != "" && kode == msg['data'][0]['idbarang_bb']){
								var qty = parseFloat($("#tjual tbody tr:eq("+(j)+")").find("input[name='qty[]']").val());

								$("#tjual tbody tr:eq("+(j)+")").find("input[name='qty[]']").val(qty+1); //Trigger Change
								$("input[name='id_barang[]'][value='"+msg['data'][0]['idbarang_bb']+"']").closest("tr").find("input[name='qty[]']").trigger("change");
								
								cek = true;
								break;
							}

						}
						
					}

					if(cek == false){
						var opt_select = "";

						$.each(msg['data'][0]['konversi'], function(i,n){
							var selected = "";
							if(n['selected'] == 1)
							{
								selected = "selected";
							}

							opt_select += '<option value="'+n['id_konversi']+'" '+selected+'>'+n['tampil2']+'</option>';
						});

						var $row = $("<tr>"
				  					+"<td></td>"
				  					+"<td>"
				  						+msg['data'][0]['barcode']
				  						+"<input type='hidden' name='id_barang[]' value='"+msg['data'][0]['id_barang']+"' />"
				  					+"</td>"
				  					+"<td>"+msg['data'][0]['nm_barang']+" - <a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>"
				  					+"<td><input type='number' min='1' name='qty[]' class='form-control' value='1' /></td>"
				  					+"<td><select class='form-control' name='konversi[]' style='width: 200px;'>"+opt_select+"</select></td>"
			  						+"<td align='right'><span class='harga'></span></td>"
				  					+"<td align='right'><span class='diskon'></span></td>"
				  					+"<td align='right'><span class='sub_total'></span></td>"
				  				+"</tr>");

						$("#tjual tbody").append($row);
					}

					$("#modal-barang").modal("hide");
					$("#tbl_barang input[type='checkbox']").prop('checked', false);
					$("select[name='konversi[]']").select2();
					//Buat Nomor
					buat_no();
				}

				addToList();

				update_harga();

			}else{ //gagal
				$("#barcode_input").removeClass("has-success").addClass("has-error");
				$("#barcode_msg").removeClass("text-green").addClass("text-red");
				$("#barcode_msg").text("Barcode : "+msg['barcode']+" tidak ditemukan.");
			}
		}
	});
}

//buat nomor
function buat_no(){
	$.each($("#tjual tbody tr"), function(i){
		$(this).find("td:first").text(i+1);
	});
}

//Remove Item
function remove_item(obj){
	$(obj).closest("tr").fadeTo("slow",0.2, function(){
		$(this).remove();

		hitung_gtotal();
	});
}

function hitung_gtotal(deposit){
	if(!deposit){
		deposit = 0;
	}

	var gtotal = 0, subtotal = 0;
	$("#tjual tbody tr").each(function(i, n){
		subtotal = Number($(n).find('td:last span.sub_total').data('sub_total'));

		gtotal += subtotal;
	});

	if(deposit > 0){
		$("#deposit-msg").text('Konsumen memiliki deposit senilai Rp. '+$.number(deposit));
		$(".tr-deposit").show();
		var tagihan = bulatkan_keatas(gtotal, 100);
		if(deposit >= tagihan){
			$("#deposit-terpakai").text($.number(tagihan));
			$(".tr-metode-bayar").hide();
			$(".tr-dp").hide();
			$(".tr-bayar").hide();
			$(".tr-tgl-tempo").hide();
			$("#kurang-bayar").text(0);
			$("#kembalian").text(0);
		}else{
			$("#deposit-terpakai").text($.number(deposit));
			$(".tr-metode-bayar").show();
			$(".tr-dp").hide();
			$(".tr-bayar").show();
			$(".tr-tgl-tempo").show();
			$("#kurang-bayar").text($.number(tagihan-deposit));
		}
	}

	$("#gtotal").data('gtotal', bulatkan_keatas(gtotal, 100));
	$("#gtotal").text($.number(bulatkan_keatas(gtotal, 100)));
}

function update_harga(){
	var idkonsumen = $("#idkonsumen").val();
	var id_barang = $("#tjual tbody input[name='id_barang[]']").map(function(){
		return $(this).val();
	}).get();

	var qty = $("#tjual tbody input[name='qty[]']").map(function(){
		return $(this).val();
	}).get();

	var id_konversi = $("#tjual tbody select[name='konversi[]']").map(function(){
		return $(this).val();
	}).get();

	// Get Harga
	$.ajax({
		url: siteurl+'penjualan/get_harga',
		type: 'post',
		dataType: 'json',
		data: {idkonsumen: idkonsumen, id_barang: id_barang, qty: qty, id_konversi: id_konversi},
		success: function(res){
			var el = null, subtotal = 0, qty = 0, diskon = 0, qty_konversi = 0;
			$.each(res.data, function(i, n){
				el = $("#tjual tbody input[name='id_barang[]'][value='"+i+"']").closest('tr');
				el.find('span.harga').text($.number(n.harga));
				el.find('span.diskon').text(n.diskon);
				qty = Number(el.find("input[name='qty[]']").val());
				qty_konversi = Number(el.find("select[name='konversi[]'] option:selected").data('qty'));

				diskon = Number(n.harga)*Number(n.diskon)/100;

				subtotal = (Number(n.harga) - diskon)*qty*qty_konversi;
				el.find('span.sub_total').data('sub_total', subtotal);
				el.find('span.sub_total').text($.number(subtotal));
			});

			hitung_gtotal(res.deposit);
		}
	});
}

function hitung_kembalian(){
	var gtotal = Number($("#gtotal").data('gtotal')),
		bayar = Number($("#bayar").val().replace(/,/g,"")),
		dp = Number($("#dp").val().replace(/,/g,"")),
		kembalian = 0,
		deposit = Number($("#deposit-terpakai").text().replace(/,/g,""));
	if(dp > 0){
		kembalian = bayar-dp;
	}else{
		kembalian = bayar-(gtotal-deposit);	
	}
	
	if(kembalian > 0 && bayar > 0){
		$("#kembalian").text($.number(kembalian));	
	}else{
		$("#kembalian").text('');	
	}
}

function bulatkan_keatas(value, bilangan){ // value = angka yang akan dibulatkan, bilangan = acuan, misal 10, 100, 1000, dst
	if(typeof bilangan == 'undefined' || !parseInt(bilangan)){
		bilangan = 10;
	}

	var angka = parseInt(value) ? parseInt(value) : 0;
	var hasil = Math.ceil(angka / bilangan) * bilangan;

	return hasil;
}