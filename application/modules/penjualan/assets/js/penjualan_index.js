$(function(){
	$("#konsumen").select2({
        placeholder :"-- Konsumen --",
        allowClear  : true
  	});
  	
	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	cek_cetak_nota();
});

function cek_cetak_nota(){
	var url = document.location.href;
	var output = url.split(/[\/]+/).pop();
	if(output.substr(0, 3) == 'PBB'){
		$("a[href='"+(siteurl+'penjualan/cetak_nota/'+output)+"']").click();
	}
}