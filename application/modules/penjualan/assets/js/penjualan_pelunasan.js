$(function(){
	$("#bayar").on("keyup", hitung_kembalian);

	$("#bayar").number(true, 0);

	$("#metode_bayar").change(function(){
		cek_lampiran();
	});

	cek_lampiran();

	$("#file_att").change(function(){
		readURL(this);
		$("#nm_file").text(this.value.split('\\').pop());
		$("#tampil_lampiran").show(400);
	});
});

function hitung_kembalian(){
	var kurang_bayar = Number($("#kurang_bayar").text().replace(/,/g,"")),
		bayar = Number($("#bayar").val().replace(/,/g,"")),
		kembalian = 0;
	
	kembalian = bayar - kurang_bayar;
	
	if(kembalian > 0){
		$("#kembalian").text($.number(kembalian));	
	}else{
		$("#kembalian").text('');	
	}
}

function cek_lampiran(){
	if($("#metode_bayar").val() == 3){
		$("#lampiran_transfer").show(400);
	}else{
		$("#lampiran_transfer").hide(400);
		$("#nm_file").text("");
		$("#thumb").prop("src","");
		$("#tampil_lampiran").hide(400);
		$("#file_att").val("");
	}
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var tempImage1 = new Image();
        reader.onload = function (e) {
            $('#thumb').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}