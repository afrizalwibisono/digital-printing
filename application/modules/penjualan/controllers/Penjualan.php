<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2019, Cokeshome
 * 
 * This is controller for print po validation

*/
class Penjualan extends Admin_Controller {

	protected $viewPermission   		= "Penjualan.View";
    protected $addPermission    		= "Penjualan.Add";
    protected $deletePermission 		= "Penjualan.Delete";
    protected $prefix                   = "PBB";

    protected $laporanPenjualanPermission   = "Laporan Penjualan Barang.View";
    protected $laporanPiutangPermission     = "Laporan Piutang Penjualan Barang.View";

    public function __construct(){
    	parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('penjualan');
        $this->load->model([
        					'penjualan/penjualan_model',
        					'penjualan/penjualan_detail_model',
                            'bahan_baku/bahan_baku_model',
                            'konsumen/konsumen_model',
                            'setting_harga_bb/setting_harga_bb_model',
                            'setting_harga_bb/setting_harga_bb_diskon_model',
                            'stok_model',
                            'penjualan/gudang_model',
                            'kas_kasir/bayar_trans_model'
    						]);

        $this->template->title(lang('pj_title'));
		$this->template->page_icon('fa fa-cart-plus');
    }

    public function index($id_penjualan = ''){
    	$this->auth->restrict($this->viewPermission);
        // Pagination
        $this->load->library('pagination');

        $search = $this->input->post('table_search') ? $this->input->post('table_search') : $this->input->get('search');
        $tgl_awal  = $this->input->post('tgl_awal') ? $this->input->post('tgl_awal') : $this->input->get('tgl_awal');
        $tgl_akhir = $this->input->post('tgl_akhir') ? $this->input->post('tgl_akhir') : $this->input->get('tgl_akhir');
        $idc       = $this->input->post('konsumen') ? $this->input->post('konsumen') : $this->input->get('idc');

        $filter = "?search=".urlencode($search);
        $search2 = $this->db->escape_str($search);

        $add_where = "";
        if($tgl_awal !='' && $tgl_akhir !='')
        {
            $filter    .= "&tgl_awal=".urlencode(date_ymd($tgl_awal))."&tgl_akhir=".urlencode(date_ymd($tgl_akhir));
            $add_where .= " AND (penjualan.tgl_tran >='".date_ymd($tgl_awal)."' AND penjualan.tgl_tran <='".date_ymd($tgl_akhir)."')";
        }

        if($idc){
            $filter    .= "&idc=".urlencode($idc);
            $add_where .= " AND konsumen.idkonsumen='".$idc."'";
        }
        
        $where = "`penjualan.deleted` = 0 $add_where
                AND (`konsumen`.`nama` LIKE '%$search2%' ESCAPE '!'
                OR `grand_total` LIKE '%$search2%' ESCAPE '!')";

        $total = $this->penjualan_model
                    ->join("konsumen", "penjualan.idkonsumen=konsumen.idkonsumen")
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->penjualan_model
                    ->select("penjualan.*, 
                    			konsumen.panggilan,
								konsumen.nama,
                    			konsumen.st, 
                    			IF(`konsumen`.`st` = 0,
						        'Konsumen',
						        IF(`konsumen`.`st` = 1,
						            'Reseller',
						            'Instansi')) AS st_konsumen
						        ")
                    ->join("konsumen", "penjualan.idkonsumen=konsumen.idkonsumen")
                    ->where($where)
                    ->order_by(['created_on' => 'DESC'])
                    ->limit($limit, $offset)
                    ->find_all();

        $dt_konsumen 	= $this->konsumen_model
                                ->select("`idkonsumen` as id, 
                                    CONCAT(IF(panggilan IS NULL, '', panggilan),
                                    ' ',
                                    `nama`,
                                    ' | ',
                                    IF(st = 0,
                                        'konsumen',
                                        IF(st = 1, 'reseller', 'instansi'))) AS nm")
								->where("deleted=0 and st < 2")
								->order_by("nama","asc")
								->find_all();

        $assets 	= 	array(
    						"plugins/select2/js/select2.js",
                            "plugins/select2/css/select2.css",
                            'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        	'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                            'penjualan/assets/js/penjualan_index.js',
    				);

        add_assets($assets);

        $cek_kas_kasir  = cek_saldo_kasir();
        if(!$cek_kas_kasir){

            $this->template->set_message(lang("pj_no_saldo"),"warning");
            $this->template->set("saldo_kosong",0);

        }

        $this->template->set('data', $data);
        $this->template->set('search', $search);
        $this->template->set('tgl1', $tgl_awal);
        $this->template->set('tgl2', $tgl_akhir);
        $this->template->set('konsumen', $idc);
        $this->template->set("dt_konsumen", $dt_konsumen);

        $this->template->set("title", lang('pj_title'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index');
    }

    public function view($id_penjualan = NULL){
        if(!$id_penjualan){
            $this->template->set_message(lang('pj_invalid_id'), 'error');
            redirect('penjualan');
        }

        if(isset($_POST['delete'])){
            if($this->delete($id_penjualan)){
                $this->template->set_message(lang('pj_deleted'), 'success');
                redirect('penjualan');    
            }
            
        }

        $tran   = $this->penjualan_model
                        ->select("penjualan.*, CONCAT(IF(panggilan IS NULL, '', panggilan),
                                        ' ',
                                        `nama`,
                                        ' | ',
                                        IF(st = 0,
                                            'konsumen',
                                            IF(st = 1, 'reseller', 'instansi'))) AS nm_konsumen")
                        ->join('konsumen', 'penjualan.idkonsumen = konsumen.idkonsumen')
                        ->find_by(['id_penjualan' => $id_penjualan, 'penjualan.deleted' => 0]);
        if(!$tran){
            $this->template->set_message(lang('pj_invalid_id'), 'error');
            redirect('penjualan');
        }

        $detail = $this->penjualan_detail_model
                            ->select("penjualan_detail.*, barcode, nm_barang, konversi_satuan.satuan_besar")
                            ->join('barang','penjualan_detail.idbarang_bb = barang.idbarang_bb')
                            ->join('konversi_satuan', 'penjualan_detail.id_konversi = konversi_satuan.id_konversi')
                            ->find_all_by(['id_penjualan' => $id_penjualan, 'penjualan_detail.deleted' => 0]);

        $lampiran = $this->bayar_trans_model
                        ->select('file')
                        ->order_by('created_on', 'DESC')
                        ->find_all_by(['id_transaksi' => $id_penjualan, 'deleted' => 0, 'file <>' => '']);
        
        $terpakai = $this->bayar_trans_model
                        ->select("sum(`bayar`) as pembayaran")
                        ->find_by(['deleted' => 0, 'id_transaksi' => $id_penjualan, 'st_sumber_bayar' => 1]);
        $deposit_terpakai = $terpakai->pembayaran;

        $tunai = $this->bayar_trans_model
                        ->select("sum(`bayar`) as tunai")
                        ->find_by(['deleted' => 0, 'id_transaksi' => $id_penjualan, 'st_sumber_bayar' => 0]);
        $bayar_tunai = $tunai->tunai;

        $this->template->set("tran", $tran);
        $this->template->set("detail", $detail);
        $this->template->set("lampiran", $lampiran);
        $this->template->set("deposit_terpakai", $deposit_terpakai);
        $this->template->set("tunai", $bayar_tunai);

        $asset  =   array(
                        "penjualan/assets/css/penjualan_form.css"
                    );

        add_assets($asset);

        $this->template->title(lang('pj_view'));
        $this->template->page_icon('fa fa-cart-plus');
        $this->template->render('view_penjualan');
    }

    public function pelunasan($id_penjualan = NULL){
        if(!$id_penjualan){
            $this->template->set_message(lang('pj_invalid_id'), 'error');
            redirect('penjualan');
        }

        if(isset($_POST['save'])){
            $simpan  = $this->save_pelunasan($id_penjualan);

            if($simpan){
                $this->template->set_message(lang('pj_pelunasan_success'),"success");
                redirect('penjualan/index/'.urlencode($id_penjualan));
            }
        }

        $tran   = $this->penjualan_model
                        ->select("penjualan.*, CONCAT(IF(panggilan IS NULL, '', panggilan),
                                        ' ',
                                        `nama`,
                                        ' | ',
                                        IF(st = 0,
                                            'konsumen',
                                            IF(st = 1, 'reseller', 'instansi'))) AS nm_konsumen")
                        ->join('konsumen', 'penjualan.idkonsumen = konsumen.idkonsumen')
                        ->find_by(['id_penjualan' => $id_penjualan, 'penjualan.deleted' => 0]);
        if(!$tran){
            $this->template->set_message(lang('pj_invalid_id'), 'error');
            redirect('penjualan');
        }

        $detail = $this->penjualan_detail_model
                            ->select("penjualan_detail.*, barcode, nm_barang, konversi_satuan.satuan_besar")
                            ->join('barang','penjualan_detail.idbarang_bb = barang.idbarang_bb')
                            ->join('konversi_satuan', 'penjualan_detail.id_konversi = konversi_satuan.id_konversi')
                            ->find_all_by(['id_penjualan' => $id_penjualan, 'penjualan_detail.deleted' => 0]);

        $this->template->set("tran", $tran);
        $this->template->set("detail", $detail);

        $asset  =   array(
                        "plugins/number/jquery.number.js",
                        "penjualan/assets/css/penjualan_form.css",
                        "penjualan/assets/js/penjualan_pelunasan.js"
                    );

        add_assets($asset);

        $this->template->title(lang('pj_pelunasan'));
        $this->template->page_icon('fa fa-money');
        $this->template->render('pelunasan_penjualan');
    }

    protected function save_pelunasan($id = NULL){
        if(!$id){
            $this->template->set_message(lang('pj_invalid_id'), 'error');
            redirect('penjualan');
        }

        $tran         = $this->penjualan_model->find($id);
        $kurang_bayar = intval($tran->kurang_bayar);
        $kode_tran    = $tran->kode_tran;
        $gtotal       = $tran->grand_total;

        if(isset($_POST['bayar'])){
            $_POST['bayar'] = remove_comma($_POST['bayar']);    
        }
        $this->form_validation->set_rules('bayar', 'lang:pj_bayar', 'required|greater_than_equal_to['.$kurang_bayar.']');
        
        if($this->form_validation->run() === false){
            $this->template->set_message(validation_errors(), 'error');

            return false;
        }

        $bayar        = $this->input->post('bayar');
        $metode_bayar = $this->input->post('metode_bayar');

        $path = "./upload/penjualan/transfer/pelunasan";
        if (!is_dir($path))
        {
            mkdir($path, 0777, TRUE);
        }

        $lokasi_file = '';
        if(isset($_FILES['file_att']) && $_FILES['file_att']['size']){
            $nm_file    = str_replace("/","_", $kode_tran);
            $nm_file    = str_replace("-","_", $nm_file);

            $config['upload_path']          = $path;
            $config['allowed_types']        = 'jpg|jpeg|png';
            $config['max_size']             = 200;
            $config['overwrite']            = TRUE;
            $config['file_name']            = $nm_file.".jpg";

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file_att')){
                
                $pesan_error = $this->upload->display_errors();
                $this->template->set_message($pesan_error,'error');

                return false;
            }else{
                $upload_data = $this->upload->data();

                $lokasi_file  = "/upload/penjualan/transfer/pelunasan/".$upload_data['file_name'];
            }
        }

        $ket_kas_kasir      = "Pembayaran pelunasan transaksi penjualan dengan kode transaksi : ".$kode_tran;

        //$bayar_fix          = 0;
        $bayar_fix          = doubleval(remove_comma($bayar));

        $simpan_bayar = set_bayar_transaksi($st_transaksi = 1,
                                                $id_transaksi = $id,
                                                $st_bayar_trans = 3,
                                                $st_sumber = 0,
                                                $st_bayar = $metode_bayar-1,
                                                $total_transaksi = $gtotal,
                                                $tagihan = $kurang_bayar,
                                                $bayar = $bayar_fix,
                                                $st_dp = 0,
                                                $lokasi_file = $lokasi_file,
                                                $id_transaksi_master = '',
                                                $keterangan_kas_masuk = $ket_kas_kasir
                                            );

        $sql = $this->db->last_query();
        if($simpan_bayar){
            // Update Penjualan
            $this->penjualan_model->update($id, ['kurang_bayar' => 0, 'st_lunas' => 1]);
            $sql .= "\n\n".$this->db->last_query();

            $keterangan     = "Sukses, menyimpan transaksi penjualan dengan kode transaksi : ".$kode_tran;
            $total          = $gtotal;
            $status         = 1;
        }else{
            $keterangan     = "Gagal, menyimpan transaksi penjualan dengan kode transaksi : ".$kode_tran;
            $total          = $gtotal;           
            $status         = 0;

            $this->template->set_message(lang('pj_pelunasan_failed'), 'error');
        }

        $nm_hak_akses   = $this->addPermission; 
        $kode_universal = $kode_tran;
        $sql            = $sql;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $total, $sql, $status);

        return $status ? true : false;
    }

    public function cetak_nota($id_penjualan = NULL){
        if(!$id_penjualan){
            $this->template->set_message(lang('pj_invalid_id'), 'error');
            redirect('penjualan');
        }

        $identitas = $this->identitas_model->find(1);

        $tran   = $this->penjualan_model
                        ->select("penjualan.*, CONCAT(IF(panggilan IS NULL, '', panggilan),
                                        ' ',
                                        `nama`) AS nm_konsumen, konsumen.telp")
                        ->join('konsumen', 'penjualan.idkonsumen = konsumen.idkonsumen')
                        ->find($id_penjualan);

        $data_bayar     = $this->bayar_trans_model
                                ->select("`idbayar_trans_kasir`,
                                            `st_sumber_bayar`,
                                            `st_bayar`,
                                            `st_dp`,
                                            `dp`,
                                            `dp_deposit`,
                                            `pembayaran`,
                                            `kurang_bayar`,
                                            `kembalian`")
                                ->where(['deleted' => 0, 'id_transaksi' => $id_penjualan])
                                ->order_by("created_on","desc")
                                ->order_by("order","desc")
                                ->find_all();
        $dp         = 0;
        $bayar      = 0;
        $krg_bayar  = 0;
        $kembalian  = 0;

        if(is_array($data_bayar) && count($data_bayar)){
            $bayar      = $data_bayar[0]->pembayaran;
            $dp         = $data_bayar[0]->dp_deposit + $data_bayar[0]->dp;
            $krg_bayar  = $data_bayar[0]->kurang_bayar; 
            $kembalian  = $data_bayar[0]->kembalian;
        }

        $terpakai     = $this->bayar_trans_model
                                ->select("sum(`pembayaran`) as pembayaran")
                                ->find_by(['deleted' => 0, 'id_transaksi' => $id_penjualan, 'st_sumber_bayar' => 1]);
        $deposit_terpakai = $terpakai->pembayaran;
        $saldo_deposit  = get_deposit($tran->idkonsumen);

        $detail = $this->penjualan_detail_model
                            ->select("penjualan_detail.*, barcode, nm_barang, konversi_satuan.satuan_besar, satuan_terkecil.alias")
                            ->join('barang','penjualan_detail.idbarang_bb = barang.idbarang_bb')
                            ->join('konversi_satuan', 'penjualan_detail.id_konversi = konversi_satuan.id_konversi')
                            ->join('satuan_terkecil', 'konversi_satuan.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil')
                            ->find_all_by(['id_penjualan' => $id_penjualan]);

        $this->template->set("idt", $identitas);
        $this->template->set("tran", $tran);
        $this->template->set("detail", $detail);
        $this->template->set("deposit_terpakai", $deposit_terpakai);
        $this->template->set("saldo_deposit", $saldo_deposit);

        $this->template->set('dp',$dp);
        $this->template->set('bayar',$bayar);
        $this->template->set('krg_bayar',$krg_bayar);
        $this->template->set('kembalian',$kembalian);

        $this->template->set_layout("cetak_struk");
        $this->template->title(lang('pj_view'));
        $this->template->render('cetak_struk_80');
    }

    public function create(){
        $cek_kas_kasir  = cek_saldo_kasir();
        if(!$cek_kas_kasir){
            redirect('penjualan');
        }

    	if(isset($_POST['save']) && has_permission($this->addPermission)){
            $id_penjualan  = $this->save();

    		if($id_penjualan){
    			$this->template->set_message(lang('label_simpan_sukses'),"success");
    			redirect('penjualan/index/'.urlencode($id_penjualan));
    		}

    	}

    	$dt_input 	= $this->session->userdata('frm_transaksi');
    	if(!$dt_input){
    		$this->save_session();
    		$dt_input 	= $this->session->userdata('frm_transaksi');
    	}

        $dt_konsumen    = $this->konsumen_model
                                    ->select("`idkonsumen` as id, 
                                        CONCAT(IF(panggilan IS NULL, '', panggilan),
                                        ' ',
                                        `nama`,
                                        ' | ',
                                        IF(st = 0,
                                            'konsumen',
                                            IF(st = 1, 'reseller', 'instansi'))) AS nm")
                                    ->where("deleted=0 and st < 2")
                                    ->order_by("nama","asc")
                                    ->find_all();

    	$asset 	= 	array(
    						"plugins/select2/js/select2.js",
                            "plugins/select2/css/select2.css",
                            "plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
                        	"plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
                        	"plugins/number/jquery.number.js",
                            "penjualan/assets/js/penjualan_form.js",
                            "penjualan/assets/css/penjualan_form.css"
    				);

    	add_assets($asset);
        $this->template->set("dt_input", $dt_input);
    	$this->template->set("dt_konsumen", $dt_konsumen);

        $this->template->title(lang('pj_new'));
        $this->template->page_icon('fa fa-cart-plus');
        $this->template->render('form_penjualan');     	    	

    }

    protected function save(){
        if(isset($_POST['dp'])){
            $_POST['dp'] = remove_comma($_POST['dp']);
        }
        if(isset($_POST['bayar'])){
            $_POST['bayar'] = remove_comma($_POST['bayar']);
        }

        $idkonsumen    = $this->input->post('idkonsumen');
        $kode_tran      = $this->input->post('kode_tran');
        $tgl_tran       = $this->input->post('tgl_tran');
        $metode_bayar   = $this->input->post('metode_bayar');
        $dp             = doubleval($this->input->post('dp'));
        $bayar          = doubleval($this->input->post('bayar'));
        $tgl_tempo      = $this->input->post('tgl_tempo');

        $id_barang      = $this->input->post('id_barang');
        $qty            = $this->input->post('qty');
        $konversi       = $this->input->post('konversi');

        if(!is_array($id_barang) || count($id_barang) == 0){
            $this->save_session();
            $this->template->set_message(lang('pj_no_input'), 'error');

            return false;
        }

        // Get ID Gudang
        $dt_gudang = $this->gudang_model->find_by(['tipe' => 1]);
        $id_gudang_asal = $dt_gudang->id_gudang;

        // Validate Qty
        $ero_msg = [];
        $urut = 0;
        foreach ($qty as $key => $val) {
            $urut ++;

            if(intval($val) == 0){
                $ero_msg[] = sprintf(lang('pj_no_qty'), $urut);
            }
            // Cek Stok
            $dt_real_qty = hitung_ke_satuan_kecil($konversi[$key], $qty[$key]);
            $real_qty    = $dt_real_qty['qty'];
            if(!$this->cek_stok($id_barang[$key], $real_qty, $id_gudang_asal))
            {
                $ero_msg[] = sprintf(lang('pj_stok_minus'), $urut);
            }
            // Cek harga
            $dt_harga = $this->get_harga($idkonsumen, [$id_barang[$key]], [$qty[$key]], [$konversi[$key]]);
            if(isset($dt_harga[$id_barang[$key]]) && $dt_harga[$id_barang[$key]]['harga'] == 0){
                $ero_msg[] = sprintf(lang('pj_no_harga'), $urut);
            }
        }

        if($ero_msg)
        {
            $this->save_session();

            $ero_msg = implode("<br>", $ero_msg);
            $this->template->set_message($ero_msg, 'error');
            return FALSE;
        }

        $lama_tempo = 0;
        if($tgl_tempo != ''){
            $date1 = new DateTime(date_ymd($tgl_tran));
            $date2 = new DateTime(date_ymd($tgl_tempo));
            $interval = $date1->diff($date2);
            $lama_tempo = $interval->days;
        }

        $cek_kas_kasir  = cek_saldo_kasir();
        if(!$cek_kas_kasir){
            $this->save_session();

            $this->template->set_message(lang("pj_no_saldo"),"error");
            return false;
        }

        $dt_harga = $this->get_harga($idkonsumen, $id_barang, $qty, $konversi);
        $gtotal = 0;
        if($id_barang){
            foreach ($dt_harga as $key => $val) {
                $gtotal += $val['real_qty'] * ($val['harga'] - ($val['harga']*$val['diskon']/100));
            }
        }

        $gtotal = bulatkan_keatas($gtotal, 100);

        $saldo_deposit = get_deposit($idkonsumen);
        $bayar_dg_deposit = 0;
        $sisa_tagihan  = 0;

        if($saldo_deposit > 0){
            if($saldo_deposit >= $gtotal){
                $bayar_dg_deposit = $gtotal;
            }else{
                $bayar_dg_deposit = $saldo_deposit;
            }    
        }

        $sisa_tagihan = $gtotal - ($dp > 0 ? $dp : ($bayar_dg_deposit > 0 ? $bayar_dg_deposit : 0));

    	$this->form_validation->set_rules('kode_tran', 'lang:pj_kode_tran', 'required|trim');
        $this->form_validation->set_rules('idkonsumen', 'lang:pj_konsumen', 'callback_pjcek_konsumen');
        $this->form_validation->set_rules('tgl_tran', 'lang:pj_tgl', 'required|trim');
        if($saldo_deposit == false || $saldo_deposit == 0){
            if((isset($_POST['dp']) && intval($_POST['dp']) > 0)){
                $this->form_validation->set_rules('dp', 'lang:pj_dp', 'numeric');
                $this->form_validation->set_rules('tgl_tempo', 'lang:pj_tgl_tempo', 'required|trim');
                $this->form_validation->set_rules('bayar', 'lang:pj_bayar', "required|greater_than_equal_to[".$dp."]");
            }else{
                $this->form_validation->set_rules('bayar', 'lang:pj_bayar', "required|greater_than_equal_to[".$gtotal."]");
            }
        }elseif ($saldo_deposit < $gtotal) {
            if(empty($bayar)){
                $this->form_validation->set_rules('tgl_tempo', 'lang:pj_tgl_tempo', 'required|trim');
            }
            if(!empty($bayar)){
                $this->form_validation->set_rules('bayar', 'lang:pj_bayar', "required|greater_than_equal_to[".$sisa_tagihan."]");
            }
        }else{
            $metode_bayar = 4; //Deposit
        }

        $this->form_validation->set_message('pjcek_konsumen', 'You should choose the "%s" field');
        $this->form_validation->CI =& $this;
        
    	if($this->form_validation->run() === false){
    		$this->save_session();
    		$this->template->set_message(validation_errors(), 'error');

    		return false;
    	}

        $this->db->trans_begin();

        $path = "./upload/penjualan/transfer";
        if (!is_dir($path))
        {
            mkdir($path, 0777, TRUE);
        }

        $lokasi_file = '';
        if(isset($_FILES['file_att']) && $_FILES['file_att']['size']){
            $nm_file    = str_replace("/","_", $kode_tran);
            $nm_file    = str_replace("-","_", $nm_file);

            $config['upload_path']          = $path;
            $config['allowed_types']        = 'jpg|jpeg|png';
            $config['max_size']             = 200;
            $config['overwrite']            = TRUE;
            $config['file_name']            = $nm_file.".jpg";

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file_att')){
                
                $pesan_error = $this->upload->display_errors();
                $this->template->set_message($pesan_error,'error');
                $this->save_session();

                return false;
            }else{
                $upload_data = $this->upload->data();

                $lokasi_file  = "/upload/penjualan/transfer/".$upload_data['file_name'];
            }
        }

        $data_insert = [
                        'id_penjualan'  => gen_primary($this->prefix, "penjualan", "id_penjualan"),
                        'idkonsumen'    => $idkonsumen,
                        'kode_tran'     => $kode_tran,
                        'tgl_tran'      => date_ymd($tgl_tran),
                        'tipe'          => $tgl_tempo != '' ? 2 : 1,
                        'lama_tempo'    => $lama_tempo,
                        'tgl_tempo'     => $tgl_tempo ? date_ymd($tgl_tempo) : null,
                        'grand_total'   => $gtotal,
                        'metode_bayar'  => $metode_bayar,
                        'dp'            => ($dp > 0 ? $dp : ($bayar_dg_deposit > 0 ? $bayar_dg_deposit : 0)),
                        'kurang_bayar'  => $sisa_tagihan < 0 ? 0 : $sisa_tagihan,
                        'st_lunas'      => ($bayar+$bayar_dg_deposit-$gtotal >= 0 || $saldo_deposit >= $gtotal) ? 1 : 0,
                        'counter_cetak' => 0,
                        'st_bayar'      => (intval($dp) > 0 || intval($bayar) || $saldo_deposit > 0) ? 1 : 0
                        ];
        $sql = '';
        $this->penjualan_model->insert($data_insert);
        $sql .= $this->db->last_query();

        $id_penjualan = $data_insert['id_penjualan'];

        $data_detail = [];
        if($id_barang){
            foreach ($dt_harga as $key => $val) {
                $dt_barang = $this->bahan_baku_model->find($key);
                $data_detail[] = [
                            'id_penjualan_detail' => gen_primary('', 'penjualan_detail', 'id_penjualan_detail'),
                            'id_penjualan' => $id_penjualan,
                            'idbarang_bb'  => $key,
                            'qty'          => $val['qty'],
                            'id_konversi'  => $val['id_konversi'],
                            'hpp'       => $val['hpp'],
                            'harga'     => $val['harga'],
                            'diskon'    => $val['diskon'],
                            'nilai_diskon' => $val['qty'] * ($val['harga']*$val['diskon']/100),
                            'total'     => $val['real_qty'] * ($val['harga'] - ($val['harga']*$val['diskon']/100))
                            ];
                //Update Stok barang
                $this->stok_model->update_stok_by_kd_barang($key, $val['real_qty'], 0, $id_gudang_asal);
                $sql .= "\n\n".$this->db->last_query();
            }
        }

        if($data_detail){
            $this->penjualan_detail_model->insert_batch($data_detail);
            $sql .= "\n\n".$this->db->last_query();    
        }

        //Simpan Penggunaan Deposit jika $bayar_dg_deposit > 0
        $st_use_deposit = true;
        if($bayar_dg_deposit > 0){
            $ket_kas_kasir      = "Pembayaran penjualan dengan deposit untuk kode transaksi : ".$kode_tran;
            $simpan_bayar = set_bayar_transaksi($st_transaksi = 1,
                                                    $id_transaksi = $id_penjualan,
                                                    $st_bayar_trans = 2,
                                                    $st_sumber = 1,
                                                    $st_bayar = 3, // Deposit
                                                    $total_transaksi = $gtotal,
                                                    $tagihan = $bayar_dg_deposit,
                                                    $xbayar  = $bayar_dg_deposit,
                                                    $st_dp = $bayar_dg_deposit >= $gtotal ? 0 : 1,
                                                    $lokasi_file = '',
                                                    $id_transaksi_master = '',
                                                    $keterangan_kas_masuk = $ket_kas_kasir,
                                                    0
                                                );

            $add_text_msg = '';
            if($saldo_deposit < $gtotal){
                $add_text_msg = 'DP ';
            }

            $st_use_deposit = use_deposit($idkonsumen, $bayar_dg_deposit, 'Pembayaran '.$add_text_msg.'tagihan pembelian dengan kode transaksi '.$kode_tran);
        }

        if($bayar > 0){
            $ket_kas_kasir      = "Pembayaran penjualan untuk kode transaksi : ".$kode_tran;
            $simpan_bayar = set_bayar_transaksi($st_transaksi = 1,
                                                $id_transaksi = $id_penjualan,
                                                $st_bayar_trans = 2,
                                                $st_sumber = 0,
                                                $st_bayar = $metode_bayar-1,
                                                $total_transaksi = $gtotal,
                                                $tagihan = $dp ? $dp : ($bayar_dg_deposit > 0 ? ($gtotal - $bayar_dg_deposit) : $gtotal),
                                                $bayar  = $bayar,
                                                $st_dp = $dp ? 1 : 0,
                                                $lokasi_file = $lokasi_file,
                                                $id_transaksi_master = '',
                                                $keterangan_kas_masuk = $ket_kas_kasir,
                                                1
                                            );
        }

    	if($this->db->trans_status() == false || $st_use_deposit == false){
            $this->db->trans_rollback();
            $keterangan     = "Gagal, menyimpan transaksi penjualan dengan kode transaksi : ".$kode_tran;
            $total          = 0;
            $status         = 0;
        }else{
            $this->db->trans_commit();
            $keterangan     = "Sukses, menyimpan transaksi penjualan dengan kode transaksi : ".$kode_tran;
            $total          = 0;           
            $status         = 1;
        }

        $nm_hak_akses   = $this->addPermission; 
        $kode_universal = $kode_tran;
        $jumlah         = 0;
        $sql            = $sql;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $total, $sql, $status);
        // Clear Session
        $this->session->unset_userdata('frm_transaksi');

        return $id_penjualan;
    }

    public function pjcek_konsumen($val){
        return $val == '' ? false : true;
    }

    protected function save_session(){
    	$kode_tran 		= $this->input->post('kode_tran');
    	$idkonsumen 	= $this->input->post('idkonsumen');
        $tgl_tran       = $this->input->post('tgl_tran');
        $metode_bayar   = $this->input->post('metode_bayar');
        $dp             = remove_comma($this->input->post('dp'));
        $bayar          = remove_comma($this->input->post('bayar'));
        $tgl_tempo      = $this->input->post('tgl_tempo');

        $id_barang      = $this->input->post('id_barang');
        $qty            = $this->input->post('qty');
    	$konversi       = $this->input->post('konversi');

        if(count($id_barang) > 0 && $idkonsumen){
            $dt_harga = $this->get_harga($idkonsumen, $id_barang, $qty, $konversi);    
        }
        
        $items    = [];
        $gtotal = 0;
        if($id_barang){
            foreach ($dt_harga as $key => $val) {
                $dt_barang = $this->bahan_baku_model->find($key);

                $item = [
                            'id_barang' => $key,
                            'barcode'   => $dt_barang->barcode,
                            'nm_barang' => $dt_barang->nm_barang,
                            'qty'  => $val['qty'],
                            'id_konversi'  => $val['id_konversi'],
                            'konversi' => get_konversi($dt_barang->id_satuan_terkecil, $val['id_konversi']),
                            'harga'  => $val['harga'],
                            'diskon'  => $val['diskon'],
                            'subtotal'  => $val['real_qty'] * ($val['harga'] - ($val['harga']*$val['diskon']/100))
                            ];

                $items[] = $item;
                $gtotal += $item['subtotal'];
            }
        }

    	$data = [
                 'kode_tran'  => $kode_tran ? $kode_tran : $this->gen_kode_tran(),
    			 'idkonsumen' => $idkonsumen,
                 'tgl_tran'     => $tgl_tran ? $tgl_tran : date('d/m/Y'),
                 'metode_bayar' => $metode_bayar,
                 'dp'           => $dp,
                 'bayar'        => $bayar,
                 'tgl_tempo'    => $tgl_tempo,
                 'gtotal'       => $gtotal,
                 'kurang_bayar' => $dp > 0 ? $gtotal-$dp : 0,
                 'kembalian'    => $bayar > 0 ? $bayar-$gtotal : ($dp > 0 ? $bayar-$dp : 0), 
                 'detail'       => $items
    			];
    	
    	$this->session->set_userdata('frm_transaksi', $data);
    }

    //Cancel Validasi cetak
    public function cancel(){
        $this->auth->restrict($this->addPermission);

        $this->session->unset_userdata('frm_transaksi');
        $this->template->set_message(lang('label_cancel_sukses'), 'success');

        redirect('penjualan');
    }

    public function delete($id = NULL){
        $this->auth->restrict($this->deletePermission);
        if(!$id){
            $this->template->set_message(lang('pj_invalid_id'), 'error');
            redirect('penjualan');
        }

        $this->db->trans_start();
        
        $dt = $this->penjualan_model->find($id);
        // Get ID Gudang
        $dt_gudang      = $this->gudang_model->find_by(['tipe' => 1]);
        $id_gudang_asal = $dt_gudang->id_gudang;
        //Kembalikan Stok
        $detail = $this->penjualan_detail_model->find_all(['deleted' => 0, 'id_penjualan' => $id]);
        $sql    = '';
        foreach ($detail as $key => $val) {
            $dt_real_qty = hitung_ke_satuan_kecil($val->id_konversi, $val->qty);
            $real_qty    = $dt_real_qty['qty'];
            //Update Stok barang (kembalikan stok)
            $this->stok_model->update_stok_by_kd_barang($val->idbarang_bb, $real_qty, 1, $id_gudang_asal);
            $sql .= "\n\n".$this->db->last_query();
        }
        // Delete Detail
        $this->penjualan_detail_model->delete_where(['id_penjualan' => $id]);
        $sql .= "\n\n".$this->db->last_query();
        // Delete Penjualan
        $del_jual = $this->penjualan_model->delete($id);
        $sql .= "\n\n".$this->db->last_query();
        // Delete Pembayaran
        if($del_jual){
            $ket = "Pengembalian Pembayaran, kepada konsumen untuk aktivitas delete transaksi. Untuk kode transaksi : ".$dt->kode_tran;
            $del = delete_pembayaran($st_delete_by = 1,
                                    $id_primary_bayar = '',
                                    $id_transaksi_master = $id,
                                    $waktu_start_delete = '',
                                    $keterangan_delete = $ket,
                                    $keterangan_kas_kasir = $ket);    
        }
        
        $sql .= "\n\n".$this->db->last_query();

        $this->db->trans_complete();

        if($this->db->trans_status() == false){
            $keterangan     = "Gagal, menghapus transaksi penjualan dengan kode transaksi : ".$dt->kode_tran;
            $total          = 0;
            $status         = 0;
        }else{
            $keterangan     = "Sukses, menghapus transaksi penjualan dengan kode transaksi : ".$dt->kode_tran;
            $total          = 0;           
            $status         = 1;
        }

        $nm_hak_akses   = $this->deletePermission; 
        $kode_universal = $dt->kode_tran;
        $jumlah         = $dt->grand_total;
        $sql            = $sql;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $total, $sql, $status);

        if($status === 0){
            $this->template->set_message(lang('pj_del_failure').$this->penjualan_model->error, 'error');
        }

        return $status ? true : false;
    }

    private function gen_kode_tran($prefix = "TRV"){

        $kode_tran  = "";

        $bulan      = date("m");
        $tahun      = date("Y");
        $day        = date("d");

        $where      = "DAY(`created_on`) = {$day} 
                        AND MONTH(`created_on`) = {$bulan}
                        AND YEAR(`created_on`) = {$tahun}";

        $data       = $this->penjualan_model->select('kode_tran')
                                        ->where($where)
                                        ->order_by("created_on","desc")
                                        ->limit(1)
                                        ->find_all();


        if(is_array($data) && count($data)){

            $no = explode("-", $data[0]->kode_tran);
            if(count($no) == 2){

                $no = intval($no[1]) + 1;

            }else{


                $no = 1;

            }


        }else{

            $no = 1;

        }
        
        $kode_tran  = $prefix."/".$tahun."/".$bulan."/".$day."-".str_pad($no, 5,"0",STR_PAD_LEFT);

        return $kode_tran;

    }

    public function cari_barang()
    {
        if(!$this->input->is_ajax_request())
        {
            die(json_encode(['type' => 'error', 'msg' => lang('pj_only_ajax')]));
        }

        $cari = $this->db->escape_str($this->input->post('cr_barang'));

        $data = $this->bahan_baku_model->select("barang.idbarang_bb, barcode, nm_barang, barang.id_satuan_terkecil, tstok.stok, satuan_terkecil.alias AS satuan, harga")
                                    ->join("setting_harga_bb","barang.idbarang_bb = setting_harga_bb.idbarang_bb")
                                    ->join("(select id_barang, id_gudang, sum(stok_update) as stok from stok where deleted = 0 AND stok_update > 0 group by id_barang, id_gudang) as tstok","setting_harga_bb.idbarang_bb = tstok.id_barang")
                                    ->join("satuan_terkecil", "barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil", "left")
                                    ->join("gudang","tstok.id_gudang = gudang.id_gudang")
                                    ->where("barang.deleted = 0 AND gudang.tipe = 1 
                                            AND setting_harga_bb.harga > 0 AND setting_harga_bb.deleted = 0
                                            AND (barcode like '%$cari%' OR nm_barang like '%$cari%')")
                                    ->order_by('nm_barang', 'ASC')
                                    ->group_by(array("setting_harga_bb.idbarang_bb"))
                                    ->find_all();

        if($data)
        {
            foreach ($data as $key => $dt) {
                $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
            }
        }

        echo json_encode($data);
    }

    public function scan_barcode()
    {
        if(!$this->input->is_ajax_request())
        {
            die(json_encode(['type' => 'error', 'msg' => lang('pj_only_ajax')]));
        }

        $barcode = $this->db->escape_str($this->input->post('barcode'));

        if($barcode != "")
        {
            $data = $this->bahan_baku_model->select("barang.idbarang_bb, barcode, nm_barang, barang.id_satuan_terkecil, tstok.stok, satuan_terkecil.alias AS satuan, harga")
                                    ->join("setting_harga_bb","barang.idbarang_bb = setting_harga_bb.idbarang_bb")
                                    ->join("(select id_barang, id_gudang, sum(stok_update) as stok from stok where deleted = 0 AND stok_update > 0 group by id_barang, id_gudang) as tstok","setting_harga_bb.idbarang_bb = tstok.id_barang")
                                    ->join("satuan_terkecil", "barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil", "left")
                                    ->join("gudang","tstok.id_gudang = gudang.id_gudang")
                                    ->where("barang.deleted = 0 AND gudang.tipe = 1 
                                            AND barcode='{$barcode}'
                                            AND setting_harga_bb.harga > 0 AND setting_harga_bb.deleted = 0")
                                    ->order_by('nm_barang', 'ASC')
                                    ->group_by(array("setting_harga_bb.idbarang_bb"))
                                    ->find_all();

            if($data)
            {
                foreach ($data as $key => $dt) {
                    $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
                }

                $return = array('type' => 'success', 'barcode' => $barcode, 'data' => $data);
            }
            else
            {
                $return = array('type' => 'error', 'barcode' => $barcode, 'data' => 0);
            }
        }
        else
        {
            $return = array('type' => 'error', 'barcode' => $barcode, 'data' => 0);
        }

        echo json_encode($return);
    }

    public function get_harga($idkonsumen = NULL, $id_barang = NULL, $qty = NULL, $id_konversi = NULL){
        if(!$idkonsumen){
            if(!$this->input->is_ajax_request())
            {
                die(json_encode(['type' => 'error', 'msg' => lang('pj_only_ajax')]));
            }    
        }

        $idkonsumen = $idkonsumen ? $idkonsumen : $this->input->post('idkonsumen');
        $id_barang  = $id_barang ? $id_barang : $this->input->post('id_barang');
        $qty        = $qty ? $qty : $this->input->post('qty');
        $id_konversi  = $id_konversi ? $id_konversi : $this->input->post('id_konversi');
        $gol_konsumen = 0; //Konsumen biasa
        
        if($idkonsumen){
            $dt_konsumen = $this->konsumen_model->find($idkonsumen);
            if($dt_konsumen){
                $gol_konsumen = $dt_konsumen->st;
            }
        }

        if(!$idkonsumen || !$id_barang || !$qty || !$id_konversi){
            return false;
        }

        $return = [];
        if($id_barang){
            foreach ($id_barang as $key => $val) {
                $dt_real_qty = hitung_ke_satuan_kecil($id_konversi[$key], $qty[$key]);
                $real_qty    = $dt_real_qty['qty'];
                
                $dt_harga = $this->setting_harga_bb_model
                                ->select(['setting_harga_bb.idsetting_harga_bb', 'hpp', 'harga', 'diskon'])
                                ->join("(SELECT 
                                            setting_harga_bb.idsetting_harga_bb, range2, diskon
                                        FROM
                                            setting_harga_bb
                                                LEFT JOIN
                                            setting_harga_bb_diskon ON setting_harga_bb.idsetting_harga_bb = setting_harga_bb_diskon.idsetting_harga_bb
                                        WHERE
                                            id_golongan_member = {$gol_konsumen}
                                                AND setting_harga_bb.idbarang_bb = '{$val}'
                                                AND (({$real_qty} >= range1 AND {$real_qty} <= range2)
                                                OR {$real_qty} >= range2)
                                        ORDER BY range2 DESC
                                        LIMIT 1) as tdiskon", 'setting_harga_bb.idsetting_harga_bb = tdiskon.idsetting_harga_bb','left')
                                ->find_by(['idbarang_bb' => $val]);

                if($dt_harga){                
                    $return['data'][$val] = ['hpp' => $dt_harga->hpp, 'harga' => $dt_harga->harga, 'diskon' => $dt_harga->diskon, 'qty' => $qty[$key], 'real_qty' => $real_qty, 'id_konversi' => $id_konversi[$key]];
                }else{
                    $return['data'][$val] = ['hpp' => 0, 'harga' => 0, 'diskon' => 0, 'qty' => $qty[$key], 'real_qty' => $real_qty, 'id_konversi' => $id_konversi[$key]];
                }
            }

            $return['type'] = 'success';
            $return['msg'] = '';
        }

        if(!$this->input->is_ajax_request()){
            return $return['data'];
        }

        $return['deposit'] = get_deposit($idkonsumen);

        echo json_encode($return);
    }

    protected function cek_stok($id_barang = "", $qty = 0, $id_gudang_asal = ''){
        $cek_stok = $this->stok_model->order_by("created_on", "desc")
                                ->find_all_by(array('deleted'   => 0, 
                                                    'id_barang' => $id_barang, 
                                                    'stok_update >' => 0,
                                                    'id_gudang' => $id_gudang_asal
                                                    ));
        
        if(!$cek_stok)
        {
            return FALSE;
        }

        //Hitung stok
        $stok = 0;
        foreach ($cek_stok as $key0 => $ck) {
            $stok += $ck->stok_update;
        }

        if($stok < $qty)
        {
           return FALSE;
        }

        return TRUE;
    }

// ========================================================================================================
// editing dekky 

    public function get_konsumen(){

        if(!$this->input->is_ajax_request()){

            redirect("lap_rekap_trans");

        }

        $st_konsumen    = $this->input->post("st");

        $data   = $this->konsumen_model->select("`idkonsumen` as id,
                                                CONCAT(`konsumen`.`panggilan`,
                                                ' ',
                                                `konsumen`.`nama`) AS nm")
                        ->where("st = {$st_konsumen}")
                        ->order_by("nm")
                        ->find_all();

        if(is_array($data) && count($data)){

            $isi    = ["st" => 1, "data" => $data];

        }else{

            $isi    = ["st" => 0, "data" => ""];

        }

        echo json_encode($isi);

    }

    public function laporan(){

        $this->auth->restrict($this->laporanPenjualanPermission);

        $where      = " penjualan.deleted = 0
                        AND penjualan_detail.deleted = 0 ";

        $tgl1       = isset($_POST['tgl1']) ? $this->input->post('tgl1') : '';
        $tgl2       = isset($_POST['tgl2']) ? $this->input->post('tgl2') : '';
        $konsumen   = isset($_POST['konsumen']) ? $this->input->post('konsumen') : '';
        $gol_kons   = isset($_POST['gol_konsumen']) ? $this->input->post('gol_konsumen') : '';        
        $st_lunas   = isset($_POST['st_lunas']) ? $this->input->post('st_lunas') : '';        

        if(strlen($tgl1) > 0){

            $tgl1_cr    = date_ymd($tgl1). " 01:00:00";
            $tgl2_cr    = date_ymd($tgl2). " 23:59:00";        
             
        }else{

            $tgl1_cr    = date("Y-m-d"). " 01:00:00";
            $tgl2_cr    = date("Y-m-d"). " 23:59:00";

            $tgl1       = date("d/m/Y");
            $tgl2       = date("d/m/Y");

        }
        
        $where      .= " AND (`penjualan`.`tgl_tran` >= '{$tgl1_cr}' AND `penjualan`.`tgl_tran` <= '{$tgl2_cr}')";

        $filter     =   [
                            'tgl1'          => $tgl1,
                            'tgl2'          => $tgl2,
                            'konsumen'      => $konsumen,
                            'gol_konsumen'  => $gol_kons,
                            'st_lunas'      => $st_lunas
                        ];

        if(strlen($st_lunas) > 0){        

            $where  .= " and penjualan.st_lunas = '{$st_lunas}'";            

        }

        if(strlen($konsumen) > 0){

            $where  .= " and `konsumen`.`nama` = '{$konsumen}'";

        }else{

            if(strlen($gol_kons) > 0){

                $where  .= " and `konsumen`.`st` = '{$gol_kons}'";

            }

        }



        $dt_penjualan   = $this->penjualan_model
                                ->select("`penjualan`.`id_penjualan`,
                                            `users`.`nm_lengkap`,
                                            CONCAT(`konsumen`.`panggilan`,
                                                    ' ',
                                                    `konsumen`.`nama`) AS nm,
                                            IF(konsumen.st = 0,
                                                'Konsumen',
                                                IF(konsumen.st = 1,
                                                    'Reseller',
                                                    'Instansi')) AS st_konsumen,
                                            `penjualan`.`kode_tran`,
                                            DATE_FORMAT(`penjualan`.`tgl_tran`,
                                                    '%d/%m/%Y %h:%i:%s') AS waktu_trans,
                                            `penjualan`.`tipe`,
                                            `penjualan`.`lama_tempo`,
                                            DATE_FORMAT(`penjualan`.`tgl_tempo`,
                                                    '%d/%m/%Y %h:%i:%s') AS jth_tempo,
                                            `penjualan`.`total`,
                                            `penjualan`.`diskon`,
                                            `penjualan`.`nilai_diskon`,
                                            `penjualan`.`potongan`,
                                            `penjualan`.`grand_total`,
                                            `penjualan`.`metode_bayar`,
                                            `penjualan`.`st_lunas`,
                                            t_bayar.total_bayar,
                                            (`penjualan`.`grand_total` - t_bayar.total_bayar) AS kurang_bayar,
                                            `barang`.`nm_barang`,
                                            `penjualan_detail`.`qty`,
                                            `penjualan_detail`.`id_konversi`,
                                            `konversi_satuan`.`satuan_besar`,
                                            `penjualan_detail`.`hpp`,
                                            (`penjualan_detail`.`harga` * `konversi_satuan`.`jml_kecil`) AS harga_pakai,
                                            `penjualan_detail`.`diskon`,
                                            `penjualan_detail`.`nilai_diskon`,
                                            `penjualan_detail`.`potongan`,
                                            `penjualan_detail`.`total` as subtotal")
                                ->join("users","penjualan.created_by = users.id_user","inner")
                                ->join("konsumen","penjualan.idkonsumen = konsumen.idkonsumen","inner")
                                ->join("penjualan_detail","penjualan.id_penjualan = penjualan_detail.id_penjualan","inner")
                                ->join("barang","penjualan_detail.idbarang_bb = barang.idbarang_bb","inner")
                                ->join("konversi_satuan","penjualan_detail.id_konversi = konversi_satuan.id_konversi","inner")
                                ->join("(SELECT 
                                                `id_transaksi_master` AS id, SUM(`bayar`) AS total_bayar
                                            FROM
                                                bayar_trans_kasir
                                            WHERE
                                                deleted = 0
                                            GROUP BY id_transaksi_master) AS t_bayar","penjualan.id_penjualan = t_bayar.id","inner")
                                ->where($where)
                                ->order_by("penjualan.tgl_tran","asc")
                                ->order_by("penjualan.kode_tran","asc")
                                ->order_by("barang.nm_barang","asc")
                                ->find_all();


        $data           = [];
        $jml_transaksi  = 0;
        $totalall       = 0;

        if(is_array($dt_penjualan) && count($dt_penjualan)){

            $id_acuan   = "";
            $index      = 0;

            foreach ($dt_penjualan as $key => $isi) {
                
                if($id_acuan != $isi->id_penjualan){

                    $data[]     =   [
                                        'no_trans'  => $isi->kode_tran,
                                        'waktu'     => $isi->waktu_trans,
                                        'konsumen'  => $isi->nm,
                                        'gol_kons'  => $isi->st_konsumen,
                                        'status'    => $isi->tipe,
                                        'st_lunas'  => $isi->st_lunas,
                                        'gtotal'    => $isi->grand_total,
                                        'op'        => $isi->nm_lengkap,
                                        'bayar'     => $isi->total_bayar,
                                        'krg_bayar' => $isi->kurang_bayar,
                                        'detail'    =>  [
                                                            [
                                                                'nm_barang' => $isi->nm_barang,
                                                                'qty'       => number_format($isi->qty)." ".$isi->satuan_besar,
                                                                'harga'     => $isi->harga_pakai,
                                                                'diskon'    => $isi->diskon,
                                                                'total'     => $isi->subtotal
                                                            ]
                                                        ]
                                    ];

                    $totalall   +=  $isi->grand_total;
                    $id_acuan   = $isi->id_penjualan;
                    $index      = $key == 0 ? 0 : $index+1;

                }else{

                    $data[$index]['detail'][]   =   [
                                                        'nm_barang' => $isi->nm_barang,
                                                        'qty'       => number_format($isi->qty)." ".$isi->satuan_besar,
                                                        'harga'     => $isi->harga_pakai,
                                                        'diskon'    => $isi->diskon,
                                                        'total'     => $isi->subtotal
                                                    ];

                }

            }

            $jml_transaksi  = count($data);

        }

        

        $asset  =   [
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'penjualan/assets/js/laporan_penjualan.js'
                    ]; 

        add_assets($asset);

        //$this->template->set("dt_resume",$dt_resume);
        $this->template->set("jml_transaksi",$jml_transaksi);
        $this->template->set("total_transaksi",$totalall);
        $this->template->set("data",$data);

        $this->template->set("filter",$filter);
        $this->template->set("toolbar_title", lang('lap-jual-title'));
        $this->template->title(lang('lap-jual-title'));
        $this->template->page_icon('fa fa-file-o');
        $this->template->render('laporan_penjualan'); 

    }


    public function laporan_piutang_penjualan(){

        $this->auth->restrict($this->laporanPiutangPermission);

        $where      = " penjualan.deleted = 0
                        AND penjualan.tipe = 2";

        $tgl1       = isset($_POST['tgl1']) ? $this->input->post('tgl1') : '';
        $tgl2       = isset($_POST['tgl2']) ? $this->input->post('tgl2') : '';
        $konsumen   = isset($_POST['konsumen']) ? $this->input->post('konsumen') : '';
        $gol_kons   = isset($_POST['gol_konsumen']) ? $this->input->post('gol_konsumen') : '';        

        if(strlen($tgl1) > 0){

            $tgl1_cr    = date_ymd($tgl1). " 01:00:00";
            $tgl2_cr    = date_ymd($tgl2). " 23:59:00";        
             
        }else{

            $tgl1_cr    = date("Y-m-d"). " 01:00:00";
            $tgl2_cr    = date("Y-m-d"). " 23:59:00";

            $tgl1       = date("d/m/Y");
            $tgl2       = date("d/m/Y");

        }
        
        $where      .= " AND (`penjualan`.`tgl_tran` >= '{$tgl1_cr}' AND `penjualan`.`tgl_tran` <= '{$tgl2_cr}')";

        $filter     =   [
                            'tgl1'          => $tgl1,
                            'tgl2'          => $tgl2,
                            'konsumen'      => $konsumen,
                            'gol_konsumen'  => $gol_kons
                            
                        ];

        if(strlen($konsumen) > 0){

            $where  .= " and `konsumen`.`nama` = '{$konsumen}'";

        }else{

            if(strlen($gol_kons) > 0){

                $where  .= " and `konsumen`.`st` = '{$gol_kons}'";

            }

        }

        $dt_resume  = $this->penjualan_model
                            ->select("COUNT(`penjualan`.`id_penjualan`) AS jml_trans,
                                        SUM(`penjualan`.`grand_total` - t_bayar.total_bayar) AS kurang_bayar")
                            ->join("users","penjualan.created_by = users.id_user","inner")
                            ->join("konsumen","penjualan.idkonsumen = konsumen.idkonsumen","inner")
                            ->join("(SELECT 
                                            `id_transaksi_master` AS id, SUM(`bayar`) AS total_bayar
                                        FROM
                                            bayar_trans_kasir
                                        WHERE
                                            deleted = 0
                                        GROUP BY id_transaksi_master) AS t_bayar","penjualan.id_penjualan = t_bayar.id","left")
                            ->where($where)
                            ->group_by("penjualan.id_penjualan")
                            ->having("kurang_bayar > 0")
                            ->order_by("penjualan.tgl_tran","asc")
                            ->find_all();

        $dt_trans   = $this->penjualan_model
                            ->select("`penjualan`.`id_penjualan`,
                                        `users`.`nm_lengkap`,
                                        CONCAT(`konsumen`.`panggilan`,
                                                ' ',
                                                `konsumen`.`nama`) AS nm,
                                        IF(konsumen.st = 0,
                                            'Konsumen',
                                            IF(konsumen.st = 1,
                                                'Reseller',
                                                'Instansi')) AS st_konsumen,
                                        `penjualan`.`kode_tran`,
                                        DATE_FORMAT(`penjualan`.`tgl_tran`,
                                                '%d/%m/%Y %h:%i:%s') AS waktu_trans,
                                        `penjualan`.`tipe`,
                                        `penjualan`.`lama_tempo`,
                                        DATE_FORMAT(`penjualan`.`tgl_tempo`,
                                                '%d/%m/%Y %h:%i:%s') AS jth_tempo,
                                        `penjualan`.`grand_total`,
                                        `penjualan`.`st_lunas`,
                                        t_bayar.total_bayar,
                                        (`penjualan`.`grand_total` - t_bayar.total_bayar) AS kurang_bayar")
                            ->join("users","penjualan.created_by = users.id_user","inner")
                            ->join("konsumen","penjualan.idkonsumen = konsumen.idkonsumen","inner")
                            ->join("(SELECT 
                                        `id_transaksi_master` AS id, SUM(`bayar`) AS total_bayar
                                    FROM
                                        bayar_trans_kasir
                                    WHERE
                                        deleted = 0
                                    GROUP BY id_transaksi_master) AS t_bayar","penjualan.id_penjualan = t_bayar.id","left")
                            ->where($where)
                            ->group_by("penjualan.id_penjualan")
                            ->having("kurang_bayar > 0")
                            ->order_by("penjualan.tgl_tran","asc")
                            ->find_all();                            

        $asset  =   [
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'penjualan/assets/js/laporan_penjualan.js'
                    ]; 

        add_assets($asset);

        $this->template->set("resume",$dt_resume);
        $this->template->set("data",$dt_trans);

        $this->template->set("filter",$filter);
        $this->template->set("toolbar_title", lang('lap-piutang-title'));
        $this->template->title(lang('lap-piutang-title'));
        $this->template->page_icon('fa fa-file-o');
        $this->template->render('laporan_piutang_penjualan'); 


    }




}