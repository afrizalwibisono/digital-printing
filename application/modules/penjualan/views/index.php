<?php
	$ENABLE_ADD     = has_permission('Penjualan.Add');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('name'=>'frm_index','id'=>'frm_index','role'=>'form','class'=>'form-inline')) ?>
	<div class="box-header">
		<div class="pull-left">
			<?php if($ENABLE_ADD && !isset($saldo_kosong)): ?>
			<a class="btn btn-success" href="<?= site_url('penjualan/create') ?>"><?= lang('btn_create') ?></a>
			<?php endif; ?>
		</div>
		<div class="pull-right">
			<div class="form-group">
				<div class="input-group input-daterange">
					<input type="text" name="tgl_awal" id="tgl_awal" class="form-control" readonly placeholder="<?= lang('pj_tgl1') ?>" value = "<?= set_value('tgl1',isset($tgl1) ? $tgl1 : '') ?>" style = "width: 120px" >
					<span class="input-group-addon">to</span>
					<input type="text" name="tgl_akhir" id="tgl_akhir" class="form-control" readonly placeholder="<?= lang('pj_tgl2') ?>" value = "<?= set_value('tgl2',isset($tgl2) ? $tgl2 : '') ?>" style = "width: 120px" >
				</div>
			</div>
			<div class="form-group">
				<select class="form-control" name="konsumen" id="konsumen" style="min-width: 230px">
					<option></option>
					<?php
						if(isset($dt_konsumen) && is_array($dt_konsumen) && count($dt_konsumen)):
							foreach ($dt_konsumen as $key => $val) :
									
					?>
					<option value="<?= $val->id ?>" <?= set_select('konsumen',$val->id, isset($konsumen) && $konsumen == $val->id) ?> ><?= ucwords($val->nm) ?></option>
					<?php
							endforeach;
						endif;
					?>
				</select>
			</div>
			<div class="form-group">
				<input type="text" name="table_search" id="table_search" class="form-control" placeholder="Pencarian.." value = "<?= set_value('search',isset($search) ? $search : '') ?>" >
			</div>
			<div class="form-group">
				<button class="btn btn-default" name="btn_cari" id="btn_cari">
				<span class="fa fa-search"></span>
				</button>
			</div>
		</div>
	</div>
	
	<?php if(isset($data) && is_array($data) && count($data)):  ?>
	<div class="box-body table-responsive">
		
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th width="50">#</th>
					<th><?= lang('pj_kode_tran') ?></th>
					<th><?= lang('pj_tgl') ?></th>
					<th><?= lang('pj_konsumen') ?></th>
					<th class="text-center"><?= lang('pj_status') ?></th>
					<th class="text-center"><?= lang('pj_tipe') ?></th>
					<th class="text-center"><?= lang('pj_tgl_tempo') ?></th>
					<th class="text-center"><?= lang('pj_st_lunas') ?></th>
					<th class="text-right"><?= lang('pj_gtotal') ?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php
					$atts = array(
	                    'width'       => 310,
	                    'height'      => 400,
	                    'scrollbars'  => 'yes',
	                    'status'      => 'yes',
	                    'resizable'   => 'yes',
	                    'screenx'     => 100,
	                    'screeny'     => 100,
	                    'window_name' => '_blank',
	                    'class'       => 'text-black',
	                    'data-toggle' => 'tooltip',
	                    'data-placement' => 'left',
	                    'title'       => 'Cetak Struk'        
            		);

					foreach ($data as $key => $val) :
						
						$label 	= "";
						switch ($val->st) {
							case 0:
								$label 	= "<span class='label label-success'>$val->st_konsumen</span>";
								break;
							case 1:
								$label 	= "<span class='label label-primary'>$val->st_konsumen</span>";
								break;
							case 2:
								$label 	= "<span class='label label-danger'>$val->st_konsumen</span>";
								break;
							default:
								$label 	= "-";
								break;
						}

						$st_tran = "";
						switch ($val->tipe) {
							case 1:
								$st_tran = "<span class='label label-primary'>".lang('pj_cash')."</span>";
								break;
							case 2:
								$st_tran = "<span class='label label-danger'>".lang('pj_tempo')."</span>";
								break;
							default:
								$st_tran = "-";
								break;
						}

						$st_lunas = "";
						switch ($val->st_lunas) {
							case 0:
								$st_lunas = "<span class='label label-danger'>".lang('pj_bl_lunas')."</span>";
								break;
							case 1:
								$st_lunas = "<span class='label label-success'>".lang('pj_lunas')."</span>";
								break;
							default:
								$st_lunas = "-";
								break;
						}
				?>
				<tr>
					<td><?= $numb; ?></td>
					<td><?= $val->kode_tran ?></td>
					<td><?= date('d/m/Y', strtotime($val->tgl_tran)) ?></td>
					<td>
						<strong><?= ucwords($val->panggilan)." ". ucwords($val->nama) ?></strong>
					</td>
					<td class="text-center" ><?= $label ?></td>
					<td class="text-center" ><?= $st_tran ?></td>
					<td class="text-center" ><?= $val->tgl_tempo ? date('d/m/Y', strtotime($val->tgl_tempo)) : '-' ?></td>
					<td class="text-center" ><?= $st_lunas ?></td>
					<td class="text-right"><?= number_format($val->grand_total) ?></td>
					<td>
						<?= anchor_popup('penjualan/cetak_nota/'.$val->id_penjualan, "<i class='fa fa-print'></i>", $atts); ?>
                        |
                        <a class="text-danger" href="<?= site_url('penjualan/pelunasan/' . $val->id_penjualan); ?>" data-toggle="tooltip" data-placement="left" title="Pelunasan Transaksi"><i class="fa fa-money"></i></a> 
                        |
                        <a class="text-black" href="<?= site_url('penjualan/view/' . $val->id_penjualan); ?>" data-toggle="tooltip" data-placement="left" title="View Detail"><i class="fa fa-folder"></i></a>
					</td>
				</tr>
				<?php
					$numb++;
					endforeach;
				?>
			</tbody>
		</table>
	</div>
	<div class="box-footer clearfix">
		<?= $this->pagination->create_links(); ?>
	</div>
	<?php else: ?>
	<div class="alert alert-info" role="alert">
		<p><i class="fa fa-warning"></i> &nbsp; <?= lang('pj_no_records_found') ?></p>
	</div>
	<?php endif;?>
	<?= form_close() ?>
</div>