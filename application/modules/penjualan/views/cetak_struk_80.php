<!-- header struk -->
<table class="struk-kecil">
	<thead>
		<tr>
			<th colspan="3">
				<img src="<?= base_url('assets/images/report/'.$idt->report) ?>">
			</th>
		</tr>
		<tr>
			<th colspan="3"><?= $idt->alamat ?>, <?= $idt->kota ?></th>
		</tr>
		<tr>
			<th colspan="3">Telp. <?= $idt->no_telp ?></th>
		</tr>
		<tr class="no-tran"></tr>
		<tr class="header-info">
			<th>No. Tran</th>
			<th>:</th>
			<th><?= $tran->kode_tran ?></th>
		</tr>
		<tr class="header-info">
			<th>Waktu</th>
			<th>:</th>
			<th><?= date('d/m/Y H:i:s', strtotime($tran->created_on)) ?></th>
		</tr>
		<tr class="header-info">
			<th>Konsumen</th>
			<th>:</th>
			<th><?= $tran->nm_konsumen ?></th>
		</tr>
		<tr class="header-info">
			<th>Telp</th>
			<th>:</th>
			<th><?= $tran->telp ?></th>
		</tr>
		<tr class="no-tran"></tr>
	</thead>
</table>

<!-- isi dari struk -->
<table class="struk-kecil">
	<thead>
		<tr>
			<td>Qty</td>
			<td>Product</td>
			<td>Total</td>
		</tr>
	</thead>
	<tbody>
		<?php 
			$total_diskon = 0;
			$total_no_diskon = 0;
			foreach ($detail as $key => $val): 
				$total_diskon 	 += $val->nilai_diskon;
				$total_no_diskon += $val->total + $val->nilai_diskon;
		?>
		<tr>
			<td colspan="3">
				<b>
					<?= $val->qty.' '.$val->satuan_besar.' x '.$val->nm_barang ?>
				</b>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				[ <?= number_format($val->harga) ?> / <?= $val->alias ?> ]
			</td>
			<td><?= number_format($val->total+$val->nilai_diskon) ?></td>
		</tr>

		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="2">Total <span>:</span></td>
			<td><?= number_format($total_no_diskon) ?></td>
		</tr>
		<?php if($total_diskon > 0): ?>
		<tr>
			<td colspan="2">Total Diskon <span>:</span></td>
			<td><?= "(".number_format($total_diskon).")" ?></td>
		</tr>
		<?php endif ?>		
		<tr>
			<td colspan="2">Grand Total <span>:</span></td>
			<td><?= number_format($tran->grand_total) ?></td>
		</tr>
		<?php if($dp > 0): ?>
		<tr>
			<td colspan="2">DP <span>:</span></td>
			<td><?= number_format($dp) ?></td>
		</tr>
		<?php endif; ?>
		<tr>
			<td colspan="2">Bayar <span>:</span></td>
			<td><?= number_format($bayar) ?></td>
		</tr>
		<tr>
			<td colspan="2">Krg. Bayar <span>:</span></td>
			<td><?= number_format($krg_bayar) ?></td>
		</tr>
		<tr>
			<td colspan="2">Tempo <span>:</span></td>
			<td>
				<?php 
						if($tran->st_lunas == 1) {

							echo "Lunas";

						}else{

							echo $tran->tgl_tempo != '' ? date('d/m/Y', strtotime($tran->tgl_tempo)) : '';

						}

				?>
			</td>
		</tr>		
		<tr>
			<td colspan="2">Kembali <span>:</span></td>
			<td><?= number_format($kembalian) ?></td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<?php if($saldo_deposit !== false) :?>
		<tr class="no-tran"></tr>
		<tr>
			<td colspan="3" class="deposit-info">
				Penggunaan deposit pada tran. ini : <strong>Rp. <?= isset($deposit_terpakai) && $deposit_terpakai > 0 ? number_format($deposit_terpakai) : '-' ?></strong><br>
				Saldo deposit anda saat ini : <strong>Rp. <?= isset($saldo_deposit) && $saldo_deposit > 0 ? number_format($saldo_deposit) : '-' ?></strong>
			</td>
		</tr>
		<tr class="no-tran"></tr>
		<?php endif; ?>
		<tr>
			<td colspan="3" class="footer-note">===== Terima Kasih =====</td>
		</tr>
	</tfoot>


</table>

<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function(e){
		window.print();
		window.close();
	});
</script>