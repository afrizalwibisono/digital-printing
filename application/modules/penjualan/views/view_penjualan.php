<?php
	$ENABLE_DELETE = has_permission('Penjualan.Delete');
?>
<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_view_penjualan','name'=>'frm_view_penjualan','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body frm-penjualan">
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label" for="kode_tran" ><?= lang('pj_kode_tran') ?></label>
					<span class="form-control"><?= $tran->kode_tran ?></span>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label" for="idkonsumen" ><?= lang('pj_konsumen') ?></label>
					<span class="form-control"><?= $tran->nm_konsumen ?></span>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label class="control-label" for="tgl_tran" ><?= lang('pj_tgl') ?></label>
					<div class="input-group">
						<span class="form-control"><?= date('d/m/Y', strtotime($tran->tgl_tran)) ?></span>
						<div class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</div>
					</div>
				</div>
			</div>

			<div class="clearfix"></div>

		  	<div class="table-responsive">
		  		<table class="table table-bordered" id="tjual">
		  			<thead>
		  				<tr class="success">
			  				<th width="50">#</th>
			  				<th><?= lang('pj_barcode') ?></th>
			  				<th><?= lang('pj_nm_br') ?></th>
			  				<th><?= lang('pj_qty') ?></th>
			  				<th><?= lang('pj_satuan') ?></th>
			  				<th><?= lang('pj_harga') ?></th>
			  				<th><?= lang('pj_diskon') ?></th>
			  				<th style="min-width: 200px;text-align: right;"><?= lang('pj_sub_total') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($detail): 
		  					foreach ($detail as $key => $val) :
		  				?>
		  				<tr>
		  					<td><?= $key+1 ?></td>
		  					<td>
		  						<?= $val->barcode ?>
		  					</td>
		  					<td><?= $val->nm_barang ?></td>
		  					<td><?= $val->qty ?></td>
		  					<td><?= $val->satuan_besar ?></td>
	  						<td align='right'><?= number_format($val->harga) ?></td>
		  					<td align='right'><?= $val->diskon ?>%</td>
		  					<td align='right'><?= number_format($val->total) ?></td>
				  		</tr>
		  				<?php endforeach;endif; ?>
		  			</tbody>
		  			<tfoot class="bg-success">
						<tr class="bg-primary text-right">
							<td colspan="7"><strong><?= lang('pj_gtotal') ?></strong></td>
							<td><strong><?= number_format($tran->grand_total) ?></strong></td>
						</tr>
						<?php if($deposit_terpakai > 0): ?>
						<tr class="bg-green text-right">
							<td colspan="7"><strong><?= lang('pj_bayar_deposit') ?></strong></td>
							<td><strong><?= number_format($deposit_terpakai) ?></strong></td>
						</tr>
						<?php endif; ?>
						<?php if($tunai > 0): ?>
						<tr class="bg-green text-right">
							<td colspan="7"><strong><?= lang('pj_bayar_tunai') ?></strong></td>
							<td><strong><?= number_format($tunai) ?></strong></td>
						</tr>
						<?php endif; ?>
						<tr class="bg-red-active text-right">
							<td colspan="7"><strong><?= lang('pj_kurang_bayar') ?></strong></td>
							<td><strong><?= number_format($tran->kurang_bayar) ?></strong></td>
						</tr>
						<?php if($tran->tgl_tempo): ?>
						<tr class="bg-primary text-right">
							<td colspan="7"><strong><?= lang('pj_tgl_tempo') ?></strong></td>
							<td>
								<div class="input-group">
									<span class="form-control"><?= $tran->tgl_tempo ? date('d/m/Y', strtotime($tran->tgl_tempo)) : '' ?></span>
									<div class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</div>
								</div>
							</td>
						</tr>
						<?php endif; ?>
					</tfoot>
		  		</table>
		  	</div>
		  	<div class="form-group">
			    <div class="col-md-12">
			    	<label class="label label-success"><?= lang('pj_lampiran') ?></label>
			    	<div class="clearfix" style="margin-top: 10px;"></div>
		    		<?php if($lampiran):
		    			foreach ($lampiran as $key => $val) :
		    				$arr = explode("/", $val->file);
		    				$file = $arr[count($arr)-1];
		    		?>
		    		<div class="col-md-3 col-sm-6 col-xs-6">
						<div class="thumbnail">
					        <img src="<?= base_url($val->file) ?>" style="width:100%">
					        <div class="caption">
					        	<p><?= $file ?></p>
					        </div>
					    </div>
					</div>
		    		<?php endforeach;endif; ?>

		    		<div class="pull-right">
			    		<?php
		                	echo anchor('penjualan', lang('btn_back'), ['class' => 'btn btn-success'])
		                ?>
		                <?php if($ENABLE_DELETE): ?>
			    		<button type="submit" name="delete" class="btn btn-danger" onclick="return confirm('<?= lang('pj_delete_confirm') ?>')"><?= lang('btn_delete') ?></button>
			    		<?php endif; ?>
			    	</div>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->