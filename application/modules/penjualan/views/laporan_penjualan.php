<div class="box box-primary">
	
	<?php echo form_open($this->uri->uri_string(),	[
														'name' 	=> 'frm_laporan',
														'id'	=> 'frm_laporan',
														'role'	=> 'form',
														'class'	=> 'form-inline'
								
													]) ?>

	<div class="box-header hidden-print">
		
		<div class="pull-right">
			
			<div class="form-group">
				<div class="input-group input-daterange">
					<input type="text" name="tgl1" id="tgl1" style="width: 120px" class="form-control" placeholder="<?= lang("tgl_awal") ?>" value="<?= set_value('tgl1',isset($filter['tgl1']) ? $filter['tgl1'] : '' ) ?>" >
					<span class="input-group-addon">to</span>
					<input type="text" name="tgl2" id="tgl2" style="width: 120px" class="form-control" placeholder="<?= lang("tgl_akhir") ?>" value="<?= set_value('tgl2',isset($filter['tgl2']) ? $filter['tgl2'] : '' ) ?>">
				</div>
			</div>

			<div class="form-group">
				<select class="form-control" name="st_lunas" id="st_lunas" style="min-width: 120px">
					<option></option>
					<option value="0" <?= set_select('st_lunas','0',isset($filter['st_lunas']) && $filter['st_lunas'] === 0 ) ?> >Hutang</option>
					<option value="1" <?= set_select('st_lunas','1',isset($filter['st_lunas']) && $filter['st_lunas'] === 1 ) ?> >Lunas</option>
					
				</select>
			</div>

			<div class="form-group">
				<select class="form-control" name="gol_konsumen" id="gol_konsumen" style="min-width: 120px">
					<option></option>
					<option value="0" <?= set_select('gol_konsumen','0',isset($filter['gol_kons']) && $filter['gol_kons'] === 0 ) ?> >Konsumen</option>
					<option value="1" <?= set_select('gol_konsumen','1',isset($filter['gol_kons']) && $filter['gol_kons'] === 1 ) ?> >Reseller</option>
					<option value="2" <?= set_select('gol_konsumen','2',isset($filter['gol_kons']) && $filter['gol_kons'] === 2 ) ?> >Instansi</option>
				</select>
			</div>

			<div class="form-group">
				<input type="hidden" name="id_konsumen_pilih" id="id_konsumen_pilih" value="<?= set_value('konsumen',isset($filter['konsumen']) ? $filter['konsumen'] : '' ) ?>" >
				<select class="form-control" name="konsumen" id="konsumen" style="min-width: 200px">
					<option></option>
				</select>
			</div>

		</div>


		<div class="clearfix"></div>
		<hr style="margin-top: 8px;margin-bottom: 5px">
		
		<div class="pull-left">
			<button type="button" id="cetak" class="btn btn-default" onclick="cetak_halaman()"><i class="fa fa-print"></i> Cetak</button>
		</div>
		<div class="pull-right">
			<button type="submit" id="cari" name="cari" class="btn btn-default"><i class="fa fa-search"></i> Filter</button>
		</div>

	</div>

	<?php echo form_close() ?>

	<?php if($idt) : ?>
    <div class="row header-laporan">
        <div class="col-md-12">
            <div class="col-md-1 laporan-logo">
                <img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo">
            </div>
            <div class="col-md-6 laporan-identitas">
                <address>
                    <h4><?= $idt->nm_perusahaan ?></h4>
                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?>
                </address>
            </div>
            <div class="col-md-5 text-right">
                <h3><?= lang("lap-jual-jdl") ?></h3>
                <h5>Periode Cetak <strong><?= $filter["tgl1"]." - ".$filter["tgl2"] ?></strong></h5>
            </div>
            <div class="clearfix"></div>
            <div class="laporan-garis"></div>
        </div>
    </div>
    <?php endif ?>

    <?php if(isset($data) && is_array($data) && count($data)) : ?>

	<div class="box-body laporan-body">

		<h4><?= lang('lap-jual-resume') ?></h4>
    	<table class="table-condensed table-summary">
    		<tbody>
    			<tr>
    				<td width="200"><?= lang("lap-jual-jml-x-trans") ?></td>
    				<td width="10">:</td>
    				<td><strong> <?= number_format($jml_transaksi). ' x(kali)' ?></strong></td>
    			</tr>
    			<tr>
    				<td width="200"><?= lang("lap-jual-total-trans") ?></td>
    				<td width="10">:</td>
    				<td> <strong><?= number_format($total_transaksi) ?></strong></td>
    			</tr>
    		</tbody>
    	</table>

    	<h4><?= lang('lap-jual-detail') ?></h4>
    	<?php foreach ($data as $key => $isi): ?>
    		
    		<div class="table-responsive no-padding">
		    	<table class="table-condensed table-head">
		    		<tr>
		    			<td><?= lang("lap-jual-head-nonota") ?></td>
		    			<td>:</td>
		    			<td><strong><?= $isi['no_trans'] ?></strong></td>
		    			<td><?= lang("lap-jual-head-waktu") ?></td>
		    			<td>:</td>
		    			<td><strong><?= $isi['waktu'] ?></strong></td>
		    			<td><?= lang("lap-jual-head-konsumen") ?></td>
		    			<td>:</td>
		    			<td>
		    				<strong><?= $isi['konsumen']." | ".$isi['gol_kons'] ?></strong>
		    			</td>
		    			<td><?= lang("lap-jual-head-status") ?></td>
		    			<td>:</td>
		    			<td>
		    				<?php  
		    						$status 	= $isi['status'] == 1 ? "Cash" : "Tempo";
		    				?>
		    				<strong><?= $status ?></strong>
		    			</td>
		    			<td><?= lang("lap-jual-head-st-bayar") ?></td>
		    			<td>:</td>
		    			<td>
		    				<?php
		    						$lunas 	= $isi['st_lunas'] == 0 ? "Hutang" : "Lunas";
		    				?>
		    				<strong><?= $lunas ?></strong>
		    			</td>
		    			<td><?= lang("lap-jual-head-op") ?></td>
		    			<td>:</td>
		    			<td><strong><?= $isi['op'] ?></strong></td>
		    		</tr>
		    	</table>
		    </div>

		    <div class="table-responsive no-padding">
		    	<table class="table table-condensed table-bordered table-detail">
		    		<thead>
		    			<tr class="success">
		    				<th class="text-center" width="10">#</th>
		    				<th class="text-center"><?= lang("lap-jual-det-barang") ?></th>
		    				<th class="text-center"><?= lang("lap-jual-det-jml") ?></th>
		    				<th class="text-center"><?= lang("lap-jual-det-harga") ?></th>
		    				<th class="text-center"><?= lang("lap-jual-det-diskon") ?></th>
		    				<th class="text-center"><?= lang("lap-jual-det-total") ?></th>
		    			</tr>
		    		</thead>
		    		<tbody>
		    			<?php foreach ($isi['detail'] as $keydet => $det) : ?>
		    			<tr>
		    				<td class="text-right"><?= $keydet+1 ?></td>
		    				<td><?= $det['nm_barang'] ?></td>
		    				<td class="text-center"><?= $det['qty'] ?></td>
		    				<td class="text-center"><?= number_format($det['harga']) ?></td>
		    				<td class="text-center"><?= $det['diskon'] ?></td>
		    				<td class="text-right">
		    					<strong>
		    					<?= number_format($det['total']) ?>
		    					</strong>
		    				</td>

		    			</tr>
		    			<?php endforeach ?>	
		    		
		    		
		    			<tr class="border-top">
		    				<td colspan="5" class="text-right">
		    					<strong><?= lang("lap-jual-det-gtotal") ?></strong>
		    				</td>
		    				<td class="text-right">
		    					<strong><?= number_format($isi['gtotal']) ?></strong>
		    				</td>
		    			</tr>
		    			<tr>
		    				<td colspan="5" class="text-right">
		    					<strong><?= lang("lap-jual-det-bayar") ?></strong>
		    				</td>
		    				<td class="text-right">
		    					<strong><?= number_format($isi['bayar']) ?></strong>
		    				</td>
		    			</tr>
		    			<tr>
		    				<td colspan="5" class="text-right">
		    					<strong><?= lang("lap-jual-det-krgbayar") ?></strong>
		    				</td>
		    				<td class="text-right">
		    					<strong><?= number_format($isi['krg_bayar']) ?></strong>
		    				</td>
		    			</tr>
		    		</tbody>
		    	</table>
		    </div>	

    	<?php endforeach ?>

	</div>

	<?php else : ?>
		<div class="alert alert-info" role="alert">
	        <p><i class=fa fa-warning"></i> &nbsp; <?= lang('pj_no_records_found') ?></p>
	    </div>
	<?php endif; ?>	

</div>