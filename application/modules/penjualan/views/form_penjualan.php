<div class="box box-primary">
    <!-- form start -->
    <?= form_open_multipart($this->uri->uri_string(),array('id'=>'frm_penjualan','name'=>'frm_penjualan','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body frm-penjualan">
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label" for="kode_tran" ><?= lang('pj_kode_tran') ?></label>
					<input type="text" class="form-control" id="kode_tran" name="kode_tran" value="<?= set_value('kode_tran', $dt_input['kode_tran']) ?>">
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label" for="idkonsumen" ><?= lang('pj_konsumen') ?></label>
					<div class="input-group">
						<select class="form-control" name="idkonsumen" id="idkonsumen" style="min-width: 230px">
							<option></option>
							<?php
								if(isset($dt_konsumen) && is_array($dt_konsumen) && count($dt_konsumen)):
									foreach ($dt_konsumen as $key => $val) :
											
							?>
							<option value="<?= $val->id ?>" <?= set_select('idkonsumen',$val->id, isset($dt_input['idkonsumen']) && $dt_input['idkonsumen'] == $val->id) ?> ><?= ucwords($val->nm) ?></option>
							<?php
									endforeach;
								endif;
							?>
						</select>
						<span class="input-group-btn">
                            <a class="btn btn-info btn-flat" target="_blank" href="<?= site_url("konsumen/create") ?>"><i class="fa fa-plus"></i></a>
                        </span>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label class="control-label" for="tgl_tran" ><?= lang('pj_tgl') ?></label>
					<div class="input-group">
						<input type="text" class="form-control" id="tgl_tran" name="tgl_tran" value="<?= set_value('tgl_tran', $dt_input['tgl_tran']) ?>" placeholder="dd/mm/yyyy">
						<div class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</div>
					</div>
				</div>
			</div>

			<div class="clearfix"></div>

		  	<div class="table-responsive">
		  		<table class="table table-bordered" id="tjual">
		  			<thead>
		  				<tr class="success">
			  				<th width="50">#</th>
			  				<th><?= lang('pj_barcode') ?></th>
			  				<th><?= lang('pj_nm_br') ?></th>
			  				<th><?= lang('pj_qty') ?></th>
			  				<th><?= lang('pj_satuan') ?></th>
			  				<th><?= lang('pj_harga') ?></th>
			  				<th><?= lang('pj_diskon') ?></th>
			  				<th style="min-width: 200px;text-align: right;"><?= lang('pj_sub_total') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($dt_input['detail']): 
		  					foreach ($dt_input['detail'] as $key => $val) :
		  				?>
		  				<tr>
		  					<td><?= $key+1 ?></td>
		  					<td>
		  						<?= $val['barcode'] ?>
		  						<input type='hidden' name='id_barang[]' value="<?= $val['id_barang'] ?>" />
		  					</td>
		  					<td><?= $val['nm_barang'] ?> - <a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>
		  					<td><input type='number' min='1' name='qty[]' class='form-control' value="<?= $val['qty'] ?>" /></td>
		  					<td>
		  						<select class='form-control' name='konversi[]' style='width: 200px;'>
		  							<?php foreach ($val['konversi'] as $key2 => $konv) : ?>
		  							<option data-qty="<?= $konv->jml_kecil ?>" value="<?= $konv->id_konversi ?>" <?= $konv->selected ==1 ? 'selected=""' : '' ?>><?= $konv->tampil2 ?></option>
		  							<?php endforeach; ?>
		  						</select>
		  					</td>
	  						<td align='right'><span class='harga'><?= number_format($val['harga']) ?></span></td>
		  					<td align='right'><span class='diskon'><?= $val['diskon'] ?></span>%</td>
		  					<td align='right'><span class='sub_total' data-sub_total="<?= $val['subtotal'] ?>"><?= number_format($val['subtotal']) ?></span></td>
				  		</tr>
		  				<?php endforeach;else: ?>
		  				<tr id="tjual-placeholder">
		  					<td colspan="8">Belum ada item yang ditambahkan</td>
		  				</tr>
		  				<?php endif; ?>
		  			</tbody>
		  			<tfoot class="bg-success">
		  				<tr>
		  					<td colspan="8">
		  						<div class="input-group" id="barcode_input">
		  							<div class="input-group-btn">
		  								<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-barang"><i class="fa fa-plus-square"></i></button>
		  							</div>
		  							<input type="text" class="form-control" id="barcode" name="barcode" placeholder="<?= lang('pj_scan_brg') ?>" autocomplete="off" autofocus />
		  						</div>
		  						<span class="help-block text-green" id="barcode_msg"></span>
		  					</td>
		  				</tr>
						<tr class="bg-primary text-right">
							<td colspan="7"><strong><?= lang('pj_gtotal') ?></strong></td>
							<td><strong><span id="gtotal" data-gtotal="<?= $dt_input['gtotal'] ?>"><?= number_format($dt_input['gtotal']) ?></span></strong></td>
						</tr>
						<tr class="bg-green text-right tr-deposit" style="display: none">
							<td colspan="7">
								<div class="pull-left" id="deposit-msg"></div>
								<strong><?= lang('pj_bayar_deposit') ?></strong>
							</td>
							<td>
								<strong><span id="deposit-terpakai"></span></strong>
							</td>
						</tr>
						<tr class="bg-red-active text-right">
							<td colspan="7"><strong><?= lang('pj_kurang_bayar') ?></strong></td>
							<td><strong><span id="kurang-bayar"></span></strong></td>
						</tr>
						<tr class="text-right tr-metode-bayar">
							<td colspan="7">
								<strong><?= lang('pj_metode_bayar') ?></strong>
							</td>
							<td>
								<select class="form-control" name="metode_bayar" id="metode_bayar">
									<option value="1" <?= set_select('metode_bayar', 1, isset($dt_input['metode_bayar']) && $dt_input['metode_bayar'] == 1) ?>><?= lang('pj_cash') ?></option>
									<option value="2" <?= set_select('metode_bayar', 2, isset($dt_input['metode_bayar']) && $dt_input['metode_bayar'] == 2) ?>><?= lang('pj_debit') ?></option>
									<option value="3" <?= set_select('metode_bayar', 3, isset($dt_input['metode_bayar']) && $dt_input['metode_bayar'] == 3) ?>><?= lang('pj_transfer') ?></option>
								</select>
								<div class="clearfix"></div>
								<div id="lampiran_transfer" style="display: none">
									<input type="file" name="file_att" id="file_att" accept=".jpeg, .jpg" style="display: none">

									<div class="pull-left" style="margin-bottom: 2px; margin-top: 2px">
										<button style="vertical-align: top !important;" class="pull-left btn btn-sm btn-default" type="button" onclick="$('#file_att').click()">
											<i class="fa fa-paperclip"></i>
										</button>	
									</div>
									<div class="clearfix"></div>
									<p style="margin-bottom: 0px !important"><strong class="small pull-left" id="nm_file" ></strong></p>
									<div class="clearfix"></div>
									<!-- area gambar -->
									<div class="file-gambar pull-left" style="margin-bottom: 0px;display: none" id="tampil_lampiran">
										<img src="" id="thumb" style="max-width: 200px;">
									</div>
									<!-- akhir area gambar -->
									<div class="clearfix"></div>
									<p class="pull-left small" style="margin-bottom: 2px; margin-top: 2px">
										file : *.jpg | max : 200KB
									</p>


								</div>
							</td>
						</tr>
						<tr class="text-right tr-dp">
							<td colspan="7"><strong><?= lang('pj_dp') ?></strong></td>
							<td><input type="text" class="form-control" style="text-align: right;" id="dp" name="dp" value="<?= set_value('dp', $dt_input['dp']) ?>"></td>
						</tr>
						<tr class="bg-primary text-right tr-bayar">
							<td colspan="7"><strong><?= lang('pj_bayar') ?></strong></td>
							<td><input type="text" class="form-control" style="text-align: right;" id="bayar" name="bayar" value="<?= set_value('bayar', $dt_input['bayar']) ?>"></td>
						</tr>
						<tr class="bg-primary text-right tr-tgl-tempo">
							<td colspan="7"><strong><?= lang('pj_tgl_tempo') ?></strong></td>
							<td>
								<div class="input-group">
									<input type="text" class="form-control" id="tgl_tempo" name="tgl_tempo" value="<?= set_value('tgl_tempo', $dt_input['tgl_tempo']) ?>" placeholder="dd/mm/yyyy">
									<div class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</div>
								</div>
							</td>
						</tr>
						<tr class="bg-green text-right">
							<td colspan="7"><strong><?= lang('pj_kembalian') ?></strong></td>
							<td><strong><span id="kembalian"></span></strong></td>
						</tr>
					</tfoot>
		  		</table>
		  	</div>
		  	<div class="form-group">
			    <div class="col-md-12">
			    	<div class="pull-right">
			    		<?php
		                	echo anchor('penjualan/cancel', lang('btn_back'), array("onclick" => "return confirm('".lang('pj_cancel_confirm')."')")).' '.lang('bf_or');
		                ?>
		                <button type="submit" name="save" class="btn btn-primary"><?= lang('btn_save') ?></button>	
			    	</div>
			    	
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->
<div class="modal" id="modal-barang" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
	    <div class="modal-content">
	    	<?= form_open("",array('id'=>'frm_barang','name'=>'frm_barang','role'=>'form')) ?>
		      	<div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        	<h4 class="modal-title"><?= lang('pj_pilih_brg') ?></h4>
		      	</div>
		      	<div class="modal-body">
		      		<div class="form-group">
		      			<div class="input-group">
		      				<input type="text" class="form-control" id="cr_barang" name="cr_barang" placeholder="<?= lang('pj_cari_brg') ?>..." />
		      				<div class="input-group-btn">
		      					<button type="button" class="btn btn-primary" name="cari_btn" id="cari_btn"><i class="fa fa-search"></i> <?= lang('btn_cari') ?></button>
		      				</div>
		      			</div>
		      		</div>
		        	<div class="table-responsive">
		        		<table id="tbl_barang" class="table table-hover table-striped">
		        			<thead>
		        				<tr class="success">
			        				<th><input type="checkbox" id="ck_all" name="ck_all" /></th>
			        				<th>#</th>
			        				<th><?= lang('pj_barcode') ?></th>
			        				<th><?= lang('pj_nm_br') ?></th>
			        				<th><?= lang('pj_stok') ?></th>
			        			</tr>
		        			</thead>
		        			<tbody>
		        				
		        			</tbody>
		        		</table>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
		      		<button type="button" class="btn btn-primary" id="pilih_btn" name="pilih_btn"><?= lang('btn_pilih') ?></button>
		      	</div>
	      	<?= form_close() ?>
	    </div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->