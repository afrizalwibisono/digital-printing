<?php
	$ENABLE_DELETE = has_permission('Penjualan.Delete');
?>
<div class="box box-primary">
    <!-- form start -->
    <?= form_open_multipart($this->uri->uri_string(),array('id'=>'frm_view_penjualan','name'=>'frm_view_penjualan','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body frm-penjualan">
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label" for="kode_tran" ><?= lang('pj_kode_tran') ?></label>
					<span class="form-control"><?= $tran->kode_tran ?></span>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label" for="idkonsumen" ><?= lang('pj_konsumen') ?></label>
					<span class="form-control"><?= $tran->nm_konsumen ?></span>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label class="control-label" for="tgl_tran" ><?= lang('pj_tgl') ?></label>
					<div class="input-group">
						<span class="form-control"><?= date('d/m/Y', strtotime($tran->tgl_tran)) ?></span>
						<div class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</div>
					</div>
				</div>
			</div>

			<div class="clearfix"></div>

		  	<div class="table-responsive">
		  		<table class="table table-bordered" id="tjual">
		  			<thead>
		  				<tr class="success">
			  				<th width="50">#</th>
			  				<th><?= lang('pj_barcode') ?></th>
			  				<th><?= lang('pj_nm_br') ?></th>
			  				<th><?= lang('pj_qty') ?></th>
			  				<th><?= lang('pj_satuan') ?></th>
			  				<th><?= lang('pj_harga') ?></th>
			  				<th><?= lang('pj_diskon') ?></th>
			  				<th style="min-width: 200px;text-align: right;"><?= lang('pj_sub_total') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($detail): 
		  					foreach ($detail as $key => $val) :
		  				?>
		  				<tr>
		  					<td><?= $key+1 ?></td>
		  					<td>
		  						<?= $val->barcode ?>
		  					</td>
		  					<td><?= $val->nm_barang ?></td>
		  					<td><?= $val->qty ?></td>
		  					<td><?= $val->satuan_besar ?></td>
	  						<td align='right'><?= number_format($val->harga) ?></td>
		  					<td align='right'><?= $val->diskon ?>%</td>
		  					<td align='right'><?= number_format($val->total) ?></td>
				  		</tr>
		  				<?php endforeach;endif; ?>
		  			</tbody>
		  			<tfoot class="bg-success">
						<tr class="bg-primary text-right">
							<td colspan="7"><strong><?= lang('pj_gtotal') ?></strong></td>
							<td><strong><?= number_format($tran->grand_total) ?></strong></td>
						</tr>
						<tr class="text-right">
							<td colspan="7"><strong><?= lang('pj_dp') ?></strong></td>
							<td><span class="form-control" style="text-align: right;"><?= number_format($tran->dp) ?></span></td>
						</tr>
						<tr class="bg-red-active text-right">
							<td colspan="7"><strong><?= lang('pj_kurang_bayar') ?></strong></td>
							<td><strong><span id="kurang_bayar"><?= number_format($tran->kurang_bayar) ?></span></strong></td>
						</tr>
						<tr class="text-right">
							<td colspan="7"><strong><?= lang('pj_tgl_tempo') ?></strong></td>
							<td>
								<div class="input-group">
									<span class="form-control"><?= $tran->tgl_tempo ? date('d/m/Y', strtotime($tran->tgl_tempo)) : '' ?></span>
									<div class="input-group-addon">
										<span class="fa fa-calendar"></span>
									</div>
								</div>
							</td>
						</tr>
						<tr class="bg-primary text-right" <?= $tran->kurang_bayar > 0 ? '' : 'style="display: none;"' ?>>
							<td colspan="7"><strong><?= lang('pj_metode_bayar') ?></strong></td>
							<td>
								<select class="form-control" name="metode_bayar" id="metode_bayar">
									<option value="1" <?= set_select('metode_bayar', 1, isset($dt_input['idkonsumen']) && $dt_input['idkonsumen'] == 1) ?>><?= lang('pj_cash') ?></option>
									<option value="2" <?= set_select('metode_bayar', 2, isset($dt_input['idkonsumen']) && $dt_input['idkonsumen'] == 2) ?>><?= lang('pj_debit') ?></option>
									<option value="3" <?= set_select('metode_bayar', 3, isset($dt_input['idkonsumen']) && $dt_input['idkonsumen'] == 3) ?>><?= lang('pj_transfer') ?></option>
								</select>
								<div class="clearfix"></div>
								<div id="lampiran_transfer" style="display: none">
									<input type="file" name="file_att" id="file_att" accept=".jpeg, .jpg" style="display: none">

									<div class="pull-left" style="margin-bottom: 2px; margin-top: 2px">
										<button style="vertical-align: top !important;" class="pull-left btn btn-sm btn-default" type="button" onclick="$('#file_att').click()">
											<i class="fa fa-paperclip"></i>
										</button>	
									</div>
									<div class="clearfix"></div>
									<p style="margin-bottom: 0px !important"><strong class="small pull-left" id="nm_file" ></strong></p>
									<div class="clearfix"></div>
									<!-- area gambar -->
									<div class="file-gambar pull-left" style="margin-bottom: 0px;display: none" id="tampil_lampiran">
										<img src="" id="thumb" style="max-width: 200px;">
									</div>
									<!-- akhir area gambar -->
									<div class="clearfix"></div>
									<p class="pull-left small" style="margin-bottom: 2px; margin-top: 2px">
										file : *.jpg | max : 200KB
									</p>


								</div>
							</td>
						</tr>
						<tr class="bg-primary text-right" <?= $tran->kurang_bayar > 0 ? '' : 'style="display: none;"' ?>>
							<td colspan="7"><strong><?= lang('pj_bayar') ?></strong></td>
							<td><input type="text" class="form-control" style="text-align: right;" id="bayar" name="bayar" value="<?= set_value('bayar') ?>"></td>
						</tr>
						<tr class="bg-green text-right" <?= $tran->kurang_bayar > 0 ? '' : 'style="display: none;"' ?>>
							<td colspan="7"><strong><?= lang('pj_kembalian') ?></strong></td>
							<td><strong><span id="kembalian"></span></strong></td>
						</tr>
					</tfoot>
		  		</table>
		  	</div>
		  	<div class="form-group">
			    <div class="col-md-12">
			    	<div class="pull-right">
			    		<?php
		                	echo anchor('penjualan', lang('btn_back'), ['class' => 'btn btn-success'])
		                ?>
		                <?php if($tran->kurang_bayar > 0) : ?>
		                <button type="submit" name="save" class="btn btn-primary"><?= lang('btn_save') ?></button>	
			    		<?php endif; ?>
			    	</div>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->