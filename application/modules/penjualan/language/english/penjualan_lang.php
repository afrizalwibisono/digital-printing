<?php defined('BASEPATH')|| exit('No direct script access allowed');

// =======================================================================================
// lang untuk laporan penjualan barang

$lang['lap-jual-title']				= "Laporan Penjualan Barang";
$lang['lap-jual-jdl']				= "Laporan Penjualan Barang";

$lang['lap-jual-resume']			= "Resume";
$lang['lap-jual-jml-x-trans']		= "Jumlah Transaksi";
$lang['lap-jual-total-trans']		= "Total Transaksi";

$lang['lap-jual-detail']			= "Detail Transaksi";

$lang['lap-jual-head-nonota']		= "No. Trans";
$lang['lap-jual-head-konsumen']		= "Konsumen";
$lang['lap-jual-head-waktu']		= "Waktu";
$lang['lap-jual-head-status']		= "Status";
$lang['lap-jual-head-st-bayar']		= "Status Bayar";
$lang['lap-jual-head-op']			= "Operator";

$lang['lap-jual-det-barang']		= "Barang";
$lang['lap-jual-det-jml']			= "Qty";
$lang['lap-jual-det-harga']			= "Harga";
$lang['lap-jual-det-diskon']		= "Diskon(%)";
$lang['lap-jual-det-total']			= "Total";
$lang['lap-jual-det-gtotal']		= "Grand Total";
$lang['lap-jual-det-bayar']			= "Total Bayar";
$lang['lap-jual-det-krgbayar']		= "Kurang Bayar";

// =======================================================================================
// lang untuk laporan piutang penjualan barang
$lang['lap-piutang-title']			= "Laporan Piutang Penjualan Barang";
$lang['lap-piutang-jdl']			= "Laporan Piutang Penjualan Barang";

$lang['lap-piutang-resume']			= "Resume";
$lang['lap-piutang-jml-x-trans']	= "Jumlah Transaksi";
$lang['lap-piutang-total']			= "Total Piutang";

$lang['lap-piutang-detail']			= "Detail Transaksi";

$lang['lap-piutang-nonota']			= "No. Trans";
$lang['lap-piutang-konsumen']		= "Konsumen";
$lang['lap-piutang-waktu']			= "Waktu";
$lang['lap-piutang-total']			= "Total Transaksi";
$lang['lap-piutang-bayar']			= "Total Bayar";
$lang['lap-piutang-krgbayar']		= "Kurang Bayang";

// =======================================================================================

$lang['pj_title']		= 'Transaksi Penjualan';
$lang['pj_new']			= 'Transaksi Penjualan Baru';
$lang['pj_view']		= 'Detail Transaksi Penjualan';
$lang['pj_pelunasan']	= 'Pelunasan Transaksi Penjualan';

$lang['pj_kode_tran']	= 'Kode Transaksi';
$lang['pj_tgl']			= 'Tgl. Transaksi';
$lang['pj_tipe']		= 'Tipe';
$lang['pj_tempo'] 		= 'Tempo';
$lang['pj_metode_bayar']= 'Metode Pembayaran';
$lang['pj_cash']  		= 'Cash/Tunai';
$lang['pj_debit']  		= 'Debit';
$lang['pj_transfer']  	= 'Transfer';
$lang['pj_deposit']  	= 'Deposit';
$lang['pj_tgl_tempo']	= 'Jatuh Tempo';
$lang['pj_sub_total'] 	= 'Sub Total';
$lang['pj_diskon'] 		= 'Diskon';
$lang['pj_potongan'] 	= 'Potongan';
$lang['pj_gtotal'] 		= 'Grand Total';
$lang['pj_pajak'] 		= 'Pajak';
$lang['pj_dp'] 			= 'Nilai DP';
$lang['pj_bayar'] 		= 'Nominal Bayar';
$lang['pj_kurang_bayar']= 'Kurang Bayar';
$lang['pj_kembalian'] 	= 'Kembalian';
$lang['pj_catatan'] 	= 'Catatan';
$lang['pj_barcode'] 	= 'Barcode';
$lang['pj_nm_br'] 		= 'Nama Barang';
$lang['pj_qty'] 		= 'Qty';
$lang['pj_harga'] 		= 'Harga';
$lang['pj_satuan'] 		= 'Satuan';
$lang['pj_stok'] 		= 'Stok';
$lang['pj_konsumen'] 	= 'Konsumen';
$lang['pj_status'] 		= 'Status';
$lang['pj_scan_brg']  	= 'Ketik / Scan Barcode Barang';
$lang['pj_pilih_brg']  	= 'Pilih Barang';
$lang['pj_cari_brg']  	= 'Cari Barang';
$lang['pj_terbayar']  	= 'Total Terbayar';
$lang['pj_st_lunas']  	= 'St. Lunas';
$lang['pj_lunas']  		= 'Lunas';
$lang['pj_bl_lunas']  	= 'Belum Lunas';
$lang['pj_lampiran']  	= 'Lampiran';
$lang['pj_bayar_deposit'] = 'Pembayaran dengan Deposit';
$lang['pj_bayar_tunai'] = 'Pembayaran dengan Cash/Transfer';

$lang['pj_tgl1']		= 'Tanggal Awal';
$lang['pj_tgl2']		= 'Tanggal Akhir';

//==============================
$lang['btn_create']				= 'Baru';
$lang['btn_delete']				= 'Hapus Transaksi';
$lang['btn_filter']				= 'Filter';
$lang['btn_back']				= 'Batal / Kembali';
$lang['btn_save']				= 'Simpan';
$lang['bf_or']					= "atau";
$lang['btn_cari']				= "Cari";
$lang['btn_pilih']				= "Pilih";

// =============================
$lang['label_simpan_sukses'] = "Simpan Transaksi Penjualan baru sukses";
$lang['label_simpan_gagal']  = "Simpan Transaksi Penjualan baru gagal";
$lang['label_cancel_sukses'] = "Transaksi Penjualan telah berhasil dibatalkan";

// Messages
$lang['pj_del_failure']		= 'Tidak dapat menghapus Transaksi Penjualan: ';
$lang['pj_delete_confirm']	= 'Apakah anda yakin akan menghapus Transaksi Penjualan ini ?';
$lang['pj_cancel_confirm']	= 'Apakah anda yakin akan membatalkan Transaksi Penjualan ?';
$lang['pj_deleted']		  	= 'Transaksi Penjualan berhasil dihapus';
$lang['pj_no_records_found']= 'Data tidak ditemukan.';
$lang['pj_invalid_id'] 		= 'ID tidak valid';
$lang['pj_no_detail'] 		= 'Tidak ada detail transaksi';
$lang['pj_no_saldo'] 		= 'Anda tidak dapat melakukan transaksi. Cek kembali Saldo Kas Kasir Anda';
$lang['pj_only_ajax'] 		= 'Hanya ajax request yang diijinkan.';
$lang['pj_no_input'] 		= 'Anda belum memasukkan barang yang akan dijual.';
$lang['pj_no_qty'] 			= 'Qty baris ke %s tidak boleh kurang dari atau sama dengan nol.';
$lang['pj_stok_minus'] 		= 'Maaf stok barang baris ke: %s tidak tidak mencukupi.';
$lang['pj_no_harga'] 		= 'Maaf harga barang baris ke: %s belum disetting.';
$lang['pj_pelunasan_success'] = 'Pelunasan transaksi penjualan berhasil disimpan.';
$lang['pj_pelunasan_failed']  = 'Maaf, pelunasan transaksi penjualan gagal disimpan.';