<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for kasir

*/
class Kasir extends Admin_Controller {

        

	protected $viewPermission   		= "Kasir.View";
    protected $addPermission    		= "Kasir.Add";
    protected $deletePermission 		= "Kasir.Delete";

	protected $manageHargaPermission 	= "Kasir Harga.Manage";    

    protected $viewCicilan              = "Cicilan Transaksi Kasir.View";
    protected $addCicilan               = "Cicilan Transaksi Kasir.Add";
    protected $deleteCicilan            = "Cicilan Transaksi Kasir.Delete";

    protected $prefixKey        		= "KSR";

    protected $id_primary               = "";/* id_primary dari transaksi */
    protected $no_nota                  = "";/* no_nota transaksi */

    public function __construct(){

        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('kasir/kasir');
        $this->load->model(array(
        							"order_model",
        							"order_detail_model",
        							"trans_model",
        							"trans_detail_model",
        							"konsumen_model",
                                    "pelunasan_model",
                                    "pelunasan_detail_model",
                                    "spk_model",
                                    "bank_model",
                                    "kas_kasir_model",
                                    "kas_kasir_detail_model",
                                    "bayar_trans_model"
        						)
    						);

        //$this->load->helper('all_helper');

        $this->template->title(lang('title'));
		$this->template->page_icon('fa fa-list');
    }

    public function index($id_transaksi=""){

        /*set_deposit($id_konsumen = 'KONSU2I0181102K1045W24M96Q5', 
                    $nilai = 235000, 
                    $ket = 'deposit ke 2', 
                    $st_valid = 1, 
                    $waktu = null, 
                    $metode_bayar = 1, 
                    $rekening_pengirim = '', 
                    $input_file_resi = null);*/
        //use_deposit($id_konsumen = 'KONSU2I0181102K1045W24M96Q5', $nilai = 30000, $ket = 'pembayaran transaksi x sample');
        //print_r(get_deposit($id_konsumen = 'KONSU2I0181102K1045W24Mxx5'));

    	$this->auth->restrict($this->viewPermission);

        if(isset($_POST['delete']) && has_permission($this->deletePermission)){

            if($this->delete()){

                $this->template->set_message(lang("konfirmasi-delete-sukses"),"success");

            }else{

                $this->template->set_message(lang("konfirmasi-error-pil-delete"),"error");

            }

        }


        $this->load->library('pagination');       

        $filter     = "";

        $where  = "transaksi.deleted = 0 and `order_produk`.`st_order` = 0 and transaksi.st_history = 0 AND `order_produk`.`st_revisi` <> 1";

        if(isset($_POST['no_faktur'])){

            
            $tgl1_trans     = isset($_POST['tgl1']) ? $this->input->post('tgl1') : '';
            $tgl2_trans     = isset($_POST['tgl2']) ? $this->input->post('tgl2') : '';

            $tgl1_tempo     = isset($_POST['tgl1_tempo']) ? $this->input->post('tgl1_tempo') : '';
            $tgl2_tempo     = isset($_POST['tgl2_tempo']) ? $this->input->post('tgl2_tempo') : '';

            $konsumen       = isset($_POST['konsumen']) ? $this->input->post('konsumen') : '';
            //$metode         = isset($_POST['metode']) ? $this->input->post('metode') : '';
            $status         = isset($_POST['status']) ? $this->input->post('status') : '';

            $no_faktur      = isset($_POST['no_faktur']) ? $this->input->post('no_faktur') : '';
            
            //$st_order       = isset($_POST['st_order']) ? $this->input->post('st_order') : '';

        }else if(isset($_GET['no_faktur'])){

            $tgl1_trans     = isset($_GET['tgl1']) ? $this->input->get('tgl1') : '';
            $tgl2_trans     = isset($_GET['tgl2']) ? $this->input->get('tgl2') : '';

            $tgl1_tempo     = isset($_GET['tgl1_tempo']) ? $this->input->get('tgl1_tempo') : '';
            $tgl2_tempo     = isset($_GET['tgl2_tempo']) ? $this->input->get('tgl2_tempo') : '';

            $konsumen       = isset($_GET['konsumen']) ? $this->input->get('konsumen') : '';
            //$metode         = isset($_GET['metode']) ? $this->input->get('metode') : '';
            $status         = isset($_GET['status']) ? $this->input->get('status') : '';

            $no_faktur      = isset($_GET['no_faktur']) ? $this->input->get('no_faktur') : '';

            //$st_order       = isset($_GET['st_order']) ? $this->input->get('st_order') : '';
            

        }else{

            $tgl1_trans     = "";
            $tgl2_trans     = "";            

            $tgl1_tempo     = "01/05/2018";

            $date_now       = new DateTime(date("Y-m-d"));
            $interval       = new DateInterval("P3D");

            $date_now->add($interval);

            $tgl2_tempo     = $date_now->format("d/m/Y");

            $konsumen       = '';
            //$metode         = '';
            $status         = 0;

            //cek id_transaksi

            $no_faktur      = '';

            //$idtrans        = urldecode($id_transaksi);

            $idtrans        = "";

            if(strlen($idtrans)>0){

                $no_faktur  = $this->trans_model->select("no_faktur")->find($idtrans);
                $no_faktur  = $no_faktur->no_faktur;

            }

            //$st_order       = 1;

        }

        if(strlen($tgl1_trans)>0){

            $tgl1   = date_ymd($tgl1_trans)." 01:00:00";
            $tgl2   = date_ymd($tgl2_trans)." 23:59:59" ;

            $where  .= " and (`transaksi`.`created_on` >= '{$tgl1}' and `transaksi`.`created_on` <= '{$tgl2}')";

            
        }


        if(strlen($tgl1_tempo)>0){

            $tgl1   = date_ymd($tgl1_tempo);
            $tgl2   = date_ymd($tgl2_tempo);

            $where  .= " and (`transaksi`.`tgl_tempo` >= '{$tgl1}' and `transaksi`.`tgl_tempo` <= '{$tgl2}')";                        
        }

        if(strlen($konsumen)>0 && (strlen($tgl1_trans)>0 || strlen($tgl1_tempo)>0)){

            $where  .=  " and `konsumen`.`idkonsumen` = '{$konsumen}'";

        }


        /* start bagian status lunas dan revisi */
        if(strlen($status)>0 && (strlen($tgl1_trans)>0 || strlen($tgl1_tempo)>0)){            

            $where  .= " and `transaksi`.`st_lunas` = {$status}";            

        }

        
        /* end bagian status lunas dan revisi */

        if(strlen($no_faktur) > 0){

            $where  = "(trans_ref.no_faktur = '{$no_faktur}' 
                        or transaksi.no_faktur = '{$no_faktur}')
                        and `order_produk`.`st_order` = 0 
                        and transaksi.deleted = 0 
                        AND `order_produk`.`st_revisi` <> 1";

        }

        $filter     = "?no_faktur={$no_faktur}&tgl1={$tgl1_trans}&tgl2={$tgl2_trans}&tgl1_tempo={$tgl1_tempo}&tgl2_tempo={$tgl2_tempo}&status={$status}";

        $total      = $this->trans_model->select(`transaksi`.`id_transaksi`)
                                        ->join("order_produk","transaksi.id_order = order_produk.id_order","inner")
                                        ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                                        ->join("(SELECT 
                                                `id_transaksi` AS id, `no_faktur`
                                            FROM
                                                transaksi
                                            WHERE
                                                deleted = 0) AS trans_ref","trans_ref.id = transaksi.id_trans_ref_asli","inner")
                                        ->where($where)
                                        ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

    	$data 	= $this->trans_model->select("`transaksi`.`id_transaksi` AS `id`,
                                                `transaksi`.`st_trans_master`,
                                                `transaksi`.`st_history`,
                                                DATE_FORMAT(`transaksi`.`created_on`,
                                                        '%d/%m/%Y %H:%i:%s') AS created_on,
                                                `transaksi`.`no_faktur`,
                                                trans_ref.no_faktur AS no_faktur_master,
                                                `order_produk`.`id_order`,
                                                `order_produk`.`no_transaksi`,
                                                `order_produk`.`st_revisi`,
                                                `konsumen`.`panggilan`,
                                                `konsumen`.`nama`,
                                                `transaksi`.`grand_total`,
                                                `transaksi`.`st_metode_bayar`,
                                                `transaksi`.`bayar`,
                                                `transaksi`.`st_lunas`")
    								->join("order_produk","transaksi.id_order = order_produk.id_order","inner")
    								->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                                    ->join("(SELECT 
                                                `id_transaksi` AS id, `no_faktur`
                                            FROM
                                                transaksi
                                            WHERE
                                                deleted = 0) AS trans_ref","trans_ref.id = transaksi.id_trans_ref_asli","inner")
    								->where($where)
    								->order_by("`transaksi`.`created_on`","asc")
                                    ->limit($limit,$offset)
    								->find_all();

        //echo $this->db->last_query();

        $where_rev  = "`transaksi`.`deleted` = 0
                        AND `order_produk`.`st_order` = 0
                        AND `transaksi`.`st_history` = 0
                        AND `order_produk`.`st_revisi` = 1";
        
        $dt_revisi  = $this->trans_model->select("`transaksi`.`id_transaksi` AS `id`,
                                                `transaksi`.`st_trans_master`,
                                                `transaksi`.`st_history`,
                                                DATE_FORMAT(`transaksi`.`created_on`,
                                                        '%d/%m/%Y %H:%i:%s') AS created_on,
                                                `transaksi`.`no_faktur`,
                                                trans_ref.no_faktur AS no_faktur_master,
                                                `order_produk`.`id_order`,
                                                `order_produk`.`no_transaksi`,
                                                `order_produk`.`st_revisi`,
                                                `konsumen`.`panggilan`,
                                                `konsumen`.`nama`,
                                                `transaksi`.`grand_total`,
                                                `transaksi`.`st_metode_bayar`,
                                                `transaksi`.`bayar`,
                                                `transaksi`.`st_lunas`")
                                    ->join("order_produk","transaksi.id_order = order_produk.id_order","inner")
                                    ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                                    ->join("(SELECT 
                                                `id_transaksi` AS id, `no_faktur`
                                            FROM
                                                transaksi
                                            WHERE
                                                deleted = 0) AS trans_ref","trans_ref.id = transaksi.id_trans_ref_asli","inner")
                                    ->where($where_rev)
                                    ->order_by("`transaksi`.`created_on`","asc")
                                    ->limit($limit,$offset)
                                    ->find_all();

        $dt_konsumen    = $this->konsumen_model
                                ->select("`idkonsumen` as id, 
                                        CONCAT(IF(panggilan IS NULL, '', panggilan),
                                        ' ',
                                        `nama`,
                                        ' | ',
                                        IF(st = 0,
                                            'konsumen',
                                            IF(st = 1, 'reseller', 'instansi'))) AS nm")
                                ->where("deleted=0")
                                ->order_by("nama","asc")
                                ->find_all();

        $asset  =   array(
                            "plugins/select2/js/select2.js",
                            "plugins/select2/css/select2.css",
                            'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                            'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                            "kasir/assets/js/index_trans.js"
                            
                    );

        add_assets($asset);

        // start cek saldo kas kasir

        //$cek_kas_kasir  = $this->cek_saldo_kasir();
        $cek_kas_kasir  = cek_saldo_kasir();
        if(!$cek_kas_kasir){

            $this->template->set_message(lang("label-err-saldo-tidak-ada"),"warning");
            $this->template->set("saldo_kosong",0);

        }

        // end cek saldo kas kasir

        $this->template->set('numb',$offset+1);        

        $this->template->set('tgl1',$tgl1_trans);
        $this->template->set('tgl2',$tgl2_trans);

        $this->template->set('tgl1_tempo',$tgl1_tempo);        
        $this->template->set('tgl2_tempo',$tgl2_tempo);
        $this->template->set('konsumen',$konsumen);
        //$this->template->set('metode',$metode);
        $this->template->set('status',$status);
        $this->template->set('no_faktur',$no_faktur);
        //$this->template->set('st_order',$st_order);

    	$this->template->set("data",$data);
        $this->template->set("data_rev",$dt_revisi);

        $this->template->set("dt_konsumen",$dt_konsumen);
    	$this->template->set("toolbar_title", lang('title'));
        $this->template->set("page_title", lang('title'));
        $this->template->render('index_trans'); 

    }

    public function get_saldo_deposit(){

        if(!$this->input->is_ajax_request()){

            $return = false;

        }

        $id_konsumen    = $this->input->post('id');
        $tagihan        = $this->input->post('tagihan');

        $nilai_saldo    = get_deposit($id_konsumen);

        if(!$nilai_saldo){

            $return = false;

        }else{

            if($nilai_saldo == 0){

                $return = 0;

            }else{

                $return = $nilai_saldo >= $tagihan ? $tagihan : $nilai_saldo;

            }

        }

        echo $return;

    }

    public function tmp_session_cetak_nota(){

        /*
            Konsep :
                1. Simpan data formnya didadalam sebuah session
                2. Gunakan session, untuk menciptakan nota

                3. Saat dicetak struk sementaranya, maka ordernya dilock

        */

        if(!$this->input->is_ajax_request()){

            $return = false;

        }

        $id_detail_order            = $this->input->post("id_detail_order");
        $st_ubah                    = $this->input->post("dft_st_ubah");
        $nm_pekerjaan               = $this->input->post("dft_nm_pekerjaan");
        $kategori                   = $this->input->post("dft_kategori");
        $produk                     = $this->input->post("dft_produk");
        $st_type_produk             = $this->input->post("dft_st_type_produk");
        $uk_p                       = $this->input->post("dft_uk_p");
        $uk_l                       = $this->input->post("dft_uk_l");
        $id_satuan_uk               = $this->input->post("dft_idsatuan_uk");
        $tmpl_uk                    = $this->input->post("dft_tmpl_satuan_uk");
        $st_finishing               = $this->input->post("dft_st_finishing");
        $jml_cetak                  = $this->input->post("dft_jml_cetak");
        $img_thumbnail              = $this->input->post("img_thumbnail");
        $st_urgent                  = $this->input->post("st_urgent");
        $tgl_selesai                = $this->input->post("tgl_selesai");

        $tampil_harga               = $this->input->post("dft_tampil_harga");
        $harga_asli                 = $this->input->post("dft_rp_harga_asli");
        $tmpl_satuan_harga          = $this->input->post("dft_tmpl_satuan_harga");
        $id_satuan_harga            = $this->input->post("dft_id_satuan_harga");

        $harga_design_asli          = $this->input->post("dft_rp_design_asli");
        $harga_cetak_asli           = $this->input->post("dft_rp_cetak_asli");
        $harga_finishing_asli       = $this->input->post("dft_rp_finishing_asli");
        
        $diskon                     = $this->input->post("dft_diskon");                  
        $pot_kecil                  = $this->input->post("dft_pot_kecil");                  
        $potongan                   = $this->input->post("dft_potongan");                   

        $subtotal_asli              = $this->input->post("dft_subtotal_asli");
        
        $harga_baru                 = $this->input->post("dft_rp_harga_baru");
        $harga_cetak_baru           = $this->input->post("dft_rp_cetak_baru");
        $harga_design_baru          = $this->input->post("dft_rp_design_baru");
        $harga_finishing_baru       = $this->input->post("dft_rp_finishing_baru");
        $subtotal_baru              = $this->input->post("dft_subtotal_baru");                    
        
        // data header
        $id_order                   = $this->input->post("id_order_produk");
        $id_konsumen                = $this->input->post("isi_id_konsumen");
        $total_all                  = $this->input->post("isi_total_order");
        $diskon_all                 = $this->input->post("diskon_all");
        $pot_all                    = $this->input->post("pot_all");
        $pajak_all                  = $this->input->post("pajak_all");
        $grand_total                = $this->input->post("isi_grandtotal");

        $dp                         = isset($_POST['dp']) ? str_replace(",", "", $this->input->post("dp")) : 0;

        $bayar                      = str_replace(",", "", $this->input->post("bayar"));
        $st_produksi                = $this->input->post("st_produksi");
        
        $ms_tempo                   = $this->input->post("ms_tempo");
        $tgl_tempo                  = $this->input->post("tgl_tempo");
        $st_bayar                   = $this->input->post("metode_bayar");        

        // start lock data ordernya

        $arr_update     = ["st_ctk_struk_tmp" => 1];
        $this->order_model->update($id_order, $arr_update);

        // end lock data ordernya

        // start data konsumen

        $dt_k   = $this->konsumen_model->select('telp, nama')
                        ->find($id_konsumen);

        $no_telp_k  = $dt_k->telp;
        $nama_k     = $dt_k->nama;

        // end data konsumen

        // start susunan array detail struck

        $det    = [];
        foreach ($id_detail_order as $key => $dt) {
            
            $det[]  =   [
                            'nm_pekerjaan'      => $nm_pekerjaan[$key],
                            'jml_cetak'         => $jml_cetak[$key],
                            'tampil_harga'      => number_format($harga_asli[$key])."/".$tmpl_satuan_harga[$key],
                            'potongan'          => $potongan[$key],
                            'biaya_cetak'       => $harga_cetak_baru[$key],
                            'biaya_design'      => $harga_design_baru[$key],
                            'biaya_finishing'   => $harga_finishing_baru[$key]
                        ];

        }

        // end susunan array detail struck

        // start array full

        $arr_dt     =   [
                            'nama_konsumen' => $nama_k,
                            'telp_konsumen' => $no_telp_k,
                            'waktu'         => date('d/m/Y H:i:s'),
                            'grand_total'   => $grand_total,
                            'detail'        => $det
                        ];

        // end array full        

        // start simpan dalam session

        $this->session->set_userdata('tmp_cetak_nota_sementara', $arr_dt);

        // end simpasn dalam session

        print_r($arr_dt);

    }

    public function cetak_struk_sementara(){

        $identitas      = $this->identitas_model->find(1);
        $idt_op         = $this->auth->userdata();

        // Start ambil data dari session yang sudah dibuat sebelumnya

        $data           = $this->session->userdata('tmp_cetak_nota_sementara');

        $this->session->unset_userdata('tmp_cetak_nota_sementara');//hapus sessionnya setelah diambil datanya

        // end ambil data dari session yang sudah dibuat sebelumnya

        $this->template->set('data',$data);
        $this->template->set('idt', $identitas);
        $this->template->set('op', $idt_op);

        $this->template->set_layout("cetak_struk");
        $this->template->title(lang('title_view'));
        $this->template->render("cetak_struk_kecil_sementara");

    }


    public function pembulatan_gt($angka = 0){

        $cek_lebihan    = $angka % 100;
        $cek_pembulatan = floor($angka / 100);

        if($cek_lebihan > 1){

            $cek_pembulatan += 1;

        }

        $hasil = $cek_pembulatan * 100;

        return $hasil;

    }

    /*public function cek_saldo_kasir(){

          
            Hanya melakukan pengecekkan apakah User yang sedang login sudah memiliki saldo / kas terbuka.
            Jika belum, maka user tidak dapat melakukan Delete, Tambah , Edit
            
        

        $user           = $this->auth->userdata();
        $id_user        = $user->id_user;
        $date           = date("Y-m-d");
        $dt_cek_kas     = $this->kas_kasir_model->where("DATE(waktu_buat) = '{$date}'
                                                        AND created_by = {$id_user}
                                                        AND st_close = 0")
                                                ->count_all();

        $return         = $dt_cek_kas == 0 ? false : true;
        return $return;


    }*/

    /*public function set_alur_kas_kasir($ket = "", $metode = 0, $status = 1, $jml = 0){

        $user           = $this->auth->userdata();
        $id_user        = $user->id_user;
        $date           = date("Y-m-d");

        $waktu_simpan   = date("Y-m-d H:i:s");

        $dt_kas_pakai   = $this->kas_kasir_model->select("idkas_kasir")
                                        ->where("DATE(waktu_buat) = '{$date}'
                                                        AND created_by = {$id_user}
                                                        AND st_close = 0")
                                        ->find_all();

        $id_primary     = $dt_kas_pakai[0]->idkas_kasir;


        $dt_saldo       = $this->kas_kasir_detail_model
                                        ->select("`kas_kasir_detail`.`saldo`")
                                        ->join("kas_kasir","kas_kasir_detail.idkas_kasir = kas_kasir.idkas_kasir")
                                        ->where("kas_kasir_detail.idkas_kasir = '{$id_primary}'")    
                                        ->order_by("`kas_kasir_detail`.`waktu`","desc")
                                        ->order_by("`kas_kasir_detail`.`order`","desc")
                                        ->limit(1,0)
                                        ->find_all();

        //kondisikan jumlah yang ditambahkan di saldo.
        //saldo hanya dijumlah jika dia kas tunai.
        if($metode == 0){

            $jml_tambahan = $jml;

        }else{

            $jml_tambahan = 0;    

        }


        if(count($dt_saldo) && is_array($dt_saldo)){

            $saldo_akhir    = $dt_saldo[0]->saldo;

        }else{

            $saldo_akhir = 0;

        }

        $saldo_sekarang = $status == 0 ? $saldo_akhir - $jml_tambahan :  $saldo_akhir + $jml_tambahan;

        //kondisikan order
        $cek_last_order     = $this->kas_kasir_detail_model
                                    ->select("order")
                                    ->where("idkas_kasir = '{$id_primary}' 
                                                and waktu = '{$waktu_simpan}'")
                                    ->order_by("order","desc")
                                    ->limit(1,0)
                                    ->find_all();

        if(is_array($cek_last_order) && count($cek_last_order)){

            $order_baru     = $cek_last_order[0]->order +1;

        }else{

            $order_baru     = 0;

        }


        $arr_detail     =   [
                                'idkas_kasir_detail'    => gen_primary("","kas_kasir_detail","idkas_kasir_detail"),
                                'idkas_kasir'           => $id_primary,
                                'st_saldo_awal'         => 0,
                                'waktu'                 => $waktu_simpan,
                                'ket'                   => $ket,
                                'st_metode'             => $metode,
                                'jumlah'                => $jml,
                                'status'                => $status,
                                'saldo'                 => $saldo_sekarang,
                                'order'                 => $order_baru
                            ];

        $this->kas_kasir_detail_model->insert($arr_detail);
        

    }*/

    private function cek_trans_po($id_transaksi = ""){

        // cek apakah transaksi yang ada merupakan transaksi Order PO

        $dt_cek     = $this->trans_model->select("transaksi.`id_transaksi`")
                            ->join('(SELECT 
                                        COUNT(`kasir_order_po_detail`.`id_kasir_order_po_detail`) AS jml_detail,
                                            `m_order_produk_detail`.`id_order`
                                    FROM
                                        kasir_order_po
                                    INNER JOIN kasir_order_po_detail ON kasir_order_po.id_kasir_order_po = kasir_order_po_detail.id_kasir_order_po
                                    INNER JOIN m_order_produk_detail ON kasir_order_po_detail.id_m_order_detail = m_order_produk_detail.id_detail_produk_order
                                    WHERE
                                        kasir_order_po.deleted = 0
                                            AND kasir_order_po_detail.deleted = 0
                                    GROUP BY `m_order_produk_detail`.`id_order`) AS db_val_cetak','transaksi.id_order = db_val_cetak.id_order','inner')
                            ->where("transaksi.st_history = 0
                                        AND transaksi.deleted = 0
                                        AND transaksi.id_transaksi = '{$id_transaksi}'")                        
                            ->count_all();

        if($dt_cek > 0){

            return false;

        }else{

            return true;

        }

    }

    public function delete($id_transaksi = ""){

        $this->auth->restrict($this->deletePermission);

        // cek posisi saldo kasir

        
        //$cek_saldo  = $this->cek_saldo_kasir();
        $cek_saldo  = cek_saldo_kasir();
        if(!$cek_saldo){

            $this->template->set_message(lang("label-err-saldo-tidak-ada"),"error");
            return false;        

        }

        //ambil id_order, id_trans_master milik transaksi
        $dt_trans           = $this->trans_model
                                    ->select("transaksi.id_order,
                                                transaksi.no_faktur,
                                                transaksi.id_trans_ref_asli,
                                                order_produk.id_konsumen")
                                    ->join("order_produk","order_produk ON transaksi.id_order = order_produk.id_order")
                                    ->where("transaksi.id_transaksi = '{$id_transaksi}'")
                                    ->find_all(); 

        $nota_master        = $dt_trans[0]->no_faktur;
        $id_order_produk    = $dt_trans[0]->id_order;
        $id_trans_master    = $dt_trans[0]->id_trans_ref_asli;
        $id_konsumen        = $dt_trans[0]->id_konsumen;

        // cek apakah, Jika transaksi Order PO / SO sudah da validasi cetaknya.
        $cek_po    = $this->cek_trans_po($id_transaksi);
        if(!$cek_po){

            $this->template->set_message(lang("konfirmasi-err-del-poso"),"error");
            return false;

        }else{

            // cek apakah sudah masuk produksi
            $cek_st_produksi    = $this->order_detail_model->select("id_detail_produk_order")
                                        ->where("deleted = 0 and st_produksi = 1 and id_order = '{$id_order_produk}'")
                                        ->count_all();

            if($cek_st_produksi > 0){

                $this->template->set_message(lang("konfirmasi-err-del-stproduksi"),"error");
                return false;

            }
                
        }

        //ambil semua nilai total bayar yang sudah dibayarkan konsumen
        $dt_bayar   = $this->bayar_trans_model
                            ->select("st_sumber_bayar, 
                                        SUM(`bayar`) AS total")
                            ->where("deleted = 0 
                                        and id_transaksi_master = '{$id_trans_master}'")
                            ->group_by('st_sumber_bayar')
                            ->find_all(); 

        $total_bayar        = 0;
        $total_deposit      = 0;

        if(is_array($dt_bayar) && count($dt_bayar)){

            foreach ($dt_bayar as $key => $isi) {
                
                if($isi->st_sumber_bayar == 0){

                    $total_bayar    = $isi->total;

                }else{

                    $total_deposit  = $isi->total;

                }

            }

        }

        $sql_all = "";

        $this->db->trans_start();

            //delete semua transaksinya
            $arr_where  = ["id_trans_ref_asli" => $id_trans_master];
            $this->trans_model->delete_where($arr_where);
            $sql_all    = $this->db->last_query();

            //delete data bayar
            $arr_where  = ["id_transaksi_master" => $id_trans_master];
            $this->bayar_trans_model->delete_where($arr_where);
            $sql_all    .= "\n\n".$this->db->last_query();            

            //update data detail order
            $arr_update_det_order   =   [
                                            'st_acc_produksi'   => 0
                                        ];
            $this->order_detail_model->update_where('id_order',$id_order_produk,$arr_update_det_order);
            $sql_all    .= "\n\n". $this->db->last_query();

            //update head order
            $arr_update_order       =   [
                                            'st_kasir'  => 0, 
                                            'st_revisi' => 0, 
                                            'st_nota'   => 0
                                        ];
            $this->order_model->update($id_order_produk, $arr_update_order);
            $sql_all    .= "\n\n". $this->db->last_query();

            //start pencatatan pembayarannya

            if($total_deposit > 0){

                $ket_pengembalian   = 'Pengembalian deposit karena transaksinya dihapus, untuk No. Nota Master : '. $nota_master;

                set_deposit($id_konsumen = $id_konsumen, 
                    $nilai = $total_deposit, 
                    $ket = $ket_pengembalian, 
                    $st_valid = 1, 
                    $waktu = null, 
                    $metode_bayar = 1, 
                    $rekening_pengirim = '', 
                    $input_file_resi = null);

                set_alur_kas_kasir($ket = $ket_pengembalian, 
                                    $metode = 3, 
                                    $status = 0, 
                                    $jml = $total_deposit);

            }

            if($total_bayar > 0){

                $ket_pengembalian   = 'Pengembalian bayar karena transaksinya dihapus, untuk No. Nota Master : '. $nota_master;

                set_alur_kas_kasir($ket = $ket_pengembalian, 
                                    $metode = 0, 
                                    $status = 0, 
                                    $jml = $total_bayar);                                

            }

            //end pencatatan pembayarannya
            

            
            // start delete list spk

            $data_list_order    = $this->order_detail_model
                                        ->select('`id_spk`')
                                        ->where("`id_order` = '{$id_order_produk}'
                                                AND deleted = 0")
                                        ->find_all();


            if(is_array($data_list_order) && count($data_list_order)){

                foreach ($data_list_order as $key => $isi) {
                        
                    delete_spk($isi->id_spk);

                }
                

            }

            $dt_update = ['id_spk' => null];

            $this->order_detail_model->update_where('id_order',$id_order_produk,$dt_update);
            $sql_all .= "\n\n".$this->db->last_query();
            
            // end delete list spk 
            

        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            $return         = false;
            $keterangan     = "Gagal, hapus data transaksi";
            $total          = 0;
            $status         = 0;

        }else{

            $return         = true;
            $keterangan     = "Sukses, hapus data transaksi";
            $total          = $total_bayar;           
            $status         = 1;

        }

        $nm_hak_akses   = $this->deletePermission; 
        $kode_universal = $id_transaksi;
        $jumlah         = 0;
        $sql            = $sql_all;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $total, $sql, $status);

        return $return;

    }


    /*public function delete(){


        if(isset($_POST['checked'])){

            $id         = $this->input->post('checked');
            $sql_all    = "";

            $this->db->trans_start();

            foreach ($id as $key => $isi) {
                
                //cek apakah ada di SPK yang sudah diambil
                $cek_spk    = $this->spk_model->where("deleted = 0 and id_transaksi = '{$isi}' and `st_ambil` = 1")
                                            ->count_all();

                $cek_saldo  = $this->cek_saldo_kasir();


                if($cek_saldo > 0 && $cek_spk <= 0){ 

                    //update st_kasir order menjadi 0
                    $dt_kasir   = $this->trans_model->select("`id_order`,`grand_total`,`bayar`,`bayar2`")->find($isi);

                    $bayar      = $dt_kasir->bayar + $dt_kasir->bayar2;
                    $gtotal     = $dt_kasir->grand_total;

                    $arr    = array(
                                        'st_kasir' => 0
                                    );

                    $this->order_model->update($dt_kasir->id_order,$arr);
                    $sql_all    .= "\n\n".$this->db->last_query();

                    // Pendataan kas kasir 
                    $nominal_keluar_kas     = ($bayar - $gtotal) >= 0 ? $gtotal : $bayar; 
                    $this->set_alur_kas_kasir("Pembatalan Transaksi", 0, 0, $nominal_keluar_kas);
                    $sql_all    .= "\n\n".$this->db->last_query();                    
                    
                    //update st_acc_produksi detail_order
                    $arr_det    = array('st_acc_produksi'=>0);
                    $this->order_detail_model->update_where('id_order',$dt_kasir->id_order,$arr_det);
                    $sql_all    .= "\n\n".$this->db->last_query();

                    //delete transaksi
                    $this->trans_model->delete($isi);
                    $sql_all    .= "\n\n".$this->db->last_query();

                    //delete spk
                    $this->spk_model->delete_where(['id_transaksi'=>$isi]);
                    $sql_all    .= "\n\n".$this->db->last_query();

                }

            }

            $this->db->trans_complete();

            if($this->db->trans_status() == false){

                $return         = false;
                $keterangan     = "Gagal, hapus data transaksi kasir";
                $total          = 0;
                $status         = 0;

            }else{

                $return         = true;
                $keterangan     = "Sukses, hapus data transaksi kasir";
                $total          = 0;           
                $status         = 1;

            }

            $nm_hak_akses   = $this->deletePermission; 
            $kode_universal = "-";
            $jumlah         = 0;
            $sql            = $sql_all;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $total, $sql, $status);

        }else{

            $return = false;

        }

        return $return;

    }*/

    public function open_order(){

        /* Buka daftar order yang belum ada notanya */

		$this->auth->restrict($this->addPermission);

		$this->load->library('pagination');

		$where 				= "order_produk.deleted = 0 AND order_produk.st_simpan = 1 AND order_produk.st_kasir = 0";
		$tambahan_where 	= "";

		if(isset($_POST['btn_cari'])){

			$tgl1 			= isset($_POST['tgl1']) ? $this->input->post('tgl1') : '';
			$tgl2 			= isset($_POST['tgl2']) ? $this->input->post('tgl2') : '';

			$nm_konsumen 	= isset($_POST['konsumen']) ? $this->input->post('konsumen') : '';
			$no_order 		= isset($_POST['no_order']) ? $this->input->post('no_order') : '';

		}else{

			$tgl1 			= isset($_GET['tgl1']) ? $this->input->get('tgl1') : '';
			$tgl2 			= isset($_GET['tgl2']) ? $this->input->get('tgl2') : '';

			$nm_konsumen 	= isset($_GET['konsumen']) ? $this->input->get('konsumen') : '';
			$no_order 		= isset($_GET['no_order']) ? $this->input->get('no_order') : '';

		}    	

		$filter 	= "?order";

		if($tgl1 != '' && $tgl2 != ''){

			$tgl1 	= date_ymd($tgl1);
			$tgl2 	= date_ymd($tgl2);

			$tambahan_where 	.= " and order_produk.tgl_order >= '$tgl1' and order_produk.tgl_order <= '$tgl2'";

		}

		if($nm_konsumen != ''){

			$tambahan_where 	.= " and order_produk.id_konsumen = $nm_konsumen";

		}

		if($no_order != ''){

			$tambahan_where 	= " and order_produk.no_transaksi = '$no_order'";

		}

		$where .= $tambahan_where;

    	$offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $total 	= $this->order_model->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
        							->where($where)
        							->count_all();

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);


    	$data 	= $this->order_model->select("`order_produk`.`id_order`,
											    `order_produk`.`no_transaksi`,
											    DATE_FORMAT(`order_produk`.`tgl_order`, '%d/%m/%Y') AS tgl_order,
											    DATE_FORMAT(`order_produk`.`tgl_permintaan_selesai`,
											            '%d/%m/%Y') AS tgl_selesai,
											    `order_produk`.`total_value_order`,
											    `konsumen`.`panggilan`,
											    `konsumen`.`nama`,
											    `konsumen`.`st`,
											    IF(`konsumen`.`st` = 0,
											        'Konsumen',
											        IF(`konsumen`.`st` = 1,
											            'Reseller',
											            'Instansi')) AS st_konsumen")
    								->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
    								->where($where)
    								->order_by("order_produk.tgl_order","desc")
    								->limit($limit, $offset)
    								->find_all();

        //echo $this->db->last_query();

    	$dt_konsumen 	= $this->konsumen_model
                                    ->select("`idkonsumen` as id, 
                                        CONCAT(IF(panggilan IS NULL, '', panggilan),
                                        ' ',
                                        `nama`,
                                        ' | ',
                                        IF(st = 0,
                                            'konsumen',
                                            IF(st = 1, 'reseller', 'instansi'))) AS nm")
									->where("deleted=0 and st < 2")
									->order_by("nama","asc")
									->find_all();

    	$asset 	= 	array(
    						"plugins/select2/js/select2.js",
                            "plugins/select2/css/select2.css",
                            'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        	'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                            "kasir/assets/js/index_order.js",
                            "kasir/assets/css/css_list.css"
    				);

    	add_assets($asset);

    	$this->template->set("tgl1",$tgl1);
    	$this->template->set("tgl2",$tgl2);
    	$this->template->set("konsumen",$nm_konsumen);
    	$this->template->set("no_order",$no_order);

    	$this->template->set("dt_konsumen",$dt_konsumen);

    	$this->template->set("numb", $offset+1);
    	$this->template->set("data", $data);
		$this->template->set("toolbar_title", lang('title_order'));
        $this->template->set("page_title", lang('title_order'));
        $this->template->title(lang('title_order'));
        $this->template->render('index_order');     	

    }

    public function cancel(){

    	$this->session->unset_userdata('frm_trans_kasir');
    	redirect('kasir');

    }

    public function get_load_ulang_cicilan(){

        if(!$this->input->is_ajax_request()){

            redirect("kasir");

        }        

        $idtransaksi    = $this->input->post('id_trans');

        $sisa_piutang   = $this->trans_model
                                ->select("`transaksi`.`id_transaksi`,
                                        `transaksi`.`id_trans_ref_asli`,
                                        `transaksi`.`st_kasir`,
                                        `transaksi`.`grand_total`,
                                        `bayar`.`total_bayar`,
                                        (`transaksi`.`grand_total` - IF(bayar.total_bayar IS NULL,
                                            0,
                                            bayar.total_bayar)) AS sisa_piutang")
                                ->join("(SELECT 
                                                `id_transaksi_master` AS id, SUM(`bayar`) AS total_bayar
                                            FROM
                                                bayar_trans_kasir
                                            WHERE
                                                deleted = 0
                                            GROUP BY id_transaksi_master) AS bayar","`transaksi`.`id_trans_ref_asli` = `bayar`.`id`","left")
                                ->where("`transaksi`.`id_transaksi` = '{$idtransaksi}'")
                                ->find_all();

        $id_ref_master  = $sisa_piutang[0]->id_trans_ref_asli;
        $sisa_piutang   = number_format($sisa_piutang[0]->sisa_piutang);

        $dt_bayar   = $this->trans_model
                            ->select("`transaksi`.`id_transaksi`,
                                        `transaksi`.`no_faktur`,
                                        `bayar_trans_kasir`.`idbayar_trans_kasir`,
                                        `bayar_trans_kasir`.`id_transaksi_master`,
                                        date_format(`bayar_trans_kasir`.`created_on`,'%d/%m/%Y %H:%i:%s') as tgl,
                                        `bayar_trans_kasir`.`created_on`,
                                        `bayar_trans_kasir`.`bayar`,
                                        `bayar_trans_kasir`.`st_bayar`,
                                        `users`.`nm_lengkap`")
                            ->join("bayar_trans_kasir","transaksi.id_transaksi = bayar_trans_kasir.id_transaksi","inner")
                            ->join("users","bayar_trans_kasir.created_by = users.id_user","inner")
                            ->where("transaksi.deleted = 0
                                        AND bayar_trans_kasir.deleted = 0
                                        AND bayar_trans_kasir.id_transaksi_master = '{$id_ref_master}'")
                            ->order_by("bayar_trans_kasir.created_on")
                            ->find_all();

        $isi_table  = "";

        if(is_array($dt_bayar) && count($dt_bayar)){

            foreach ($dt_bayar as $key => $isi) {
                
                $no     = $key+1;

                switch ($isi->st_bayar) {
                    case 0:
                        $metode     = "<span class='label label-success'>Cash / Tunai</span>";  
                        break;
                    case 1:
                        $metode     = "<span class='label label-warning'>Debit</span>"; 
                        break;
                    case 2:
                        $metode     = "<span class='label label-primary'>Transfer</span>";  
                        break;
                }

                if(has_permission($this->deleteCicilan)){

                    $delete     = "<a href='#del' onclick='del_cicilan(this);' title='Delete Cicilan'><span class='text-danger fa fa-close'></span></a>";

                }else{

                    $delete     = "";

                }

                $nilai_bayar    = number_format($isi->bayar);

                $isi_table  .=   "<tr>
                                    <td>
                                        {$no}
                                        <input type='hidden' name='dft_idbayar_trans[]' id='dft_idbayar_trans' value='{$isi->idbayar_trans_kasir}'>    
                                        <input type='hidden' name='dft_idmaster_trans[]' id='dft_idmaster_trans' value='{$isi->id_transaksi_master}'>  
                                        <input type='hidden' name='dft_waktu_trans[]' id='dft_waktu_trans' value='{$isi->created_on}'> 
                                    </td>
                                    <td class='text-center'>{$isi->tgl}</td>
                                    <td class='text-center'>{$isi->no_faktur}</td>
                                    <td class='text-center'>{$nilai_bayar}</td>
                                    <td class='text-center'>{$metode}</td>
                                    <td class='text-center'>{$isi->nm_lengkap}</td>
                                    <td class='text-center'>{$delete}</td>
                                </tr>";

            }

        }

        $return     =   [
                            'sisa'  => $sisa_piutang,
                            'tabel' => $isi_table
                        ];

        echo json_encode($return);

    }

    public function get_total_bayar_delete(){

        if(!$this->input->is_ajax_request()){

            redirect("kasir");

        }

        $idbayar    = $this->input->post("idbayar");
        $idmaster   = $this->input->post("idmaster");
        $waktu      = $this->input->post("waktu");

        // start cek apakah cicilan yang dihapus terdapat transaksi yang sudah dihistorikan

        $dt_cek_trans   = $this->bayar_trans_model
                                ->select("`bayar_trans_kasir`.`idbayar_trans_kasir`,
                                            `bayar_trans_kasir`.`id_transaksi`,
                                            `transaksi`.`st_trans_master`,
                                            `transaksi`.`st_history`")
                                ->join("transaksi","bayar_trans_kasir.id_transaksi = transaksi.id_transaksi")
                                ->where("`bayar_trans_kasir`.`created_on` >= '{$waktu}'
                                            AND `bayar_trans_kasir`.`deleted` = 0
                                            AND `bayar_trans_kasir`.`id_transaksi_master` = '{$idmaster}'
                                            AND transaksi.st_history = 1")
                                ->count_all();

        // end cek apakah cicilan yang dihapus terdapat transaksi yang sudah dihistorikan
        
        if($dt_cek_trans > 0){

            $pesan  = "<b>Error</b> <br>
                        Anda tidak diijinkan untuk menghapus cicilan pembayaran transaksi yang telah dihistorikan";

            $arr_hasil  =   [
                                'status'    => 0,
                                'pesan'     => $pesan
                            ];

        }else{

            $dt_bayar   = $this->bayar_trans_model
                            ->select("SUM(`bayar`) AS total_bayar_del")
                            ->where("created_on >= '{$waktu}'
                                    AND deleted = 0
                                    AND `id_transaksi_master` = '{$idmaster}'")
                            ->find_all();

            //echo $this->db->last_query();

            $total_uang = 0;

            if(is_array($dt_bayar) && count($dt_bayar)){

                $total_uang     = $dt_bayar[0]->total_bayar_del;

            }

            $status     = 1; // 1 sukses, 0 gagal

            if($total_uang <= 0){

                $status = 0;

            }

            $nominal    = number_format($total_uang);

            $pesan      = "Menghapus data cicilan pelunasan Transaksi, berarti : <br><br>
                            <b>1.</b> Semua cicilan pada waktu setelahnya juga akan dihapus.<br><br>
                            <b>2.</b> Kasir wajib mengembalikan uang bayar konsumen sebesar <b>Rp. ".$nominal."</b><br><br>
                            <b>Lanjutkan proses delete?</b>
                            <br>
                            <br>
                            <span class ='text-danger small'>Keterangan :</span>";

            $arr_hasil  =   [
                                'status'    => $status,
                                'pesan'     => $pesan
                            ];

        }

        echo json_encode($arr_hasil);

    }

    public function delete_cicilan(){

        if(!$this->input->is_ajax_request()){

            redirect("kasir");

        }

        $idbayar    = $this->input->post("idbayar");
        $idmaster   = $this->input->post("idmaster");
        $waktu      = $this->input->post("waktu");
        $ket        = $this->input->post("ket");


        // start ambil no.nota yang aktif

        $dt_nota    = $this->trans_model->select("no_faktur")
                            ->where("st_trans_master = 1
                                    AND deleted = 0
                                    AND `id_trans_ref_asli` = '{$idmaster}'")
                            ->find_all();


        if(is_array($dt_nota) && count($dt_nota)){

            $status         = 1;
            $no_faktur_ket  = $dt_nota[0]->no_faktur;

            $ket_kasir      = "Pengembalian Pembayaran, kepada konsumen untuk aktivitas delete transaksi. Untuk Nota Master : ".$no_faktur_ket;

        }else{

            $status     = 0;

        }

        // end ambil no.nota yang aktif

        if($status == 1){            

            $user       = $this->auth->userdata();
            $id_user    = $user->id_user;

            $this->db->trans_start();

            $del    = delete_pembayaran($st_delete_by = 2,
                                            $id_primary_bayar = '',
                                            $id_transaksi_master = $idmaster,
                                            $waktu_start_delete = $waktu,
                                            $keterangan_delete = $ket,
                                            $keterangan_kas_kasir = $ket_kasir);

            if(!$del){

                $status = 0;

            }

            $this->db->trans_complete();            

            if($this->db->trans_status() == false){

                $status = 0;

            }else{

                $this->db->trans_start();    

                //start update transaksi dengan status belum lunas

                $arr_update     =   [
                                        'st_lunas'  => 0
                                    ];

                $where_lunas    = "`id_trans_ref_asli` = '{$idmaster}' 
                                    AND `st_history` = 0
                                    AND `deleted` = 0";

                $this->trans_model->update_where($where_lunas,null,$arr_update);

                //end update transaksi dengan status belum lunas            

                $this->db->trans_complete();

                if($this->db->trans_status() == false){

                    $status = 0;

                }else{

                    $status = 1;

                }


            }


        }

        echo $status;


    }

    /*public function delete_cicilan(){

        if(!$this->input->is_ajax_request()){

            redirect("kasir");

        }

        $idbayar    = $this->input->post("idbayar");
        $idmaster   = $this->input->post("idmaster");
        $waktu      = $this->input->post("waktu");
        $ket        = $this->input->post("ket");

        // start cek jml total pembayaran yang didelete

        $dt_bayar   = $this->bayar_trans_model
                            ->select("SUM(`bayar`) AS total_bayar_del")
                            ->where("created_on >= '{$waktu}'
                                    AND deleted = 0
                                    AND `id_transaksi_master` = '{$idmaster}'")
                            ->find_all();

        $total_uang = 0;

        if(is_array($dt_bayar) && count($dt_bayar)){

            $total_uang     = $dt_bayar[0]->total_bayar_del;

        }

        $status     = 1; // 1 sukses, 0 gagal

        if($total_uang <= 0){

            $status = 0;

        }

        // end cek jml total pembayaran yang didelete

        // start ambil no.nota yang aktif

        $dt_nota    = $this->trans_model->select("no_faktur")
                            ->where("st_trans_master = 1
                                    AND deleted = 0
                                    AND `id_trans_ref_asli` = '{$idmaster}'")
                            ->find_all();


        if(is_array($dt_nota) && count($dt_nota)){

            $status         = 1;
            $no_faktur_ket  = $dt_nota[0]->no_faktur;

        }else{

            $status     = 0;

        }

        // end ambil no.nota yang aktif


        if($status == 1){            

            $user       = $this->auth->userdata();
            $id_user    = $user->id_user;

            $where      = "created_on >= '{$waktu}'
                            AND `id_transaksi_master` = '{$idmaster}'";        

            $this->db->trans_start();

                $arr_update     =   [
                                        'deleted'       => 1,
                                        'deleted_by'    => $id_user,
                                        'ket_delete'    => $ket
                                    ];    


                $this->bayar_trans_model->update_where($where,null,$arr_update);

                $ket    = "Pengembalian Pembayaran, kepada konsumen untuk aktivitas delete transaksi. Untuk Nota Master : ".$no_faktur_ket;

                set_alur_kas_kasir($ket, 0, 0, $total_uang);            

            $this->db->trans_complete();        

            if($this->db->trans_status() == false){

                $status = 0;

            }else{// jika transaksi database sukses

                //$status = 1;

                $this->db->trans_start();    

                //start update transaksi dengan status belum lunas

                $arr_update     =   [
                                        'st_lunas'  => 0
                                    ];

                $where_lunas    = "`id_trans_ref_asli` = '{$idmaster}' 
                                    AND `st_history` = 0
                                    AND `deleted` = 0";

                $this->trans_model->update_where($where_lunas,null,$arr_update);

                //end update transaksi dengan status belum lunas            

                $this->db->trans_complete();

                if($this->db->trans_status() == false){

                    $status = 0;

                }else{

                    $status = 1;

                }

            }

        }

        echo $status;


    }*/

    public function simpan_cicilan(){

        if(!has_permission($this->addCicilan)){

            echo 0;

            return false;

        }

        // start cek saldo kas kasir

        $cek_kas_kasir  = cek_saldo_kasir();
        if(!$cek_kas_kasir){

            echo 0;

            return false;

        }

        // end cek saldo kas kasir

        // start variable post dari form

        $cicilan        = str_replace(",", "", $this->input->post('cicilan'));
        $bayar          = str_replace(",", "", $this->input->post('bayar'));
        $metode         = $this->input->post('metode_bayar');
        $no_faktur      = $this->input->post("no_faktur");
        $id_transaksi   = $this->input->post("id_transaksi");
        $grand_total    = $this->input->post("gt");

        // end variable post dari form

        // start cek kondisi nota, apakah masih aktiv atau tidak

        $dt_cek     = $this->trans_model
                            ->select("id_transaksi")
                            ->where("deleted = 0
                                        AND st_history = 0
                                        AND id_transaksi = '{$id_transaksi}'
                                    ")
                            ->count_all();

        if($dt_cek <= 0){ // 0 berarti transaksi bisa saja dihapus / ref nota.

            //$this->template->set_message(lang("label-err-trans-hilang"),"error");

            echo 0;
            return false;

        }

        

        // end cek kondisi nota, apakah masih aktiv atau tidak

        $this->db->trans_start();

        $dt_trans           = $this->trans_model
                                    ->select("id_trans_ref_asli")
                                    ->find($id_transaksi);

        $id_trans_master    = $dt_trans->id_trans_ref_asli;

        $ket_kas_kasir      = "Pelunasan Cicilan Transaksi No. Nota : ".$no_faktur;

        $simpan_bayar = set_bayar_transaksi($st_transaksi = 0,
                                                $id_transaksi = $id_transaksi,
                                                $st_bayar_trans = 0,
                                                $st_sumber = 0,
                                                $st_bayar = $metode,
                                                $total_transaksi = $grand_total,
                                                $tagihan = $cicilan,
                                                $bayar  = $bayar,
                                                $st_dp = 0,
                                                $lokasi_file = "",
                                                $id_transaksi_master = $id_trans_master,
                                                $keterangan_kas_masuk = $ket_kas_kasir);

        if(!$simpan_bayar){

            //$this->template->set_message(lang("label-err-cicilan-simpan"),"error");
            echo 0;
            return false;

        }

        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            $return = 0;

        }else{

            // start ubah status lunas jika semua tagihan telah terbayarkan
            $this->db->trans_start();

            $dt_cek_lunas   = $this->trans_model
                                    ->select("`transaksi`.`id_transaksi`,
                                                `transaksi`.`id_trans_ref_asli`,
                                                `transaksi`.`st_kasir`,
                                                `transaksi`.`grand_total`,
                                                `bayar`.`total_bayar`,
                                                if(bayar.total_bayar < `transaksi`.`grand_total` or bayar.total_bayar is null, 0, 1) as st_lunas")
                                    ->join("(SELECT 
                                                    `id_transaksi_master` AS id, SUM(`bayar`) AS total_bayar
                                                FROM
                                                    bayar_trans_kasir
                                                WHERE
                                                    deleted = 0
                                                GROUP BY id_transaksi_master) AS bayar","`transaksi`.`id_trans_ref_asli` = `bayar`.`id`","left")
                                    ->where("`transaksi`.`id_transaksi` = '{$id_transaksi}'")
                                    ->find_all();

            $st_trans_lunas     = 0;

            if(is_array($dt_cek_lunas) && count($dt_cek_lunas)){

                $st_trans_lunas     = $dt_cek_lunas[0]->st_lunas;
                $id_master          = $dt_cek_lunas[0]->id_trans_ref_asli;

                $arr_update     =   [
                                        'st_lunas'  => $st_trans_lunas
                                    ];

                $this->trans_model->update_where('id_trans_ref_asli',$id_master,$arr_update);

            }else{

                $return = 0;

            }


            $this->db->trans_complete();

            if($this->db->trans_status() == false){

                $return = 0;

            }else{

                $return = 1;

            }    

            // end ubah status lunas jika semua tagihan telah terbayarkan            

        }


        echo $return;

    }

    public function cicilan_bayar($id_transaksi){

        $this->auth->restrict($this->viewCicilan);


        $cek_kas_kasir  = cek_saldo_kasir();
        if(!$cek_kas_kasir){

            $this->template->set_message(lang("label-err-saldo-tidak-ada"),"warning");
            $this->template->set("saldo_kosong",0);

        }


        /*if(has_permission($this->addCicilan) && isset($_POST['simpan'])){

            $simpan     = $this->simpan_cicilan($id_transaksi);

            if($simpan){

                $this->template->set_message(lang("label_simpan_cicilan_sukses"),"success");

            }

        }*/

        // header informasi 

        $dt_info    = $this->trans_model
                            ->select("`transaksi`.`id_transaksi`,
                                        date_format(`transaksi`.`waktu`,'%d/%m/%Y %H:%i:%s') as tgl,
                                        `transaksi`.`no_faktur`,
                                        `transaksi`.`st_trans_master`,
                                        `t_master`.`no_faktur` AS no_faktur_master,
                                        `transaksi`.`id_trans_ref_asli`,
                                        `transaksi`.`st_kasir`,
                                        `konsumen`.`panggilan`,
                                        `konsumen`.`nama`,
                                        `konsumen`.`st`,
                                        `transaksi`.`grand_total`,
                                        bayar.total_bayar,
                                        (`transaksi`.`grand_total` - IF(bayar.total_bayar IS NULL,
                                            0,
                                            bayar.total_bayar)) AS sisa_piutang")
                            ->join("`order_produk`","`transaksi`.`id_order` = `order_produk`.`id_order`","inner")
                            ->join("`konsumen`","`konsumen`.`idkonsumen` = `order_produk`.`id_konsumen`")
                            ->join("(SELECT 
                                            `id_transaksi` AS id, `no_faktur`
                                        FROM
                                            transaksi
                                        WHERE
                                            deleted = 0) AS t_master","`t_master`.`id` = `transaksi`.`id_trans_ref_asli`","inner")
                            ->join("(SELECT 
                                        `id_transaksi_master` AS id, SUM(`bayar`) AS total_bayar
                                    FROM
                                        bayar_trans_kasir
                                    WHERE
                                        deleted = 0
                                    GROUP BY id_transaksi_master) AS bayar","transaksi.id_trans_ref_asli = bayar.id","left")
                            //->where("transaksi.deleted = 0 and st_history = 0 and id_transaksi = '{$id_transaksi}'")
                            ->find($id_transaksi);
                            //->find_all();

        $id_ref_master  = $dt_info->id_trans_ref_asli;

        //echo $this->db->last_query();

        $dt_bayar   = $this->trans_model
                            ->select("`transaksi`.`id_transaksi`,
                                        `transaksi`.`no_faktur`,
                                        `bayar_trans_kasir`.`idbayar_trans_kasir`,
                                        `bayar_trans_kasir`.`id_transaksi_master`,
                                        date_format(`bayar_trans_kasir`.`created_on`,'%d/%m/%Y %H:%i:%s') as tgl,
                                        `bayar_trans_kasir`.`created_on`,
                                        `bayar_trans_kasir`.`bayar`,
                                        `bayar_trans_kasir`.`st_bayar`,
                                        `users`.`nm_lengkap`")
                            ->join("bayar_trans_kasir","transaksi.id_transaksi = bayar_trans_kasir.id_transaksi","inner")
                            ->join("users","bayar_trans_kasir.created_by = users.id_user","inner")
                            ->where("transaksi.deleted = 0
                                        AND bayar_trans_kasir.deleted = 0
                                        AND bayar_trans_kasir.id_transaksi_master = '{$id_ref_master}'")
                            ->order_by("bayar_trans_kasir.created_on")
                            ->find_all();

        
        
        $asset  =   array(
                            "plugins/select2/js/select2.js",
                            "plugins/select2/css/select2.css",
                            "plugins/number/jquery.number.js",
                            "kasir/assets/js/form_cicilan.js"
                    );

        add_assets($asset);

        $this->template->set("dt_bayar",$dt_bayar);
        $this->template->set("dt_trans",$dt_info);
        $this->template->set("toolbar_title", lang('title_cicilan'));
        $this->template->set("page_title", lang('title_cicilan'));
        $this->template->title(lang('title_cicilan'));
        $this->template->page_icon('fa fa-list-ol');
        $this->template->render('form_cicilan_trans');              

    }

    public function view($id_transaksi){

        if(isset($_POST['st_delete']) && $_POST['st_delete'] == 1 && has_permission($this->deletePermission)){

            $st_proses_delete   = $this->delete($id_transaksi);

            if($st_proses_delete){

                $this->template->set_message(lang("konfirmasi-delete-sukses"),"success");
                redirect("kasir");

            }

        }        

        $data   = $this->session->userdata('frm_trans_kasir');
        if(!$data){

            $this->set_session_edit_view($id_transaksi,1);
            $data   = $this->session->userdata('frm_trans_kasir');

        }

        //print_r($data);

        $this->session->unset_userdata('frm_trans_kasir');        

        $asset  =   array(
                            "plugins/select2/js/select2.js",
                            "plugins/select2/css/select2.css",
                            "plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
                            "plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
                            "plugins/number/jquery.number.js",
                            "kasir/assets/js/form_trans.js",
                            "kasir/assets/css/css_list.css"
                    );

        add_assets($asset);

        /* cek saldo kas kasir */
        //$cek_kas_kasir  = $this->cek_saldo_kasir();
        $cek_kas_kasir  = cek_saldo_kasir();
        if(!$cek_kas_kasir){

            $this->template->set_message(lang("label-err-saldo-tidak-ada"),"warning");
            $this->template->set("saldo_kosong",0);

        }

        $this->template->set("dt",$data);

        $this->template->set("toolbar_title", lang('title_view'));
        $this->template->set("page_title", lang('title_view'));
        $this->template->title(lang('title_pelunasan'));
        $this->template->page_icon('fa fa-money');
        $this->template->render('form_trans');              

    }

    public function create($id){

    	/* transaksi dibuat dari data order */

        $this->auth->restrict($this->addPermission);

    	if(isset($_POST['simpan']) && has_permission($this->addPermission)){
        

            $st_simpan  = $this->save($id);

    		if($st_simpan){

    			$this->template->set_message(lang('label_simpan_sukses'),"success");

                $this->session->unset_userdata('frm_trans_kasir');

    			redirect('kasir/index/'.$this->id_primary);

    		}

    	}

    	$data 	= $this->session->userdata('frm_trans_kasir');
    	if(!$data){

    		$this->set_session_trans(0,$id,null);
    		$data 	= $this->session->userdata('frm_trans_kasir');

    	}

        $this->session->unset_userdata('frm_trans_kasir');

    	$asset 	= 	array(
    						"plugins/select2/js/select2.js",
                            "plugins/select2/css/select2.css",
                            "plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
                        	"plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
                        	"plugins/number/jquery.number.js",
                            "kasir/assets/js/form_trans.js",
                            "kasir/assets/css/css_list.css"
    				);

    	add_assets($asset);

        /* cek saldo kas kasir */
        //$cek_kas_kasir  = $this->cek_saldo_kasir();
        $cek_kas_kasir  = cek_saldo_kasir();
        if(!$cek_kas_kasir){

            $this->template->set_message(lang("label-err-saldo-tidak-ada"),"warning");
            $this->template->set("saldo_kosong",0);

        }

    	$this->template->set("dt",$data);

		$this->template->set("toolbar_title", lang('title_new'));
        $this->template->set("page_title", lang('title_new'));
        $this->template->title(lang('title_new'));
        $this->template->page_icon('fa fa-money');
        $this->template->render('form_trans');     	    	

    }

    public function pelunasan($id){

        $this->auth->restrict($this->viewPermission);        

        if(isset($_POST['simpan']) && has_permission($this->addPermission)){

            if($this->save_pelunasan($id)){

                $this->template->set_message(lang('label_simpan_sukses'),"success");
                $this->session->unset_userdata('frm_trans_kasir');

                $id_cetak   = $this->id_primary;
                redirect('kasir/index/'.$id_cetak."/1");

            }

        }
        
        /* Isi datanya */
        $data   = $this->session->userdata('frm_trans_kasir');
        if(!$data){

            $this->set_session_edit_view($id,0);
            $data   = $this->session->userdata('frm_trans_kasir');

        }

        $this->session->unset_userdata('frm_trans_kasir');        

        $asset  =   array(
                            "plugins/select2/js/select2.js",
                            "plugins/select2/css/select2.css",
                            "plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
                            "plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
                            "plugins/number/jquery.number.js",
                            "kasir/assets/js/form_trans.js",
                            "kasir/assets/css/css_list.css"
                    );

        add_assets($asset);

        /* cek saldo kas kasir */
        //$cek_kas_kasir  = $this->cek_saldo_kasir();
        $cek_kas_kasir  = cek_saldo_kasir();
        if(!$cek_kas_kasir){

            $this->template->set_message(lang("label-err-saldo-tidak-ada"),"warning");
            $this->template->set("saldo_kosong",0);

        }

        $this->template->set("dt",$data);

        $this->template->set("toolbar_title", lang('title_view'));
        $this->template->set("page_title", lang('title_view'));
        $this->template->title(lang('title_pelunasan'));
        $this->template->page_icon('fa fa-money');
        $this->template->render('form_trans');              

    }

    


    public function get_noFaktur($prefix = "VLB"){

        $no_faktur  = "";

        $bulan      = date("m");
        $tahun      = date("Y");
        $day        = date("d");

        $where      = "DAY(`created_on`) = {$day} AND 
                        MONTH(`created_on`) = {$bulan}
                        AND YEAR(`created_on`) = {$tahun}";

        $data       = $this->trans_model->select('no_faktur')
                                        ->where($where)
                                        ->order_by("created_on","desc")
                                        ->limit(1,0)
                                        ->find_all();


        if(is_array($data) && count($data)){

            $no     = explode("-", $data[0]->no_faktur);
            if(count($no) == 2){

                $no = intval($no[1]) + 1;

            }else{


                $no = 1;

            }


        }else{

            $no = 1;

        }
        
        $no_faktur  = $prefix."/".$tahun."/".$bulan."/".$day."-".str_pad($no, 5,"0",STR_PAD_LEFT);

        return $no_faktur;

    }

    private function get_kode_worksheet($id_detail_order = ""){

        $data   = $this->order_detail_model->select("`produk`.`id_worksheet` as id")
                                    ->join("produk","order_produk_detail.id_produk = produk.idproduk","inner")
                                    ->where("order_produk_detail.id_detail_produk_order = '$id_detail_order'")
                                    ->find_all();

        if(is_array($data) && count($data)){

            return $data[0]->id;

        }else{

            return false;

        }

    }

    private function cek_order_po($id_order = ''){

        /*
            jika transaksi merupakan PO, apapun alasannya pembayaran harus lunas, tidak boleh 0
            return
            True    = jika order merupakan order PO dan Tidak diperbolehkan untuk dibayar tempo.
            false   = jika order biasa / tempo yang diijinkan untuk dibayar tempo.
        */

        $data_cek   = $this->order_model->select('st_po,st_acc_po_tempo')->find($id_order);
        $st_po      = $data_cek->st_po;
        $st_tempo   = $data_cek->st_acc_po_tempo;

        if($st_po == 1 && $st_tempo == 0){

            return true;

        }else{

            return false;
        
        }

    }

    private function cek_order_trans($id_order = ""){

        /*
            Melakukan pengecekan apakah Order sudah dibuatkan transaksi,
            sehingga meminimalkan save 2 Nota untuk 1 Order.
        */

        $data   = $this->order_model->where("deleted = 0 
                                            and `st_simpan` = 1 
                                            and `st_kasir` = 1 
                                            and `id_order` = '{$id_order}'")
                        ->count_all();

        if($data <= 0){

            return true; // Order belum dilakukan proses kasir

        }else{

            return false; // order sudah dilakukan proses kasir

        }

    }

    public function save_pelunasan($id_transaksi = ""){

        /* start cek saldo kas kasir */
        
        //$cek_kas_kasir  = $this->cek_saldo_kasir();
        $cek_kas_kasir  = cek_saldo_kasir();

        if(!$cek_kas_kasir){

            $this->template->set_message(lang("label-err-saldo-tidak-ada"),"error");

        }        
        /* end cek saldo kas kasir */        


        /* start form validation */
        if($_POST['metode_bayar'] > 0 || (isset($_POST['dp']) && $_POST['dp'] > 0)){

            $this->form_validation->set_rules("bayar","lang:capt-trans-bayar","required");

        }

        $this->form_validation->set_rules("metode_bayar","lang:capt-trans-metode-bayar","required");        

        if($this->form_validation->run() === false){

            $this->set_session_trans(1,$id,null);
            $this->template->set_message(validation_errors(),'error');
            return false;

        }
        /* end form validation */

        // validasi agar bayar harus diisi lunas
        $cek_bayar      = doubleval(str_replace(",", "",$_POST['bayar']));
        $cek_deposit    = doubleval(str_replace(",", "",$_POST['isi_deposit_pakai']));

        if($cek_bayar > 0 || $cek_deposit > 0){

            if(doubleval($_POST['isi_kurang_bayar']) > 0 ){

                $this->set_session_edit_view($id_transaksi,0);
                $this->template->set_message(lang('label-err-bayar-kurang'),'error');
                return false;            

            }

        }

        $id_detail_order            = $this->input->post("id_detail_order");
        $st_ubah                    = $this->input->post("dft_st_ubah");
        $nm_pekerjaan               = $this->input->post("dft_nm_pekerjaan");
        $kategori                   = $this->input->post("dft_kategori");
        $produk                     = $this->input->post("dft_produk");
        $st_type_produk             = $this->input->post("dft_st_type_produk");
        $uk_p                       = $this->input->post("dft_uk_p");
        $uk_l                       = $this->input->post("dft_uk_l");
        $id_satuan_uk               = $this->input->post("dft_idsatuan_uk");
        $tmpl_uk                    = $this->input->post("dft_tmpl_satuan_uk");
        $st_finishing               = $this->input->post("dft_st_finishing");
        $jml_cetak                  = $this->input->post("dft_jml_cetak");
        $img_thumbnail              = $this->input->post("img_thumbnail");
        $st_urgent                  = $this->input->post("st_urgent");
        $tgl_selesai                = $this->input->post("tgl_selesai");

        $tampil_harga               = $this->input->post("dft_tampil_harga");
        $harga_asli                 = $this->input->post("dft_rp_harga_asli");
        $tmpl_satuan_harga          = $this->input->post("dft_tmpl_satuan_harga");
        $id_satuan_harga            = $this->input->post("dft_id_satuan_harga");

        $harga_design_asli          = $this->input->post("dft_rp_design_asli");
        $harga_cetak_asli           = $this->input->post("dft_rp_cetak_asli");
        $harga_finishing_asli       = $this->input->post("dft_rp_finishing_asli");
        
        $diskon                     = $this->input->post("dft_diskon");                  
        $pot_kecil                  = $this->input->post("dft_pot_kecil");                   
        $potongan                   = $this->input->post("dft_potongan");                   

        $subtotal_asli              = $this->input->post("dft_subtotal_asli");
        
        $harga_baru                 = $this->input->post("dft_rp_harga_baru");
        $harga_cetak_baru           = $this->input->post("dft_rp_cetak_baru");
        $harga_design_baru          = $this->input->post("dft_rp_design_baru");
        $harga_finishing_baru       = $this->input->post("dft_rp_finishing_baru");
        $subtotal_baru              = $this->input->post("dft_subtotal_baru");                    
        // data header
        $id_konsumen                = $this->input->post("isi_id_konsumen");
        $total_all                  = $this->input->post("isi_total_order");
        $diskon_all                 = $this->input->post("diskon_all");
        $pot_all                    = $this->input->post("pot_all");
        $pajak_all                  = $this->input->post("pajak_all");
        $grand_total                = $this->input->post("isi_grandtotal");

        $dp                         = str_replace(",", "", $this->input->post("dp"));

        $deposit                    = isset($_POST['isi_deposit_pakai']) ? $this->input->post('isi_deposit_pakai') : 0;
        $bayar                      = str_replace(",", "", $this->input->post("bayar"));
        $st_produksi                = $this->input->post("st_produksi");
        
        $st_bayar                   = $this->input->post("metode_bayar");

        $kurang_bayar               = $this->input->post("isi_kurang_bayar");
        $lebih_bayar                = $this->input->post("isi_lebih_bayar");

        $cek_size_file              = $_FILES['file_att']['size'];
        $cek_error_file             = $_FILES['file_att']['error'];

        // start cek kondisi deposit yang dipakai

        $deposit_cek    = get_deposit($id_konsumen);
        //echo $deposit_cek."-".$deposit;

        if($deposit_cek < $deposit){

            $this->set_session_edit_view($id_transaksi,0);
            $this->template->set_message(lang('label-err-deposit'),'warning');
            return false;

        }

        // end cek kondisi deposit yang dipakai        

        /* cek apakah order direvisi atau belum */                    
        $cek     = $this->trans_model
                            ->select("`transaksi`.`id_transaksi`,
                                        `transaksi`.`no_faktur`,
                                        `transaksi`.`st_trans_master`,
                                        `transaksi`.`id_trans_ref_asli`,
                                        `transaksi`.`id_order`,
                                        `transaksi`.`st_history`,
                                        `order_produk`.`st_revisi`")
                            ->join("order_produk","transaksi.id_order = order_produk.id_order","inner")
                            ->where("`transaksi`.`id_transaksi` = '{$id_transaksi}'")
                            ->find_all();

        $st_revisi          = $cek[0]->st_revisi;  
        $id_order           = $cek[0]->id_order;
        $id_trans_master    = $cek[0]->id_trans_ref_asli;

        /*
            jika status revisinya 0 / 2, maka hanya pelunasan biasa.
            jika status revisinya 1, maka load ulang data ordernya.
        */

        /* start ambil data no_faktur masternya */
        $dt_trans_m         = $this->trans_model
                                    ->select("no_faktur,
                                                st_metode_bayar,
                                                lama_tempo, 
                                                tgl_tempo")
                                    ->find($id_trans_master);

        $no_faktur_master   = $dt_trans_m->no_faktur;
        $st_metode_bayar    = $dt_trans_m->st_metode_bayar;
        $lama_tempo         = $dt_trans_m->lama_tempo;
        $tgl_tempo          = $dt_trans_m->tgl_tempo;
        /* end ambil data no_faktur masternya */

        /*
            ambil semua data pembayaran / dp yang sudah masuk
        */
        $dt_bayar = $this->bayar_trans_model->select("sum(`bayar`) as total")
                                ->where("`id_transaksi_master` = '{$id_trans_master}'
                                            AND deleted = 0")
                                ->find_all();

        $total_all_sudah_bayar  = 0;

        if(is_array($dt_bayar) && count($dt_bayar)){

            $total_all_sudah_bayar  = $dt_bayar[0]->total;            

        }

        if($st_revisi == 1){

            $id_primary     = gen_primary("","transaksi","id_transaksi");
            $no_nota        = $this->get_noFaktur();  

        }else{

            $no_nota        = $no_faktur_master;

        }
        
        $sql_all = "";

        $lokasi_file    = "";

        if($cek_size_file > 0 && $cek_error_file == 0 && $st_bayar > 0){

            $nm_file    = str_replace("/","_", $no_nota);
            $nm_file    = str_replace("-","_", $no_nota);

            $config['upload_path']          = './upload/transfer';
            $config['allowed_types']        = 'jpg|jpeg|png';
            $config['max_size']             = 200;
            $config['overwrite']            = TRUE;
            $config['file_name']            = $nm_file.".jpg";

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file_att')){
                
                $pesan_error = lang('label-err-lampiran')."<br>".$this->upload->display_errors();
                $this->template->set_message($pesan_error,'error');
                $this->set_session_edit_view($id_transaksi,0);
                return false;

            }else{

                $upload_data = $this->upload->data();

                $lokasi_file  = "/upload/transfer/".$upload_data['file_name'];

            }
        }


        /* start simpan transaksi */
        
        if($st_revisi == 1){

            $hasil  = $this->cek_order_po($id_order);
            if($hasil){

                if($kurang_bayar > 0){

                    $this->template->set_message(lang('label-err-bayar-po-kurang'),'error');
                    return false;

                }

            }
            
            $this->db->trans_start();

            // simpan head transaksi revisi

            // start lock transaksi dengan master yang sama jika ordernya direvisi

            $arr_lock_trans     = ['st_history'=>1];
            $this->trans_model->update_where('id_trans_ref_asli',$id_trans_master, $arr_lock_trans);
            $sql_all    .= "\n\n".$this->db->last_query();

            // end lock transaksi dengan master yang sama jika ordernya direvisi

            // update status pakai ref pada master transaksi
            $arr_st_pakai_ref   = ['st_ref'=>1];
            $this->trans_model->update($id_trans_master, $arr_st_pakai_ref);
            $sql_all    .= "\n\n".$this->db->last_query();


            $arr_head   =   [
                                'id_transaksi'      => $id_primary, 
                                'waktu'             => date("Y-m-d H:i:s"),
                                'no_faktur'         => $no_nota, 
                                'id_order'          => $id_order,
                                'st_trans_master'   => 0,
                                'id_trans_ref_asli' => $id_trans_master, 
                                'lama_tempo'        => $lama_tempo, 
                                'tgl_tempo'         => $tgl_tempo, 
                                'total'             => $total_all, 
                                'diskon'            => $diskon_all, 
                                'potongan'          => $pot_all, 
                                'pajak'             => $pajak_all, 
                                'grand_total'       => $grand_total, 
                                'st_metode_bayar'   => $st_metode_bayar, 
                                'nilai_dp'          => $dp, 
                                'bayar'             => $bayar, 
                                'kurang_bayar'      => 0, 
                                'lebih_bayar'       => $lebih_bayar, 
                                'st_produksi'       => 1, 
                                'st_lunas'          => $kurang_bayar <= 0 ? 1 : 0
                            ];  

            $this->trans_model->insert($arr_head);
            $sql_all    .= "\n\n".$this->db->last_query();

            // simpan detail     

            $arr_detail = [];
        
            foreach ($id_detail_order as $key => $isi) {
                
                // array detail transaksi
                $arr_detail[]   =   [
                                        'id_transaksi_detail'       => gen_primary("","transaksi_detail","id_transaksi_detail"), 
                                        'id_transaksi'              => $id_primary, 
                                        'id_detail_produk_order'    => $isi, 
                                        'harga_by_satuan'           => $harga_baru[$key], 
                                        'satuan_harga_by'           => $id_satuan_harga[$key],   
                                        'biaya_cetak'               => $harga_cetak_baru[$key], 
                                        'biaya_design'              => $harga_design_baru[$key], 
                                        'biaya_finishing'           => $harga_finishing_baru[$key], 
                                        'diskon'                    => $diskon[$key], 
                                        'pot_kecil'                 => $pot_kecil[$key],
                                        'potongan'                  => $potongan[$key], 
                                        'subtotal'                  => $subtotal_baru[$key], 
                                        'st_ubahan'                 => $st_ubah[$key]
                                    ];

            }

            $this->trans_detail_model->insert_batch($arr_detail);
            $sql_all .= "\n\n".$this->db->last_query();
            /* end insert detail transaksi */

            // start simpan Pembayaran

            if($deposit > 0){

                $status_dp  = 0;
                $keterangan = "Pelunasan transaksi revisi dengan deposit untuk transaksi : ".$no_nota;

                set_bayar_transaksi(0, 
                                    $id_primary, 
                                    0, 
                                    1, 
                                    3, 
                                    $grand_total, 
                                    $deposit, 
                                    $deposit, 
                                    $status_dp,
                                    $lokasi_file,
                                    $id_trans_master,
                                    $keterangan
                                );

                // kurangi nilai deposit
                use_deposit($id_konsumen = $id_konsumen, $nilai = $deposit, $ket = $keterangan);


                if($bayar > 0){

                    $bayar_asli     = $bayar - $lebih_bayar;
                    $status_dp      = 0;
                    $keterangan     = "Pelunasan transaksi revisi untuk transaksi : ".$no_nota;                    

                    set_bayar_transaksi(
                                        0,
                                        $id_primary,
                                        0,
                                        0,
                                        $st_bayar,
                                        $grand_total,
                                        $bayar_asli,
                                        $bayar,
                                        $status_dp,
                                        $lokasi_file,
                                        $id_trans_master,
                                        $keterangan,
                                        1
                                    );

                }

            }else{

                //if($bayar > 0){

                    $tagihan    = $bayar > 0 ? $bayar - $lebih_bayar : 0;

                    $ket        = "Pelunasan transaksi revisi untuk transaksi : ". $no_nota;

                    $st_dp      = 0;

                    set_bayar_transaksi(
                                        0,
                                        $id_primary,
                                        0,
                                        0,
                                        $st_bayar,
                                        $grand_total,
                                        $tagihan,
                                        $bayar,
                                        $st_dp,
                                        $lokasi_file,
                                        $id_trans_master,
                                        $ket
                                    );


                //}

            }
            // end simpan Pembayaran        
    

            /* start update data order */
            $arr_update_order   =   ['st_revisi' => 2];
            $this->order_model->update($id_order , $arr_update_order);
            $sql_all    .= "\n\n".$this->db->last_query();                    
            /* end update data order */

            $this->db->trans_complete();
    
            if($this->db->trans_status() == false){

                $return         = false;
                $keterangan     = "Gagal, simpan data transaksi referensi, id = " . $id_primary;
                $total          = 0;
                $status         = 0;

            }else{

                $return         = true;
                $keterangan     = "Sukses, simpan data transaksi referensi, id = " . $id_primary;
                $total          = $bayar;
                $status         = 0;

                $this->id_primary   = $id_primary;
                $this->no_nota      = $no_nota;

            }

            $nm_hak_akses   = $this->addPermission; 
            $kode_universal = $id_primary;
            $jumlah         = $bayar;
            $sql            = $sql_all;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $total, $sql, $status);

            return $return;            

        }else{ // st_revisi == null / 0 / 2, pelunasan biasa tidak ganti nota

            if($kurang_bayar > 0){

                $this->template->set_message(lang('label-err-bayar-kurang'),'error');
                return false;

            }    

            $this->db->trans_start();

            /* start update data transaksi */
            $arr_update_trans   =   [
                                       'bayar2'         => $bayar, 
                                       'lebih_bayar'    => $lebih_bayar, 
                                       'st_lunas'       => $kurang_bayar <= 0 ? 1 : 0
                                    ];

            $this->trans_model->update($id_transaksi, $arr_update_trans);
            $sql_all    .= "\n\n". $this->db->last_query();

            /* end update data transaksi */            

            // start simpan Pembayaran

            if($deposit > 0){

                $status_dp  = 0;
                $keterangan = "Pelunasan transaksi dengan deposit untuk transaksi : ".$no_nota;

                set_bayar_transaksi(0, 
                                    $id_transaksi, 
                                    0, 
                                    1, 
                                    3, 
                                    $grand_total, 
                                    $deposit, 
                                    $deposit, 
                                    $status_dp,
                                    $lokasi_file,
                                    $id_trans_master,
                                    $keterangan
                                );

                // kurangi nilai deposit
                use_deposit($id_konsumen = $id_konsumen, $nilai = $deposit, $ket = $keterangan);


                if($bayar > 0){

                    $bayar_asli     = $bayar - $lebih_bayar;
                    $status_dp      = 0;

                    set_bayar_transaksi(
                                        0,
                                        $id_transaksi,
                                        0,
                                        0,
                                        $st_bayar,
                                        $grand_total,
                                        $bayar_asli,
                                        $bayar,
                                        $status_dp,
                                        $lokasi_file,
                                        $id_trans_master,
                                        $keterangan,
                                        1
                                    );

                }

            }else{

                if($bayar > 0){

                    $tagihan    = $bayar - $lebih_bayar;

                    $ket        = "Pelunasan transaksi untuk transaksi : ". $no_nota;

                    $st_dp      = 0;

                    set_bayar_transaksi(
                                        0,
                                        $id_transaksi,
                                        0,
                                        0,
                                        $st_bayar,
                                        $grand_total,
                                        $tagihan,
                                        $bayar,
                                        $st_dp,
                                        $lokasi_file,
                                        $id_trans_master,
                                        $ket
                                    );


                }

            }
            // end simpan Pembayaran        

            $this->db->trans_complete();
    
            if($this->db->trans_status() == false){

                $return         = false;
                $keterangan     = "Gagal, simpan data pelunasan transaksi id = " . $id_transaksi;
                $total          = 0;
                $status         = 0;

            }else{

                $return         = true;
                $keterangan     = "Sukses, simpan data pelunasan transaksi id = " . $id_transaksi;
                $total          = $bayar;
                $status         = 0;

                $this->id_primary   = $id_transaksi;
                $this->no_nota      = $no_nota;

            }

            $nm_hak_akses   = $this->addPermission; 
            $kode_universal = $id_transaksi;
            $jumlah         = $bayar;
            $sql            = $sql_all;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $total, $sql, $status);

            return $return;                        

        }

        /* end simpan data transaksi */            

    }

    public function save($id = ""){ // untuk transaksi master

        /* start cek saldo kas kasir */
        
        //$cek_kas_kasir  = $this->cek_saldo_kasir();
        $cek_kas_kasir  = cek_saldo_kasir();
        if(!$cek_kas_kasir){

            $this->template->set_message(lang("label-err-saldo-tidak-ada"),"error");

        }        
        /* end cek saldo kas kasir */

        /* start cek list kosong / isi */
        if(!isset($_POST['id_detail_order'])){

            $this->template->set_message(lang('label-err-order-kosong'),'error');
            $this->set_session_trans(1,$id,null);
            return false;
        }
        /* end cek list kosong / isi */

        // start cek status transaksi kasir untuk ordernya
        $cek    = $this->cek_order_trans($id);
        if(!$cek){

            $this->template->set_message(lang('label-err-st-kasir-order'),'error');
            redirect('kasir');
            return false;

        }
        // end cek status transaksi kasir untuk ordernya


        /* start  */
        $id_detail_order            = $this->input->post("id_detail_order");
        $st_ubah                    = $this->input->post("dft_st_ubah");
        $nm_pekerjaan               = $this->input->post("dft_nm_pekerjaan");
        $kategori                   = $this->input->post("dft_kategori");
        $produk                     = $this->input->post("dft_produk");
        $st_type_produk             = $this->input->post("dft_st_type_produk");
        $uk_p                       = $this->input->post("dft_uk_p");
        $uk_l                       = $this->input->post("dft_uk_l");
        $id_satuan_uk               = $this->input->post("dft_idsatuan_uk");
        $tmpl_uk                    = $this->input->post("dft_tmpl_satuan_uk");
        $st_finishing               = $this->input->post("dft_st_finishing");
        $jml_cetak                  = $this->input->post("dft_jml_cetak");
        $img_thumbnail              = $this->input->post("img_thumbnail");
        $st_urgent                  = $this->input->post("st_urgent");
        $tgl_selesai                = $this->input->post("tgl_selesai");

        $tampil_harga               = $this->input->post("dft_tampil_harga");
        $harga_asli                 = $this->input->post("dft_rp_harga_asli");
        $tmpl_satuan_harga          = $this->input->post("dft_tmpl_satuan_harga");
        $id_satuan_harga            = $this->input->post("dft_id_satuan_harga");

        $harga_design_asli          = $this->input->post("dft_rp_design_asli");
        $harga_cetak_asli           = $this->input->post("dft_rp_cetak_asli");
        $harga_finishing_asli       = $this->input->post("dft_rp_finishing_asli");
        
        $diskon                     = $this->input->post("dft_diskon");                  
        $pot_kecil                  = $this->input->post("dft_pot_kecil");                  
        $potongan                   = $this->input->post("dft_potongan");                   

        $subtotal_asli              = $this->input->post("dft_subtotal_asli");
        
        $harga_baru                 = $this->input->post("dft_rp_harga_baru");
        $harga_cetak_baru           = $this->input->post("dft_rp_cetak_baru");
        $harga_design_baru          = $this->input->post("dft_rp_design_baru");
        $harga_finishing_baru       = $this->input->post("dft_rp_finishing_baru");
        $subtotal_baru              = $this->input->post("dft_subtotal_baru");                    
        // data header
        $id_konsumen                = $this->input->post("isi_id_konsumen");
        $total_all                  = $this->input->post("isi_total_order");
        $diskon_all                 = $this->input->post("diskon_all");
        $pot_all                    = $this->input->post("pot_all");
        $pajak_all                  = $this->input->post("pajak_all");
        $grand_total                = $this->input->post("isi_grandtotal");

        $deposit                    = isset($_POST['isi_deposit_pakai']) ? $this->input->post('isi_deposit_pakai') : 0;

        $dp                         = isset($_POST['dp']) ? str_replace(",", "", $this->input->post("dp")) : 0;

        $bayar                      = str_replace(",", "", $this->input->post("bayar"));

        $st_produksi                = $this->input->post("st_produksi");
        
        $ms_tempo                   = $this->input->post("ms_tempo");
        $tgl_tempo                  = $this->input->post("tgl_tempo");
        
        $st_bayar                   = $this->input->post("metode_bayar");
        $cek_size_file              = $_FILES['file_att']['size'];
        $cek_error_file             = $_FILES['file_att']['error'];


        // start cek pembayaran untuk order po

        $hasil_cek_po  = $this->cek_order_po($id);
        if($hasil_cek_po){

            if($bayar < $grand_total){

                $this->set_session_trans(1,$id,null);
                $this->template->set_message(lang('label-err-bayar-po-kurang'),'error');
                return false;

            }

        }

        // end cek pembayaran untuk order po

        // start cek kondisi deposit yang dipakai

        $deposit_cek    = get_deposit($id_konsumen);
        //echo $deposit_cek."-".$deposit;

        if($deposit_cek < $deposit){

            $this->set_session_trans(1,$id,null);
            $this->template->set_message(lang('label-err-deposit'),'warning');
            return false;

        }

        // end cek kondisi deposit yang dipakai

        // start hitung lebih  & kurang bayar

        if($deposit <= 0){

            if($dp > 0){

                $lebih_bayar    = ($bayar - $dp) >= 0 ? $bayar - $dp : 0;

            }else{

                $lebih_bayar    = ($bayar - $grand_total) >= 0 ? $bayar - $grand_total : 0;

            }


            if($dp > 0){

                $kurang_bayar   = $grand_total - $dp;

            }else{

                $kurang_bayar   = $grand_total - $bayar >= 0 ? $grand_total - $bayar : 0;

            }

        }else{

            $total_bayar        = $deposit + $bayar;

            $lebih_bayar        = $total_bayar >= $grand_total ? $total_bayar - $grand_total : 0;
            $kurang_bayar       = $total_bayar < $grand_total ? $grand_total - $total_bayar : 0;

        }

        // end hitung lebih  & kurang bayar

        // start form validation 

        if($_POST['metode_bayar'] > 0 || (isset($_POST['dp']) && $_POST['dp'] > 0)){

            $this->form_validation->set_rules("bayar","lang:capt-trans-bayar","required");

        }

        $this->form_validation->set_rules("metode_bayar","lang:capt-trans-metode-bayar","required");        

        if($this->form_validation->run() === false){

            $this->set_session_trans(1,$id,null);
            $this->template->set_message(validation_errors(),'error');
            return false;

        }

        // jika dp == 0 maka nilai bayar tidak boleh < grantotal 

        $tagihan_cek    = $deposit > 0 ? $grand_total - $deposit : $grand_total;

        if($dp == 0 && $bayar > 0 && $bayar < $tagihan_cek && $deposit > 0){

            $this->set_session_trans(1,$id,null);
            $this->template->set_message(lang('label-err-bayar-lunas'),'error');
            return false;

        }

        // end form validation

        $sql_all = "";

        $this->db->trans_start();

        // ..:: simpan header transaksi ::.. 
        $id_primary     = gen_primary("","transaksi","id_transaksi");
        $no_nota        = $this->get_noFaktur();    
        //$gambar         = $this->upload_data($no_nota);    

        $lokasi_file    = "";

        if($cek_size_file > 0 && $cek_error_file == 0 && $st_bayar > 0){

            $nm_file    = str_replace("/","_", $no_nota);
            $nm_file    = str_replace("-","_", $no_nota);

            $config['upload_path']          = './upload/transfer';
            $config['allowed_types']        = 'jpg|jpeg|png';
            $config['max_size']             = 200;
            $config['overwrite']            = TRUE;
            $config['file_name']            = $nm_file.".jpg";

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file_att')){
                
                $pesan_error = lang('label-err-lampiran')."<br>".$this->upload->display_errors();
                $this->template->set_message($pesan_error,'error');
                $this->set_session_trans(1,$id,null);
                return false;

            }else{

                $upload_data = $this->upload->data();

                $lokasi_file  = "/upload/transfer/".$upload_data['file_name'];

                /*$this->load->library('image_lib');
                $image_config = array(
                    'image_library' => 'imagemagick',
                    'library_path'  => '/usr/bin',
                    'source_image'  => $upload_data['full_path'],
                    'new_image'     => $upload_data['file_path']."thumb",
                    'maintain_ratio'=> TRUE,
                    'width'         => 200,
                    'height'        => 200
                );

                $this->image_lib->initialize($image_config);
                $this->image_lib->resize();
                $this->image_lib->clear();*/

            }
        }


        $lama_tempo     = $dp > 0 || $bayar == 0 ? $ms_tempo : 0; 
        $tgl_tempo      = $dp > 0 || $bayar == 0 ? date_ymd($tgl_tempo) : null; 

        $st_lunas       = $kurang_bayar > 0 ? 0 : 1;        

        if($kurang_bayar == 0){

            $st_metode_bayar = 0;

        }elseif ($dp > 0 && ($bayar > 0 || strlen($bayar) > 0)) {

            $st_metode_bayar = 1;            
            
        }else{

            $st_metode_bayar = 4;

        }

        /* start simpan data head transaksi */

        $arr_head   =   [

                            'id_transaksi'      => $id_primary, 
                            'waktu'             => date("Y-m-d H:i:s"),
                            'no_faktur'         => $no_nota, 
                            'id_order'          => $id, 
                            'id_trans_ref_asli' => $id_primary, 
                            'lama_tempo'        => $lama_tempo, 
                            'tgl_tempo'         => $tgl_tempo, 
                            'total'             => $total_all , 
                            'diskon'            => $diskon_all,
                            'potongan'          => $pot_all, 
                            'pajak'             => $pajak_all, 
                            'grand_total'       => $grand_total, 
                            'st_metode_bayar'   => $st_metode_bayar, 
                            'nilai_dp'          => $dp, 
                            'bayar'             => $bayar, 
                            'kurang_bayar'      => $kurang_bayar, 
                            'lebih_bayar'       => $lebih_bayar, 
                            'st_produksi'       => $st_produksi, 
                            'st_lunas'          => $st_lunas

                        ];

        $this->trans_model->insert($arr_head);
        $sql_all .= $this->db->last_query();
        /* end simpan data head transaksi */

        /* start update status kasir di head order_produk */
        $arr_up_order   =  ['st_kasir' => 1, 'st_nota' => 1, 'st_revisi' => 0];
        $this->order_model->update($id,$arr_up_order);
        $sql_all .= "\n\n". $this->db->last_query();
        /* end update status kasir di head order_produk */

        
        /* start insert detail transaksi */
        $arr_detail = [];
        $arr_up_det_order = [];
        foreach ($id_detail_order as $key => $isi) {
            
            // array detail transaksi
            $arr_detail[]   =   [
                                    'id_transaksi_detail'       => gen_primary("","transaksi_detail","id_transaksi_detail"), 
                                    'id_transaksi'              => $id_primary, 
                                    'id_detail_produk_order'    => $isi, 
                                    'harga_by_satuan'           => $harga_baru[$key], 
                                    'satuan_harga_by'           => $id_satuan_harga[$key],   
                                    'biaya_cetak'               => $harga_cetak_baru[$key], 
                                    'biaya_design'              => $harga_design_baru[$key], 
                                    'biaya_finishing'           => $harga_finishing_baru[$key], 
                                    'diskon'                    => $diskon[$key], 
                                    'pot_kecil'                 => $pot_kecil[$key],
                                    'potongan'                  => $potongan[$key], 
                                    'subtotal'                  => $subtotal_baru[$key], 
                                    'st_ubahan'                 => $st_ubah[$key]
                                ];

            /* update order_detail */
            $arr_up_det_order[]   = [
                                        'id_detail_produk_order'    => $isi,
                                        'st_acc_produksi'           => $st_produksi
                                    ];  

        }

        $this->trans_detail_model->insert_batch($arr_detail);
        $sql_all .= "\n\n".$this->db->last_query();
        /* end insert detail transaksi */

        /* start update status acc produksi, di m_order_produk_detail */
        /*$this->order_detail_model->update_batch($arr_up_det_order,'id_detail_produk_order');
        $sql_all .= "\n\n". $this->db->last_query();*/
        /* end update status acc produksi, di m_order_produk_detail */

        /* start buat data spk */
        if($st_produksi == 1 && !$hasil_cek_po){ 
            // buat spk jika bukan order po

            foreach ($id_detail_order as $key => $id_det_order) {
                
                $st_spk = simpan_spk(0,$id_det_order);
                set_proses_order($id_lokasi = 1, $id_ket_proses = 1, $id_detail_produk_order = $id_det_order, $jumlah_proses = 0);
                $sql_all .= "\n\n". $this->db->last_query();

            }

        }
        /* end buat data spk */

        // start simpan Pembayaran

        if($deposit > 0){

            $status_dp  = $deposit < $grand_total ? 1 : 0;
            $keterangan = "Pembayaran dengan deposit untuk transaksi : ".$no_nota;

            set_bayar_transaksi(0, 
                                $id_primary, 
                                0, 
                                1, 
                                3, 
                                $grand_total, 
                                $deposit, 
                                $deposit, $status_dp,
                                $lokasi_file,
                                $id_primary,
                                $keterangan
                            );

            // kurangi nilai deposit
            use_deposit($id_konsumen = $id_konsumen, $nilai = $deposit, $ket = $keterangan);


            if($bayar > 0){

                $bayar_asli     = $bayar - $lebih_bayar;
                $status_dp      = 0;

                set_bayar_transaksi(
                                    0,
                                    $id_primary,
                                    0,
                                    0,
                                    $st_bayar,
                                    $grand_total,
                                    $bayar_asli,
                                    $bayar,
                                    $status_dp,
                                    $lokasi_file,
                                    $id_primary,
                                    $keterangan,
                                    1
                                );

            }

        }else{

            if($bayar > 0){

                $tagihan    = $dp > 0 ? $dp : $grand_total;

                $ket        = $dp > 0 ? "Dibayar DP untuk transaksi ".$no_nota : "Dibayar transaksi ". $no_nota;

                $st_dp      = $dp > 0 ? 1 : 0;

                set_bayar_transaksi(
                                    0,
                                    $id_primary,
                                    0,
                                    0,
                                    $st_bayar,
                                    $grand_total,
                                    $tagihan,
                                    $bayar,
                                    $st_dp,
                                    $lokasi_file,
                                    $id_primary,
                                    $ket
                                );

            }

        }
        // end simpan Pembayaran        


        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            $return         = false;
            $keterangan     = "Gagal, simpan data transaksi id = " . $id_primary;
            $total          = 0;
            $status         = 0;

        }else{

            $return         = true;
            $keterangan     = "Sukses, simpan data transaksi id = " . $id_primary;
            $total          = $bayar;
            $status         = 0;

            $this->id_primary   = $id_primary;
            $this->no_nota      = $no_nota;

        }

        $nm_hak_akses   = $this->addPermission; 
        $kode_universal = $id_primary;
        $jumlah         = $bayar;
        $sql            = $sql_all;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $total, $sql, $status);

        return $return;

    }   

    public function set_session_trans($st=0, $id_order=null, $id_trans=null){

        /*
            Keterangan

                Hanya untuk data transaksi baru, yang datanya di load dari order
        */

		if($st == 0 && $id_order != null && $id_trans == null){ //data baru, diambil dari database order

            $data   = $this->order_model
                            ->select("`order_produk`.`id_order`,
                                    `order_produk`.`no_transaksi`,
                                    `order_produk`.`id_konsumen`,
                                    `konsumen`.`panggilan`,
                                    `konsumen`.`nama`,
                                    `konsumen`.`st`,
                                    DATE_FORMAT(`order_produk`.`tgl_order`, '%d/%m/%Y') AS tgl_order,
                                    `order_produk`.`total_value_order`,
                                    `m_order_produk_detail`.`nama_pekerjaan`,
                                    `kategori`.`nmkategori`,
                                    `produk`.`nm_produk`,
                                    `produk`.`st_tipe` as st_tipe_produk,
                                    `m_order_produk_detail`.`id_detail_produk_order`,
                                    `m_order_produk_detail`.`thumbnail`,
                                    `m_order_produk_detail`.`p`,
                                    `m_order_produk_detail`.`l`,
                                    `m_order_produk_detail`.`id_satuan` as id_satuan_uk,
                                    CONCAT(konversi_satuan.satuan_besar,
                                            ' (',
                                            `konversi_satuan`.`jml_kecil`,
                                            ' ',
                                            `satuan_terkecil`.`alias`,
                                            ')') AS tampil2,
                                    `m_order_produk_detail`.`st_urgent`,
                                    DATE_FORMAT(`m_order_produk_detail`.`tgl_permintaan_selesai`,
                                            '%d/%m/%Y') AS tgl_selesai,
                                    IF(`m_order_produk_detail`.`st_finishing` = 0,
                                        'Finishing Standart',
                                        'Custom Finishing') AS st_finishing,
                                    `m_order_produk_detail`.`jml_cetak`,
                                    (`m_order_produk_detail`.`harga_cetak_persatuan_terkecil` / `m_order_produk_detail`.`harga_by`) + ((`m_order_produk_detail`.`harga_cetak_persatuan_terkecil` / `m_order_produk_detail`.`harga_by`) * (`m_order_produk_detail`.`kenaikan_persen` / 100)) AS harga_terkecil,
                                    `m_order_produk_detail`.`satuan_cetak_by` as id_satuan_harga,
                                    satuan_harga.satuan as satuan_harga_kecil,
                                    (`m_order_produk_detail`.`biaya_cetak` + `m_order_produk_detail`.`kenaikan_value`) as biaya_cetak ,
                                    `m_order_produk_detail`.`biaya_design`,
                                    `m_order_produk_detail`.`biaya_finishing`,
                                    `m_order_produk_detail`.`sub_total`")
                            ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                            ->join("m_order_produk_detail","order_produk.id_order = m_order_produk_detail.id_order","inner")
                            ->join("kategori","m_order_produk_detail.id_kategori = kategori.idkategori","inner")
                            ->join("produk","m_order_produk_detail.id_produk = produk.idproduk","inner")
                            ->join("konversi_satuan","m_order_produk_detail.id_satuan = konversi_satuan.id_konversi","inner")
                            ->join("satuan_terkecil","konversi_satuan.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","inner")
                            ->join("(SELECT 
                                        `id_konversi` AS id, `satuan_besar` AS satuan
                                    FROM
                                        konversi_satuan
                                    WHERE
                                        deleted = 0) AS satuan_harga "," m_order_produk_detail.satuan_cetak_by = satuan_harga.id","inner")
                            ->where("order_produk.deleted = 0
                                        AND order_produk.st_simpan = 1
                                        AND order_produk.st_kasir = 0
                                        AND m_order_produk_detail.deleted = 0
                                        AND order_produk.id_order = '$id_order'")
                            ->order_by("kategori.nmkategori","asc")
                            ->find_all();

  			//echo $this->db->last_query();

  			$dt_detail 	= array();

  			foreach ($data as $key => $isi) {
  				
                $uk_cetak       = $isi->p."x".$isi->l."(".$isi->tampil2.")";
                $harga_kecil    = number_format($isi->harga_terkecil)." /".$isi->satuan_harga_kecil;

  				$dt_detail[] 	=   [
										"st_ubah"             => 0,
                                        "id_detail_order"     => $isi->id_detail_produk_order,
										"thumbnail"           => $isi->thumbnail,
										"nm_pekerjaan"        => $isi->nama_pekerjaan,
										"kategori"            => $isi->nmkategori,
										"produk"              => $isi->nm_produk,
                                        "st_tipe_produk"        => $isi->st_tipe_produk,
                                        "st_finishing"          => $isi->st_finishing,
                                        "st_urgent"             => $isi->st_urgent,
                                        "tgl_selesai"           => $isi->tgl_selesai,
                                        "tampil_uk_cetak"       => $uk_cetak,
                                        "uk_p"                  => $isi->p,
                                        "uk_l"                  => $isi->l,
                                        "id_satuan_uk"          => $isi->id_satuan_uk,
                                        "tampil_harga"          => $harga_kecil,
                                        "harga"                 => $isi->harga_terkecil,
                                        "tampil_satuan_harga"   => $isi->satuan_harga_kecil,
                                        "id_satuan_harga"       => $isi->id_satuan_harga,  
										"jml_cetak"             => $isi->jml_cetak,
                                        "biaya_cetak"           => $isi->biaya_cetak,
										"biaya_design"          => $isi->biaya_design, 
										"biaya_finishing"       => $isi->biaya_finishing,
                                        "diskon"                => 0,
                                        "pot_kecil"             => 0,
										"potongan"              => 0,
										"subtotal"              => $isi->sub_total,
                                        "harga_baru"            => $isi->harga_terkecil,
                                        "biaya_cetak_baru"      => $isi->biaya_cetak,
                                        "biaya_design_baru"     => $isi->biaya_design,
                                        "biaya_finishing_baru"  => $isi->biaya_finishing,
                                        "subtotal_baru"         => $isi->sub_total
  									];

  			}

            //start cek apakah user memiliki deposit

            $sisa_saldo     = get_deposit($data[0]->id_konsumen);
            $gt_bulat       = $this->pembulatan_gt($data[0]->total_value_order);

            $pakai_deposit  = 0;

            if($sisa_saldo > 0){

                $pakai_deposit  = $sisa_saldo > $gt_bulat ? $gt_bulat : $sisa_saldo;

            }

            //end cek apakah user memiliki deposit

  			$arr_sess 	=   [
								"st_baru"           => 1,/* 1 = nota baru, 0 = Revisi, 3 = view */
                                "st_lihat"			=> 0,
                                "st_lunas"          => 0,
                                "st_pelunasan"      => 0,
                                "st_edit_harga"     => 1,
								"id_trans"			=> "",
                                "st_trans_master"   => 1,
                                "id_trans_master"   => "", /* id_transaksi referensi */
                                "no_faktur"         => "",
                                "no_faktur_master"  => "",
								"id_order" 			=> $data[0]->id_order,
                                "no_transaksi"      => $data[0]->no_transaksi,
								"nm_konsumen" 		=> $data[0]->panggilan." ".$data[0]->nama,
                                "id_konsumen"       => $data[0]->id_konsumen,
								"st_konsumen" 		=> $data[0]->st,
								"tgl_order" 		=> $data[0]->tgl_order,
								"total"				=> $data[0]->total_value_order,
								"diskon"			=> "",
								"potongan"			=> "",
								"grand_total"		=> $gt_bulat,
								"lama_tempo" 		=> "",
								"tgl_tempo" 		=> "",
                                "metode_bayar"      => 0,
                                "nilai_dp"          => 0,
								"bayar" 			=> 0,
                                "total_bayar"       => 0, // semua niali nominal yang sudah dibayarkan
								"kurang_bayar" 		=> 0,
								"lebih_bayar" 		=> 0,
								"st_produksi" 		=> 1,
                                "pakai_deposit"     => $pakai_deposit,
								"detai_order"		=> $dt_detail 
  							];

  			$this->session->set_userdata('frm_trans_kasir', $arr_sess);


		}else if($st == 1 && $id_order != null && $id_trans == null){ //data baru, sumber didapat dari inputan form

	    	$data 	= $this->order_model->select("`order_produk`.`id_order`,
												    `order_produk`.`no_transaksi`,
                                                    `order_produk`.`id_konsumen`,
												    `konsumen`.`panggilan`,
												    `konsumen`.`nama`,
												    `konsumen`.`st`,
												    DATE_FORMAT(`order_produk`.`tgl_order`, '%d/%m/%Y') AS tgl_order")
	    								->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
	    								->where("order_produk.deleted = 0
											        AND order_produk.st_simpan = 1
											        AND order_produk.st_kasir = 0
											        AND order_produk.id_order = '$id_order'")
	    								->find_all();
			
			//data detail

	    	$id_detail_order 			= $this->input->post("id_detail_order");
            $st_ubah                    = $this->input->post("dft_st_ubah");
	    	$nm_pekerjaan 				= $this->input->post("dft_nm_pekerjaan");
	    	$kategori 					= $this->input->post("dft_kategori");
            $produk                     = $this->input->post("dft_produk");
            $st_type_produk             = $this->input->post("dft_st_type_produk");
	    	$uk_p                       = $this->input->post("dft_uk_p");
            $uk_l                       = $this->input->post("dft_uk_l");
            $id_satuan_uk               = $this->input->post("dft_idsatuan_uk");
            $tmpl_uk                    = $this->input->post("dft_tmpl_satuan_uk");
            $st_finishing 				= $this->input->post("dft_st_finishing");
	    	$jml_cetak 					= $this->input->post("dft_jml_cetak");
			$img_thumbnail 				= $this->input->post("img_thumbnail");
            $st_urgent                  = $this->input->post("st_urgent");
            $tgl_selesai                = $this->input->post("tgl_selesai");

            $tampil_harga               = $this->input->post("dft_tampil_harga");
            $harga_asli                 = $this->input->post("dft_rp_harga_asli");
            $tmpl_satuan_harga          = $this->input->post("dft_tmpl_satuan_harga");
            $id_satuan_harga            = $this->input->post("dft_id_satuan_harga");

            $harga_design_asli          = $this->input->post("dft_rp_design_asli");
	    	$harga_cetak_asli 			= $this->input->post("dft_rp_cetak_asli");
	    	$harga_finishing_asli		= $this->input->post("dft_rp_finishing_asli");
	    	
            $diskon                     = $this->input->post("dft_diskon");    
            $pot_kecil                  = $this->input->post("dft_pot_kecil");                  
            $potongan                   = $this->input->post("dft_potongan");                   

            $subtotal_asli              = $this->input->post("dft_subtotal_asli");
            
            $harga_baru                 = $this->input->post("dft_rp_harga_baru");
            $harga_cetak_baru           = $this->input->post("dft_rp_cetak_baru");
            $harga_design_baru          = $this->input->post("dft_rp_design_baru");
            $harga_finishing_baru       = $this->input->post("dft_rp_finishing_baru");
            $subtotal_baru              = $this->input->post("dft_subtotal_baru");            

	    	$dt_detail = array();

			foreach ($id_detail_order as $key => $isi) {

  				$dt_detail[]=   [
                                    "st_ubah"               => $st_ubah[$key],
                                    "id_detail_order"       => $isi,
                                    "thumbnail"             => $img_thumbnail[$key],
                                    "nm_pekerjaan"          => $nm_pekerjaan[$key],
                                    "kategori"              => $kategori[$key],
                                    "produk"                => $produk[$key],
                                    "st_tipe_produk"        => $st_type_produk[$key],
                                    "st_finishing"          => $st_finishing[$key],
                                    "st_urgent"             => $st_urgent[$key],
                                    "tgl_selesai"           => $tgl_selesai[$key],
                                    "tampil_uk_cetak"       => $tmpl_uk[$key],
                                    "uk_p"                  => $uk_p[$key],
                                    "uk_l"                  => $uk_l[$key],
                                    "id_satuan_uk"          => $id_satuan_uk[$key],
                                    "tampil_harga"          => $tampil_harga[$key],
                                    "harga"                 => $harga_asli[$key],
                                    "tampil_satuan_harga"   => $tmpl_satuan_harga[$key],
                                    "id_satuan_harga"       => $id_satuan_harga[$key],
                                    "jml_cetak"             => $jml_cetak[$key],
                                    "biaya_cetak"           => $harga_cetak_asli[$key],
                                    "biaya_design"          => $harga_design_asli[$key],
                                    "biaya_finishing"       => $harga_finishing_asli[$key],
                                    "diskon"                => $diskon[$key],
                                    "pot_kecil"             => $pot_kecil[$key],
                                    "potongan"              => $potongan[$key],
                                    "subtotal"              => $subtotal_asli[$key],
                                    "harga_baru"            => $harga_baru[$key],
                                    "biaya_cetak_baru"      => $harga_cetak_baru[$key],
                                    "biaya_design_baru"     => $harga_design_baru[$key],
                                    "biaya_finishing_baru"  => $harga_finishing_baru[$key],
                                    "subtotal_baru"         => $subtotal_baru[$key]
  								];

  			}	    	


  			// data header

	    	$total_all 					= $this->input->post("isi_total_order");
	    	$diskon_all 				= $this->input->post("diskon_all");
	    	$pot_all 					= $this->input->post("pot_all");
	    	$pajak_all					= $this->input->post("pajak_all");
	    	$grand_total				= $this->input->post("isi_grandtotal");
            $pakai_deposit              = isset($_POST['dp']) ? $this->input->post("isi_deposit_pakai") : 0;

            $dp                         = isset($_POST['dp']) ? str_replace(",", "", $this->input->post("dp")) : 0;

            $bayar                      = str_replace(",", "", $this->input->post("bayar"));
            $st_produksi                = $this->input->post("st_produksi");
	    	
            $ms_tempo 					= $this->input->post("ms_tempo");
	    	$tgl_tempo 					= $this->input->post("tgl_tempo");
            $metode_bayar               = $this->input->post("metode_bayar");

            /* hitung lebih bayar */
            if($dp > 0){

                $lebih_bayar    = ($bayar - $dp) >= 0 ? $bayar - $dp : 0;

            }else{

                $lebih_bayar    = ($bayar - $grand_total) >= 0 ? $bayar - $grand_total : 0;

            }

            /* hitung kurang bayar */
            if($dp > 0){

                $kurang_bayar   = $grand_total - $dp;

            }else{

                $kurang_bayar   = $grand_total - $bayar >=0 ? $grand_total - $bayar : 0;

            }

            $arr_sess   =   [
                                "st_baru"           => 1,/* 1 = nota baru, 0 = Revisi, 3 = view */
                                "st_lihat"          => 0,
                                "st_lunas"          => 0,
                                "st_pelunasan"      => 0,
                                "st_edit_harga"     => 1,
                                "id_trans"          => "",
                                "st_trans_master"   => 1,
                                "id_trans_master"   => "", /* id_transaksi referensi */
                                "no_faktur"         => "",
                                "no_faktur_master"  => "",
                                "id_order"          => $data[0]->id_order,
                                "no_transaksi"      => $data[0]->no_transaksi,
                                "nm_konsumen"       => $data[0]->panggilan." ".$data[0]->nama,
                                "id_konsumen"       => $data[0]->id_konsumen,
                                "st_konsumen"       => $data[0]->st,
                                "tgl_order"         => $data[0]->tgl_order,
                                "total"             => $total_all,
                                "diskon"            => $diskon_all,
                                "potongan"          => $pot_all ,
                                "grand_total"       => $grand_total,
                                "lama_tempo"        => $ms_tempo,
                                "tgl_tempo"         => $tgl_tempo,
                                "metode_bayar"      => $metode_bayar,
                                "nilai_dp"          => $dp,
                                "bayar"             => $bayar,
                                "total_bayar"       => 0, // semua nilai nominal yang sudah dibayarkan
                                "kurang_bayar"      => $kurang_bayar,
                                "lebih_bayar"       => $lebih_bayar,
                                "st_produksi"       => $st_produksi,
                                "pakai_deposit"     => $pakai_deposit,
                                "detai_order"       => $dt_detail 
                            ];


  			$this->session->set_userdata('frm_trans_kasir', $arr_sess);


		}	


    }

    public function set_session_edit_view($id_transaksi = "", $st_view = 0){

        /*
            $st_callback    == 1; mengembalikan data hanya untuk view

            $st_callback    == 0; menampilkan data dari database.
        */

        if($st_view == 0){

            /* cek apakah order direvisi atau belum */                    
            $cek     = $this->trans_model
                                ->select("`transaksi`.`id_transaksi`,
                                            `transaksi`.`no_faktur`,
                                            `transaksi`.`st_trans_master`,
                                            `transaksi`.`id_trans_ref_asli`,
                                            `transaksi`.`id_order`,
                                            `transaksi`.`st_history`,
                                            `order_produk`.`st_revisi`")
                                ->join("order_produk","transaksi.id_order = order_produk.id_order","inner")
                                ->where("`transaksi`.`id_transaksi` = '{$id_transaksi}'")
                                ->find_all();

            $st_revisi          = $cek[0]->st_revisi;  
            $id_order           = $cek[0]->id_order;
            $id_trans_master    = $cek[0]->id_trans_ref_asli;

            /*
                jika status revisinya 0 / 2, maka hanya pelunasan biasa.
                jika status revisinya 1, maka load ulang data ordernya.
            */

            /* start ambil data no_faktur masternya */
            $dt_trans_m         = $this->trans_model->select("no_faktur")
                                        ->find($id_trans_master);

            $no_faktur_master   = $dt_trans_m->no_faktur;
            /* end ambil data no_faktur masternya */


            /*
                ambil semua data pembayaran / dp yang sudah masuk
            */
            
            $dt_bayar = $this->bayar_trans_model->select("`bayar`,`dp`, `file`")
                                    ->where("`id_transaksi_master` = '{$id_trans_master}'
                                                AND deleted = 0")
                                    ->find_all();

            //echo $this->db->last_query();

            $total_all_sudah_bayar  = 0;
            $dp_pakai               = 0;

            $arr_all_bayar          = [];

            if(is_array($dt_bayar) && count($dt_bayar) ){
 
                foreach ($dt_bayar as $key => $val) {
                
                    $arr_all_bayar[] =  [
                                            'file'  => $val->file
                                        ];

                    $total_all_sudah_bayar += $val->bayar;

                }

            }

            if($st_revisi == 1){//load data ulang dari order

                $data   = $this->order_model
                            ->select("`order_produk`.`id_order`,
                                    `order_produk`.`no_transaksi`,
                                    `order_produk`.`id_konsumen`,
                                    `konsumen`.`panggilan`,
                                    `konsumen`.`nama`,
                                    `konsumen`.`st`,
                                    DATE_FORMAT(`order_produk`.`tgl_order`, '%d/%m/%Y') AS tgl_order,
                                    `order_produk`.`total_value_order`,
                                    `m_order_produk_detail`.`nama_pekerjaan`,
                                    `kategori`.`nmkategori`,
                                    `produk`.`nm_produk`,
                                    `produk`.`st_tipe` as st_tipe_produk,
                                    `m_order_produk_detail`.`id_detail_produk_order`,
                                    `m_order_produk_detail`.`thumbnail`,
                                    `m_order_produk_detail`.`p`,
                                    `m_order_produk_detail`.`l`,
                                    `m_order_produk_detail`.`id_satuan` as id_satuan_uk,
                                    CONCAT(konversi_satuan.satuan_besar,
                                            ' (',
                                            `konversi_satuan`.`jml_kecil`,
                                            ' ',
                                            `satuan_terkecil`.`alias`,
                                            ')') AS tampil2,
                                    `m_order_produk_detail`.`st_urgent`,
                                    DATE_FORMAT(`m_order_produk_detail`.`tgl_permintaan_selesai`,
                                            '%d/%m/%Y') AS tgl_selesai,
                                    IF(`m_order_produk_detail`.`st_finishing` = 0,
                                        'Finishing Standart',
                                        'Custom Finishing') AS st_finishing,
                                    `m_order_produk_detail`.`jml_cetak`,
                                    (`m_order_produk_detail`.`harga_cetak_persatuan_terkecil` / `m_order_produk_detail`.`harga_by`) + ((`m_order_produk_detail`.`harga_cetak_persatuan_terkecil` / `m_order_produk_detail`.`harga_by`) * (`m_order_produk_detail`.`kenaikan_persen` / 100)) AS harga_terkecil,
                                    `m_order_produk_detail`.`satuan_cetak_by` as id_satuan_harga,
                                    satuan_harga.satuan as satuan_harga_kecil,
                                    (`m_order_produk_detail`.`biaya_cetak` + `m_order_produk_detail`.`kenaikan_value`) as biaya_cetak,
                                    `m_order_produk_detail`.`biaya_design`,
                                    `m_order_produk_detail`.`biaya_finishing`,
                                    `m_order_produk_detail`.`sub_total`")
                            ->join("konsumen","order_produk.id_konsumen = konsumen.idkonsumen","inner")
                            ->join("m_order_produk_detail","order_produk.id_order = m_order_produk_detail.id_order","inner")
                            ->join("kategori","m_order_produk_detail.id_kategori = kategori.idkategori","inner")
                            ->join("produk","m_order_produk_detail.id_produk = produk.idproduk","inner")
                            ->join("konversi_satuan","m_order_produk_detail.id_satuan = konversi_satuan.id_konversi","inner")
                            ->join("satuan_terkecil","konversi_satuan.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","inner")
                            ->join("(SELECT 
                                        `id_konversi` AS id, `satuan_besar` AS satuan
                                    FROM
                                        konversi_satuan
                                    WHERE
                                        deleted = 0) AS satuan_harga "," m_order_produk_detail.satuan_cetak_by = satuan_harga.id","inner")
                            ->where("order_produk.deleted = 0
                                        AND order_produk.st_simpan = 1
                                        AND m_order_produk_detail.deleted = 0
                                        AND order_produk.id_order = '$id_order'")
                            ->order_by("kategori.nmkategori","asc")
                            ->find_all();

                //echo $this->db->last_query();
                if(!$data){

                    $this->template->set_message(lang('label-err-order-kosong'),"error");
                    redirect('kasir');
                    return false;

                }

                $dt_detail  = array();

                foreach ($data as $key => $isi) {
                    
                    $uk_cetak       = $isi->p."x".$isi->l."(".$isi->tampil2.")";
                    $harga_kecil    = number_format($isi->harga_terkecil)." /".$isi->satuan_harga_kecil;

                    $dt_detail[]    =   [
                                            "st_ubah"               => 0,
                                            "id_detail_order"       => $isi->id_detail_produk_order,
                                            "thumbnail"             => $isi->thumbnail,
                                            "nm_pekerjaan"          => $isi->nama_pekerjaan,
                                            "kategori"              => $isi->nmkategori,
                                            "produk"                => $isi->nm_produk,
                                            "st_tipe_produk"        => $isi->st_tipe_produk,
                                            "st_finishing"          => $isi->st_finishing,
                                            "st_urgent"             => $isi->st_urgent,
                                            "tgl_selesai"           => $isi->tgl_selesai,
                                            "tampil_uk_cetak"       => $uk_cetak,
                                            "uk_p"                  => $isi->p,
                                            "uk_l"                  => $isi->l,
                                            "id_satuan_uk"          => $isi->id_satuan_uk,
                                            "tampil_harga"          => $harga_kecil,
                                            "harga"                 => $isi->harga_terkecil,
                                            "tampil_satuan_harga"   => $isi->satuan_harga_kecil,
                                            "id_satuan_harga"       => $isi->id_satuan_harga,  
                                            "jml_cetak"             => $isi->jml_cetak,
                                            "biaya_cetak"           => $isi->biaya_cetak,
                                            "biaya_design"          => $isi->biaya_design, 
                                            "biaya_finishing"       => $isi->biaya_finishing,
                                            "diskon"                => 0,
                                            "pot_kecil"             => 0,
                                            "potongan"              => 0,
                                            "subtotal"              => $isi->sub_total,
                                            "harga_baru"            => $isi->harga_terkecil,
                                            "biaya_cetak_baru"      => $isi->biaya_cetak,
                                            "biaya_design_baru"     => $isi->biaya_design,
                                            "biaya_finishing_baru"  => $isi->biaya_finishing,
                                            "subtotal_baru"         => $isi->sub_total
                                        ];

                }

                $ttl_trans  = $this->pembulatan_gt($data[0]->total_value_order);

                $kurang_bayar_pelunasan     = $total_all_sudah_bayar < $data[0]->total_value_order ? $ttl_trans - $total_all_sudah_bayar : 0;

                $lebih_bayar_pelunasan      = $total_all_sudah_bayar >= $data[0]->total_value_order ? $total_all_sudah_bayar - $ttl_trans : 0;

                // start kondisikan nilai deposit yang bisa dipakai

                $deposit                = get_deposit($data[0]->id_konsumen);
                $nilai_tagihan          = $kurang_bayar_pelunasan > 0 ? $kurang_bayar_pelunasan : 0;
                $nilai_pakai_deposit    = 0;

                if($deposit > 0){

                    $nilai_pakai_deposit    = $deposit >= $nilai_tagihan ? $nilai_tagihan : $deposit;

                }

                // end kondisikan nilai deposit yang bisa dipakai



                $arr_sess   =   [
                                    "st_baru"           => 0,/* 1 = nota baru, 0 = Revisi,  3 = view */
                                    "st_lihat"          => 0,
                                    "st_lunas"          => 0,
                                    "st_pelunasan"      => 1,
                                    "st_edit_harga"     => 1,
                                    "id_trans"          => "",
                                    "st_trans_master"   => 0,
                                    "id_trans_master"   => $id_trans_master, /* id_transaksi referensi */
                                    "no_faktur"         => "",
                                    "no_faktur_master"  => $no_faktur_master,
                                    "id_order"          => $data[0]->id_order,
                                    "no_transaksi"      => $data[0]->no_transaksi,
                                    "nm_konsumen"       => $data[0]->panggilan." ".$data[0]->nama,
                                    "id_konsumen"       => $data[0]->id_konsumen,
                                    "st_konsumen"       => $data[0]->st,
                                    "tgl_order"         => $data[0]->tgl_order,
                                    "total"             => $data[0]->total_value_order,
                                    "diskon"            => "",
                                    "potongan"          => "",
                                    "grand_total"       => $this->pembulatan_gt($data[0]->total_value_order),
                                    "lama_tempo"        => "",
                                    "tgl_tempo"         => "",
                                    "metode_bayar"      => 0,
                                    "nilai_dp"          => $dp_pakai,
                                    "bayar"             => 0,
                                    "total_bayar"       => $total_all_sudah_bayar, // semua niali nominal yang sudah dibayarkan
                                    "kurang_bayar"      => $kurang_bayar_pelunasan,
                                    "lebih_bayar"       => $lebih_bayar_pelunasan,
                                    "pakai_deposit"     => $nilai_pakai_deposit,
                                    "st_produksi"       => 1,
                                    "detai_order"       => $dt_detail,
                                    "detail_file"       => $arr_all_bayar
                                ];

                $this->session->set_userdata('frm_trans_kasir', $arr_sess);

            }else{// load datanya untuk pelunasan

                $data   = $this->trans_model
                                ->select("`transaksi`.`id_transaksi`,
                                            `transaksi`.`waktu`,
                                            `transaksi`.`no_faktur`,
                                            `t_master`.`no_faktur` AS no_faktur_master,
                                            `transaksi`.`st_kasir`,
                                            `transaksi`.`st_trans_master`,
                                            `transaksi`.`st_ref`,
                                            `transaksi`.`id_trans_ref_by`,
                                            `transaksi`.`id_trans_ref_asli`,
                                            `transaksi`.`lama_tempo`,
                                            date_format(`transaksi`.`tgl_tempo`,'%d/%m/%Y') as tgl_tempo,
                                            `transaksi`.`total` as total_all,
                                            `transaksi`.`diskon` as diskon_all,
                                            `transaksi`.`potongan` as pot_all,
                                            `transaksi`.`pajak` as pajak_all,
                                            `transaksi`.`grand_total`,
                                            `transaksi`.`st_metode_bayar`,
                                            `transaksi`.`kurang_bayar`,
                                            `transaksi`.`lebih_bayar`,
                                            `transaksi`.`st_produksi`,
                                            `transaksi`.`id_order`,
                                            `konsumen`.`panggilan`,
                                            `konsumen`.`nama`,
                                            `konsumen`.`st`,
                                            `order_produk`.`no_transaksi`,
                                            `order_produk`.`id_konsumen`,
                                            DATE_FORMAT(`order_produk`.`tgl_order`, '%d/%m/%Y') AS tgl_order,
                                            `order_produk`.`total_value_order`,
                                            `order_produk`.`st_revisi`,
                                            `m_order_produk_detail`.`nama_pekerjaan`,
                                            `kategori`.`nmkategori`,
                                            `produk`.`nm_produk`,
                                            `produk`.`st_tipe` AS `st_tipe_produk`,
                                            `m_order_produk_detail`.`id_detail_produk_order`,
                                            `m_order_produk_detail`.`thumbnail`,
                                            `m_order_produk_detail`.`p`,
                                            `m_order_produk_detail`.`l`,
                                            `m_order_produk_detail`.`id_satuan` AS `id_satuan_uk`,
                                            CONCAT(konversi_satuan.satuan_besar,
                                                    ' (',
                                                    `konversi_satuan`.`jml_kecil`,
                                                    ' ',
                                                    `satuan_terkecil`.`alias`,
                                                    ')') AS tampil2,
                                            `m_order_produk_detail`.`st_urgent`,
                                            DATE_FORMAT(`m_order_produk_detail`.`tgl_permintaan_selesai`,
                                                    '%d/%m/%Y') AS tgl_selesai,
                                            IF(`m_order_produk_detail`.`st_finishing` = 0,
                                                'Finishing Standart',
                                                'Custom Finishing') AS st_finishing,
                                            `m_order_produk_detail`.`jml_cetak`,
                                            `transaksi_detail`.`harga_by_satuan`,
                                            `transaksi_detail`.`satuan_harga_by`,
                                            `satuan_harga`.`satuan` AS `satuan_harga_kecil`,
                                            `transaksi_detail`.`biaya_cetak`,
                                            `transaksi_detail`.`biaya_design`,
                                            `transaksi_detail`.`biaya_finishing`,
                                            `transaksi_detail`.`diskon`,
                                            `transaksi_detail`.`pot_kecil`,
                                            `transaksi_detail`.`potongan`,
                                            `transaksi_detail`.`subtotal`,
                                            `transaksi_detail`.`st_ubahan`")
                                ->join("order_produk","transaksi.id_order = order_produk.id_order","inner")
                                ->join("konsumen","konsumen.idkonsumen = order_produk.id_konsumen","inner")
                                ->join("transaksi_detail","transaksi_detail.id_transaksi = transaksi.id_transaksi","inner")
                                ->join("m_order_produk_detail"," m_order_produk_detail.id_detail_produk_order = transaksi_detail.id_detail_produk_order","inner")
                                ->join("kategori","kategori.idkategori = m_order_produk_detail.id_kategori","inner")
                                ->join("produk","produk.idproduk = m_order_produk_detail.id_produk","inner")
                                ->join("konversi_satuan","konversi_satuan.id_konversi = m_order_produk_detail.id_satuan","inner")
                                ->join("`satuan_terkecil`","`konversi_satuan`.`id_satuan_terkecil` = `satuan_terkecil`.`id_satuan_terkecil`","inner")
                                ->join("(SELECT 
                                        `id_konversi` AS id, `satuan_besar` AS satuan
                                        FROM
                                            konversi_satuan
                                        WHERE
                                            deleted = 0) AS satuan_harga","transaksi_detail.satuan_harga_by = `satuan_harga`.`id`","inner")
                                ->join("(SELECT 
                                            `id_transaksi` AS id, `no_faktur`
                                        FROM
                                            transaksi
                                        WHERE
                                            deleted = 0) AS t_master","t_master.id = transaksi.id_trans_ref_asli","left")
                                ->where("transaksi.deleted = 0 
                                        and `transaksi`.`id_transaksi` = '{$id_transaksi}'")
                                ->order_by("kategori.nmkategori","asc")
                                ->find_all();

                //echo $this->db->last_query();

                $dt_detail  = array();

                foreach ($data as $key => $isi) {
                     
                    $uk_cetak       = $isi->p."x".$isi->l."(".$isi->tampil2.")";
                    $harga_kecil    = number_format($isi->harga_by_satuan-$isi->pot_kecil)." /".$isi->satuan_harga_kecil;    

                    $dt_detail[]    =   [
                                            "st_ubah"               => 0,
                                            "id_detail_order"       => $isi->id_detail_produk_order,
                                            "thumbnail"             => $isi->thumbnail,
                                            "nm_pekerjaan"          => $isi->nama_pekerjaan,
                                            "kategori"              => $isi->nmkategori,
                                            "produk"                => $isi->nm_produk,
                                            "st_tipe_produk"        => $isi->st_tipe_produk,
                                            "st_finishing"          => $isi->st_finishing,
                                            "st_urgent"             => $isi->st_urgent,
                                            "tgl_selesai"           => $isi->tgl_selesai,
                                            "tampil_uk_cetak"       => $uk_cetak,
                                            "uk_p"                  => $isi->p,
                                            "uk_l"                  => $isi->l,
                                            "id_satuan_uk"          => $isi->id_satuan_uk,
                                            "tampil_harga"          => $harga_kecil,
                                            "harga"                 => $isi->harga_by_satuan,
                                            "tampil_satuan_harga"   => $isi->satuan_harga_kecil,
                                            "id_satuan_harga"       => $isi->satuan_harga_by,  
                                            "jml_cetak"             => $isi->jml_cetak,
                                            "biaya_cetak"           => $isi->biaya_cetak,
                                            "biaya_design"          => $isi->biaya_design, 
                                            "biaya_finishing"       => $isi->biaya_finishing,
                                            "diskon"                => $isi->diskon,
                                            "pot_kecil"             => $isi->pot_kecil,
                                            "potongan"              => $isi->potongan,
                                            "subtotal"              => $isi->subtotal,
                                            "harga_baru"            => $isi->harga_by_satuan,
                                            "biaya_cetak_baru"      => $isi->biaya_cetak,
                                            "biaya_design_baru"     => $isi->biaya_design,
                                            "biaya_finishing_baru"  => $isi->biaya_finishing,
                                            "subtotal_baru"         => $isi->subtotal
                                        ];                    

                }

                $kurang_bayar_pelunasan     = $total_all_sudah_bayar < $data[0]->grand_total ? $data[0]->grand_total - $total_all_sudah_bayar : 0;

                $lebih_bayar_pelunasan      = $total_all_sudah_bayar >= $data[0]->grand_total ? $total_all_sudah_bayar - $data[0]->grand_total  : 0;

                // start kondisikan nilai deposit yang bisa dipakai

                $deposit                = get_deposit($data[0]->id_konsumen);
                $nilai_tagihan          = $kurang_bayar_pelunasan > 0 ? $kurang_bayar_pelunasan : 0;
                $nilai_pakai_deposit    = 0;

                if($deposit > 0){

                    $nilai_pakai_deposit    = $deposit >= $nilai_tagihan ? $nilai_tagihan : $deposit;

                }

                // end kondisikan nilai deposit yang bisa dipakai

                $arr_sess   =   [
                                    "st_baru"           => 0,/* 1 = nota baru, 0 = Revisi, 2 = view */
                                    "st_lihat"          => 0,
                                    "st_lunas"          => 0,
                                    "st_edit_harga"     => 0,
                                    "st_pelunasan"      => 1,
                                    "id_trans"          => $data[0]->id_transaksi,
                                    "st_trans_master"   => $data[0]->st_trans_master,
                                    "id_trans_master"   => $data[0]->id_trans_ref_asli, /* id_transaksi referensi */
                                    "no_faktur"         => $data[0]->no_faktur,
                                    "no_faktur_master"  => $data[0]->no_faktur_master,
                                    "id_order"          => $data[0]->id_order,
                                    "no_transaksi"      => $data[0]->no_transaksi,
                                    "nm_konsumen"       => $data[0]->panggilan." ".$data[0]->nama,
                                    "id_konsumen"       => $data[0]->id_konsumen,
                                    "st_konsumen"       => $data[0]->st,
                                    "tgl_order"         => $data[0]->tgl_order,
                                    "total"             => $data[0]->total_value_order,
                                    "diskon"            => $data[0]->diskon_all,
                                    "potongan"          => $data[0]->pot_all,
                                    "grand_total"       => $data[0]->grand_total,
                                    "lama_tempo"        => $data[0]->lama_tempo,
                                    "tgl_tempo"         => $data[0]->tgl_tempo,
                                    "metode_bayar"      => 0,
                                    "nilai_dp"          => $dp_pakai,//$total_all_sudah_bayar < $data[0]->grand_total ? $total_all_sudah_bayar : 0,
                                    "bayar"             => 0, //$total_all_sudah_bayar >= $data[0]->grand_total ? $total_all_sudah_bayar : 0,
                                    "total_bayar"       => $total_all_sudah_bayar, // semua niali nominal yang sudah dibayarkan
                                    "kurang_bayar"      => $kurang_bayar_pelunasan,
                                    "lebih_bayar"       => $lebih_bayar_pelunasan,
                                    "pakai_deposit"     => $nilai_pakai_deposit,
                                    "st_produksi"       => $data[0]->st_produksi,
                                    "detai_order"       => $dt_detail,
                                    "detail_file"       => $arr_all_bayar
                                ];

                $this->session->set_userdata('frm_trans_kasir', $arr_sess);

            }


        }else{ // tampil data untuk view saja

            $data   = $this->trans_model
                            ->select("`transaksi`.`id_transaksi`,
                                        `transaksi`.`waktu`,
                                        `transaksi`.`no_faktur`,
                                        `t_master`.`no_faktur` AS no_faktur_master,
                                        `transaksi`.`st_kasir`,
                                        `transaksi`.`st_trans_master`,
                                        `transaksi`.`st_ref`,
                                        `transaksi`.`st_history`,
                                        `transaksi`.`id_trans_ref_by`,
                                        `transaksi`.`id_trans_ref_asli`,
                                        `transaksi`.`lama_tempo`,
                                        date_format(`transaksi`.`tgl_tempo`,'%d/%m/%Y') as tgl_tempo,
                                        `transaksi`.`total` as total_all,
                                        `transaksi`.`diskon` as diskon_all,
                                        `transaksi`.`potongan` as pot_all,
                                        `transaksi`.`pajak` as pajak_all,
                                        `transaksi`.`grand_total`,
                                        `transaksi`.`st_metode_bayar`,
                                        `transaksi`.`kurang_bayar`,
                                        `transaksi`.`lebih_bayar`,
                                        `transaksi`.`st_produksi`,
                                        `transaksi`.`id_order`,
                                        `konsumen`.`panggilan`,
                                        `konsumen`.`nama`,
                                        `konsumen`.`st`,
                                        `order_produk`.`no_transaksi`,
                                        `order_produk`.`id_konsumen`,
                                        DATE_FORMAT(`order_produk`.`tgl_order`, '%d/%m/%Y') AS tgl_order,
                                        `order_produk`.`total_value_order`,
                                        `order_produk`.`st_revisi`,
                                        `m_order_produk_detail`.`nama_pekerjaan`,
                                        `kategori`.`nmkategori`,
                                        `produk`.`nm_produk`,
                                        `produk`.`st_tipe` AS `st_tipe_produk`,
                                        `m_order_produk_detail`.`id_detail_produk_order`,
                                        `m_order_produk_detail`.`thumbnail`,
                                        `m_order_produk_detail`.`p`,
                                        `m_order_produk_detail`.`l`,
                                        `m_order_produk_detail`.`id_satuan` AS `id_satuan_uk`,
                                        CONCAT(konversi_satuan.satuan_besar,
                                                ' (',
                                                `konversi_satuan`.`jml_kecil`,
                                                ' ',
                                                `satuan_terkecil`.`alias`,
                                                ')') AS tampil2,
                                        `m_order_produk_detail`.`st_urgent`,
                                        DATE_FORMAT(`m_order_produk_detail`.`tgl_permintaan_selesai`,
                                                '%d/%m/%Y') AS tgl_selesai,
                                        IF(`m_order_produk_detail`.`st_finishing` = 0,
                                            'Finishing Standart',
                                            'Custom Finishing') AS st_finishing,
                                        `m_order_produk_detail`.`jml_cetak`,
                                        `transaksi_detail`.`harga_by_satuan`,
                                        `transaksi_detail`.`satuan_harga_by`,
                                        `satuan_harga`.`satuan` AS `satuan_harga_kecil`,
                                        `transaksi_detail`.`biaya_cetak`,
                                        `transaksi_detail`.`biaya_design`,
                                        `transaksi_detail`.`biaya_finishing`,
                                        `transaksi_detail`.`diskon`,
                                        `transaksi_detail`.`pot_kecil`,
                                        `transaksi_detail`.`potongan`,
                                        `transaksi_detail`.`subtotal`,
                                        `transaksi_detail`.`st_ubahan`")
                            ->join("order_produk","transaksi.id_order = order_produk.id_order","inner")
                            ->join("konsumen","konsumen.idkonsumen = order_produk.id_konsumen","inner")
                            ->join("transaksi_detail","transaksi_detail.id_transaksi = transaksi.id_transaksi","inner")
                            ->join("m_order_produk_detail"," m_order_produk_detail.id_detail_produk_order = transaksi_detail.id_detail_produk_order","inner")
                            ->join("kategori","kategori.idkategori = m_order_produk_detail.id_kategori","inner")
                            ->join("produk","produk.idproduk = m_order_produk_detail.id_produk","inner")
                            ->join("konversi_satuan","konversi_satuan.id_konversi = m_order_produk_detail.id_satuan","inner")
                            ->join("`satuan_terkecil`","`konversi_satuan`.`id_satuan_terkecil` = `satuan_terkecil`.`id_satuan_terkecil`","inner")
                            ->join("(SELECT 
                                    `id_konversi` AS id, `satuan_besar` AS satuan
                                    FROM
                                        konversi_satuan
                                    WHERE
                                        deleted = 0) AS satuan_harga","transaksi_detail.satuan_harga_by = `satuan_harga`.`id`","inner")
                            ->join("(SELECT 
                                        `id_transaksi` AS id, `no_faktur`
                                    FROM
                                        transaksi
                                    WHERE
                                        deleted = 0) AS t_master","t_master.id = transaksi.id_trans_ref_asli","left")
                            ->where("transaksi.deleted = 0 
                                    and `transaksi`.`id_transaksi` = '{$id_transaksi}'")
                            ->order_by("kategori.nmkategori","asc")
                            ->find_all();

            //echo $this->db->last_query();

            $dt_detail  = array();

            foreach ($data as $key => $isi) {
                 
                $uk_cetak       = $isi->p."x".$isi->l."(".$isi->tampil2.")";
                $harga_kecil    = number_format($isi->harga_by_satuan - $isi->pot_kecil)." /".$isi->satuan_harga_kecil;    

                $dt_detail[]    =   [
                                        "st_ubah"               => 0,
                                        "id_detail_order"       => $isi->id_detail_produk_order,
                                        "thumbnail"             => $isi->thumbnail,
                                        "nm_pekerjaan"          => $isi->nama_pekerjaan,
                                        "kategori"              => $isi->nmkategori,
                                        "produk"                => $isi->nm_produk,
                                        "st_tipe_produk"        => $isi->st_tipe_produk,
                                        "st_finishing"          => $isi->st_finishing,
                                        "st_urgent"             => $isi->st_urgent,
                                        "tgl_selesai"           => $isi->tgl_selesai,
                                        "tampil_uk_cetak"       => $uk_cetak,
                                        "uk_p"                  => $isi->p,
                                        "uk_l"                  => $isi->l,
                                        "id_satuan_uk"          => $isi->id_satuan_uk,
                                        "tampil_harga"          => $harga_kecil,
                                        "harga"                 => $isi->harga_by_satuan,
                                        "tampil_satuan_harga"   => $isi->satuan_harga_kecil,
                                        "id_satuan_harga"       => $isi->satuan_harga_by,  
                                        "jml_cetak"             => $isi->jml_cetak,
                                        "biaya_cetak"           => $isi->biaya_cetak,
                                        "biaya_design"          => $isi->biaya_design, 
                                        "biaya_finishing"       => $isi->biaya_finishing,
                                        "diskon"                => $isi->diskon,
                                        "pot_kecil"             => $isi->pot_kecil,
                                        "potongan"              => $isi->potongan,
                                        "subtotal"              => $isi->subtotal,
                                        "harga_baru"            => $isi->harga_by_satuan,
                                        "biaya_cetak_baru"      => $isi->biaya_cetak,
                                        "biaya_design_baru"     => $isi->biaya_design,
                                        "biaya_finishing_baru"  => $isi->biaya_finishing,
                                        "subtotal_baru"         => $isi->subtotal
                                    ];                    

            }


            // start ambil data pembayaran
            
            $dp                     = 0;
            $bayar                  = 0;
            $krg_bayar              = $data[0]->grand_total;
            $kembalian              = 0;
            $deposit                = 0;
            $st_sumber_bayar        = 0;
            $arr_all_bayar          = [];
            $dt_bayar   = $this->bayar_trans_model
                                ->select("`dp`,
                                         dp_deposit,   
                                        `bayar`,
                                        pembayaran, 
                                        `kurang_bayar`, 
                                        `kembalian`,
                                        `st_dp`,
                                        `st_bayar`,
                                        `st_sumber_bayar`,
                                        file")
                                ->where("`id_transaksi` = '{$id_transaksi}'
                                            AND deleted = 0")
                                ->order_by("created_on","desc")
                                ->order_by("order","desc")
                                ->limit(1,0)
                                ->find_all();

            //echo $this->db->last_query();

            if(is_array($dt_bayar) && count($dt_bayar) ){

                $krg_bayar          = $dt_bayar[0]->kurang_bayar;
                $kembalian          = $dt_bayar[0]->kembalian;
                $st_sumber_bayar    = $dt_bayar[0]->st_sumber_bayar;
                $bayar              = $st_sumber_bayar == 1 || $dt_bayar[0]->bayar < 0 ? 0 : $dt_bayar[0]->pembayaran;
                $dp                 = $dt_bayar[0]->dp;

                $deposit            = $st_sumber_bayar == 1 ? $dt_bayar[0]->bayar + $dt_bayar[0]->dp_deposit : $dt_bayar[0]->dp_deposit;
                $st_bayar           = $dt_bayar[0]->st_bayar;

                $arr_all_bayar[]    =   [
                                            'file'  => $dt_bayar[0]->file
                                        ];

            }

            // end ambil data pembayaran all

            //$kurang_bayar_pelunasan     = $total_all_sudah_bayar < $data[0]->grand_total ? $data[0]->grand_total - $total_all_sudah_bayar : 0;

            //$lebih_bayar_pelunasan      = $total_all_sudah_bayar >= $data[0]->grand_total ? $total_all_sudah_bayar - $data[0]->grand_total  : 0;

            $arr_sess   =   [
                                "st_baru"           => 2,/* 1 = nota baru, 0 = Revisi, 2 = lihat */
                                "st_lihat"          => 1,
                                "st_lunas"          => 0,
                                "st_edit_harga"     => 0,
                                "st_pelunasan"      => 1,
                                "st_history"        => $data[0]->st_history,
                                "id_trans"          => $data[0]->id_transaksi,
                                "st_trans_master"   => $data[0]->st_trans_master,
                                "id_trans_master"   => $data[0]->id_trans_ref_asli, /* id_transaksi referensi */
                                "no_faktur"         => $data[0]->no_faktur,
                                "no_faktur_master"  => $data[0]->no_faktur_master,
                                "id_order"          => $data[0]->id_order,
                                "no_transaksi"      => $data[0]->no_transaksi,
                                "nm_konsumen"       => $data[0]->panggilan." ".$data[0]->nama,
                                "id_konsumen"       => $data[0]->id_konsumen,
                                "st_konsumen"       => $data[0]->st,
                                "tgl_order"         => $data[0]->tgl_order,
                                "total"             => $data[0]->total_value_order,
                                "diskon"            => $data[0]->diskon_all,
                                "potongan"          => $data[0]->pot_all,
                                "grand_total"       => $data[0]->grand_total,
                                "lama_tempo"        => $data[0]->lama_tempo,
                                "tgl_tempo"         => $data[0]->tgl_tempo,
                                "metode_bayar"      => $st_bayar,
                                "nilai_dp"          => $dp,
                                "bayar"             => $bayar,
                                "pakai_deposit"     => $deposit,
                                "total_bayar"       => 0, // semua niali nominal yang sudah dibayarkan
                                "kurang_bayar"      => $krg_bayar,
                                "lebih_bayar"       => $kembalian,
                                "st_produksi"       => $data[0]->st_produksi,
                                "detai_order"       => $dt_detail,
                                "detail_file"       => $arr_all_bayar
                            ];

            $this->session->set_userdata('frm_trans_kasir', $arr_sess);            

        }


    }

    function cetak_struk(){

        $id_transaksi   = $this->input->get("id_transaksi");
        $st_nota        = $this->input->get("st_nota");

        $identitas      = $this->identitas_model->find(1);
        $idt_op         = $this->auth->userdata();

        $data           = $this->trans_model
                                ->select("`transaksi`.`id_transaksi`,
                                        date_format(`transaksi`.`waktu`, '%d/%m/%Y %H:%i:%s') AS waktu,
                                        `transaksi`.`no_faktur`,
                                        `t_master`.`no_faktur` AS `no_faktur_master`,
                                        `transaksi`.`st_trans_master`,
                                        `transaksi`.`st_ref`,
                                        `transaksi`.`st_lunas`,
                                        `transaksi`.`lama_tempo`,
                                        DATE_FORMAT(`transaksi`.`tgl_tempo`, '%d/%m/%Y') AS tgl_tempo,
                                        `transaksi`.`total` AS `total_all`,
                                        `transaksi`.`diskon` AS `diskon_all`,
                                        `transaksi`.`potongan` AS `pot_all`,
                                        `transaksi`.`pajak` AS `pajak_all`,
                                        `transaksi`.`grand_total`,
                                        `transaksi`.`st_metode_bayar`,
                                        `konsumen`.`idkonsumen`,
                                        `konsumen`.`panggilan`,
                                        `konsumen`.`nama`,
                                        `konsumen`.`st`,
                                        `order_produk`.`no_tlp`,
                                        `m_order_produk_detail`.`nama_pekerjaan`,
                                        `kategori`.`nmkategori`,
                                        `produk`.`nm_produk`,
                                        `m_order_produk_detail`.`nama_pekerjaan`,
                                        `m_order_produk_detail`.`id_detail_produk_order`,
                                        `m_order_produk_detail`.`thumbnail`,
                                        `m_order_produk_detail`.`p`,
                                        `m_order_produk_detail`.`l`,
                                        `m_order_produk_detail`.`id_satuan` AS `id_satuan_uk`,
                                        konversi_satuan.satuan_besar AS tampil2,
                                        `m_order_produk_detail`.`jml_cetak`,
                                        `transaksi_detail`.`harga_by_satuan`,
                                        `transaksi_detail`.`satuan_harga_by`,
                                        `satuan_harga`.`satuan` AS `satuan_harga_kecil`,
                                        `transaksi_detail`.`biaya_cetak`,
                                        `transaksi_detail`.`biaya_design`,
                                        `transaksi_detail`.`biaya_finishing`,
                                        `transaksi_detail`.`diskon`,
                                        `transaksi_detail`.`pot_kecil`,
                                        `transaksi_detail`.`potongan`,
                                        `transaksi_detail`.`subtotal`,
                                        `transaksi_detail`.`st_ubahan`")
                                ->join("order_produk","transaksi.id_order = order_produk.id_order","inner")
                                ->join("konsumen","konsumen.idkonsumen = order_produk.id_konsumen","inner")
                                ->join("transaksi_detail","transaksi_detail.id_transaksi = transaksi.id_transaksi","inner")
                                ->join("m_order_produk_detail"," m_order_produk_detail.id_detail_produk_order = transaksi_detail.id_detail_produk_order","inner")
                                ->join("kategori","kategori.idkategori = m_order_produk_detail.id_kategori","inner")
                                ->join("produk","produk.idproduk = m_order_produk_detail.id_produk","inner")
                                ->join("konversi_satuan","konversi_satuan.id_konversi = m_order_produk_detail.id_satuan","inner")
                                ->join("(SELECT 
                                        `id_konversi` AS id, `satuan_besar` AS satuan
                                        FROM
                                            konversi_satuan
                                        WHERE
                                            deleted = 0) AS satuan_harga","transaksi_detail.satuan_harga_by = `satuan_harga`.`id`","inner")
                                ->join("(SELECT 
                                            `id_transaksi` AS id, `no_faktur`
                                        FROM
                                            transaksi
                                        WHERE
                                            deleted = 0) AS t_master","t_master.id = transaksi.id_trans_ref_asli","left")
                                ->where("transaksi.deleted = 0 
                                        and `transaksi`.`id_transaksi` = '{$id_transaksi}'")
                                ->order_by("kategori.nmkategori","asc")
                                ->find_all();

        //echo $this->db->last_query();

        /*$data_bayar     = $this->bayar_trans_model
                                ->select("`idbayar_trans_kasir`,
                                            `st_bayar`,
                                            `dp`,
                                            `pembayaran`,
                                            `kurang_bayar`,
                                            `kembalian`")
                                ->where("deleted = 0 and id_transaksi = '{$id_transaksi}'")
                                ->order_by("created_on","desc")
                                ->limit(1,0)
                                ->find_all();*/

        $total_all_sudah_bayar  = 0;
        $total_all_kembalian    = 0;
        $dp                     = 0;
        $bayar                  = 0;
        $krg_bayar              = $data[0]->grand_total;
        $kembalian              = 0;
        $deposit                = 0;
        $st_sumber_bayar        = 0;

        $id_konsumen            = $data[0]->idkonsumen;

        $dt_bayar   = $this->bayar_trans_model
                            ->select("`dp`,
                                     dp_deposit,   
                                    `bayar`,
                                    pembayaran, 
                                    `kurang_bayar`, 
                                    `kembalian`,
                                    `st_dp`,
                                    `st_bayar`,
                                    `st_sumber_bayar`")
                            ->where("`id_transaksi` = '{$id_transaksi}'
                                        AND deleted = 0")
                            ->order_by("created_on","desc")
                            ->order_by("order","desc")
                            ->limit(1,0)
                            ->find_all();

        //echo $this->db->last_query();

        if(is_array($dt_bayar) && count($dt_bayar) ){

            $krg_bayar          = $dt_bayar[0]->kurang_bayar;
            $kembalian          = $dt_bayar[0]->kembalian;
            $st_sumber_bayar    = $dt_bayar[0]->st_sumber_bayar;
            $bayar              = $dt_bayar[0]->pembayaran;
            $dp                 = $dt_bayar[0]->bayar < 0 ? 0 : $dt_bayar[0]->dp + $dt_bayar[0]->dp_deposit;

            $deposit            = $st_sumber_bayar == 1 ? $dt_bayar[0]->bayar + $dt_bayar[0]->dp_deposit : $dt_bayar[0]->dp_deposit;

        }

        $sisa_saldo     = get_deposit($id_konsumen);
        $st_saldo       = $sisa_saldo !== false ? 1 : 0;

        $this->template->set('data',$data);
        $this->template->set('idt', $identitas);
        $this->template->set('op', $idt_op);

        // var bayar
        
        $this->template->set('dp',$dp);
        $this->template->set('deposit',$deposit);
        $this->template->set('sumber_bayar',$st_sumber_bayar);
        $this->template->set('bayar',$bayar);
        $this->template->set('krg_bayar',$krg_bayar);
        $this->template->set('kembalian',$kembalian);

        $this->template->set('st_deposit',$st_saldo);        
        $this->template->set('sisa_saldo',$sisa_saldo);        

        $this->template->set_layout("cetak_struk");
        $this->template->title(lang('title_view'));
        $this->template->render("cetak_struk_kecil");

    }

    public function get_harga_cetak(){

        $harga_baru         = $this->input->post("harga_baru");
        $pot_harga          = $this->input->post("pot_harga");
        $diskon             = $this->input->post("diskon");
        $satuan_harga       = $this->input->post("satuan_harga");
        $tipe_produk        = $this->input->post("tipe_produk"); 
        $uk_p               = $this->input->post("uk_p"); 
        $uk_l               = $this->input->post("uk_l"); 
        $satuan_uk          = $this->input->post("satuan_uk");
        $jml_cetak          = $this->input->post("jml_cetak"); 
        $satuan_jml_cetak   = 8;        

        // start hitung potongan pakai

        $potongan       = ($harga_baru * ($diskon / 100)) + $pot_harga;
        $harga_baru     -= $potongan;

        // end hitung potongan pakai

        $total_potongan     = 0;

        if($tipe_produk == 0){ //fix yang dihitung hanya jml cetaknya.

            $biaya_cetak    = $harga_baru * $jml_cetak;
            $total_potongan = $potongan * $jml_cetak;
            
            $hasil = ['cetak' => $biaya_cetak, 'potongan' => $total_potongan];

        }else{

            // konversi ke hitungan terkecil
            $p_real = hitung_ke_satuan_kecil($id_konversi = $satuan_uk, $qty = $uk_p);
            $l_real = hitung_ke_satuan_kecil($id_konversi = $satuan_uk, $qty = $uk_l);

            $st_harga_terkecil  = hitung_ke_satuan_kecil($id_konversi = $satuan_harga, 
                                                                    $qty = 1);


            $harga_real         = $harga_baru / $st_harga_terkecil['qty'];  
            $pot_real           = $potongan / $st_harga_terkecil['qty'];  

            $biaya_cetak        = ($p_real['qty'] * $l_real['qty']) * $harga_real;
            $total_potongan     = ($p_real['qty'] * $l_real['qty']) * $pot_real;

            $biaya_cetak        *= $jml_cetak;
            $total_potongan     *= $jml_cetak;     

            $hasil = ['cetak' => $biaya_cetak, 'potongan' => $total_potongan];

        }

        echo json_encode($hasil);


    }

    public function info_delete(){

        if(!$this->input->is_ajax_request()){

            $return = false;

        }

        $id_transaksi   = $this->input->post('id');

        //ambil id_order, id_trans_master milik transaksi
        $dt_trans           = $this->trans_model->select("id_trans_ref_asli")
                                    ->find($id_transaksi);

        $id_trans_master    = $dt_trans->id_trans_ref_asli;

        //ambil semua nilai total bayar yang sudah dibayarkan konsumen
        $dt_bayar   = $this->bayar_trans_model->select("sum(`bayar`) as total")
                            ->where("deleted = 0 and id_transaksi_master = '{$id_trans_master}'")
                            ->find_all(); 

        $total_bayar    = $dt_bayar[0]->total;      

        if($total_bayar > 0){

            $ket    = "Mendelete transaksi kasir ini berarti : <br><br>
                        1. Menghapus semua transaksi dengan master referensi yang sama.<br><br>
                        2. Kasir wajib mengembalikan uang bayar konsumen sebesar <strong>Rp. ".number_format($total_bayar)."</strong><br><br><strong>Lanjutkan proses delete ?</strong>";

            $return = true;

        }else{

            $ket    = "Mendelete transaksi kasir ini berarti : <br><br>
                        1. Menghapus semua transaksi dengan master referensi yang sama.<br><br>
                        2. Kasir wajib menerima / meminta uang bayar kepada konsumen sebesar <strong>Rp. ".number_format($total_bayar)."</strong><br><br><strong>Lanjutkan proses delete ?</strong>";

            $return = true;

        }

        $arr_hasil  =   [
                            'status'    => $return,
                            'ket'       => $ket
                        ];

        echo json_encode($arr_hasil);
                
    }


}
	
?>