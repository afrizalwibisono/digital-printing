<table class="struk-kecil">
	<thead>
		<tr>
			<th colspan="6"><?= $idt->nm_perusahaan ?></th>
		</tr>
		<tr>
			<th colspan="6"><?= $idt->alamat ?>, <?= $idt->kota ?></th>
		</tr>
		<tr>
			<th colspan="6">Telp. <?= $idt->no_telp ?></th>
		</tr>
		<tr class="no-tran">
			<th colspan="6"><?= $data[0]->no_faktur ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
			
			foreach ($data as $key => $d) :
		?>

		<tr>
			<td colspan="6"><?= ucfirst($d->nm_produk) ?></td>
		</tr>
		<tr>
			<td align="10"><?= $d->jml_cetak ?></td>
			<td>D:</td>
			<td><?= number_format($d->biaya_design) ?></td>
			<td>C:</td>
			<?php

				$cetak 		= $d->biaya_cetak;
				$finishing 	= $d->biaya_finishing;
				$urgent 	= $d->kenaikan_value;
				$design 	= $d->biaya_design;

				$potongan 	= $d->potongan;

				$total 		= $cetak+$finishing+$urgent+$design;

				$potongan 	+= $total * ($d->diskon/100);

				$total 		-= $potongan;

				$cetak_tampil 	= $total - $design;

			?>
			<td><?= number_format($cetak_tampil) ?></td>
			<td><?= number_format($d->subtotal) ?></td>
		</tr>

		<?php
			endforeach;
		?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4"><?= lang('capt-table-final-total') ?></td>
			<td>:</td>
			<td><?= number_format($data[0]->total_all) ?></td>
		</tr>

		<?php
				if($data[0]->diskon_all >0) :
		?>
			<tr>
				<td colspan="4"><?= lang('capt-table-final-diskon') ?></td>
				<td>:</td>
				<td><?= number_format($data[0]->diskon_all) ?></td>
			</tr>
		
		<?php endif; 
		
			if($data[0]->pot_all >0) :
		?>

		<tr>
			<td colspan="4"><?= lang('capt-table-final-potongan') ?></td>
			<td>:</td>
			<td><?= number_format($data[0]->pot_all) ?></td>
		</tr>

		<?php endif; ?>

		<tr>
			<td colspan="4"><?= lang('capt-table-final-gt') ?></td>
			<td>:</td>
			<td><b><?= number_format($data[0]->grand_total) ?></b></td>
		</tr>
		<tr>
			<td colspan="4"><?= lang('capt-table-final-bayar') ?></td>
			<td>:</td>
			<td><b><?= number_format($data[0]->bayar + $data[0]->bayar2) ?></b></td>
		</tr>
		
		<?php
				if($data[0]->kurang_bayar >0) :
		?>
			<tr>
				<td colspan="4"><?= lang('capt-table-final-kurang') ?></td>
				<td>:</td>
				<td><b><?= number_format($data[0]->kurang_bayar) ?></b></td>
			</tr>
		<?php endif; ?>

		<tr>
			<td colspan="4"><?= lang('capt-table-final-kembali') ?></td>
			<td>:</td>
			<td><?= number_format($data[0]->lebih_bayar) ?></td>
		</tr>

		<tr>
			<td colspan="5" class="footer-note">===== Terima Kasih =====</td>
		</tr>
	</tfoot>
</table>

<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function(e){
		window.print();
		//window.close();
	});
</script>	
