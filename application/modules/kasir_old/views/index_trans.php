<?php
		$ENABLE_ADD     = has_permission('Kasir.Add'); 

    	if($this->uri->segment(3) === false){

    		$idtrans 	= "";

    	}else{

    		$idtrans 	= urldecode($this->uri->segment(3));

    	}

    	if($this->uri->segment(4) === false){
    		
    		$stnota 	= 0;

    	}else{

    		$stnota 	= 1;

    	}	

?>

<div class="box box-primary">
<?= form_open(site_url('kasir'),array('name'=>'frm_index','id'=>'frm_index','role'=>'form','data-idtrans'=>$idtrans)) ?>

	<div class="box-header">
		
		<div class="box-body form-horizontal">

			<div class="col-sm-6">			

				<div class="form-group" style="margin-bottom: 5px">
					<label for="tgl1" class="col-sm-4 control-label"><?= lang("capt-filter-waktu") ?></label>
					<div class="col-sm-7">
						<div class="input-group input-daterange" >
							<input type="text" name="tgl1" id="tgl1" class="form-control" readonly placeholder="<?= lang('capt-seacrh-order-tgl1') ?>" value = "<?= set_value('tgl1',isset($tgl1) ? $tgl1 : '') ?>" >
							<span class="input-group-addon">to</span>
							<input type="text" name="tgl2" id="tgl2" class="form-control" readonly placeholder="<?= lang('capt-seacrh-order-tgl2') ?>" value = "<?= set_value('tgl2',isset($tgl2) ? $tgl2 : '') ?>" >
						</div>
					</div>
				</div>

				<div class="form-group" style="margin-bottom: 5px">
					<label for="konsumen" class="col-sm-4 control-label" ><?= lang("capt-filter-konsumen") ?></label>
					<div class="col-sm-7">
						<select class="form-control" name="konsumen" id="konsumen">
							<option></option>
							<?php 
									if(isset($dt_konsumen) && is_array($dt_konsumen) && count($dt_konsumen)):
										foreach ($dt_konsumen as $key => $isi) :
											
							?>

									<option value="<?= $isi->id ?>" <?= set_select('konsumen',$isi->id, isset($konsumen) && $konsumen == $isi->id) ?> ><?= ucwords($isi->nm) ?></option>

							<?php
										endforeach;
									endif;
							?>
						</select>
					</div>

				</div>

				<div class="form-group" style="margin-bottom: 5px">
					<label for="no_faktur" class="col-sm-4 control-label">
						<?= lang("capt-filter-no-trans-sub") ?>
					</label>
					<div class="col-sm-7">
						<input type="text" name="no_faktur" id="no_faktur" class="form-control" value="<?= set_value('no_faktur',isset($no_faktur) ? $no_faktur : '') ?>">
					</div>
				</div>	

			</div>	

			<div class="col-sm-6">			

				<div class="form-group" style="margin-bottom: 5px">
					<label for="waktu_tempo" class="col-sm-4 control-label"><?= lang("capt-filter-waktu-tempo") ?></label>
					<div class="col-sm-7">
						<div class="input-group input-daterange">
							<input type="text" name="tgl1_tempo" id="tgl1_tempo" class="form-control" readonly placeholder="<?= lang('capt-seacrh-order-tgl1') ?>" value = "<?= set_value('tgl1_tempo',isset($tgl1_tempo) ? $tgl1_tempo : '') ?>" >
							<span class="input-group-addon">to</span>
							<input type="text" name="tgl2_tempo" id="tgl2_tempo" class="form-control" readonly placeholder="<?= lang('capt-seacrh-order-tgl2') ?>" value = "<?= set_value('tgl2_tempo',isset($tgl2_tempo) ? $tgl2_tempo : '') ?>" >
						</div>
					</div>
				</div>

				<div class="form-group" style="margin-bottom: 5px">
					<label for="status" class="col-sm-4 control-label"><?= lang("capt-filter-status") ?></label>
					<div class="col-sm-7">
						<select class="form-control" id="status" name="status">
							<option></option>
							<option value="0" <?= set_select('status','0',isset($status) && $status == '0') ?> >
								<?= lang("isi-filter-status-utang") ?>
							</option>
							<option value="1" <?= set_select('status','1',isset($status) && $status == '1') ?> >
								<?= lang("isi-filter-status-lunas") ?>
							</option>
						</select>						
					</div>
				</div>

				<!-- <div class="form-group" style="margin-bottom: 5px">
					<label for="status" class="col-sm-4 control-label"><?= lang("capt-filter-order-revisi") ?></label>
					<div class="col-sm-7">
						<select class="form-control" id="st_order" name="st_order">
							<option></option>
							<option value="0" <?= set_select('st_order','0',isset($st_order) && $st_order == '0') ?> >
								<?= lang("isi-filter-order-bayar") ?>
							</option>
							<option value="1" <?= set_select('st_order','1',isset($st_order) && $st_order == '1') ?> >
								<?= lang("isi-filter-order-revisi") ?>
							</option>
						</select>						
					</div>
				</div> -->


			</div>

		</div>	

		<div class="box-footer">			

				<?php if($ENABLE_ADD && !isset($saldo_kosong)){ ?>

					<a href="<?= site_url('kasir/open_order') ?>" class='btn btn-success'>
						<?= lang('btn_create') ?>
					</a>	

				<?php } ?>

			<div class="pull-right">

				<button class="btn btn-primary"><?= lang("btn_filter") ?> <span class="fa fa-search"></span></button>
				
			</div>

				
		</div>

	</div>

	<!-- area daftar transaksi yang direvisi ordernya -->

	<div class="box-body">

		<?php 
			if(isset($data_rev) && is_array($data_rev) && count($data_rev)) :
		?>

		<div class="form-group">
			<div class="col-sm-4">
				<div class="checkbox">
				<input type="checkbox" name="tampil_rev" id="tampil_rev" checked>
				<span class="label label-warning"> <?= lang('capt-trans-centang-rev'); ?> </span>
				</div>
			</div>
		</div>	

		<div class="clearfix"></div>

		<div class="table-responsive" id="dft_rev">		
			<table class="table table-striped table-hover" >
				<thead>
					<tr class="success">
						<th class="text-center" width="30">#</th>
						<th class="text-center" ><?= lang("capt-trans-waktu") ?></th>
						<th class="text-center" ><?= lang("capt-trans-st-ref") ?></th>
						<th class="text-center" ><?= lang("capt-trans-no-faktur") ?></th>
						<th class="text-center" ><?= lang("capt-trans-konsumen") ?></th>
						<th class="text-center" ><?= lang("capt-trans-total") ?></th>
						<th class="text-center" ><?= lang("capt-trans-status") ?></th>
						<th></th>
					</tr>	
				</thead>
				<tbody>
				<?php foreach ($data_rev as $key => $rev) : ?>
					<tr>
						<td class="text-center" >
							<input type="hidden" name="dft_idtrans[]" id="dft_idtrans" value="<?= $rev->id ?>">
							<input type="hidden" name="dft_idorder[]" id="dft_idorder" value="<?= $rev->id_order ?>">
							<?= ($key+1) ?>
						</td>
						<td class="text-center" ><?= $rev->created_on ?></td>
						<td class="text-center" >
							<?php if($rev->st_trans_master == 1) : ?>
								<span class="label label-success">Master <?= $rev->st_history == 1 ? "<i class = 'fa fa-lock' ></i>" : ""; ?> </span>
							<?php else : ?>
								<span class="label label-warning">Referensi</span>
							<?php endif; ?>	
								
						</td>
						<td class="text-center" ><?= $rev->no_faktur ?></td>
						<td class="text-center" >
							<?= ucwords($rev->panggilan." ".$rev->nama) ?>
						</td>
						<td class="text-center" ><?= number_format($rev->grand_total) ?></td>
						<td class="text-center" >
							<?php
								if($rev->st_lunas && ($rev->st_revisi == 2 || $rev->st_revisi == 0)) :
							?>
								<span class="label label-success"><?= lang("capt-label-lunas") ?></span>
							<?php 
								elseif($rev->st_revisi == 1) :
							?>
								<span class="label label-danger"><?= lang("capt-label-hutang-ref") ?></span>	
							<?php
								else:
							?>
								<span class="label label-danger"><?= lang("capt-label-blm-lunas") ?></span>
							<?php endif ?>
						</td>
						<td class="text-center">
							<a class="text-black" data-id="<?= $isi->id ?>" onclick="cetak(this, event)" href="#cetak" data-toggle="tooltip" data-placement="left" title="Cetak Nota"><i class="fa fa-print"></i></a> 
							|
							<?php if($rev->st_history == 0 && ($rev->st_lunas == 0 || $rev->st_revisi == 1) ): ?>
							<a class="text-danger" href="<?= site_url('kasir/pelunasan/' . $rev->id); ?>" data-toggle="tooltip" data-placement="left" title="Pelunasan Transaksi"><i class="fa fa-money"></i></a> 
							|
							<?php endif; ?>
							<a class="text-black" href="<?= site_url('kasir/view/' . $rev->id); ?>" data-toggle="tooltip"  data-placement="left" title="Lihat Transaksi" target="_blank" ><i class="fa fa-folder-open"></i></a> 
						</td>
					</tr>
				<?php endforeach; ?>	
				</tbody>	
			</table>
		</div>	

		<?php
			endif;
		
			if(isset($data) && is_array($data) && count($data)) :
		?>

		<!-- area daftar transaksi Normal -->

		<div class="label label-success">
			<?= lang("capt-trans-judul-daftar") ?>
		</div>
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th class="text-center" width="30">#</th>
						<th class="text-center" ><?= lang("capt-trans-waktu") ?></th>
						<th class="text-center" ><?= lang("capt-trans-st-ref") ?></th>
						<th class="text-center" ><?= lang("capt-trans-no-faktur") ?></th>
						<th class="text-center" ><?= lang("capt-trans-konsumen") ?></th>
						<th class="text-center" ><?= lang("capt-trans-total") ?></th>
						<th class="text-center" ><?= lang("capt-trans-status") ?></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($data as $key => $isi) :
					?>
						<tr >
							<td class="text-center" >
								<input type="hidden" name="dft_idtrans[]" id="dft_idtrans" value="<?= $isi->id ?>">
								<input type="hidden" name="dft_idorder[]" id="dft_idorder" value="<?= $isi->id_order ?>">
								<?= $numb ?>
							</td>
							<td class="text-center" ><?= $isi->created_on ?></td>
							<td class="text-center" >
								<?php if($isi->st_trans_master == 1) : ?>
									<span class="label label-success">Master <?= $isi->st_history == 1 ? "<i class = 'fa fa-lock' ></i>" : ""; ?> </span>
								<?php else : ?>
									<span class="label label-warning">Referensi</span>
								<?php endif; ?>	
									
							</td>
							<td class="text-center" ><?= $isi->no_faktur ?></td>
							<td class="text-center" >
								<?= ucwords($isi->panggilan." ".$isi->nama) ?>
							</td>
							<td class="text-center" ><?= number_format($isi->grand_total) ?></td>
							<td class="text-center" >
								<?php
									if($isi->st_lunas && ($isi->st_revisi == 2 || $isi->st_revisi == 0)) :
								?>
									<span class="label label-success"><?= lang("capt-label-lunas") ?></span>
								<?php 
									elseif($isi->st_revisi == 1) :
								?>
									<span class="label label-danger"><?= lang("capt-label-hutang-ref") ?></span>	
								<?php
									else:
								?>
									<span class="label label-danger"><?= lang("capt-label-blm-lunas") ?></span>
								<?php endif ?>
							</td>
							<td class="text-center">
								<a class="text-black" data-id="<?= $isi->id ?>" onclick="cetak(this, event)" href="#cetak" data-toggle="tooltip" data-placement="left" title="Cetak Nota"><i class="fa fa-print"></i></a> 
								|
								<?php if($isi->st_history == 0 && ($isi->st_lunas == 0 || $isi->st_revisi == 1) ): ?>
								<a class="text-danger" href="<?= site_url('kasir/pelunasan/' . $isi->id); ?>" data-toggle="tooltip" data-placement="left" title="Pelunasan Transaksi"><i class="fa fa-money"></i></a> 
								|
								<a class="text-primary" href="<?= site_url('kasir/cicilan_bayar/' . $isi->id); ?>" data-toogle='tooltip' data-placement="left" title="Cicilan" ><i class="fa fa-list-ol"></i></a>
								|
								<?php endif; ?>
								<a class="text-black" href="<?= site_url('kasir/view/' . $isi->id); ?>" data-toggle="tooltip"  data-placement="left" title="Lihat Transaksi" target="_blank" ><i class="fa fa-folder-open"></i></a> 
								|
								<a class="text-black" href="<?= site_url('log_proses_order/view/'.$isi->id_order) ?>" data-toggle="tooltip" data-placement="left" title="History Proses" target="_blank"><i class="fa fa-clock-o"></i></a> 			
							</td>

						</tr>
					<?php
							$numb++;
						endforeach;
					?>
				</tbody>
			</table>
		</div>

	</div><!-- akhir body -->

	<div class="box box-footer clearfix">	
		
		<?php 
		echo $this->pagination->create_links(); 
		?>	
	</div>
	<?php
			else:
	?>

		<div class="alert alert-info" role="alert">
        	<p><i class="fa fa-warning"></i> &nbsp; <?= lang('konfirmasi-data-tidak-ada') ?></p>
    	</div>

	<?php
			endif;
	?>

<?= form_close() ?>
</div> 


<!-- Modal -->
<!-- <div id="myModal" class="modal fade in" role="dialog">
  	<div class="modal-dialog">

	    <div class="modal-content">
	    	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal">&times;</button>
	        	<h4 class="modal-title">Cetak Nota</h4>
	      	</div>
	      	<div class="modal-body form-horizontal">
	        	<input type="hidden" name="id_trans_cetak" id="id_trans_cetak">
	        	<div class="form-group">
	        		<label class="control-label col-sm-3" for="tipe_nota" ><?= lang("capt-modal-jns-nota") ?></label>
	        		<div class="col-sm-9">
	        			<select class="form-control" name="tipe_nota" id="tipe_nota">
	        				<option value="0" selected ><?= lang("isi-jns-nota-besar") ?></option>
	        				<option value="1"><?= lang("isi-jns-nota-kecil") ?></option>
	        			</select>
	        		</div>
	        	</div>
	        	
	      	</div>
	      	<div class="modal-footer">
	      		<a href="#" id="cetak" class="btn btn-default" class="text-black" data-toggle="tooltip" data-placement="left" title="Cetak Nota"> <?= lang("capt-print") ?> </a>	

	      	</div>
		    	
	    </div>

  	</div>
</div>

 -->

<script type="text/javascript">
		
	var iddata 	= "<?= $idtrans ?>";
	var stnota 	= "<?= $stnota ?>";

</script>