<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for Setting Upload File
	
*/
class Setting_upload extends Admin_Controller {

	/**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Setting Upload.View";
    protected $managePermission = "Setting Upload.View";
    
    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('setting_upload/setting_upload');
        $this->load->model(array(
        							'setting_upload_model'
        						)
    						);

        $this->template->title(lang('setting_upload_title'));
		$this->template->page_icon('fa fa-gears');
    }


    public function index(){

        $this->auth->restrict($this->viewPermission);

        if(isset($_POST['simpan']) && has_permission($this->managePermission)){

        	if($this->simpan()){

        		$this->template->set_message(lang("label_simpan_sukses"),"success");

        	}

        }


        // ============ ambil data settingan ====================

        $data 	= $this->setting_upload_model->find(1);


        $asset 	= array(
        					'setting_upload/assets/js/form_setting.js'
        			);

        add_assets($asset);
     	
     	$this->template->set("data",$data);
     	$this->template->title(lang('setting_upload_title'));
		$this->template->page_icon('fa fa-gears');   
        $this->template->render('form_setting'); 

    }

    function simpan(){

    	$return 	= false;

    	$target 	= $this->input->post("target_server");
    	$url 		= $this->input->post("url_server");
    	

    	if($_POST['target_server'] == 0 ){

    		
    		$this->form_validation->set_rules("target_server","lang:capt-target-server","required");
    		$this->form_validation->set_rules("url_server","lang:capt-url-server","required");
    		
    		if($this->form_validation->run() === false){

    			$this->template->set_message(validation_errors(),"error");
    			return false;

    		}

    		$array_simpan 	= array(
    									'st_target_server' 	=> $target, 
    									'url' 				=> $url 

    								);


    	}else{
            $array_simpan   = array(
                                    'st_target_server'  => $target
                                );    
        }

    	$this->db->trans_start();

    		$this->setting_upload_model->update(1,$array_simpan);
    		$sql 	= $this->db->last_query();

    	$this->db->trans_complete();

    	if($this->db->trans_status() == false){

    		$return 		= false;
    		$keterangan 	= "Gagal, update target upload gambar";
    		$status 		= 0;

    	}else{

    		$return 		= true;
    		$keterangan 	= "Sukses, update target upload gambar";
    		$status 		= 1;

    	}

    	$nm_hak_akses   = $this->managePermission; 
        $kode_universal = "1";
        $jumlah         = 0;
       
        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

    	return $return;

    }

    function batal(){
    	redirect("setting_upload");
    }
    

}	