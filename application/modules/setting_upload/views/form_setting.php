<div class="box box-primary">

	<?= form_open($this->uri->uri_string(),array("name"=>"frm_baru", "id"=>"frm_baru","class"=>"form-horizontal","role"=>"form")) ?>

		<div class="box-body">
			
			<div class="form-group <?= form_error('target_server') ? 'has-error' : '' ?>">
				<label class="col-sm-2 control-label" for="target_server"><?= Lang("capt-target-server") ?></label>
				<div class="col-sm-4">
					<select name="target_server" id="target_server" class="form-control">
						<option value="0" <?= set_select("target_server","0",isset($data->st_target_server) && $data->st_target_server == 0 ) ?> ><?= lang("capt-isi-target-lokal") ?></option>
						<option value="1" <?= set_select("target_server","1",isset($data->st_target_server) && $data->st_target_server == 1) ?> ><?= lang("capt-isi-target-online") ?></option>
					</select>
				</div>
			</div>

			<div id="url_lokal" class="form-group <?= form_error('url_server') ? 'has-error' : '' ?>">
				<label class="col-sm-2 control-label" for="url_server"><?= Lang("capt-url-server") ?></label>
				<div class="col-sm-4">
					<input type="text" name="url_server" id="url_server" class="form-control" value="<?= set_value("url_server", isset($data->url) ? $data->url : '') ?>">
				</div>
			</div>

			<div class="form-group">
				
				<div class="col-sm-offset-2  col-sm-4">
					<button class="btn btn-primary" type="submit" name="simpan">
						<?= lang('capt-tbl-simpan') ?>
					</button>
					<?php
		                echo lang('capt-tbl-or') . ' ' . anchor('setting_upload/batal', lang('capt-tbl-batal'));
		            ?>


				</div>
			</div>			


		</div>

	<?= form_close() ?>
	
</div>