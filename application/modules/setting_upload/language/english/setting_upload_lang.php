<?php defined('BASEPATH')|| exit('No direct script access allowed');


// ============================
$lang['setting_title']				= 'Setting Lokasi Server';

// =============================

$lang['capt-target-server']			= 'Target Server Upload';
$lang['capt-url-server']			= 'Alamat Server';

$lang['capt-isi-target-lokal']		= 'Server Lokal';
$lang['capt-isi-target-online']		= 'Server Online';

$lang['capt-label-ftp']				= 'Koneksi FTP';
$lang['capt-ket-label-ftp']			= '(Kirim thumbnail gambar)';

$lang['capt-host-ftp']				= 'Host';
$lang['capt-port-ftp']				= 'Port';
$lang['capt-username-ftp']			= 'Username Login';
$lang['capt-pass-ftp']				= 'Password Login';
$lang['capt-path-ftp']				= 'Path upload';

// =============================

$lang['capt-tbl-simpan']			= 'Simpan';
$lang['capt-tbl-or']				= 'or';
$lang['capt-tbl-batal']				= 'Batal';


// =============================
$lang['label_simpan_sukses'] 		= "Simpan setting sukses";




