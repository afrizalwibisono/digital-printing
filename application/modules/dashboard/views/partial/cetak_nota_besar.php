<div class="a4">
	<div class="header">
		<div class="logo">
			<img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo" />
		</div>
		<div class="address">
			<h4><?= $idt->nm_perusahaan ?></h4>
            <?= $idt->alamat ?>, <?= $idt->kota ?><br>
            Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
            Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?>
		</div>
	</div>
	<div class="body">
		<div class="summary">
			<div>
				<table class="info-konsumen">
					<tr>
						<td rowspan="3" valign="top">Kepada</td>
						<td>Nama Konsumen</td>
					</tr>
					<tr>
						<td>Alamat</td>
					</tr>
					<tr>
						<td>Telp</td>
					</tr>
				</table>
			</div>
			<div>
				<table class="info-invoice">
					<tr>
						<td width="110">No. Invoice</td>
						<td>xxxxxxxxxxx</td>
					</tr>
					<tr>
						<td>No. Invoice Ref.</td>
						<td>xxxxxxxxxxx</td>
					</tr>
					<tr>
						<td>Tempo</td>
						<td>xxxx hari</td>
					</tr>
					<tr>
						<td>Tgl. Jatuh Tempo</td>
						<td>xx/xx/xxxx</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="main">
			<table>
				<thead>
					<tr>
						<th>No.</th>
						<th>Deskripsi</th>
						<th>Jml. Cetak</th>
						<th>Biaya Cetak</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1.</td>
						<td>
							Nama pekerjaan<br>
							Produk<br>
							Ukuran PxL
						</td>
						<td align="center">5</td>
						<td align="right">20.000</td>
						<td align="right">100.000</td>
					</tr>
					<tr>
						<td>2.</td>
						<td>
							Nama pekerjaan<br>
							Produk<br>
							Ukuran PxL
						</td>
						<td align="center">5</td>
						<td align="right">20.000</td>
						<td align="right">100.000</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="gtotal">
			<div>Grand Total :</div>
			<div>200.000</div>
		</div>
	</div>
	<div class="footer">
		<div>
			Penerima,
			<br>
			<br>
			<br>
			<br>
			[ .................................. ]
		</div>
		<div>
			Hormat kami,
			<br>
			<br>
			<br>
			<br>
			[Kasir]
		</div>
	</div>
</div>
<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function(e){
		window.print();
		// window.close();
	});
</script>