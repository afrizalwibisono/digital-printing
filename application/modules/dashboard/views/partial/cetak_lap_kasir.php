<table class="lap-kasir">
	<thead>
		<tr>
			<td colspan="2" class="title">Lap. Pendapatan Kasir</td>
		</tr>
		<tr>
			<td colspan="2">Tgl. 30/09/2018 11:58:23</td>
		</tr>
		<tr class="border-solid">
			<td colspan="2">Kasir 1</td>
		</tr>		
	</thead>
	<tbody>
		<tr class="border-dot">
			<td colspan="2" class="title">Transaksi</td>
		</tr>
		<tr>
			<td>A3</td>
			<td>2,000,000</td>
		</tr>
		<tr>
			<td>Indoor</td>
			<td>2,000,000</td>
		</tr>
		<tr class="border-dot">
			<td>Outdoor</td>
			<td>2,000,000</td>
		</tr>
		<tr>
			<td colspan="2" class="total">6,000,000</td>
		</tr>
		<tr class="border-solid">
			<td>Piutang</td>
			<td>2,000,000</td>
		</tr>
		<tr class="border-dot">
			<td colspan="2" class="title">Setoran Kasir</td>
		</tr>
		<tr>
			<td>Saldo Awal</td>
			<td>2,000,000</td>
		</tr>
		<tr>
			<td>Kas Masuk</td>
			<td>2,000,000</td>
		</tr>
		<tr class="border-dot">
			<td>Kas Keluar</td>
			<td>(2,000,000)</td>
		</tr>
		<tr>
			<td>Saldo Akhir</td>
			<td>2,000,000</td>
		</tr>
		<tr>
			<td>Saldo Ref</td>
			<td>2,500,000</td>
		</tr>
		<tr class="border-solid">
			<td>Selisih</td>
			<td>500,000</td>
		</tr>
		<tr class="border-dot">
			<td colspan="2" class="title">Non Tunai</td>
		</tr>
		<tr>
			<td>Debit</td>
			<td>2,000,000</td>
		</tr>
		<tr>
			<td>Transfer</td>
			<td>2,000,000</td>
		</tr>
	</tbody>
	<tfoot>
		<tr class="ttd">
			<td>( Kasir 1 )</td>
			<td>( Kasir 2 )</td>
		</tr>
	</tfoot>
</table>
<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function(e){
		window.print();
		// window.close();
	});
</script>