<table class="struk-kecil-58">
	<thead>
		<tr>
			<th colspan="3" class="barcode">
				<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAL4AAAAeAQMAAACopjYwAAAABlBMVEX///8AAABVwtN+AAAAAXRSTlMAQObYZgAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAClJREFUKJFjOHP+84HDzDY8PAYGZ84cMP7w5/Bnfn4gm2FUYlRiuEkAADI4b3LjxdlCAAAAAElFTkSuQmCC">
				<span>[ Barcode ]</span>
			</th>
		</tr>
		<tr>
			<th width="70">Konsumen</th>
			<th colspan="2">Bp. Andri</th>
		</tr>
		<tr>
			<th>No. Order</th>
			<th colspan="2">TR123456</th>
		</tr>
		<tr>
			<th>No. Faktur</th>
			<th colspan="2">FK123456</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="3"><strong>Nama Projek 1</strong></td>
		</tr>
		<tr>
			<td colspan="2">Produk 1</td>
			<td>2</td>
		</tr>
		<tr>
			<td colspan="3"><strong>Nama Projek 2</strong></td>
		</tr>
		<tr>
			<td colspan="2">Produk 2</td>
			<td>1</td>
		</tr>
		<tr>
			<td colspan="3"><strong>Nama Projek 3</strong></td>
		</tr>
		<tr>
			<td colspan="2">Produk 3</td>
			<td>1</td>
		</tr>
	</tbody>
</table>
<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function(e){
		window.print();
		window.close();
	});
</script>