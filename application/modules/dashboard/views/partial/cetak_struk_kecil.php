<table class="struk-kecil">
	<thead>
		<tr>
			<th colspan="3">
				<img src="<?= base_url('assets/images/report/'.$idt->report) ?>">
			</th>
		</tr>
		<tr>
			<th colspan="3">Jl. Ngesrep Barat III Semarang</th>
		</tr>
		<tr>
			<th colspan="3">Telp. 085726815115</th>
		</tr>
		<tr class="no-tran"></tr>
		<tr class="header-info">
			<th>No. Tran</th>
			<th>:</th>
			<th>KSR-NMKASIR-20180723-01</th>
		</tr>
		<tr class="header-info">
			<th>Waktu</th>
			<th>:</th>
			<th>20 Jul 2018 10:30</th>
		</tr>
		<tr class="header-info">
			<th>Konsumen</th>
			<th>:</th>
			<th>Karis</th>
		</tr>
		<tr class="no-tran"></tr>
	</thead>
</table>
<table class="struk-kecil">
	<thead>
		<tr>
			<td>Qty</td>
			<td>Product</td>
			<td>Total</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="3"><b>1x Spanduk 3 Meter (2 x 3 M)</b></td>
		</tr>
		<tr>
			<td colspan="2">[ 4,500 / m2 ]</td>
			<td>100.000</td>
		</tr>
		<tr>
			<td colspan="3"><b>2x Xbanner (1 x 2 M)</b></td>
		</tr>
		<tr>
			<td colspan="2">[ 5,000 / m2 ]</td>
			<td>200.000</td>
		</tr>
		<tr>
			<td colspan="3"><b>1x Baliho (2 x 3 M)</b></td>
		</tr>
		<tr>
			<td colspan="2">[ 6,000 / m2 ]</td>
			<td>300.000</td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="2">Total <span>:</span></td>
			<td>185,000</td>
		</tr>
		<tr>
			<td colspan="2">Diskon <span>:</span></td>
			<td>5%</td>
		</tr>	
		<tr>
			<td colspan="2">Potongan <span>:</span></td>
			<td>10,000</td>
		</tr>
		<tr>
			<td colspan="2">Bayar <span>:</span></td>
			<td>250,000</td>
		</tr>
		<tr>
			<td colspan="2">Krg. Bayar <span>:</span></td>
			<td>0</td>
		</tr>
		<tr>
			<td colspan="2">Tempo <span>:</span></td>
			<td>20 Jan 2019</td>
		</tr>		
		<tr>
			<td colspan="2">Kembali <span>:</span></td>
			<td>0</td>
		</tr>
		<tr>
			<td colspan="3" class="footer-note">===== Terima Kasih =====</td>
		</tr>
		<tr>
			<td colspan="3" class="footer-warning">
				Perhatian:<br>
				Struk ini bukan bukti pembayaran.
			</td>
		</tr>
	</tfoot>
</table>
<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function(e){
		window.print();
		// window.close();
	});
</script>