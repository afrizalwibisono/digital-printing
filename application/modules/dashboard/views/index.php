<?php
	$atts = array(
                    'width'       => 310,
                    'height'      => 400,
                    'scrollbars'  => 'yes',
                    'status'      => 'yes',
                    'resizable'   => 'yes',
                    'screenx'     => 100,
                    'screeny'     => 100,
                    'window_name' => '_blank',
                    'class'       => 'text-black',
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'left',
                    'title'       => 'Cetak Struk 80mm'        
            );

     $atts2 = array(
                    'width'       => 250,
                    'height'      => 400,
                    'scrollbars'  => 'yes',
                    'status'      => 'yes',
                    'resizable'   => 'yes',
                    'screenx'     => 100,
                    'screeny'     => 100,
                    'window_name' => '_blank',
                    'class'       => 'text-black',
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'left',
                    'title'       => 'Cetak Struk 58mm'        
            );

     $atts3 = array(
                    'width'       => 310,
                    'height'      => 400,
                    'scrollbars'  => 'yes',
                    'status'      => 'yes',
                    'resizable'   => 'yes',
                    'screenx'     => 100,
                    'screeny'     => 100,
                    'window_name' => '_blank',
                    'class'       => 'text-black',
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'left',
                    'title'       => 'Cetak Lap. Kasir'        
            );

     $atts4 = array(
                    'width'       => 810,
                    'height'      => 500,
                    'scrollbars'  => 'yes',
                    'status'      => 'yes',
                    'resizable'   => 'yes',
                    'screenx'     => 100,
                    'screeny'     => 100,
                    'window_name' => '_blank',
                    'class'       => 'text-black',
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'left',
                    'title'       => 'Cetak Nota Besar'        
            );
?>
<!-- 
<?= anchor_popup('dashboard/cetak_struk', "<i class='fa fa-print'></i> Cetak Struk 80mm", $atts); ?>
&nbsp;&nbsp;&nbsp;
<?= anchor_popup('dashboard/cetak_struk_58', "<i class='fa fa-print'></i> Cetak Struk 58mm", $atts2); ?>
&nbsp;&nbsp;&nbsp;
<?= anchor_popup('dashboard/cetak_lap_kasir', "<i class='fa fa-print'></i> Cetak Lap. Kasir", $atts3); ?>
&nbsp;&nbsp;&nbsp;
<?= anchor_popup('dashboard/cetak_nota_besar', "<i class='fa fa-print'></i> Cetak Nota Besar", $atts4); ?>
 -->
<br>
<br>
<h1 class="text-center"><?= $idt->nm_perusahaan ?></h1>
<div class="text-center">
	<img src="<?= base_url('assets/images/logo/'.$idt->logo) ?>" alt="logo">	
</div>
<br>
<br>
<h4 class="text-center"><?= $idt->alamat.", ".$idt->kota.", Telp: ".$idt->no_telp ?></h4>