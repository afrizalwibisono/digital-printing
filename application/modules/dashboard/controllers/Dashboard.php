<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {
	protected $viewSPK    	= "SPK.View";
    protected $viewProduksi = "Produksi.View";

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('dashboard');
		$this->load->helper('cookie');

		$this->load->model(['worksheet/worksheet_model']);
	}

	public function index()
	{

		//contoh penggunaan helper mendapatkan HPP bahan baku produksi

		/*$arr_idbarang 	= ['BB20180730103406B508','BB2018I092QT416X00411D345'];

		$cek 	= get_hpp_bb_by_produksi($arr_idbarang);

		print_r($cek);*/

		$this->template->title('Dashboard');
		$this->template->render('index');
	}

	public function save_menu_state()
	{
		$state = $this->input->get('state');
		$cookie = array(
					'name' 	 => 'menu-state',
					'value'  => $state,
					'expire' => '86500'
				);

		$this->input->set_cookie($cookie);

		echo json_encode(array('state'=> $state));
	}

	public function cetak_struk(){
		$this->template->set_layout('cetak_struk');
		$this->template->title('Struk');
        $this->template->render('partial/cetak_struk_kecil');
	}

	public function cetak_struk_58(){
		$this->template->set_layout('cetak_struk');
		$this->template->title('Struk 58mm');
        $this->template->render('partial/cetak_struk_kecil_58');
	}

	public function cetak_lap_kasir(){
		$this->template->set_layout('cetak_struk');
		$this->template->title('Laporan Kasir');
        $this->template->render('partial/cetak_lap_kasir');
	}

	public function cetak_nota_besar(){
		//Identitas
        $identitas = $this->identitas_model->find(1);
        $this->template->set('idt', $identitas);

		$this->template->set_layout('cetak_struk');
		$this->template->title('Nota Besar');
        $this->template->render('partial/cetak_nota_besar');
	}

	public function get_notif(){
		if(!$this->input->is_ajax_request())
        {
        	die(json_encode(['type' => 'error', 'msg' => lang('dashboard_only_ajax')]));
        }

        // Disable penggolongan Notif berdasarkan user / 15/02/2019 05:36
        /*$userdata 	= $this->auth->userdata();
        $idkaryawan = $userdata->idkaryawan;

        if($this->auth->is_admin()){
        	$dt_notif = $this->worksheet_model
	        				->select(["spk.id_worksheet", "count(spk.id_worksheet) as count", "if(spk.st_ambil = 0, 1, 0) AS spk", "spk.id_user"])
	        				->join('spk','worksheet.id_worksheet=spk.id_worksheet')
	        				->where(['spk.st_selesai' => 0, 'spk.deleted' => 0])
	        				->group_by(["spk.id_worksheet", "spk.st_ambil", "spk.id_user"])
	        				->find_all();
        }else{
        	$dt_notif = $this->worksheet_model
	        				->select(["spk.id_worksheet", "count(spk.id_worksheet) as count", "if(spk.st_ambil = 0, 1, 0) AS spk", "spk.id_user"])
	        				->join('spk','worksheet.id_worksheet=spk.id_worksheet')
	        				->join('karyawan_detail_worksheet','worksheet.id_worksheet=karyawan_detail_worksheet.id_worksheet')
	        				->join('karyawan','karyawan_detail_worksheet.idkaryawan=karyawan.idkaryawan')
	        				->where(['spk.st_selesai' => 0, 'spk.deleted' => 0, 'karyawan_detail_worksheet.deleted' => 0, 'karyawan.idkaryawan' => $idkaryawan])
	        				->group_by(["spk.id_worksheet", "spk.st_ambil", "spk.id_user"])
	        				->find_all();
        }*/
        // End Disable penggolongan Notif berdasarkan user

        $dt_notif = $this->worksheet_model
	        				->select(["spk.id_worksheet", "count(spk.id_worksheet) as count", "if(spk.st_ambil = 0, 1, 0) AS spk", "spk.id_user"])
	        				->join('spk','worksheet.id_worksheet=spk.id_worksheet')
	        				->where(['spk.st_selesai' => 0, 'spk.deleted' => 0])
	        				->group_by(["spk.id_worksheet", "spk.st_ambil", "spk.id_user"])
	        				->find_all();
        
	    $spk 	  = [];
	    $produksi = [];
	    if($dt_notif){
	    	foreach ($dt_notif as $key => $val) {
		    	if($val->spk == 1){ //SPK
		    		if(has_permission($this->viewSPK)){
		    			$spk[$val->id_worksheet] = $val->count;	
		    		}
		    	}else{ //Produksi
		    		// Disable penggolongan Notif berdasarkan user / 15/02/2019 05:36
		    		/*if(has_permission($this->viewProduksi) && $val->id_user == $userdata->id_user){*/
		    		// End Disable penggolongan Notif berdasarkan user
		    		if(has_permission($this->viewProduksi)){
		    			$produksi[$val->id_worksheet] = $val->count;	
		    		}
		    	}
		    }	
	    }

        $dt_worksheet = $this->worksheet_model->select(["id_worksheet"])
        								->find_all_by(['deleted' => 0]);
        $return = [
        		"spk" 			 => [],
        		"produksi"  	 => [],
        		"total_spk" 	 => 0,
        		"total_produksi" => 0,
        	];

        foreach ($dt_worksheet as $key => $val) {
        	// SPK Notif
        	if(array_key_exists($val->id_worksheet, $spk)){
        		$return["spk"][$val->id_worksheet]  = intval($spk[$val->id_worksheet]);
        		$return["total_spk"] 			   += $spk[$val->id_worksheet];
        	}else{
        		$return["spk"][$val->id_worksheet] = 0;
        	}
        	// Produksi Notif
        	if(array_key_exists($val->id_worksheet, $produksi)){
        		$return["produksi"][$val->id_worksheet]  = intval($produksi[$val->id_worksheet]);
        		$return["total_produksi"] 				+= $produksi[$val->id_worksheet];
        	}else{
        		$return["produksi"][$val->id_worksheet] = 0;
        	}
        }

        echo json_encode($return);
	}

	public function get_color(){
		$color_count = shell_exec('gs -o - -sDEVICE=inkcov /var/www/html/Tes_warna.pdf | grep -v "^ 0.00000  0.00000  0.00000" | grep "^ " | wc -l');
		$bw_count 	 = shell_exec('gs -o - -sDEVICE=inkcov /var/www/html/Tes_warna.pdf | grep "^ 0.00000  0.00000  0.00000" | grep "^ " | wc -l');
		
		print_r(['color' => $color_count, 'bw' => $bw_count]);
	}
}
