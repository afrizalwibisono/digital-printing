<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['settings_title_manage'] 	= 'General Settings';
$lang['settings_security'] 		= 'Security';
$lang['settings_email'] 		= 'Email';
$lang['settings_list'] 			= 'List';
$lang['settings_product'] 		= 'Product';

// form/table
$lang['settings_remember_pass'] = 'Remember Password Expiration (in second)';
$lang['settings_send_email'] 	= 'Email Send From';
$lang['settings_system_email'] 	= 'System Email';
$lang['settings_list_limit'] 	= 'List Limit';
$lang['settings_new_product_show'] = 'New Product to Show';
$lang['settings_user_activation']  = 'New User Activation';
$lang['settings_show_price']  	= 'Show Price';

$lang['settings_manual_activation'] = 'Manual by Admin';
$lang['settings_auto_activation'] 	= 'Auto Activation';

$lang['settings_btn_save'] 		= "Save";
$lang['settings_btn_cancel'] 	= "Cancel";
$lang['settings_or'] 			= "or";

$lang['settings_edit_success'] 	= 'General Settings saved successfully';
$lang['settings_edit_failed'] 	= 'General Settings saving failed';
$lang['settings_invalid_id'] 	= 'Invalid ID';