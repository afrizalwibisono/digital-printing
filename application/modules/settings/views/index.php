<div class="box box-primary">
    <!-- form start -->
    <?= form_open_multipart($this->uri->uri_string(),array('id'=>'frm_settings','name'=>'frm_settings','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
    		<div class="col-md-6">
    			<legend><?= lang('settings_product') ?></legend>
    			<div class="form-group <?= form_error('new_product_limit') ? ' has-error' : ''; ?>">
				    <label for="new_product_limit" class="col-md-4 control-label"><?= lang('settings_new_product_show') ?></label>
				    <div class="col-md-2">
				    	<input type="text" class="form-control" id="new_product_limit" name="new_product_limit" maxlength="45" value="<?php echo set_value('new_product_limit', isset($data->new_products_show_limit) ? $data->new_products_show_limit : ''); ?>" required autofocus>
				    </div>
			  	</div>
			  	<div class="form-group <?= form_error('show_price') ? ' has-error' : ''; ?>">
				    <label for="show_price" class="col-md-4 control-label"><?= lang('settings_show_price') ?></label>
				    <div class="col-sm-2">
				    	<select id="show_price" name="show_price" class="form-control">
				    		<option value="0" <?= set_select('show_price', 0, $data->show_price == 0) ?>>No</option>
				    		<option value="1" <?= set_select('show_price', 1, $data->show_price == 1) ?>>Yes</option>
				    	</select>
				    </div>
			  	</div>
			  	<legend><?= lang('settings_list') ?></legend>
			  	<div class="form-group <?= form_error('list_limit') ? ' has-error' : ''; ?>">
				    <label for="list_limit" class="col-md-4 control-label"><?= lang('settings_list_limit') ?></label>
				    <div class="col-md-2">
				    	<input type="text" class="form-control" id="list_limit" name="list_limit" maxlength="15" value="<?php echo set_value('list_limit', isset($data->list_limit) ? $data->list_limit : ''); ?>" required="">
				    </div>
			  	</div>
			  	<legend><?= lang('settings_email') ?></legend>
			  	<div class="form-group <?= form_error('email_sender') ? ' has-error' : ''; ?>">
				    <label for="email_sender" class="col-md-4 control-label"><?= lang('settings_email') ?></label>
				    <div class="col-md-6">
				    	<input type="text" class="form-control" id="email_sender" name="email_sender" maxlength="10" value="<?php echo set_value('email_sender', isset($data->email_sender) ? $data->email_sender : ''); ?>" required="">
				    </div>
			  	</div>
			  	<div class="form-group <?= form_error('system_email') ? ' has-error' : ''; ?>">
				    <label for="system_email" class="col-md-4 control-label"><?= lang('settings_system_email') ?></label>
				    <div class="col-md-6">
				    	<input type="text" class="form-control" id="system_email" name="system_email" maxlength="15" value="<?php echo set_value('system_email', isset($data->email_system_notification) ? $data->email_system_notification : ''); ?>" required="">
				    </div>
			  	</div>
			  	<legend><?= lang('settings_security') ?></legend>
			  	<div class="form-group <?= form_error('user_activation') ? ' has-error' : ''; ?>">
				    <label for="user_activation" class="col-md-4 control-label"><?= lang('settings_user_activation') ?></label>
				    <div class="col-md-2">
				    	<select id="user_activation" name="user_activation" class="form-control">
				    		<option value="0" <?= set_select('user_activation', 0, $data->new_user_need_activation == 0) ?>>No</option>
				    		<option value="1" <?= set_select('user_activation', 1, $data->new_user_need_activation == 1) ?>>Yes</option>
				    	</select>
				    </div>
			  	</div>
			  	<div class="form-group <?= form_error('remember_expiration') ? ' has-error' : ''; ?>">
				    <label for="remember_expiration" class="col-md-4 control-label"><?= lang('settings_remember_pass') ?></label>
				    <div class="col-md-5">
				    	<input type="text" class="form-control" id="remember_expiration" name="remember_expiration" maxlength="15" value="<?php echo set_value('remember_expiration', isset($data->auth_remember_length) ? $data->auth_remember_length : ''); ?>" required="">
				    </div>
			  	</div>
			  	<div class="form-group">
			  		<div class="col-md-4 col-md-offset-4">
			  			<button type="submit" name="save" class="btn btn-primary"><i class="fa fa-save"></i> <?= lang('settings_btn_save') ?></button>	
			  			<?php
		                    echo lang('settings_or') . ' ' . anchor('settings', lang('settings_btn_cancel'));
		                ?>	
			  		</div>
			  	</div>
    		</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->
<script type="text/javascript">
	$(function(){
		$("#show_price, #user_activation").select2();
	});
</script>