<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Suwito
 * @copyright Copyright (c) 2017, Suwito
 * 
 * This is controller for Copyright
 */

class Settings extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Settings.View";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('settings/settings');
        $this->load->model(array('settings_model'));

        $this->template->title(lang('settings_title_manage'));
		$this->template->page_icon('fa fa-gears');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if(isset($_POST['save']))
        {
            if($this->save_settings())
            {
                $this->template->set_message(lang('settings_edit_success'), 'success');
            }
        }

        $settings = $this->settings_model->order_by('setting_name', 'ASC')->find_all();
        $data = new stdClass();
        foreach ($settings as $key => $st) {
            $val = str_replace(".", "_", $st->setting_name);
            $data->{$val} = $st->value;
        }

        $assets = array(
                    'js/plugins/select2/css/select2.min.css',
                    'js/plugins/select2/css/select2-bootstrap.min.css',
                    'js/plugins/select2/js/select2.min.js'
                    );

        add_assets($assets);

        $this->template->set('data', $data);

        $this->template->render('index'); 
    }

   	protected function save_settings()
   	{
        $data = $this->identitas_model->find(1);

        $this->form_validation->set_rules('new_product_limit','lang:settings_new_product_show','required|trim|numeric');
        $this->form_validation->set_rules('show_price','lang:settings_show_price','required');
        $this->form_validation->set_rules('list_limit','lang:settings_list_limit','required|numeric');
        $this->form_validation->set_rules('email_sender','lang:settings_send_email','required|trim|max_length[255]');
        $this->form_validation->set_rules('system_email','lang:settings_system_email','required|trim|max_length[255]');
        $this->form_validation->set_rules('user_activation','lang:settings_user_activation','required');
        $this->form_validation->set_rules('remember_expiration','lang:settings_remember_pass','required|numeric');

        if ($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        unset($_POST['save']);
        
        $update_data = array();

        $update_data[] = array(
                        'setting_name' => 'auth.remember_length',
                        'value'        => $this->input->post('remember_expiration')
                        );
        $update_data[] = array(
                        'setting_name' => 'email.sender',
                        'value'        => $this->input->post('email_sender')
                        );
        $update_data[] = array(
                        'setting_name' => 'email.system.notification',
                        'value'        => $this->input->post('system_email')
                        );
        $update_data[] = array(
                        'setting_name' => 'list.limit',
                        'value'        => $this->input->post('list_limit')
                        );
        $update_data[] = array(
                        'setting_name' => 'new.products.show.limit',
                        'value'        => $this->input->post('new_product_limit')
                        );
        $update_data[] = array(
                        'setting_name' => 'new.user.need.activation',
                        'value'        => $this->input->post('user_activation')
                        );
        $update_data[] = array(
                        'setting_name' => 'show.price',
                        'value'        => $this->input->post('show_price')
                        );

        $result = $this->settings_model->update_batch($update_data, 'setting_name');

        if(!$result)
        {
            $this->template->set_message(lang('settings_edit_failed'), 'error');
        }

        return $result;
   	}
}
?>