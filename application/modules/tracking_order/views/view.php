<div class="box box-primary">
    <!--form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_tracking_order','name'=>'frm_tracking_order','role','class'=>'form-horizontal','enctype'=>'multipart/form-data'))?>
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="tdet_order" name="tdet_order">
                <thead>
                    <tr class="success">
                        <th rowspan="2" style="vertical-align:middle; text-align: center;"><?= lang('tracking_order_no') ?></th>
                        <th rowspan="2" style="vertical-align:middle; text-align: center;" colspan="2"><?= lang('tracking_order_nama_pekerjaan') ?></th>
                        <th rowspan="2" width="120px" style="vertical-align:middle;text-align: center;"><?= lang('tracking_order_subtotal') ?></th>
                        <th colspan="4" style="vertical-align:middle;text-align: center; "><?= lang('tracking_order_status') ?></th>
                    </tr>
                    <tr class="danger">
                        <th style="vertical-align: middle;" class="text-center"><?= lang('tracking_order_st_acc_produksi')?></th>
                        <th width="150px" style="vertical-align:middle;" class="text-center"><?= lang('tracking_order_st_produksi') ?></th>
                        <th width="150px" style="vertical-align:middle;" class="text-center"><?= lang('tracking_order_st_collecting') ?></th>
                        <th width="150px" style="vertical-align:middle;" class="text-center"><?= lang('tracking_order_st_ambil') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($detail) : ?>
                            <?php 
                                $no = 1;
                                foreach($detail as $key => $dt) :
                            ?>
                        <tr>
                            <td><?= $no++ ?></td>
                            <td><input type="hidden" name="id_detail_produk_order[]"" value="<?=$dt->id_detail_produk_order ?>"><img src="<?= base_url($dt->thumbnail)?>" width="65"></td>
                            <td>
                                <p style="font-weight: bold; margin-bottom: 0px;">
                                    <?= strtoupper($dt->nama_pekerjaan) ?>
                                </p>
                                <p style="margin-bottom: 0px;">
                                    <?= strtoupper($dt->nm_produk) ?>    
                                </p>
                                <p style="margin-bottom: 0px;">
                                    <?= $dt->st_finishing==0?'Finishing Standar':'Finishing Custom' ?>
                                </p>
                            </td>
                            <td align="right"><?= number_format($dt->sub_total)?></td>
                            <td align="center"><input type="checkbox" <?= ($dt->st_acc_produksi == 1?'checked="true"':'')?>"></td>
                            <td align="center"><input type="checkbox" <?= ($dt->st_produksi == 1?'checked="true"':'')?>"></td>
                            <td align="center"><input type="checkbox" <?= ($dt->st_collecting == 1?'checked="true"':'')?>"></td>
                            <td align="center"><input type="checkbox" <?= ($dt->st_ambil == 1?'checked="true"':'')?>"></td>
                        </tr>
                    <?php 
                        endforeach;
                    endif;
                    ?>
                </tbody>
            </table>
        </div>
        <div class="form-group">
            <div class="col-sm-9">
                <?php
                echo anchor('tracking_order', lang('tracking_order_btn_cancel'),array('class' => 'btn btn-success'));
                ?>
            </div>
        </div>
    </div>
    <?= form_close() ?>
</div>
