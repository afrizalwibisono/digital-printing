<?php $total = 0; ?>
<style type="text/css">
    hr {
        margin: 0px;
        border-width: 3px;
    }
</style>
<div class="box box-primary">
    <!--form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_order_produk','name'=>'frm_order_produk','role','class'=>'form-horizontal','enctype'=>'multipart/form-data'))?>
    <div class="box-body">
        <!-- setting upload config -->
        <input type="hidden" name="settings" id="settings" value='<?= $setting ?>'>
        <input type="hidden" name="target_server" id="target_server" value="<?= set_value('target_server', isset($data['target_server'])? $data['target_server']:'')?>">
        <input type="hidden" name="alamat_server" id="alamat_server" value="<?= set_value('alamat_server', isset($data['alamat_server'])? $data['alamat_server']:'')?>">
        
        <!-- end setting upload config -->
        <div class="form-group <?= form_error('no_transaksi') ? ' has-error' : ''; ?>">
            <label for="no_transaksi" class="col-sm-3 control-label"><?= lang('order_produk_no_transaksi') ?></label>
            <div class="col-sm-3">
                <input type="hidden" name="id_order" id="id_order" value="<?= set_value('id_order', isset($data['id_order'])?$data['id_order']:'')?>">
                <input class="form-control" type="text" name="no_transaksi" id="no_transaksi" value="<?= set_value('no_transaksi', isset($data['no_transaksi'])? $data['no_transaksi']:'')?>" readonly>
            </div>
        </div>
        <div class="form-group <?= form_error('tgl_order') ? ' has-error' : ''; ?>">
            <label for="tgl_order" class="col-sm-3 control-label"><?= lang('order_produk_tgl_order') ?></label>
            <div class="col-sm-3">
                <input class="form-control" type="text" name="tgl_order" id="tgl_order" value="<?= set_value('tgl_order', isset($data['tgl_order'])?$data['tgl_order']:'')?>" readonly>
            </div>
        </div>
        <div class="form-group <?= form_error('id_konsumen') ? ' has-error' : ''; ?>">
            <label for="id_konsumen" class="col-sm-3 control-label"><?= lang('order_produk_id_konsumen') ?></label>
            <div class="col-sm-7">
                <select id="id_konsumen" name="id_konsumen" class="form-control">
                    <option></option>
                    <?php foreach ($konsumen as $key => $record) : ?>
                    <option value="<?= $record->idkonsumen; ?>" <?= set_select('id_konsumen', $record->idkonsumen, isset($data['id_konsumen']) && $data['id_konsumen'] == $record->idkonsumen) ?>><?= $record->panggilan ."  ". $record->nama ?></option> 
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group <?= form_error('st_urgent') ? ' has-error' : ''; ?>">
            <label for="st_urgent" class="col-sm-3 control-label"><?= lang('order_produk_st_urgent') ?></label>
            <div class="col-sm-7">
                <select id="st_urgent" name="st_urgent" class="form-control">
                    <!-- 0: non urgent 1: urgent-->
                    <option value="0" <?= set_select('st_urgent', 0, isset($data->st_urgent) && $data->st_urgent == 0) ?>><?= lang('order_produk_st_urgent_non_urgent') ?></option> 
                    <option value="1" <?= set_select('st_urgent', 1, isset($data->st_urgent) && $data->st_urgent == 1) ?>><?= lang('order_produk_st_urgent_urgent') ?></option> 
                </select>
            </div>
        </div>
        <div id="d-tgl-selesai" class="form-group <?= form_error('tgl_selesai') ? ' has-error' : ''; ?>">
            <label for="tgl_selesai" class="col-sm-3 control-label"><?= lang('order_produk_tgl_selesai') ?></label>
            <div class="col-sm-3">
                <input class="form-control" type="text" name="tgl_selesai" id="tgl_selesai" value="<?= set_value('tgl_selesai', isset($data['tgl_selesai'])?$data['tgl_selesai']:'')?>">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-10">
                <hr>
            </div>
        </div>
        <div class="form-group <?= form_error('nama_pekerjaan') ? ' has-error' : ''; ?>">
            <label for="nama_pekerjaan" class="col-sm-3 control-label"><?= lang('order_produk_nama_pekerjaan') ?></label>
            <div class="col-sm-7">
                <input class="form-control" type="text" name="nama_pekerjaan" id="nama_pekerjaan" value="<?= set_value('nama_pekerjaan', isset($data->nama_pekerjaan)?$data->nama_pekerjaan:'')?>">
            </div>
        </div>
        <div class="form-group <?= form_error('id_kategori') ? ' has-error' : ''; ?>">
            <label for="id_kategori" class="col-sm-3 control-label"><?= lang('order_produk_id_kategori') ?></label>
            <div class="col-sm-7">
                <select id="id_kategori" name="id_kategori" class="form-control">
                    <option></option>
                    <?php foreach ($kategori as $key => $record) : ?>
                    <option value="<?= $record->idkategori; ?>" <?= set_select('id_kategori', $record->idkategori, isset($data->id_kategori) && $data->id_kategori == $record->idkategori) ?>><?= $record->nmkategori ?></option> 
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group <?= form_error('id_produk') ? ' has-error' : ''; ?>">
            <label for="id_produk" class="col-sm-3 control-label"><?= lang('order_produk_id_produk') ?></label>
            <div class="col-sm-7">
                <select id="id_produk" name="id_produk" class="form-control">
                    <option></option>
                </select>
            </div>
            <!-- jika st_ukuran = 1 berati panjang dan lebar gambar upload harus sesuai dengan panjang dan lebar di fild kategori -->
            <!-- jika 0 bebas; -->
            <!-- tapi jika set_dimensi(cekbok diform) di unceheck ukuran diabaikan -->
            <input type="hidden" name="st_ukuran" id="st_ukuran">
            <input type="hidden" name="p_kategori" id="p_kategori">
            <input type="hidden" name="l_kategori" id="l_kategori">
            <!-- cek minimal order di tabel produk.
            -jika st_minimal_order = 1
                -baca st_tipe
                    -jika 0(fix) maka jumlah cetak tidak boleh kurang dari st_minimal_order
                    -jika 1(custom) maka panjang x l order tidak boleh kurang dari panjang_minimal x lebar_minimal -->
            <input type="hidden" name="st_minimal_order" id="st_minimal_order">
            <input type="hidden" name="st_tipe" id="st_tipe">
            <input type="hidden" name="jml_minimal_order" id="jml_minimal_order">
            <input type="hidden" name="panjang_minimal" id="panjang_minimal">
            <input type="hidden" name="lebar_minimal" id="lebar_minal">

            <input type="hidden" name="XResolution" id="XResolution">
            <input type="hidden" name="YResolution" id="YResolution">
            <input type="hidden" name="imageWidth" id="imageWidth">
            <input type="hidden" name="imageHeight" id="imageHeight">

        </div>
        <div class="form-group <?= form_error('gambar') ? ' has-error' : ''; ?>">
            <label for="gambar" class="col-sm-3 control-label"><?= lang('order_produk_gambar_cetak') ?></label>
            <div class="col-sm-9">
                <div id="preview-gambar" style="display: none;padding-bottom: 5px;">
                    <img width="50%" height="50%" src="" id="img-preview" name="img-preview" onload="get_size(this)">
                </div>
                <div style="margin-bottom: 5px !important;">
                    <label id="info-gambar" class="label label-success" style="font-size: 12px" ></label>
                </div>
                <div class="col-sm-3" style="padding: 0px !important;">
                    <input type="file" name="gambar" id="gambar" value="<?= set_value('gambar', isset($data->gambar)?$data->gambar:'')?>">
                </div>
            </div>
        </div>
        <!-- cek dimensi sesuai gambar -->
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-7">
                <div class="checkbox" name="set_dimensi">
                  <label class="text-red">
                    <input type="checkbox" name="st_dimensi" id="st_dimensi"> <?= lang('order_produk_dimensi_susai_gambar') ?>
                  </label>
                </div>
            </div>
        </div>
        <div class="form-group <?= form_error('ukuran_cetak') ? ' has-error' : ''; ?>">
            <label for="ukuran_cetak" class="col-sm-3 control-label"><?= lang('order_produk_ukuran_cetak') ?></label>
            <div class="col-sm-7" style="padding: 0px;">
                <div class="col-sm-4" style="padding-right: 0px;">
                    <div class="input-group">
                        <span class="input-group-addon">P</span>
                        <input type="hidden" name="hide_p" id="hide_p">
                        <input class="form-control" type="text" name="p" id="p" value="<?= set_value('p', isset($data->p)?$data->p:'')?>" readonly>
                    </div>
                </div>
                <div class="col-sm-4" style="padding-right: 0px;">
                    <div class="input-group">
                        <span class="input-group-addon">L</span>
                        <input type="hidden" name="hide_l" id="hide_l">
                        <input class="form-control" type="text" name="l" id="l" value="<?= set_value('l', isset($data->l)?$data->l:'')?>" readonly>
                    </div>
                </div>
                <div class="col-sm-4">
                    <select id="id_konversi" name="id_konversi" class="form-control" onchange="konversi_panjang(this)">
                        <option></option>
                        <?php foreach ($konversi as $key => $record) : ?>
                        <option value="<?= $record->id_konversi; ?>" data-jml_kecil ="<?= $record->jml_kecil;?>" data-jml_besar ="<?= $record->jml_besar;?>" <?= set_select('konversi', $record->id_konversi, isset($data->konversi) && $data->konversi == $record->id_konversi) ?>><?= $record->satuan_besar ?></option> 
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        
        <div class="form-group <?= form_error('jml_order') ? ' has-error' : ''; ?>">
            <label for="jml_order" class="col-sm-3 control-label"><?= lang('order_produk_jml_order') ?></label>
            <div class="col-sm-1" style="padding-right : 0px;">
                <input class="form-control" type="text" name="jml_order" id="jml_order" value="<?= set_value('jml_order', isset($data->jml_order)?$data->jml_order:'')?>">
            </div>
            <div class="col-sm-2">
                <select id="id_konversi_jml_order" name="id_konversi_jml_order" class="form-control">
                    <option></option>
                    <?php foreach ($konversi_jml_order as $key => $record) : ?>
                    <option value="<?= $record->id_konversi; ?>" data-jml_kecil ="<?= $record->jml_kecil;?>" data-jml_besar ="<?= $record->jml_besar;?>" <?= set_select('konversi', $record->id_konversi, isset($data->konversi) && $data->konversi == $record->id_konversi) ?>><?= $record->satuan_besar ?></option> 
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="form-group <?= form_error('biaya_cetak') ? ' has-error' : ''; ?>">
            <label for="biaya_cetak" class="col-sm-3 control-label"><?= lang('order_produk_biaya_cetak') ?></label>
            <div class="col-sm-3">
                <input class="form-control" type="text" name="biaya_cetak" id="biaya_cetak" value="<?= set_value('biaya_cetak', isset($data->biaya_cetak)?$data->biaya_cetak:'')?>">
            </div>
        </div>
        <div class="form-group <?= form_error('biaya_desain') ? ' has-error' : ''; ?>">
            <label for="biaya_desain" class="col-sm-3 control-label"><?= lang('order_produk_biaya_desain') ?></label>
            <div class="col-sm-3">
                <input class="form-control" type="text" name="biaya_desain" id="biaya_desain" value="<?= set_value('biaya_desain', isset($data->biaya_desain)?$data->biaya_desain:'')?>">
            </div>
        </div>
        <div class="form-group <?= form_error('note_produk') ? ' has-error' : ''; ?>">
            <label for="note_produk" class="col-sm-3 control-label"><?= lang('order_produk_note_produk') ?></label>
            <div class="col-sm-7">
                <textarea class="form-control" id="note_produk" name="note_produk"><?php echo set_value('note_produk', isset($data->note_produk) ? $data->note_produk : ''); ?></textarea>
            </div>
        </div>
        <div class="form-group <?= form_error('st_finishing') ? ' has-error' : ''; ?>">
            <label for="st_finishing" class="col-sm-3 control-label"><?= lang('order_produk_st_finishing') ?></label>
            <div class="col-sm-7">
                <select id="st_finishing" name="st_finishing" class="form-control">
                    <option></option>
                    <option value="0" <?= set_select('st_finishing', 1, isset($data->st_finishing) && $data->st_finishing == 0) ?>><?= lang('order_produk_finishing_standart') ?></option>
                    <option value="1" <?= set_select('st_finishing', 1, isset($data->st_finishing) && $data->st_finishing == 1) ?>><?= lang('order_produk_finishing_custom') ?></option> 
                </select>
            </div>
        </div>
        <div id="d-hide">
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-7">
                    <div class="checkbox" name="st_finishing_standart">
                      <label class="text-red">
                        <!-- 0: digunakan 1: finishing stantard tidak digunakan -->
                        <input type="checkbox" name="st_finishing_standart" id="st_finishing_standart" checked="checked"> <?= lang('order_produk_finishing_standart_digunakan') ?>
                      </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-10">
                    <hr>
                </div>
            </div>
            <div class="form-group <?= form_error('id_finishing') ? ' has-error' : ''; ?>">
                <label for="id_finishing" class="col-sm-3 control-label"><?= lang('order_produk_id_finishing') ?></label>
                <div class="col-sm-7">
                    <select id="id_finishing" name="id_finishing" class="form-control">
                        <option></option>
                    </select>
                </div>
            </div>
            <div class="form-group <?= form_error('jml')?'has-error':'';?>">
                <label for="jml" class="control-label col-sm-3"><?= lang('order_produk_jml_pakai_finishing')?></label>
                <div class="col-sm-3">
                    <div class="input-group">
                    <input type="text" name="jml_finishing" id="jml_finishing" class="form-control" value="<?= set_value('jml',isset($data->jml) ? $data->jml:'');?>">
                    <span class="input-group-addon success">SET</span>
                    </div>
                </div>
                <div class="col-sm-2" style="padding: 0px; visibility: hidden;">
                    <select id="id_konversi_jml_finishing" name="id_konversi_jml_finishing" class="form-control" onchange="get_harga_finishing()">
                        <option></option>
                        <?php foreach ($konversi_jml_order as $key => $record) : ?>
                        <option value="<?= $record->id_konversi; ?>" data-jml_kecil ="<?= $record->jml_kecil;?>" data-jml_besar ="<?= $record->jml_besar;?>" <?= set_select('konversi', $record->id_konversi, isset($data->konversi) && $data->konversi == $record->id_konversi) ?>><?= $record->satuan_besar ?></option> 
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group <?= form_error('note_finishing') ? ' has-error' : ''; ?>">
                <label for="note_finishing" class="col-sm-3 control-label"><?= lang('order_produk_note_finishing') ?></label>
                <div class="col-sm-7">
                    <textarea class="form-control" id="note_finishing" name="note_finishing"><?php echo set_value('note_finishing', isset($data->note_finishing) ? $data->note_finishing : ''); ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="harga_finishing" class="control-label col-sm-3"><?= lang('order_produk_harga_finishing')?></label>
                <div class="col-sm-3">
                    <input type="text" name="harga_finishing" id="harga_finishing" class="form-control" value="<?= set_value('harga', isset($data->harga)?$data->harga:'');?>" readonly>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="button" name="add_finishing" id="add_finishing" class="btn btn-info"><?= lang('order_produk_btn_pakai_finishing') ?></button>
                </div>
            </div>
            <div class="col-md-12">
                <!-- Detail Finishing -->
                <p style="margin-top: 15px; margin-bottom: 3px;">
                    <span class="label label-success" style="font-size: 12px"><?= lang('order_produk_list_finishing') ?></span>
                </p>
                <div class="table-responsive">
                    <table class="table table-bordered" id="tdet_finishing" name="tdet_finishing">
                        <thead>
                            <tr class="success">
                                <th style="vertical-align:middle;"><?= lang('order_produk_no') ?></th>
                                <th style="vertical-align:middle;"><?= lang('order_produk_id_finishing') ?></th>
                                <th style="vertical-align: middle;"><?= lang('order_produk_note_finishing')?></th>
                                <th width="150px" style="vertical-align:middle;" class="text-center"><?= lang('order_produk_jml_pakai_finishing') ?></th>
                                <th width="150px" style="vertical-align:middle;"><?= lang('order_produk_harga_finishing') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4">
                                    Total
                                </td>
                                <td class="gtotal_finishing" align="right">
                                    
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="button" id="add_produk" name="add_produk" class="btn btn-info"><?= lang('order_produk_btn_tambah_order') ?></button>
                </div>
            </div>
        <div class="col-md-12">
            <!-- Detail Finishing -->
            <p style="margin-top: 15px; margin-bottom: 3px;">
                <span class="label label-warning" style="font-size: 12px"><?= lang('order_produk_list_order') ?></span>
            </p>
            <div class="table-responsive">
                <table class="table table-bordered" id="tdet_order" name="tdet_order">
                    <thead>
                        <tr class="success">
                            <th style="vertical-align:middle;"><?= lang('order_produk_no') ?></th>
                            <th style="vertical-align:middle;" colspan="2"><?= lang('order_produk_nama_pekerjaan') ?></th>
                            <th style="vertical-align: middle;"><?= lang('order_produk_jml_order')?></th>
                            <th width="150px" style="vertical-align:middle;" class="text-center"><?= lang('order_produk_biaya_cetak') ?></th>
                            <th width="150px" style="vertical-align:middle;" class="text-center"><?= lang('order_produk_biaya_desain') ?></th>
                            <th width="150px" style="vertical-align:middle;" class="text-center"><?= lang('order_produk_biaya_finishing') ?></th>
                            <th width="150px" style="vertical-align:middle;"><?= lang('order_produk_subtotal') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if($data['list_order']) : ?>
                                <?php 
                                    $no = 1;
                                    foreach($data['list_order'] as $key => $dt) :
                                ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><input type="hidden" name="id_detail_produk_order[]"" value="<?=$dt->id_detail_produk_order ?>"><img src="<?= base_url($dt->thumbnail)?>" width="65"></td>
                                <td>
                                    <p style="font-weight: bold; margin-bottom: 0px;">
                                        <?= strtoupper($dt->nama_pekerjaan) ?>

                                        <a href="#" onclick="remove_order(this)"><i class="fa fa-times-circle text-red"></i></a>

                                        <a id="edit_tgl" onclick="view_order(this, event)" data-id="<?= $dt->id_detail_produk_order?>" href="#" data-toggle="lightbox-view"> <i class="fa fa-folder-open"></i></a>

                                            
                                    </p>
                                    <p style="margin-bottom: 0px;">
                                        <?= strtoupper($dt->nm_produk) ?>    
                                    </p>
                                    <p style="margin-bottom: 0px;">
                                        <?= $dt->st_finishing==0?'Finishing Standar':'Finishing Custom' ?>
                                    </p>
                                </td>
                                <td><?= $dt->jml_cetak." ".$dt->satuan_besar?></td>
                                <td align="right"><?= number_format($dt->biaya_cetak)?></td>
                                <td align="right"><?= number_format($dt->biaya_design)?></td>
                                <td align="right"><?= number_format($dt->biaya_finishing)?></td>
                                <td align="right"><?= number_format($dt->sub_total)?></td>
                            </tr>
                                <?php $total+= $dt->sub_total; endforeach; ?>
                            <?php endif; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="7" align="right">
                                TOTAL:
                            </td>
                            <td class="gtotal_order" align="right">
                                <?= number_format($total) ?>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" name="save" id="save" class="btn btn-success"><?= lang('order_produk_btn_save') ?></button>
                <?php
                echo lang('order_produk_or') . ' ' . anchor('order_produk/cancel', lang('order_produk_btn_cancel'), array("onclick" => "return confirm('".lang('order_produk_cancel_confirm')."')"));
                ?>
            </div>
        </div>
    </div>
    <?= form_close() ?>
</div>
<!-- modal view -->
<div id="lightbox-view" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 5px !important;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Preview Detail Order</h4>
        </div>
        <div class="modal-body form-horizontal">
            <div class="form-group <?= form_error('nama_pekerjaan') ? ' has-error' : ''; ?>">
                <label for="nama_pekerjaan" class="col-sm-3 control-label"><?= lang('order_produk_nama_pekerjaan') ?></label>
                <div class="col-sm-3">
                    <span class="input" id="view_nama_pekerjaan"></span>
                </div>
            </div>
            <div class="form-group <?= form_error('id_kategori') ? ' has-error' : ''; ?>">
                <label for="id_kategori" class="col-sm-3 control-label"><?= lang('order_produk_id_kategori') ?></label>
                <div class="col-sm-4">
                    <span class="input" id="view_id_kategori_produk"></span>
                </div>
            </div>
            <div class="form-group <?= form_error('id_produk') ? ' has-error' : ''; ?>">
                <label for="id_produk" class="col-sm-3 control-label"><?= lang('order_produk_id_produk') ?></label>
                <div class="col-sm-4">
                    <span class="input" id="view_id_produk"></span>
                </div>
            </div>
            <div class="form-group <?= form_error('gambar') ? ' has-error' : ''; ?>">
                <label for="gambar" class="col-sm-3 control-label"><?= lang('order_produk_gambar_cetak') ?></label>
                <div class="col-sm-9">
                    <img width="50%" height="50%" src="" id="view_img" name="view_img">
                </div>
            </div>
            <!-- cek dimensi sesuai gambar -->
            <!-- <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <div class="checkbox" name="set_dimensi">
                      <span class="input" id="view_st_dimensi"></span>
                    </div>
                </div>
            </div> -->
            <div class="form-group <?= form_error('ukuran_cetak') ? ' has-error' : ''; ?>">
                <label for="ukuran_cetak" class="col-sm-3 control-label"><?= lang('order_produk_ukuran_cetak') ?></label>
                <div class="col-sm-9">
                    <div class="col-sm-2" style="padding: 0px;">
                        <span class="input" id="view_p"></span>
                    </div>
                </div>
            </div>
            <div class="form-group <?= form_error('st_biaya_cetak') ? ' has-error' : ''; ?>">
                <label for="st_biaya_cetak" class="col-sm-3 control-label"><?= lang('order_produk_st_biaya_cetak') ?></label>
                <div class="col-sm-4">
                    <span class="input" id="view_st_biaya_cetak"></span>
                </div>
            </div>
            <div class="form-group <?= form_error('jml_order') ? ' has-error' : ''; ?>">
                <label for="jml_order" class="col-sm-3 control-label"><?= lang('order_produk_jml_order') ?></label>
                <div class="col-sm-1">
                    <span class="input" id="view_jml_order"></span>
                </div>
                <div class="col-sm-2" style="padding: 0px;">
                    <span class="input" id="view_id_konversi_jml_order"></span>
                </div>
            </div>
            <div class="form-group <?= form_error('biaya_cetak') ? ' has-error' : ''; ?>">
                <label for="biaya_cetak" class="col-sm-3 control-label"><?= lang('order_produk_biaya_cetak') ?></label>
                <div class="col-sm-3">
                    <span class="input" id="view_biaya_cetak"></span>
                </div>
            </div>
            <div class="form-group <?= form_error('biaya_desain') ? ' has-error' : ''; ?>">
                <label for="biaya_desain" class="col-sm-3 control-label"><?= lang('order_produk_biaya_desain') ?></label>
                <div class="col-sm-3">
                    <span class="input" id="view_biaya_desain"></span>
                </div>
            </div>
            <div class="form-group <?= form_error('note_produk') ? ' has-error' : ''; ?>">
                <label for="note_produk" class="col-sm-3 control-label"><?= lang('order_produk_note_produk') ?></label>
                <div class="col-sm-3">
                    <span class="input" id="view_note_produk"></span>
                    <!-- <textarea class="form-control" id="view_note_produk" name="view_note_produk" disabled></textarea> -->
                </div>
            </div>
            <div class="form-group <?= form_error('st_finishing') ? ' has-error' : ''; ?>">
                <label for="st_finishing" class="col-sm-3 control-label"><?= lang('order_produk_st_finishing') ?></label>
                <div class="col-sm-4">
                    <span class="input" id="view_st_finishing"></span>
                </div>
            </div>
            <div class="col-md-12" id="detail-hide">
                <!-- view detail finishing -->
                <p style="margin-top: 15px; margin-bottom: 3px;">
                    <span class="label label-warning" style="font-size: 12px"><?= lang('order_produk_list_order') ?></span>
                </p>
                <div class="table-responsive">
                    <table class="table table-bordered" id="view_tdet_finishing" name="view_tdet_finishing">
                        <thead>
                            <tr class="success">
                                <th style="vertical-align:middle;"><?= lang('order_produk_no') ?></th>
                                <th style="vertical-align:middle;"><?= lang('order_produk_id_finishing') ?></th>
                                <th style="vertical-align: middle;"><?= lang('order_produk_note_finishing')?></th>
                                <th width="150px" style="vertical-align:middle;" class="text-center"><?= lang('order_produk_jml_pakai_finishing') ?></th>
                                <th width="150px" style="vertical-align:middle;"><?= lang('order_produk_harga_finishing') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4">
                                    Total
                                </td>
                                <td class="view_gtotal_finishing" align="right">
                                    
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
             
        </div>  
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" src="<?= base_url('assets/plugins/exif/exif.js')?>"></script>
<script type="text/javascript">
    if (typeof(Storage) !== "undefined") {
        sessionStorage.setItem("kode_edit", "<?= (isset($data->id_produk)) ? $data->id_produk : '' ?>");
    }
</script>