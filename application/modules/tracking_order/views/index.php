<div class="box box-primary"> 
  <?= form_open($this->uri->uri_string(),array('id'=>'frm_tracking_order','name'=>'frm_tracking_order','class'=>'form-inline')) ?> 
  <div class="box-header"> 
      <div class="pull-right"> 
        <div class="form-group">
          <input type="hidden" name="idkonsumen" id="idkonsumen" value="<?php echo isset($idkonsumen) ? $idkonsumen : ''; ?>" >
          <div class="input-daterange input-group"> 
              <input type="text" class="form-control" name="tgl_awal" id="tgl_awal" value="<?= isset($tgl_awal) && $tgl_awal !='' ? $tgl_awal : '' ?>" placeholder="<?= lang('penjulan_tgl_awal') ?>" required /> 
              <span class="input-group-addon text-black">to</span> 
              <input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?= isset($tgl_akhir) && $tgl_akhir !='' ? $tgl_akhir : '' ?>" placeholder="<?= lang('tracking_order_tgl_akhir') ?>" required /> 
          </div> 
          </div>
          <div class="form-group" style="padding: 0px;" id="d-konsumen">
              <select id="id_konsumen" name="id_konsumen" class="form-control">
                  <option></option>
                  <?php foreach ($konsumen as $key => $record) : ?>
                  <option value="<?= $record->idkonsumen; ?>" <?= set_select('id_konsumen', $record->idkonsumen, isset($data['id_konsumen']) && $data['id_konsumen'] == $record->idkonsumen) ?>><?= $record->panggilan ."  ". $record->nama ?></option> 
                  <?php endforeach; ?>
              </select>
          </div>
          <div class="form-group"> 
              <div class="input-group"> 
                  <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="Search" autofocus /> 
                  <div class="input-group-btn"> 
                      <button class="btn btn-default"><i class="fa fa-search"></i></button> 
                  </div> 
              </div> 
          </div> 
      </div> 
  </div> 
  <?php if (isset($results) && is_array($results) && count($results)>0) : ?> 
    <div class="box-body table-responsive no-padding"> 
              <table class="table table-hover table-striped"> 
                  <thead> 
                      <tr> 
                          <th width="50">NO.</th> 
                          <th><?= lang('tracking_order_no_transaksi') ?></th> 
                          <th><?= lang('tracking_order_id_konsumen') ?></th> 
                          <th><?= lang('tracking_order_tgl_order') ?></th> 
                          <th><?= lang('tracking_order_st_order')?></th>
                          <th><?= lang('tracking_order_st_urgent')?></th>
                          <th><?= lang('tracking_order_total_order') ?></th> 
                          <th width="25"></th> 
                      </tr> 
                  </thead> 
                  <tbody> 
                      <?php foreach ($results as $record) : ?> 
                        <!-- <?php echo"<pre>",print_r($record),"</pre>"?> --> 
                      <tr> 
                          <td><?= $numb; ?></td> 
                          <td><?= $record->no_transaksi ?></td>
                          <td><?= $record->nama ?></td> 
                          <td><?= $record->tgl_order ?></td> 
                          <td><?= ($record->st_order==1 ? "<span class='label label-info'>Order Reseller</span>" : "<span class='label label-warning'>Order Kantor</span>")   ?></td> 
                          <td><?= ($record->st_urgent==1 ? "<span class='label label-danger'>Urgent</span>" : "<span class='label label-success'>Non Urgent</span>")   ?></td>
                          <td><?= number_format($record->total_value_order) ?></td>
                          <td style="padding-right:20px"><a class="text-black" href="<?= site_url('tracking_order/view/' . $record->id_order); ?>" data-toggle="tooltip" data-placement="left" title="View Status Order"><i class="fa fa-folder-open"></i></a></td> 
                      </tr> 
                      <?php $numb++; endforeach; ?> 
                  </tbody> 
      </table> 
    </div><!-- /.box-body --> 
    <div class="box-footer clearfix"> 
      <?php
      echo $this->pagination->create_links();  
      ?> 
    </div> 
  <?php else: ?> 
    <div class="alert alert-info" role="alert"> 
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('tracking_order_no_records_found') ?></p> 
    </div> 
    <?php 
  endif; 
  echo form_close();  
  ?>   
</div><!-- /.box-->