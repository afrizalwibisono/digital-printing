$(document).ready(function(){
	var idkonsumen = $("#idkonsumen").val();
	if (idkonsumen.length > 0) {
		$("#d-konsumen").hide();
	}else{
		$("#d-konsumen").show();
	}
	$("#id_konsumen").select2({placeholder : '-Pilih Konsumen-', allowClear: true});    
	$('#tgl_awal,#tgl_akhir').datepicker({ 
      	todayBtn: "linked", 
      	format: "yyyy-mm-dd", 
      	autoclose: true, 
      	clearBtn: true, 
      	todayHighlight: true 
	});
});
