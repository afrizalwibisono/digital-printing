<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 * 
 * This is controller for order_produk

 */

class Tracking_order extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Tracking Order.View";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('tracking_order');
        $this->load->model(array(
                                'order_produk_model',
                                'produk_model',
                                'konsumen_model',
                                'order_produk_detail_model',
                                'order_produk_detail_finishing_model',
                                'kategori_model',
                                'konversi_satuan/konversi_satuan_model',
                                'setting_harga/setting_harga_model',
                                'setting_harga/setting_harga_detail_model',
                                'setting_promo/setting_promo_model',
                                'setting_upload/setting_upload_model'
                            ));

        $this->template->title(lang('tracking_order_title_manage'));
        $this->template->page_icon('fa fa-table');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search     = isset($_POST['table_search']) ? $this->input->post('table_search') :'';
            $tgl_awal   = isset($_POST['tgl_awal']) ? $this->input->post('tgl_awal') : '';
            $tgl_akhir  = isset($_POST['tgl_awal']) ? $this->input->post('tgl_akhir') : '';
            $id_konsumen= isset($_POST['id_konsumen']) ? $this->input->post('id_konsumen') :'';

        }
        else
        {
            $search     = isset($_GET['search']) ? $this->input->get('search') :'';
            $tgl_awal   = isset($_GET['tgl_awal']) ? $this->input->get('tgl_awal') : '';
            $tgl_akhir  = isset($_GET['tgl_awal']) ? $this->input->get('tgl_akhir') : '';
            $id_konsumen= isset($_GET['id_konsumen']) ? $this->input->get('id_konsumen') :'';
        }

        $filter = "?search=".$search;
        $addWhere = "";
        if($tgl_awal != '' && $tgl_akhir != '')
        {
            $filter   .= "&tgl_awal=".$tgl_awal."&tgl_akhir=".$tgl_akhir."&id_konsumen=".$id_konsumen;
            $addWhere .= " AND ( date(tgl_order) >='".$tgl_awal."' AND date(tgl_order) <='".$tgl_akhir."')";
        }
        else
        {
            $tgl_awal = date('Y-m-d');
            $tgl_akhir = date('Y-m-d');
            
            $filter   .= "&tgl_awal=".$tgl_awal."&tgl_akhir=".$tgl_akhir;
            $addWhere .= " AND ( date(tgl_order) >='".$tgl_awal."' AND date(tgl_order) <='".$tgl_akhir."')";
        }
        if ($id_konsumen !='') {
            $addWhere .= " AND order_produk.id_konsumen = '$id_konsumen' ";
        }
        $data_konsumen = $this->auth->userdata();
        $idkonsumen = $data_konsumen->idkonsumen;
        if ($idkonsumen !="") {
            $addWhere .= " AND order_produk.id_konsumen = '$idkonsumen' ";   
        }
        $search2 = $this->db->escape_str($search);
        
        $where = "order_produk.deleted = 0 $addWhere
                AND (`nama` LIKE '%$search2%' ESCAPE '!'
                OR `no_transaksi` LIKE '%$search2%' ESCAPE '!')";
                
        $total = $this->order_produk_model
                    ->join("konsumen","konsumen.idkonsumen=order_produk.id_konsumen","left")
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $assets = array('plugins/select2/dist/js/select2.js', 
                        'plugins/select2/dist/css/select2.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js', 
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
                        'tracking_order/assets/js/tracking_order.js',
                        'plugins/number/jquery.number.js'
                        );
        
        add_assets($assets);
        $konsumen   = $this->konsumen_model->find_all_by(['deleted
            =>0']);

        $data = $this->order_produk_model
                    ->join("konsumen","konsumen.idkonsumen=order_produk.id_konsumen","left")
                    ->where($where)
                    ->order_by('nama','ASC')
                    ->limit($limit, $offset)->find_all();

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('tgl_awal',$tgl_awal);
        $this->template->set('tgl_akhir',$tgl_akhir);
        $this->template->set('id_konsumen', $id_konsumen);
        $this->template->set('konsumen', $konsumen);
        $this->template->set('idkonsumen', $idkonsumen);
        $this->template->set("toolbar_title", lang('tracking_order_title_manage'));
        $this->template->title(lang('tracking_order_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }
    //view Order
    public function view($id_order = '')
    {
        $this->auth->restrict($this->viewPermission);

        if (!$id_order)
        {
            $this->template->set_message(lang("order_produk_invalid_id"), 'error');
            redirect('order_produk');
        }

        $data   = $this->order_produk_model
                    ->join('konsumen','konsumen.idkonsumen=order_produk.id_konsumen','left')
                    ->find($id_order);
        $detail = $this->order_produk_detail_model
                                            ->join('produk','produk.idproduk = order_produk_detail.id_produk','left')
                                            ->join('konversi_satuan','konversi_satuan.id_konversi=order_produk_detail.id_konversi_jml_cetak','left')
                                            ->find_all_by(['order_produk_detail.id_order'=> $id_order]);
        $this->template->set('data', $data);
        $this->template->set('detail', $detail);

        $this->template->title(lang('tracking_order_title_view'));
        $this->template->render('tracking_order/view');
    }

    
}
?>