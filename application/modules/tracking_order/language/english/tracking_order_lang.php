<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ============================
$lang['tracking_order_title_manage']		= 'Tracking Order';
$lang['tracking_order_title_new']			= 'Data Order Baru';
$lang['tracking_order_title_edit']			= 'Edit Data Order';

// from/table
$lang['tracking_order_no_transaksi']		= 'No. Transaksi';
$lang['tracking_order_id_konsumen']			= 'Konsumen';
$lang['tracking_order_tgl_order']			= 'Tanggal Order';
$lang['tracking_order_st_urgent']			= 'Status Urgensi';
$lang['tracking_order_st_order']			= 'Status Order';
$lang['tracking_order_total_order']			= 'Total Order';


$lang['tracking_order_st_urgent_non_urgent']= 'Non Urgent';
$lang['tracking_order_st_urgent_urgent']	= 'Urgent';

$lang['tracking_order_nama_pekerjaan']		= 'Nama Project/File';
$lang['tracking_order_subtotal']			= 'Biaya';
$lang['tracking_order_status']				= 'Status';
$lang['tracking_order_st_acc_produksi'] 	= 'ACC Produksi';
$lang['tracking_order_st_produksi']			= 'Produksi';
$lang['tracking_order_st_collecting']		= 'Collecting';
$lang['tracking_order_st_ambil']			= 'Pengambilan';


// label
$lang['tracking_order_finishing_standart']			 = 'Finishing Standart';
$lang['tracking_order_finishing_custom']			 = 'Finishing Custom';

$lang['tracking_order_no'] 			= 'NO';
$lang['tracking_order_deskripsi']	= 'Deskripsi';
$lang['tracking_order_harga']		= 'Harga';





//button
$lang['tracking_order_btn_cancel']			= 'Batal';


//messages
$lang['tracking_order_no_records_found']	= 'Data tidak ditemukan.';

$lang['tracking_order_invalid_id']			= 'ID tidak Valid';

