$(function(){

	$("#kategori").select2({
        placeholder :"-- Kategori --",
        allowClear  : true
  	});

});

function konfirm_delete(obj){

	var kode 	= $(obj).closest("tr").find("input[name='id_mesin[]']").val();

	console.log(kode);

	var ket = "Delete data mesin akan mengakibatkan.<br></br>"+
				"<strong>1.</strong> Untuk Kategori A3, data Bahan Baku Cetak akan hilang. Sehingga akan berpengaruh kepada <strong class='text-red'>Rekap Penggunaan Bahan Baku</strong> dan <strong>Report Penggunaan Bahan Baku</strong><br><br>"+
				"<strong>Apakah proses delete akan dilanjutkan ?</strong>"

	alertify.confirm(ket,
	function(){
		//tombol OK

		$("#id_delete").val(kode);
		$("#frm_index").submit();

	},
	function(){
		//tombol Cancel
		$("#id_delete").val(kode);
	});

}