$(function(){

	$("#dft_hpp_bb tbody tr").find("input[name='hpp_bb[]']").number(true);

	$("#id_kategori").select2({
        placeholder :"-- Kategori --",
        allowClear  : true
  	});

	$("#metode_rekap").select2();  	

	tampil_metode();

	$("#id_kategori").change(function(){

		tampil_metode();

	});

});

function tampil_metode(){

	var kategori  	= $("#id_kategori").val();
	var st_view 	= $("#st_view").val();

	if(kategori == 1){

		$("#area-metode-rekap-a3").show(400);

	}else{

		$("#area-metode-rekap-a3").hide(400);

	}

}