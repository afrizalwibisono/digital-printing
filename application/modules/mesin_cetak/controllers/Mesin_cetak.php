<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 */

class Mesin_cetak extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Mesin Cetak.View";
    protected $addPermission    = "Mesin Cetak.Add";
    protected $managePermission = "Mesin Cetak.Manage";
    protected $deletePermission = "Mesin Cetak.Delete";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('mesin_cetak/mesin');
        $this->load->model(
                                [
                                    'bahan_baku_model',
                                    'kategori_model',
                                    'mesin_model'
                                ]    
                            );
        $this->template->title(lang('title_index'));
		$this->template->page_icon('fa fa-table');
    }

    public function index(){
        
        $this->auth->restrict($this->viewPermission);

        if(isset($_POST['id_delete']) && strlen($_POST['id_delete']) > 0 && has_permission($this->deletePermission)){

            //proses delete mesin
            $id_mesin   = $_POST['id_delete'];
            $del        = $this->delete_mesin($id_mesin);

            if($del){

                $this->template->set_message(lang('msg-delete_sukses'),'success');

            }else{

                $this->template->set_message(lang('msg-delete_fail'),'error');

            }

        }


        $this->load->library('pagination');       

        $filter     = "";

        $where      = "mesin_cetak.deleted = 0";

        if(isset($_POST['univ'])){

            $cr_kategori    = $this->input->post('kategori');
            $cr_univ        = $this->input->post('univ');

        }else if(isset($_GET['univ'])){

            $cr_kategori    = $this->input->get('kategori');
            $cr_univ        = $this->input->get('univ');

        }else{

            $cr_kategori    = '';
            $cr_univ        = '';            

        }

        if(strlen($cr_kategori)){

            $where  .= " And mesin_cetak.idkategori = {$cr_kategori}";

        }

        if(strlen($cr_univ)){

            $where  .= " and mesin_cetak.nm_mesin like '%{$cr_univ}%'";

        }

        $filter = "?kategori={$cr_kategori}&univ={$cr_univ}";

        $total  = $this->mesin_model->where($where)->count_all();

        $offset = $this->input->get('per_page');

        $limit  = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);     

        $data   = $this->mesin_model
                        ->select("`mesin_cetak`.`id_mesin`,
                                    `kategori`.`nmkategori`,
                                    `mesin_cetak`.`nm_mesin`,
                                    `mesin_cetak`.`alias`")
                        ->join("kategori","mesin_cetak.idkategori = kategori.idkategori","inner")
                        ->order_by('`kategori`.`nmkategori`')
                        ->where($where)
                        ->find_all();


        $data_kategori  = $this->kategori_model->where("deleted",0)
                                                ->order_by("nmkategori","ASC")
                                                ->find_all(); 


        $assets =   [
                        'plugins/select2/dist/js/select2.js', 
                        'plugins/select2/dist/css/select2.css',
                        'mesin_cetak/assets/js/mesin_index.js'
                    ];
        
        add_assets($assets);

        $this->template->set('cr_kategori',$cr_kategori);
        $this->template->set('cr_univ',$cr_univ);

        $this->template->set('data',$data);
        $this->template->set('data_kategori', $data_kategori);

        $this->template->set("toolbar_title", lang('title_index'));
        $this->template->title(lang('title_index'));
        
        $this->template->render('mesin_index'); 
    }

    public function delete_mesin($id_mesin){

        $this->db->trans_start();

        $sql = "";

        // start delete mesin cetak
        $this->mesin_model->delete($id_mesin);
        $sql = $this->db->last_query();
        // end delete mesin cetak

        // start delete bahan baku cetak
        $del_where  = ['id_mesin' => $id_mesin];
        $this->bahan_baku_model->delete_where($del_where);
        $sql .= "\n\n".$this->db->last_query();
        // end delete bahan baku cetak


        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            $return         = false;
            $keterangan     = "Gagal, delete data mesin";
            $total          = 0;
            $status         = 0;

        }else{

            $return         = true;
            $keterangan     = "Sukses, delete data mesin";
            $total          = 0;
            $status         = 1;

        }
        
        $nm_hak_akses   = $this->deletePermission; 
        $kode_universal = $id_mesin;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $total, $sql, $status);

        return $return;


    }

    public function create(){

        $this->auth->restrict($this->addPermission);

        if(isset($_POST['simpan'])){

            $simpan_data    = $this->simpan();
            
            if($simpan_data){

                $this->session->unset_userdata('sess_mesin_cetak');
                $this->template->set_message(lang('msg-simpan_sukses'),'success');
                redirect('mesin_cetak');

            }

        }


        $data = $this->session->userdata('sess_mesin_cetak');
        if(!$data){

            $this->set_session(0,null);
            $data = $this->session->userdata('sess_mesin_cetak');

        }

        $data_kategori  = $this->kategori_model->where("deleted",0)
                                                ->order_by("nmkategori","ASC")
                                                ->find_all();

        $assets     =   [
                            'plugins/select2/dist/js/select2.js', 
                            'plugins/select2/dist/css/select2.css',
                            'plugins/number/jquery.number.js',
                            'mesin_cetak/assets/js/mesin_form.js'
                        ];

        add_assets($assets);

        $this->template->set('data',$data);
        $this->template->set('data_kategori', $data_kategori);
        $this->template->set("toolbar_title", lang('title_new'));
        $this->template->title(lang('title_new'));
        $this->template->page_icon('fa fa-gears');

        $this->template->render('mesin_form'); 

    }


    public function view($id = ""){

        if(isset($_POST['simpan']) && has_permission($this->managePermission)){

            $simpan = $this->simpan_edit($id);

            if($simpan){

                $this->template->set_message(lang('msg-edit_sukses'),'success');
                redirect('mesin_cetak');

            }

        }

        $this->set_session(2,$id);
        $data = $this->session->userdata('sess_mesin_cetak');


        $data_kategori  = $this->kategori_model->where("deleted",0)
                                                ->order_by("nmkategori","ASC")
                                                ->find_all();

        $assets     =   [
                            'plugins/select2/dist/js/select2.js', 
                            'plugins/select2/dist/css/select2.css',
                            'plugins/number/jquery.number.js',
                            'mesin_cetak/assets/js/mesin_form.js'

                        ];

        add_assets($assets);

        $this->template->set('data',$data);
        $this->template->set('data_kategori', $data_kategori);
        $this->template->set("toolbar_title", lang('title_new'));
        $this->template->title(lang('title_new'));
        $this->template->page_icon('fa fa-gears');

        $this->template->render('mesin_form');         

    }

    public function cek_pil_dropdown($val){

        return $val == "" ? false : true;

    }

    public function simpan_edit($id_mesin){

        $nm_mesin       = $this->input->post('nm_mesin');
        $metode         = $this->input->post('metode_rekap');
        $ket            = $this->input->post('ket');

        // Ambil data kategorinya
        $dt             = $this->mesin_model->select('idkategori')->find($id_mesin);

        $kategori       = $dt->idkategori;

        if($kategori == 1){

            $id_bb      = str_replace(",","",$this->input->post('id_bb'));
            $hpp_bb     = str_replace(",","",$this->input->post('hpp_bb'));

        }

        $sql = "";

        $this->db->trans_start();

        // start simpan data mesin
        $dt_mesin   =   [
                            'nm_mesin'          => $nm_mesin, 
                            'st_rekap_cetak'    => $metode, 
                            'ket'               => $ket
                        ];

        $this->mesin_model->update($id_mesin, $dt_mesin);
        $sql    = $this->db->last_query();

        // end simpan data mesin

        // start simpan data HPP manual BB cetak

        if($kategori == 1){

            $dt_bb = [];

            foreach ($id_bb as $key => $bb) {
                
                $dt_bb[]    =   [
                                    'idbarang_bb'   => $bb, 
                                    'hpp_manual'    => $hpp_bb[$key]
                                ];

            }

            $this->bahan_baku_model->update_batch($dt_bb,'idbarang_bb');
            $sql    .= "\n\n".$this->db->last_query();
        }

        // end simpan data HPP manual BB cetak


        $this->db->trans_complete();        

        if($this->db->trans_status() == false){

            $return         = false;
            $keterangan     = "Gagal, edit data mesin";
            $total          = 0;
            $status         = 0;

        }else{

            $return         = true;
            $keterangan     = "Sukses, edit data mesin";
            $total          = 0;
            $status         = 1;

        }
        
        $nm_hak_akses   = $this->managePermission; 
        $kode_universal = $id_mesin;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $total, $sql, $status);

        return $return;


    }

    public function simpan(){

        $id_kategori    = $this->input->post('id_kategori');
        $nm_mesin       = $this->input->post('nm_mesin');
        $alias          = $this->input->post('alias');
        $metode         = $this->input->post('metode_rekap');
        $ket            = $this->input->post('ket');
        
        $this->form_validation->set_rules("id_kategori","lang:frm-id_kategori","callback_cek_pil_dropdown");
        $this->form_validation->set_rules("nm_mesin","lang:frm-nm_mesin","required|trim");
        $this->form_validation->set_rules("alias","lang:frm-alias","required|trim");

        if($this->form_validation->run() === false){

            $this->set_session(1,null);
            return false;

        }

        $sql = "";

        $this->db->trans_start();

        // Start simpan data mesin

        $id_primary     = gen_primary('','mesin_cetak','id_mesin');

        $isi_data       =   [
                                'id_mesin'          => $id_primary, 
                                'idkategori'        => $id_kategori, 
                                'nm_mesin'          => $nm_mesin, 
                                'alias'             => $alias, 
                                'st_rekap_cetak'    => $metode, 
                                'ket'               => $ket
                            ];

        $this->mesin_model->insert($isi_data);
        $sql .= $this->db->last_query();

        // end simpan data mesin

        // start create data bahan baku cetaknya, jika kategorinya A3

        if($id_kategori == 1){

            $isi_bb_cetak = [];

            for ($i=0; $i <= 1 ; $i++) { 
                
                $nm_bb_cetak    = $i == 0 ? 'Cetak BW-' : 'Cetak CMYK-';
                $nm_bb_cetak    .= $alias;

                $isi_bb_cetak[] =   [
                                        'idbarang_bb'           => gen_primary('','barang','idbarang_bb'), 
                                        'st_cetak_a3'           => 1, 
                                        'st_warna'              => $i, 
                                        'st_fix'                => 1, 
                                        'id_mesin'              => $id_primary, 
                                        'st_pakai_stok'         => 0, 
                                        'idkategori'            => 1, 
                                        'idjenis_bb'            => 1, 
                                        'idmerk_bb'             => 1, 
                                        'nm_barang'             => $nm_bb_cetak, 
                                        'id_satuan_terkecil'    => 1, 
                                        'ket_barang'            => '', 
                                        'st_hpp_manual'         => 1, 
                                        'hpp_manual'            => 0
                                    ];  

            } 

            $this->bahan_baku_model->insert_batch($isi_bb_cetak);
            $sql    .= "\n\n".$this->db->last_query();

        }

        // end create data bahan baku cetaknya

        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            $return         = false;
            $keterangan     = "Gagal, simpan data mesin";
            $total          = 0;
            $status         = 0;

        }else{

            $return         = true;
            $keterangan     = "Sukses, simpan data mesin";
            $total          = 0;
            $status         = 1;

        }
        
        $nm_hak_akses   = $this->addPermission; 
        $kode_universal = $id_primary;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $total, $sql, $status);

        return $return;

    }



    public function set_session($st_tampil = 0, $id = null){

        /*
            Keterangan
                $st_tampil  =   0 : Baru / data baru.
                                1 : Error / repopulate data form.
                                2 : View / Edit.

                $id_primary =   Diisi jika edit / view  
        */

        if($st_tampil == 0){

            $data_header    =   [
                                    'st_view'       => 0,
                                    'id_kategori'   => '',
                                    'nm_mesin'      => '',
                                    'alias'         => '',
                                    'metode'        => '',
                                    'ket'           => ''
                                ];


        }elseif($st_tampil == 1){

            $id_kategori    = $this->input->post('id_kategori');
            $nm_mesin       = $this->input->post('nm_mesin');
            $alias          = $this->input->post('alias');
            $metode         = $this->input->post('metode_rekap');
            $ket            = $this->input->post('ket');

            $data_header    =   [
                                    'st_view'       => 0,
                                    'id_kategori'   => $id_kategori,
                                    'nm_mesin'      => $nm_mesin,
                                    'alias'         => $alias,
                                    'metode'        => $id_kategori == 1 ? $metode : 0,
                                    'ket'           => $ket
                                ];            


        }else{

            $data   = $this->mesin_model
                            ->select("`mesin_cetak`.`id_mesin`,
                                        `mesin_cetak`.`idkategori`,
                                        `mesin_cetak`.`nm_mesin`,
                                        `mesin_cetak`.`alias`,
                                        `mesin_cetak`.`st_rekap_cetak`,
                                        `mesin_cetak`.`ket`,
                                        `barang`.`idbarang_bb`,
                                        `barang`.`nm_barang`,
                                        `barang`.`hpp_manual`")
                            ->join(' barang','mesin_cetak.id_mesin = barang.id_mesin','left')
                            ->where("mesin_cetak.deleted = 0
                                        AND `mesin_cetak`.`id_mesin` = '{$id}'")
                            ->find_all();
            
            // data bahan baku cetaknya
            $bb_cetak   = [];
            if($data[0]->idkategori == 1){
                
                foreach ($data as $key => $isi) {
                    
                    $bb_cetak[]     =   [
                                            'id_bb'     => $isi->idbarang_bb,
                                            'nm_bb'     => $isi->nm_barang,
                                            'hpp_bb'    => $isi->hpp_manual
                                        ];

                }
            }

            // data header
            $data_header    =   [
                                    'st_view'       => 1,
                                    'id_kategori'   => $data[0]->idkategori,
                                    'nm_mesin'      => $data[0]->nm_mesin,
                                    'alias'         => $data[0]->alias,
                                    'metode'        => $data[0]->idkategori == 1 ? $data[0]->st_rekap_cetak : 0,
                                    'ket'           => $data[0]->ket,
                                    'dt_bb'         => $bb_cetak
                                ];                        

        }

        $this->session->set_userdata('sess_mesin_cetak',$data_header);

    }
    
    public function batal_kembali(){

        $this->session->unset_userdata('sess_mesin_cetak');
        redirect('mesin_cetak');

    }

   
}
?>