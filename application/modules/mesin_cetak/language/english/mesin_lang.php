<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ..:: Judul ::..

$lang['title_index']					= 'Data Mesin Cetak';
$lang['title_new']						= 'Input Mesin Cetak Baru';
$lang['title_edit']						= 'Edit Data Mesin Cetak';

$lang['btn_new']						= 'Baru';
$lang['btn_delete']						= 'Hapus';
$lang['btn_save']						= 'Simpan';
$lang['btn_cancel']						= 'batal / kembali';
$lang['bf_or']							= 'or';

// ..:: Index ::..

$lang['index-tbl-kategori']				= 'Kategori';
$lang['index-tbl-nm_mesin']				= 'Nama Mesin';
$lang['index-tbl-alias']				= 'Alias';

// ..:: Form ::..
$lang['frm-id_kategori']				= 'Kategori';
$lang['frm-nm_mesin']					= 'Nama Mesin';
$lang['frm-alias']						= 'Alias';
$lang['frm-ket_alias']					= 'Text yang akan ditambahkan pada nama Bahan Baku Cetak';
$lang['frm-metode_rekap']				= 'Metode Rekap';

	$lang['frm-metode_rekap1']			= 'Extract file *.txt';
	$lang['frm-metode_rekap2']			= 'Rekap counter manual';

$lang['frm-ket']						= 'Keterangan';

$lang['frm-table-nm_bb']				= 'Nama Bahan Baku';
$lang['frm-table-hpp_bb']				= 'HPP Manual Bahan Baku';



// ..:: Label Informasi Success / Error ::..

$lang['msg-delete_fail']				= 'Hapus data mesin cetak gagal';
$lang['msg-delete_sukses']				= 'Hapus data mesin cetak sukses';
$lang['msg-edit_sukses']				= 'Edit data mesin cetak sukses';
$lang['msg-simpan_sukses']				= 'Simpan data mesin cetak sukses';
$lang['label-data-tidak-ada']			= 'Data tidak ditemukan.';


