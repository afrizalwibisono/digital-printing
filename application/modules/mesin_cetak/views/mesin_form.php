<?php 
		$enable = '';
		if($data['st_view'] == 1){

			$enable	= 'disabled';

		}
?>


<?= form_open($this->uri->uri_string(),[
											'name' 	=> 'form_baru',
											'id'	=> 'form_baru',
											'role'	=> 'form',
											'class'	=> 'form-horizontal'
										]) ?>
<div class="box box-primary">
	
	<input type="hidden" name="st_view" id="st_view" value="<?= $data['st_view'] ?>">

	<div class="box-body">
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for='id_kategori'><?= lang('frm-id_kategori') ?></label>
			<div class="col-sm-2">
				<select name="id_kategori" id="id_kategori" class="form-control" <?= $enable ?> >
					<option></option>
					<?php 
						if(isset($data_kategori) && is_array($data_kategori) && count($data_kategori)) :
							foreach ($data_kategori as $key => $isi) :
					?>

						<option value="<?= $isi->idkategori ?>" 
								<?= set_select('id_kategori',$isi->idkategori,isset($data['id_kategori']) && $data['id_kategori'] == $isi->idkategori) ?> > 
								<?= strtoupper($isi->nmkategori) ?> 
						</option>

					<?php
							endforeach;
						endif;
					?>

				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-2" for='nm_mesin'><?= lang('frm-nm_mesin') ?></label>
			<div class="col-sm-4">
				<input type="text" name="nm_mesin" id='nm_mesin' class="form-control" value="<?= set_value('nm_mesin', isset($data['nm_mesin']) ? $data['nm_mesin'] : '' )?>" >
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-2" for='alias'><?= lang('frm-alias') ?></label>
			
			<div class="col-sm-4">
				<input type="text" name="alias" id='alias' class="form-control" value="<?= set_value('alias', isset($data['alias']) ? $data['alias'] : '' )?>" <?= $enable ?> >
				<span class="small text-primary"><?= lang('frm-ket_alias') ?></span>
			</div>
		</div>

		<div class="form-group" id="area-metode-rekap-a3" style="display: none">
			<label class="control-label col-sm-2" for='metode_rekap'><?= lang('frm-metode_rekap') ?></label>
			<div class="col-sm-4">
				<select name="metode_rekap" id="metode_rekap" class="form-control" style="width: 100%">
					<option value="0" <?= set_select('metode_rekap','0',isset($data['metode']) && $data['metode'] == 0) ?> ><?= lang('frm-metode_rekap2') ?></option>
					<option value="1" <?= set_select('metode_rekap','1',isset($data['metode']) && $data['metode'] == 1) ?> ><?= lang('frm-metode_rekap1') ?></option>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-2" for='ket'><?= lang('frm-ket') ?></label>
			<div class="col-sm-4">
				<textarea class="form-control" name="ket" id="ket"><?= set_value('ket',isset($data['ket']) ? $data['ket'] : '') ?></textarea>
			</div>
		</div>

		<?php 
		
			if($data['st_view'] == 1 && $data['id_kategori'] == 1) : 
		
		?>

		<div class="table-responsive">
			
			<table class="table table-bordered" id="dft_hpp_bb">
				<thead>
					<tr class="success">
						<th width="30">#</th>
						<th class="text-center"><?= lang('frm-table-nm_bb') ?></th>
						<th class="text-center" width="200"><?= lang('frm-table-hpp_bb') ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($data['dt_bb'] as $key => $bb) :	?>

						<tr>
							<td class="text-center">
								<?= ($key+1) ?>
								<input type="hidden" name="id_bb[]" id='id_bb' value="<?= $bb['id_bb'] ?>">
							</td>
							<td class="text-center"><?= $bb['nm_bb'] ?></td>
							<td class="text-center">
								<input type="text" name="hpp_bb[]" id="hpp_bb" class="form-control input-sm" value="<?= $bb['hpp_bb'] ?>">
							</td>
						</tr>

					<?php endforeach; ?>
				</tbody>
			</table>

		</div>

		<?php 
				endif; 
		?>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-4">
				<button type="submit" class="btn btn-success" name="simpan" id="simpan" title="<?= lang('btn_save') ?>" ><?= lang('btn_save') ?></button>
				<?php echo lang('bf_or').' '.anchor('mesin_cetak/batal_kembali',lang('btn_cancel')) ?>
			</div>
		</div>

	</div>
	
</div>
<?= form_close() ?>