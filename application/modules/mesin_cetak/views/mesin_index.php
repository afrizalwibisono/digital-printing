<?php
	$ENABLE_ADD			= has_permission('Mesin Cetak.Add');
	$ENABLE_DELETE		= has_permission('Mesin Cetak.Delete');
?>

<?= form_open($this->uri->uri_string(),	[
											'name'		=> 'frm_index',
											'id'		=> 'frm_index',
											'role'		=> 'form',
											'class'		=> 'form-inline'

										]) ?>

<div class="box box-primary">

	<input type="hidden" name="id_delete" id="id_delete"><!-- nilai harus kosong saat devault -->

	<div class="box-header">
		
		<?php if($ENABLE_ADD) : ?>
		<a href="<?= site_url('mesin_cetak/create') ?>" class="btn btn-success" title="<?= lang('btn_new') ?>" ><?= lang('btn_new') ?></a>
		<?php endif ?>

		<div class="pull-right">
			
			<div class="form-group">
				<select class="form-control" name="kategori" id="kategori" style="min-width: 200px">
					<option></option>
					<?php

						if(isset($data_kategori) && is_array($data_kategori) && count($data_kategori)) :

							foreach ($data_kategori as $key => $isi) :
								
					?>

						<option value="<?= $isi->idkategori ?>" 
							<?= set_select('id_kategori',$isi->idkategori,isset($cr_kategori) && $cr_kategori == $isi->idkategori ) ?> > 
							<?= strtoupper($isi->nmkategori) ?> 
						</option>

					<?php
							endforeach;
						endif;

					?>
				</select>
			</div>

			<div class="form-group">
				<div class="input-group">
					<input type="text" name="univ" id="univ" class="form-control" value="<?= set_value('univ',isset($cr_univ) ? $cr_univ : '') ?>">
					<div class="input-group-btn">
						<button type="submit" name="cari" id="cari" class="btn btn-primary">
							<i class="fa fa-search"></i>
						</button>
					</div>
				</div>
			</div>

		</div>

		

	</div>

	<?php
			if(isset($data) && is_array($data) && count($data)) :
	?>

	<div class="box-body table-responsive">

		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th width="30">#</th>
					<th class="text-center"><?= lang('index-tbl-kategori') ?></th>
					<th class="text-center"><?= lang('index-tbl-nm_mesin') ?></th>
					<th class="text-center"><?= lang('index-tbl-alias') ?></th>
					<th width="70"></th>
				</tr>
			</thead>
			<tbody>
				<?php 

					foreach ($data as $key => $isi) :

				?>

				<tr>
					<td>
						<input type="hidden" name="id_mesin[]" id="id_mesin" value="<?= $isi->id_mesin ?>">
					</td>
					<td class="text-center"><?= strtoupper($isi->nmkategori) ?></td>
					<td class="text-center"><?= strtoupper($isi->nm_mesin) ?></td>
					<td class="text-center"><?= strtoupper($isi->alias) ?></td>
					<td class="text-center">
						<a href="<?= site_url('mesin_cetak/view/'.$isi->id_mesin) ?>" class="text-black" title="Lihat data"><i class="fa fa-folder"></i></a>
						<?php if($ENABLE_DELETE) : ?>
						|
						<a href="#del_msn" class="text-red" title="Hapus data" onclick="konfirm_delete(this);" ><i class="fa fa-trash"></i></a>
						<?php endif; ?>
					</td>
				</tr>

				<?php

					endforeach;

				?>
			</tbody>

		</table>
		
	</div>
	<div class="box-footer clearfix">
		<?= $this->pagination->create_links();  ?>
	</div>

	<?php 
			else :
	?>

		<div class="alert alert-info" role="alert">
        	<p><i class="fa fa-warning"></i> &nbsp; <?= lang('label-data-tidak-ada') ?></p>
    	</div>

    <?php
    		endif;
    ?>

</div>