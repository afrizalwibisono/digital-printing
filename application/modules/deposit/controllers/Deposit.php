<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for Deposit
 */

class Deposit extends Admin_Controller {

    protected $uploadFolder     = "./upload/deposit";
    protected $viewPermission   = "Deposit.View";
    protected $addPermission    = "Deposit.Add";
    
    public function __construct()
    {
        parent::__construct();

        $this->lang->load('deposit/deposit');

        $this->load->model([
                        'deposit/deposit_model', 
                        'deposit/konsumen_model'
                        ]);

        $this->template->title(lang('dp_title'));
        $this->template->page_icon('fa fa-th-large');
    }

    public function index(){
        $this->auth->restrict($this->viewPermission);

        $search = $this->input->post('table_search') ? $this->input->post('table_search') : $this->input->get('search');
        $idc    = $this->input->post('idkonsumen') ? $this->input->post('idkonsumen') : $this->input->get('idc');

        $filter = "?search=".urlencode($search);
        $search2 = $this->db->escape_str($search);

        $where = "deposit.deleted = 0 AND nama like '%$search2%'";
        if($idc){
            $filter .= "&idc=".urlencode($idc);
            $where  .= " AND deposit.idkonsumen='".$idc."'";
        }

        $total = $this->konsumen_model
                    ->join("(SELECT 
                                    deposit.idkonsumen, deposit.saldo, deposit.deleted
                                FROM
                                    deposit
                                    join
                                    (SELECT 
                                    MAX(id_deposit) as id_deposit, idkonsumen
                                    FROM
                                        deposit
                                    where deposit.deleted = 0
                                    GROUP BY idkonsumen) as tsaldo on deposit.id_deposit=tsaldo.id_deposit
                                WHERE
                                    deleted = 0) AS deposit", "konsumen.idkonsumen=deposit.idkonsumen")
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;
        // Pagination
        $this->load->library('pagination');

        $this->pagination->initialize($this->pager);

        $data = $this->konsumen_model
                    ->select("deposit.*,
                                konsumen.panggilan,
                                konsumen.nama,
                                konsumen.st, 
                                IF(`konsumen`.`st` = 0,
                                'Konsumen',
                                IF(`konsumen`.`st` = 1,
                                    'Reseller',
                                    'Instansi')) AS st_konsumen,
                                deposit.saldo
                                ")
                    ->join("(SELECT 
                                    deposit.idkonsumen, deposit.saldo, deposit.deleted
                                FROM
                                    deposit
                                    join
                                    (SELECT 
                                    MAX(id_deposit) as id_deposit, idkonsumen
                                    FROM
                                        deposit
                                    where deposit.deleted = 0
                                    GROUP BY idkonsumen) as tsaldo on deposit.id_deposit=tsaldo.id_deposit
                                WHERE
                                    deleted = 0) AS deposit", "konsumen.idkonsumen=deposit.idkonsumen")
                    ->where($where)
                    ->order_by(['nama' => 'ASC'])
                    ->limit($limit, $offset)
                    ->find_all();

        $dt_konsumen    = $this->konsumen_model
                                ->select("`idkonsumen` as id, 
                                    CONCAT(IF(panggilan IS NULL, '', panggilan),
                                    ' ',
                                    `nama`,
                                    ' | ',
                                    IF(st = 0,
                                        'konsumen',
                                        IF(st = 1, 'reseller', 'instansi'))) AS nm")
                                ->where("deleted", 0)
                                ->order_by("nama","asc")
                                ->find_all();

        $assets = array('plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'deposit/assets/js/deposit_index.js'
                        );

        add_assets($assets);

        $this->template->set("data", $data);
        $this->template->set("dt_konsumen", $dt_konsumen);
        $this->template->set("idkonsumen", $idc);
        $this->template->set("search", $search);

        $this->template->title(lang('dp_title'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index');
    }

    public function create(){
        $this->auth->restrict($this->addPermission);

        if(isset($_POST['save'])){
            if($this->save_deposit()){
              $this->template->set_message(lang("dp_create_success"), 'success');
              redirect('deposit');
            }
        }

        $dt_konsumen    = $this->konsumen_model
                                ->select("`idkonsumen` as id, 
                                    CONCAT(IF(panggilan IS NULL, '', panggilan),
                                    ' ',
                                    `nama`,
                                    ' | ',
                                    IF(st = 0,
                                        'konsumen',
                                        IF(st = 1, 'reseller', 'instansi'))) AS nm")
                                ->where("deleted", 0)
                                ->order_by("nama","asc")
                                ->find_all();

        $assets = array('plugins/datetimepicker/bootstrap-datetimepicker.css',
                        'plugins/datetimepicker/bootstrap-datetimepicker.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'plugins/number/jquery.number.js',
                        'deposit/assets/js/deposit_create.js'
                        );

        add_assets($assets);

        $this->template->set("dt_konsumen", $dt_konsumen);

        $this->template->title(lang('dp_title_new'));
        $this->template->render('form');
    }

    public function penarikan($id_konsumen = ''){
        if(!$id_konsumen){
            redirect('deposit');
        }

        if(isset($_POST['save'])){
            if($this->save_penarikan($id_konsumen)){
                $this->template->set_message(lang("dp_tarik_success"), 'success');
                redirect('deposit');
            }
        }

        $dt_konsumen    = $this->konsumen_model
                                ->select("`idkonsumen` as id, 
                                    CONCAT(IF(panggilan IS NULL, '', panggilan),
                                    ' ',
                                    `nama`,
                                    ' | ',
                                    IF(st = 0,
                                        'konsumen',
                                        IF(st = 1, 'reseller', 'instansi'))) AS nm")
                                ->find($id_konsumen);
        if(!$dt_konsumen){
            $this->template->set_message(lang("dp_invalid_id"), 'error');
            redirect('deposit');
        }

        $this->auth->restrict($this->addPermission);

        $assets = array('plugins/datetimepicker/bootstrap-datetimepicker.css',
                        'plugins/datetimepicker/bootstrap-datetimepicker.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'plugins/number/jquery.number.js',
                        'deposit/assets/js/deposit_penarikan.js'
                        );

        add_assets($assets);

        $saldo = get_deposit($id_konsumen);

        $this->template->set("dt_konsumen", $dt_konsumen);
        $this->template->set("saldo", $saldo);

        $this->template->title(lang('dp_title_penarikan'));
        $this->template->render('penarikan');
    }

    public function detail($id_konsumen = '', $xlimit = 0, $pdf = 0){
        $this->auth->restrict($this->viewPermission);

        if(!$id_konsumen){
            $this->template->set_message(lang('dp_invalid_id'), 'error');
            redirect('deposit');
        }

        $limit = $this->input->get('tampilkan');
        if(!$limit){
            $limit = 10;
        }

        if($limit > 1000){
            $limit = 1000;
        }

        if($xlimit){
            $limit = $xlimit;
        }

        $konsumen = $this->konsumen_model->select("idkonsumen, panggilan, nama, alamat, email, kota")->find($id_konsumen);
        $data = $this->deposit_model
                            ->order_by('id_deposit', 'DESC')
                            ->limit($limit)
                            ->find_all_by(['deleted' => 0, 'idkonsumen' => $id_konsumen]);
        //Identitas
        $identitas = $this->identitas_model->find(1);

        $assets = array('deposit/assets/js/deposit_detail.js');

        add_assets($assets);

        $this->template->set("konsumen", $konsumen);
        $this->template->set("data", $data);
        $this->template->set("idt", $identitas);
        $this->template->set("limit", $limit);

        $this->template->title(lang('dp_title_view'));

        if($pdf){
            $this->load->library('pdfmaker');

            $this->template->set_layout('pdf');
            $html = $this->template->render('pdf', [], true);

            $pdf = $this->pdfmaker->generate($html,'deposit', false);
            return $pdf;
        }else{
            $this->template->render('detail');
        }
    }

    /**
     * Simpan transaksi deposit
     * @param  string  $id_konsumen       Id primary key Konsumen
     * @param  integer $st_in             1 = In, 0 = Out
     * @param  integer $nilai             nilai deposit yang masuk/keluar
     * @param  string  $ket               Keterangan tambahan
     * @param  integer $st_valid          0 = Tidak valid, 1 = Valid, 2 = Menunggu pengecekan ke bank (jika metode bayar == 2 [transfer]), hanya diisi jika st_in == 1
     * @param  datetime  $waktu           Waktu Transaksi, jika dikosongi maka akan dianggap waktu sekarang
     * @param  integer $metode_bayar      1 = Cash, 2 = Transfer
     * @param  string  $rekening_pengirim nomor rekening pengirim, diisi jika metode bayar == 2 [transfer]
     * @param  array  $input_file_resi    isi dengan name file input upload resi
     * @return boolean                     true jika sukses, false jika gagal
     */
    public function _simpan_deposit($id_konsumen = '', $st_in = null, $nilai = 0, $ket = '', $st_valid = 1, $waktu = null, $metode_bayar = 1, $rekening_pengirim = '', $input_file_resi = null){
        if(!$id_konsumen || is_null($st_in) || $st_in === '' || doubleval($nilai) <= 0){
            return false;
        }
        
        if(!$waktu){
            $waktu = date('Y-m-d H:i:s');
        }

        //validate konsumen
        $cek_konsumen = $this->konsumen_model->find($id_konsumen);
        if(!$cek_konsumen){
            return false;
        }

        if($st_in == 1){ // Deposit Masuk
            // Jika transfer maka wajib $rekening_pengirim dan resi
            if($metode_bayar == 2 && (!$rekening_pengirim || !$input_file_resi)){
                return false;
            }
            // Get Last Saldo
            $last_saldo = $this->_get_last_saldo($id_konsumen);
            if(!$last_saldo){
                $last_saldo = 0;
            }

            $data = [
                'idkonsumen' => $id_konsumen,
                'st_in' => $st_in,
                'waktu' => $waktu,
                'metode_bayar' => $metode_bayar,
                'rekening_pengirim' => $rekening_pengirim,
                'nilai' => doubleval($nilai),
                'saldo' => $last_saldo + doubleval($nilai),
                'ket' => $ket,
                'st_valid' => $st_valid
                ];

            $id_deposit = $this->deposit_model->insert($data);
            if($id_deposit && $_FILES[$input_file_resi]['name'] != ''){
                if (!is_dir($this->uploadFolder)){
                    mkdir($this->uploadFolder, 0777, TRUE);
                }
                // Upload Resi
                $this->load->library('upload');
                $config['upload_path']   = $this->uploadFolder;
                $config['allowed_types'] = 'jpeg|jpg|png';
                $config['max_size']      = '0'; // Max size unlimited, tapi akan diresize dibawah
                $config['file_ext_tolower']  = TRUE;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload($input_file_resi)){
                    return FALSE;               
                }else{
                    $upload_data = $this->upload->data();
                    $file_name   = $upload_data['file_name'];
                    //update deposit
                    $this->deposit_model->update($id_deposit, ['file_resi' => $file_name]);

                    //Resize Photo
                    $this->load->library('image_lib');
                    $image_config = array(
                        'image_library' => 'imagemagick',
                        'library_path' => '/usr/bin',
                        'source_image' => $upload_data['full_path'],
                        'maintain_ratio' => TRUE,
                        'width' => 720,
                    );

                    $this->image_lib->initialize($image_config);
                    $this->image_lib->resize();
                    $this->image_lib->clear();
                }
            }

            return $id_deposit ? true : false;
        }else{ // Deposit keluar
            $saldo = $this->_get_last_saldo($id_konsumen);
            if(!$saldo){
                $saldo = 0;
            }

            if($saldo == 0 || $saldo < $nilai){
                return false;
            }

            $nilai = doubleval($nilai);
            $saldo_akhir = $saldo - $nilai;

            // Insert Deposit yang dipakai
            $dt_insert  = [
                            'idkonsumen' => $id_konsumen,
                            'st_in' => $st_in,
                            'waktu' => $waktu,
                            'nilai' => $nilai,
                            'saldo' => $saldo_akhir,
                            'ket' => $ket,
                            'st_valid' => 1
                            ];
            $insert = $this->deposit_model->insert($dt_insert);

            return $insert ? true : false;
        }
    }

    public function _get_saldo_deposit($id_konsumen = ''){
        if(!$id_konsumen){
            return false;
        }

        $saldo = $this->_get_last_saldo($id_konsumen);

        return $saldo;
    }

    private function _get_last_saldo($id_konsumen = ''){
        if(!$id_konsumen){
            return false;
        }

        $dp_data = $this->deposit_model->select('saldo')
                                        ->order_by('id_deposit', 'DESC')
                                        ->limit(1)
                                        ->find_by(['idkonsumen' => $id_konsumen, 'st_valid' => 1, 'deleted' => 0]);
        if($dp_data){
            return doubleval($dp_data->saldo);
        }

        return false;
    }

    private function save_deposit(){
        $idkonsumen = $this->input->post('idkonsumen');
        $nilai      = $this->input->post('nilai');
        $_POST['nilai'] = remove_comma($nilai); 
        $nilai          = $_POST['nilai'];
        $ket        = $this->input->post('ket');
        $waktu      = $this->input->post('waktu');
        $metode_bayar = $this->input->post('metode_bayar');
        $rekening_pengirim = $this->input->post('rekening_pengirim');
        $file_resi    = $this->input->post('file_resi');

        $this->form_validation->set_rules('idkonsumen','lang:dp_konsumen','callback_default_select');
        $this->form_validation->set_rules('nilai','lang:dp_nilai','required|greater_than[0]');
        $this->form_validation->set_rules('ket','lang:dp_ket','trim');
        $this->form_validation->set_rules('waktu','lang:dp_tanggal','trim|required');
        $this->form_validation->set_rules('metode_bayar','lang:dp_metode_bayar','callback_default_select');
        if($metode_bayar == 2){
            $this->form_validation->set_rules('rekening_pengirim','lang:dp_no_rek','trim|required');
            if (empty($_FILES['file_resi']['name'])){
                $this->form_validation->set_rules('file_resi','lang:dp_upload_resi','required');
            }
        }

        $this->form_validation->set_message('default_select', 'You should choose the "%s" field');

        if ($this->form_validation->run() === FALSE) {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        $waktu = date_ymd($waktu);

        $result = set_deposit($idkonsumen, $nilai, $ket, 1, $waktu, $metode_bayar, $rekening_pengirim, 'file_resi');
        if($result){
            // Send Email
            $this->send_email_saldo($idkonsumen, 'New Deposit', ['nominal' => $nilai, 'tanggal' => $waktu], 1);
        }

        return $result;
    }

    private function save_penarikan($id_konsumen = ''){
        $nilai      = $this->input->post('nilai');
        $_POST['nilai'] = remove_comma($nilai); 
        $nilai          = $_POST['nilai'];
        $ket        = $this->input->post('ket');

        $saldo = get_deposit($id_konsumen);
        if(!$saldo){
            $saldo = 0;
        }

        $this->form_validation->set_rules('nilai','lang:dp_nilai','required|less_than_equal_to['.$saldo.']');
        $this->form_validation->set_rules('ket','lang:dp_ket','trim|required');

        if ($this->form_validation->run() === FALSE) {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        $result = use_deposit($id_konsumen, $nilai, $ket);
        if($result){
            // Send Email
            $this->send_email_saldo($id_konsumen, 'Penarikan Deposit', ['nominal' => $nilai, 'tanggal' => date('Y-m-d H:i:s')], 2);
        }

        return $result;
    }

    public function default_select($val)
    {
        return $val == "" ? FALSE : TRUE;
    }

    public function cek_konsumen_email(){
        if(!$this->input->is_ajax_request()){
            $this->template->set_message(lang('dp_only_ajax'), 'error');
            redirect('deposit');
        }

        $idkonsumen = $this->input->post('idkonsumen');
        $cek = $this->konsumen_model->find($idkonsumen);
        if(!$cek){
            $return = ['type' => 'error', 'msg' => lang('dp_no_konsumen')];
        }

        if(!filter_var($cek->email, FILTER_VALIDATE_EMAIL)){
            $return = ['type' => 'error', 'msg' => lang('dp_email_not_valid')];
        }else{
            $return = ['type' => 'success', 'msg' => ''];
        }

        echo json_encode($return);
    }
    /**
     * $type => 1 = New Deposit, 2 = Penarikan Deposit, 3 = Rekening koran
     */
    public function send_email_saldo($idkonsumen = '', $subject = 'Email Notif', $data = ['nominal' => 0, 'tanggal' => ''], $type = 1, $xlimit = 10){
        if(!$idkonsumen){
            if(!$this->input->is_ajax_request()){
                $this->template->set_message(lang('dp_only_ajax'), 'error');
                redirect('deposit');
            }

            $subject = $this->input->post('subject');
        }

        $cek = $this->konsumen_model->find($idkonsumen);
        if(!$cek){
            $return = ['type' => 'error', 'msg' => lang('dp_no_konsumen')];
        }

        if(!filter_var($cek->email, FILTER_VALIDATE_EMAIL)){
            return false;
        }

        //Send Email to Customer
        $subject = $subject;
        $to      = $cek->email;
        $data_msg = ['full_name' => $cek->panggilan." ".$cek->nama,
                     'nominal'   => $data['nominal'],
                     'tanggal'   => $data['tanggal']
                    ];
        if($type == 1){
            $message = $this->load->view('deposit/email/new_deposit', $data_msg, TRUE);    
        }else if($type == 2){
            $message = $this->load->view('deposit/email/penarikan_deposit', $data_msg, TRUE);    
        }else{
            $message = $this->load->view('deposit/email/rekening_koran_deposit', $data_msg, TRUE); 
        }
        
        $message_body  = $this->load->view('deposit/email/_header.php', '', TRUE);
        $message_body .= $message;
        $message_body .= $this->load->view('deposit/email/_footer.php', '', TRUE);
        $pdf = $this->detail($idkonsumen, $xlimit, 1);
        //Load Email Library
        $this->load->library('mailer');
        $mail = $this->mailer->load();
        $mail->Subject = $subject;
        $mail->Body = $message_body;
        $mail->AddAddress($to);
        $mail->addStringAttachment($pdf, "Saldo Deposit " . date("d-m-Y-H-i-s") . ".pdf");
        // $mail->SMTPDebug = 2;
        $sent = $mail->Send();
        
        if($sent !== false){
            return true;
        }

        return false;
    }

    public function send_rekening_koran(){
        if(!$this->input->is_ajax_request()){
            $this->template->set_message(lang('dp_only_ajax'), 'error');
            redirect('deposit');
        }

        $idkonsumen = $this->input->post('idkonsumen');
        $limit      = $this->input->post('limit');

        $cek = $this->konsumen_model->find($idkonsumen);
        if(!$cek){
            $return = ['type' => 'error', 'msg' => lang('dp_invalid_id')];
        }

        $saldo = get_deposit($idkonsumen);
        $waktu = date('Y-m-d H:i:s');
        // Send Email
        $sent = $this->send_email_saldo($idkonsumen, 'Informasi Saldo Deposit', ['nominal' => $saldo, 'tanggal' => $waktu], 3);
        if($sent !== false){
            $return = ['type' => 'success', 'msg' => lang('dp_email_sent')];
        }else{
            $return = ['type' => 'error', 'msg' => lang('dp_email_sent_failed')];
        }

        echo json_encode($return);
    }
}
