<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['dp_title'] 		= 'Deposit';
$lang['dp_title_new'] 	= 'Deposit Baru';
$lang['dp_title_penarikan'] = 'Penarikan Deposit';
$lang['dp_title_view'] 	= 'Detail Transaksi Deposit';

// form/table
$lang['dp_konsumen'] 	= 'Konsumen';
$lang['dp_tp_konsumen'] = 'Tipe Konsumen';
$lang['dp_tanggal'] 	= 'Tanggal';
$lang['dp_metode_bayar']= 'Metode Pembayaran';
$lang['dp_cash'] 		= 'Cash';
$lang['dp_transfer'] 	= 'Transfer';
$lang['dp_no_rek'] 		= 'No. Rekening Pengirim';
$lang['dp_upload_resi'] = 'Upload Bukti Transfer';
$lang['dp_resi'] 		= 'Bukti Transfer';
$lang['dp_nilai'] 		= 'Nominal Deposit';
$lang['dp_nilai_penarikan'] = 'Nominal Penarikan';
$lang['dp_uang_masuk'] 	= 'Uang Masuk';
$lang['dp_uang_keluar'] = 'Uang Keluar';
$lang['dp_saldo'] 		= 'Saldo';
$lang['dp_saldo_akhir'] = 'Saldo Akhir';
$lang['dp_ket'] 	= 'Keterangan';
$lang['dp_status'] 	= 'Status Pembayaran';
$lang['dp_tdk_valid'] 	= 'Tidak Valid';
$lang['dp_valid'] 		= 'Valid';
$lang['dp_cek_bank'] 	= 'Menunggu Pengecekan';
$lang['dp_detail'] 	= 'Rincian Transaksi Deposit';
$lang['dp_alamat'] 	= 'Alamat';
$lang['dp_tampilkan'] = 'Tampilkan';
$lang['dp_tgl_cetak'] = 'Tgl. Cetak';
$lang['dp_detail'] 	= 'Detail %s transaksi terakhir';
$lang['dp_email'] 	= 'Email';
$lang['dp_pilih'] 	= 'Pilih';

// button
$lang['dp_btn_new'] 	= 'Baru';
$lang['dp_btn_delete'] 	= 'Hapus';
$lang['dp_btn_save'] 	= 'Simpan';
$lang['dp_btn_cancel'] 	= 'Batal';
$lang['dp_btn_back'] 	= 'Kembali';
$lang['dp_or'] 			= 'atau';
$lang['dp_close'] 		= 'Tutup';

// messages
$lang['dp_del_error']		= 'Anda belum memilih deposit yang akan dihapus.';
$lang['dp_del_failure']		= 'Tidak dapat menghapus deposit: ';
$lang['dp_delete_confirm']	= 'Apakah anda yakin akan menghapus deposit terpilih ?';
$lang['dp_cancel_confirm']	= 'Apakah anda yakin akan membatalkan transaksi Deposit ?';
$lang['dp_deleted']			= 'Data Deposit berhasil dihapus';
$lang['dp_no_records_found'] = 'Data tidak ditemukan.';
$lang['dp_no_konsumen'] 	= 'Data konsumen tidak ditemukan.';
$lang['dp_email_not_valid'] = 'Alamat email konsumen belum ada/tidak valid.
<br>Alamat email diperlukan untuk mengirimkan bukti setor deposit kepada konsumen.
<br><br>Apakah anda ingin tetap melanjutkan ?';
$lang['dp_email_sent'] = 'Email telah terkirim';
$lang['dp_email_sent_failed'] = 'Email gagal dikirim';

$lang['dp_create_failure'] 	= 'Deposit baru gagal disimpan: ';
$lang['dp_create_success'] 	= 'Deposit baru berhasil disimpan';
$lang['dp_canceled'] 		= 'Deposit telah berhasil dibatalkan';

$lang['dp_tarik_failure'] 	= 'Penarikan deposit gagal disimpan: ';
$lang['dp_tarik_success'] 	= 'Penarikan deposit berhasil disimpan';

$lang['dp_edit_success'] 	= 'Deposit berhasil disimpan';
$lang['dp_invalid_id'] 		= 'ID Tidak Valid';

$lang['dp_only_ajax'] 		= 'Hanya ajax request yang diijinkan.';

$lang['dp_date_required'] 	= 'Silahkan pilih tanggal awal dan akhir.';
