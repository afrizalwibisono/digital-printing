$(function(){
	$("#tampilkan").on("change", function(){
		$("#frm-detail")[0].submit();
	});

	$("#send-email").on("click", function(){
		$(this).prop('disabled', true);
		send_email($(this).data('id'));
	});

	$(".md-detail").on("click", function(){
		var metode_bayar = $(this).data("metode"),
			nilai = $(this).data("nilai"),
			ket = $(this).data("ket"),
			rekening = $(this).data("rekening"),
			resi = $(this).data("resi");

		$("#d-metode-bayar").val(metode_bayar);
		$("#d-nilai").val('Rp. '+nilai);
		$("#d-ket").val(ket);
		
		if(metode_bayar == "Transfer"){
			$("#d-no-rek").val(rekening);
			$("#d-resi").prop('src', resi);	

			$(".div-transfer").show();
			if(resi == "#"){
				$("#d-resi").closest(".div-transfer").hide();
			}
		}else{
			$(".div-transfer").hide();
		}
		
	});
});

function cetak_halaman(){
	window.print();
}

function send_email(idkonsumen){
	var limit = $("#tampilkan").val();
	if(!idkonsumen){
		alertify.error('Terjadi kesalahan, silahkan reload halaman ini.');
	}

	$.ajax({
		url: siteurl+'deposit/send_rekening_koran',
		type: 'post',
		data: {idkonsumen: idkonsumen, limit: limit},
		dataType: 'json',
		success: function(res){
			console.log(res);
		},
		complete: function(){
			$("#send-email").prop('disabled', false);
		}
	});
}