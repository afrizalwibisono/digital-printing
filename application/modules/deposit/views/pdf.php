<div class="box box-primary pdf">
	<?php if($idt) : ?>
    <div class="row header-laporan">
        <div class="col-md-12">
            <div class="col-md-1 laporan-logo">
                <img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo">
            </div>
            <div class="col-md-6 laporan-identitas">
                <address>
                    <h4><?= $idt->nm_perusahaan ?></h4>
                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?>
                </address>
            </div>
            <div class="text-right">
                <h4>Deposit Konsumen</h4>
            </div>
            <div class="clearfix"></div>
            <div class="laporan-garis"></div>
        </div>
    </div>
    <?php endif ?>
	<div class="box-header">
		<?= form_open($this->uri->uri_string(), ['class' => 'form-horizontal', 'id' => 'frm-detail', 'method' => 'get']) ?>
			<div class="form-group">
				<label for="nm_konsumen" class="control-label col-xs-2"><?= lang('dp_konsumen') ?></label>
				<div class="col-xs-5">
					<?= strtoupper($konsumen->panggilan." ".$konsumen->nama) ?>
				</div>
			</div>
			<div class="form-group">
				<label for="alamat" class="control-label col-xs-2"><?= lang('dp_alamat') ?></label>
				<div class="col-xs-5">
					<?= $konsumen->alamat." ".$konsumen->kota ?>
				</div>
			</div>
			<?php if($konsumen->email) : ?>
			<div class="form-group">
				<label for="email" class="control-label col-xs-2"><?= lang('dp_email') ?></label>
				<div class="col-xs-5">
					<?= $konsumen->email ?>
				</div>
			</div>
			<?php endif; ?>
			<div class="form-group">
				<label for="tgl_cetak" class="control-label col-xs-2"><?= lang('dp_tgl_cetak') ?></label>
				<div class="col-xs-5">
					<?= date('d/m/Y H:i:s') ?>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="control-label col-xs-2"><?= lang('dp_saldo_akhir') ?></label>
				<div class="col-xs-5">
					<strong>Rp. <?= isset($data) ? number_format($data[0]->saldo) : '0' ?></strong>
				</div>
			</div>
		<?= form_close(); ?>
	</div>
    <div class="box-body table-responsive laporan-body">
    	<h4><?= sprintf(lang('dp_detail'), $limit) ?></h4>
        <table class="table table-condensed table-bordered" style="table-layout: fixed;">
            <thead>
                <tr>
                    <th width="5%">#</th>
                    <th class="text-center" width="20%"> <?= lang('dp_tanggal') ?> </th>
                    <th class="text-center" width="30%" style="width: 150px;word-break:break-all;word-wrap: break-word;"> <?= lang('dp_ket') ?></th>
                    <th class='text-center' width="15%"> <?= lang('dp_uang_masuk') ?> </th>
                    <th class='text-center' width="15%"> <?= lang('dp_uang_keluar') ?> </th>
                    <th class='text-center' width="15%"> <?= lang('dp_saldo') ?> </th>
                </tr>    
            </thead>            
            <tbody>
                <?php 
                    $num    = 0;
                    foreach ($data as $key => $isi):
                    $num++;
                ?>
                    <tr>
                        <td><?= $num ?></td>
                        <td><?= date('d/m/Y H:i:s', strtotime($isi->waktu)) ?></td>
                        <td><?= $isi->ket ?></td>
                        <td class="text-right"><?= $isi->st_in == 1 ? number_format($isi->nilai) : '-' ?></td>
                        <td class="text-right text-red"><?= $isi->st_in == 0 ? number_format($isi->nilai) : '-' ?></td>
                        <td class="text-right"><?= number_format($isi->saldo) ?></td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div><!-- /.box -->
