<div class="box box-primary" lang="en">
    <!--form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_penarikan','name'=>'frm_penarikan','class'=>'form-horizontal'))?>
    <div class="box-body">
    	<div class="form-group <?= form_error('idkonsumen') ? ' has-error' : ''; ?>">
            <label for="idkonsumen" class="col-sm-2 control-label"><?= lang('dp_konsumen') ?></label>
            <div class="col-sm-3">
                <input type="text" class="form-control" name="idkonsumen" value="<?= strtoupper($dt_konsumen->nm) ?>" readonly="">
            </div>
        </div>
        <div class="form-group">
            <label for="saldo" class="col-sm-2 control-label"><?= lang('dp_saldo') ?></label>
            <div class="col-sm-3">
                <div class="input-group">
                    <span class="input-group-addon">Rp</span>
                    <input type="text" class="form-control" name="saldo" value="<?= number_format($saldo) ?>" readonly="">
                </div>
            </div>
        </div>
        <div class="form-group <?= form_error('nilai') ? ' has-error' : ''; ?>">
            <label for="nilai" class="col-sm-2 control-label"><?= lang('dp_nilai_penarikan') ?></label>
            <div class="col-sm-3">
                <div class="input-group">
	                <span class="input-group-addon">Rp</span>
	                <input type="text" class="form-control" name="nilai" value="<?= set_value('nilai') ?>" autofocus="">
	            </div>
            </div>
        </div>
        <div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
            <label for="ket" class="col-sm-2 control-label"><?= lang('dp_ket') ?></label>
            <div class="col-sm-4">
                <textarea class="form-control" id="ket" name="ket"><?php echo set_value('ket'); ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" name="save" class="btn btn-primary"><?= lang('dp_btn_save') ?></button>
                <?php
                echo lang('dp_or') . ' ' . anchor('deposit', lang('dp_btn_cancel'));
                ?>
            </div>
        </div>
    </div>
    <?= form_close() ?>
</div>