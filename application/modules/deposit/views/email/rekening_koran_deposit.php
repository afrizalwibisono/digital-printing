<p>
	Kepada Yth. <?= $full_name ?>,<br>

	Kami informasikan bahwa anda telah melakukan penarikan deposit senilai Rp. <?= number_format($nominal) ?><br>
	Pada tanggal <?= date('d/m/Y H:i:s', strtotime($tanggal)) ?><br>
	Berikut kami lampirkan rincian saldo deposit anda.
	<br>
	<br>
	Terima Kasih.
</p>