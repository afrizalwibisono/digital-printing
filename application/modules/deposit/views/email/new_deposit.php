<p>
	Kepada Yth. <?= $full_name ?>,<br>

	Terima kasih telah melakukan topup deposit senilai Rp. <?= number_format($nominal) ?><br>
	Pada tanggal <?= date('d/m/Y H:i:s', strtotime($tanggal)) ?><br>
	Berikut kami lampirkan rincian saldo deposit anda.
	<br>
	<br>
	Terima Kasih.
</p>