<p>
	Kepada Yth. <?= $full_name ?>,<br>

	Kami informasikan bahwa saldo deposit anda pada tanggal <?= date('d/m/Y H:i:s', strtotime($tanggal)) ?><br>
	adalah senilai Rp. <?= number_format($nominal) ?><br>
	Berikut kami lampirkan rincian saldo deposit anda.
	<br>
	<br>
	Terima Kasih.
</p>