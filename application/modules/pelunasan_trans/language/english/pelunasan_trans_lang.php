<?php defined('BASEPATH')|| exit('No direct script access allowed');


// ===============================================================================================================
$lang['title']									= 'Data Pelunasan Transaksi';
$lang['title_new']								= 'Pelunasan Baru';
$lang['title_view']								= 'Pelunasan Transaksi';

// ===================== Index ======================================

$lang['capt-seacrh-order-tgl1'] 				= "Tanggal Awal";
$lang['capt-seacrh-order-tgl2'] 				= "Tanggal Akhir";


$lang['capt-index-table-waktu']					= "Waktu";
$lang['capt-index-table-konsumen']				= "Konsumen";
$lang['capt-index-table-metode']				= "Metode Pembayaran";
$lang['capt-index-table-nominal']				= "Nominal Bayar";
$lang['capt-index-table-st-cair']				= "Status Cair";
	
	$lang['capt-isi-status-cair-cair']			= "Sudah Cair";
	$lang['capt-isi-status-cair-belum-cair']	= "Menunggu";

// ===================== Form Pelunasan =============================
$lang['capt-form-konsumen'] 					= "Konsumen";
$lang['capt-form-tgl-catat'] 					= "Tanggal Pencatatan";
$lang['capt-form-tipe'] 						= "Tipe Pelunasan";
	
	$lang['capt-form-isi-tipe-cash']			= "Tunai / Cash";
	$lang['capt-form-isi-tipe-debit']			= "Debit";
	$lang['capt-form-isi-tipe-transfer']		= "Transfer";
	$lang['capt-form-isi-tipe-bg']				= "BG";

$lang['capt-form-bayar'] 						= "Nominal Bayar";
$lang['capt-form-no-bg'] 						= "No. BG";
$lang['capt-form-tgl-cair'] 					= "Tgl Estimasi Cair";
$lang['capt-form-tgl-pasti-cair'] 				= "Tgl Pencairan";

$lang['capt-form-bank-asal'] 					= "Bank Pengirim";
$lang['capt-form-an-bank'] 						= "Atas Nama";
$lang['capt-form-no-rek'] 						= "No. Rek";
$lang['capt-form-bank-tujuan'] 					= "Bank Tujuan";

$lang['capt-form-pil-subnota'] 					= "SubNota";
$lang['capt-form-pil-nota'] 					= "Cari Nota";

$lang['capt-form-ph-nota-utama']				= "No. Nota / Invoice";

/*--------------------- Form Judul Tabel ----------------------------*/
$lang['capt-table-form-tgltrans']				= "Tgl. Trans";
$lang['capt-table-form-nonota']					= "No. Nota";
$lang['capt-table-form-sisa-tagihan']			= "Sisa Tagihan";
$lang['capt-table-form-potongan']				= "Potongan";
$lang['capt-table-form-total']					= "Total";
$lang['capt-table-form-bayar']					= "Bayar";
$lang['capt-table-form-tgltempo']				= "Tgl. Tempo";

$lang['capt-table-form-sisapembayaran']			= "Sisa Pembayaran";


// =============================
$lang['btn_create']								= 'Baru';
$lang['btn_delete']								= 'Hapus';
$lang['btn_filter']								= 'Filter';

// =============================
$lang['btn-save']								= "Simpan";
$lang['btn-batal']								= "Batal / Kembali";
$lang['btn-delete']								= "Hapus Data Pelunasan";
$lang['btn-filter']								= "Filter";

$lang['bf_or']									= "or";

// =============================
$lang['label_simpan_sukses'] 					= "Simpan Data Pelunasan baru sukses";
$lang['label_simpan_validasi'] 					= "Simpan Data Pencairan Pelunasan sukses";
$lang['label_cancel_sukses']					= "Data Pelunasan telah berhasil dibatalkan";

$lang['label_simpan_gagal_daftar_kosong']		= "Tidak ada daftar Nota yang akan dilunasi";
$lang['label_simpan_gagal_cicilan_kurang']		= "Total Cicilan kurang dari Nominal Bayar";
$lang['label_simpan_gagal_bayar_lebih']			= "Sisa Pembayaran tidak boleh >(lebih dari) 0";

// =============================
$lang['konfirmasi-delete']						= "Apakah anda yakin akan menghapus data Pelunasan terpilih ? ";
$lang['konfirmasi-data-tidak-ada']				= "Data tidak ditemukan";
$lang['konfirmasi-error-pil-delete']			= "Tidak ada data Pelunasan Transaksi yang dipilih";
$lang['konfirmasi-delete-sukses']				= "Delete data Pelunasan Transaksi sukses";



