var sisa_pembayaran = 0;

$(function(){

	cek_inputan_bank();

	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	$("#tgl_tran, #tgl_cair_bg, #tgl_pencair_bg").datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	$("#konsumen").select2({
        placeholder :"-- Pilih Konsumen --",
        allowClear  : true
  	});

	$("#tipe_bayar").select2({
        placeholder :"-- Pilih Metode Pembayaran --"
  	});

	$("#bank_tujuan").select2({
        placeholder :"-- Pilih Bank Tujuan --",
        allowClear  : true
  	});

  	$("#bayar").number(true);


  	$("#tipe_bayar").change(function(){

  		cek_inputan_bank();

  	});

  	/*$("#pil_subfaktur").change(function(){

  		kosongkan_filter();
  		tampil_nota();

  	});

  	$("#konsumen").change(function(){

  		kosongkan_filter();
  		tampil_nota();

  	});*/

  	$("#btn_cr_nota_utama").click(function(){

  		tampil_nota();

  	});

  	$("#delete").click(function(){

  		info_delete();

  	});

});

function info_delete(){

	var nominal_bayar 	= $("#bayar").val();

	var info 	= 	"Menghapus data Pelunasan, berarti <br><br> "+
					"1. Harus mengembalikan uang senilai <strong>Rp. <span class='text-red'>" + $.number(nominal_bayar)+ "</span></strong>.<br><br>" +
					"2. Alur kas akan mencatat sebagai pengeluaran untuk Bank terkait, sesuai dengan data pelunasan.<br><br>"+
					"<strong>Lanjutkan proses hapus data ?</strong>";

	alertify.confirm(info,
		function(){//jika OK
			$("#st_delete").val(1);
			$("#frm_pelunasan").submit();
		},
		function(){//jika cancel
			$("#st_delete").val('');
		}
	)
}


function normalisasi_pelunasan(){

	var jml_row 		= $("#daftar_nota tr").length - 1;	

	var nominal_bayar 	= parseFloat($("#bayar").val());	

	for (var i = 1; i <= jml_row; i++) {

		var centang = $("#daftar_nota tr:eq("+i+")").find("input[name='checked[]']").is(":checked");
		var tagihan = $("#daftar_nota tr:eq("+i+")").find("input[name='dft_tagihan[]']").val();

		if(centang){

			if(nominal_bayar > tagihan){

				$("#daftar_nota tr:eq("+i+")").find("input[name='dft_bayar[]']").val(tagihan);
				$("#daftar_nota tr:eq("+i+")").children("td:eq(6)").text($.number(tagihan));

				nominal_bayar -= tagihan;

			}else{

				$("#daftar_nota tr:eq("+i+")").find("input[name='dft_bayar[]']").val(nominal_bayar);
				$("#daftar_nota tr:eq("+i+")").children("td:eq(6)").text($.number(nominal_bayar));				

				nominal_bayar = 0;

			}

		}

	}

	$("#sisa_bayar").val(nominal_bayar);
	$("#sisa_pelunasan").text($.number(nominal_bayar));

}

function cek_bayar(obj){

	var jml_row 		= $("#daftar_nota tr").length - 1;

	if($(obj).is(':checked')){

		var nominal_bayar 	= parseFloat($("#bayar").val());

		if(nominal_bayar <= 0){

			alertify.error('Nominal Bayar, KOSONG');
			$(obj).closest("tr").find("input[name='checked[]']").prop('checked',false);			

		}

		var total_cicilan 		= 0;
		
		for (var i = 1; i <= jml_row; i++) {
			
			var centang = $("#daftar_nota tr:eq("+i+")").find("input[name='checked[]']").is(":checked");

			if(centang){

				var dftcicilan 	= $("#daftar_nota tr:eq("+i+")").find("input[name='dft_bayar[]']").val();
				
				total_cicilan		+= parseFloat(dftcicilan);
				
			}

		}

	
		var sisa_tagihan 	= $(obj).closest("tr").find("input[name='dft_tagihan[]']").val();
		var sisa_bayar 		= nominal_bayar - total_cicilan;

		console.log(sisa_tagihan+"\n");
		console.log(total_cicilan + " ==> " + sisa_bayar);

		var sisa_pembayaran = 0;

		if(parseFloat(sisa_tagihan) >= sisa_bayar){

			var cicilan_pakai 	= sisa_bayar;
			var stlunas 		= 0;
			sisa_pembayaran 	= 0;

		}else{

			var cicilan_pakai 	= sisa_tagihan;
			var stlunas 		= 1;

			sisa_pembayaran		= sisa_bayar - sisa_tagihan;			

		}

		if(cicilan_pakai > 0){	

			$(obj).closest("tr").find("input[name='dft_bayar[]']").val(cicilan_pakai);
			$(obj).closest("tr").children("td:eq('6')").text($.number(cicilan_pakai,0));

			$("#sisa_bayar").val(sisa_pembayaran);
			$("#sisa_pelunasan").text($.number(sisa_pembayaran));

		}else{

			alertify.error("Nominal Bayar Tidak Mencukupi");
			$(obj).closest("tr").find("input[name='checked[]']").prop('checked',false);						

		}

	}else{

		/*menetralkan / meng-0 kan cicilan yang dibayarkan*/
		for (var i = 1; i <= jml_row; i++) {
			
			var centang = $("#daftar_nota tr:eq("+i+")").find("input[name='checked[]']").is(":checked");

			if(!centang){

				$("#daftar_nota tr:eq("+i+")").find("input[name='dft_bayar[]']").val('0');
				$("#daftar_nota tr:eq("+i+")").children("td:eq('6')").text('');

			}

		}

		normalisasi_pelunasan();

	}

}

function tampil_nota(){

	var konsumen 	= $("#konsumen").val();
	var id_tran 	= $("#no_faktur_utama").val();
	var tgl1 		= $("#tgl1").val();
	var tgl2 		= $("#tgl2").val();
	var type 		= $("#pil_subfaktur").is(":checked") ? 1 : 0;

	var nominal_bayar 	= $("#bayar").val();

	if(nominal_bayar <= 0){

		alertify.error("Nilai bayar tidak boleh 0");
		return false;

	}

	$("#sisa_bayar").val(nominal_bayar);
	$("#sisa_pelunasan").text($.number(nominal_bayar));

	


	var data 		= {id_konsumen:konsumen,
						id_trans:id_tran,
						tgl1:tgl1,
						tgl2:tgl2,
						st_pil:type};

	$.ajax({
				url 		: baseurl+"pelunasan_trans/get_nota",
				type		: "post",
				dataType 	: "json",
				data 		: data,
				success 	: function(msg){

									if(parseFloat(msg['st'])==1){

										$("#daftar_nota tbody tr").remove()
										$("#daftar_nota tbody").append(msg['tampil']);

									}else{

										$("#daftar_nota tbody tr").remove();										

									}

								}

	});


}

function kosongkan_filter(){

	$("#no_faktur_utama").val('');
	$("#tgl1").val('');
	$("#tgl2").val('');

}

function cek_inputan_bank(){

	var tipe 	= $("#tipe_bayar").val();

	if(tipe.lenght <= 0 || tipe == 0 || tipe == 1){

		$("#baris_bg").hide(400);
		$("#baris_bank").hide(400);

	}else if(tipe == 2){

		$("#baris_bg").hide(400);
		$("#baris_bank").show(400);

	}else{

		$("#baris_bg").show(400);
		$("#baris_bank").show(400);

	}

	var st_lihat 	= parseFloat($("#st_lihat").val());

	if(st_lihat == 0){

		$("#no_bg").val('');
		$("#tgl_cair_bg").val('');
		$("#nm_bank_asal").val('');
		$("#an_bank").val('');
		$("#no_rek_asal").val('');
		$("#bank_asal").val('').trigger('change');

	}

}