$(function(){

  cek_stbg();

	$("#konsumen").select2({
        placeholder :"-- Pilih Konsumen --",
        allowClear  : true
  	});

	$("#metode").select2({
        placeholder :"-- Pilih Metode --",
        allowClear  : true
  	});

	$("#st_bg").select2({
        placeholder :"Status",
        allowClear  : true
  	});

  $('.input-daterange').datepicker({
      todayBtn: "linked",
      format: "dd/mm/yyyy",
      autoclose: true,
      clearBtn: true,
      todayHighlight: true
  });

  $("#metode").change(function(){

    cek_stbg();

  });

});

function cek_stbg(){

  var metode  = $("#metode").val();

  if(metode == 2){

    $("#inputan_st_bg").show(400);

  }else{

    $("#inputan_st_bg").hide(400);

  }

}