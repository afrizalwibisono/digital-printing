<?php
		
		$ENABLE_ADD     = has_permission('Pelunasan Transaksi.Add'); 
    	$ENABLE_DELETE  = has_permission('Pelunasan Transaksi.Delete'); 
   
?>


<?= form_open($this->uri->uri_string(),array("name"=>"frm_index", "id"=>"frm_index", "role"=>"form", "class"=>"form-inline")) ?>

<div class="box box-primary">
	
	<div class="box-header">
		
		<?php if ($ENABLE_ADD) : ?> 
        <a href="<?= site_url('pelunasan_trans/create') ?>" class="btn btn-success" title="<?= lang('btn_create') ?>"><?= lang('btn_create') ?></a> 
        <?php endif; ?> 

        <div class="pull-right">
        		
        	<div class="form-group">
        		<div class="form-group input-daterange">
					<div class="input-group">
						<input type="text" name="tgl1" id="tgl1" class="form-control" readonly value="<?= set_value('tgl1',isset($tgl1) ? $tgl1 : '' ) ?>" placeholder="<?= lang('capt-seacrh-order-tgl1') ?>" style="width: 120px" >
						<span class="input-group-addon">s/d</span>
						<input type="text" name="tgl2" id="tgl2" class="form-control" readonly value="<?= set_value('tgl2',isset($tgl2) ? $tgl2 : '' ) ?>" placeholder="<?= lang('capt-seacrh-order-tgl2') ?>" style="width: 120px" >
					</div>
				</div>			
        	</div>

        	<div class="form-group">
        		<select class="form-control" name="konsumen" id="konsumen" style="min-width: 200px">
        			<option></option>
        			<?php 
					if(isset($konsumen) && is_array($konsumen) && count($konsumen)):
						foreach ($konsumen as $key => $isi) :
									
					?>
							
							<option value="<?= $isi->id ?>" <?= set_select('konsumen',$isi->id,isset($idkonsumen) && $idkonsumen == $isi->id) ?> >
								<?= ucwords($isi->nm) ?>
							</option>

					<?php			
						endforeach;
					endif;
					?>
        		</select>
        	</div>

        	<div class="form-group">
        		<select class="form-control" name="metode" id="metode" style="min-width: 150px">
        			<option></option>
					<option value="0" <?= set_select('metode',"0",isset($metode) && $metode == '0' ) ?> >
						<?= lang('capt-form-isi-tipe-cash') ?>
					</option>
					<option value="1" <?= set_select('metode',"1",isset($metode) && $metode == '1' ) ?> >
						<?= lang('capt-form-isi-tipe-debit') ?>
					</option>
					<option value="2" <?= set_select('metode',"2",isset($metode) && $metode == '2' ) ?> >
						<?= lang('capt-form-isi-tipe-transfer') ?>
					</option>
					
        		</select>
        	</div>

        	<div class="form-group" id="inputan_st_bg" >
        		<select class="form-control" name="st_bg" id="st_bg" style="min-width: 100px">
        			<option></option>
        			<option value="0" <?= set_select("st_bg","0",isset($st_bg) && $st_bg == '0') ?> >
        				<?= lang("capt-isi-status-cair-belum-cair") ?>
        			</option>
        			<option value="1" <?= set_select("st_bg","1",isset($st_bg) && $st_bg == '1') ?> >
        				<?= lang("capt-isi-status-cair-cair") ?>
        			</option>
        		</select>
        	</div>

			<div class="form-group">
        		<button name="cari" class="btn btn-primary">
        			<?= lang("btn_filter") ?> <span class="fa fa-search"></span> 
        		</button>
        	</div>        
        		
        

        </div>


	</div>
	<?php

		if(isset($data) && is_array($data) && count($data)):
			

	?>
	<div class="box-body table-responsive no-padding">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th class="text-center" width="30">#</th>
					<th class="text-center" > <?= lang("capt-index-table-waktu") ?> </th>
					<th class="text-center" > <?= lang("capt-index-table-konsumen") ?> </th>
					<th class="text-center" > <?= lang("capt-index-table-metode") ?> </th>
					<th class="text-center" > <?= lang("capt-index-table-nominal") ?> </th>
					<th></th>
				</tr>
			</thead>	
			<tbody>
				<?php
					foreach ($data as $key => $isi):

						switch ($isi->st_bayar) {
							case 0:
								$tampil 	= "<span class='label label-success'>".lang("capt-form-isi-tipe-cash")."</span>";
								break;
							case 1:
								$tampil 	= "<span class='label label-success'>".lang("capt-form-isi-tipe-debit")."</span>";
								break;
							case 2:
								$tampil 	= "<span class='label label-primary'>".lang("capt-form-isi-tipe-transfer")."</span>";
								break;
							
						}

				?>

					<tr>
						<td class="text-center"><?= $numb ?></td>
						<td class="text-center"><?= $isi->tgl ?></td>
						<td class="text-center"><?= ucwords($isi->nama_konsumen) ?></td>
						<td class="text-center"><?= $tampil ?></td>
						<td class="text-center"><?= number_format($isi->nominal_bayar) ?></td>
						<td>
							<a class="text-black" target="_blank" href="<?= site_url('pelunasan_trans/view/' .$isi->id); ?>" data-toggle="tooltip" data-placement="left" title="Lihat Transaksi"><i class="fa fa-folder-open"></i></a> 
						</td>

					</tr>

				<?php
				
					$numb++;
					endforeach;
				?>
			</tbody>		
		</table>
	</div>
	<div class="box-footer clearfix">
		
		<?php
			echo $this->pagination->create_links(); 
		?>	
		
	</div>

	<?php
		
		else:
	?>

		<div class="alert alert-info" role="alert">
        	<p><i class="fa fa-warning"></i> &nbsp; <?= lang('konfirmasi-data-tidak-ada') ?></p>
    	</div>

	<?php		
		endif;	
	?>

</div>


<?= form_close() ?>