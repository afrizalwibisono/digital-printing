<?php
		$ENABLE_DELETE  = has_permission('Pelunasan Transaksi.Delete'); 

		$disabled 		= $data['st_lihat'] == 1 ? 'disabled' : '';
		if($data['st_lihat'] == 0 || $data['st_lihat'] == 1 && isset($validasi_bg)){

			$disable_tgl_simpan 	= '';

		}else{

			$disable_tgl_simpan 	= 'disabled';

		}
?>

<?= form_open($this->uri->uri_string(),array("name"=>"frm_pelunasan", "id"=>"frm_pelunasan", "role"=>"form")) ?>

<div class="box box-primary">
	
	<input type="hidden" name="st_lihat" id="st_lihat" value="<?= $data['st_lihat'] ?>">

	<div class="box-body">
		
		<div class="row"> <!-- Baris Pertama -->
			<div class="col-sm-3">
				
				<div class="form-group <?= form_error('konsumen') ? 'has-error' : '' ?>">
					<label class="control-label nomargin_bottom_label" for="konsumen"><?= lang("capt-form-konsumen") ?></label>
					<select name="konsumen" id="konsumen" class="form-control" <?= $disabled ?> >
						<option></option>
						<?php 
						if(isset($konsumen) && is_array($konsumen) && count($konsumen)):
							foreach ($konsumen as $key => $isi) :
										
						?>
								
								<option value="<?= $isi->id ?>" <?= set_select('konsumen',$isi->id,isset($data['idkonsumen']) && $data['idkonsumen'] == $isi->id) ?> >
									<?= ucwords($isi->nm) ?>
								</option>

						<?php			
							endforeach;
						endif;
						?>
					</select>
				</div>

			</div>
			<div class="col-sm-3">

				<div class="form-group <?= form_error('tgl_tran') ? 'has-error' : '' ?>">
					<label class="control-label nomargin_bottom_label" ><?= lang("capt-form-tgl-catat") ?></label>
					<input type="text" name="tgl_tran" id="tgl_tran" class="form-control" value="<?= set_value('tgl_tran',isset($data['tgl_catat']) ? $data['tgl_catat'] : '') ?>" readonly <?= $disabled ?> >
				</div>

			</div>
			<div class="col-sm-3">

				<div class="form-group <?= form_error('tipe_bayar') ? 'has-error' : '' ?>">
					<label class="control-label nomargin_bottom_label" ><?= lang("capt-form-tipe") ?></label>
					<select name="tipe_bayar" id="tipe_bayar" class="form-control" <?= $disabled ?> >
						<option></option>
						<option value="0" <?= set_select('tipe_bayar',"0",isset($data['tipe_pelunasan']) && $data['tipe_pelunasan'] == 0 ) ?> >
							<?= lang('capt-form-isi-tipe-cash') ?>
						</option>
						<option value="1" <?= set_select('tipe_bayar',"1",isset($data['tipe_pelunasan']) && $data['tipe_pelunasan'] == 1 ) ?> >
							<?= lang('capt-form-isi-tipe-debit') ?>
						</option>
						<option value="2" <?= set_select('tipe_bayar',"2",isset($data['tipe_pelunasan']) && $data['tipe_pelunasan'] == 2 ) ?> >
							<?= lang('capt-form-isi-tipe-transfer') ?>
						</option>
						<!-- <option value="3" <?= set_select('tipe_bayar',"3",isset($data['tipe_pelunasan']) && $data['tipe_pelunasan'] == 3 ) ?> >
							<?= lang('capt-form-isi-tipe-bg') ?>
						</option> -->
					</select>
				</div>

			</div>
			<div class="col-sm-3 <?= form_error('bayar') ? 'has-error' : '' ?>">
				<div class="form-group">
					<label class="control-label nomargin_bottom_label" ><?= lang("capt-form-bayar") ?></label>
					<input type="text" name="bayar" id="bayar" class="form-control" value="<?= set_value('bayar',isset($data['nominal_bayar']) ? $data['nominal_bayar'] : '') ?>" <?= $disabled ?> >
				</div>
			</div>
		</div>

		<div class="row" id="baris_bg"> <!-- Baris Ke-Dua -->

			<div class="col-sm-3">

				<div class="form-group <?= form_error('no_bg') ? 'has-error' : '' ?>">
					<label class="control-label nomargin_bottom_label" ><?= lang("capt-form-no-bg") ?></label>
					<input type="text" name="no_bg" id="no_bg" class="form-control" value="<?= set_value('no_bg',isset($data['no_bg']) ? $data['no_bg'] : '' ) ?>" <?= $disabled ?> >
				</div>
				
			</div>
			<div class="col-sm-3">

				<div class="form-group <?= form_error('tgl_cair_bg') ? 'has-error' : '' ?>">
					<label class="control-label nomargin_bottom_label" ><?= lang("capt-form-tgl-cair") ?></label>
					<input type="text" name="tgl_cair_bg" id="tgl_cair_bg" class="form-control" value="<?= set_value('tgl_cair_bg',isset($data['tgl_est_cair']) ? $data['tgl_est_cair'] : '') ?>" readonly <?= $disabled ?> >
				</div>

			</div>

			<div class="col-sm-3">

				<div class="form-group <?= form_error('tgl_pencair_bg') ? 'has-error' : '' ?>">
					<label class="control-label nomargin_bottom_label" >
						<?= lang("capt-form-tgl-pasti-cair") ?>
					</label>
					<input type="text" name="tgl_pencair_bg" id="tgl_pencair_bg" class="form-control" readonly value="<?= set_value('tgl_pencair_bg',isset($data['tgl_cair']) ? $data['tgl_cair'] : '') ?>" >
				</div>				

			</div>
			
		</div>

		<div class="row" id="baris_bank"> <!-- Baris Ke-Tiga -->

			<div class="col-sm-3 <?= form_error('nm_bank_asal') ? 'has-error' : '' ?>">

				<div class="form-group">
					<label class="control-label nomargin_bottom_label" ><?= lang("capt-form-bank-asal") ?></label>
					<input type="text" name="nm_bank_asal" id="nm_bank_asal" class="form-control" value="<?= set_value('nm_bank_asal',isset($data['bank_asal']) ? $data['bank_asal'] : '') ?>" <?= $disabled ?> >
				</div>				
				
			</div>
			<div class="col-sm-3">

				<div class="form-group <?= form_error('an_bank') ? 'has-error' : '' ?>">
					<label class="control-label nomargin_bottom_label" ><?= lang("capt-form-an-bank") ?></label>
					<input type="text" name="an_bank" id="an_bank" class="form-control" value="<?= set_value('an_bank',isset($data['bank_nama']) ? $data['bank_nama'] : '') ?>" <?= $disabled ?> >
				</div>				
				
				
			</div>
			<div class="col-sm-3">
				
				<div class="form-group <?= form_error('no_rek_asal') ? 'has-error' : '' ?>">
					<label class="control-label nomargin_bottom_label" ><?= lang("capt-form-no-rek") ?></label>
					<input type="text" name="no_rek_asal" id="no_rek_asal" class="form-control" value="<?= set_value('no_rek_asal',isset($data['bank_rek']) ? $data['bank_rek'] : '') ?>" <?= $disabled ?> >
				</div>				

			</div>
			<div class="col-sm-3">
				
				<div class="form-group <?= form_error('bank_tujuan') ? 'has-error' : '' ?>">
					<label class="control-label nomargin_bottom_label" ><?= lang("capt-form-bank-tujuan") ?></label>
					<select name="bank_tujuan" id="bank_tujuan" class="form-control" <?= $disabled ?> >
						<option></option>
						<?php
							if(isset($bank) && is_array($bank) && count($bank)):
								foreach ($bank as $key => $isi) :
									
						?>
							<option value="<?= $isi->id ?>" <?= set_select('bank_tujuan',$isi->id,isset($data['id_bank']) && $data['id_bank'] == $isi->id ) ?> >
								<?= $isi->nama ?>
							</option>
						<?php
								endforeach;
							endif;	
						?>
					</select>
				</div>

			</div>
			
		</div>
		<hr>
		<div class="row"> <!-- baris inputan pencarian no.faktur -->
			
			<!-- <div class="col-sm-1">

				<div class="form-group">
					<div class="checkbox">
						<label>
							<?php
									$centang = '';
									if($data['st_pil_subnota'] > 0){

										$centang 	= "checked";

									}
							?>
						<input type="checkbox" <?= $centang ?> name="pil_subfaktur" id="pil_subfaktur" value="1" >
							<strong><?= lang("capt-form-pil-subnota") ?></strong>
						</label>	
					</div>	
				</div>

			</div> -->
			<div class="col-sm-12 form-inline">

				<div class="form-group">
					<input type="text" name="no_faktur_utama" id="no_faktur_utama" class="form-control" placeholder="<?= lang("capt-form-ph-nota-utama") ?>" value="<?= set_value('no_faktur_utama',isset($data['no_utama_filter']) ? $data['no_utama_filter'] : '') ?>"	 >
				</div>	
				<div class="form-group input-daterange">
					<div class="input-group">
						<input type="text" name="tgl1" id="tgl1" class="form-control" placeholder="<?= lang('capt-seacrh-order-tgl1') ?>" readonly value="<?= set_value('tgl1',isset($data['tgl1_filter']) ? $data['tgl1_filter'] : '' ) ?>">
						<span class="input-group-addon">s/d</span>
						<input type="text" name="tgl2" id="tgl2" class="form-control" placeholder="<?= lang('capt-seacrh-order-tgl2') ?>" readonly value="<?= set_value('tgl2',isset($data['tgl2_filter']) ? $data['tgl2_filter'] : '' ) ?>" >
					</div>
				</div>			
				<div class="form-group">
					<button type="button" id="btn_cr_nota_utama" name="btn_cr_nota_utama" class="btn btn-success" <?= $disabled ?> >
						<span><?= lang("btn_filter") ?></span>
					</button>
				</div>
			</div>

		</div>

		<div style="padding-top: 10px !important"></div>

		<div class="table-responsive form-horizontal">
			
			<table class="table table-bordered" id="daftar_nota">
				<thead>
					<tr class="success">
						<th width="30" class="text-center">#</th>
						<th class="text-center" ><?= lang("capt-table-form-tgltrans") ?></th>
						<th class="text-center" ><?= lang("capt-table-form-nonota") ?></th>
						<th class="text-center" ><?= lang("capt-table-form-tgltempo") ?></th>
						<th class="text-center" ><?= lang("capt-table-form-total") ?></th>
						<th class="text-center" ><?= lang("capt-table-form-sisa-tagihan") ?></th>
						<th class="text-center" ><?= lang("capt-table-form-bayar") ?></th>
					</tr>
				</thead>
				<tbody>
					<?php 
							if(isset($data['detail']) && is_array($data['detail']) && count($data['detail'])):	
								foreach ($data['detail'] as $key => $dt):
										
					?>

						<tr>
							<td>
								<?php

									if($data['st_lihat'] == 0):

										$centang_list 	= $dt['checked'] > 0 ? "checked" : '';
								?>
								<input type="checkbox" <?= $centang_list ?> name="checked[]" id="checked" value="<?= ($key+1) ?>" onchange="cek_bayar(this)">

								<?php

									endif;

								?>
							</td>
							<td class="text-center"><?= $dt['dft_waktu_trans'] ?></td>
							<td class="text-center">
								<input type="hidden" name="dft_idtrans[]" 			value="<?= $dt['dft_idtrans'] ?>"> 
								<input type='hidden' name='dft_stsubnota[]'         value='<?= $dt['dft_stsubnota'] ?>'>
                                <input type='hidden' name='dft_idtrans_master[]'    value='<?= $dt['dft_idtrans_master'] ?>'>
                                <input type='hidden' name='dft_gtotal[]'            value='<?= $dt['dft_gtotal'] ?>'>
                                <input type='hidden' name='dft_tagihan[]'           value='<?= $dt['dft_tagihan'] ?>'>
                                <input type='hidden' name='dft_bayar[]'           	value='<?= $dt['dft_bayar'] ?>'>
                                <input type='hidden' name='dft_nofaktur[]'          value='<?= $dt['dft_nofaktur'] ?>'>
                                <input type='hidden' name='dft_waktu_trans[]'       value='<?= $dt['dft_waktu_trans'] ?>'>
                                <input type='hidden' name='dft_jtmp[]'              value='<?= $dt['dft_jtmp'] ?>'>
								<?= $dt['dft_nofaktur'] ?>
							</td>
							<td class="text-center"><?= $dt['dft_jtmp'] ?></td>
							<td class="text-center"><?= number_format($dt['dft_gtotal']) ?></td>
							<td class="text-center"><?= $dt['dft_tagihan']>0 ? number_format($dt['dft_tagihan']) : '-' ?></td>
							<td class="text-right"><?= number_format($dt['dft_bayar']) ?></td>
						</tr>

					<?php
								endforeach;
							endif;
					?>
				</tbody>
				<tfoot>
					<tr class="info">
						<th colspan="6" class="text-right"><?= lang('capt-table-form-sisapembayaran') ?></th>
						<th class="text-right">
							<input type="hidden" name="sisa_bayar" id="sisa_bayar" value="<?= set_value('sisa_bayar',isset($data['sisa_bayar']) ? number_format($data['sisa_bayar']) : '0' ) ?>">
							<span id="sisa_pelunasan">
								<?= isset($data['sisa_bayar']) ? number_format($data['sisa_bayar']) : '0'  ?>	
							</span>
							
						</th>
					</tr>
				</tfoot>

			</table>

		</div>

		<div class="form-horizontal">
			<div class="form-group">
				<div class="col-sm-12">
					<?php 
						if($data['st_lihat'] == 1 && $ENABLE_DELETE):
					?>

						<button type="button" name="delete" id="delete" class="btn btn-danger">
		    				<?= lang("btn-delete") ?> <span class="fa fa-close"></span> 
		    			</button>
		    			<input type="hidden" name="st_delete" id="st_delete" value="0">

					<?php 

						endif;

					?>

					<div class="pull-right">
						<?php
			                echo anchor('pelunasan_trans/cancel', lang('btn-batal')).' '. lang('bf_or');
			            ?>

						<button class="btn btn-primary" type="submit" name="simpan" <?= $disable_tgl_simpan ?> >
							<?= lang('btn-save') ?>
						</button>

					</div>

				</div>
			</div>
		</div>


	</div>

</div>

<?= form_close() ?>
