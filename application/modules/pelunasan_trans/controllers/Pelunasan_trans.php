<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Pelunasan_trans extends Admin_Controller {

    protected $viewPermission   = "Pelunasan Transaksi.View";
    protected $addPermission    = "Pelunasan Transaksi.Add";
    protected $deletePermission = "Pelunasan Transaksi.Delete";
    protected $managePermission = "Pelunasan Transaksi.Manage";

    protected $prefixKey        = "PL";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('pelunasan_trans/pelunasan_trans');
        $this->load->model(array(
        							"konsumen_model",
        							"pelunasan_detail_model",
        							"pelunasan_model",
        							"subtrans_model",
        							"trans_model",
        							"bank_model",
                                    "bayar_trans_model"
        						)
    						);

        $this->template->title(lang('title'));
		$this->template->page_icon('fa fa-list');
    }

    public function index(){

    	$this->auth->restrict($this->viewPermission);
    	
    	$this->load->library('pagination');

    	$filter 	= "?pl_trans";

    	if(isset($_POST["tgl1"])){

    		$tgl1_filter 	= isset($_POST['tgl1']) ? $this->input->post("tgl1") : '';
    		$tgl2_filter 	= isset($_POST['tgl2']) ? $this->input->post("tgl2") : '';

    		$konsumen 		= isset($_POST['konsumen']) ? $this->input->post("konsumen") : '';
    		$metode      	= isset($_POST['metode']) ? $this->input->post("metode") : '';
    		$st_bg      	= isset($_POST['st_bg']) ? $this->input->post("st_bg") : '0' ;

    	}else if(isset($_GET["tgl1"])){

    		$tgl1_filter 	= isset($_GET['tgl1']) ? $this->input->get("tgl1") : '';
    		$tgl2_filter 	= isset($_GET['tgl2']) ? $this->input->get("tgl2") : '';

    		$konsumen 		= isset($_GET['konsumen']) ? $this->input->get("konsumen") : '';
    		$metode      	= isset($_GET['metode']) ? $this->input->get("metode") : '';
    		$st_bg      	= isset($_GET['st_bg']) ? $this->input->get("st_bg") : '0' ;

    	}else{

    		$tgl1_filter 	= date('d/m/Y');
    		$tgl2_filter 	= date('d/m/Y');

    		$konsumen 		= '';
    		$metode      	= '';
    		$st_bg      	= '';

    	}       


    	$tgl1 	= strlen($tgl1_filter) > 0 ? date_ymd($tgl1_filter)." 01:00:00" : '';
    	$tgl2 	= strlen($tgl2_filter) > 0 ? date_ymd($tgl2_filter)." 23:59:00" : '';

    	$where 	= "`pelunasan_trans_jual`.`deleted` = 0 and `pelunasan_trans_jual`.`waktu` >= '{$tgl1}' and `pelunasan_trans_jual`.`waktu` <= '{$tgl2}' ";

    	if(strlen($konsumen) > 0){

    		$where 	.= " and `pelunasan_trans_jual`.`idkonsumen` = '{$konsumen}' ";

    	}

    	if(strlen($metode)>0){

    		$where 	.= " and `pelunasan_trans_jual`.`st_bayar` = {$metode} ";

    	}

    	if(strlen($st_bg)>0){

			$where 	.= " and  `pelunasan_trans_jual`.`st_cair` = {$st_bg} ";

    	}

    	$total 	= $this->pelunasan_model->select('`pelunasan_trans_jual`.`idpelunasan`')
    									->join("konsumen","pelunasan_trans_jual.idkonsumen = konsumen.idkonsumen","inner")
    									->where($where)
    									->count_all();

    	$offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data 	= $this->pelunasan_model
                        ->select("`pelunasan_trans_jual`.`id_pelunasan_trans_jual` as id,
                                    `pelunasan_trans_jual`.`st_trans`,
                                    DATE_FORMAT(`pelunasan_trans_jual`.`waktu`,
                                            '%d/%m/%Y %H:%i:%s') AS tgl,
                                    `pelunasan_trans_jual`.`st_bayar`,
                                    `pelunasan_trans_jual`.`nominal_bayar`,
                                    `konsumen`.`panggilan`,
                                    `konsumen`.`nama`,
                                    `konsumen`.`st`,
                                    IF(`konsumen`.`st` = 0,
                                        'Konsumen',
                                        IF(`konsumen`.`st` = 1,
                                            'Reseller',
                                            'Instansi')) AS st_konsumen,
                                    CONCAT(`konsumen`.`panggilan`,
                                            ' ',
                                            `konsumen`.`nama`) AS nama_konsumen")
						->join("konsumen","pelunasan_trans_jual.idkonsumen = konsumen.idkonsumen","inner")
						->where($where)
                        ->order_by(`pelunasan_trans_jual`.`waktu`, 'asc')
						->limit($limit,$offset)
						->find_all();

    	$data_konsumen 	= $this->konsumen_model
                                ->select("`idkonsumen` as id, 
                                        CONCAT(IF(panggilan IS NULL, '', panggilan),
                                        ' ',
                                        `nama`,
                                        ' | ',
                                        IF(st = 0,
                                            'konsumen',
                                            IF(st = 1, 'reseller', 'instansi'))) AS nm")
                                ->where("deleted = 0")
                                ->order_by("nama","asc")
                                ->find_all();

    	$asset	= array(
    						"plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
                            "plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
    						"plugins/select2/dist/css/select2.min.css",
                        	"plugins/select2/dist/js/select2.min.js",
    						"pelunasan_trans/assets/js/index_pelunasan.js"
    				);

    	add_assets($asset);

    	$this->template->set("numb",$offset+1);

    	$this->template->set("data",$data);
    	$this->template->set("idkonsumen", $konsumen);
    	$this->template->set("tgl1", $tgl1_filter);
    	$this->template->set("tgl2", $tgl2_filter);
    	$this->template->set("metode", $metode);
    	$this->template->set("st_bg", $st_bg);

    	$this->template->set("konsumen",$data_konsumen);
    	$this->template->set("toolbar_title", lang('title'));
        $this->template->set("page_title", lang('title'));
        $this->template->set("title", lang('title'));

        $this->template->render('index');

    }


    public function delete($id_pelunasan = null){

        $id         = $id_pelunasan;
        $sql_all    = '';


        $this->db->trans_start();

        // start update transaksinya menjadi belum lunas dan tambah nilai kurang bayarnya

        $sql    = "UPDATE transaksi
                            INNER JOIN
                        (SELECT 
                            id_transaksi,
                            bayar
                        FROM
                            bayar_trans_kasir
                        WHERE
                            `id_pelunasan_trans_jual` = '{$id}' and deleted = 0) 
                            AS lunas ON transaksi.id_transaksi = lunas.id_transaksi 
                    SET 
                        transaksi.st_lunas = 0,
                        transaksi.kurang_bayar = transaksi.kurang_bayar + lunas.bayar
                    WHERE
                        transaksi.id_transaksi = lunas.id_transaksi";

        $this->db->query($sql);
        $sql_all    .= "\n\n".$sql;

        // end update transaksinya menjadi belum lunas

        // start buat kas keluarnya

        $dt_bayar   = $this->pelunasan_model
                            ->select("`st_bayar`, `id_bank`, `nominal_bayar`")
                            ->find($id);

        $cr_bayar       = $dt_bayar->st_bayar;
        $id_bank        = $dt_bayar->id_bank;
        $nominal_bayar  = $dt_bayar->nominal_bayar;

        $ket    = "Pengeluaran untuk pembatalan pelunasan kode : ".$id;

        if($cr_bayar == 0){

            simpan_alurkas($kode_accountKas = 4, $ket = $ket, $total = $nominal_bayar , $status = 0, $nm_hak_akses = $this->deletePermission, $model_transaksi = 0, $id_bank = 0, $tgl_proses = "");

        }elseif($cr_bayar == 1){

            $bank = $this->bank_model->select('`id_bank`')
                        ->where('deleted = 0 and `st_default` = 1')
                        ->find_all();

            if(count($bank) && is_array($bank)){

                $id_bank    = $bank[0]->id_bank;

            }else{

                $id_bank    = null;

            }

            simpan_alurkas($kode_accountKas = 4, $ket = $ket, $total = $nominal_bayar , $status = 0, $nm_hak_akses = $this->deletePermission, $model_transaksi = 1, $id_bank = $id_bank, $tgl_proses = "");                

        }else{

            simpan_alurkas($kode_accountKas = 4, $ket = $ket, $total = $nominal_bayar , $status = 0, $nm_hak_akses = $this->deletePermission, $model_transaksi = 1, $id_bank = $id_bank, $tgl_proses = "");                

        }

        // end buat kas keluarnya        

        // start delete data bayar transaksinya

        $this->bayar_trans_model->delete_where(['id_pelunasan_trans_jual' => $id]);
        $sql_all .= "\n\n".$this->db->last_query();

        // end delete data bayar transaksinya

        // start delete data pelunasan_trans

        $this->pelunasan_model->delete($id);
        $sql_all .= "\n\n".$this->db->last_query();        

        // end delete data pelunasan_trans

        $this->db->trans_complete();

        
		if($this->db->trans_status() == false){

			$return     = false;
        	$keterangan = "Gagal, delete data pelunasan";
        	$status     = 0;


		}else{

			$return     = true;
        	$keterangan = "Sukses, delete data pelunasan";
        	$status     = 1;

		}

		$nm_hak_akses   = $this->deletePermission; 
    	$kode_universal = "-";
    	$jumlah         = 0;
    	$sql 			= $sql_all;

    	simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);    		

    	return $return;
    	
    }

    public function create(){

    	if(isset($_POST["simpan"]) && has_permission($this->addPermission)){

    		if($this->save()){

    			$this->template->set_message(lang("label_simpan_sukses"),"success");
    			$this->session->unset_userdata("frm_pelunasan_trans");
    			redirect("pelunasan_trans");

    		}

    	}

    	$data 	= $this->session->userdata("frm_pelunasan_trans");

    	if(!$data){

    		$this->set_buat_session(0,"");
			$data 	= $this->session->userdata("frm_pelunasan_trans");    		

    	}
		
	   	$data_konsumen 	= $this->konsumen_model
                                ->select("`idkonsumen` as id, 
                                        CONCAT(IF(panggilan IS NULL, '', panggilan),
                                        ' ',
                                        `nama`,
                                        ' | ',
                                        IF(st = 0,
                                            'konsumen',
                                            IF(st = 1, 'reseller', 'instansi'))) AS nm")
                                ->where("deleted = 0")
                                ->order_by("nama","asc")
                                ->find_all();

		$data_bank		= $this->bank_model->select("`id_bank` AS id,
													    CONCAT(`nama_bank`,
													            ' | ',
													            `nomer_rek`,
													            ' | ',
													            `atas_nama`) AS nama")
											->order_by("nama_bank","asc")
											->where("deleted","0")
											->find_all();

    	$asset	= array(
    						"plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
                            "plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
    						"plugins/number/jquery.number.js",
    						"plugins/select2/dist/css/select2.min.css",
                        	"plugins/select2/dist/js/select2.min.js",
    						"pelunasan_trans/assets/css/style.css",
    						"pelunasan_trans/assets/js/frm_pelunasan.js"
    				);

    	add_assets($asset);

    	$this->template->set("data",$data);

    	$this->template->set("bank",$data_bank);
    	$this->template->set("konsumen",$data_konsumen);
		$this->template->set("toolbar_title", lang('title_new'));
        $this->template->set("page_title", lang('title_new'));
        $this->template->title(lang('title_new'));
        $this->template->page_icon('fa fa-file');;

        $this->template->render('frm_pelunasan');    	

    }

    public function cek_pil_dropdown($val){

        return $val == "" ? false : true;

    }

    public function validasi(){

    	$this->auth->restrict($this->managePermission);

    	$id 	= $this->uri->segment(3);
    	$st 	= $this->uri->segment(4);

    	$param 	= $id."|".$st;

    	if(isset($_POST['simpan'])){

    		if($this->simpan_validasi($param)){

    			$this->template->set_message(lang("label_simpan_validasi"),"success");
    			$this->session->unset_userdata("frm_pelunasan_trans");
    			redirect("pelunasan_trans");

    		}

    	}

    	
    	$this->set_buat_session(1,$param);

    	$data 	= $this->session->userdata("frm_pelunasan_trans");

    	$this->session->unset_userdata("frm_pelunasan_trans");

		$data_konsumen 	= $this->konsumen_model
                                ->select("`idkonsumen` as id, 
                                        CONCAT(IF(panggilan IS NULL, '', panggilan),
                                        ' ',
                                        `nama`,
                                        ' | ',
                                        IF(st = 0,
                                            'konsumen',
                                            IF(st = 1, 'reseller', 'instansi'))) AS nm")
                                ->where("deleted=0")
                                ->order_by("nama","asc")
                                ->find_all();

		$data_bank		= $this->bank_model->select("`id_bank` AS id,
													    CONCAT(`nama_bank`,
													            ' | ',
													            `nomer_rek`,
													            ' | ',
													            `atas_nama`) AS nama")
											->order_by("nama_bank","asc")
											->where("deleted","0")
											->find_all();

    	$asset	= array(
    						"plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
                            "plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
    						"plugins/number/jquery.number.js",
    						"plugins/select2/dist/css/select2.min.css",
                        	"plugins/select2/dist/js/select2.min.js",
    						"pelunasan_trans/assets/css/style.css",
    						"pelunasan_trans/assets/js/frm_pelunasan.js"
    				);

    	add_assets($asset);

    	$this->template->set("validasi_bg",1);

    	$this->template->set("data",$data);

    	$this->template->set("bank",$data_bank);
    	$this->template->set("konsumen",$data_konsumen);
		$this->template->set("toolbar_title", lang('title_view'));
        $this->template->set("page_title", lang('title_view'));
        $this->template->title(lang('title_view'));
        $this->template->page_icon('fa fa-file');;

        $this->template->render('frm_pelunasan');    	    	

    }

    public function simpan_validasi($id){

    	/* jika sudah divalidasi, maka dimasukkan dalam alur kas */

		$tgl_cair 			= $this->input->post("tgl_pencair_bg");
				
		$this->form_validation->set_rules("tgl_pencair_bg","lang:capt-form-tgl-pasti-cair","required");

		if($this->form_validation->run()=== false){

			$this->set_buat_session(1,$id);
            Template::set_message(validation_errors(),'error');
            return false;			

		}

		$pecah 	= explode("|", $id);
		$id 	= $pecah[0];// id pelunasan		

		$data 	= $this->pelunasan_model->select("`id_bank`, `bayar`, `no_bg`")->find($id);

		$arr 	= array("st_cair" => 1, "tgl_cair" => date_ymd($tgl_cair));

		$this->db->trans_start();

			$this->pelunasan_model->update($id,$arr);
			$sql 	= $this->db->last_query();

		$this->db->trans_complete();

		if($this->db->trans_status()==false){

			$return     = false;
            $keterangan = "GAGAL, update status pencairan BG " ;
            $status     = 0;

		}else{

			$return     = true;
            $keterangan = "SUKSES, update status pencairan BG " ;
            $status     = 1;
		}	

		$nm_hak_akses   = $this->managePermission; 
        $kode_universal = $id;
        $jumlah         = $data->bayar;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        if($return){

        	simpan_alurkas($kode_accountKas = 4, $ket = "BG no. {$data->no_bg} dicairkan", $total = $jumlah , $status = 1, $nm_hak_akses = $this->managePermission, $model_transaksi = 1, $id_bank = $data->id_bank, $tgl_proses = date_ymd($tgl_cair));				

        }

        return $return;

    }


    public function view(){

    	$id 	= $this->uri->segment(3);
    	//$st 	= $this->uri->segment(4);

        if(isset($_POST['st_delete']) && $_POST['st_delete'] == 1 && has_permission($this->deletePermission)){

            $del    = $this->delete($id);

            if($del){

                $this->template->set_message(lang('konfirmasi-delete-sukses'),'success');
                redirect('pelunasan_trans');

            }

        }


    	$param 	= $id;

    	$this->set_buat_session(1,$param);

    	$data 	= $this->session->userdata("frm_pelunasan_trans");

    	$this->session->unset_userdata("frm_pelunasan_trans");

		$data_konsumen 	= $this->konsumen_model
                                ->select("`idkonsumen` as id, 
                                        CONCAT(IF(panggilan IS NULL, '', panggilan),
                                        ' ',
                                        `nama`,
                                        ' | ',
                                        IF(st = 0,
                                            'konsumen',
                                            IF(st = 1, 'reseller', 'instansi'))) AS nm")
                                ->where("deleted=0")
                                ->order_by("nama","asc")
                                ->find_all();

		$data_bank		= $this->bank_model->select("`id_bank` AS id,
													    CONCAT(`nama_bank`,
													            ' | ',
													            `nomer_rek`,
													            ' | ',
													            `atas_nama`) AS nama")
											->order_by("nama_bank","asc")
											->where("deleted","0")
											->find_all();

    	$asset	= array(
    						"plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
                            "plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
    						"plugins/number/jquery.number.js",
    						"plugins/select2/dist/css/select2.min.css",
                        	"plugins/select2/dist/js/select2.min.js",
    						"pelunasan_trans/assets/css/style.css",
    						"pelunasan_trans/assets/js/frm_pelunasan.js"
    				);

    	add_assets($asset);

    	$this->template->set("data",$data);

    	$this->template->set("bank",$data_bank);
    	$this->template->set("konsumen",$data_konsumen);
		$this->template->set("toolbar_title", lang('title_view'));
        $this->template->set("page_title", lang('title_view'));
        $this->template->title(lang('title_view'));
        $this->template->page_icon('fa fa-file');;

        $this->template->render('frm_pelunasan');    	    	

    }

    public function save(){

    	$idkonsumen 		= $this->input->post("konsumen");
		$tgl_catat 			= $this->input->post("tgl_tran");
		$tipe_pelunasan 	= $this->input->post("tipe_bayar");
		$nominal_bayar 		= doubleval(str_replace(",", "", $this->input->post("bayar")));
		$no_bg 				= $this->input->post("no_bg");
		$tgl_est_cair 		= $this->input->post("tgl_cair_bg");
		$tgl_cair 			= $this->input->post("tgl_pencair_bg");
		$bank_asal 			= $this->input->post("nm_bank_asal");
		$bank_nama 			= $this->input->post("an_bank");
		$bank_rek 			= $this->input->post("no_rek_asal");
		$id_bank 			= $this->input->post("bank_tujuan");
		//$st_pil_subnota 	= $this->input->post("pil_subfaktur");
		$no_utama_filter 	= $this->input->post("no_faktur_utama");
		$tgl1_filter 		= $this->input->post("tgl1");
		$tgl2_filter 		= $this->input->post("tgl2");

		/*----- Inputan daftar -------*/

		$checked 			= $this->input->post("checked");
		$dft_idtrans 		= $this->input->post("dft_idtrans");
        $dft_stsubnota		= $this->input->post("dft_stsubnota");
        $dft_idtrans_master = $this->input->post("dft_idtrans_master");
        $dft_gtotal         = $this->input->post("dft_gtotal");
        $dft_tagihan        = $this->input->post("dft_tagihan");
        $dft_bayar          = $this->input->post("dft_bayar");
        $dft_nofaktur       = $this->input->post("dft_nofaktur");

		$this->form_validation->set_rules("konsumen","lang:capt-form-konsumen","callback_cek_pil_dropdown");
		$this->form_validation->set_rules("bayar","lang:capt-form-bayar","required");

		if($_POST["tipe_bayar"] > 2){// hanya berlaku jika memilih bg

			$this->form_validation->set_rules("nm_bank_asal","lang:capt-form-bank-asal","required");			
			$this->form_validation->set_rules("an_bank","lang:capt-form-an-bank","required");			
			$this->form_validation->set_rules("no_rek_asal","lang:capt-form-no-rek","required");			
			$this->form_validation->set_rules("bank_tujuan","lang:capt-form-bank-tujuan","callback_cek_pil_dropdown");

			$this->form_validation->set_rules("no_bg","lang:capt-form-no-bg","required");
			$this->form_validation->set_rules("tgl_cair_bg","lang:capt-form-tgl-cair","required");

		}			

		$this->form_validation->set_message("cek_pil_dropdown", "You should choose the {field} field");

		if($this->form_validation->run() === false){

			$this->set_buat_session(1,"");
            Template::set_message(validation_errors(),'error');
            return false;

		}else{

			/* -- cek kondisi daftar nota terpilih -- */

			if(!isset($_POST['checked'])){ //cek apakah ada nota yang dipilih untuk pelunasan

				//$this->set_buat_session(1,"");
            	Template::set_message(lang("label_simpan_gagal_daftar_kosong"),'error');
            	return false;				

			}

			/* -- Cek apakah jumlah cicilan telah sama dengan bayar -- */

			if(isset($_POST['dft_bayar'])){

				$total_cicilan 	= array_sum($dft_bayar);

				if($total_cicilan < $nominal_bayar){

					$this->set_buat_session(1,"");
            		Template::set_message(lang("label_simpan_gagal_bayar_lebih"),'error');
            		return false;									

				}

			}else{ //tidak ada daftar sama sekali

				$this->set_buat_session(1,"");
            	Template::set_message(lang("label_simpan_gagal_daftar_kosong"),'error');
            	return false;								

			}


		}

		/* simpan data */
		
		$sql_all 	= "";

		$this->db->trans_start();

			/* array pelunasan */

			$idPrimary 	= gen_primary($this->prefixKey, "pelunasan_trans_jual", "id_pelunasan_trans_jual");

			$status_cair 	= 0;

			if($tipe_pelunasan < 3){

				$status_cair 	= 1;

			}else{

				$status_cair 	= strlen($tgl_cair) <= 0  ? 0 : 1;

			}


            if($tipe_pelunasan == 3){

                $tgl_est_cair   = date_ymd($tgl_est_cair);
                $tgl_cair       = strlen($tgl_cair) > 0 ? date_ymd($tgl_cair) : null;

            }else{

                $tgl_est_cair   = null;
                $tgl_cair       = null; 

            }


            // get kode bank default

            if($tipe_pelunasan == 2){

                $bank = $this->bank_model->select('`id_bank`')
                            ->where('deleted = 0 and `st_default` = 1')
                            ->find_all();

                if(count($bank) && is_array($bank)){

                    $id_bank    = $bank[0]->id_bank;

                }else{

                    $id_bank    = null;

                }

            }


            // start simpan data header pelunasan

			$arr_pelunasan 	= array(
									
                                        'id_pelunasan_trans_jual'   => $idPrimary, 
                                        'idkonsumen'               => $idkonsumen,
                                        'st_trans'                  => 1, 
                                        'waktu'                     => date_ymd($tgl_catat).' '.date('H:i:s'), 
                                        'st_bayar'                  => $tipe_pelunasan, 
                                        'no_bg'                     => $no_bg, 
                                        'bank_bg'                   => $bank_asal, 
                                        'an_bank_bg'                => $bank_nama, 
                                        'no_rek_bg'                 => $bank_rek, 
                                        'estimasi_tgl_cair'         => $tgl_est_cair, 
                                        'tgl_cair'                  => $tgl_cair, 
                                        'st_cair'                   => $status_cair,
                                        'id_bank'                   => $tipe_pelunasan > 0 ? null : $id_bank,
                                        'nominal_bayar'             => $nominal_bayar

									);

			$this->pelunasan_model->insert($arr_pelunasan);

            $sql_all    = $this->db->last_query();

            // end simpan data header pelunasan

			// start simpan detail pelunasan

            $arr_bayar          = [];
            $arr_update_trans   = [];
            $faktur_bayar       = '';

            foreach ($checked  as $key => $val) {
                
                $key = $val - 1;                

                $arr_bayar[] =  [
                                    'idbayar_trans_kasir'       => gen_primary('','bayar_trans_kasir','idbayar_trans_kasir'), 
                                    'id_transaksi_master'       => $dft_idtrans_master[$key], 
                                    'id_transaksi'              => $dft_idtrans[$key], 
                                    'id_pelunasan_trans_jual'   => $idPrimary, 
                                    'st_bayar_trans'            => 1, 
                                    'st_bayar'                  => $tipe_pelunasan, 
                                    'id_bank'                   => $id_bank, 
                                    'bayar'                     => $dft_bayar[$key], 
                                    'kurang_bayar'              => $dft_tagihan[$key] - $dft_bayar[$key]
                                ];

                $arr_update_trans[]  =  [
                                            'id_transaksi'  => $dft_idtrans[$key], 
                                            'kurang_bayar'  => $dft_tagihan[$key] - $dft_bayar[$key], 
                                            'st_lunas'      => $dft_tagihan[$key] == $dft_bayar[$key] ? 1 : 0
                                        ];


                if($key > 0){

                    $faktur_bayar .= ",";

                }

                $faktur_bayar .= $dft_nofaktur[$key];
                
            }

            $this->bayar_trans_model->insert_batch($arr_bayar);
            $sql    .= "\n\n". $this->db->last_query();

            $this->trans_model->update_batch($arr_update_trans,'id_transaksi');
            $sql    .= "\n\n". $this->db->last_query();            

            // end simpan detail pelunasan

            $ket_kas    = "Dibayar pelunasan untuk no. Faktur : ". $faktur_bayar;
			
		$this->db->trans_complete();	

		if($this->db->trans_status() == false){

			$return     = false;
            $keterangan = "Gagal tambah data pelunasan transaksi, untuk konsumen : ".$idkonsumen ;
            $status     = 0;


		}else{ // simpan sukses

			/* -- Masukkan Alur Kas -- */

			$tgl_catat_pelunasan 	= strlen($tgl_catat) > 0 ? date_ymd($tgl_catat)." ".date("H:i:s") : '';
			$tgl_pencairan 			= strlen($tgl_cair) > 0 ? date_ymd($tgl_cair)." ".date("H:i:s") : '';


			if($tipe_pelunasan == 0){

				simpan_alurkas($kode_accountKas = 4, $ket = $ket_kas, $total = $nominal_bayar , $status = 1, $nm_hak_akses = $this->addPermission, $model_transaksi = 0, $id_bank = 0, $tgl_proses = $tgl_catat_pelunasan);

			}else if($tipe_pelunasan == 1 || $tipe_pelunasan == 2){

				simpan_alurkas($kode_accountKas = 4, $ket = $ket_kas, $total = $nominal_bayar , $status = 1, $nm_hak_akses = $this->addPermission, $model_transaksi = 1, $id_bank = $id_bank, $tgl_proses = $tgl_catat_pelunasan);				

			}else{

				if(strlen($tgl_cair) > 0){

					simpan_alurkas($kode_accountKas = 4, $ket = $ket_kas, $total = $nominal_bayar , $status = 1, $nm_hak_akses = $this->addPermission, $model_transaksi = 1, $id_bank = $id_bank, $tgl_proses = $tgl_pencairan);				

				}

			}


			$return     = true;
            $keterangan = "SUKSES tambah data pelunasan transaksi, untuk konsumen : ".$idkonsumen ;
            $status     = 1;

            /*if($st_pil_subnota == 1){

            	$this->cek_lunas_nota_utama($arr_idtrans_cek_lunas);

            }*/


		}	

		$nm_hak_akses   = $this->addPermission; 
        $kode_universal = $idkonsumen;
        $jumlah         = 0;

        $sql 			= $sql_all;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        if($return){

            $this->session->unset_userdata("frm_pelunasan_trans");

        }

        return $return;
		
    }

    public function cek_lunas_nota_utama($idtrans_cek = array()){

    	if(count($idtrans_cek)){ //jika array tidak kosong

    		$idtrans 	= array_unique($idtrans_cek);

    		$arr_update 	= array();

    		foreach ($idtrans as $key => $dt) {
    			
    			$st 	= $this->pelunasan_detail_model
								->select("`transaksi`.`grand_total`,
											sum(pelunasan_transaksi_detail.`cicilan`) as total,
										    if(`transaksi`.`grand_total` = sum(pelunasan_transaksi_detail.`cicilan`),1,0) as st")
								->join("transaksi","transaksi.id_transaksi = pelunasan_transaksi_detail.id_transaksi","inner")
								->join("pelunasan_transaksi","pelunasan_transaksi.idpelunasan = pelunasan_transaksi_detail.idpelunasan","inner")
								->where("transaksi.id_transaksi ='{$dt}' and pelunasan_transaksi.deleted = 0 ")
								->find_all();

				echo $this->db->last_query();

				if($st[0]->st == 1){

					$arr_update[] 	= array("id_transaksi" => $dt, "st_lunas" => 1);

				}

    		}

    		$this->db->trans_start();

    			if(is_array($arr_update) && count($arr_update)){
    				
    				$this->trans_model->update_batch($arr_update,'id_transaksi');
    				$sql 	= $this->db->last_query();

    			}

			$this->db->trans_complete();    

			if($this->db->trans_status() == false){

				$return     = false;
	            $keterangan = "GAGAL, update Status lunas transaksi" ;
	            $status     = 0;

			}else{ // simpan sukses

				$return     = true;
	            $keterangan = "SUKSES, update Status lunas transaksi" ;
	            $status     = 1;

			}	

			$nm_hak_akses   = $this->addPermission; 
	        $kode_universal = "-";
	        $jumlah         = 0;

	        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);	    	
	    }
    }

    public function get_nota(){

    	if(!$this->input->is_ajax_request()){
            redirect('pelunasan_trans');
        }

      	$id_konsumen 	= isset($_POST['id_konsumen']) ? $this->input->post('id_konsumen') : '';
      	$id_transaksi 	= isset($_POST['id_trans']) ? $this->input->post('id_trans') : '';
      	$tgl1 			= isset($_POST['tgl1']) ? $this->input->post('tgl1') : '';
      	$tgl2 			= isset($_POST['tgl2']) ? $this->input->post('tgl2') : '';
      	$st_pil 		= isset($_POST['st_pil']) ? $this->input->post('st_pil') : 0;

        $baris          = "";

      	if(strlen($id_konsumen)<=0){

      		$st_ketemu	= 0;

      	}

      	//echo $id_konsumen .", ".$id_transaksi.", ".$tgl1.", ".$tgl2.", ".$st_pil;

      	//exit;

      	if($st_pil == 0){//buka data transaksi

      		$where 			= "`order_produk`.`id_konsumen` = '{$id_konsumen}' 
                                and transaksi.st_kasir = 0
      							and `transaksi`.`deleted` = 0
      							and transaksi.st_lunas = 0 
      							and transaksi.st_sub_nota = 0
                                and transaksi.st_history = 0";

      		if(strlen($tgl1) > 0){

      			$tgl_trans1 	= date_ymd($tgl1)." 01:00:00";
      			$tgl_trans2 	= date_ymd($tgl2)." 23:59:00";

      			$where 	.= " and `transaksi`.`created_on` >= '{$tgl_trans1}' 
      						and `transaksi`.`created_on` <= '{$tgl_trans2}'";

      		}

      		if(strlen($id_transaksi) > 0){

				$where 	= " `transaksi`.`no_faktur` = '{$id_transaksi}' 
							and `order_produk`.`id_konsumen` = '{$id_konsumen}' 
							and `transaksi`.`deleted` = 0
							and transaksi.st_lunas = 0 
							and transaksi.st_sub_nota = 0
                            and transaksi.st_history = 0";

      		}

      		// Start ambil data transaksi yang belum lunas
      		
            $dt_trans   = $this->trans_model
                                ->select("`transaksi`.`id_transaksi`,
                                            DATE_FORMAT(`transaksi`.`waktu`, '%d/%m/%Y %H:%i:%s') AS waktu_tampil,
                                            `transaksi`.`no_faktur`,
                                            `order_produk`.`id_konsumen`,
                                            `transaksi`.`id_trans_ref_asli`,
                                            DATE_FORMAT(`transaksi`.`tgl_tempo`,
                                                    '%d/%m/%Y') AS tempo,
                                            `transaksi`.`grand_total`,
                                            `bayar`.`total_bayar`,
                                            (`transaksi`.`grand_total` - IF(bayar.total_bayar IS NULL,
                                                0,
                                                bayar.total_bayar)) AS kurang_bayar,
                                            (`transaksi`.`grand_total` - (`transaksi`.`grand_total` - IF(bayar.total_bayar IS NULL,
                                                0,
                                                bayar.total_bayar))) AS sudah_bayar")
                                ->join("order_produk","transaksi.id_order = order_produk.id_order","inner")
                                ->join("(SELECT 
                                            `id_transaksi_master` AS id, SUM(`bayar`) AS total_bayar
                                        FROM
                                            bayar_trans_kasir
                                        WHERE
                                            deleted = 0
                                        GROUP BY id_transaksi_master) AS bayar","`transaksi`.`id_trans_ref_asli` = `bayar`.`id`","left")
                                ->where($where)
                                ->order_by('`transaksi`.`waktu`','asc')
                                ->find_all();

            //echo $this->db->last_query();

			if(is_array($dt_trans) && count($dt_trans)){

				$st_ketemu = 1;

				foreach ($dt_trans as $key => $d) {

					$val_cek 		= $key+1;

					$baris 	.= 	"<tr>".
									"<td>".
										"<input type='checkbox' name='checked[]' id='checked' value='{$val_cek}' onchange='cek_bayar(this)' >".
									"</td>".
                                    "<td class='text-center'>{$d->waktu_tampil}</td>".
									"<td class='text-center'>".
										"<input type='hidden' name='dft_idtrans[]'              value='{$d->id_transaksi}'> ".
										"<input type='hidden' name='dft_stsubnota[]'            value='0'> ".
                                        "<input type='hidden' name='dft_idtrans_master[]'       value='{$d->id_trans_ref_asli}'> ".
                                        "<input type='hidden' name='dft_gtotal[]'               value='{$d->grand_total}'> ".
                                        "<input type='hidden' name='dft_tagihan[]'              value='{$d->kurang_bayar}'> ".
                                        "<input type='hidden' name='dft_bayar[]'                value='0'>".
                                        "<input type='hidden' name='dft_nofaktur[]'             value='{$d->no_faktur}'>".
                                        "<input type='hidden' name='dft_waktu_trans[]'          value='{$d->waktu_tampil}'>".
                                        "<input type='hidden' name='dft_jtmp[]'                 value='{$d->tempo}'>".
                                        $d->no_faktur.
									"</td>".
									"<td class='text-center'>{$d->tempo}</td>".
                                    "<td class='text-center'>". number_format($d->grand_total) ."</td>".
									"<td class='text-center'>". number_format($d->kurang_bayar) ."</td>".
									"<td class='text-right'></td>".
								"</tr>";

				}

			}else{

				$st_ketemu = 0;

			}

			$hasil 	= array("st"=>$st_ketemu,"tampil"=>$baris);


      	}else{ //buka data subtransaksi

      		$where 			= "`order_produk`.`id_konsumen` = '{$id_konsumen}' 
      							and `sub_transaksi`.`deleted` = 0
      							and `sub_transaksi`.st_lunas = 0";

			if(strlen($tgl1) > 0){

      			$tgl_trans1 	= date_ymd($tgl1)." 01:00:00";
      			$tgl_trans2 	= date_ymd($tgl2)." 23:59:00";

      			$where 	.= " and `sub_transaksi`.`transaksi`.`created_on` >= '{$tgl_trans1}' 
      						and `sub_transaksi`.`created_on` <= '{$tgl_trans2}'";

      		}

      		if(strlen($id_transaksi) > 0){

				$where 	= " `transaksi`.`no_faktur` = '{$id_transaksi}' 
							and `order_produk`.`id_konsumen` = '{$id_konsumen}' 
							and `sub_transaksi`.`deleted` = 0
							and `sub_transaksi`.st_lunas = 0";

      		}      		

      		//buka data total cicilan
      		$where_cicilan 	= $where." and `pelunasan_transaksi`.deleted = 0";
      		$dt_cicilan 	= $this->pelunasan_detail_model
      									->select("`pelunasan_transaksi_detail`.`idsub_transaksi` as id, 
    												sum(`pelunasan_transaksi_detail`.`cicilan`) as total")
      									->join("sub_transaksi","pelunasan_transaksi_detail.idsub_transaksi = sub_transaksi.`idsub_transaksi`","inner")
      									->join("order_produk","sub_transaksi.id_order = order_produk.id_order","inner")
      									->join("transaksi","transaksi.id_transaksi = sub_transaksi.id_transaksi","inner")
      									->join("pelunasan_transaksi","pelunasan_transaksi.idpelunasan = pelunasan_transaksi_detail.idpelunasan","inner")
										->where($where_cicilan)
										->group_by("`pelunasan_transaksi_detail`.`idsub_transaksi`")
										->find_all();

			//rubah susunan arraynya
			if(is_array($dt_cicilan) && count($dt_cicilan)){

				$arr_total 	= array();

				foreach ($dt_cicilan as $key => $isi) {
					$arr_total[$isi->id] = $isi->total;
				}

			}			

			//buka data transaksi
			$dt_trans 		= $this->subtrans_model
										->select("`sub_transaksi`.`idsub_transaksi`,
												    `sub_transaksi`.`id_transaksi`,
												    `order_produk`.`id_konsumen`,
												    `sub_transaksi`.`no_faktur`,
												    date_format(`sub_transaksi`.`tgl_tempo`,'%d/%m/%Y') as tgl_tempo,
												    `sub_transaksi`.`st_lunas`,
												    `sub_transaksi`.`grand_total`,
												    `sub_transaksi`.`bayar`,
												    (`sub_transaksi`.`grand_total` - `sub_transaksi`.`bayar`) as tagihan")
										->join("order_produk","sub_transaksi.id_order = order_produk.id_order","inner")
										->join("transaksi","transaksi.id_transaksi = sub_transaksi.id_transaksi","inner")
										->where($where)
										->order_by("no_faktur","asc")
										->find_all();

			$st_ketemu 	= 0;
			$baris 		= "";

			if(is_array($dt_trans) && count($dt_trans)){

				$st_ketemu = 1;

				foreach ($dt_trans as $key => $d) {
					
					$total_cicilan 	= isset($arr_total[$d->idsub_transaksi]) ? $arr_total[$d->idsub_transaksi] : 0;

					$sisa 			= $d->tagihan - $total_cicilan;
					$tampil_sisa 	= number_format($sisa);

					$val_cek 		= $key+1;

					$baris 	.= 	"<tr>".
									"<td>".
										"<input type='checkbox' name='checked[]' id='checked' value='{$val_cek}' onchange='cek_bayar(this)'>".
									"</td>".
									"<td class='text-center'>".
										"<input type='hidden' name='dft_idtrans[]' 		value='{$d->id_transaksi}'> ".
										"<input type='hidden' name='dft_sttrans[]' 		value='1'> ".
										"<input type='hidden' name='dft_idsub[]' 		value='{$d->idsub_transaksi}'> ".
										"<input type='hidden' name='dft_stcicilan[]'	value='1'> ".
										"<input type='hidden' name='dft_cicilan[]'		value='0'> ".
										"<input type='hidden' name='dft_sisa[]'			value='{$sisa}'> ".
										"<input type='hidden' name='dft_nofaktur[]'		value='{$d->no_faktur}'>". 
										"<input type='hidden' name='dft_tgl_tempo[]'	value='{$d->tgl_tempo}'>". 
										$d->no_faktur.
									"</td>".
									"<td class='text-center'>{$d->tgl_tempo}</td>".
									"<td class='text-center'>{$tampil_sisa}</td>".
									"<td class='text-center'></td>".
									"<td></td>".
								"</tr>";

				}

			}else{

				$st_ketemu = 0;

			}

			$hasil 	= array("st"=>$st_ketemu,"tampil"=>$baris);

      	}

      	echo json_encode($hasil);

    }

    public function set_buat_session($st_lihat = 0, $id = ""){



    	/*
			Kondisi
				$st_lihat == 0 && $id == "" => Baru
				$st_lihat == 0 && $id == "" => Baru, inputan dari form
				$st_lihat == 1 && $id != "" => Lihat, 

    	*/

		if($st_lihat == 0 && strlen($id)<=0){//baru / create

			$arr_head 	= array(
									"st_lihat"			=> 0,
									"idkonsumen"		=> "",
									"tgl_catat"			=> date("d/m/Y"),
									"tipe_pelunasan"	=> 0,
									"nominal_bayar"		=> "",	
									"no_bg"				=> "",
									"tgl_est_cair"		=> "",
									"tgl_cair"			=> "",
									"bank_asal"			=> "",
									"bank_nama"			=> "",
									"bank_rek"			=> "",
									"id_bank"			=> "",
                                    "sisa_bayar"        => 0,
									"st_pil_subnota"	=> 0,
									"no_utama_filter"	=> "",
									"tgl1_filter"		=> "",
									"tgl2_filter"		=> "",
									"detail"			=> array()
								);

			$this->session->set_userdata("frm_pelunasan_trans",$arr_head);

		}else if($st_lihat == 1 && strlen($id)<=0){// baru / ambil data dari inputan form
			
            $idkonsumen         = $this->input->post("konsumen");
            $tgl_catat          = $this->input->post("tgl_tran");
            $tipe_pelunasan     = $this->input->post("tipe_bayar");
            $nominal_bayar      = doubleval(str_replace(",", "", $this->input->post("bayar")));
            $no_bg              = $this->input->post("no_bg");
            $tgl_est_cair       = $this->input->post("tgl_cair_bg");
            $tgl_cair           = $this->input->post("tgl_pencair_bg");
            $bank_asal          = $this->input->post("nm_bank_asal");
            $bank_nama          = $this->input->post("an_bank");
            $bank_rek           = $this->input->post("no_rek_asal");
            $id_bank            = $this->input->post("bank_tujuan");
            $sisa_bayar         = $this->input->post("sisa_bayar");
            //$st_pil_subnota   = $this->input->post("pil_subfaktur");
            $no_utama_filter    = $this->input->post("no_faktur_utama");
            $tgl1_filter        = $this->input->post("tgl1");
            $tgl2_filter        = $this->input->post("tgl2");

            /*----- Inputan daftar -------*/
            $checked            = isset($_POST['checked']) ? $this->input->post("checked") : [];
            $dft_idtrans        = $this->input->post("dft_idtrans");
            $dft_stsubnota      = $this->input->post("dft_stsubnota");
            $dft_idtrans_master = $this->input->post("dft_idtrans_master");
            $dft_gtotal         = $this->input->post("dft_gtotal");
            $dft_tagihan        = $this->input->post("dft_tagihan");
            $dft_bayar          = $this->input->post("dft_bayar");
            $dft_nofaktur       = $this->input->post("dft_nofaktur");
            $dft_waktu_trans    = $this->input->post("dft_waktu_trans");
            $dft_jtmp           = $this->input->post("dft_jtmp");

            $arr_detail     = [];

            foreach ($dft_idtrans as $key => $val) {
                
                $val_check  = 0;    

                foreach ($checked as $key => $ck) {
                        
                    if($ck == ($key+1)){

                        $val_check = $ck;
                        break;

                    }

                }



                $arr_detail[]   =   [
                                        'checked'               => $val_check,
                                        'dft_idtrans'           => $val,                             
                                        'dft_stsubnota'         => 0,
                                        'dft_idtrans_master'    => $dft_idtrans_master[$key],
                                        'dft_gtotal'            => $dft_gtotal[$key],
                                        'dft_tagihan'           => $dft_tagihan[$key],
                                        'dft_bayar'             => $dft_bayar[$key],
                                        'dft_nofaktur'          => $dft_nofaktur[$key],
                                        'dft_waktu_trans'       => $dft_waktu_trans[$key],
                                        'dft_jtmp'              => $dft_jtmp[$key]
                                    ];

            }


            // header

             $arr_head   =   [
                                "st_lihat"          => 0,
                                "idkonsumen"        => $idkonsumen,
                                "tgl_catat"         => $tgl_catat,
                                "tipe_pelunasan"    => $tipe_pelunasan,
                                "nominal_bayar"     => $nominal_bayar,  
                                "no_bg"             => $no_bg,
                                "tgl_est_cair"      => $tgl_est_cair,
                                "tgl_cair"          => $tgl_cair,
                                "bank_asal"         => $bank_asal,
                                "bank_nama"         => $bank_nama,
                                "bank_rek"          => $bank_rek,
                                "id_bank"           => $id_bank,
                                "sisa_bayar"        => $sisa_bayar,
                                "st_pil_subnota"    => 0,
                                "no_utama_filter"   => "",
                                "tgl1_filter"       => "",
                                "tgl2_filter"       => "",
                                "detail"            => $arr_detail
                            ];

			$this->session->set_userdata("frm_pelunasan_trans",$arr_head);


		}else{// lihat data


			$data    = $this->pelunasan_model
                            ->select("`pelunasan_trans_jual`.`id_pelunasan_trans_jual`,
                                        `pelunasan_trans_jual`.`idkonsumen`,
                                        date_format(`pelunasan_trans_jual`.`waktu`,'%d/%m/%Y') as waktu_catat,
                                        `pelunasan_trans_jual`.`st_bayar`,
                                        `pelunasan_trans_jual`.`no_bg`,
                                        `pelunasan_trans_jual`.`bank_bg`,
                                        `pelunasan_trans_jual`.`an_bank_bg`,
                                        `pelunasan_trans_jual`.`no_rek_bg`,
                                        `pelunasan_trans_jual`.`id_bank`,
                                        `pelunasan_trans_jual`.`nominal_bayar`,
                                        `bayar_trans_kasir`.`idbayar_trans_kasir`,
                                        `bayar_trans_kasir`.`id_transaksi_master`,
                                        `bayar_trans_kasir`.`id_transaksi`,
                                        `bayar_trans_kasir`.`id_pelunasan_trans_jual`,
                                        `bayar_trans_kasir`.`st_bayar`,
                                        `bayar_trans_kasir`.`id_bank`,
                                        `bayar_trans_kasir`.`bayar`,
                                        `bayar_trans_kasir`.`kurang_bayar`,
                                        `transaksi`.`no_faktur`,
                                        date_format(`transaksi`.`waktu`,'%d/%m/%Y %H:%i:%s') as waktu_trans,
                                        date_format(`transaksi`.`tgl_tempo`,'%d/%m/%Y') as tgl_jtmp,
                                        `transaksi`.`grand_total`,
                                        `transaksi`.`kurang_bayar` as kb_trans")
                            ->join("bayar_trans_kasir","pelunasan_trans_jual.id_pelunasan_trans_jual = bayar_trans_kasir.id_pelunasan_trans_jual",'inner')
                            ->join("transaksi","bayar_trans_kasir.id_transaksi = transaksi.id_transaksi",'inner')
                            ->where("pelunasan_trans_jual.deleted = 0 
                                        and `pelunasan_trans_jual`.`id_pelunasan_trans_jual` = '{$id}'")
                            ->order_by("`transaksi`.`waktu`")
                            ->find_all();   

            //detail data

            $arr_detail     = [];

            foreach ($data as $key => $val) {
                
                $arr_detail[]   =   [
                                        'checked'               => 0,
                                        'dft_idtrans'           => $val->id_transaksi,                             
                                        'dft_stsubnota'         => 0,
                                        'dft_idtrans_master'    => $val->id_transaksi_master,
                                        'dft_gtotal'            => $val->grand_total,
                                        'dft_tagihan'           => $val->kb_trans,
                                        'dft_bayar'             => $val->bayar,
                                        'dft_nofaktur'          => $val->no_faktur,
                                        'dft_waktu_trans'       => $val->waktu_trans,
                                        'dft_jtmp'              => $val->tgl_jtmp
                                    ];

            }

            //header 
            
            $arr_head   =   [
                                "st_lihat"          => 1,
                                "idkonsumen"        => $data[0]->idkonsumen,
                                "tgl_catat"         => $data[0]->waktu_catat,
                                "tipe_pelunasan"    => $data[0]->st_bayar,
                                "nominal_bayar"     => $data[0]->nominal_bayar,  
                                "no_bg"             => "",
                                "tgl_est_cair"      => "",
                                "tgl_cair"          => "",
                                "bank_asal"         => $data[0]->bank_bg,
                                "bank_nama"         => $data[0]->an_bank_bg,
                                "bank_rek"          => $data[0]->no_rek_bg,
                                "id_bank"           => $data[0]->id_bank,
                                "sisa_bayar"        => 0,
                                "st_pil_subnota"    => 0,
                                "no_utama_filter"   => "",
                                "tgl1_filter"       => "",
                                "tgl2_filter"       => "",
                                "detail"            => $arr_detail
                            ];



			$this->session->set_userdata("frm_pelunasan_trans",$arr_head);

		}


    }

    public function cancel(){

    	$this->session->unset_userdata("frm_pelunasan_trans");

    	$this->template->set_message(lang("label_cancel_sukses"),"success");

    	redirect("pelunasan_trans");

    }



}