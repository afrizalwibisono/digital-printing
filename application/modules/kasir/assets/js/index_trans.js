$(function(){

	$("#konsumen").select2({
        placeholder :"-- Konsumen --",
        allowClear  : true
  	});

  	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	$("#metode").select2({
        placeholder :"-- Pilih Metode Pembayaran --",
        allowClear  : true
  	});

	$("#status").select2({
        placeholder :"-- Pilih Status Pembayaran --",
        allowClear  : true
  	});  	

  	$("#st_order").select2({
        placeholder :"-- Pilih Status Order --",
        allowClear  : true
  	});  	


  	$("#cetak").click(function(){

  		tombol_cetak();

  	});

  	cek_cetakan_invoice();

  	$("#tampil_rev").click(function(){

  		if($(this).prop("checked") == true ){

  			$("#dft_rev").show(400);
  			
  		}else{

  			$("#dft_rev").hide(400);

  		}

  	});


});

function cek_cetakan_invoice(){

	var id_trans 	= iddata;
	var st_nota 	= stnota;

	if(id_trans.length>0){

		var url 		= baseurl+"kasir/cetak_struk?id_transaksi="+ id_trans+"&st_nota="+st_nota;
		window.open(url, '_blank', 'width=310,height=400,scrollbars=yes,menubar=no,status=yes,resizable=yes,screenx=100,screeny=100'); return false;
	
	}

}

function cetak(obj,e){

	e.preventDefault();

	var id_trans 	= $(obj).closest("tr").find("input[name='dft_idtrans[]']").val();
	var url 		= baseurl+"kasir/cetak_struk?id_transaksi="+ id_trans;

	window.open(url, '_blank', 'width=310,height=400,scrollbars=yes,menubar=no,status=yes,resizable=yes,screenx=100,screeny=100'); return false;

}

function tombol_cetak(){

	var id_trans 	= $("#id_trans_cetak").val();
	var tipe_nota 	= $("#tipe_nota").val();

	var url 		= baseurl+"kasir/cetak_invoice?tipe="+ tipe_nota +"&id_transaksi="+ id_trans;

	if(tipe_nota == 0){

		window.open(url, '_blank', 'width=744,height=600,scrollbars=yes,menubar=no,status=yes,resizable=yes,screenx=100,screeny=100'); return false; 
	
	}else{

		window.open(url, '_blank', 'width=310,height=400,scrollbars=yes,menubar=no,status=yes,resizable=yes,screenx=100,screeny=100'); return false;

	}

}