$(function(){

	$("input[name='cicilan']").number(true,",");
	$("input[name='bayar']").number(true,",");

	$("#metode_bayar").select2({
        placeholder :"-- Pilih Metode Pembayaran --"
  	});	

	$("#bayar_sama").click(function(){

		var nilai_cicilan 	= $("#cicilan").val();

		nilai_cicilan = parseFloat(nilai_cicilan);

		if(nilai_cicilan > 0) {

			$("#bayar").val(nilai_cicilan);

		}

	});

	$("#bayar").keyup(function(){

		var nilai_bayar 	= parseFloat($("#bayar").val());		
		var nilai_cicilan 	= parseFloat($("#cicilan").val());

		var kembalian 		= nilai_bayar >= nilai_cicilan ? nilai_bayar - nilai_cicilan : 0;

		$("#lebih_bayar").val(kembalian);

		$("#kembalian").text($.number(kembalian));

	});

	$("#batal_reset").click(function(){

		$("#cicilan").val("");
		$("#bayar").val("");
		$("#lebih_bayar").val("");
		$("#kembalian").text("");
		$("#metode_bayar").val(0).trigger("change");

	});

	$("#simpan").click(function(){

		simpan_data();

	});


});

function simpan_data(){

	var nilai_cicilan 	= $("#cicilan").val();
	var nilai_bayar 	= $("#bayar").val();
	var metode_bayar 	= $("#metode_bayar").val();
	var id_trans_form 	= idtrans_aktiv;
	var grand_total		= $("#gt").val();
	var no_nota 		= $("#no_faktur").val();

	var kondisi 		= 1;

	if(nilai_cicilan.length <= 0 || nilai_bayar.length <= 0){

		kondisi = 0;

	}else if(parseFloat(nilai_cicilan) <= 0){

		kondisi = 0;

	}else if(parseFloat(nilai_bayar) < parseFloat(nilai_cicilan)){

		kondisi = 0;

	}

	if(kondisi == 0){

		alertify.error("Cek kembali inputan cicilan pembayaran");

	}else{

		var data 			= 	{
									cicilan 		: nilai_cicilan,
									bayar 			: nilai_bayar,
									metode_bayar	: metode_bayar,
									id_transaksi 	: id_trans_form,
									no_faktur 		: no_nota,
									gt 				: grand_total
								};


		$.ajax({
					url 	: baseurl + "kasir/simpan_cicilan",
					type 	: "post",
					data 	: data,
					success : function(msg){

								if(parseFloat(msg) == 1){

									reload_data();
									alertify.success("Simpan data cicilan Pembayaran, Sukses");

								}else{

									alertify.error("Simpan data cicilan Pembayaran, Gagal");

								}

							}

		});

	}

}

function reload_data(){

	var id_trans_form 	= idtrans_aktiv;

	var data 			= {id_trans : id_trans_form};

	$.ajax({

				url 		: baseurl + "kasir/get_load_ulang_cicilan",
				data 		: data,
				dataType 	: "json",
				type 		: "post",
				success 	: function(msg){

								if(msg){

									console.log(msg['sisa']);

									$("#info_sisa_piutang").text(msg['sisa']);

									$("#daftar_cicilan tbody tr").remove();

									$("#daftar_cicilan tbody").append(msg['tabel']);		

									$("#cicilan").val("");
									$("#bayar").val("");
									$("#kembalian").text("");

									$("#metode_bayar").val(0).trigger("change");
									
								}

							}

	});

}

function proses_delete(waktu, idmaster, ket){

	var waktu_mulai 	= waktu;
	var idmaster_trans 	= idmaster;
	var ket_delete 		= ket;

	var data 			= {waktu : waktu_mulai, idmaster : idmaster_trans, ket: ket_delete};

	$.ajax({
				url 	: baseurl + 'kasir/delete_cicilan',
				type 	: "post",
				data 	: data,
				success : function(msg){

							var st 	= parseFloat(msg);

							if(st == 1){

								//location.reload();
								reload_data();
								alertify.success("Hapus data cicilan sukses");							

							}else{

								alertify.error("Delete cicilan bayar Gagal, refresh halaman untuk update terbaru");

							}

						}

	});


}

function del_cicilan(obj){

	var idbayar 	= $(obj).closest("tr").find("input[name='dft_idbayar_trans[]']").val();
	var idmaster 	= $(obj).closest("tr").find("input[name='dft_idmaster_trans[]']").val();
	var waktu 		= $(obj).closest("tr").find("input[name='dft_waktu_trans[]']").val();

	$.ajax({
				url 		: baseurl + 'kasir/get_total_bayar_delete',
				type 		: "post",
				data 		: {waktu : waktu, idmaster : idmaster},
				dataType 	: "json",
				success 	: function(msg){

								if(msg){

									var st 	= parseFloat(msg['status']);

									if(st == 1){

										alertify.prompt(msg['pesan'],"",

											function(evt, value ){

												if(value.length < 10){

													alertify.error('Minimum Keterangan 10 Karakter');

												}else{

													//alertify.success('Ok: ' + value);
													//location.reload();

													proses_delete(waktu, idmaster, value);

												}

												
											},
											function(){
											    alertify.error('Cancel');
											}
										);

									}else{

										alertify.error(msg['pesan']);

									}

								}

							}

	});

}