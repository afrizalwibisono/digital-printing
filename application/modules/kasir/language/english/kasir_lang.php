<?php defined('BASEPATH')|| exit('No direct script access allowed');


// =============================
$lang['title']							= 'Data Transaksi';
$lang['title_new']						= 'Transaksi Baru';
$lang['title_view']						= 'Transaksi';
$lang['title_pelunasan']				= 'Pelunasan Transaksi';
$lang['title_cicilan']					= 'Cicilan Transaksi';

$lang['title_order']					= 'Data Order';

// ======== Form cicilan transaksi =========================================================

$lang['capt-ccl-lbl_resume']			= 'Resume Transaksi';
$lang['capt-ccl-lbl_bayar']				= 'Pembayaran Cicilan';

$lang['capt-ccl-cicilan']				= 'Cicilan';
$lang['capt-ccl-bayar']					= 'Nominal Bayar';
$lang['capt-ccl-metode']				= 'Metode';
$lang['capt-ccl-kembalian']				= 'Kembalian';
$lang['capt-ccl-sisapiutang']			= 'Piutang';
$lang['capt-ccl-lampiran']				= 'Lampiran';


// ------------------ Tabel daftar cicilan --------------------------------------

$lang['capt-ccl-lst-label']				= 'Daftar Cicilan Pembayaran';

$lang['capt-ccl-lst-faktur']			= 'Faktur';
$lang['capt-ccl-lst-waktu']				= 'Waktu Bayar';
$lang['capt-ccl-lst-cicilan']			= 'Cicilan';
$lang['capt-ccl-lst-bayar']				= 'Nominal Bayar';
$lang['capt-ccl-lst-metode']			= 'Metode';
$lang['capt-ccl-lst-kembalian']			= 'Kembalian';
$lang['capt-ccl-lst-piutang']			= 'Kurang Bayar';
$lang['capt-ccl-lst-kasir']				= 'Kasir';
$lang['capt-ccl-lst-lampiran']			= 'Lampiran';

// ======== index trans ====================================================================
$lang['capt-trans-waktu']				= 'Waktu';
$lang['capt-trans-st-ref']				= 'Ref';
$lang['capt-trans-no-faktur']			= 'No. Nota';
$lang['capt-trans-no-order']			= 'No. Order';
$lang['capt-trans-konsumen']			= 'Konsumen';
$lang['capt-trans-total']				= 'Total';
$lang['capt-trans-st-bayar']			= 'Metode';
$lang['capt-trans-nominal-bayar']		= 'Bayar';
$lang['capt-trans-status']				= 'Status';
	
	$lang['capt-label-lunas']			= 'Lunas';
	$lang['capt-label-blm-lunas']		= 'Hutang';

	$lang['capt-label-hutang-ref']		= 'Order-Ref';	

// ======== index trans bukan tabel ====================

$lang['capt-trans-centang-rev']			= 'Data Order - Nota Refisi';
$lang['capt-trans-judul-daftar']		= 'Data Transaksi';

//---------------------- Filter Index --------------------------
$lang['capt-filter-waktu']				= 'Range Tanggal';
$lang['capt-filter-konsumen']			= 'Konsumen';
$lang['capt-filter-metode']				= 'Metode Transaksi';
$lang['capt-filter-waktu-tempo']		= 'Range Tgl Tempo';
$lang['capt-filter-status']				= 'Status Transaksi';
	
	$lang['isi-filter-status-lunas']	= 'Lunas';
	$lang['isi-filter-status-utang']	= 'Hutang';

$lang['capt-filter-no-trans-sub']		= 'No. Nota';

$lang['capt-filter-order-revisi']		= 'Status Order';
	
	$lang['isi-filter-order-revisi']	= 'Revisi';
	$lang['isi-filter-order-bayar']		= 'Pelunasan / Pembayaran';

// ========Index Order =====================
$lang['capt-seacrh-order-no']			= 'No Order';
$lang['capt-seacrh-order-tgl1']			= 'Tanggal Awal';
$lang['capt-seacrh-order-tgl2']			= 'Tanggal Akhir';
// =============================

$lang['capt-order-tabel-noorder']		= 'No Order';
$lang['capt-order-tabel-user']			= 'Atas Nama';
$lang['capt-order-tabel-status']		= 'Status';
$lang['capt-order-tabel-tgl']			= 'Tanggal Order';
$lang['capt-order-tabel-tglselesai']	= 'Request Selesai';
$lang['capt-order-tabel-total']			= 'Total Order';

//==== capt form trans =========

$lang['capt-trans-no-faktur']			= "No. Nota";
$lang['capt-trans-no-faktur-ref']		= "No. Nota Referensi";
$lang['capt-trans-no-order']			= "No. Order";
$lang['capt-trans-konsumen']			= "Konsumen";
$lang['capt-trans-tglorder']			= "Tgl Order";
$lang['capt-trans-tglselesai']			= "Tgl Req. Selesai";
$lang['capt-trans-tgltrans']			= "Tgl Transaksi";
$lang['capt-trans-st-bayar']			= "Pembayaran";

	$lang['isi-st-bayar-kontan']		= "lunas dimuka";
	$lang['isi-st-bayar-dp-last']		= "DP";
	$lang['isi-st-bayar-tempo-dp']		= "Tempo, DP";
	$lang['isi-st-bayar-tempo-nodp']	= "Tempo";
	$lang['isi-st-bayar-kontan-jadi']	= "Dibayar lunas setelah jadi";

$lang['capt-trans-tempo']				= "Jatuh Tempo";
$lang['capt-trans-tempo-hari']			= "Hari";
$lang['capt-trans-metode-bayar']		= "Metode Pembayaran";
	
	$lang['isi-metode-bayar-cash']		= "Cash / Tunai";
	$lang['isi-metode-bayar-debit']		= "Debit";
	$lang['isi-metode-bayar-transfer']	= "Transfer";

$lang['capt-trans-deposit']				= "Deposit Pakai";
$lang['capt-trans-dp']					= "Nilai DP";
$lang['capt-trans-bayar']				= "Nominal Bayar";
$lang['capt-trans-kurang-bayar']		= "Kurang Bayar";
$lang['capt-trans-lebih-bayar']			= "Kembalian";
$lang['capt-trans-st-produksi']			= "Status Produksi";
	
	$lang['isi-st-produksi-tunda']		= "Tunda produksi";
	$lang['isi-st-produksi-masuk']		= "Masuk produksi";

$lang['capt-image-null']				= "Gambar tidak ditemukan";

$lang['capt-trans-cetak-kecil']			= "Cetak Nota Kecil";
$lang['capt-trans-cetak-besar']			= "Cetak Nota Besar";

// ======= Modal revisi ======== 	
$lang['capt-modal-rp-harga']			= "Harga";	
$lang['capt-modal-rp-design']			= "Biaya Design";
$lang['capt-modal-rp-cetak']			= "Biaya Cetak";
$lang['capt-modal-rp-finishing']		= "Biaya Finishing";
$lang['capt-modal-diskon']				= "Diskon";
$lang['capt-modal-potongan']			= "Potongan Tunai";
$lang['capt-modal-total-potongan']		= "Total Potongan";
$lang['capt-tbl-terapkan']				= "Terapkan";

// ======= modal cetak =========
$lang['capt-modal-jns-nota']			= "Jenis Nota";
	
	$lang['isi-jns-nota-besar']			= "Nota Besar";
	$lang['isi-jns-nota-kecil']			= "Nota Kecil";

$lang['capt-print']						= "Print";


// ======= tabel form ==========

$lang['capt-trans-table-deskripsi']		= "Deskripsi";
$lang['capt-trans-table-status']		= "Tgl Selesai";
$lang['capt-trans-table-jml-cetak']		= "Qty";
$lang['capt-trans-table-harga-cetak']	= "Harga";
$lang['capt-trans-table-rp-cetak']		= "Biaya Cetak";
$lang['capt-trans-table-rp-design']		= "Biaya Design";
$lang['capt-trans-table-rp-finishing']	= "Biaya Finishing";
$lang['capt-trans-table-diskon']		= "Diskon";
$lang['capt-trans-table-potongan']		= "Potongan";
$lang['capt-trans-table-subtotal']		= "Sub Total";

$lang['capt-trans-total-all']			= "Total";
$lang['capt-trans-diskon-all']			= "Diskon(%)";
$lang['capt-trans-pot-all']				= "Potongan(Rp.)";
$lang['capt-trans-pajak-all']			= "Pajak(%)";
$lang['capt-trans-grandtotal-all']		= "Grand Total";

//=== capt cetakan nota ===
$lang['capt-nota-no-faktur']			= "No Faktur";
$lang['capt-nota-tgl-trans']			= "Tgl Trans";
$lang['capt-nota-konsumen']				= "Konsumen";
$lang['capt-nota-st-faktur']			= "Status";
$lang['capt-nota-lama-tempo']			= "Lama Tempo";
$lang['capt-nota-tgl-tempo']			= "Tgl Tempo";

$lang['capt-table-nm-file']				= "Nama File";
$lang['capt-table-nm-produk']			= "Produk";
$lang['capt-table-ukuran']				= "Ukuran";
$lang['capt-table-jml']					= "Qty";
$lang['capt-table-total']				= "Total";
$lang['capt-table-potongan']			= "Potongan";
$lang['capt-table-sub-total']			= "Sub Total";

$lang['capt-table-final-total']			= "Total";
$lang['capt-table-final-potongan']		= "Potongan";
$lang['capt-table-final-diskon']		= "Diskon";
$lang['capt-table-final-pajak']			= "Pajak";
$lang['capt-table-final-gt']			= "Grand Total";
$lang['capt-table-final-bayar']			= "Bayar";
$lang['capt-table-final-kurang']		= "Kurang Bayar";
$lang['capt-table-final-kembali']		= "Kembali";

$lang['capt-nota-terbilang']			= "Terbilang";

$lang['capt-table-ttd-hormat']			= "Hormat Kami";
$lang['capt-table-ttd-penerima']		= "Penerima";
$lang['capt-table-ttd-op']				= "Operator / Kasir";

//==============================
$lang['btn_create']						= 'Baru';
$lang['btn_delete']						= 'Hapus';
$lang['btn_filter']						= 'Filter';

// =============================
$lang['btn-save']						= "Simpan";
$lang['btn-batal']						= "Batal / Kembali";
$lang['btn-reset_clear']				= "Batal / Reset";
$lang['btn-delete']						= "Hapus";
$lang['btn-delete-history']				= "Hapus Semua History Transaksi";
$lang['btn-hanya-cetak']				= "Hanya Cetak Nota";
$lang['btn-kembali']					= "Kembali";

$lang['bf_or']							= "or";

// =============================
$lang['label_simpan_sukses'] 			= "Simpan Data Transaksi baru sukses";
$lang['label_simpan_cicilan_sukses'] 	= "Simpan Data Cicilan baru sukses";
$lang['label_cancel_sukses']			= "Data Transaksi telah berhasil dibatalkan";

$lang['label-err-deposit']				= "Sisa Saldo Deposit, tidak sesuai dengan nilai Deposit yang digunakan untuk pembayaran.";

$lang['label-err-trans-hilang']			= "Transaksi sudah tidak ada, cek apakah dihapus atau refisi.";
$lang['label-err-cicilan-simpan']		= "Gagal, simpan transaksi cicilan.";

$lang['label-err-nol-harga-cetak']		= "Harga cetak untuk record ke - %u tidak boleh Nol(0).";
$lang['label-err-bayar-st-0']			= "Bayar tidak boleh kurang dari Grant Total";
$lang['label-err-bayar-st-1-2']			= "Bayar tidak boleh sama / lebih dari Grant Total";
$lang['label-err-saldo-tidak-ada']		= "Anda tidak dapat melakukan transaksi. Cek kembali Saldo Kas Kasir Anda";
$lang['label-err-st-kasir-order']		= "Transaksi Order, sudah diproses Kasir";

$lang['label-err-lampiran']				= "Terjadi kesalahan pada file lampiran.";
$lang['label-err-order-kosong']			= "Tidak ada data ORDER yang akan disimpan transaksinya";
$lang['label-err-bayar-kurang']			= "Bayar tidak boleh kurang dari Kurang Bayar, saat proses pelunasan";
$lang['label-err-status-produksi-batal']= "Status Produksi tidak dapat dibatalkan, karena SPK telah divalidasi";
$lang['label-err-bayar-lunas']			= "Jika DP = 0 atau Deposit > 0, maka Bayar harus lunas";
$lang['label-err-bayar-po-kurang']		= "Untuk Order dengan System PO, pembayaran harus lunas";
// =============================
$lang['konfirmasi-delete']				= "Apakah anda yakin akan menghapus Transaksi terpilih ?";
$lang['konfirmasi-data-tidak-ada']		= "Data tidak ditemukan";
$lang['konfirmasi-error-pil-delete']	= "Tidak ada data Transaksi yang dipilih";

$lang['konfirmasi-err-del-poso']		= "Transaksi tidak dapat dihapus, karena data sudah ada Validasi Cetak untuk Order PO / SO";
$lang['konfirmasi-err-del-stproduksi']	= "Transaksi tidak dapat dihapus, karena data pada list Order telah masuk pada proses Produksi";



$lang['konfirmasi-delete-sukses']		= "Delete Data Transaksi sukses";



