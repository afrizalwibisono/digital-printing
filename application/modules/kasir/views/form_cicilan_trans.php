<?php
		
		$addCicilan 	= has_permission("Cicilan Transaksi Kasir.Add");
		$deleteCicilan 	= has_permission("Cicilan Transaksi Kasir.Delete");


		$idtrans 		= $this->uri->segment(3);

?>


<?= form_open_multipart($this->uri->uri_string(),[	'name'				=> 'frm_cicilan_trans',
													'id'				=> 'frm_cicilan_trans', 
													'role'				=> 'form'
													//'class'				=> 'form-horizontal'
												]
						) ?>

<div class="box box-primary">
	<div class="box-body">
		
		<div id="area-resume-tagihan">

			<div class="row">
				<div class="col-sm-3" style="margin-bottom: 8px">
					<a href="<?= site_url("kasir") ?>" class="btn btn-primary">
						<span class="fa fa-long-arrow-left"> <?= lang("btn-kembali") ?></span>
					</a>
				</div>	
			</div>

			<div class="row">

				<div class="col-sm-3">
					<div class="form-group">
						<label><?= lang("capt-trans-no-faktur") ?></label>
						<input type="hidden" name="no_faktur" id="no_faktur" value="<?= $dt_trans->no_faktur ?>">
						<span class="form-control input-sm"><?= $dt_trans->no_faktur ?></span>
					</div>
				</div>

				<div class="col-sm-3">
					<div class="form-group">
						<label for='no_faktur'><?= lang("capt-trans-no-faktur-ref") ?></label>
						<span class="form-control input-sm">
							<?php

								if($dt_trans->st_trans_master == 0){

									echo $dt_trans->no_faktur_master;

								}

							?>
						</span>
					</div>
				</div>
			</div><!-- end row -->

			<div class="row">
			
				<div class="col-sm-3">
					<div class="form-group">
						<label><?= lang("capt-trans-tgltrans") ?></label>
						<span class="form-control input-sm">
							<?= $dt_trans->tgl ?>
						</span>
					</div>
				</div>

				<div class="col-sm-3">

					<div class="form-group">
						<label><?= lang("capt-trans-konsumen") ?></label>
						<div class="input-group">
							<span class="form-control input-sm">
								<?= $dt_trans->nama ?>
							</span>	
							<span class="input-group-addon input-sm">

								<?= $dt_trans->st_gol ?>
								
							</span>	
						</div>
					</div>
				</div>

				<div class="col-sm-3">
					<div class="form-group">
						<label><?= lang("capt-trans-grandtotal-all") ?></label>
						<input type="hidden" id="gt" name="gt" value="<?= $dt_trans->grand_total ?>">
						<span class="form-control input-sm text-right">
							<strong><?= number_format($dt_trans->grand_total) ?></strong>
						</span>
					</div>				
				</div>		

				<div class="col-sm-3">
					<div class="form-group">
						<label><?= lang("capt-ccl-sisapiutang") ?></label>
						<span class="form-control input-sm text-right">
							<strong id="info_sisa_piutang"><?= number_format($dt_trans->sisa_piutang) ?></strong>
						</span>
					</div>				
				</div>		

			</div><!-- end row -->
			
		</div>

		<?php

			$hide_bayar 	= "";

			if(isset($saldo_kosong) || !$addCicilan ){

				$hide_bayar = "display : none";

			}

		?>

		<div id="area-bayar" style="<?= $hide_bayar ?>">
			
			<div class="label label-success">
				<?= lang("capt-ccl-lbl_bayar") ?>
			</div>			
			<hr style="margin-top: 5px !important; margin-bottom: 5px !important">
			<div class="row">
				
				<div class="col-sm-3">
					<div class="form-group">
						<label class="control-label">
							<?= lang("capt-ccl-cicilan") ?>
						</label>
						<input type="text" name="cicilan" id="cicilan" class="form-control text-right">
					</div>
				</div>

				<div class="col-sm-3">
					<div class="form-group">	
						<label class="control-label">
							<?= lang("capt-ccl-bayar") ?>
						</label>
						<div class="input-group">
							<div class="input-group-btn">
								<button type="button" class="btn btn-primary" id="bayar_sama">
									<span class="fa fa-exchange"></span>
								</button>
							</div>
							<input type="text" name="bayar" id="bayar" class="form-control text-right">	
						</div>
						
					</div>
				</div>

				<div class="col-sm-3" id="tampil_kembalian">
					<div class="form-group">
						<label class="control-label">
							<?= lang("capt-ccl-kembalian") ?>
						</label>
						<input type="hidden" name="lebih_bayar" id="lebih_bayar">
						<span id="kembalian" class="form-control text-right"></span>
					</div>
				</div>

				<div class="col-sm-3">
					<div class="form-group">
						<label class="control-label">
							<?= lang("capt-ccl-metode") ?>
						</label>
						<select name="metode_bayar" id="metode_bayar" class="form-control">
							<option value="0" selected><?= lang("isi-metode-bayar-cash") ?></option>
							<option value="1" ><?= lang("isi-metode-bayar-debit") ?></option>
							<option value="2" ><?= lang("isi-metode-bayar-transfer") ?></option>
						</select>
					</div>
				</div>

			</div>

			<div class="row">
			
				<div class="col-sm-12">
					
					<div class="pull-right">
						
						<a href="#reset_inputan" id = "batal_reset" name= "batal_reset" ><span><?= lang('btn-reset_clear') ?></span></a>
						<?= lang("bf_or") ?>
						<button type="button" name="simpan" id="simpan" class="btn btn-primary">
		    				<?= lang("btn-save") ?>
		    			</button>

					</div>

				</div>

			</div>

		</div>

		


		<div class="area-list-cicilan">
		
			<div class="label label-warning">
				<?= lang("capt-ccl-lst-label") ?>
			</div>
			<div class="table-responsive">

				<table class="table table-striped table-hover" id="daftar_cicilan">
					<thead>
						<tr>
							<th class="text-center" width="30">No.</th>
							<th class="text-center"><?= lang('capt-ccl-lst-waktu') ?></th>
							<th class="text-center"><?= lang('capt-ccl-lst-faktur') ?></th>
							<th class="text-center"><?= lang('capt-ccl-lst-cicilan') ?></th>
							<th class="text-center"><?= lang('capt-ccl-lst-metode') ?></th>
							<th class="text-center"><?= lang('capt-ccl-lst-kasir') ?></th>
							<th width="60"></th>
						</tr>
					</thead>

					<tbody>

						<?php 

							if(isset($dt_bayar) && is_array($dt_bayar) && count($dt_bayar)):
								foreach ($dt_bayar as $key => $isi) :
						?>

						<tr>
							<td>
								<?= $key+1 ?>
								<input type="hidden" name="dft_idbayar_trans[]" id="dft_idbayar_trans" value="<?= $isi->idbayar_trans_kasir ?>">	
								<input type="hidden" name="dft_idmaster_trans[]" id="dft_idmaster_trans" value="<?= $isi->id_transaksi_master ?>">	
								<input type="hidden" name="dft_waktu_trans[]" id="dft_waktu_trans" value="<?= $isi->created_on ?>">	
							</td>							
							<td class="text-center"><?= $isi->tgl ?></td>
							<td class="text-center"><?= $isi->no_faktur ?></td>
							<td class="text-center"><?= number_format($isi->bayar) ?></td>
							<td class="text-center">
								<?php 
									switch ($isi->st_bayar) {
										case 0:
											$metode 	= "<span class='label label-success'>Cash / Tunai</span>";	
											break;
										case 1:
											$metode 	= "<span class='label label-warning'>Debit</span>";	
											break;
										case 2:
											$metode 	= "<span class='label label-primary'>Transfer</span>";	
											break;
									}

									echo $metode;
								?>
							</td>
							<td class="text-center"><?= $isi->nm_lengkap ?></td>
							<td class="text-center">
								<?php 
										if($deleteCicilan):
								?>
								<a href="#del" onclick="del_cicilan(this);" title="Delete Cicilan"><span class="text-danger fa fa-close"></span></a>
								<?php
										endif;
								?>
							</td>

						</tr>

						<?php
								endforeach;
							endif;
						?>
					</tbody>

				</table>
				
			</div>
			
		</div>

	</div><!-- end box-body -->
</div><!-- end box box-primary -->


<?= form_close() ?>

<script type="text/javascript">
	
	var idtrans_aktiv 	= "<?= $idtrans ?>";

</script>