<?php
	
	$ttl_design 	= 0;
	$ttl_finishing 	= 0;
	$ttl_potongan 	= 0;
	$ttl_trans		= 0;

?>

<!-- header struk -->
<table class="struk-kecil">
	<thead>
		<tr>
			<th colspan="3">
				<!-- <img src="<?php //base_url('assets/images/logo_200px.png') ?>"> -->
				VALIBRI
			</th>
		</tr>
		<tr>
			<th colspan="3"><?= $idt->alamat ?>, <?= $idt->kota ?></th>
		</tr>
		<tr>
			<th colspan="3">Telp. <?= $idt->no_telp ?></th>
		</tr>
		<tr class="no-tran"></tr>
		<tr class="header-info">
			<th>No. Tran</th>
			<th>:</th>
			<th>Struk Sementara</th>
		</tr>
		<tr class="header-info">
			<th>Waktu</th>
			<th>:</th>
			<th><?= $data['waktu'] ?></th>
		</tr>
		<tr class="header-info">
			<th>Konsumen</th>
			<th>:</th>
			<th><?= $data['nama_konsumen'] ?></th>
		</tr>
		<tr class="header-info">
			<th>Telp</th>
			<th>:</th>
			<th><?= $data['telp_konsumen'] ?></th>
		</tr>
		<tr class="no-tran"></tr>
	</thead>
</table>

<!-- isi dari struk -->
<table class="struk-kecil">
	<thead>
		<tr>
			<td>Qty</td>
			<td>Product</td>
			<td>Total</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($data['detail'] as $key => $isi): ?>

		<tr>
			<td colspan="3">
				<b>
				<?php 
					$jml 		= $isi['jml_cetak']."x";
					$jml 		= str_pad($jml,8,'~');
					$jml 		= str_replace("~", "&nbsp;", $jml);
					
					echo $jml.ucwords(substr($isi['nm_pekerjaan'], 0, 30));
				?>
				</b>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				[ <?= $isi['tampil_harga'] ?> ]
			</td>
			<td>
				<?php

					$biaya_cetak_tampil = $isi['biaya_cetak'] + $isi['potongan'];
					echo number_format($biaya_cetak_tampil);

					//total semua variable penambahan
					$ttl_design 	+= $isi['biaya_design'];
					$ttl_finishing 	+= $isi['biaya_finishing'];
					$ttl_potongan 	+= $isi['potongan'];
					$ttl_trans 		+= $biaya_cetak_tampil;

				?>
			</td>
		</tr>

		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<?php if($ttl_potongan > 0 || $ttl_finishing > 0 || $ttl_potongan > 0 ): ?>
		<tr>
			<td colspan="2">Total Cetak <span>:</span></td>
			<td><?= number_format($ttl_trans) ?></td>
		</tr>
		<?php endif; ?>

		<?php if($ttl_design > 0): ?>
		<tr>
			<td colspan="2">Total Design <span>:</span></td>
			<td><?= number_format($ttl_design) ?></td>
		</tr>
		<?php endif ?>		

		<?php if($ttl_finishing > 0): ?>
		<tr>
			<td colspan="2">Total Finishing <span>:</span></td>
			<td><?= number_format($ttl_finishing) ?></td>
		</tr>
		<?php endif ?>

		<?php if($ttl_potongan > 0): ?>
		<tr>
			<td colspan="2">Total Potongan <span>:</span></td>
			<td><?= "(".number_format($ttl_potongan).")" ?></td>
		</tr>
		<?php endif ?>		

		<tr>
			<td colspan="2">Grand Total <span>:</span></td>
			<td><?= number_format($data['grand_total']) ?></td>
		</tr>
		<tr>
			<td colspan="2">Tempo <span>:</span></td>
			<td>HUTANG</td>
		</tr>		
		<tr>
			<td colspan="3" class="footer-note">===== Terima Kasih =====</td>
		</tr>
		<tr>
			<td colspan="3" class="footer-warning">
				Perhatian:<br>
				Struk ini bukan NOTA / bukti pembayaran.
			</td>
		</tr>
		<tr>
			<td colspan="3" class="footer-warning">
				Bukan Bukti Pengambilan Order
			</td>
		</tr>
	</tfoot>


</table>

<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function(e){
		window.print();
		//window.close();
	});
</script>