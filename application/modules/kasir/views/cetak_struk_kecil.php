<?php
	
	$ttl_design 	= 0;
	$ttl_finishing 	= 0;
	$ttl_potongan 	= 0;
	$ttl_trans		= 0;

?>

<!-- header struk -->
<table class="struk-kecil">
	<thead>
		<tr>
			<th colspan="3">
				<img src="<?= base_url('assets/images/report/report.png') ?>">
			</th>
		</tr>
		<tr>
			<th colspan="3"><?= $idt->alamat ?>, <?= $idt->kota ?></th>
		</tr>
		<tr>
			<th colspan="3">Telp. <?= $idt->no_telp ?></th>
		</tr>
		<tr class="no-tran"></tr>
		<tr class="header-info">
			<th>No. Tran</th>
			<th>:</th>
			<th><?= $data[0]->no_faktur ?></th>
		</tr>
		<?php if($data[0]->st_trans_master == 0) : ?> 
			<!-- jika nota bukan nota master -->
		<tr class="header-info">
			<th>No. Ref</th>
			<th>:</th>
			<th><?= $data[0]->no_faktur_master ?></th>
		</tr>
		<?php endif; ?>
		<tr class="header-info">
			<th>Waktu</th>
			<th>:</th>
			<th><?= $data[0]->waktu ?></th>
		</tr>
		<tr class="header-info">
			<th>Konsumen</th>
			<th>:</th>
			<th><?= $data[0]->nama ?></th>
		</tr>
		<tr class="header-info">
			<th>Telp</th>
			<th>:</th>
			<th><?= $data[0]->no_tlp ?></th>
		</tr>
		<tr class="no-tran"></tr>
	</thead>
</table>

<!-- isi dari struk -->
<table class="struk-kecil">
	<thead>
		<tr>
			<td>Qty</td>
			<td>Product</td>
			<td>Total</td>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($data as $key => $isi): ?>

		<tr>
			<td colspan="3">
				<b>
				<?php 
					$jml 		= $isi->jml_cetak."x";
					$jml 		= str_pad($jml,8,'~');
					$jml 		= str_replace("~", "&nbsp;", $jml);
					$tampil_uk 	= "(".number_format($isi->p)." x ".number_format($isi->l)." ".$isi->tampil2.")";

					//echo $jml.$isi->nm_produk.$tampil_uk;
					echo $jml.ucwords(substr($isi->nama_pekerjaan, 0, 30));
				?>
				</b>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				[ <?= number_format($isi->harga_by_satuan,0) ?> / <?= $isi->satuan_harga_kecil ?> ]
				<?php 

					if($isi->potongan > 0) : 

						$potongan_tampil 	= number_format($isi->potongan);

				?> 
					<span style="text-align: right !important;float: right !important;">
						<strong>- <?= $potongan_tampil ?></strong>
					</span>
				<?php endif; ?>
			</td>
			<td>

				<?php
					//berlaku jika total potongan semuanya diletakkan dibawah
					//$biaya_cetak_tampil = $isi->biaya_cetak + $isi->potongan; 
					$biaya_cetak_tampil = $isi->biaya_cetak;
					echo number_format($biaya_cetak_tampil);

					//total semua variable penambahan
					$ttl_design 	+= $isi->biaya_design;
					$ttl_finishing 	+= $isi->biaya_finishing;
					$ttl_potongan 	+= $isi->potongan;
					$ttl_trans 		+= $biaya_cetak_tampil;

				?>
			</td>
		</tr>

		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<?php if($ttl_potongan > 0 || $ttl_finishing > 0 || $ttl_potongan > 0 ): ?>
		<tr>
			<td colspan="2">Total Cetak <span>:</span></td>
			<td><?= number_format($ttl_trans) ?></td>
		</tr>
		<?php endif; ?>

		<?php if($ttl_design > 0): ?>
		<tr>
			<td colspan="2">Total Design <span>:</span></td>
			<td><?= number_format($ttl_design) ?></td>
		</tr>
		<?php endif ?>		

		<?php if($ttl_finishing > 0): ?>
		<tr>
			<td colspan="2">Total Finishing <span>:</span></td>
			<td><?= number_format($ttl_finishing) ?></td>
		</tr>
		<?php endif ?>

		<!-- <?php if($ttl_potongan > 0): ?>
		<tr>
			<td colspan="2">Total Potongan <span>:</span></td>
			<td><?= "(".number_format($ttl_potongan).")" ?></td>
		</tr>
		<?php endif ?>		 -->

		<tr>
			<td colspan="2">Grand Total <span>:</span></td>
			<td><?= number_format($isi->grand_total) ?></td>
		</tr>

		<?php if($dp > 0) : ?>
		<tr>
			<td colspan="2">DP <span>:</span></td>
			<td><?= number_format($dp) ?></td>
		</tr>
		<?php endif; ?>
		
		<?php if($bayar > 0): ?>
		<tr>
			<td colspan="2">Bayar <span>:</span></td>
			<td><?= number_format($bayar) ?></td>
		</tr>
		<?php endif; ?>

		<tr>
			<td colspan="2">Krg. Bayar <span>:</span></td>
			<td><?= number_format($krg_bayar) ?></td>
		</tr>
		<tr>
			<td colspan="2">Tempo <span>:</span></td>
			<td>
				<?php 
						if($isi->st_lunas == 1) {

							echo "Lunas";

						}else{

							echo $isi->tgl_tempo;

						}

				?>
			</td>
		</tr>		
		<tr>
			<td colspan="2">Kembali <span>:</span></td>
			<td><?= number_format($kembalian) ?></td>
		</tr>
		<tr>
			<td colspan="3">&nbsp</td>
		</tr>
		<tr class="no-tran"></tr>
		<tr>
			<td colspan="3" class="deposit-info" >
				Penggunaan deposit pada trans ini :
				<strong><?= "Rp. ".number_format($deposit) ?></strong>
				<br>
				Saldo deposit anda saat ini :
				<strong><?= "Rp. ".number_format($sisa_saldo) ?></strong>
			</td>
		</tr>
		<tr class="no-tran"></tr>
		<tr>
			<td colspan="3">&nbsp</td>
		</tr>
		<tr>
			<td colspan="3" class="footer-note">===== Terima Kasih =====</td>
		</tr>

	</tfoot>
</table>

<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function(e){
		window.print();
		//window.close();
	});
</script>