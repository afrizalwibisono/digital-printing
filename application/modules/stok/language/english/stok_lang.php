<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ============================
$lang['stok_title_manage']		= 'Data Stok Barang';
$lang['list_konversi_title_manage']	= 'List Konversi Satuan';
$lang['pilih_tipe']				= 'Pilih Tipe Barang';
$lang['pilih_kategori']			= 'Pilih Jenis Barang';
$lang['lap_hpp']				= 'Laporan Riwayat HPP';

// from/table
$lang['barcode']				= 'Barcode';
$lang['nm_barang']				= 'Nama Barang';
$lang['ket']					= 'Deskripsi';
$lang['merk']					= 'Merk Barang';
$lang['jenis']					= 'Jenis Barang';
$lang['kategori']				= 'Kategori';
$lang['id_satuan_terkecil']		= 'Satuan Terkecil';
$lang['satuan'] 				= "Satuan";
$lang['satuan_baru'] 			= "Satuan Baru";
$lang['stok'] 					= "Stok";
$lang['min_stok'] 				= "Stok Minimum";
$lang['gudang'] 				= "Gudang";
$lang['cabang'] 				= "Cabang";
$lang['dimensi'] 				= "Dimensi (cm)";
$lang['berat'] 					= "Berat (kg)";
$lang['panjang'] 				= "Panjang";
$lang['lebar'] 					= "Lebar";
$lang['tinggi'] 				= "Tinggi";
$lang['tgl_awal'] 				= "Tgl. Awal";
$lang['tgl_akhir'] 				= "Tgl. Akhir";
$lang['nm_supplier'] 			= "Nama Supplier";
$lang['hpp'] 					= "HPP";
$lang['tgl'] 					= "Tanggal";
$lang['range_tgl'] 				= 'Range Tanggal';


//button
$lang['stok_btn_new']		= 'Baru';
$lang['stok_btn_delete']	= 'Hapus';
$lang['stok_btn_save']		= 'Simpan';
$lang['stok_btn_cancel']	= 'Batal';
$lang['stok_btn_pilih']		= 'Pilih';
$lang['stok_or']			= 'atau';
$lang['cetak']				= 'Cetak';


//messages
$lang['stok_del_error']			= 'Anda belum memilih data barang yang akan dihapus.';
$lang['stok_del_failure']		= 'Tidak dapat menghapus data barang';
$lang['stok_delete_confirm']	= 'Apakah anda yakin akan menghapus data barang terpilih?';
$lang['stok_deleted']			= 'Data barang berhasil dihaspus';
$lang['stok_no_records_found']  = 'Data tidak ditemukan.';

$lang['stok_create_failure']	= 'Data barang baru gagal disimpan';
$lang['stok_create_success']	= 'Data barang baru berhasil disimpan';

$lang['stok_edit_success']		= 'Data barang berhasil disimpan';
$lang['stok_invalid_id']		= 'ID tidak Valid';
$lang['stok_only_ajax']			= 'Hanya ajax request yang diijinkan';

