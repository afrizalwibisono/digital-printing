<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 * 
 * This is controller for Stok

 */

class Stok extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Stok.View";
    protected $laporanHPP       = "Laporan HPP.View";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load(array('stok/stok'));
        $this->load->model(array('bahan_baku/bahan_baku_model',
                                'jenis/jenis_model',
                                'konversi_satuan/satuan_terkecil_model',
                                'konversi_satuan/konversi_satuan_model',
                                'merk/merk_model',
                                'kategori/kategori_model',
                                'stok_model',
                                'gudang/gudang_model'
                                ));

        $this->template->title(lang('stok_title_manage'));
		$this->template->page_icon('fa fa-cubes');
    }

    public function index($stok = NULL)
    {
        $this->auth->restrict($this->viewPermission);

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search     = isset($_POST['table_search'])?$this->input->post('table_search'):'';
            $id_jenis   = $this->input->post('id_jenis');
            $id_kategori= $this->input->post('id_kategori');
            $id_merek   = $this->input->post('id_merek');
            $id_gudang  = $this->input->post('id_gudang');
        }
        else
        {
            $search     = $this->input->get('search');
            $id_jenis   = $this->input->get('jenis');
            $id_kategori= $this->input->get('kategori');
            $id_merek   = $this->input->get('merek');
            $id_gudang  = $this->input->get('gudang');
        }

        $filter = "?search=".$search;

        $search2 = $this->db->escape_str($search);

        $addWhere = "";
        if($id_jenis !="")
        {
            $addWhere .= "AND barang.idjenis_bb=".urldecode($id_jenis)." ";
            $filter   .= "&jenis=".urlencode($id_jenis);
        }
        if($id_kategori !="")
        {
            $addWhere .= "AND barang.idkategori=".urldecode($id_kategori)." ";
            $filter   .= "&kategori=".urlencode($id_kategori);
        }
        if($id_merek !="")
        {
            $addWhere .= "AND barang.idmerk_bb=".urldecode($id_merek)." ";
            $filter   .= "&merek=".urlencode($id_merek);
        }
        if($id_gudang !="")
        {
            $addWhere .= "AND stok.id_gudang=".urldecode($id_gudang)." ";
            $filter   .= "&gudang=".urlencode($id_gudang);
        }

        $where="barang.deleted = 0
                    $addWhere
                    AND (`nm_barang` LIKE '%$search2%' ESCAPE '!'
                    OR `barcode` LIKE '%$search2%' ESCAPE '!' 
                    OR `nmjenis_bb` LIKE '%$search2%' ESCAPE '!'
                    OR `nmkategori` LIKE '%$search2%' ESCAPE '!'
                    OR `nm_merk_bb` LIKE '%$search2%' ESCAPE '!'
                    OR `ket_barang` LIKE '%$search2%' ESCAPE '!'
                   )";
        
        $total = $this->bahan_baku_model
                    ->select(array("barang.*","jenis_bb.nmjenis_bb","merk_bb.nm_merk_bb","nmkategori","satuan_terkecil.alias as satuan_terkecil","nama_gudang","SUM(IF(stok.deleted = 0, stok_update, 0)) AS stok"))
                    ->join("jenis_bb","barang.idjenis_bb = jenis_bb.idjenis_bb","left")
                    ->join("kategori","barang.idkategori = kategori.idkategori","left")
                    ->join("satuan_terkecil","barang.id_satuan_terkecil=satuan_terkecil.id_satuan_terkecil","left")
                    ->join("merk_bb","barang.idmerk_bb = merk_bb.idmerk_bb","left")
                    ->join("stok","barang.idbarang_bb = stok.id_barang","left")
                    ->join("gudang","stok.id_gudang = gudang.id_gudang","left")
                    ->where($where)
                    ->group_by(array("barang.idbarang_bb","stok.id_gudang"))
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->bahan_baku_model
                    ->select(array("barang.*","jenis_bb.nmjenis_bb","kategori.nmkategori","merk_bb.nm_merk_bb","satuan_terkecil.alias as satuan_terkecil","nama_gudang","SUM(IF(stok.deleted = 0, stok_update, 0)) AS stok"))
                    ->join("jenis_bb","barang.idjenis_bb = jenis_bb.idjenis_bb","left")
                    ->join("kategori","barang.idkategori = kategori.idkategori","left")
                    ->join("satuan_terkecil","barang.id_satuan_terkecil=satuan_terkecil.id_satuan_terkecil","left")
                    ->join("merk_bb","barang.idmerk_bb=merk_bb.idmerk_bb","left")
                    ->join("stok","barang.idbarang_bb = stok.id_barang","left")
                    ->join("gudang","stok.id_gudang = gudang.id_gudang","left")
                    ->where($where)
                    ->group_by(array("barang.idbarang_bb","stok.id_gudang"))
                    ->order_by('nm_barang','ASC')
                    ->limit($limit, $offset)
                    ->find_all();

        if($data)
        {
            foreach ($data as $key => $dt) {
                $data[$key]->opt_select = get_konversi_select($dt->id_satuan_terkecil,2);
                $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
            }
        }

        $jenis_bb   = $this->jenis_model->order_by('nmjenis_bb','ASC')->find_all_by('deleted', 0);
        $merek      = $this->merk_model->order_by('nm_merk_bb', 'ASC')->find_all_by('deleted', 0);
        $kategori   = $this->kategori_model->order_by('nmkategori', 'ASC')->find_all_by('deleted', 0);
        $gudang     = $this->gudang_model->order_by('nama_gudang', 'ASC')->find_all_by('deleted', 0);
        
        $assets = array(
                    'plugins/select2/dist/css/select2.min.css',
                    'plugins/select2/dist/js/select2.min.js',
                    'plugins/number/jquery.number.js',
                    'stok/assets/js/stok_index.js'
                    );

        add_assets($assets);

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('jenis', $jenis_bb);
        $this->template->set('merek', $merek);
        $this->template->set('kategori', $kategori);
        $this->template->set('gudang', $gudang);
        $this->template->set('id_jenis', $id_jenis);
        $this->template->set('idkategori', $id_kategori);
        $this->template->set('id_merek', $id_merek);
        $this->template->set('id_gudang', $id_gudang);

        $this->template->title(lang('stok_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

    public function laporan_hpp(){

        $this->auth->restrict($this->laporanHPP);

        $this->form_validation->set_rules('tgl_awal','lang:tgl_awal','trim');
        $this->form_validation->set_rules('tgl_akhir','lang:tgl_akhir','trim');

        if($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
        }

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
            $tgl_awal       = $this->input->post('tgl_awal') ? $this->input->post('tgl_awal') : date('01/m/Y');
            $tgl_akhir      = $this->input->post('tgl_akhir') ? $this->input->post('tgl_akhir') : date('t/m/Y');
        }else{
            $search = isset($_GET['search'])?$this->input->get('search'):'';
            $tgl_awal       = $this->input->get('tgl_awal') ? $this->input->get('tgl_awal') : date('01/m/Y');
            $tgl_akhir      = $this->input->get('tgl_akhir') ? $this->input->get('tgl_akhir') : date('t/m/Y');
        }

        $search2 = $this->db->escape_str($search);
       
        $filter = "?search=".urlencode($search)."&tgl_awal=".urlencode($tgl_awal)."&tgl_akhir=".urlencode($tgl_akhir);

        $where = "`stok.deleted` = 0 ";

        if($tgl_awal !="" && $tgl_akhir !="")
        {
            $tgl1 = date_ymd($tgl_awal);
            $tgl2 = date_ymd($tgl_akhir);

            $where .= " AND (stok.created_on >='$tgl1' AND stok.created_on <= '$tgl2')";
        }

        if($search!="")
        {
            $where .= "AND (`nm_barang` like '%$search2%' OR `nm_supplier` like '%$search2%')";
        }

        $data = $this->bahan_baku_model
                    ->select(array("barang.idbarang_bb","nm_barang","jenis_bb.nmjenis_bb","kategori.nmkategori","merk_bb.nm_merk_bb","kedatangan_barang.id_kedatangan","supplier.nm_supplier","stok.hpp","stok.created_on"))
                    ->join("jenis_bb","barang.idjenis_bb = jenis_bb.idjenis_bb","left")
                    ->join("kategori","barang.idkategori = kategori.idkategori","left")
                    ->join("merk_bb","barang.idmerk_bb=merk_bb.idmerk_bb","left")
                    ->join("stok","barang.idbarang_bb = stok.id_barang","left")
                    ->join("kedatangan_barang","stok.no_faktur=kedatangan_barang.id_kedatangan")
                    ->join("supplier","kedatangan_barang.id_supplier=supplier.idsupplier")
                    ->where($where)
                    ->order_by('stok.created_on','DESC')
                    ->order_by('nm_barang','ASC')
                    ->find_all();

        $return = [];
        if($data){
            foreach ($data as $key => $dt) {
                $det = new stdClass();

                if(isset($return[$dt->idbarang_bb])){
                    $det->id_kedatangan = $dt->id_kedatangan;
                    $det->nm_supplier   = $dt->nm_supplier;
                    $det->hpp        = $dt->hpp;
                    $det->created_on = $dt->created_on;

                    $return[$dt->idbarang_bb]->detail[] = $det;
                }else{
                    $return[$dt->idbarang_bb] = new stdClass();
                    $return[$dt->idbarang_bb]->idbarang_bb = $dt->idbarang_bb;
                    $return[$dt->idbarang_bb]->nm_barang  = $dt->nm_barang;
                    $return[$dt->idbarang_bb]->nmjenis_bb = $dt->nmjenis_bb;
                    $return[$dt->idbarang_bb]->nmkategori = $dt->nmkategori;
                    $return[$dt->idbarang_bb]->nm_merk_bb = $dt->nm_merk_bb;

                    $det->id_kedatangan = $dt->id_kedatangan;
                    $det->nm_supplier   = $dt->nm_supplier;
                    $det->hpp        = $dt->hpp;
                    $det->created_on = $dt->created_on;
                    if(intval($dt->hpp) > 0){
                        $return[$dt->idbarang_bb]->detail[] = $det;    
                    }else{
                        $return[$dt->idbarang_bb]->detail = [];    
                    }
                }
            }
        }

        //Identitas
        $identitas = $this->identitas_model->find(1);

        $assets = ['plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                    'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                    'plugins/select2/css/select2.min.css',
                    'plugins/select2/js/select2.min.js',
                    'stok/assets/js/laporan_stok.js'
                    ];

        add_assets($assets);

        $this->template->set('results', $return);
        $this->template->set('search', $search);
        $this->template->set('idt', $identitas);
        $this->template->set('period1', $tgl_awal);
        $this->template->set('period2', $tgl_akhir);
        $this->template->title(lang('lap_hpp'));
        $this->template->render('laporan');
    }
}
?>