<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_barang','name'=>'frm_barang')) ?>
	<div class="box-header">
		<div class="form-group">
            <div class="pull-right form-inline">
                <div class="form-group">
                    <select id="id_gudang" name="id_gudang" class="form-control" style="min-width: 150px;">
                        <option></option>
                        <?php foreach ($gudang as $gd) : ?>
                        <option value="<?= $gd->id_gudang;?>" <?= set_select('id_gudang', $gd->id_gudang, isset($id_gudang) && $id_gudang == $gd->id_gudang)?>> <?= $gd->nama_gudang;?> </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <select id="id_kategori" name="id_kategori" class="form-control" style="min-width: 150px;">
                        <option></option>
                        <?php foreach ($kategori as $kat) : ?>
                        <option value="<?= $kat->idkategori;?>" <?= set_select('id_kategori', $kat->idkategori, isset($id_kategori) && $id_kategori == $kat->idkategori)?>> <?= $kat->nmkategori;?> </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <select id="id_jenis" name="id_jenis" class="form-control" style="min-width: 150px;">
                        <option></option>
                        <?php foreach ($jenis as $jn) : ?>
                        <option value="<?= $jn->idjenis_bb;?>" <?= set_select('id_jenis', $jn->idjenis_bb, isset($id_jenis) && $id_jenis == $jn->idjenis_bb)?>> <?= $jn->nmjenis_bb;?> </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <select id="id_merek" name="id_merek" class="form-control" style="min-width: 160px;">
                        <option></option>
                        <?php foreach ($merek as $mr) : ?>
                        <option value="<?= $mr->idmerk_bb;?>" <?= set_select('id_merek', $mr->idmerk_bb, isset($id_merek) && $id_merek == $mr->idmerk_bb)?>> <?= $mr->nm_merk_bb;?> </option>
                        <?php endforeach; ?>
                    </select>
                </div>
				<div class="input-group">
					<input type="text" name="table_search" value="<?php echo isset($search) ? $search:''; ?>" class="form-control pull-right" placeholder="Search" autofocus>
					<div class="input-group-btn">
						<button class="btn btn-success"><i class="fa fa-search"></i></button>
					</div>
				</div>	
			</div>
		</div>
	</div>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
	<div class="box-body table-responsive no-padding">
            <table class="table table-striped" id="tbarang">
                <thead>
                    <tr>
                        <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
                        <th width="50">#</th>
                        <th><?= lang('barcode') ?></th>
                        <th><?= lang('nm_barang') ?></th>
                        <th><?= lang('kategori') ?></th>
                        <th><?= lang('jenis') ?></th>
                        <th><?= lang('merk') ?></th>
                        <th><?= lang('gudang') ?></th>
                        <th><?= lang('stok') ?></th>
                        <th><?= lang('satuan') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($results as $record) : ?>
                    <tr>
                        <td class="column-check"><input type="checkbox" name="checked[]" value="<?= $record->idbarang_bb ?>" /></td>
                        <td><?= $numb; ?></td>
                        <td><?= $record->barcode ?></td>
                        <td><?= $record->nm_barang ?></td>
                        <td><?= $record->nmkategori ?></td>
                        <td><?= $record->nmjenis_bb ?></td>
                        <td><?= $record->nm_merk_bb ?></td>
                        <td><?= $record->nama_gudang ?></td>
                        <td>
                            <input type="hidden" name="stok[]" value="<?= $record->stok ?>">
                            <label class="lbl-stok"><?= number_format($record->stok) ?></label>
                            </td>
                        <td>
                            <select class="form-control" name="konversi[]" >
                                <?php 
                                    if($record->konversi) :
                                        foreach ($record->konversi as $key2 => $kf) :
                                ?>
                                <option value="<?= $kf->id_konversi ?>" <?= ($kf->selected == 1) ? "selected='selected'" : "" ?> data-jml-kecil="<?= $kf->jml_kecil ?>" data-jml-besar="<?= $kf->jml_besar ?>"><?= $kf->tampil2 ?></option>
                                <?php
                                        endforeach; 
                                    endif; 
                                ?>
                            </select>
                        </td>
                    </tr>
                    <?php $numb++; endforeach; ?>
                </tbody>
	  </table>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">
		<?php
		echo $this->pagination->create_links(); 
		?>
	</div>
	<?php else: ?>
     <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('stok_no_records_found') ?></p>
    </div>
    <?php
	endif;
	echo form_close(); 
	?>	
</div><!-- /.box-->