<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_laporan','name'=>'frm_laporan','class' => 'form-inline')) ?>
        <div class="box-header hidden-print">
            <div class="pull-left">
                <button type="button" id="cetak" class="btn btn-default" onclick="cetak_halaman()"><i class="fa fa-print"></i> <?= lang('cetak') ?></button>
            </div>
            <div class="pull-right">
                <div class="form-group">
                    <div class="input-daterange input-group" title="<?= lang('range_tgl') ?>" data-toggle="tooltip" data-placement="top">
                        <input type="text" style="width: 120px" class="form-control" name="tgl_awal" id="tgl_awal" value="<?= isset($period1) ? $period1 : '' ?>" placeholder="<?= lang('tgl_awal') ?>" />
                        <span class="input-group-addon">to</span>
                        <input type="text" style="width: 120px" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?= isset($period2) ? $period2 : '' ?>" placeholder="<?= lang('tgl_akhir') ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="nm barang / nama supplier" autofocus />
                        <div class="input-group-btn">
                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?= form_close() ?>
    <?php if($idt) : ?>
    <div class="row header-laporan">
        <div class="col-md-12">
            <div class="col-md-1 laporan-logo">
                <img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo">
            </div>
            <div class="col-md-6 laporan-identitas">
                <address>
                    <h4><?= $idt->nm_perusahaan ?></h4>
                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?>
                </address>
            </div>
            <div class="col-md-5 text-right">
                <h5>Laporan Riwayat HPP</h5>
                <h5>Periode <?= $period1." - ".$period2 ?></h5>
            </div>
            <div class="clearfix"></div>
            <div class="laporan-garis"></div>
        </div>
    </div>
    <?php endif ?>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
    	<div class="box-body">
            <?php 
                $num = 1;
                foreach ($results as $key => $dt): ?>
                <div class="table-responsive">
                    <table class="table-condensed table-head">
                        <tr>
                            <td><?= '<strong>'.($num).'.</strong> '.lang('nm_barang') ?></td>
                            <td>:</td>
                            <td><strong><?= $dt->nm_barang ?></strong></td>
                            <td><?= lang('merk') ?></td>
                            <td>:</td>
                            <td><?= $dt->nm_merk_bb ?></td>
                        </tr>
                        <tr>
                            <td><?= '&nbsp;&nbsp;&nbsp;&nbsp;'.lang('kategori') ?></td>
                            <td>:</td>
                            <td><?= $dt->nmkategori ?></td>
                            <td><?= lang('jenis') ?></td>
                            <td>:</td>
                            <td><?= $dt->nmjenis_bb ?></td>
                        </tr>
                    </table>
                </div>
                <div class="table-responsive no-padding">
                    <table class="table table-condensed table-bordered table-detail">
                        <thead>
                            <tr class="bg-success">
                                <th width="50">#</th>
                                <th><?= lang('nm_supplier') ?></th>
                                <th><?= lang('hpp') ?></th>
                                <th><?= lang('tgl') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $total = 0;
                                if($dt->detail):
                                foreach ($dt->detail as $key2 => $record) : 
                            ?>
                            <tr>
                                <td><?= $key2+1 ?></td>
                                <td><?= $record->nm_supplier ?></td>
                                <td align="right"><?= number_format($record->hpp) ?></td>
                                <td><?= date('d M Y', strtotime($record->created_on)) ?></td>
                            </tr>
                            <?php endforeach;endif; ?>
                        </tbody>
        	        </table>
                </div>
          <?php $num++;endforeach ?>
    	</div><!-- /.box-body -->
        
	<?php else: ?>
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('stok_no_records_found') ?></p>
    </div>
    <?php endif;?>
</div><!-- /.box -->