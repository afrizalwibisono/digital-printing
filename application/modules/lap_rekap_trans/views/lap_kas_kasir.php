<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_laporan','name'=>'frm_laporan','class' => 'form-inline')) ?>
        <div class="box-header hidden-print">
            <div class="pull-left">
                <button type="button" id="cetak" class="btn btn-default" onclick="cetak_halaman()"><i class="fa fa-print"></i> <?= lang('cetak') ?></button>
            </div>
            <div class="pull-right">
                <div class="form-group">
                    <div class="input-daterange input-group" title="<?= lang('range_tgl') ?>" data-toggle="tooltip" data-placement="top">
                        <input type="text" style="width: 120px" class="form-control" name="tgl_awal" id="tgl_awal" value="<?= isset($period1) ? $period1 : '' ?>" placeholder="<?= lang('tgl_awal') ?>" readonly />
                        <span class="input-group-addon">to</span>
                        <input type="text" style="width: 120px" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?= isset($period2) ? $period2 : '' ?>" placeholder="<?= lang('tgl_akhir') ?>" readonly />
                    </div>
                </div>
                
                <div class="form-group">
                    <select name="karyawan" id="karyawan" class="form-control" style="min-width: 300px !important" >
                        <option value=""></option>
                        <?php foreach ($dt_karyawan as $key => $dt) : ?>
                            <option value="<?= $dt->id ?>" <?= set_select("karyawan",$dt->id,isset($dt->id) && $karyawan == $dt->id) ?> >
                                <?= ucwords($dt->nm) ?>
                            </option>
                        <?php endforeach ?>
                    </select>
                </div>
                <form class="form-group">
                    <button class="btn btn-default"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>
    <?= form_close() ?>
    <?php if($idt) : ?>
    <div class="row header-laporan">
        <div class="col-md-12">
            <div class="col-md-1 laporan-logo">
                <img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo">
            </div>
            <div class="col-md-6 laporan-identitas">
                <address>
                    <h4><?= $idt->nm_perusahaan ?></h4>
                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?>
                </address>
            </div>
            <div class="col-md-5 text-right">
                <h5><?= lang("judul_kas_kasir") ?></h5>
                <h5>Periode <?= $period1." - ".$period2 ?></h5>
            </div>
            <div class="clearfix"></div>
            <div class="laporan-garis"></div>
        </div>
    </div>
    <?php endif ?>
	<?php if (isset($data) && is_array($data) && count($data)) : ?>
    	
        <div class="box-body laporan-body">
            
            <h4><?= lang('resume_kasir') ?></h4>
            <table class="table-condensed table-summary">
                <tbody>
                    <tr>
                        <td width="200"><?= lang('ttl_cash_masuk_kasir') ?></td>
                        <td width="10">:</td>
                        <td width="200" class="text-right"><strong><?= number_format($data_resume[0]->ttl_cash_masuk) ?></strong></td>
                        <td>&nbsp;</td>
                    </tr>
                    
                    <tr>
                        <td width="200"><?= lang('ttl_cash_keluar_kasir') ?></td>
                        <td width="10">:</td>
                        <td width="200" class="text-right"><strong><?= number_format($data_resume[0]->ttl_cash_keluar) ?></strong></td>
                        <td>&nbsp;</td>
                    </tr>
                    
                    <tr>
                        <td width="200"><?= lang('ttl_debit_kasir') ?></td>
                        <td width="10">:</td>
                        <td width="200" class="text-right"><strong><?= number_format($data_resume[0]->ttl_debit) ?></strong></td>
                        <td>&nbsp;</td>
                    </tr>
                    
                    <tr>
                        <td width="200"><?= lang('ttl_transfer_kasir') ?></td>
                        <td width="10">:</td>
                        <td width="200" class="text-right"><strong><?= number_format($data_resume[0]->ttl_transfer) ?></strong></td>
                        <td>&nbsp;</td>
                    </tr>
                    
                    <tr>
                        <td width="200"><?= lang('ttl_deposit_kasir') ?></td>
                        <td width="10">:</td>
                        <td width="200" class="text-right"><strong><?= number_format($data_resume[0]->ttl_deposit)  ?></strong></td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td width="200"><?= lang('ttl_closing_kasir') ?></td>
                        <td width="10">:</td>
                        <td width="200" class="text-right"><strong><?= number_format($data_resume[0]->ttl_final_closing) ?></strong></td>
                        <td>&nbsp;</td>
                    </tr>

                </tbody>   
            </table>    

            <h4><?= lang('jdl_detail_data') ?></h4>

            <div class="table-responsive no-padding">
                <table class="table table-condensed table-bordered table-detail">
                    <thead>
                        <tr class="bg-success">
                            <th width="30" class="text-center">No.</th>
                            <th class="text-center"><?= lang("lbl_jdl_tbl_tgl_kasir") ?></th>
                            <th class="text-center"><?= lang("lbl_jdl_tbl_op_kasir") ?></th>
                            <th class="text-center"><?= lang("lbl_jdl_tbl_saldo_awal_kasir") ?></th>
                            <th class="text-center"><?= lang("lbl_jdl_tbl_masuk_kasir") ?></th>
                            <th class="text-center"><?= lang("lbl_jdl_tbl_keluar_kasir") ?></th>
                            <th class="text-center"><?= lang("lbl_jdl_tbl_debit_kasir") ?></th>
                            <th class="text-center"><?= lang("lbl_jdl_tbl_transfer_kasir") ?></th>
                            <th class="text-center"><?= lang("lbl_jdl_tbl_piutang_kasir") ?></th>
                            <th class="text-center"><?= lang("lbl_jdl_tbl_saldo_akhir_kasir") ?></th>
                            <th class="text-center"><?= lang("lbl_jdl_tbl_saldo_akhir_ref_kasir") ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $key => $isi) : ?>
                            <tr>
                                <td class="text-right">
                                    <?= str_pad($key+1, 2,"0",STR_PAD_LEFT) ?>
                                </td>
                                <td class="text-center">
                                    <?= $isi->tgl ?>
                                </td>
                                <td class="text-center">
                                    <?= ucwords($isi->nama) ?>
                                </td>
                                <td class="text-center">
                                    <?= number_format($isi->saldo_awal) ?>
                                </td>
                                <td class="text-center">
                                    <?= number_format($isi->total_masuk) ?>
                                </td>
                                <td class="text-center">
                                    <?= number_format($isi->total_keluar) ?>
                                </td>
                                <td class="text-center">
                                    <?= number_format($isi->total_debit) ?>
                                </td>
                                <td class="text-center">
                                    <?= number_format($isi->total_transfer) ?>
                                </td>
                                <td class="text-center">
                                    <?= number_format($isi->total_piutang) ?>
                                </td>
                                <td class="text-center">
                                    <strong>
                                    <?= number_format($isi->saldo_akhir_system) ?>
                                    </strong>
                                </td>
                                <td class="text-center">
                                    <strong>
                                    <?php 

                                        if($isi->saldo_revisi > 0){

                                            echo number_format($isi->saldo_revisi);

                                        }else{

                                            echo number_format($isi->saldo_akhir_system);
                                        }

                                    ?>
                                    </strong>
                                </td>






                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            
        </div><!-- end box-body -->

	<?php else: ?>
    <div class="alert alert-info" role="alert">
            <p><i class=fa fa-warning"></i> &nbsp; <?= lang('lbl_data_tidak_ditemukan') ?></p>
    </div>
    <?php endif;?>
</div><!-- /.box -->