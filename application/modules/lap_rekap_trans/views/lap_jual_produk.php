<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_laporan','name'=>'frm_laporan')) ?>
        <div class="box-header hidden-print">
    
            <div class="row">
                
                <div class="col-lg-3 col-sm-12" >
                    
                    <div class="form-group">
                        <div class="input-daterange input-group" title="<?= lang('range_tgl') ?>">             
                            <input type="text" class="form-control" name="tgl_awal" id="tgl_awal" value="<?= isset($period1) ? $period1 : '' ?>" placeholder="<?= lang('tgl_awal') ?>" readonly />
                            <span class="input-group-addon">to</span>
                            <input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?= isset($period2) ? $period2 : '' ?>" placeholder="<?= lang('tgl_akhir') ?>" readonly />
                        </div>
                    </div>    

                </div>

                <div class="col-lg-2 col-sm-12" >
                    
                    <div class="form-group">

                        <select class="form-control" name="grouping" id="grouping">
                            <option value="0" <?= set_select("grouping",'0',isset($grouping) && $grouping == '0' ) ?> >
                                <?= lang('lbl_isi_grouping_konsumen') ?>
                            </option>
                            <option value="1" <?= set_select("grouping",'1',isset($grouping) && $grouping == '1' ) ?> >
                                <?= lang('lbl_isi_grouping_produk') ?>
                            </option>
                        </select>

                    </div>

                </div>

                <div id="by_produk">
                    
                    <div class="col-lg-7 col-sm-12">
                        
                        <div class="form-group">

                            <?=
                                form_dropdown('pil_kategori[]',$kategori,$pil_kategori,
                                    [   'id'        => "pil_kategori",
                                        'multiple'  => "multiple",
                                        'class'     => "form-control"
                                    ]);
                            ?>
                            <!-- <select class="form-control" name="pil_kategori[]" id="pil_kategori" multiple="multiple" >

                            </select> -->
                        </div>

                    </div>

                </div>

                <div id="by_konsumen" >

                    <div class="col-lg-3 col-sm-12" >

                        <div class="form-group">
                            
                            <select name="st_konsumen" id="st_konsumen" class="form-control">
                                <option value="" <?= set_select('st_konsumen','',isset($st_konsumen) && $st_konsumen == '') ?> ><?= lang("lbl_isi_kategori_all") ?></option>
                                <option value="0" <?= set_select('st_konsumen','0',isset($st_konsumen) && $st_konsumen == '0') ?> ><?= lang("lbl_isi_kategori_0") ?></option>
                                <option value="1" <?= set_select('st_konsumen','1',isset($st_konsumen) && $st_konsumen == '1') ?>><?= lang("lbl_isi_kategori_1") ?></option>
                                <option value="2" <?= set_select('st_konsumen','2',isset($st_konsumen) && $st_konsumen == '2') ?> ><?= lang("lbl_isi_kategori_2") ?></option>
                            </select>

                        </div>

                    </div>   

                    <div class="col-lg-4 col-sm-12">
                        <div class="form-group">
                            <input type="hidden" name="isi_konsumen" id="isi_konsumen" value="<?= set_value('konsumen', isset($konsumen) ? $konsumen : '') ?>">
                            <select name="konsumen" id="konsumen" class="form-control">
                                <option value=""></option>
                            </select>
                        </div>
                    </div> 

                </div>

            </div>

            <div class="pull-right">
                
               
               
                
            </div>

            <div class="row">

               <div class="col-sm-12">

                    <div class="pull-left">
                        <button type="button" id="cetak" class="btn btn-default" onclick="cetak_halaman()"><i class="fa fa-print"></i> <?= lang('cetak') ?></button>
                    </div>
                    <div class="pull-right">
                        <form class="form-group">
                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                        </form>
                    </div>

                </div>

            </div>

        </div>
    <?= form_close() ?>
    <?php if($idt) : ?>
    <div class="row header-laporan">
        <div class="col-md-12">
            <div class="col-md-1 laporan-logo">
                <img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo">
            </div>
            <div class="col-md-6 laporan-identitas">
                <address>
                    <h4><?= $idt->nm_perusahaan ?></h4>
                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?>
                </address>
            </div>
            <div class="col-md-5 text-right">
                <h5><?= lang("judul_jual_produk") ?></h5>
                <h5>Periode <?= $period1." - ".$period2 ?></h5>
            </div>
            <div class="clearfix"></div>
            <div class="laporan-garis"></div>
        </div>
    </div>
    <?php endif ?>
	<?php if (isset($data) && is_array($data) && count($data)) : ?>
    	
        <div class="box-body laporan-body">
            
            <h4><?= lang("lbl_resume_produk") ?></h4>
            <table class="table-condensed table-summary">
                <?php foreach ($resume as $key => $rs) : ?>
                    <tr>
                        <td width="200"><?= strtoupper($rs->nmkategori) ?></td>
                        <td width="10">:</td>
                        <td><strong><?= number_format($rs->total) ?> </strong></td>
                    </tr>

                <?php endforeach; ?>
            </table>

            <h4><?= lang('lbl_detail_produk') ?></h4>
            <?php foreach ($data as $key => $dt) : ?>

            <div class="table-responsive no-padding">
                <table class="table-condensed table-head">
                    <tr>
                        <?php 

                            if($grouping == 0): //jika filter berdasarkan konsumen

                        ?>
                            <td><?= lang("lbl_head_group_konsumen_produk") ?></td>
                            <td>:</td>
                            <td><strong><?= $dt['dt_header']?></strong></td>
                        <?php else : ?>
                            <td><?= lang("lbl_head_group_kategori_produk") ?></td>
                            <td>:</td>
                            <td><strong><?= $dt['dt_header']?></strong></td>
                        <?php endif ?>    
                    </tr>
                </table>
            </div>    

            <div class="table-responsive no-padding">
                <table class="table table-condensed table-bordered table-detail">
                    <thead>
                        <tr class="bg-success">
                            <th width="30">No.</th>
                            <th class="text-center"><?= lang("lbl_jdl_tbl_nm_produk") ?></th>
                            <th class="text-center"><?= lang("lbl_jdl_tbl_kategori_produk") ?></th>
                            <th class="text-center"><?= lang("lbl_jdl_tbl_cetak_produk") ?></th>
                            <th class="text-right"><?= lang("lbl_jdl_tbl_total_produk") ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 

                            $total_group = 0;

                            foreach ($dt['dt_detail'] as $key => $isi) : 

                                $total_group    += $isi['ttl_trans'];

                        ?>
                        <tr>
                            <td class="text-right"><?= str_pad($key+1, 2, "00",STR_PAD_LEFT) ?></td>
                            <td class="text-center"><?= strtoupper($isi['nm_produk']) ?></td>
                            <td class="text-center"><?= strtoupper($isi['kategori']) ?></td>
                            <td class="text-center">
                                <?= number_format($isi['cetak'],2)." ".$isi['satuan'] ?>
                            </td>
                            <td class="text-right"><?= number_format($isi['ttl_trans']) ?></td>
                        </tr>
                        <?php endforeach; ?>    
                    </tbody>
                    <tfoot>
                        <tr class="bg-info">
                            <th colspan="4" class="text-center">
                                <?= lang("lbl_total_foot_group_produk") ?>
                            </th>
                            <th class="text-right">
                                <?= number_format($total_group) ?> 
                            </th>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <?php endforeach; ?>

        </div><!-- end box-body -->

	<?php else: ?>
    <div class="alert alert-info" role="alert">
            <p><i class=fa fa-warning"></i> &nbsp; <?= lang('lbl_data_tidak_ditemukan') ?></p>
    </div>
    <?php endif;?>
</div><!-- /.box -->