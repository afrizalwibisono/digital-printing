<div class="box box-primary">
	<?php echo form_open($this->uri->uri_string(),	[
														'name' 	=> 'frm_laporan',
														'id'	=> 'frm_laporan',
														'role'	=> 'form',
														'class'	=> 'form-inline'
													
													]) ?>

	<div class="box-header hidden-print">
		
		<div class="pull-right">
			
			<div class="form-group">
				<div class="input-group input-daterange">
					<input type="text" name="tgl1" id="tgl1" style="width: 120px" class="form-control" placeholder="<?= lang("tgl_awal") ?>" value="<?= set_value('tgl1',isset($filter['tgl1']) ? $filter['tgl1'] : '' ) ?>" >
					<span class="input-group-addon">to</span>
					<input type="text" name="tgl2" id="tgl2" style="width: 120px" class="form-control" placeholder="<?= lang("tgl_akhir") ?>" value="<?= set_value('tgl2',isset($filter['tgl2']) ? $filter['tgl2'] : '' ) ?>">
				</div>
			</div>

			<div class="form-group">
				<select class="form-control" name="posisi" id="posisi" style="min-width: 120px">
					<option></option>
					<option value="1" <?= set_select('posisi',1,isset($filter['st_kasir']) && $filter['st_kasir'] === 1 ) ?> >Kasir</option>
					<option value="0" <?= set_select('posisi',0,isset($filter['st_kasir']) && $filter['st_kasir'] === 0 ) ?> >Accounting</option>
				</select>
			</div>

			<div class="form-group">
				<select class="form-control" name="gol_konsumen" id="gol_konsumen" style="min-width: 120px">
					<option></option>
					<option value="0" <?= set_select('gol_konsumen','0',isset($filter['gol_kons']) && $filter['gol_kons'] === 0 ) ?> >Konsumen</option>
					<option value="1" <?= set_select('gol_konsumen','1',isset($filter['gol_kons']) && $filter['gol_kons'] === 1 ) ?> >Reseller</option>
					<option value="2" <?= set_select('gol_konsumen','2',isset($filter['gol_kons']) && $filter['gol_kons'] === 2 ) ?> >Instansi</option>
				</select>
			</div>

			<div class="form-group">
				<input type="hidden" name="id_konsumen_pilih" id="id_konsumen_pilih" value="<?= set_value('konsumen',isset($filter['konsumen']) ? $filter['konsumen'] : '' ) ?>" >
				<select class="form-control" name="konsumen" id="konsumen" style="min-width: 200px">
					<option></option>
				</select>
			</div>

		</div>

		<div class="clearfix"></div>
		<hr style="margin-top: 8px;margin-bottom: 5px">
		
		<div class="pull-left">
			<button type="button" id="cetak" class="btn btn-default" onclick="cetak_halaman()"><i class="fa fa-print"></i> Cetak</button>
		</div>
		<div class="pull-right">
			<button type="submit" id="cari" name="cari" class="btn btn-default"><i class="fa fa-search"></i> Filter</button>
		</div>

	</div>
	<?php echo form_close(); ?>

	<?php if($idt) : ?>
    <div class="row header-laporan">
        <div class="col-md-12">
            <div class="col-md-1 laporan-logo">
                <img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo">
            </div>
            <div class="col-md-6 laporan-identitas">
                <address>
                    <h4><?= $idt->nm_perusahaan ?></h4>
                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?>
                </address>
            </div>
            <div class="col-md-5 text-right">
                <h3><?= lang("title-a3-jdl-lap") ?></h3>
                <h5>Periode Cetak <strong><?= $filter["tgl1"]." - ".$filter["tgl2"] ?></strong></h5>
            </div>
            <div class="clearfix"></div>
            <div class="laporan-garis"></div>
        </div>
    </div>
    <?php endif ?>

    <?php if(isset($data) && is_array($data) && count($data)) : ?>

	<div class="box-body laporan-body">

		<h4><?= lang('trans-a3-resume-jdl') ?></h4>
    	<table class="table-condensed table-summary">
    		<tbody>
    			<?php foreach ($dt_resume as $key => $resume) : ?>
    			<tr>
    				<td width="200"><strong><?= strtoupper($resume->nm_barang) ?></strong></td>
    				<td width="10">:</td>
    				<td><?= number_format($resume->cetak_a3). ' (kali)' ?></td>
    			</tr>
    			<?php endforeach ?>
    		</tbody>
    	</table>


    	<h4><?= lang('trans-a3-detail-jdl') ?></h4>
    	<?php foreach ($data as $key => $isi) : ?>
	    	<div class="table-responsive no-padding">
		    	<table class="table-condensed table-head">
		    		<tr>
	    				<td><?= lang('trans-order-head-nota') ?></td>
	    				<td>:</td>
	    				<td>
	    					<strong><?= $isi['no_nota'] ?></strong>
	    				</td>
	    				<td><?= lang('trans-order-head-waktu') ?></td>
	    				<td>:</td>
	    				<td><?= $isi['waktu'] ?></td>
	    				<td><?= lang('trans-order-head-konsumen') ?></td>
	    				<td>:</td>
	    				<td>
	    					<strong><?= $isi['konsumen']." | ".strtoupper($isi['gol_konsumen']) ?></strong>
	    				</td>
	    				<td><?= lang('trans-order-head-lokasi') ?></td>
	    				<td>:</td>
	    				<td>
	    					<?= $isi['st_kasir'] ?>
	    				</td>
	    				<td><?= lang('trans-order-head-op') ?></td>
	    				<td>:</td>
	    				<td><?= $isi['op'] ?></td>
	    			</tr>
		    	</table>
		    </div>

		    <div class="table-responsive no-padding">
	    		<table class="table table-condensed table-bordered table-detail">
	    			<thead>
	    				<tr class="success">
	    					<th class="text-center" width="10">#</th>
	    					<th class="text-center"><?= lang("trans-a3-tbl-file") ?></th>
	    					<th class="text-center"><?= lang("trans-a3-tbl-produk") ?></th>
	    					<th class="text-center"><?= lang("trans-a3-tbl-cetak") ?></th>
	    					<th class="text-center"><?= lang("trans-a3-tbl-jml-order") ?></th>
	    					<th class="text-center"><?= lang("trans-a3-tbl-jml-a3") ?></th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    				<?php foreach ($isi['detail'] as $key_det => $det) : ?>
	    					<tr>
	    						<td class="text-right"><?= $key_det+1 ?></td>
	    						<td><?= $det['nm_file'] ?></td>
	    						<td class="text-center" ><?= $det['produk'] ?></td>
	    						<td class="text-center" ><?= $det['bahan_cetak'] ?></td>
	    						<td class="text-center" ><?= $det['jml_order'] ?></td>
	    						<td class="text-center" >
	    							<strong><?= number_format($det['jml_cetak_a3']) ?></strong>
	   							</td>
	    					</tr>
	    				<?php endforeach ?>
	    			</tbody>
	    		</table>
    		</div>	
	    <?php endforeach ?>	

	</div>


    <?php else: ?>
    	<div class="alert alert-info" role="alert">
	        <p><i class=fa fa-warning"></i> &nbsp; <?= lang('lbl_data_tidak_ditemukan') ?></p>
	    </div>
    <?php endif ?>

</div>