$(function(){

	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	$("#pil_produk").select2({
        placeholder :"Pilih Produk",
        allowClear  : true
  	});  

	$("#posisi").select2({
		placeholder :"Lokasi Transaksi",
        allowClear  : true
	});

	$("#jns_order").select2({
		placeholder :"Order",
        allowClear  : true
	});

	$("#st_lunas").select2({
		placeholder :"Status",
        allowClear  : true
	});

	$("#gol_konsumen").select2({
		placeholder :"Golongan",
        allowClear  : true
	});

	$("#konsumen").select2({
		placeholder :"konsumen",
        allowClear  : true
	});

	$("#gol_konsumen").change(function(){

  		$("#isi_konsumen_pilih").val('');
  		get_nama_konsumen();

  	});

	get_nama_konsumen();
	
});

function get_nama_konsumen(){

	var jns_konsumen 	= $("#gol_konsumen").val();
	var id_pilih 		= $("#id_konsumen_pilih").val();

	var st_ada_pilih 	= id_pilih.length > 0 ? true : false;

	$.ajax({
				url 		: baseurl + "lap_rekap_trans/get_konsumen",
				type 		: "post",
				dataType 	: "json",
				data 		: {st : jns_konsumen},
				success 	: function(msg){

								if(msg['st'] == 1){

									$("#konsumen option").remove();

									var isi 	= "<option></option>";
									var select 	= "";

									$.each(msg['data'],function(i,n){

										if(st_ada_pilih  && n['id'] == id_pilih){

											select 	= "selected";

										}else{

											select = "";
										}

										isi += "<option value='" +n['id']+"' " + select + ">"+ n['nm'] +"</option>";

									});

									$("#konsumen").append(isi);

								}

							}


	});

}

function cetak_halaman(){
	window.print();
}