$(function(){
	//Date Picker
	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	$("#st_konsumen").select2({
        placeholder :"Pilih Kategori Konsumen",
        allowClear  : true
  	});

  	$("#konsumen").select2({
        placeholder :"Pilih Konsumen",
        allowClear  : true
  	});

	$("#pil_kategori").select2({
        placeholder :"Pilih Kategori Produk",
        allowClear  : true
  	});  	

  	$("#st_konsumen").change(function(){

  		$("#isi_konsumen").val('');
  		get_nama_konsumen();

  	});

  	$("#grouping").change(function(){

  		cek_pilihan();

  	});

  	get_nama_konsumen();

  	cek_pilihan();

	// $(".page-title").addClass("hidden-print");
});

function cek_pilihan(){

	var pil_by 	= $("#grouping").val();

	if(pil_by == 0){

		$("#by_produk").hide(400);
		$("#by_konsumen").show(400);

	}else{

		$("#by_konsumen").hide(400);
		$("#by_produk").show(400);

	}

}

function get_nama_konsumen(){

	var jns_konsumen 	= $("#st_konsumen").val();
	var id_pilih 		= $("#isi_konsumen").val();

	var st_ada_pilih 	= $("#isi_konsumen").val().length > 0 ? true : false;

	$.ajax({
				url 		: baseurl + "lap_rekap_trans/get_konsumen",
				type 		: "post",
				dataType 	: "json",
				data 		: {st : jns_konsumen},
				success 	: function(msg){

								if(msg['st'] == 1){

									$("#konsumen option").remove();

									var isi 	= "<option></option>";
									var select 	= "";

									$.each(msg['data'],function(i,n){

										if(st_ada_pilih  && n['id'] == id_pilih){

											select 	= "selected";

										}else{

											select = "";
										}

										isi += "<option value='" +n['id']+"' " + select + ">"+ n['nm'] +"</option>";

									});

									$("#konsumen").append(isi);

								}

							}


	});

}

function cetak_halaman(){
	window.print();
}