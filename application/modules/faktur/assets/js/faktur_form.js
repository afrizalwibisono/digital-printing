$(function(){

	$("input[name='dft_rp_cetak[]'],input[name='dft_rp_finishing[]'],input[name='dft_potongan[]']").number(true);
	$("input[name='dft_diskon[]']").number(true);
	$("#ms_tempo").number(true);
	$("#rev_rp_harga, #rev_rp_design, #rev_rp_finishing, #rev_diskon, #rev_pot").number(true);

	$("#bank_terima").select2({
		placeholder:'Pilih Bank Penerima',
    	allowClear:true
	});

	$("#tgl_tempo").datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	/* start perintah yang jalan ketika form start */

	kondisikan_inputan_st_bayar();

	//cek_kondisi_tgl_tempo_awal();

	cek_lampiran();

	total_all();

	//cek_dp();

	//cek_bayar();

	/* end perintah yang jalan ketika form start */

	$("#st_tempo, #cara_bayar").change(function(){

		kondisikan_inputan_st_bayar();		

	});
		
	$("#ms_tempo").keyup(function(){

		set_tempo();

	});

	$("#ms_tempo").change(function(){

		set_tempo();

	});

	$("#tgl_tempo").change(function(){

		ht_tempo();

	});

	/*$("#dp").keyup(function(){

		cek_kondisi_tgl_tempo_awal();
		cek_dp();
		cek_bayar();

	});

	$("#bayar").keyup(function(){

		hitung_bayar();

	});*/

	//kondisikan_input_custom();

	$("#modal_revisi").on('shown.bs.modal',function(e){

		$("#rev_rp_harga").focus();

	});


	$("#open_custom_total").click(function(e){

		e.preventDefault();
		buka_potongan_nota();

	});

	$("#rev_rp_harga, #rev_rp_finishing, #rev_rp_design, #rev_diskon, #rev_pot").keyup(function(){

		hitung_revisi_harga();

	});

	$("#tbl_terapkan").click(function(){

		terapkan_revisi();

	});

	$("#file_att").change(function(){
		readURL(this);
		$("#nm_file").text(this.value.split('\\').pop());
		$("#tampil_lampiran").show(400);

	});

	$("#metode_bayar").change(function(){

		cek_lampiran();

	});

	$("#simpan").click(function(){

		//proses_cetak();
		//event.preventDefault();

	});

	$("#delete").click(function(){

		info_delete();

	});

});

function pembulatan(angka){

	//var angka = 20034701;

	var cek_lebihan 	= angka % 100;
	var cek_bulatan 	= Math.floor(angka / 100);
	var hasil 			= 0;

	//console.log("asli : " + angka);
	//console.log("lebihan : " + cek_lebihan);
	//console.log("bulatan : " + cek_bulatan);

	if(cek_lebihan > 1){

		cek_bulatan += 1;

	} 

	hasil = cek_bulatan * 100;
	//hasil = angka;
	
	//console.log(hasil);

	return hasil;

}

function kondisikan_inputan_st_bayar(){

	var st_bayar 	= $("#st_tempo").val();
	var cr_bayar 	= $("#cara_bayar").val();

	if(st_bayar == 0){

		$("#area_cara_bayar").hide(400);
		$("#area-bank-transfer").hide(400);

		$("#area-range-tempo").show(400);

	}else{

		$("#area-range-tempo").hide(400);		

		$("#area_cara_bayar").show(400);

		if(cr_bayar == 2){

			$("#area-bank-transfer").show(400);

		}else{

			$("#area-bank-transfer").hide(400);			

		}

	}


}

function info_delete(){

	var id_trans 	= id_uri_form_kasir;

	var data 		= {id : id_trans};

	$.ajax({
				url 		: baseurl + "faktur/info_delete",
				type 		: "post",
				data 		: data,
				dataType	: "json",
				success 	: function(msg){

								if(msg['status']){

									alertify.confirm(msg['ket'],
									function(){
										
										$("#st_delete").val(1);
										$("#frm_trans").submit();

									},
									function(){
										
										$("#st_delete").val(0);	
										alertify.success("Delete batal");

									});

								}

							}


	});


}


function cek_dp(){

	var total_bayar = parseFloat($("#total_bayar").val());

	var dp 			= $("#dp").val().length > 0 ? parseFloat($("#dp").val()) : 0;
	var gtotal 		= parseFloat($("#isi_grandtotal").val());
	var sisa 		= 0;

	if(total_bayar > 0 && total_bayar < gtotal){

		$("#dp").val(total_bayar);
		$("#bayar").val("");
		dp = total_bayar;

	}

	if(dp > 0){

		if(dp > gtotal){

			alertify.error("DP melebihi nilai nota");

		}else{

			sisa 	= gtotal - dp;
			$("#kurang_bayar").text($.number(sisa));
			$("#isi_kurang_bayar").val(sisa);
			
		}

	}

	

}

function cek_lampiran(){

	if($("#metode_bayar").val() == 2){

		$("#lampiran_transfer").show(400);

	}else{

		$("#lampiran_transfer").hide(400);
		$("#nm_file").text("");
		$("#thumb").prop("src","");
		$("#tampil_lampiran").hide(400);
		$("#file_att").val("");

	}


}


function readURL(input) {

    if (input.files && input.files[0]) {

        var reader = new FileReader();
        var tempImage1 = new Image();
        reader.onload = function (e) {
            $('#thumb').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
    
}    

function terapkan_revisi(){

	if($("#isi_rev_rp_cetak").val() <= 0){

		alertify.error('Biaya cetak tidak boleh <= 0');
		return false;

	}

	var row 			= $("#index_table_daftar").val();
	
	var harga 			= parseFloat($("#rev_rp_harga").val());
	var design 			= parseFloat($("#rev_rp_design").val());
	var cetak 			= parseFloat($("#isi_rev_rp_cetak").val());
	var finishing 		= parseFloat($("#rev_rp_finishing").val());
	//var diskon 			= parseFloat($("#rev_diskon").val());
	var potongan 		= parseFloat($("#rev_pot").val());
	var total_potongan 	= $("#total_potongan").val(); //total dari diskon + potongan tunai.

	var total 			= $("#isi_rev_subtotal").val();

	var target			= $("#daftar_order tbody tr:eq("+ row +")");

	target.find("input[name='dft_rp_harga_baru[]']").val(harga);
	target.find("input[name='dft_rp_cetak_baru[]']").val(cetak);
	target.find("input[name='dft_rp_design_baru[]']").val(design);
	target.find("input[name='dft_rp_finishing_baru[]']").val(finishing);
	target.find("input[name='dft_subtotal_baru[]']").val(total);
	//target.find("input[name='dft_diskon[]']").val(diskon);
	target.find("input[name='dft_potongan[]']").val(total_potongan);
	target.find("input[name='dft_pot_kecil[]']").val(potongan);

	/* rubah tampilan biaya */

	var tampil_harga = $.number((harga - potongan))+" /"+target.find("input[name='dft_tmpl_satuan_harga[]']").val();

	target.find("input[name='dft_tampil_harga[]']").val(tampil_harga);

	target.find("input[name='dft_st_ubah[]']").val(1);

	target.find("td:eq(3)").html(tampil_harga);

	target.find("td:eq(5)").text($.number(cetak));// biaya cetak
	target.find("td:eq(6)").text($.number(design));// biaya design
	target.find("td:eq(7)").text($.number(finishing));// biaya finishing
	target.find("#dft_sub_total").text($.number(total));

	total_all();	

	$("#modal_revisi").modal("hide");

}


function hitung_revisi_harga(){


	/* start variable biaya cetak */
	var type_produk 	= parseFloat($("#st_tipe_produk_rev").val());

	var uk_p 			= parseFloat($("#uk_p_rev").val());
	var uk_l 			= parseFloat($("#uk_l_rev").val());
	var satuan_ukuran 	= parseFloat($("#id_satuan_uk_rev").val());

	var jml_cetak 		= parseFloat($("#jml_cetak_ref").val());

	var satuan_harga 	= parseFloat($("#id_satuan_harga_ref").val());

	/* end variable biaya cetak */

	var harga 		= parseFloat($("#rev_rp_harga").val());
	var design 		= $("#rev_rp_design").val().length > 0 ? parseFloat($("#rev_rp_design").val()) : 0;
	var finishing 	= $("#rev_rp_finishing").val().length > 0 ? parseFloat($("#rev_rp_finishing").val()) : 0;
	var diskon 		= $("#rev_diskon").val().length > 0 ? parseFloat($("#rev_diskon").val()) : 0;
	var potongan 	= $("#rev_pot").val().length > 0 ? parseFloat($("#rev_pot").val()) : 0;

	var data 		= {harga_baru : harga, 
						pot_harga : potongan,
						diskon : diskon,
						satuan_harga : satuan_harga, 
						tipe_produk : type_produk, 
						uk_p : uk_p, 
						uk_l : uk_l, 
						satuan_uk : satuan_ukuran, 
						jml_cetak : jml_cetak, 
						satuan_jml_cetak : 8};

	$.ajax({
				url 	 	: baseurl+"faktur/get_harga_cetak",
				type 		: "post",
				dataType 	: "json",
				data 		: data,
				success 	: function(msg){

								biaya_cetak = parseFloat(msg['cetak']);
								potongan 	= parseFloat(msg['potongan']);

								var total  	= design+biaya_cetak+finishing;
								//potongan 	+= (total * (diskon/100));
								//total 		-= potongan;								

								$("#rev_rp_cetak").text($.number(biaya_cetak));
								$("#isi_rev_rp_cetak").val(biaya_cetak);

								$("#total_potongan").val(potongan);
								$("#rev_pot_total_list").text($.number(potongan))
								$("#rev-subtotal").text($.number(total));
								$("#isi_rev_subtotal").val(total);

							}

	});


}



function buka_potongan_nota(){

	/*

		Jika isi dari data-posisi = buka, maka actionnya adalah membuka inputan, begitu sebaliknya.

	*/

	var st_pelunasan 	= parseFloat($("#st_pelunasan").val());

	if(st_pelunasan == 0){

		var posisi = $("#open_custom_total").data("posisi");
		if(posisi=="buka"){

			$("#open_custom_total").data("posisi","tutup");
			$("#icon_custom_total").prop("class","fa fa-minus-circle");

			$("#bag-subtotal").show(400);
			$("#inputan-diskon-all").show(400);
			$("#inputan-pot-all").show(400);

			$("#diskon_all").focus();

		}else{

			$("#open_custom_total").data("posisi","buka");
			$("#icon_custom_total").prop("class","fa fa-edit");		

			$("#bag-subtotal").hide(400);
			$("#inputan-diskon-all").hide(400);
			$("#inputan-pot-all").hide(400);

		}

	}


}


function reset_harga(obj,e){

	e.preventDefault();


	var harga_asli 		= $(obj).closest("tr").find("input[name='dft_rp_harga_asli[]']").val();

	var cetak_asli		= $(obj).closest("tr").find("input[name='dft_rp_cetak_asli[]']").val();
	var design_asli		= $(obj).closest("tr").find("input[name='dft_rp_design_asli[]']").val();
	var finishing_asli	= $(obj).closest("tr").find("input[name='dft_rp_finishing_asli[]']").val();
	var subtotal_asli	= $(obj).closest("tr").find("input[name='dft_subtotal_asli[]']").val();

	/* netralkan harga_baru */
	$(obj).closest("tr").find("input[name='dft_rp_harga_baru[]']").val(harga_asli);
	$(obj).closest("tr").find("input[name='dft_rp_cetak_baru[]']").val(cetak_asli);
	$(obj).closest("tr").find("input[name='dft_rp_design_baru[]']").val(design_asli);
	$(obj).closest("tr").find("input[name='dft_rp_finishing_baru[]']").val(finishing_asli);
	$(obj).closest("tr").find("input[name='dft_subtotal_baru[]']").val(subtotal_asli);
	$(obj).closest("tr").find("input[name='dft_diskon[]']").val("0");
	$(obj).closest("tr").find("input[name='dft_pot_kecil[]']").val("0");
	$(obj).closest("tr").find("input[name='dft_potongan[]']").val("0");
	$(obj).closest("tr").find("input[name='dft_st_ubah[]']").val("0");
	
	var tampil_harga = $.number(harga_asli)+ " /" + $(obj).closest("tr").find("input[name='dft_tmpl_satuan_harga[]']").val();
	$(obj).closest("tr").find("input[name='dft_tampil_harga[]']").val(tampil_harga);
	/* Edit tampilan tabel */	

	$(obj).closest("tr").find("td:eq(3)").html(tampil_harga);// harga
	$(obj).closest("tr").find("td:eq(5)").text($.number(cetak_asli)); // biaya cetak
	$(obj).closest("tr").find("td:eq(6)").text($.number(design_asli));// biaya design
	$(obj).closest("tr").find("td:eq(7)").text($.number(finishing_asli));// biaya finishing
	$(obj).closest("tr").find("#dft_sub_total").text($.number(subtotal_asli));

	total_all();

}

function remove_item(obj){
	$(obj).closest("tr").animate({backgroundColor:'red'}, 500).fadeOut(500,function() {
	    $(obj).closest("tr").remove();
	});
}

function edit_harga(obj,e){

	e.preventDefault();

	var index_row 			= $(obj).closest("tr").index();

	var st_tipe_produk 		= $(obj).closest("tr").find("input[name='dft_st_type_produk[]']").val();

	var uk_p_rev 			= $(obj).closest("tr").find("input[name='dft_uk_p[]']").val();	 	
	var uk_l_rev 			= $(obj).closest("tr").find("input[name='dft_uk_l[]']").val();	 	
	var id_satuan_uk_rev	= $(obj).closest("tr").find("input[name='dft_idsatuan_uk[]']").val();	 	

	var jml_cetak			= $(obj).closest("tr").find("input[name='dft_jml_cetak[]']").val();	 	

	var satuan_harga		= $(obj).closest("tr").find("input[name='dft_tmpl_satuan_harga[]']").val();	 	

	var id_satuan_harga		= $(obj).closest("tr").find("input[name='dft_id_satuan_harga[]']").val();	 	

	var rp_harga_baru 		= $(obj).closest("tr").find("input[name='dft_rp_harga_baru[]']").val();
	var rp_cetak_baru 		= $(obj).closest("tr").find("input[name='dft_rp_cetak_baru[]']").val();
	var rp_finishing_baru 	= $(obj).closest("tr").find("input[name='dft_rp_finishing_baru[]']").val();
	var rp_design 			= $(obj).closest("tr").find("input[name='dft_rp_design_baru[]']").val();
	var subtotal 			= $(obj).closest("tr").find("input[name='dft_subtotal_baru[]']").val();

	var diskon 				= $(obj).closest("tr").find("input[name='dft_diskon[]']").val();
	var pot_kecil 			= $(obj).closest("tr").find("input[name='dft_pot_kecil[]']").val();
	var potongan 			= $(obj).closest("tr").find("input[name='dft_potongan[]']").val();
	
	$("#rev_rp_harga").val(rp_harga_baru);
	$("#tmpl_satuan_harga").html("/"+ satuan_harga);

	$("#rev_rp_cetak").text($.number(rp_cetak_baru));
	$("#isi_rev_rp_cetak").val(rp_cetak_baru);

	$("#rev_rp_design").val(rp_design);
	
	$("#rev_rp_finishing").val(rp_finishing_baru);
	
	//$("#rev_diskon").val(diskon);

	$("#rev_pot").val(pot_kecil);
	$("#rev_pot_total_list").text($.number(potongan));

	$("#rev-subtotal").text($.number(subtotal));

	// start variable umum untuk modal
	$("#index_table_daftar").val(index_row);
	$("#st_tipe_produk_rev").val(st_tipe_produk);
	$("#total_potongan").val(potongan);
	$("#uk_p_rev").val(uk_p_rev);
	$("#uk_l_rev").val(uk_l_rev);
	$("#id_satuan_uk_rev").val(id_satuan_uk_rev);
	$("#jml_cetak_ref").val(jml_cetak);
	$("#id_satuan_harga_ref").val(id_satuan_harga);
	// end variable umum untuk modal

	$("#modal_revisi").modal("show");
	
}


function kondisikan_input_custom(){

	var jml = $("#daftar_order tr").length - 6;

	for (var i = 1; i <= jml; i++) {
		
		var centang_cetak 		= $("#daftar_order tr:eq("+ i +")").find("input[name='dft_st_custom_cetak[]']").is(':checked');
		var centang_finishing	= $("#daftar_order tr:eq("+ i +")").find("input[name='dft_st_custom_finishing[]']").is(':checked');
		
		if(centang_cetak){

			$("#daftar_order tr:eq("+ i +")").find("input[name='dft_rp_cetak[]']").removeAttr('readonly');

		}

		if(centang_finishing){

			$("#daftar_order tr:eq("+ i +")").find("input[name='dft_rp_finishing[]']").removeAttr('readonly');

		}

	}

}

function hitung_bayar(){

	var tagihan 		= parseFloat($("#isi_grandtotal").val());	
	var st_baru 		= parseFloat($("#st_baru").val());
	var total_bayar_all = parseFloat($("#total_bayar").val());
	var nilai_dp 		= $("#dp").val().length ? parseFloat($("#dp").val()) : 0;
	var bayar 			= $("#bayar").val().length ? parseFloat($("#bayar").val()) : 0;
	var kurang_bayar 	= 0;
	var kembalian 		= 0;

	if(st_baru > 0){//nota baru

		if(nilai_dp > 0){

			lebih_bayar 	= bayar > nilai_dp ? bayar - nilai_dp : 0;

		}else{

			lebih_bayar 	= bayar > tagihan ? bayar - tagihan : 0;
			kurang_bayar    = bayar < tagihan ? tagihan - bayar : 0;
			$("#isi_kurang_bayar").val(kurang_bayar);	
			$("#kurang_bayar").text($.number(kurang_bayar));	

		}	

	}else{//nota revisi / pelunasan

		var nilai_bayar 	= total_bayar_all + bayar;
		lebih_bayar 		= nilai_bayar > tagihan ? nilai_bayar - tagihan : 0;
		kurang_bayar 		= nilai_bayar < tagihan ? tagihan - nilai_bayar : 0;

		$("#isi_kurang_bayar").val(kurang_bayar);	
		$("#kurang_bayar").text($.number(kurang_bayar));	

	}

	$("#isi_lebih_bayar").val(lebih_bayar);
	$("#lebih_bayar").text($.number(lebih_bayar));

}


function cek_bayar(){

	var tagihan 		= parseFloat($("#isi_grandtotal").val());
	var st_baru 		= parseFloat($("#st_baru").val());
	var bayar 			= $("#bayar").val().length > 0 ? parseFloat($("#bayar").val()) : 0;
	var sisa 			= 0;
	var nilai_dp 		= $("#dp").val().length ? parseFloat($("#dp").val()) : 0;
	var total_bayar_all = parseFloat($("#total_bayar").val());

	if(st_baru > 0 ){ //transaksi nota baru

		if(bayar > 0){

			if(nilai_dp > 0){ //dibayar dp, tidak dilunasi

				sisa = bayar - nilai_dp;

			}else{ //jika dp < 0, dianggap dilunasi

				sisa = bayar >= tagihan ? bayar - tagihan : 0;

			}

		}

	}else{
		
		var nilai_total_bayar 	= 0;
		var nilai_krg_bayar 	= 0;

		if(total_bayar_all < tagihan) {

			//kondisi ini menyebabkan total_bayar_all akan masuk sebagai dp
			nilai_total_bayar 	= total_bayar_all;
			$("#bayar").removeAttr("readonly");

		}else{

			nilai_total_bayar = total_bayar_all;
			$("#bayar").prop("readonly","true");

		}

		if(nilai_total_bayar < tagihan){

			$("#dp").val(nilai_total_bayar);
			$("#bayar").val("");

		}else{

			$("#dp").val("");
			$("#bayar").val(nilai_total_bayar);

		}


		nilai_krg_bayar		= tagihan >= nilai_total_bayar ? tagihan - nilai_total_bayar : 0;
		sisa 				= nilai_total_bayar >= tagihan ? nilai_total_bayar - tagihan  : 0;

		$("#isi_kurang_bayar").val(nilai_krg_bayar);
		$("#kurang_bayar").text($.number(nilai_krg_bayar));

		
	}

	$("#lebih_bayar").text($.number(sisa));
	$("#isi_lebih_bayar").val(sisa);

}

function input_cetak(obj){

	if($(obj).is(":checked")){

		$(obj).closest("tr").find("input[name='dft_rp_cetak[]']").removeAttr("readonly");
		$(obj).closest("tr").find("input[name='dft_rp_cetak[]']").focus();

	}else{

		$(obj).closest("tr").find("input[name='dft_rp_cetak[]']").prop("readonly","true");		

	}

}


function total_all(){

	var jml 	= $("#daftar_order tbody tr").length;//dikurangi tr inputan bayar dll
	var total 	= 0;

	for (var i = 1; i <= jml; i++) {
		
		var sbtotal = parseFloat($("#daftar_order tr:eq("+ i +") td:last").text().replace(/,/g,""));
		total += sbtotal;

	}

	/*//console.log(total);

	$("#total_order").text($.number(total));
	$("#isi_total_order").val(total);

	var diskon 		= parseFloat($("#diskon_all").val());
	var potongan 	= parseFloat($("#pot_all").val());
	
	if(isNaN(diskon)) diskon = 0;
	if(isNaN(potongan)) potongan = 0;
	
	//total potongan
	diskon 		= Math.round((diskon/100)*total);
	potongan 	+= diskon;
	total 		-= potongan;*/

	total 	= pembulatan(total);

	$("#grand_total").text($.number(total));
	$("#isi_grandtotal").val(total);

	/*$("#isi_kurang_bayar").val(total);
	$("#kurang_bayar").text($.number(total,0,","))*/

	$("#tagihan").val(total);

	//cek_dp();
	//cek_bayar();

}


function input_finishing(obj){

	if($(obj).is(":checked")){

		$(obj).closest("tr").find("input[name='dft_rp_finishing[]']").removeAttr("readonly");
		$(obj).closest("tr").find("input[name='dft_rp_finishing[]']").focus();

	}else{

		$(obj).closest("tr").find("input[name='dft_rp_finishing[]']").prop("readonly","true");		

	}

}

function hitung_subtotal(obj){

	var biaya_cetak 	= parseFloat($(obj).closest("tr").find("input[name='dft_rp_cetak[]']").val());
	var biaya_finishing = parseFloat($(obj).closest("tr").find("input[name='dft_rp_finishing[]']").val());
	var diskon 			= parseFloat($(obj).closest("tr").find("input[name='dft_diskon[]']").val());
	var potongan 		= parseFloat($(obj).closest("tr").find("input[name='dft_potongan[]']").val());

	var total 			= biaya_cetak + biaya_finishing;
	
	//hitung potongan
	if(isNaN(biaya_finishing)) biaya_finishing = 0;
	if(isNaN(diskon)) diskon = 0;
	if(isNaN(potongan)) potongan = 0;

	var diskon_value 	= ((total *(diskon/100)) + potongan);

	total -= diskon_value;

	$(obj).closest("tr").find("input[name='dft_subtotal[]']").val(total);
	$(obj).closest("tr").find("td:last").text($.number(total));

	total_all();

}

function cek_kondisi_tgl_tempo_awal(){

	//var grand_total 	= parseFloat($("#isi_grandtotal").val());
	var isi_tgl_tempo 	= $("#tgl_tempo").val();
	var st_pelunasan 	= $("#st_pelunasan").val();
	var status_tempo 	= $("#st_tempo").val();

	if(status_tempo == 0 && isi_tgl_tempo.length < 0){

		var tgl_sekarang 	= new Date();

		//console.log("ok");
		//console.log(tgl_sekarang);

		$("#tgl_tempo").datepicker('update',tgl_sekarang);					

	}

 	console.log($("#tgl_tempo").datepicker("getDate"));

	ht_tempo();

	/*if(isi_tgl_tempo.length > 0){

		$("#inputan_tgl_tempo").show(400);		

	}else{

		$("#inputan_tgl_tempo").show(400);		
		// set tgl tempo
		$("#tgl_tempo").datepicker('update',new Date());					
		
		if(st_pelunasan == 0){
			// dijalankan jika posisi nota baru / bukan pelunasan
			

		}
		

	}*/



}

//Set Tanggal Tempo
function set_tempo(){

	var tgl 	= $("#tgl_order").text().split('/');
	var ctgl 	= new Date(tgl[2],tgl[1]-1,tgl[0]);
	var jmlhari = parseInt($("#ms_tempo").val());

	if (jmlhari > 0) {

		ctgl.setDate(ctgl.getDate()+jmlhari);		
		$("#tgl_tempo").datepicker('setUTCDate',ctgl);

	}else{

		$("#tgl_tempo").val("");

	}
	
}

//hitung jml hari
function ht_tempo(){

	var tgl 		= $("#tgl_order").text().split('/');
	var tgl_tran 	= new Date(tgl[2],tgl[1]-1,tgl[0]);
	var tgl_tempo 	= $("#tgl_tempo").datepicker("getUTCDate");

	//console.log(tgl_tempo);

	if(tgl_tempo < tgl_tran ){
		alert('Tanggal tempo harus lebih dari tanggal order !');
		$("#tgl_tempo").datepicker('update',tgl_tran);		
		$("#ms_tempo").val(0);
		return;
	}

	var oneday = 24*3600*1000; //Milisecond



	var diff = Math.ceil((tgl_tempo.getTime() - tgl_tran.getTime()) / oneday);

	/*if(diff > 3){

		alertify.error('Maksimal tempo 3 hari');
		$("#tgl_tempo").datepicker('update',tgl_tran);		
		diff 	= 0;

	}*/

	$("#ms_tempo").val(diff);	

}