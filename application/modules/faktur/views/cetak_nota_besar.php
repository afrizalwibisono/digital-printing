<div class="a4">
	<div class="header">
		<div class="logo">
			<img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo" />
		</div>
		<div class="address">
			<h4><?= $idt->nm_perusahaan ?></h4>
            <?= $idt->alamat ?>, <?= $idt->kota ?><br>
            Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
            Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?>
		</div>
	</div>
	<div class="body">
		<div class="summary">
			<div>
				<table class="info-konsumen">
					<tr>
						<td rowspan="3" valign="top">Kepada</td>
						<td><?= $data[0]->panggilan.' '.$data[0]->nama_konsumen ?></td>
					</tr>
					<tr>
						<td><?= $data[0]->alamat.'<br>'.ucwords($data[0]->kota) ?></td>
					</tr>
					<tr>
						<td><?= 'Telp : '.$data[0]->telp ?></td>
					</tr>
				</table>
			</div>
			<div>
				<table class="info-invoice">
					<tr>
						<td width="110">No. Invoice</td>
						<td><?= $data[0]->no_faktur ?></td>
					</tr>
					<?php if($data[0]->st_trans_master == 0): ?>
					<tr>
						<td>No. Invoice Ref.</td>
						<td><?= $data[0]->no_faktur_master ?></td>
					</tr>
					<?php endif ?>
					<tr>
						<td>Tempo</td>
						<td>
							<?php 
								if($data[0]->lama_tempo > 0){
									
									echo $data[0]->lama_tempo.' Hari';

								}else{

									echo "Lunas";

								} 
							?>

						</td>
					</tr>
					<?php if($data[0]->lama_tempo > 0): ?>
					<tr>
						<td>Tgl. Jatuh Tempo</td>
						<td><?= $data[0]->tgl_tempo ?></td>
					</tr>
					<?php endif ?>
				</table>
			</div>
		</div>
		<div class="main">
			<table>
				<thead>
					<tr>
						<th>No.</th>
						<th>Deskripsi</th>
						<th>Jml. Cetak</th>
						<th>Biaya Cetak</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>


					<?php 

						$gtotal = 0;
						foreach ($data as $key => $isi): 
					?>

					<tr>
						<td align="center"><?= str_pad($key+1, 2,'0',STR_PAD_LEFT)  ?></td>
						<td>
							<?= $isi->nama_pekerjaan ?><br>
							<?= $isi->nmkategori.' - '.$isi->nm_produk ?><br>
							<?php 
								if($isi->st_ukuran != 1) {

									echo 'Ukuran Cetak = P : '.$isi->p.' x L : '.$isi->p.' ('.$isi->tampil2.')';
												
								} 
							?>
						</td>
						<td align="center"><?= $isi->jml_cetak ?></td>
						<td align="right">
							<?php 
									$total = ($isi->biaya_cetak+$isi->potongan)+$isi->biaya_design+$isi->biaya_finishing;
									$gtotal += $total;

									$ttampil 	= $total / $isi->jml_cetak;
									echo number_format($ttampil);
							?>
						</td>
						<td align="right"> <?= number_format($total); ?> </td>
					</tr>

					<?php endforeach; ?>	

				</tbody>
			</table>
		</div>
		<div class="gtotal">
			<div>Grand Total :</div>
			<div><?= number_format($gtotal) ?></div>
		</div>
	</div>
	<div class="footer">
		<div>
			Penerima,
			<br>
			<br>
			<br>
			<br>
			[ .................................. ]
		</div>
		<div>
			Hormat kami,
			<br>
			<br>
			<br>
			<br>
			[ <?= ucwords($isi->nm_kasir) ?> ]
		</div>
	</div>
</div>
<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function(e){
		window.print();
		// window.close();
	});
</script>