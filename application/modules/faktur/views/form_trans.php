<?php
	
		$enableManage_harga 	= has_permission("Kasir Harga.Manage");
		$ENABLE_DELETE  		= has_permission('Kasir.Delete'); 

		if($this->uri->segment(3) === false){

    		$idtrans 	= "";

    	}else{

    		$idtrans 	= urldecode($this->uri->segment(3));

    	}

?>

<?= form_open_multipart($this->uri->uri_string(),[	'name'				=> 'frm_trans',
													'id'				=> 'frm_trans', 
													'role'				=> 'form',
													'class'				=> 'form-horizontal'
												]
						) ?>


<!-- Start Area global variable -->

<input type="hidden" name="st_lunas_view" id="st_lunas_view" value="<?= $dt['st_lunas'] ?>">

<input type="hidden" name="st_pelunasan" id="st_pelunasan" value="<?= $dt['st_pelunasan'] ?>" >
<!-- 
	didalam form faktur st_pelunasan sebagai penanda bahwa form ini merupakan Transaksi yang diload ulang karena adanya revisi order.
	Karena, pada form ini tidak ada yang namanya pelunasan.
 -->

<input type="hidden" name="st_baru" id="st_baru" value="<?= $dt['st_baru'] ?>">

<!-- <input type="hidden" name="tagihan" id="tagihan" value="<?= $dt['kurang_bayar'] ?>"> -->

<input type="hidden" name="id_trans" id="id_trans" value="<?= $dt['id_trans'] ?>">
<input type="hidden" name="id_trans_master" id="id_trans_master" value="<?= $dt['id_trans_master'] ?>">
<input type="hidden" name="st_trans_master" id="st_trans_master" value="<?= $dt['st_trans_master']?>" >

<!-- <input type="hidden" name="total_bayar" id="total_bayar" value="<?= $dt['total_bayar']?>"> -->

<!-- End Area global variable -->


<div class="box box-primary">
	<div class="box-body">

		<?php

			if(strlen($dt['no_faktur']) > 0):

		?>

		<div class="col-sm-3">
			<input type="hidden" name="no_nota_view" id="no_nota_view" value="<?= $dt['no_faktur'] ?>">
			<div class="form-group reduce_form_control">
				<label class="control-label nomargin_bottom_label" for="no_faktur"><?= lang("capt-trans-no-faktur") ?></label>
				<span class="form-control" id="no_faktur"><?= $dt['no_faktur'] ?></span>
			</div>
		</div>

		<?php 

			endif;
			if($dt['st_trans_master'] == 0): 
		?>
			<div class="col-sm-3">
				<input type="hidden" name="no_nota_referensi" id="no_nota_referensi" value="<?= $dt['id_trans_master'] ?>">
				<div class="form-group reduce_form_control">
					<label class="control-label nomargin_bottom_label" for="no_trans_ref"><?= lang("capt-trans-no-faktur-ref") ?></label>
					<span class="form-control" id="no_trans_ref"><?= $dt['no_faktur_master'] ?></span>
				</div>
			</div>

		<?php endif ?>

		<?php if($dt['st_baru'] == 0): ?>

			<div class="clearfix"></div>

		<?php endif; ?>


		<div class="col-sm-3">

			<input type="hidden" name="id_order_view" id="id_order_view" value="<?= $dt['no_transaksi'] ?>">

			<div class="form-group reduce_form_control">
		
				<label class="control-label nomargin_bottom_label" for="no_order" ><?=  lang('capt-trans-no-order') ?></label>
				<span class="form-control" id="no_order"><?= $dt['no_transaksi'] ?></span>

			</div>
		</div>

		<div class="col-sm-3">
			<div class="form-group reduce_form_control">

				<label class="control-label nomargin_bottom_label" for="konsumen" ><?=  lang('capt-trans-konsumen') ?></label>
				<div class="input-group">
					<input type="hidden" name="isi_id_konsumen" value="<?= $dt['id_konsumen'] ?>">
					<span class="form-control" id="konsumen"> <?= ucwords($dt['nm_konsumen']) ?> </span>
					<span class="input-group-addon" id="st_konsumen">
						<?php
								$st 	= $dt['st_konsumen'];
								switch ($st) {
									case 0:
										echo "Konsumen";
										break;
									case 1:
										echo "Reseller";
										break;
									case 2:
										echo "Instansi";
										break;	
									
								}
						?>

					</span>
				</div>
								
			</div>	
		</div>
		
		<div class="col-sm-3">
			<div class="form-group reduce_form_control">
				<label class="control-label nomargin_bottom_label" for="tgl_order" ><?=  lang('capt-trans-tglorder') ?></label>	
				<span class="form-control" id="tgl_order" >
					<?= $dt['tgl_order'] ?>
				</span>
			</div>
		</div>

		<div class="col-sm-3">
			<div class="form-group reduce_form_control">
				<label class="control-label nomargin_bottom_label" for="no_po" ><?=  lang('capt-trans-nopo') ?></label>	
				<span class="form-control" id="no_po" >
					<?= $dt['no_po'] ?>
				</span>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="col-sm-3">
			<div class="form-group reduce_form_control">
				<label class="control-label nomargin_bottom_label" for="st_tempo" ><?=  lang('capt-trans-sttempo') ?></label>	

				<?php 	if($dt['st_baru'] != 1 ){

							$disable = 'disabled';

						}else{

							$disable = '';	

						}
				?>

				<select id="st_tempo" name="st_tempo" class="form-control" <?= $disable ?> >
					<option value="0" <?= set_select("st_tempo","0",isset($dt['status_bayar']) && $dt['status_bayar'] == '0' ) ?> >Tempo</option>
					<option value="1" <?= set_select("st_tempo","1",isset($dt['status_bayar']) && $dt['status_bayar'] == '1' ) ?> >Lunas</option>
				</select>
			</div>
		</div>			

		<div class="col-sm-3" id="area_cara_bayar" style="display: none">
			<div class="form-group reduce_form_control">
				<label class="control-label nomargin_bottom_label" for="cara_bayar" ><?=  lang('capt-trans-metode-bayar') ?></label>	
				<select id="cara_bayar" name="cara_bayar" class="form-control">
					<option value="0"><?= lang("isi-metode-bayar-cash") ?></option>
					<option value="1"><?= lang("isi-metode-bayar-debit") ?></option>
					<option value="2"><?= lang("isi-metode-bayar-transfer") ?></option>
				</select>
			</div>
		</div>				

		<div class="col-sm-3" id="area-range-tempo" style="display: none">
			<div class="form-group reduce_form_control">
				<label class="control-label nomargin_bottom_label" for="tgl_tempo" ><?=  lang('capt-trans-jthtempo') ?></label>	
				<div class="input-group">
					<input style="width: 45px !important" type="text" name="ms_tempo" id="ms_tempo" class="form-control text-right" placeholder="hari" value="<?= set_value("ms_tempo", isset($dt['lama_tempo']) ? $dt['lama_tempo'] : '') ?>">
					<span class="input-group-addon">Hari</span>
					<input type="text" name="tgl_tempo" id="tgl_tempo" class="form-control" placeholder="Tgl Tempo" value="<?= set_value("tgl_tempo", isset($dt['tgl_tempo']) ? $dt['tgl_tempo'] : '') ?>" readonly >
				</div>
			</div>
		</div>

		<div class="col-sm-6" id="area-bank-transfer" style="display: none" >
			<div class="form-group reduce_form_control">
				<label class="control-label nomargin_bottom_label" for="bank_terima" ><?=  lang('capt-trans-bank') ?></label>	
				<select name="bank_terima" id="bank_terima" style="width: 100%" class="form-control">
					<?php foreach ($bank as $key => $bk) : ?>
						<option value="<?= $bk->id ?>" ><?= $bk->tampil ?></option>
					<?php endforeach ?>
				</select>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="table-responsive">
			<table class="table table-bordered" id="daftar_order">
				<thead>
					<tr class="success">
						<th width="30">#</th>
						<th colspan="2"><?= lang('capt-trans-table-deskripsi') ?></th>
						<th class="text-center"><?= lang('capt-trans-table-harga-cetak') ?></th>
						<th class="text-center"><?= lang('capt-trans-table-jml-cetak') ?></th>
						<th class="text-center"><?= lang('capt-trans-table-rp-cetak') ?></th>
						<th class="text-center"><?= lang('capt-trans-table-rp-design') ?></th>
						<th class="text-center"><?= lang('capt-trans-table-rp-finishing') ?></th>
						<!-- <th class="text-center" width="50"><?= lang('capt-trans-table-diskon') ?></th>
						<th class="text-center"><?= lang('capt-trans-table-potongan') ?></th> -->
						<th class="text-right"><?= lang('capt-trans-table-subtotal') ?></th>
					</tr>
				</thead>

				<tbody>
					
					<?php
							$numb=1;
							if(isset($dt['detai_order']) && is_array($dt['detai_order']) && count($dt['detai_order'])) :
								foreach ($dt['detai_order'] as $key => $isi) :			

									$warna_urgent 	= "";
									if($isi['st_urgent'] == 1){

										$warna_urgent = "class='bg-gray color-palette'";

									}

					?>

					<tr <?= $warna_urgent ?> >
						<td>
							<?= $numb ?>
							<!-- inputan hidden semua field -->

							<input type="hidden" name="dft_st_ubah[]" 		
							value="<?= $isi['st_ubah'] ?>">							

							<input type="hidden" name="id_detail_order[]" 		
							value="<?= $isi['id_detail_order'] ?>">
							
							<input type="hidden" name="dft_nm_pekerjaan[]" 		
							value="<?= $isi['nm_pekerjaan'] ?>">

							<input type="hidden" name="dft_kategori[]" 			
							value="<?= $isi['kategori'] ?>">

							<input type="hidden" name="dft_produk[]" 			
							value="<?= $isi['produk'] ?>">

							<input type="hidden" name="dft_st_type_produk[]" 			
							value="<?= $isi['st_tipe_produk'] ?>">							

							<!-- start ukuran cetak produk -->

							<input type="hidden" name="dft_uk_p[]"
							value="<?= $isi['uk_p'] ?>">

							<input type="hidden" name="dft_uk_l[]"
							value="<?= $isi['uk_l'] ?>">							

							<input type="hidden" name="dft_idsatuan_uk[]"
							value="<?= $isi['id_satuan_uk'] ?>">	

							<input type="hidden" name="dft_tmpl_satuan_uk[]"
							value="<?= $isi['tampil_uk_cetak'] ?>">	

							<!-- end ukuran cetak produk -->

							<input type="hidden" name="dft_st_finishing[]" 		
							value="<?= $isi['st_finishing'] ?>">

							<input type="hidden" name="dft_jml_cetak[]" 		
							value="<?= $isi['jml_cetak'] ?>">

							<input type="hidden" name="img_thumbnail[]" 		
							value="<?= $isi['thumbnail'] ?>">

							<input type="hidden" name="st_urgent[]" 			
							value="<?= $isi['st_urgent'] ?>">

							<input type="hidden" name="tgl_selesai[]" 			
							value="<?= $isi['tgl_selesai'] ?>">

							<input type="hidden" name="dft_tampil_harga[]"		
							value="<?= $isi['tampil_harga'] ?>">							

							<input type="hidden" name="dft_rp_harga_asli[]"		
							value="<?= $isi['harga'] ?>">

							<input type="hidden" name="dft_tmpl_satuan_harga[]"		
							value="<?= $isi['tampil_satuan_harga'] ?>">

							<input type="hidden" name="dft_id_satuan_harga[]"	
							value="<?= $isi['id_satuan_harga'] ?>">

							<input type="hidden" name="dft_rp_design_asli[]"	
							value="<?= $isi['biaya_design'] ?>">

							<input type="hidden" name="dft_rp_cetak_asli[]"		
							value="<?= $isi['biaya_cetak'] ?>">

							<input type="hidden" name="dft_rp_finishing_asli[]"	
							value="<?= $isi['biaya_finishing'] ?>">

							<input type="hidden" name="dft_diskon[]"			
							value="<?= $isi['diskon'] ?>">

							<input type="hidden" name="dft_pot_kecil[]"			
							value="<?= $isi['pot_kecil'] ?>">

							<input type="hidden" name="dft_potongan[]"			
							value="<?= $isi['potongan'] ?>">

							<input type="hidden" name="dft_subtotal_asli[]"     
							value="<?= $isi['subtotal'] ?>">

							<input type="hidden" name="dft_rp_harga_baru[]"		
							value="<?= $isi['harga_baru'] ?>">

							<input type="hidden" name="dft_rp_cetak_baru[]"		
							value="<?= $isi['biaya_cetak_baru'] ?>">

							<input type="hidden" name="dft_rp_design_baru[]"	
							value="<?= $isi['biaya_design_baru'] ?>">

							<input type="hidden" name="dft_rp_finishing_baru[]"	
							value="<?= $isi['biaya_finishing_baru'] ?>">

							<input type="hidden" name="dft_subtotal_baru[]"     
							value="<?= $isi['subtotal_baru'] ?>">

						</td>
						<td class="text-center thumb-daftar">
							
							<img src="<?= base_url().$isi['thumbnail'] ?>" class="img-rounded" alt="<?= lang('capt-image-null') ?>">

						</td>
						<td class="deskripsi" style="width: 20%">
							<p class="nama-pekerjaan"><?= $isi['nm_pekerjaan']?></p>
							<p><?= $isi['kategori']." - ".$isi['produk'] ?></p>
							<p>
								<?php 

									if($isi['st_urgent'] == 1){

										echo lang('capt-trans-table-status')." : <span class='text-danger'>".$isi['tgl_selesai']."</span>";

									}

								?>

							</p>
							<p>
								<?php

									if($isi['st_tipe_produk'] == 1){

										echo "Uk : ". $isi['tampil_uk_cetak'];

									}

								?>
							</p>
							<p><?= $isi['st_finishing'] ?></p>
							<p>
								<span>
									<!-- <a target="_blank" href="<?= site_url('order_produk/view_detail/'.$isi['id_detail_order']) ?>" class="text-blue" data-toggle="tooltip" data-placement="left" title="Hapus Order" ><i class="fa fa-folder"></i></a>
									| -->
									<a href="#" class="text-black" data-toggle="tooltip" data-placement="left" title="Edit Harga" onclick="edit_harga(this,event);" >
										<i class="fa fa-pencil"></i>
									</a>
									<?php 
										if($dt['st_edit_harga'] == 1) :
									?>
									|
									<a href="#" class="text-black" data-toggle="tooltip" data-placement="left" title="Reset Harga" onclick="reset_harga(this,event)"><i class="fa fa-undo"></i></a>

									<?php 	
										
										endif; 
									?>

								</span>
							</p>
							
						</td>
						<td class="text-center">
							<?= $isi['tampil_harga'] ?>
						</td>
						<td class="text-center">
							<?= $isi['jml_cetak'] ?>
						</td>
						<td class="text-center">
							<?= number_format($isi['biaya_cetak_baru']) ?>
						</td>
						<td class="text-center">
							<?php

								$biaya_design	= strlen($isi['biaya_design_baru']) <=0 ? 0 : number_format($isi['biaya_design_baru'],0);

								echo $biaya_design;
							?>
						</td>
						<td class="text-center">
							
							<?php 

								$biaya_finishing 	= strlen($isi['biaya_finishing_baru']) <= 0 ? 0 :number_format($isi['biaya_finishing_baru'],0); 

								echo $biaya_finishing;

							?>						

						</td>
						<td class="text-right">
							
							<strong id="dft_sub_total" class="text-blue">
								<?= number_format($isi['subtotal_baru']) ?>
							</strong>
							
						</td>
					</tr>

					<?php
								
									$numb++;
								endforeach;
							endif;

					?>

				</tbody>

				<tfoot class="bg-success">

					<?php

						/* status inputan potongan */
						$readonly = $dt['st_pelunasan'] == 1 ? "readonly" : "";

					?>
					<tr class="bg-primary">
						<th colspan="8" class="text-right" style="vertical-align: middle !important;">
							<a href="#" id="open_custom_total" style="color: #FFFFFF !important; display: none" data-posisi="buka" data-toggle="tooltip" data-placement="left" title="Input Potongan">
								<i id="icon_custom_total" class="fa fa-edit"></i>
							</a>
							<?= lang("capt-trans-grandtotal-all") ?>
						</th>
						<td  class="text-right">
							<input type="hidden" name="isi_grandtotal" id="isi_grandtotal" value="<?= $dt['grand_total'] ?>">
							<strong id="grand_total"> <?= number_format($dt['grand_total'],0) ?> </strong>
						</td>						
					</tr>
					<tr class="bg-success">
						<th colspan="8" class="text-right" style="vertical-align: middle !important;">
							<?= lang("capt-trans-bayar") ?>
						</th>
						<td  class="text-right">
							<input type="hidden" name="isi_bayar" id="isi_bayar" value="<?= $dt['total_bayar'] ?>">
							<input type="hidden" id="isi_dp_ref" name ="isi_dp_ref" value="<?= $dt['dp_ref'] ?>"><!-- diisi jika transaksi masternya lunas, lalu ada revisi -->
							<strong id="total_bayar"> <?= number_format($dt['total_bayar']) ?> </strong>
						</td>						
					</tr>
					<tr class="bg-red">
						<th colspan="8" class="text-right" style="vertical-align: middle !important;">
							<?= lang("capt-trans-kurang-bayar") ?>
						</th>
						<td  class="text-right">
							<input type="hidden" name="isi_kurang_bayar" id="isi_kurang_bayar" value="<?= $dt['kurang_bayar'] ?>">
							<strong id="kurang_bayar"> <?= number_format($dt['kurang_bayar']) ?> </strong>
						</td>						
					</tr>

				</tfoot>

			</table>
		</div>
		
		
		<div class="form-group">
    		<div class="col-sm-12">

    			<?php

    				if(isset($saldo_kosong) || $dt['st_lihat'] == 1) {

    					$disable 	= "disabled";
    						
    				}else{

    					$disable 	= "";

    				}

    			?>

    			<?php if($dt['st_lihat'] == 1 && $ENABLE_DELETE && isset($dt['st_history']) && $dt['st_history'] == 0) : ?>

	    			<button type="button" name="delete" id="delete" class="btn btn-danger">
	    				<?= lang("btn-delete-history") ?> <span class="fa fa-times-circle"></span> 
	    			</button>
	    			<input type="hidden" name="st_delete" id="st_delete" value="0">


    			<?php endif; ?>

    			<div class="pull-right">
	    			<?php echo anchor('faktur/cancel', lang('btn-batal'))." ".lang("bf_or")." " ?>
	    			<button type="submit" name="simpan" id="simpan" class="btn btn-primary" <?= $disable ?> >
	    				<?= lang("btn-save") ?>
	    			</button>
    			</div>
    			
    		</div>
    	</div>

	</div>

	<div class="box-footer">
		
		<div class="form-group">
			<div class="col-sm-4">
				<span class="label label-success">File Lampiran</span>		
			</div>
		</div>

		
		
		<div class="form-group">

			<?php 
				if(strlen($dt['file_po']) > 0):
			?>
				<div class="col-sm-2">
				
					<div class="kotak-gambar pull-left" style="margin-bottom: 0px">
						<img class="img-thumbnail"  src= "<?= base_url()."upload/lampiran/".$dt['file_po'] ?>" id="thum">
						<a target="_blank" href="<?= base_url()."upload/lampiran/".$dt['file_po'] ?>" class="btn btn-primary btn-xs fa fa-download download"></a>
						<!-- <span class="btn btn-primary btn-xs fa fa-download download"></span> -->
					</div>

				</div>

			<?php
				endif;
			?>

		</div>

	</div>

</div>

<?= form_close(); ?>

<!-- Modal Revisi Harga -->
<div id="modal_revisi" class="modal fade in" role="dialog">
  	<div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	    	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal">&times;</button>
	        	<h4 class="modal-title">Custom Harga</h4>
	      	</div>
	      	<div class="modal-body form-horizontal">
	      		
	      		<!-- start global variable modal -->

	        	<input type="hidden" name="index_table_daftar" 	id="index_table_daftar">
	        	<input type="hidden" name="total_potongan" 		id="total_potongan">
				
				<input type="hidden" name="st_tipe_produk_rev" 	id="st_tipe_produk_rev">

				<input type="hidden" name="uk_p_rev" 			id="uk_p_rev">
				<input type="hidden" name="uk_l_rev" 			id="uk_l_rev">
				<input type="hidden" name="id_satuan_uk_rev" 	id="id_satuan_uk_rev">

				<input type="hidden" name="jml_cetak_ref" 		id="jml_cetak_ref">

				<input type="hidden" name="id_satuan_harga_ref" id="id_satuan_harga_ref">

	        	<!-- end global variable modal -->

	        	<div class="form-group">
	        		<label class="control-label col-sm-4" for="rev_rp_harga" ><?= lang("capt-modal-rp-harga") ?></label>
	        		<div class="col-sm-4">

	        			<div class="input-group">
	        				<input type="text" name="rev_rp_harga" id="rev_rp_harga" class="form-control input-sm">	
	        				<div class="input-group-addon"><span id='tmpl_satuan_harga'></span></div>
	        			</div>
	        			
	        		</div>
	        	</div>

	        	<div class="form-group">
	        		<label class="control-label col-sm-4" for="rev_pot" ><?= lang("capt-modal-potongan") ?></label>
	        		<div class="col-sm-4">
	        			<input type="text" name="rev_pot" id="rev_pot" class="form-control input-sm">
	        		</div>
	        	</div>

	        	<div class="form-group">
	        		<label class="control-label col-sm-4" for="rev_rp_cetak" ><?= lang("capt-modal-rp-cetak") ?></label>
	        		<div class="col-sm-4">
	        			<span id="rev_rp_cetak" class="form-control input-sm"></span>
	        			<input type="hidden" name="isi_rev_rp_cetak" id="isi_rev_rp_cetak">
	        		</div>
	        	</div>

	        	<div class="form-group">
	        		<label class="control-label col-sm-4" for="rev_rp_design" ><?= lang("capt-modal-rp-design") ?></label>
	        		<div class="col-sm-4">
	        			<input type="text" name="rev_rp_design" id="rev_rp_design" class="form-control input-sm">
	        			<!-- <span class="form-control input-sm" id="rev_rp_design"></span> -->
	        		</div>
	        	</div>
	        	
	        	<div class="form-group">
	        		<label class="control-label col-sm-4" for="rev_rp_finishing" ><?= lang("capt-modal-rp-finishing") ?></label>
	        		<div class="col-sm-4">
	        			<input type="text" name="rev_rp_finishing" id="rev_rp_finishing" class="form-control input-sm">
	        		</div>
	        	</div>
	        	
	        	<hr>
	        	<div class="form-group" style="display: none">
	        		<label class="control-label col-sm-4" for="rev_diskon" ><?= lang("capt-modal-diskon") ?></label>
	        		<div class="col-sm-4">
	        			<div class="input-group">
	        				<input type="text" name="rev_diskon" id="rev_diskon" maxlength="2" class="form-control input-sm">
	        				<span class="input-group-addon bg-green color-palette"> % </span>	
	        			</div>
	        		</div>
	        	</div>

	        	<div class="form-group">
	        		<label class="control-label col-sm-4" for="rev_pot_total_list" ><?= lang("capt-modal-total-potongan") ?></label>
	        		<div class="col-sm-4">
	        			<span class="form-control input-sm text-danger" id="rev_pot_total_list"></span>
	        		</div>
	        	</div>


	        	<hr style="margin-top: 2px;margin-bottom: 10px" >

	        	<div class="form-group">
	        		<div class="col-sm-12">
	        			<span class="pull-right">
	        				<h3 id="rev-subtotal" style="margin-top: 1px;margin-bottom: 1px">0,000</h3>
	        				<input type="hidden" name="isi_rev_subtotal" id="isi_rev_subtotal">
	        			</span>
	        		</div>
	        	</div>

	      	</div>
	      	<div class="modal-footer">
	      		<?php 
					
					if($enableManage_harga) :	
						if($dt['st_edit_harga'] == 1) :

				?>
	      		<a href="#" id="tbl_terapkan" class="btn btn-primary" class="text-black" data-toggle="tooltip" data-placement="left" title="Terapkan Perubahan"> <?= lang("capt-tbl-terapkan") ?> </a>	

	      		<?php

	      				endif;
	      			endif;

	      		?>

	      	</div>
		    	
	    </div>

  	</div>
</div>

<script type="text/javascript">
	
	var id_uri_form_kasir 	= "<?= $this->uri->segment(3) ?>";

</script>