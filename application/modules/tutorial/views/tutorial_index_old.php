<div class="box box-primary">
	<div class="box-header">
		
		<?= form_open($this->uri->uri_string(),[
													'name' 	=> 'frm_index',
													'id'	=> 'frm_index',
													'role'	=> 'form',
													'class'	=> 'form-inline'
												]) ?>

		<div class="form-group pull-right">
			<div class="input-group">
				<input type="text" name="cari" id="cari" class="form-control" value="<?= set_value('cari',isset($cari) ? $cari : '' ) ?>" >
				<div class="input-group-btn">
					<button type="submit" name="search" id="search" class="btn btn-success">
						<span class="fa fa-search"></span>
					</button>
				</div>
			</div>
		</div>

		<?= form_close(); ?>

	</div>

	<?php if(isset($data) && is_array($data) && count($data)): ?>
	<div class="box-body table-responsive no-padding">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th class="text-right" width="30">#</th>
					<th class="text-center"><?= lang('tbl-judul') ?></th>
					<th class="text-center"><?= lang('tbl-update') ?></th>
				</tr>
			</thead>
			<tbody>
				<?php
						foreach ($data as $key => $isi) :
				?>

					<tr>
						<td class="text-right"><?= $key+1 ?></td>
						<td class="text-left">
							<a class="text-blue" target="_blank" href="<?= $isi->alamat?>" >
								<strong><?= ucwords($isi->judul) ?></strong>
							</a>
						</td>
						<td class="text-center"><?= $isi->tgl ?></td>
					</tr>

				<?php
						endforeach;
				?>
			</tbody>
		</table>
	</div>

	<div class="box-footer clearfix">
		
		<?php 
		echo $this->pagination->create_links(); 
		?>	

	</div>

	<?php else: ?>

	<div class="alert alert-info" role="alert">
    <p><i class="fa fa-warning"></i> &nbsp; <?= lang('konfirmasi-data-tidak-ada') ?></p>
	</div>

	<?php endif; ?>
	
</div>