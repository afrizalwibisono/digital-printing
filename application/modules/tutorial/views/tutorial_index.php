<div class="box box-primary">
	<?php if(isset($data) && is_array($data)) : ?>
	<div class="box-body table-responsive no-padding">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th class="text-center" width="30">#</th>
					<th class="text-center"><?= lang('tbl-judul') ?></th>
					<th class="text-center"><?= lang('tbl-update') ?></th>
				</tr>
			</thead>
			<tbody>
				<?php
						foreach ($data as $key => $isi) :
				?>

					<tr>
						<td class="text-right"><?= $key+1 ?></td>
						<td class="text-left">
							<a class="text-blue" target="_blank" href="<?= $isi->link?>" >
								<strong><?= ucwords($isi->judul) ?></strong>
							</a>
						</td>
						<td class="text-center"><?= $isi->tgl_update ?></td>
					</tr>

				<?php
						endforeach;
				?>
			</tbody>
		</table>
	</div>

	<?php else: ?>

	<div class="alert alert-info" role="alert">
    <p><i class="fa fa-warning"></i> &nbsp; <?= lang('konfirmasi-data-tidak-ada') ?></p>
	</div>

	<?php endif; ?>
	
</div>