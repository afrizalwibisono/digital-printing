<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for tutorial

*/

class Tutorial extends Admin_Controller {

	protected $viewPermission   		= "Tutorial.View";

    public function __construct(){

        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('tutorial/tutorial');
        $this->load->model("tutorial_model");;

        $this->template->title(lang('title'));
		$this->template->page_icon('fa fa-video-camera');
    }

	public function index(){

		$this->auth->restrict($this->viewPermission);

		$string 	= file_get_contents(APPPATH."modules/tutorial/config/source_link.json");
		$data 		= json_decode($string);

		$this->template->set('data',$data);
    	$this->template->render('tutorial_index');

	}


	// index lama
    /* public function index(){

    	$this->auth->restrict($this->viewPermission);

    	if(isset($_POST['cari'])){

    		$cari 	= isset($_POST['cari']) ? $this->input->post('cari') : '';

    	}else{

    		$cari 	= isset($_GET['cari']) ? $this->input->get('cari') : '';

    	}

    	$this->load->library('pagination');

    	$filter = "";

    	$where 	= "";

    	if(strlen($cari) > 0){

    		$cari 	= $this->db->escape_str($cari);

    		$where 	= "judul like '%{$cari}%'";

    		$filter = "?cari=".$cari;

    	}

    	$total 	= $this->tutorial_model
    					->where($where)
    					->count_all();

    	$offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

    	$this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

    	$dt 	= $this->tutorial_model
    					->select("`judul`,
								    `alamat`,
								    DATE_FORMAT(`tgl_update`, '%d/%m/%Y') AS tgl")
    					->where($where)
    					->order_by('tgl_update')
                        ->order_by('judul')
    					->limit($limit,$offset)
    					->find_all();

    	$this->template->set('cari', $cari);
    	$this->template->set('data',$dt);
    	$this->template->render('tutorial_index');

    } */


}
