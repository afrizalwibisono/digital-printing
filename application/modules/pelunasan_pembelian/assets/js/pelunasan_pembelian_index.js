$(function(){
	//Date Picker
	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	//Status Pelunasan
	$("input[name='st_pelunasan']").on("click", function(){
		var val = $(this).val();
		var st  = $(this).prop("checked") ? 1 : 0;
		
		update_status_pelunasan(val, st, this);
	})
});

function update_status_pelunasan(id_pelunasan, status, target)
{
	var conf = confirm('Apakah anda yakin akan mengubah status Pelunasan');
	if(!conf)
	{
		$(target).prop("checked", !status);
		return false;
	}

	$.ajax({
		url : baseurl+"pelunasan_pembelian/update_pelunasan",
		type : "post",
		dataType : "json",
		data : {id_pelunasan: id_pelunasan, st: status},
		success : function(data){
			var elm = $(target).closest("td").find("label.label");
			if(data['type']=='success'){
				elm.text(data['label']);
				if(status == 1){
					elm.switchClass('label-danger', 'label-success');
				}else{
					elm.switchClass('label-success', 'label-danger');
				}

		        alertify.success(data['msg']);
			}else{
				$(target).prop("checked", !status);

		        alertify.error(data['msg']);
			}
		}
	});
}