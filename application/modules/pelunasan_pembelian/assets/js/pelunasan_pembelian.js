$(function(){
	//Check All Claim
	$("#ck_all_claim").on("click", function(){
		if($(this).prop('checked')){
			$("#tbl_claim input[name='id_claim[]']").prop('checked', true);	
		}else{
			$("#tbl_claim input[name='id_claim[]']").prop('checked', false);	
		}
	});
	//Check All Retur
	$("#ck_all_retur").on("click", function(){
		if($(this).prop('checked')){
			$("#tbl_retur input[name='id_detail_retur[]']").prop('checked', true);	
		}else{
			$("#tbl_retur input[name='id_detail_retur[]']").prop('checked', false);	
		}
	});
	//No faktur
	$("#id_pembelian").select2({
		allowClear : true,
		placeholder : '-- Pilih --'
	});
	//Supplier
	$("#id_supplier").select2({ 
		placeholder : "-- Pilih Supplier --",
		allowClear : true
	});
	$("#id_supplier").on("change", function(){
		$("#t_faktur tbody").html('');
		$("#nilai_bayar").val(0);

		get_pembelian($(this).val());

		hitung_total();
	});
	$("#id_supplier").on("select2:unselect", function(){
		$("#id_pembelian option").remove();
	});
	//Date Picker
	$('#tgl_pelunasan').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});
	$('#tgl_estimasi_cair').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});
	//Cari Retur
	$("#modal-retur #cari_btn").on("click", function(){
		get_retur();
	});
	//Pilih Retur
	$("#pilih_retur").on("click", function(){
		add_retur();
	});
	$("#modal-retur #txt_cari").on("keydown", function(e){
		if(e.keyCode == 13){
			e.preventDefault();
			$("#modal-retur #cari_btn").click();
		}
	});
	//Load Pembelian
	var id_sup = $("#id_supplier").val();
	if(id_sup){
		get_pembelian(id_sup);
	}
	$("#frm_pelunasan").on("submit", function(e){
		e.preventDefault();

		$("button#save").prop('disabled', true);
		save_pelunasan();
	});
	//Metode Bayar
	$("#metode_bayar").on("change", function(){
		cek_metode_bayar($(this).val());
	});
	var metode_bayar = $("#metode_bayar").val();
	cek_metode_bayar(metode_bayar);
	//Check ALL
	$(".ck_all").on("click", function(){
		if($(this).is(':checked')){
			$(".ck_item").prop('checked', true);
		}else{
			$(".ck_item").prop('checked', false);
		}
	});
	//Format Nilai Bayar
	$("#nilai_bayar").number(true);
	$("#nilai_bayar").focus(function(){
		var val = parseFloat(this.value.replace(/,/g,""));
		if(!val){
			this.value = '';
		}
	});
});

function cek_metode_bayar(metode_bayar){
	if(metode_bayar == '' || metode_bayar == 0){
		$(".baris_bank").hide();
		$(".baris_bg").hide();
	}else if(metode_bayar == 1){
		$(".baris_bg").hide();
		$(".baris_bank").show('slow');
	}else{
		$(".baris_bg").show('slow');
		$(".baris_bank").show('slow');
	}
}

function get_pembelian(id_supplier){
	if(!id_supplier){
		return false;
	}

	$.ajax({
		url: baseurl+'pelunasan_pembelian/get_pembelian',
		type: 'post',
		data: {id_supplier: id_supplier},
		success: function(data){
			$("#t_faktur tbody").html("");
			if(data != '')
			{
				$("#t_faktur tbody").html(data);
				//Hitung total terpilih
				$(".ck_all, .ck_item").on("click", function(){
					hitung_total();
				});
			}
		}
	});
}

function get_retur(){
	var id_supplier = $("#id_supplier").val(),
		search 		= $("#modal-retur #txt_cari").val();

	if(!id_supplier){
		$("#tbl_retur tbody").html("");
		alertify.error('Silahkan pilih supplier dulu');
		
		return false;
	}

	$.ajax({
		url: baseurl+'pelunasan_pembelian/get_retur_beli',
		type: 'post',
		data: {id_supplier: id_supplier, search: search},
		success: function(data){
			$("#tbl_retur tbody").html("");
			if(data != '')
			{
				$("#tbl_retur tbody").html(data);	
			}
		}
	});
}
//Add Retur
function add_retur(){
	var $obj_ids   = $("#tbl_retur input[name='id_detail_retur[]']:checked");

	var ids_retur = $obj_ids.map(function(){
		return $(this).val();
	}).get();

	var nm_barangs = [];
	var qtys 	 = [];
	var satuans  = [];
	var hargas 	 = [];
	var sub_totals = [];

	$.each($obj_ids, function(){
		nm_barangs.push($(this).closest("tr").find("input[name='nm_barang[]']").val());
		qtys.push($(this).closest("tr").find("input[name='qty[]']").val());
		satuans.push($(this).closest("tr").find("input[name='satuan[]']").val());
		hargas.push($(this).closest("tr").find("input[name='harga[]']").val());
		sub_totals.push($(this).closest("tr").find("input[name='sub_total[]']").val());
	});

	if(ids_retur.length == 0)
	{
		alertify.error('Silahkan pilih retur yang akan ditambahkan dulu');
        return false;
	}

	var pj_ids = ids_retur.length;

	for (var i = 0; i < pj_ids; i++) {
		var $row = $("<tr>"
		  					+"<td></td>"
		  					+"<td>"
		  						+nm_barangs[i]
		  						+"<input type='hidden' name='id_detail_retur[]' value='"+ids_retur[i]+"' />"
		  						+" <a href='javascript:void(0)' onclick='del_tr(this)'><i class='fa fa-remove text-red'></i> Hapus</a>"
		  					+"</td>"
		  					+"<td>"+qtys[i]+' '+satuans[i]+"</td>"
		  					+"<td class='text-right' width='150'><input type='hidden' class='form-control text-right' name='harga[]' value='"+hargas[i]+"'>"+$.number(hargas[i])+"</td>"
		  					+"<td class='text-right' data-gtotal='"+sub_totals[i]+"'>"+$.number(sub_totals[i])+"</td>"
		  				+"</tr>");

		var pj = $("#t_retur tr").length;
		var cek = false;

		if(pj > 1){
			for (var j = 0; j <= pj - 1; j++) {
				var kode = $("#t_retur tr:eq("+(j+1)+")").find("input[name='id_detail_retur[]']").val();

				if(kode != "" && kode == ids_retur[i]){
					cek = true;
					break;
				}

			}
			
		}

		if(cek == false){
			$("#t_retur tbody").append($row);
		}	
	}

	hitung_total();

	$("#t_retur input[name='harga[]']").on("change", function(){
		var val = $(this).val().replace(/,/g,"");
		$(this).closest("tr").find("td:last").data("gtotal", val);
		$(this).closest("tr").find("td:last").text($.number(val));

		hitung_total();
	});

	$("#modal-retur").modal("hide");
	$("#modal-retur input[type='checkbox']").prop('checked', false);
	$("#t_retur input[name='harga[]']").number(true);
	//Buat Nomor
	buat_no("#t_retur");
}

function hitung_total(){
	var total_pembelian=0,
		total_retur=0,
		sisa_belum_bayar=0;
	//Pembelian
	$.each($("#t_faktur tbody tr"), function(i){
		if($(this).find(".ck_item").is(':checked')){
			var nominal = $(this).find("td:last").data('sisa');
			nominal = nominal ? nominal : 0;

			total_pembelian+= parseFloat(nominal);
		}
	});
	//Retur
	$.each($("#t_retur tbody tr"), function(i){
		var nominal = $(this).find("td:last").data('gtotal');
		nominal = nominal ? nominal : 0;

		total_retur+= parseFloat(nominal);
	});

	sisa_belum_bayar = total_pembelian - total_retur;
	sisa_belum_bayar =  sisa_belum_bayar < 0 ? 0 : sisa_belum_bayar;
	
	//Sisa Bayar
	$("#total-tagihan").text($.number(total_pembelian));
	$("#total-pengurang").text($.number(total_retur));
	$("#gtotal").text($.number(sisa_belum_bayar));
}

function buat_no(selector){
	$.each($(selector+' tbody tr'), function(i){
		$(this).find("td:first").text(i+1);
	});
}

function del_tr(obj){
	$(obj).closest("tr").remove();
	hitung_total();
}

function save_pelunasan(){
	var dataForm = $("#frm_pelunasan").serialize();

	$.ajax({
		url: baseurl+'pelunasan_pembelian/save_pelunasan',
		type: 'post',
		dataType: 'json',
		data: dataForm,
		success: function(data){
			if(data)
			{
				if(data['type']=='success'){
					alertify.success(data['msg'], 2, function(){
						window.location.href=siteurl+'pelunasan_pembelian'; 
					});
				}else{
					$("button#save").prop('disabled', false);
					alertify.error(data['msg']);
				}
			}
		}
	});
}