<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_pelunasan','name'=>'frm_pelunasan','role'=>'form')) ?>
    	<div class="box-body">
    		<div class="row">
    			<div class="col-md-3">
					<div class="form-group <?= form_error('id_supplier') ? 'has-error' : '';?>">
				    	<label for="id_supplier" class="control-label"><?= lang('pelunasan_nm_supplier') ?></label>
				    	<select id="id_supplier" name="id_supplier" class="form-control">
				    		<option></option>
				    		<?php foreach ($supplier as $key => $sp) : ?>
				    		<option value="<?= $sp->idsupplier ?>" <?= set_select('id_supplier', $sp->idsupplier, isset($data['id_supplier']) && ($data['id_supplier'] === $sp->idsupplier)) ?>><?= $sp->nm_supplier ?></option>
				    		<?php endforeach; ?>
				    	</select>
			    	</div>
			    </div>
    			<div class="col-md-3">
					<div class="form-group <?= form_error('tgl_pelunasan') ? 'has-error' : '';?>">
			            <label for="tgl_pelunasan" class="control-label"><?= lang('pelunasan_tgl') ?></label>
		                <input type="text" class="form-control" id="tgl_pelunasan" name="tgl_pelunasan" placeholder="dd/mm/yyyy" readonly=""></input>
			        </div>
				</div>
    		</div>

			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<label for="t_faktur"><?= lang('pelunasan_info_beli') ?></label>
						<table class="table table-bordered" id="t_faktur">
							<thead>
								<tr class="success">
									<td width="10"><input type="checkbox" class="ck_all" /></td>
									<th width="40"><?= lang('pelunasan_no') ?></th>
									<th width="250"><?= lang('pelunasan_no_faktur') ?></th>
									<th width="150"><?= lang('pelunasan_tgl_tempo') ?></th>
									<th class="text-right"><?= lang('pelunasan_nilai_beli') ?></th>
									<th class="text-right"><?= lang('pelunasan_terbayar') ?></th>
									<th class="text-right"><?= lang('pelunasan_sisa') ?></th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<label for="t_retur"><?= lang('pelunasan_daftar_retur') ?></label>&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-sm btn-success" id="add_retur" data-toggle="modal" data-target="#modal-retur" style="margin-bottom: 5px;"><?= lang('pelunasan_add_retur') ?></button>
						<table class="table table-bordered" id="t_retur">
							<thead>
								<tr class="success">
									<th width="40"><?= lang('pelunasan_no') ?></th>
									<th width="430"><?= lang('pelunasan_nm_barang') ?></th>
									<th width="150"><?= lang('pelunasan_qty') ?></th>
									<th class="text-right"><?= lang('pelunasan_harga') ?></th>
									<th class="text-right"><?= lang('pelunasan_total_retur') ?></th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered" id="t_bayar">
							<tr class="bg-primary">
								<th class="text-right"><?= lang('pelunasan_total_tagihan') ?></th>
								<td class="text-right" width="250"><span id="total-tagihan">0</span></td>
							</tr>
							<tr>
								<th class="text-right"><?= lang('pelunasan_total_pengurang') ?></th>
								<td class="text-right"><span id="total-pengurang">0</span></td>
							</tr>
							<tr class="bg-red">
								<th class="text-right"><?= lang('pelunasan_gtotal') ?></th>
								<td class="text-right"><strong><span id="gtotal">0</span></strong></td>
							</tr>
							<tr class="bg-primary">
								<th class="text-right"><?= lang('pelunasan_metode_bayar') ?></th>
								<td>
									<select id="metode_bayar" name="metode_bayar" class="form-control">
										<option value="">--<?= lang('pelunasan_btn_pilih') ?>--</option>
										<option value="0"><?= lang('pelunasan_bayar_cash') ?></option>
										<option value="1"><?= lang('pelunasan_bayar_transfer') ?></option>
										<option value="2"><?= lang('pelunasan_bayar_bg') ?></option>
									</select>
								</td>
							</tr>
							<tr class="baris_bg">
								<th class="text-right"><?= lang('pelunasan_no_bg') ?></th>
								<td class="text-right">
									<input type="text" name="no_bg" id="no_bg" class="form-control" value="<?= set_value('no_bg',isset($data['no_bg'])? $data['no_bg'] : '' ) ?>">
								</td>
							</tr>
							<tr class="baris_bg">
								<th class="text-right"><?= lang('pelunasan_estimasi_cair') ?></th>
								<td class="text-right">
									<input type="text" name="tgl_estimasi_cair" id="tgl_estimasi_cair" class="form-control" value="<?= set_value('tgl_estimasi_cair',isset($data['tgl_estimasi_cair']) ? $data['tgl_estimasi_cair'] : '') ?>" readonly>
								</td>
							</tr>

							<tr class="baris_bank">
								<th class="text-right"><?= lang('pelunasan_bank_pengirim') ?></th>
								<td class="text-right">
									<select id="bank_pengirim" name="bank_pengirim" class="form-control">
										<option value="0">--<?= lang('pelunasan_btn_pilih') ?>--</option>
										<?php if($bank):
											foreach ($bank as $key => $bk):
										?>
										<option value="<?= $bk->id_bank ?>"><?= $bk->nama_bank.' ('.$bk->atas_nama.')'.' - '.$bk->nomer_rek ?></option>>
										<?php endforeach;endif; ?>
									</select>
								</td>
							</tr>
							<tr class="baris_bank">
								<th class="text-right"><?= lang('pelunasan_bank_tujuan') ?></th>
								<td class="text-right">
									<input type="text" name="nm_bank_tujuan" id="nm_bank_tujuan" class="form-control" value="<?= set_value('nm_bank_tujuan',isset($data['bank_tujuan']) ? $data['bank_tujuan'] : '') ?>">
								</td>
							</tr>
							<tr class="baris_bank">
								<th class="text-right"><?= lang('pelunasan_an') ?></th>
								<td class="text-right">
									<input type="text" name="an_bank" id="an_bank" class="form-control" value="<?= set_value('an_bank',isset($data['an_bank']) ? $data['an_bank'] : '') ?>">
								</td>
							</tr>
							<tr class="baris_bank">
								<th class="text-right"><?= lang('pelunasan_norek') ?></th>
								<td class="text-right">
									<input type="text" name="no_rek_tujuan" id="no_rek_tujuan" class="form-control" value="<?= set_value('no_rek_tujuan',isset($data['no_rek_tujuan']) ? $data['no_rek_tujuan'] : '') ?>" >
								</td>
							</tr>
							<tr class="bg-green">
								<th class="text-right"><?= lang('pelunasan_nominal_bayar') ?></th>
								<td class="text-right"><input type="text" class="form-control text-right" id="nilai_bayar" name="nilai_bayar" value="0" /></td>
							</tr>
						</table>

					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 top-10">
					<div class="pull-right">
				    	<?php
		                	echo anchor('pelunasan_pembelian/cancel', lang('pelunasan_btn_cancel'), array("onclick" => "return confirm('".lang('pelunasan_cancel_confirm')."')"))." ".lang('pelunasan_or');
		                ?>
		                <button type="submit" id="save" name="save" class="btn btn-primary"><?= lang('pelunasan_btn_save') ?></button>
			    	</div>
			    </div>
			</div>
	 	</div>
	<?= form_close() ?>
</div><!-- /.box -->
<div class="modal" id="modal-retur" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
	    <div class="modal-content">
	    	<?= form_open('',array('id'=>'frm_retur','name'=>'frm_retur','role'=>'form')) ?>
		      	<div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        	<h4 class="modal-title"><?= lang('pelunasan_pilih_retur') ?></h4>
		      	</div>
		      	<div class="modal-body">
		      		<div class="form-group">
		      			<div class="input-group">
		      				<input type="text" class="form-control" id="txt_cari" name="txt_cari" placeholder="<?= lang('pelunasan_cari_retur') ?>" />
		      				<div class="input-group-btn">
		      					<button type="button" class="btn btn-primary" name="cari_btn" id="cari_btn"><i class="fa fa-search"></i> <?= lang('pelunasan_btn_cari') ?></button>
		      				</div>
		      			</div>
		      		</div>
		        	<div class="table-responsive">
		        		<table id="tbl_retur" class="table table-hover table-striped">
		        			<thead>
		        				<tr class="success">
			        				<th><input type="checkbox" id="ck_all_retur" name="ck_all_retur" /></th>
			        				<th><?= lang('pelunasan_no') ?></th>
			        				<th><?= lang('pelunasan_nm_barang') ?></th>
			        				<th><?= lang('pelunasan_qty') ?></th>
			        				<th><?= lang('pelunasan_harga') ?></th>
			        				<th><?= lang('pelunasan_total_retur') ?></th>
			        			</tr>
		        			</thead>
		        			<tbody>
		        				
		        			</tbody>
		        		</table>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
		      		<button type="button" class="btn btn-primary" id="pilih_retur" name="pilih_retur"><?= lang('pelunasan_btn_pilih') ?></button>
		      	</div>
	      	<?= form_close() ?>
	    </div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->