<?php if($data) : 
	foreach ($data as $key => $dt) : ?>
<tr>
	<td><input type="checkbox" class="ck_item" name="id_item_pembelian[]" value="<?= $dt->id_pembelian ?>" /></td>
	<td><?= $key+1 ?></td>
	<td>
		<?= $dt->no_nota ?>
	</td>
	<td><?= date('d/m/Y', strtotime($dt->tgl_tempo)) ?></td>
	<td class="text-right" data-gtotal='<?= $dt->gtotal ?>'><?= number_format($dt->gtotal) ?></td>
	<td class="text-right" data-terbayar='<?= $dt->terbayar ?>'><?= number_format($dt->terbayar) ?></td>
	<td class="text-right" data-sisa='<?= ($dt->gtotal-$dt->terbayar) ?>'><?= number_format(($dt->gtotal-$dt->terbayar)) ?></td>
</tr>
<?php endforeach;endif; ?>