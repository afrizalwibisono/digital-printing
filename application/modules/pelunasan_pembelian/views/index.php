<?php 
    $ENABLE_ADD     = has_permission('Pelunasan Pembelian.Add');
    $ENABLE_MANAGE  = has_permission('Pelunasan Pembelian.Manage');
    $ENABLE_DELETE  = has_permission('Pelunasan Pembelian.Delete');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_pelunasan','name'=>'frm_pelunasan')) ?>
        <div class="box-header">
                <?php if ($ENABLE_ADD) : ?>
	  	        <a href="<?= site_url('pelunasan_pembelian/create') ?>" class="btn btn-success" title="<?= lang('pelunasan_btn_new') ?>"><?= lang('pelunasan_btn_new') ?></a>
                <?php endif; ?>
                <div class="pull-right form-inline">
                    <div class="form-group">
                        <div class="input-daterange input-group" data-toggle="tooltip" data-placement="top" title="<?= lang('pelunasan_range_tgl') ?>">
                            <input type="text" class="form-control" name="tgl_awal" id="tgl_awal" value="" placeholder="<?= lang('pelunasan_tgl_awal') ?>" readonly style="width: 100px" />
                            <span class="input-group-addon text-black">to</span>
                            <input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" value="" placeholder="<?= lang('pelunasan_tgl_akhir') ?>" readonly style="width: 100px" />
                        </div>
                    </div>
                    <div class="input-group">
                        <input type="text" id="table_search" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="<?= lang('pelunasan_placeholder_cari') ?>" autofocus autocomplete="off" />
                        <div class="input-group-btn">
                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
        </div>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
	<div class="box-body table-responsive no-padding">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
                        <th width="50"><?= lang('pelunasan_no') ?></th>
                        <th><?= lang('pelunasan_tgl') ?></th>
                        <th><?= lang('pelunasan_nm_supplier') ?></th>
                        <th><?= lang('pelunasan_nominal_bayar') ?></th>
                        <th width="135"><?= lang('pelunasan_status') ?></th>
                        <?php if($ENABLE_MANAGE) : ?>
                        <th width="100"></th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($results as $record) : ?>
                    <tr>
                        <td class="column-check"><input type="checkbox" name="checked[]" value="<?= $record->idpelunasan ?>" /></td>
                        <td><?= $numb; ?></td>
                        <td><?= date('d/m/Y', strtotime($record->waktu)) ?></td>
                        <td><?= $record->nm_supplier ?></td>
                        <td><?= number_format($record->bayar) ?></td>
                        <td><label class="label label-<?= $record->st_cair == 1 ? 'success' : 'danger' ?>"><?= $record->st_cair == 1 ? lang('pelunasan_st_cair') : lang('pelunasan_st_belum_cair') ?></label></td>
                        <?php if($ENABLE_MANAGE) : ?>
                        <td style="padding-right:20px">
                            <a class="text-black" href="<?= site_url('pelunasan_pembelian/view/' . $record->idpelunasan); ?>" data-toggle="tooltip" data-placement="left" title="View Detail Pelunasan"><i class="fa fa-folder"></i></a>
                        </td>
                        <?php endif; ?>
                    </tr>
                    <?php $numb++; endforeach; ?>
                </tbody>
	  </table>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">
		<?php if($ENABLE_DELETE) : ?>
		<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('pelunasan_btn_delete') ?>" onclick="return confirm('<?= (lang('pelunasan_delete_confirm')); ?>')">
		<?php endif;
		echo $this->pagination->create_links(); 
		?>
	</div>
	<?php else: ?>
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('pelunasan_no_records_found') ?></p>
    </div>
    <?php
	endif;
	echo form_close(); 
	?>
</div><!-- /.box -->