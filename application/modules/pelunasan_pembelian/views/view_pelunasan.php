<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_pelunasan','name'=>'frm_pelunasan','role'=>'form')) ?>
    	<div class="box-body">
    		<div class="row">
    			<div class="col-sm-3">
					<div class="form-group">
				    	<label for="nm_supplier" class="control-label"><?= lang('pelunasan_nm_supplier') ?></label>
				    	<input type="text" class="form-control" id="nm_supplier" name="nm_supplier" value="<?= $data->nm_supplier ?>" readonly>
			    	</div>
			    </div>
    			<div class="col-sm-3">
					<div class="form-group">
			            <label for="tgl_pelunasan" class="control-label"><?= lang('pelunasan_tgl') ?></label>
		                <input type="text" class="form-control" id="tgl_pelunasan" name="tgl_pelunasan" placeholder="dd/mm/yyyy" value="<?= date('d/m/Y', strtotime($data->waktu)) ?>" readonly=""></input>
			        </div>
				</div>
    		</div>
    		
			<div class="row">
				<div class="col-sm-12">
					<div class="table-responsive">
						<label for="t_faktur"><?= lang('pelunasan_view_info_beli') ?></label>
						<table class="table table-bordered" id="t_faktur">
							<thead>
								<tr class="success">
									<th width="40"><?= lang('pelunasan_no') ?></th>
									<th width="250"><?= lang('pelunasan_no_faktur') ?></th>
									<th width="150"><?= lang('pelunasan_tgl_tempo') ?></th>
									<th class="text-right"><?= lang('pelunasan_nilai_beli') ?></th>
									<th class="text-right"><?= lang('pelunasan_telah_bayar') ?></th>
									<th class="text-right"><?= lang('pelunasan_sisa') ?></th>
								</tr>
							</thead>
							<tbody>
								<?php 
									$total_tagihan = 0;
									if($pembelian) : 
									foreach ($pembelian as $key => $dt) : ?>
								<tr>
									<td><?= $key+1 ?></td>
									<td>
										<?= $dt->no_nota ?>
									</td>
									<td><?= date('d/m/Y', strtotime($dt->tgl_tempo)) ?></td>
									<td class="text-right"><?= number_format($dt->gtotal) ?></td>
									<td class="text-right"><?= number_format($dt->terbayar) ?></td>
									<td class="text-right"><?= number_format($dt->gtotal-$dt->terbayar) ?></td>
								</tr>
								<?php 
									$total_tagihan += $dt->gtotal;
									endforeach;
									endif; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?php 
				$total_pengurang = 0;
				if($retur): ?>
			<div class="row">
				<div class="col-sm-12">
					<div class="table-responsive">
						<label for="t_retur"><?= lang('pelunasan_daftar_retur') ?></label>
						<table class="table table-bordered" id="t_retur">
							<thead>
								<tr class="success">
									<th width="40"><?= lang('pelunasan_no') ?></th>
									<th width="430"><?= lang('pelunasan_nm_barang') ?></th>
									<th width="150"><?= lang('pelunasan_qty') ?></th>
									<th class="text-right"><?= lang('pelunasan_harga') ?></th>
									<th class="text-right"><?= lang('pelunasan_total_retur') ?></th>
								</tr>
							</thead>
							<tbody>
								<?php 
									if($retur) :
									foreach ($retur as $key => $dt) : ?>
								<tr>
									<td><?= $key+1 ?></td>
									<td><?= $dt->nm_barang ?></td>
									<td><?= $dt->qty." ".$dt->nm_satuan ?></td>
									<td class="text-right"><?= number_format($dt->harga) ?></td>
									<td class="text-right"><?= number_format($dt->total) ?></td>
								</tr>
								<?php 
									$total_pengurang += $dt->total;
									endforeach;
									endif; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?php endif; ?>

			<div class="row">
				<div class="col-sm-12">
					<div class="table-responsive">
						<table class="table table-bordered">
							<tbody>
								<tr class="bg-primary">
									<th class="text-right"><?= lang('pelunasan_total_tagihan') ?></th>
									<td class="text-right" width="250"><?= number_format($total_tagihan) ?></td>
								</tr>
								<tr>
									<th class="text-right"><?= lang('pelunasan_total_pengurang') ?></th>
									<td class="text-right"><?= number_format($total_pengurang) ?></td>
								</tr>
								<tr class="bg-red">
									<th class="text-right"><?= lang('pelunasan_gtotal') ?></th>
									<td class="text-right"><strong><?= number_format($total_tagihan-$total_pengurang) ?></strong></td>
								</tr>
								<tr class="bg-primary">
									<th class="text-right"><?= lang('pelunasan_nominal_bayar') ?></th>
									<td class="text-right" width="250"><?= number_format($data->bayar) ?></td>
								</tr>
								<tr class="bg-primary">
									<?php
										switch ($data->st_bayar) {
											case 0:
												$st_bayar = lang('pelunasan_bayar_cash');
												break;
											case 1:
												$st_bayar = lang('pelunasan_bayar_transfer');
												break;
											case 2:
												$st_bayar = lang('pelunasan_bayar_bg');
												break;
											default:
												$st_bayar = '-';
												break;
										}
									?>
									<th class="text-right"><?= lang('pelunasan_metode_bayar') ?></th>
									<td class="text-right">
										<?= $st_bayar ?>
									</td>
								</tr>
								<?php if($data->st_bayar == 2): ?>
								<tr class="baris_bg">
									<th class="text-right"><?= lang('pelunasan_no_bg') ?></th>
									<td class="text-right">
										<span class="form-control"><?= $data->no_bg ?></span>
									</td>
								</tr>
								<tr class="baris_bg">
									<th class="text-right"><?= lang('pelunasan_estimasi_cair') ?></th>
									<td class="text-right">
										<span class="form-control"><?= date('d/m/Y', strtotime($data->tgl_estimasi_cair_bg)) ?></span>
									</td>
								</tr>
								<tr class="baris_bg bg-red">
									<th class="text-right"><?= lang('pelunasan_cair') ?></th>
									<td class="text-right">
										<input type="text" name="tgl_cair" id="tgl_cair" class="form-control text-right" value="<?= $data->tgl_cair ? date('d/m/Y', strtotime($data->tgl_cair)) : '' ?>" readonly>
									</td>
								</tr>
								<?php endif; ?>
								<?php if($data->st_bayar == 1 || $data->st_bayar == 2) :?>
								<tr class="baris_bank">
									<th class="text-right"><?= lang('pelunasan_bank_pengirim') ?></th>
									<td class="text-right">
										<input class="form-control" value="<?= $data->nama_bank ?>" readonly="">
									</td>
								</tr>
								<tr class="baris_bank">
									<th class="text-right"><?= lang('pelunasan_bank_tujuan') ?></th>
									<td class="text-right">
										<span class="form-control"><?= $data->bank ?></span>
									</td>
								</tr>
								<tr class="baris_bank">
									<th class="text-right"><?= lang('pelunasan_an') ?></th>
									<td class="text-right">
										<span class="form-control"><?= $data->an ?></span>
									</td>
								</tr>
								<tr class="baris_bank">
									<th class="text-right"><?= lang('pelunasan_norek') ?></th>
									<td class="text-right">
										<span class="form-control"><?= $data->no_rek ?></span>
									</td>
								</tr>
								<?php endif; ?>
								<tr class="bg-green">
									<th class="text-right"><?= lang('pelunasan_total_terbayar') ?></th>
									<td class="text-right" width="250"><?= number_format($total_terbayar) ?></td>
								</tr>
								<tr class="bg-red">
									<td class="text-right text-bold"><?= lang('pelunasan_sisa_bayar') ?></td>
									<td class="text-right text-bold"><?= number_format($total_tagihan-$total_terbayar) ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row hidden-print">
				<div class="col-sm-12 top-10 <?= $data->st_cair == 0 ? 'text-right' : '' ?>">
			    	<?php
	                	echo anchor('pelunasan_pembelian', lang('pelunasan_btn_back'), ['class' => 'btn btn-success']);
	                ?>
	                <?php if($data->st_cair == 0) : ?>
			      	<button type="submit" id="save" name="save" class="btn btn-primary"><?= lang('pelunasan_btn_save') ?></button>
			    	<?php endif; ?>
			    </div>
			</div>
	 	</div>
	<?= form_close() ?>
</div><!-- /.box -->