<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2018, Cokeshome
 * 
 * This is controller for pelunasan_pembelian
 */

class Pelunasan_pembelian extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Pelunasan Pembelian.View";
    protected $addPermission    = "Pelunasan Pembelian.Add";
    protected $managePermission = "Pelunasan Pembelian.Manage";
    protected $deletePermission = "Pelunasan Pembelian.Delete";
    protected $prefixKey        = "PB";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('pelunasan_pembelian/pelunasan_pembelian');
        $this->load->model(['pelunasan_pembelian/pelunasan_pembelian_model',
                            'pelunasan_pembelian/pelunasan_pembelian_detail_model',
                            'pelunasan_pembelian/pelunasan_pembelian_retur_model',
                            'retur_pembelian/retur_pembelian_detail_model',
                            'supplier/supplier_model',
                            'bank/bank_model',
                            'pembelian/pembelian_model',
                            'stok_model'
                            ]);

        $this->template->title(lang('pelunasan_title'));
        $this->template->page_icon('fa fa-cc-mastercard');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if (isset($_POST['delete']) && has_permission($this->deletePermission))
        {
            $checked = $this->input->post('checked');
            if($checked)
            {
                $jml_success = 0;
                foreach ($checked as $key => $pid) {
                    $dt_pelunasan = $this->pelunasan_pembelian_model->find($pid);

                    $dt_pembelian = $this->pelunasan_pembelian_detail_model
                                                    ->find_all_by(['idpelunasan' => $pid]);

                    //Hapus Pelunasan
                    $this->db->trans_start();
                    $deleted = $this->pelunasan_pembelian_model->delete($pid);
                    $query_hapus = $this->db->last_query();
                    if($deleted)
                    {
                        $keterangan = "SUKSES, hapus data Pelunasan Pembelian dengan ID : ".$pid;
                        $status     = 1;
                        $jml_success++;
                        //Delete Detail Pelunasan
                        $this->pelunasan_pembelian_detail_model->delete_where(['idpelunasan' => $pid]);
                        //Kembalikan alur kas jika sudah cair
                        if($dt_pelunasan->st_cair == 1)
                        {
                            simpan_alurkas(6, "Pembatalan pembayaran ke supplier untuk pelunasan pembelian dengan ID : ".$dt_pelunasan->idpelunasan." karena penghapusan data pelunasan", $dt_pelunasan->bayar , 1, $this->addPermission);
                        }
                        //Retur
                        $dt_retur = $this->pelunasan_pembelian_retur_model->find_all_by(['id_pelunasan' => $pid]);
                        if($dt_retur)
                        {
                            foreach ($dt_retur as $key => $rt) {
                                $this->retur_pembelian_detail_model->update($rt->id_retur, ['st_pakai_potong' => 0, 'tgl_pakai_potongan' => '']);
                                $query_hapus  .= $this->db->last_query();
                            }
                        }
                        //Update Pembelian
                        //Update jadi Belum Lunas
                        foreach ($dt_pembelian as $key => $bl) {
                            $this->pembelian_model->update($bl->id_transaksi, ['st_lunas' => 0]);
                            $query_hapus  .= $this->db->last_query();
                        }
                        
                        $this->db->trans_complete();
                    }
                    else
                    {
                        $keterangan = "GAGAL, hapus data Pelunasan Pembelian dengan ID : ".$pid;
                        $status     = 0;
                    } 

                    $nm_hak_akses   = $this->deletePermission; 
                    $kode_universal = $pid;
                    $jumlah         = $dt_pelunasan->bayar;
                    $sql            = $query_hapus;

                    simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                }

                if ($jml_success > 0)
                {
                    $this->template->set_message($jml_success .' '. lang('pelunasan_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('pelunasan_del_failure')."\n\n".$this->pelunasan_pembelian_model->error, 'error');
                } 
            }
            else
            {
                $this->template->set_message(lang('pelunasan_del_error'), 'error');
            }

            unset($_POST);
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';
            $tgl_awal  = $this->input->post('tgl_awal');
            $tgl_akhir = $this->input->post('tgl_akhir');
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
            $tgl_awal  = $this->input->get('tgl_awal');
            $tgl_akhir = $this->input->get('tgl_akhir');
        }

        $filter = "?search=".urlencode($search);
        $search2 = $this->db->escape_str($search);

        $add_where = "";
        if($tgl_awal !='' && $tgl_akhir !='')
        {
            $filter    .= "&tgl_awal=".urlencode(date_ymd($tgl_awal))."&tgl_akhir=".urlencode(date_ymd($tgl_akhir));
            $add_where .= " AND (waktu >='".date_ymd($tgl_awal)."' AND waktu <='".date_ymd($tgl_akhir)."')";
        }

        $where = "`pelunasan_transaksi.deleted` = 0 $add_where
                AND (`nm_supplier` LIKE '%$search2%' ESCAPE '!'
                OR `bayar` LIKE '%$search2%' ESCAPE '!')";
        
        $total = $this->pelunasan_pembelian_model
                    ->select("pelunasan_transaksi.*, nm_supplier")
                    ->join('supplier','pelunasan_transaksi.id_supplier = supplier.idsupplier')
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->pelunasan_pembelian_model
                    ->select("pelunasan_transaksi.*, nm_supplier")
                    ->join('supplier','pelunasan_transaksi.id_supplier = supplier.idsupplier')
                    ->where($where)
                    ->order_by('pelunasan_transaksi.created_on','DESC')
                    ->limit($limit, $offset)
                    ->find_all();

        $assets = array('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'pelunasan_pembelian/assets/js/pelunasan_pembelian_index.js'
                        );

        add_assets($assets);

        $this->template->set('results', $data);
        $this->template->set('search', $search);
       
        $this->template->title(lang('pelunasan_title'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

    //Create Pelunasan
    public function create()
    {
        $this->auth->restrict($this->addPermission);
        //cek session data
        $data = $this->session->userdata('pl_pembelian');
        if(!$data)
        {
            $data = array('pl_pembelian' => array(
                                        'items'         => array(),
                                        'id_pembelian'  => '',
                                        'tgl_pelunasan' => '',
                                        'id_supplier'   => '',
                                        'selected_ids_beli' => []
                                        ));

            $this->session->set_userdata($data);
            $data = $this->session->userdata('pl_pembelian');
        }

        $supplier = $this->supplier_model->order_by('nm_supplier', 'ASC')
                                        ->find_all_by(['deleted' => 0]);

        $bank   = $this->bank_model->find_all_by(['deleted' => 0]);

        $assets = array('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        "plugins/number/jquery.number.js",
                        "pelunasan_pembelian/assets/js/pelunasan_pembelian.js"
                        );

        add_assets($assets);

        $this->template->set('supplier', $supplier);
        $this->template->set('data', $data);
        $this->template->set('bank', $bank);
        $this->template->title(lang('pelunasan_title_new'));
        $this->template->render('pelunasan_form');
    }

    //View Pelunasan
    public function view($id_pelunasan = 0)
    {
        if(!$id_pelunasan)
        {
            $this->template->set_message(lang('pelunasan_invalid_id'),'error');
            redirect('pelunasan_pembelian');
        }

        if(isset($_POST['save'])){
            $update = $this->update_pelunasan($id_pelunasan);
            if($update){
                $this->template->set_message(lang('pelunasan_edit_success'), 'success');
            }
        }

        $dt_pelunasan = $this->pelunasan_pembelian_model
                                                ->select(['pelunasan_transaksi.*', 'nm_supplier', "concat(nama_bank,' (', bank.atas_nama, ') - ', bank.nomer_rek) as nama_bank"])
                                                ->join('supplier','pelunasan_transaksi.id_supplier=supplier.idsupplier')
                                                ->join('bank', 'pelunasan_transaksi.id_bank=bank.id_bank','left')
                                                ->find($id_pelunasan);

        $dt_pembelian = $this->pelunasan_pembelian_detail_model
                                        ->select("pelunasan_transaksi_detail.*, no_nota, tgl_tempo, gtotal")
                                        ->join('pembelian','pelunasan_transaksi_detail.id_transaksi=pembelian.id_pembelian','left')
                                        ->find_all_by(['idpelunasan' => $id_pelunasan]);
        $total_terbayar = 0;
        if($dt_pembelian){
            foreach ($dt_pembelian as $key => $val) {
                // Get Nilai Retur
                $nilai_retur = $this->get_nilai_retur($val->id_transaksi);
                $terbayar = $this->get_terbayar($val->id_transaksi);
                $dt_pembelian[$key]->terbayar = $terbayar+$nilai_retur;
                $total_terbayar += $terbayar+$nilai_retur;
            }
        }

        $dt_retur     = $this->pelunasan_pembelian_retur_model
                                                    ->select('pelunasan_transaksi_retur.*, nm_barang, barcode, konversi_satuan.satuan_besar as nm_satuan')
                                                    ->join('barang','pelunasan_transaksi_retur.id_barang=barang.idbarang_bb')
                                                    ->join('konversi_satuan','pelunasan_transaksi_retur.id_konversi = konversi_satuan.id_konversi')
                                                    ->find_all_by('id_pelunasan',$id_pelunasan);

        $this->template->set('data', $dt_pelunasan);
        $this->template->set('pembelian', $dt_pembelian);
        $this->template->set('retur', $dt_retur);
        $this->template->set('total_terbayar', $total_terbayar);

        $assets = ['plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                   'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                   'pelunasan_pembelian/assets/js/pelunasan_view.js'
                ];

        add_assets($assets);

        $this->template->title(lang('pelunasan_title_view'));

        $this->template->render('view_pelunasan');
    }

    public function get_pembelian()
    {
        if(!$this->input->is_ajax_request())
        {
            echo '';
        }

        $id_supplier = $this->input->post('id_supplier');

        $dt_pembelian = $this->pembelian_model->order_by('created_on', 'ASC')
                                        ->find_all_by(['deleted' => 0, 'type' => 0, 'st_lunas' => 0, 'id_supplier' => $id_supplier]);
        if($dt_pembelian){
            foreach ($dt_pembelian as $key => $val) {
                // Get Nilai Retur
                $nilai_retur = $this->get_nilai_retur($val->id_pembelian);
                $dt_pembelian[$key]->terbayar = $this->get_terbayar($val->id_pembelian);
                $dt_pembelian[$key]->terbayar += $nilai_retur;
            }
        }

        echo $this->template->load_view('partial/daftar_beli_terpilih', ['data' => $dt_pembelian]);
    }

    protected function get_nilai_retur($id_pembelian = []){
        $dt_pelunasan = $this->pelunasan_pembelian_detail_model
                                        ->select("sum(nilai_retur) as nilai_retur")
                                        ->where_in('id_transaksi', $id_pembelian)
                                        ->find_all_by(['deleted' => 0]);
        if($dt_pelunasan){
            return $dt_pelunasan[0]->nilai_retur;
        }

        return 0;
    }

    protected function get_terbayar($id_pembelian = [], $datetime_acuan = NULL)
    {
        $where = ['deleted' => 0];
        if($datetime_acuan){
            $where['created_on <'] = $datetime_acuan;
        }

        $dt_pelunasan = $this->pelunasan_pembelian_detail_model
                                        ->select('sum(cicilan) as total_terbayar')
                                        ->where_in('id_transaksi', $id_pembelian)
                                        ->find_all_by($where);

        if($dt_pelunasan){
            return $dt_pelunasan[0]->total_terbayar;
        }

        return 0;
    }

    public function get_retur_beli()
    {
        if(!$this->input->is_ajax_request())
        {
            echo '';
        }

        $id_supplier = $this->input->post('id_supplier');
        $search      = $this->input->post('search');
        $dt_retur_beli = $this->retur_pembelian_detail_model
                                        ->select("retur_pembelian_detail.*, nm_barang, barcode, konversi_satuan.satuan_besar as nm_satuan")
                                        ->join('retur_pembelian','retur_pembelian_detail.id_retur=retur_pembelian.id_retur')
                                        ->join('barang','retur_pembelian_detail.id_barang=barang.idbarang_bb')
                                        ->join('konversi_satuan','retur_pembelian_detail.id_konversi = konversi_satuan.id_konversi')
                                        ->like('nm_barang', $search)
                                        ->find_all_by(['st_pakai_potong' => 0, 'id_supplier' => $id_supplier]);
        if($dt_retur_beli)
        {
            foreach ($dt_retur_beli as $key => $dt) {
                $dt_real_qty = hitung_ke_satuan_kecil($dt->id_konversi, $dt->qty);
                $real_qty  = $dt_real_qty['qty'];

                $harga       = $this->stok_model->get_hpp($dt->id_barang);

                if($real_qty != $dt->qty)
                {
                    $harga = $harga * $real_qty;
                }

                $dt_retur_beli[$key]->harga = $harga;
                $dt_retur_beli[$key]->sub_total = $harga * $dt->qty;
            }
        }
        echo $this->template->load_view('partial/daftar_retur', ['data' => $dt_retur_beli]);
    }

    public function save_pelunasan()
    {
        $this->auth->restrict($this->addPermission);
                    
        if(!$this->input->is_ajax_request())
        {
            die(json_encode(['type' => 'error', 'msg' => lang('pelunasan_only_ajax')]));
        }

        $_POST['nilai_bayar']   = floatval(remove_comma($this->input->post('nilai_bayar')));

        $id_supplier   = $this->input->post('id_supplier');
        $tgl_pelunasan = $this->input->post('tgl_pelunasan');
        $metode_bayar  = $this->input->post('metode_bayar');
        $nilai_bayar   = $this->input->post('nilai_bayar');

        $no_bg              = $this->input->post('no_bg');
        $tgl_estimasi_cair  = $this->input->post('tgl_estimasi_cair');

        $bank_pengirim      = $this->input->post('bank_pengirim');
        $nm_bank_tujuan     = $this->input->post('nm_bank_tujuan');
        $an_bank        = $this->input->post('an_bank');
        $no_rek_tujuan  = $this->input->post('no_rek_tujuan');

        $id_pembelian       = $this->input->post('id_item_pembelian');
        $id_detail_returs   = $this->input->post('id_detail_retur');
        
        $sisa_bayar         = $this->hitung_sisa_bayar($id_pembelian, $id_detail_returs);
        
        $this->form_validation->set_rules('id_supplier','lang:pelunasan_nm_supplier','required');
        $this->form_validation->set_rules('tgl_pelunasan','lang:pelunasan_tgl','required|trim');
        $this->form_validation->set_rules('metode_bayar','lang:pelunasan_metode_bayar',"callback_default_select");
        $this->form_validation->set_rules('nilai_bayar','lang:pelunasan_nominal_bayar',"greater_than[0]|less_than_equal_to[$sisa_bayar]");
        if($_POST['metode_bayar'] == 1){ //Jika Metode Transfer
            $this->form_validation->set_rules('bank_pengirim','lang:pelunasan_bank_pengirim',"callback_default_select");
            $this->form_validation->set_rules('nm_bank_tujuan','lang:pelunasan_bank_tujuan',"required");
            $this->form_validation->set_rules('an_bank','lang:pelunasan_an',"required");
            $this->form_validation->set_rules('no_rek_tujuan','lang:pelunasan_norek',"required");
        }
        if($_POST['metode_bayar'] == 2){ //Jika Metode BG
            $this->form_validation->set_rules('no_bg','lang:pelunasan_no_bg',"required|trim");
            $this->form_validation->set_rules('tgl_estimasi_cair','lang:pelunasan_estimasi_cair',"required|trim");
            
            $this->form_validation->set_rules('bank_pengirim','lang:pelunasan_bank_pengirim',"callback_default_select");
            $this->form_validation->set_rules('nm_bank_tujuan','lang:pelunasan_bank_tujuan',"required|trim");
            $this->form_validation->set_rules('an_bank','lang:pelunasan_an',"required|trim");
            $this->form_validation->set_rules('no_rek_tujuan','lang:pelunasan_norek',"required|trim");
        }
        $this->form_validation->set_message('default_select', 'You should choose the "%s" field');
        
        if($this->form_validation->run() === FALSE)
        {
            die(json_encode(['type' => 'error', 'msg' => validation_errors()]));
        }

        $this->db->trans_start();
        $query = "";

        $data_pelunasan = [
                            'idpelunasan'  => gen_primary($this->prefixKey),
                            'tipe' => 2, //Pelunasan Pembelian
                            'id_supplier'   => $id_supplier,
                            'waktu' => date_ymd($tgl_pelunasan).' '.date('H:i:s'),
                            'st_bayar' => $metode_bayar,
                            'tgl_estimasi_cair_bg' => $tgl_estimasi_cair ? date_ymd($tgl_estimasi_cair) : NULL,
                            'st_cair' => $metode_bayar == 2 ? 0 : 1, //Belum Cair jika BG
                            'no_bg'    => $no_bg,
                            'an'       => $an_bank,
                            'bank'     => $nm_bank_tujuan,
                            'no_rek'   => $no_rek_tujuan,
                            'id_bank'  => $bank_pengirim,
                            'bayar'    => $nilai_bayar
                            ];

        $this->pelunasan_pembelian_model->insert($data_pelunasan);
        $query .= $this->db->last_query();

        //Pelunasan Detail
        $data_detail        = [];
        $data_update_lunas  = [];
        $total_pengurang    = $this->hitung_total_pengurang($id_detail_returs);
        if($id_pembelian){
            $total_bayar = $nilai_bayar;
            foreach ($id_pembelian as $key => $val) {
                $dt_pembelian = $this->pembelian_model->select('gtotal')->find($val);
                $dt_terbayar  = $this->get_terbayar($id_pembelian);
                $dt_sisa      = $dt_pembelian->gtotal - $dt_terbayar;
                $nilai_retur  = 0;

                if($total_bayar >= $dt_sisa){
                    $total_pelunasan = $dt_sisa;
                    $total_bayar    -= $dt_sisa;
                }else{
                    $total_pelunasan = $total_bayar;
                    $total_bayar     = 0;
                }

                if($total_pengurang > 0){
                    if($total_pengurang >= $total_pelunasan){
                        $nilai_retur      = $total_pelunasan;
                        $total_pengurang -= $total_pelunasan;
                    }else{
                        $nilai_retur     = $total_pengurang;
                        $total_pengurang = 0;
                    }
                }

                //Detail Transaksi Pelunasan
                $data_detail[] = ['idpelunasan_transaksi_detail' => gen_primary(),
                                  'idpelunasan'  => $data_pelunasan['idpelunasan'],
                                  'id_transaksi' => $val,
                                  'cicilan'      => $total_pelunasan,
                                  'nilai_retur'  => $nilai_retur,
                                  'st_cicilan'   => 1 // Cicilan pelunasan, 0 = DP
                                ];
                //Data untuk update status pembelian
                $data_update_lunas[] = ['id_pembelian' => $val, 'st_lunas' => 1];

                if($total_bayar == 0){
                    break;
                }
            }
            //Save Detail
            if($data_detail){
                $this->pelunasan_pembelian_detail_model->insert_batch($data_detail);
                $query  .= $this->db->last_query();
            }
            //Update Status lunas Pembelian
            $sisa_bayar = $this->hitung_sisa_bayar($id_pembelian, $id_detail_returs);
            if($sisa_bayar == 0){
                $this->pembelian_model->update_batch($data_update_lunas, 'id_pembelian');
                $query  .= $this->db->last_query();
            }
        }

        //Retur
        $data_retur         = [];
        $data_update_retur  = [];
        if($id_detail_returs)
        {
            $dt_retur = $this->retur_pembelian_detail_model->where_in('id_detail_retur', $id_detail_returs)
                                                        ->find_all();
            foreach ($dt_retur as $key => $rt) {
                foreach ($id_detail_returs as $key2 => $val) {
                    if($val == $rt->id_detail_retur)
                    {
                        $qty    = $rt->qty;
                        $harga  = $rt->harga;
                        $total_retur = $qty * $harga;

                        $data_retur[] = [
                                            'id_pelunasan_retur' => gen_primary(),
                                            'id_pelunasan' => $data_pelunasan['idpelunasan'],
                                            'id_retur'     => $val,
                                            'id_barang'    => $rt->id_barang,
                                            'qty'          => $qty,
                                            'id_konversi'  => $rt->id_konversi,
                                            'harga'        => $harga,
                                            'total'        => $total_retur
                                        ];
                        //Update status pakai retur
                        $data_update_retur[] = ['id_detail_retur' => $val, 
                                                'st_pakai_potong' => 1, 
                                                'tgl_pakai_potongan' => date('Y-m-d H:i:s')
                                            ];
                        break;
                    }
                }
            }
        }

        if($data_retur)
        {
            $this->pelunasan_pembelian_retur_model->insert_batch($data_retur);
            $query .= $this->db->last_query();
        }
        //Update Status Pakai Retur
        if($data_update_retur){
            $this->retur_pembelian_detail_model->update_batch($data_update_retur, 'id_detail_retur');
            $query  .= $this->db->last_query();
        }
        //Simpan Alur Kas, Kode 6 untuk pembayaran pembelian tempo
        if($metode_bayar == "0" || $metode_bayar == "1"){ //Jika Tunai atau Transfer
            simpan_alurkas(6, "Pembayaran ke supplier untuk pelunasan pembelian dengan ID : ".($data_pelunasan['idpelunasan'])." untuk pembelian dengan ID : ".implode(', ', $id_pembelian) , $nilai_bayar , 0, $this->addPermission, ($metode_bayar == 1 || $metode_bayar == 2) ? 1 : 0, $bank_pengirim ? $bank_pengirim : 0);
        }
        
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE)
        {
            echo json_encode(['type' => 'error', 'msg' => lang('pelunasan_create_failure')]);

            $keterangan = "GAGAL, tambah data pelunasan pembelian untuk pembelian dengan ID : ".($data_pelunasan['idpelunasan'])." untuk pembelian dengan ID : ".implode(', ', $id_pembelian);
            $status     = 0;
        }
        else
        {
            echo json_encode(['type' => 'success', 'msg' => lang('pelunasan_create_success')]); 

            $keterangan = "SUKSES, tambah data pelunasan pembelian dengan ID : ".($data_pelunasan['idpelunasan'])." untuk pembelian dengan ID : ".implode(', ', $id_pembelian);  
            $status     = 1;
        }

        //Save Log
        $nm_hak_akses   = $this->addPermission; 
        $kode_universal = $data_pelunasan['idpelunasan'];
        $jumlah         = $nilai_bayar;
        $sql            = $query;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

    }

    protected function hitung_sisa_bayar($id_pembelian=[], $id_detail_returs = [])
    {
        if(!$id_pembelian)
        {
            return 0;
        }

        $total_pembelian = 0;
        $total_terbayar = 0;
        $total_pengurang = 0;
        
        $dt_pembelian = $this->pembelian_model->select(['sum(gtotal) as gtotal'])
                                            ->where_in('id_pembelian', $id_pembelian)
                                            ->find_all_by('deleted', 0);
        $total_pembelian = $dt_pembelian[0]->gtotal;
        //Get Terbayar
        $total_terbayar = $this->get_terbayar($id_pembelian);
        //Pengurang
        $total_pengurang = $this->hitung_total_pengurang($id_detail_returs);

        $sisa_bayar = $total_pembelian - $total_terbayar - $total_pengurang;
        return $sisa_bayar;
    }

    protected function hitung_total_pengurang($id_detail_returs = [])
    {
        $total_retur = 0;
        
        if($id_detail_returs)
        {
            $dt_retur = $this->retur_pembelian_detail_model->where_in('id_detail_retur', $id_detail_returs)
                                                        ->find_all();
            foreach ($dt_retur as $key => $rt) {
                foreach ($id_detail_returs as $key2 => $val) {
                    if($val == $rt->id_detail_retur)
                    {
                        $qty    = $rt->qty;
                        $harga  = $rt->harga;

                        $total_retur += $qty * $harga;
                        break;
                    }
                }
            }
        }

        return $total_retur;
    }

    public function cancel()
    {
        $this->auth->restrict($this->addPermission);

        $this->session->unset_userdata('pl_pembelian');
        $this->template->set_message(lang('pelunasan_canceled'), 'success');

        redirect('pelunasan_pembelian');
    }

    public function update_pelunasan($id_pelunasan = '')
    {
        $this->auth->restrict($this->managePermission);

        $tgl_cair  = $this->input->post('tgl_cair');

        $this->form_validation->set_rules('tgl_cair', 'lang:pelunasan_cair', 'required');
        if(!$this->form_validation->run()){
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        if($id_pelunasan !='')
        {
            $id_pembelian = [];
            $dt_pembelian = $this->pelunasan_pembelian_detail_model
                                                    ->find_all_by(['idpelunasan' => $id_pelunasan]);

            $dt_pelunasan = $this->pelunasan_pembelian_model->find($id_pelunasan);

            foreach ($dt_pembelian as $key => $val) {
                $id_pembelian[] = $val->id_transaksi;
            }

            $this->db->trans_start();

            $update = $this->pelunasan_pembelian_model->update($id_pelunasan, ['tgl_cair' => date_ymd($tgl_cair), 'st_cair' => 1]);
            $query  = $this->db->last_query();
            if($update)
            {
                $keterangan = 'SUKSES, update tanggal pencairan pelunasan pembelian dengan ID : '.$id_pelunasan;
                $st_log = 1;
                //Save Alur Kas Keluar
                simpan_alurkas(6, "Pembayaran ke supplier untuk Pelunasan Pembelian dengan ID: ".$id_pelunasan.' untuk pembelian dengan ID :'.implode(', ', $id_pembelian), $dt_pelunasan->bayar , 0, $this->addPermission, 1, $dt_pelunasan->id_bank, date_ymd($tgl_cair).' '.date('H:i:s'));
                
                $query  .= $this->db->last_query();
            }
            else
            {
                $keterangan = 'GAGAL, update tanggal pencairan pelunasan pembelian dengan ID : '.$id_pelunasan;
                $st_log = 0;
            }

            $this->db->trans_complete();
        }
        else
        {
            $st_log = 0;
            $keterangan = 'GAGAL, update status Pelunasan, invalid input';
        }

        unset($_POST);

        //Save Log
        $nm_hak_akses   = $this->managePermission; 
        $kode_universal = $id_pelunasan;
        $jumlah         = $dt_pelunasan ? $dt_pelunasan->bayar : 0;
        $sql            = $query;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $st_log);

        if($update){
            return TRUE;
        }

        $this->template->set_message(lang('pelunasan_edit_failed'), 'error');
        return FALSE;
    }

    public function default_select($val)
    {
        return $val == "" ? FALSE : TRUE;
    }
}
?>