<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['pelunasan_title'] 	= 'Pelunasan Pembelian';
$lang['pelunasan_title_new']= 'Pelunasan Pembelian Baru';
$lang['pelunasan_title_view']= 'Detail Pelunasan Pembelian';
$lang['pelunasan_title_cetak']= 'Cetak Pelunasan Pembelian';

$lang['pelunasan_tgl'] 		= 'Tanggal Pencatatan';
$lang['pelunasan_no_faktur'] 		= 'Faktur Pembelian';
$lang['pelunasan_nm_supplier'] 		= 'Nama Supplier';
$lang['pelunasan_total_tagihan'] 	= 'Total Tagihan';
$lang['pelunasan_total'] 			= 'Total Pembayaran';
$lang['pelunasan_total_pengurang'] 	= 'Total Pengurang';
$lang['pelunasan_gtotal'] 		= 'Grand Total';
$lang['pelunasan_catatan'] 		= 'Catatan';
$lang['pelunasan_status'] 		= 'Status';
$lang['pelunasan_st_cair']  	  = 'Telah Cair';
$lang['pelunasan_st_belum_cair']  = 'Belum Cair';

$lang['pelunasan_info_beli'] 	= 'Informasi Pembelian [<span class="text-red">centang tagihan yang akan dibayar</span>]';
$lang['pelunasan_view_info_beli'] = 'Informasi Pembelian';
$lang['pelunasan_telah_bayar'] 	= 'Telah Terbayar';
$lang['pelunasan_daftar_retur'] = 'Daftar Retur';
$lang['pelunasan_tgl_bayar'] 	= 'Tanggal Pembayaran';
$lang['pelunasan_no'] 		 	= '#';
$lang['pelunasan_nm_barang'] 	= 'Nama Barang';
$lang['pelunasan_harga'] 		= 'Harga';
$lang['pelunasan_qty'] 		 	= 'Qty';
$lang['pelunasan_total_retur'] 	= 'Total Retur';
$lang['pelunasan_nilai_beli']   = 'Nilai Pembelian';
$lang['pelunasan_terbayar'] 	= 'Terbayar';
$lang['pelunasan_total_terbayar'] = 'Total Terbayar';
$lang['pelunasan_sisa'] 		= 'Belum Dibayar';
$lang['pelunasan_nominal_bayar']= 'Nominal Bayar';
$lang['pelunasan_nm_potongan'] = 'Nama Potongan';
$lang['pelunasan_nilai_potongan'] = 'Nilai Potongan';
$lang['pelunasan_sisa_bayar']= 'Sisa yang Belum Dibayar';
$lang['pelunasan_potongan_lain'] = 'Potongan Lain-lain';
$lang['pelunasan_tgl_tempo'] = 'Jatuh Tempo';
$lang['pelunasan_range_tgl'] = 'Range Tanggal';
$lang['pelunasan_tgl_awal']  = 'Tgl. awal';
$lang['pelunasan_tgl_akhir'] = 'Tgl. akhir';
$lang['pelunasan_pilih_claim'] = 'Pilih Claim';
$lang['pelunasan_cari_claim']  = 'Cari Claim...';
$lang['pelunasan_pilih_retur'] = 'Pilih Retur';
$lang['pelunasan_cari_retur']  = 'Cari Retur...';
$lang['pelunasan_metode_bayar']= 'Metode Pembayaran';
$lang['pelunasan_bayar_cash']  = 'Cash/Tunai';
$lang['pelunasan_bayar_transfer'] = 'Transfer';
$lang['pelunasan_bayar_bg'] 	= 'BG';
$lang['pelunasan_no_bg'] 		= 'No. BG';
$lang['pelunasan_estimasi_cair']= 'Tgl. Estimasi Cair';
$lang['pelunasan_cair'] 		= 'Tgl. Pencairan';
$lang['pelunasan_bank_pengirim'] = 'Bank Pengirim';
$lang['pelunasan_an'] 		= 'Atas Nama';
$lang['pelunasan_norek'] 	= 'No. Rekening';
$lang['pelunasan_bank_tujuan'] 	= 'Bank Tujuan';

// button
$lang['pelunasan_btn_new'] 		= 'Baru';
$lang['pelunasan_btn_delete'] 	= 'Hapus';
$lang['pelunasan_btn_save'] 	= 'Simpan';
$lang['pelunasan_btn_cancel'] 	= 'Batal';
$lang['pelunasan_btn_back'] 	= 'Kembali';
$lang['pelunasan_or'] 			= 'atau';
$lang['pelunasan_add_claim'] 	= 'Tambah Claim';
$lang['pelunasan_add_retur'] 	= 'Tambah Retur';
$lang['pelunasan_add_potongan'] = 'Tambah Potongan';
$lang['pelunasan_btn_cari'] 	= 'Cari';
$lang['pelunasan_btn_pilih'] 	= 'Pilih';

// messages
$lang['pelunasan_del_error']		= 'Anda belum memilih pelunasan yang akan dihapus.';
$lang['pelunasan_del_failure']		= 'Tidak dapat menghapus pelunasan pembelian: ';
$lang['pelunasan_delete_confirm']	= 'Apakah anda yakin akan menghapus pelunasan pembelian terpilih ?';
$lang['pelunasan_cancel_confirm']	= 'Apakah anda yakin akan membatalkan input pelunasan ?';
$lang['pelunasan_deleted']			= 'Data Pelunasan berhasil dihapus';
$lang['pelunasan_no_records_found'] = 'Data tidak ditemukan.';

$lang['pelunasan_create_failure'] 	= 'Pelunasan baru gagal disimpan';
$lang['pelunasan_create_success'] 	= 'Pelunasan baru berhasil disimpan';
$lang['pelunasan_canceled'] 		= 'Pelunasan telah berhasil dibatalkan';

$lang['pelunasan_edit_success'] = 'Perubahan tanggal cair berhasil disimpan';
$lang['pelunasan_edit_failed']  = 'Perubahan tanggal cair gagal disimpan';
$lang['pelunasan_invalid_id'] 		= 'ID Tidak Valid';
$lang['pelunasan_placeholder_cari'] = 'Pencarian';
$lang['pelunasan_only_ajax'] 		= 'Only ajax request allowed';