<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<?php $total = 0; ?>
<div class="row">
    <!--form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_order_produk','name'=>'frm_order_produk','role','class'=>'form-horizontal','enctype'=>'multipart/form-data'))?>
    <!-- left  side -->
    <div class="col-md-12">
        <!-- header -->
        <!-- setting upload config -->
        <!-- form id value 1= edit data -->
        <input type="hidden" name="formId" id="formId" value="1">
        <input type="hidden" name="settings" id="settings" value='<?= $setting ?>'>
        <input type="hidden" name="target_server" id="target_server" value="<?= $setting_upload->st_target_server?>">
        <input type="hidden" name="alamat_server" id="alamat_server" value="<?=  $setting_upload->url?>">
        <input type="hidden" name="default_jam" id="default_jam" value="<?= $default_jam ?>">
        <!-- end setting upload config -->

        <!--start image operation config -->
        <input type="hidden" name="use-image-cnf" id="use-image-cnf" value="<?= $use_image ?>">
        <input type="hidden" name="upload-file-cnf" id="upload-file-cnf" value="<?= $upload_file ?>">
        <input type="hidden" name="auto-image-measuring-cnf" id="auto-image-measuring-cnf" value="<?= $auto_image_measuring ?>">
        <!-- end image operation config -->

        <!-- start Header -->
        <div class="col-md-2 bg-warning header-small">
            <label for="st_po" class="control-label">Type Order</label>
            <div>
                <hr>
                <div class="radio">
                    <label><input type="radio" name="st_po" id="st_po" value="0"  <?php echo set_value('st_po', $data->st_po) == 0 ? "checked" : ""; 
                        ?> disabled> &nbsp; Reguler </label>
                </div>
                <div class="radio">
                    <label><input type="radio" name="st_po" value="1" <?php echo set_value('st_po', $data->st_po) == 1 ? "checked" : ""; 
                        ?> disabled> &nbsp; PO/SO</label>
                </div>

            </div>
        </div>
        <div class="col-md-10 header-utama">
            <div class="bg-success header-content">
                <div class="col-md-10 col-sm-10">
                    <!-- first row -->
                    <div class="row" style="padding-left: 10px">
                        <div class="col-md-4 col-sm-4">
                            <div style="padding-right: 5px;margin-bottom: 0px;" class="form-group<?= form_error('no_transaksi') ? ' has-error' : ''; ?>">
                                    <label for="no_transaksi" class="control-label"><?= lang('order_produk_no_transaksi') ?></label>
                                    <input type="hidden" name="id_order" id="id_order" value="<?= set_value('id_order', isset($data->id_order)?$data->id_order:'')?>">
                                    <input class="form-control input-sm" type="text" name="no_transaksi" id="no_transaksi" value="<?= set_value('no_transaksi', isset($data->no_transaksi)? $data->no_transaksi:'')?>" readonly>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4" style="margin-bottom: 5px">
                            <div style="padding-right: 5px;margin-bottom: 0px;" class="form-group <?= form_error('tgl_order') ? ' has-error' : ''; ?>">
                                <label for="tgl_order" class="control-label"><?= lang('order_produk_tgl_order') ?></label>
                                <input class="form-control input-sm" type="text" name="tgl_order" id="tgl_order" value="<?= set_value('tgl_order', isset($data->tgl_order)?$data->tgl_order:'')?>" readonly>
                            </div>
                        </div> 
                        <div class="col-md-4 col-sm-4">
                            <div style="padding-right: 5px;margin-bottom: 0px;" class="form-group <?= form_error('id_konsumen') ? ' has-error' : ''; ?>">
                                <label for="id_konsumen" class="control-label"><?= lang('order_produk_id_konsumen') ?></label>
                                <div class="input-group input-group-sm">
                                    <select id="id_konsumen" name="id_konsumen" class="form-control input-sm" required disabled>
                                        <option></option>
                                    </select>
                                    <span class="input-group-btn">
                                        <a target="_blank" href="<?= base_url("konsumen/create")?>">
                                            <button type="button" class="btn btn-sm btn-info btn-flat"><i class="fa fa-plus"></i></button>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div> 
                        
                    </div>
                    <!-- second row -->
                    <div class="row" style="padding-left: 10px">
                        <div class="col-md-4 col-sm-4">
                            <div style="padding-right: 5px;margin-bottom: 0px;" class="form-group <?= form_error('no_tlp') ? ' has-error' : ''; ?>">
                                <label for="no_tlp" class="control-label"><?= lang('order_produk_no_tlp') ?></label>
                                <input class="form-control input-sm" type="text" name="no_tlp" id="no_tlp" value="<?= set_value('no_tlp', isset($data->no_tlp)?$data->no_tlp:'')?>" required>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4" style="margin-bottom: 5px" id="d-lampiran">
                            <div style="padding-right: 5px;margin-bottom: 0px;" class="form-group <?= form_error('no_po') ? ' has-error' : ''; ?>">
                                <label for="loadLampiran" class="control-label col-md-12" style="text-align: left;padding-left: 0px"><?= lang('order_produk_no_po') ?></label>
                                <input class="form-control input-sm" type="text" name="no_po" id="no_po" value="<?= set_value('no_po', isset($data->no_po)?$data->no_po:'')?>">
                                <input type="file" style="display:none;" name="lampiran" id="lampiran" value="<?= set_value('lampiran', isset($data->lampiran)?$data->lampiran:'')?>" accept=".jpg, .jpeg">
                            </div>
                        </div> 
                    </div>
                </div>
                <!-- lampiran div -->
                <div class="col-md-2 col-sm-2 add-lampiran">
                    <p>Lampiran</p>
                    <img class="preview-lampiran" id="preview-lampiran" 
                        src ="<?php 
                        if($data->lampiran){
                            $explode = explode(".", $data->lampiran);
                            if(end($explode) !== 'pdf'){
                                echo base_url('upload/lampiran/'. $data->lampiran);
                            }else{
                                echo base_url('assets/images/default-pdf.png');
                            }
                        }else{
                            echo base_url('assets/images/empty-image.webp');
                        }?>" 
                        alt="Add Lampiran" />
                </div>

            </div>
        </div>
        <!-- end header -->
        <div class="col-md-12 body-order">
            <div class="header-body">
                <h4> <i class="fa fa-list"></i> Detail order</h4>
            </div>
            <hr>
            <!-- begin left side -->
            <div class="col-md-2">
                <div class="form-group <?= form_error('id_kategori') ? ' has-error' : ''; ?>">
                    <label for="id_kategori" class="control-label"><?= lang('order_produk_id_kategori') ?></label>
                    <select id="id_kategori" name="id_kategori" class="form-control">
                        <option></option>
                        <?php foreach ($kategori as $key => $record) : ?>
                        <option value="<?= $record->idkategori; ?>" <?= set_select('id_kategori', $record->idkategori, isset($data->id_kategori) && $data->id_kategori == $record->idkategori) ?>><?= $record->nmkategori ?></option> 
                        <?php endforeach; ?>
                    </select>
                    <input type="hidden" name="kategori_name" id="kategori_name">
                </div>
                <div class="form-group <?= form_error('id_produk') ? ' has-error' : ''; ?>">
                    <label for="id_produk" class="control-label"><?= lang('order_produk_id_produk') ?></label>
                    <select id="id_produk" name="id_produk" class="form-control">
                        <option></option>
                    </select>
                    <!-- jika st_ukuran = 1 berati panjang dan lebar gambar upload harus sesuai dengan panjang dan lebar di fild kategori -->
                    <!-- jika 0 bebas; -->
                    <!-- tapi jika set_dimensi(cekbok diform) di unceheck ukuran diabaikan -->
                    <input type="hidden" name="st_ukuran" id="st_ukuran">
                    <input type="hidden" name="p_kategori" id="p_kategori">
                    <input type="hidden" name="l_kategori" id="l_kategori">
                    <!-- cek minimal order di tabel produk.
                    -jika st_minimal_order = 1
                        -baca st_tipe
                            -jika 0(fix) maka jumlah cetak tidak boleh kurang dari st_minimal_order
                            -jika 1(custom) maka panjang x l order tidak boleh kurang dari panjang_minimal x lebar_minimal -->
                    <input type="hidden" name="st_minimal_order" id="st_minimal_order">
                    <input type="hidden" name="st_tipe" id="st_tipe">
                    <input type="hidden" name="jml_minimal_order" id="jml_minimal_order">
                    <input type="hidden" name="panjang_minimal" id="panjang_minimal">
                    <input type="hidden" name="lebar_minimal" id="lebar_minal">

                    <input type="hidden" name="XResolution" id="XResolution">
                    <input type="hidden" name="YResolution" id="YResolution">
                    <input type="hidden" name="imageWidth" id="imageWidth">
                    <input type="hidden" name="imageHeight" id="imageHeight">
                    <input type="hidden" name="kenaikan_persen" id="kenaikan_persen">
                    <input type="hidden" name="kenaikan_value" id="kenaikan_value">
                    <input type="hidden" name="biaya_cetak" id="biaya_cetak">
                    <input type="hidden" name="detail_gambar" id="detail_gambar">
                    <input type="hidden" name="harga_cetak_persatuan_terkecil" id="harga_cetak_persatuan_terkecil">
                    <input type="hidden" name="qty_harga_by" id="qty_harga_by">
                    <input type="hidden" name="harga_by" id="harga_by">
                    <input type="hidden" name="satuan_cetak_by" id="satuan_cetak_by">
                    <input type="hidden" name="jml_cetak_bom" id="jml_cetak_bom">

                </div>
                <div style="padding-right: 5px;margin-bottom: 0px;" class="form-group <?= form_error('st_urgent') ? ' has-error' : ''; ?>">
                    <label for="st_urgent" class="control-label"><?= lang('order_produk_st_urgent') ?></label>
                    <div class="checkbox">
                      <label class="text-red">
                        <input type="checkbox" name="st_urgent" id="st_urgent"> <?= lang('order_produk_st_urgent_urgent') ?>
                      </label>
                    </div> 
                </div>
                <div style="padding-right: 5px;margin-bottom: 0px;" class="form-group d-tgl-selesai <?= form_error('tgl_selesai') ? ' has-error' : ''; ?>">
                    <label for="tgl_selesai" class="control-label"><?= lang('order_produk_tgl_selesai') ?></label>
                    <input class="form-control input-sm" type="text" name="tgl_selesai" id="tgl_selesai" value="<?= set_value('tgl_selesai', isset($data->tgl_selesai)?$data->tgl_selesai:'')?>">
                    <label for="jam_selesai" class="control-label"><?= lang('order_produk_jam_selesai') ?></label>
                    <input class="form-control input-sm" type="text" name="jam_selesai" id="jam_selesai" value="<?= set_value('jam_selesai', isset($data->jam_selesai)?$data->jam_selesai:'')?>">
                </div>
            </div>
            <!-- end left side -->
            <!-- begin body input-->
            <div class="col-md-5">
                <div class="form-group <?= form_error('gambar') ? ' has-error' : ''; ?>">
                    <label for="gambar" class="col-sm-5 control-label" style="padding-top:0"><?= lang('order_produk_gambar_cetak') ?><br> <sup class="text-red">Max file size 50MB</sup></label>
                    <div class="col-sm-7" style="padding: 0px;">
                        <div id="preview-gambar" style="display: none;padding-bottom: 5px;">
                            <div class="col-sm-8" style="padding: 0px">
                                <img width="65px" height="65px" src="" id="img-preview" name="img-preview" style="display: none;">
                                <canvas id="preview" height="65px" width="65px"></canvas>
                                <label id="info-gambar" class="text-red" style="font-size: 10px" ></label>
                            </div>
                        </div>
                        <div class="col-sm-12" style="padding-left:0px !important; padding-top: 3px;">
                            <input type="button" id="loadFileXml" value="Browse" onclick="document.getElementById('gambar').click();"  class="btn-info" />
                            <input type="file" style="display:none;" name="gambar" id="gambar" accept=".pdf,.jpg,.jpeg,.zip" value="<?= set_value('gambar', isset($data->gambar)?$data->gambar:'')?>">
                        </div>
                    </div>
                </div>
                <!-- tabel untuk menyinpan cetak PDF -->
                <table name="tabel-detail-pdf" id="tabel-detail-pdf" style="display: none;">
                    <thead>
                        <tr>
                            <th>Page</th>
                            <th>JML Cetak</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
                <div class="form-group <?= form_error('nama_pekerjaan') ? ' has-error' : ''; ?>">
                    <label for="nama_pekerjaan" class="col-sm-5 control-label"><?= lang('order_produk_nama_pekerjaan') ?></label>
                    <div class="col-sm-7" style="padding: 0px;">
                        <input class="form-control input-sm" type="text" name="nama_pekerjaan" id="nama_pekerjaan" value="<?= set_value('nama_pekerjaan', isset($data->nama_pekerjaan)?$data->nama_pekerjaan:'')?>">
                    </div>
                </div>
                <!-- cek dimensi sesuai gambar -->
                <div class="form-group ukuran-cetak custom-check">
                    <div class="col-sm-offset-5 col-sm-7" style="padding: 0px;">
                        <div class="checkbox" name="set_dimensi">
                          <label class="text-red">
                            <input type="checkbox" name="st_dimensi" id="st_dimensi"> <?= lang('order_produk_dimensi_susai_gambar') ?>
                          </label>
                        </div>
                    </div>
                </div>
                <div class="form-group ukuran-cetak  <?= form_error('ukuran_cetak') ? ' has-error' : ''; ?>">
                    <label for="ukuran_cetak" class="col-sm-5 control-label"><?= lang('order_produk_ukuran_cetak') ?></label>
                    <div class="col-sm-7" style="padding: 0px;">
                        <div class="col-sm-12" style="padding: 0px;">
                            <div class="input-group">
                                <span class="input-group-addon">P</span>
                                <input type="hidden" name="hide_p" id="hide_p">
                                <input class="form-control input-sm" type="text" name="p" id="p" value="<?= set_value('p', isset($data->p)?$data->p:'')?>" readonly>
                                <span class="input-group-addon">L</span>
                                <input type="hidden" name="hide_l" id="hide_l">
                                <input class="form-control input-sm" type="text" name="l" id="l" value="<?= set_value('l', isset($data->l)?$data->l:'')?>" readonly>
                                <span class="input-group-addon" style="padding: 0px !important">
                                    <button type="button" class="btn btn-flat btn-danger" name="btn-switch" id="btn-switch" style="padding-top: 3px;padding-bottom: 3px;margin-top: 0px !important;margin-bottom: 0px;"><i class="fa fa-refresh"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-offset-5 col-sm-7" style="padding: 0px ;margin-top: 5px;">
                        <select id="id_konversi" name="id_konversi" class="form-control" onchange="konversi_panjang(this)">
                                <option></option>
                                <?php foreach ($konversi as $key => $record) : ?>
                                <option value="<?= $record->id_konversi; ?>" data-jml_kecil ="<?= $record->jml_kecil;?>" data-jml_besar ="<?= $record->jml_besar;?>" <?= set_select('konversi', $record->id_konversi, isset($data->konversi) && $data->konversi == $record->id_konversi) ?>><?= $record->satuan_besar ?></option> 
                                <?php endforeach; ?>
                            </select>
                    </div>
                    <div class="col-sm-offset-5 col-sm-7" style="padding: 0px ;margin-top: 5px;">
                        <sup class="text-red"><b> Note:</b> Ukuran untuk menghitung harga bukan ukuran file asli. Silahkan ubah ukuran jika bahan yang digunakan perlu pernyesuaian ukuran.</sup>
                    </div>
                </div>
                <div class="form-group <?= form_error('jml_order') ? ' has-error' : ''; ?>">
                    <label for="jml_order" class="col-sm-5 control-label"><?= lang('order_produk_jml_order') ?></label>
                    <div class="col-sm-3" style="padding: 0px;">
                        <input class="form-control input-sm" type="text" name="jml_order" id="jml_order" value="<?= set_value('jml_order', isset($data->jml_order)?$data->jml_order:'')?>">
                        <input type="hidden" name="id_konversi_jml_order" id="id_konversi_jml_order" value="8">
                    </div>
                    <div class="col-sm-1" id="view_detail_pdf" style="display: none;"><label class="control-label"><a href="#" onclick="showDetailPdf()"><i class="fa fa-eye"></i></a></label> </div>
                </div>
                <div class="form-group <?= form_error('biaya_cetak') ? ' has-error' : ''; ?>">
                    <label for="biaya_cetak" class="col-sm-5 control-label"><?= lang('order_produk_biaya_cetak') ?></label>
                    <div class="col-sm-3" style="padding: 0px;">
                        <input class="form-control input-sm" type="text" name="biaya_cetak1" id="biaya_cetak1" value="" readonly="">
                    </div>
                </div>
                <div class="form-group <?= form_error('biaya_desain') ? ' has-error' : ''; ?>">
                    <label for="biaya_desain" class="col-sm-5 control-label"><?= lang('order_produk_biaya_desain') ?></label>
                    <div class="col-sm-3" style="padding: 0px;">
                        <input class="form-control input-sm" type="text" name="biaya_desain" id="biaya_desain" value="<?= set_value('biaya_desain', isset($data->biaya_desain)?$data->biaya_desain:'')?>">
                    </div>
                </div>  
            </div>
            <div class="col-md-5">
                 <div class="form-group <?= form_error('note_produk') ? ' has-error' : ''; ?>">
                    <label for="note_produk" class="col-sm-5 control-label"><?= lang('order_produk_note_produk') ?></label>
                    <div class="col-sm-7" style="padding: 0px">
                        <textarea class="form-control input-sm" id="note_produk" name="note_produk"><?php echo set_value('note_produk', isset($data->note_produk) ? $data->note_produk : ''); ?></textarea>
                    </div>
                </div> 
                <div class="form-group <?= form_error('st_finishing') ? ' has-error' : ''; ?>">
                    <label for="st_finishing" class="col-sm-5 control-label"><?= lang('order_produk_st_finishing') ?></label>
                    <div class="col-sm-7" style="padding: 0px">
                        <select id="st_finishing" name="st_finishing" class="form-control">
                            <option value="0" <?= set_select('st_finishing', 0, isset($data->st_finishing) && $data->st_finishing == 0) ?>><?= lang('order_produk_finishing_standart') ?></option>
                            <option value="1" <?= set_select('st_finishing', 1, isset($data->st_finishing) && $data->st_finishing == 1) ?>><?= lang('order_produk_finishing_custom') ?></option> 
                        </select>
                    </div>
                </div>
                <div id="d-hide">
                    <div class="form-group">
                        <div class="col-sm-offset-5 col-sm-7" style="padding: 0px">
                            <div class="checkbox" name="st_finishing_standart">
                              <label class="text-red">
                                <!-- 0: digunakan 1: finishing stantard tidak digunakan -->
                                <input type="checkbox" name="st_finishing_standart" id="st_finishing_standart" checked="checked"> <?= lang('order_produk_finishing_standart_digunakan') ?>
                              </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group <?= form_error('id_finishing') ? ' has-error' : ''; ?>">
                        <label for="id_finishing" class="col-sm-5 control-label"><?= lang('order_produk_id_finishing') ?></label>
                        <div class="col-sm-7" style="padding: 0px">
                            <select id="id_finishing" name="id_finishing" class="form-control">
                                <option></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group <?= form_error('jml')?'has-error':'';?>">
                        <label for="jml" class="control-label col-sm-5"><?= lang('order_produk_jml_pakai_finishing')?></label>
                        <div class="col-sm-7" style="padding: 0px">
                            <div class="input-group">
                            <input type="text" name="jml_finishing" id="jml_finishing" class="form-control input-sm" value="<?= set_value('jml',isset($data->jml) ? $data->jml:'');?>">
                            <span class="input-group-addon success">SET</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group<?= form_error('note_finishing') ? ' has-error' : ''; ?>">
                        <label for="note_finishing" class="col-sm-5 control-label"><?= lang('order_produk_note_finishing') ?></label>
                        <div class="col-sm-7" style="padding: 0px">
                            <textarea class="form-control input-sm" id="note_finishing" name="note_finishing"><?php echo set_value('note_finishing', isset($data->note_finishing) ? $data->note_finishing : ''); ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="harga_finishing" class="control-label col-sm-5"><?= lang('order_produk_harga_finishing')?></label>
                        <div class="col-sm-7" style="padding: 0px">
                            <input type="text" name="harga_finishing" id="harga_finishing" class="form-control input-sm" value="<?= set_value('harga', isset($data->harga)?$data->harga:'');?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-5 col-sm-7" style="padding: 0px;">
                            <button type="button" name="add_finishing" id="add_finishing" class="btn btn-info btn-sm pull-right"><?= lang('order_produk_btn_pakai_finishing') ?></button>
                        </div>
                    </div> 
                </div>
            </div>
        
            <div id="t-hide">
                <div class="col-lg-offset-4 col-md-8" style="padding: 0px;">
                    <!-- Detail Finishing -->
                    <div class="table-responsive">
                        <table class="table table-bordered" id="tdet_finishing" name="tdet_finishing">
                            <thead>
                                <tr class="success">
                                    <th style="vertical-align:middle;"><?= lang('order_produk_no') ?></th>
                                    <th style="vertical-align:middle;"><?= lang('order_produk_id_finishing') ?></th>
                                    <th style="vertical-align: middle;"><?= lang('order_produk_note_finishing')?></th>
                                    <th width="150px" style="vertical-align:middle;" class="text-center"><?= lang('order_produk_jml_pakai_finishing') ?></th>
                                    <th width="150px" style="vertical-align:middle;"><?= lang('order_produk_harga_finishing') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4">
                                        Total
                                    </td>
                                    <td class="gtotal_finishing" align="right">
                                        
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <button type="button" id="add_produk_edit" name="add_produk_edit" class="btn btn-info pull-right btn-sm"> <i class="fa  fa-plus-square"></i> <?= lang('order_produk_btn_tambah_order') ?></button>
                </div>
            </div>
            <hr>
            <!-- <p style="margin-bottom: 3px;">
                <span class="label label-warning" style="font-size: 12px"><lang('order_produk_list_order') ?></span>
            </p> -->
            <div class="table-responsive">
                <table class="table table-bordered" id="tdet_order" name="tdet_order">
                    <thead>
                        <tr class="success">
                            <th width="20" style="vertical-align:middle;"><?= lang('order_produk_no') ?></th>
                            <th style="vertical-align:middle;" colspan="2"><?= lang('order_produk_nama_pekerjaan') ?></th>
                            <th><?= lang('order_produk_detail_gambar')?></th>
                            <th><?=lang('order_produk_st_urgent')?></th>
                            <th width="50px" style="vertical-align: middle;"><?= lang('order_produk_jml_order')?></th>
                            <th><?= lang('order_produk_harga_persatuan') ?></th>
                            <th width="60px" style="vertical-align:middle;" class="text-center"><?= lang('order_produk_biaya_cetak') ?></th>
                            <th width="60px" style="vertical-align:middle;" class="text-center"><?= lang('order_produk_biaya_desain') ?></th>
                            <th width="60px" style="vertical-align:middle;" class="text-center"><?= lang('order_produk_biaya_finishing') ?></th>
                            <th width="70px" style="vertical-align:middle;"><?= lang('order_produk_subtotal') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if($detail) : ?>
                                <?php 
                                    $no = 1;
                                    foreach($detail as $key => $dt) :
                                ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><input type="hidden" name="id_detail_produk_order[]" value="<?=$dt->id_detail_produk_order ?>"><img src="<?= base_url($dt->thumbnail)?>" width="65" height="65"></td>
                                <td>
                                    <p style="font-weight: bold; margin-bottom: 0px;">
                                        <?= strtoupper($dt->nama_pekerjaan) ;
                                            if ($dt->st_acc_produksi == 0):
                                        ?>
                                        <a href="#" onclick="remove_order(this)"><i class="fa fa-times-circle text-red"></i></a>
                                        <?php endif;?>

                                        <a id="edit_tgl" onclick="view_order(this, event)" data-id="<?= $dt->id_detail_produk_order?>" href="#" data-toggle="lightbox-view"> <i class="fa fa-folder-open"></i></a>

                                            
                                    </p>
                                    <p style="margin-bottom: 0px;">
                                        <?= strtoupper($dt->nm_produk) ?>    
                                    </p>
                                    <p style="margin-bottom: 0px;">
                                        <?= $dt->st_finishing==0?'Finishing Standar':'Finishing Custom' ?>
                                    </p>
                                </td>
                                <td><?= $dt->detail_gambar."<br/><i> P: ".$dt->p. $dt->nm_satuan. " x L: ".$dt->l. $dt->nm_satuan."<i/><br/>". ($dt->st_dimensi == 1 ? "<span class='label label-danger'>Custom Size</span>": "<span class='label label-success'>Original Size</span>")?>
                                </td>
                                <td><?=($dt->st_urgent==1 ? "<span class='label label-danger'>Urgent</span>" : "<span class='label label-success'>Non Urgent</span>") ?></td>
                                <td><?= number_format($dt->jml_cetak) ?></td>
                                <td><?= $dt->harga_by." ".$dt->satuan_besar ."@".number_format($dt->harga_cetak_persatuan_terkecil) ?></td>
                                <td align="right"><?= number_format($dt->biaya_cetak + $dt->kenaikan_value )?></td>
                                <td align="right"><?= number_format($dt->biaya_design)?></td>
                                <td align="right"><?= number_format($dt->biaya_finishing)?></td>
                                <td align="right"><?= number_format($dt->sub_total)?></td>
                            </tr>
                                <?php $total+= $dt->sub_total; endforeach; ?>
                            <?php endif; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="10" align="right">
                                TOTAL:
                            </td>
                            <td class="gtotal_order" align="right">
                                <strong><?= number_format($total) ?></strong>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="form-group">
                <div class="col-md-12" style="padding-bottom: 10px">
                    <?php
                    echo anchor('order_produk/cancel', '<i class="fa  fa-home"> </i>'. lang('order_produk_btn_cancel') , array("onclick" => "return confirm('".lang('order_produk_cancel_confirm')."')"));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?= form_close() ?>
</div>

<!-- modal view -->
<div id="lightbox-view" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="border-radius: 5px !important;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Preview Detail Order</h4>
            </div>
            <div class="modal-body form-horizontal">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group <?= form_error('nama_pekerjaan') ? ' has-error' : ''; ?>">
                            <label for="nama_pekerjaan" class="col-sm-5 control-label"><?= lang('order_produk_nama_pekerjaan') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_nama_pekerjaan"></span>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('id_kategori') ? ' has-error' : ''; ?>">
                            <label for="id_kategori" class="col-sm-5 control-label"><?= lang('order_produk_id_kategori') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_id_kategori_produk"></span>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('id_produk') ? ' has-error' : ''; ?>">
                            <label for="id_produk" class="col-sm-5 control-label"><?= lang('order_produk_id_produk') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_id_produk"></span>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('gambar') ? ' has-error' : ''; ?>">
                            <label for="gambar" class="col-sm-5 control-label"><?= lang('order_produk_gambar_cetak') ?></label>
                            <div class="col-sm-7">
                                <img width="100" height="100" src="" id="view_img" name="view_img">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('ukuran_cetak') ? ' has-error' : ''; ?>">
                            <label for="ukuran_cetak" class="col-sm-5 control-label"><?= lang('order_produk_ukuran_cetak') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_p"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group <?= form_error('jml_order') ? ' has-error' : ''; ?>">
                            <label for="jml_order" class="col-sm-5 control-label"><?= lang('order_produk_jml_order') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_jml_order"></span>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('biaya_cetak') ? ' has-error' : ''; ?>">
                            <label for="biaya_cetak" class="col-sm-5 control-label"><?= lang('order_produk_biaya_cetak') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_biaya_cetak"></span>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('biaya_desain') ? ' has-error' : ''; ?>">
                            <label for="biaya_desain" class="col-sm-5 control-label"><?= lang('order_produk_biaya_desain') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_biaya_desain"></span>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('note_produk') ? ' has-error' : ''; ?>">
                            <label for="note_produk" class="col-sm-5 control-label"><?= lang('order_produk_note_produk') ?></label>
                            <div class="col-sm-7">
                                <textarea id="view_note_produk" disabled class="form-control">
                                    
                                </textarea>
                                <!-- <span class="form-control" id="view_note_produk"></span> -->
                                <!-- <textarea class="form-control" id="view_note_produk" name="view_note_produk" disabled></textarea> -->
                            </div>
                        </div>
                        <div class="form-group <?= form_error('st_finishing') ? ' has-error' : ''; ?>">
                            <label for="st_finishing" class="col-sm-5 control-label"><?= lang('order_produk_st_finishing') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_st_finishing"></span>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-12" id="detail-hide">
                    <!-- view detail finishing -->
                    <p style="margin-top: 15px; margin-bottom: 3px;">
                        <span class="label label-warning" style="font-size: 12px"><?= lang('order_produk_list_finishing') ?></span>
                    </p>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="view_tdet_finishing" name="view_tdet_finishing">
                            <thead>
                                <tr class="success">
                                    <th style="vertical-align:middle;"><?= lang('order_produk_no') ?></th>
                                    <th style="vertical-align:middle;"><?= lang('order_produk_id_finishing') ?></th>
                                    <th style="vertical-align: middle;"><?= lang('order_produk_note_finishing')?></th>
                                    <th width="150px" style="vertical-align:middle;" class="text-center"><?= lang('order_produk_jml_pakai_finishing') ?></th>
                                    <th width="150px" style="vertical-align:middle;"><?= lang('order_produk_harga_finishing') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                
                            </tbody>
                            <tfoot>
                                <tr><!-- 
                                    <td colspan="4">
                                        Total
                                    </td>
                                    <td class="view_gtotal_finishing" align="right">
                                        
                                    </td>
                                </tr -->>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                 
            </div>  
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- modal pdf file -->
<div id="pdf-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="border-radius: 5px !important;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Setting Jumlah Print PDF File</h4>
            </div>
            <div class="modal-body form-horizontal" style="padding-right: 0px;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label class="control-label col-sm-6" for="page"><?= lang('order_produk_label_page')?></label>
                            <div class="col-sm-6">
                                <input type="text" name="page" id="page" class="form-control">
                            </div>
                        </div>    
                        </div>
                        <div class="col-md-5" style="padding-left: 0px;">
                            <div class="form-group">
                                <label for="jml_cetak_page" class="control-label col-sm-6"><?= lang('order_produk_label_jumlah') ?></label>
                                <div class="col-sm-6">
                                    <input type="number" name="jml_cetak_page" id="jml_cetak_page" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1" style="padding-left: 0px;">
                            <button class="btn btn-success btn-flat" name="btn-add-page" id="btn-add-page" onclick="addPage()"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table-pdf">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th class="text-right">Halaman</th>
                                        <th class="text-right">Jml Cetak</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>    
            </div>
            <div class="modal-footer">
                <label class="control-label pull-left" id="halaman-pdf"></label>
                <button class="btn btn-success btn-flat" onclick="addPagePdfList()"><i class="fa fa-save"> Save</i></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<script type="text/javascript" src="<?= base_url('assets/plugins/exif/exif.js')?>"></script>
<script type="text/javascript" src="<?= base_url('assets/plugins/pdfjs/pdf.js')?>"></script>
<script type="text/javascript" src="<?= base_url('assets/plugins/pdfjs/pdf.worker.js')?>"></script>
<script type="text/javascript">
    if (typeof(Storage) !== "undefined") {
        sessionStorage.setItem("id_konsumen", "<?= (isset($data->id_konsumen)) ? $data->id_konsumen : '' ?>");
    }
    $("body").addClass('sidebar-collapse');
</script>