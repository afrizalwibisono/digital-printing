<?php 
  $ENABLE_ADD    = has_permission('Order Produk.Add'); 
  $ENABLE_MANAGE  = has_permission('Order Produk.Manage'); 
  $ENABLE_DELETE  = has_permission('Order Produk.Delete'); 
  $ENABLE_DELETE_RESELLER = has_permission('Order Reseller.Delete');
?> 
<div class="box box-primary"> 
  <?= form_open($this->uri->uri_string(),array('id'=>'frm_order_produk','name'=>'frm_order_produk','class'=>'form-inline')) ?> 
  <div class="box-header"> 
    <?php if ($ENABLE_ADD) : ?> 
      <div class="pull-left"> 
          <a href="<?= site_url('order_produk/create') ?>" class="btn btn-success" title="<?= lang('order_produk_btn_new') ?>"><?= lang('order_produk_btn_new') ?></a> 
      </div> 
      <?php endif; ?> 
      <div class="pull-right"> 
          <div class="form-group"> 
              <div class="input-daterange input-group"> 
                  <input type="text" class="form-control" name="tgl_awal" id="tgl_awal" value="<?= isset($tgl_awal) && $tgl_awal !='' ? $tgl_awal : '' ?>" placeholder="<?= lang('order_produk_tgl_awal') ?>" required /> 
                  <span class="input-group-addon text-black">to</span> 
                  <input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?= isset($tgl_akhir) && $tgl_akhir !='' ? $tgl_akhir : '' ?>" placeholder="<?= lang('order_produk_tgl_akhir') ?>" required /> 
              </div> 
          </div> 
          <div class="form-group"> 
              <div class="input-group"> 
                  <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="Search" autofocus /> 
                  <div class="input-group-btn"> 
                      <button class="btn btn-default"><i class="fa fa-search"></i></button> 
                  </div> 
              </div> 
          </div> 
      </div> 
  </div> 
  <?php if (isset($results) && is_array($results) && count($results)>0) : ?> 
    <div class="box-body table-responsive no-padding"> 
              <table class="table table-hover table-striped"> 
                  <thead> 
                      <tr> 
                          <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th> 
                          <th width="50">#</th> 
                          <th><?= lang('order_produk_no_transaksi') ?></th> 
                          <th><?= lang('order_produk_id_konsumen') ?></th> 
                          <th class="text-center"><?= lang('order_produk_st_konsumen') ?></th>
                          <th  class="text-center"><?= lang('order_produk_st_order') ?></th>
                          <th><?= lang('order_produk_tgl_order') ?></th> 
                          <th><?= lang('order_produk_total_order') ?></th> 
                          <?php if($ENABLE_MANAGE) : ?> 
                          <th width="80">ACT</th> 
                          <?php endif; ?> 
                      </tr> 
                  </thead> 
                  <tbody> 
                      <?php foreach ($results as $record) : ?> 
                        <?php 
                            switch ($record->st) {
                                case 0:
                                    $st = "<span class='label label-success'>Konsumen</span>";
                                    break;
                                case 1:
                                    $st = "<span class='label label-info'>Reseller</span>";
                                    break;
                                
                                default:
                                    $st = "<span class='label label-danger'>Instansi</span>";
                                    break;
                            }
                        ?>
                        <!-- <?php echo"<pre>",print_r($record),"</pre>"?> --> 
                      <tr> 
                          <td class="column-check">
                            <?php 
                              if ($record->st_kasir== 0):
                                if ($record->st_order == 1):
                                  if ($ENABLE_DELETE_RESELLER):
                                      if ($record->st_nota==0):
                                  ?>

                                    <input type="checkbox" name="checked[]" value="<?= $record->id_order ?>" />
                              <?php
                                      endif;  
                                  endif;
                                else:
                                  if ($record->st_nota==0):
                                  ?>
                                <input type="checkbox" name="checked[]" value="<?= $record->id_order ?>" />
                                <?php
                                  endif;
                                endif;
                            endif;
                            ?>
                          </td> 
                          <td><?= $numb; ?></td> 
                          <td><?= $record->no_transaksi ?></td>
                          <td><?= $record->nama ?></td> 
                          <td class="text-center"><?= $st ?></td>
                          <td class="text-center"><?= $record->st_po == 0 ? "<span class='label label-success'>Order Reguler</span>": "<span class='label label-warning'>Order PO/SO</span>" ?></td>
                          <td><?= date("d-m-Y", strtotime($record->tgl_order))  ?></td> 
                          <td><?= number_format($record->total_value_order) ?></td>
                          <?php if($ENABLE_MANAGE) : ?> 
                              <td style="padding-right:20px">
                                <?php 
                                  if ($record->st_close_order==0):?>
                                    <a class="text-green" href="<?= site_url('order_produk/edit/' . $record->id_order); ?>" data-toggle="tooltip" data-placement="left" title="Edit Order"><i class="fa fa-pencil"></i></a> &nbsp|&nbsp
                              <?php endif;?>
                                    <a class="text-black" href="<?= site_url('order_produk/view/' . $record->id_order); ?>" data-toggle="tooltip" data-placement="left" title="View Order"><i class="fa fa-folder-open"></i></a>

                              </td> 
                          <?php endif; ?> 
                      </tr> 
                      <?php $numb++; endforeach; ?> 
                  </tbody> 
      </table> 
    </div><!-- /.box-body --> 
    <div class="box-footer clearfix"> 
      <?php if($ENABLE_DELETE) : ?> 
      <input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('order_produk_btn_delete') ?>" onclick="return confirm('<?= lang('order_produk_delete_confirm'); ?>')"> 
      <?php endif; 
      echo $this->pagination->create_links();  
      ?> 
    </div> 
  <?php else: ?> 
    <div class="alert alert-info" role="alert"> 
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('order_produk_no_records_found') ?></p> 
    </div> 
    <?php 
  endif; 
  echo form_close();  
  ?>   
</div><!-- /.box-->