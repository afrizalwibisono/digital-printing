<?php $total = 0; ?>
<style type="text/css">
    hr {
        margin: 0px;
        border-width: 3px;
        margin-top: 10px; 
        margin-bottom: 10px; 
        border-top: 3px double #8c8b8b;
    }
</style>
<div class="box box-primary">
    <!--form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_order_produk','name'=>'frm_order_produk','role','class'=>'form-horizontal','enctype'=>'multipart/form-data'))?>
    <div class="box-body">
        <div class="col-md-12">
            <div class="col-md-3">
                <div style="padding-right: 10px;" class="form-group <?= form_error('no_transaksi') ? ' has-error' : ''; ?>">
                    <label for="no_transaksi" class="control-label"><?= lang('order_produk_no_transaksi') ?></label>
                    <input type="hidden" name="id_order" id="id_order" value="<?= set_value('id_order', isset($data->id_order)?$data->id_order:'')?>">
                    <input class="form-control" type="text" name="no_transaksi" id="no_transaksi" value="<?= set_value('no_transaksi', isset($data->no_transaksi)? $data->no_transaksi:'')?>" readonly>
                </div>
            </div>
            <div class="col-md-3">
                <div style="padding-right: 10px;" class="form-group <?= form_error('tgl_order') ? ' has-error' : ''; ?>">
                    <label for="tgl_order" class="control-label"><?= lang('order_produk_tgl_order') ?></label>
                    <input class="form-control" type="text" name="tgl_order" id="tgl_order" value="<?=$data->tgl_order?>" readonly>
                </div>
            </div>
            <div class="col-md-3">
                <div style="padding-right: 10px;" class="form-group <?= form_error('id_konsumen') ? ' has-error' : ''; ?>">
                    <label for="id_konsumen" class="control-label"><?= lang('order_produk_id_konsumen') ?></label>
                    <input class="form-control" type="text" name="id_konsumen" id="id_konsumen" value="<?= $data->panggilan." ".$data->nama?>" readonly>
                </div>
            </div>
            <div class="col-md-3">
                <div style="padding-right: 10px;" class="form-group <?= form_error('no_po') ? ' has-error' : ''; ?>">
                    <label for="no_po" class="control-label"><?= lang('order_produk_no_po') ?></label>
                    <input class="form-control" type="text" name="no_po" id="no_po" value="<?= $data->no_po ?>" readonly>
                </div>
            </div>

            <div class="col-md-3">
                <div style="padding-right: 10px;" class="form-group <?= form_error('type_order') ? ' has-error' : ''; ?>">
                    <label for="no_po" class="control-label"><?= lang('order_produk_tipe_order') ?></label>
                    <input class="form-control" type="text" name="no_po" id="no_po" value="<?= $data->st_po == 0 ? 'Reguler' : 'PO/SO' ?>" readonly>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group <?= form_error('lampiran') ? ' has-error' : ''; ?>">
                    <label for="id_konsumen" class="col-md-12">&nbsp</label>
                    <?php if ($data->lampiran !=""):?>
                        <label><a href="<?= base_url('upload/lampiran/'.$data->lampiran) ?>" target="_blank"><i class="fa fa-paperclip fa-lg"></i> Download Lampiran</a></label>
                    <?php endif; ?>
                </div>
            </div>
            </div>
        <div class="col-md-12">
            <hr>
            <!-- Detail Finishing -->
            <p style="margin-top: 15px; margin-bottom: 3px;">
                <span class="label label-warning" style="font-size: 12px"><?= lang('order_produk_list_order') ?></span>
            </p>
            <div class="table-responsive">
                <table class="table table-bordered" id="tdet_order" name="tdet_order">
                    <thead>
                        <tr class="success">
                            <th style="vertical-align:middle;"><?= lang('order_produk_no') ?></th>
                            <th style="vertical-align:middle;" colspan="2"><?= lang('order_produk_nama_pekerjaan') ?></th>
                            <th><?= lang('order_produk_detail_gambar')?></th>
                            <th class="text-center"><?=lang('order_produk_st_urgent')?></th>
                            <th width="50px" style="vertical-align: middle;"><?= lang('order_produk_jml_order')?></th>
                            <th><?= lang('order_produk_harga_persatuan') ?></th>
                            <th width="60px" style="vertical-align:middle;" class="text-center"><?= lang('order_produk_biaya_cetak') ?></th>
                            <th width="60px" style="vertical-align:middle;" class="text-center"><?= lang('order_produk_biaya_desain') ?></th>
                            <th width="60px" style="vertical-align:middle;" class="text-center"><?= lang('order_produk_biaya_finishing') ?></th>
                            <th width="70px" style="vertical-align:middle;"><?= lang('order_produk_subtotal') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if($detail) : ?>
                                <?php 
                                    $no = 1;
                                    foreach($detail as $key => $dt) :
                                ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><input type="hidden" name="id_detail_produk_order[]"" value="<?=$dt->id_detail_produk_order ?>"><img src="<?=  base_url($dt->thumbnail) ;?>" width="65" height="65"></td>
                                <td>
                                    <p style="font-weight: bold; margin-bottom: 0px;">
                                        <?= strtoupper($dt->nama_pekerjaan) ?>
                                        <a id="edit_tgl" onclick="view_order(this, event)" data-id="<?= $dt->id_detail_produk_order?>" href="#" data-toggle="lightbox-view"> <i class="fa fa-folder-open"></i></a>

                                            
                                    </p>
                                    <p style="margin-bottom: 0px;">
                                        <?= strtoupper($dt->nm_produk) ?>    
                                    </p>
                                    <p style="margin-bottom: 0px;">
                                        <?= $dt->st_finishing==0?'Finishing Standar':'Finishing Custom' ?>
                                    </p>
                                </td>
                                <td>
                                    <?= $dt->detail_gambar ."<br/><i> P: ".$dt->p. $dt->nm_satuan. " x L: ".$dt->l. $dt->nm_satuan."<i/><br/>". ($dt->st_dimensi == 1 ? "<span class='label label-danger'>Custom Size</span>": "<span class='label label-success'>Original Size</span>") ?>
                                    
                                </td>
                                <td align="middle"><?=($dt->st_urgent==1 ? "<span class='label label-danger'>Urgent</span>" : "<span class='label label-success'>Non Urgent</span>") ?></td>
                                <td><?= $dt->jml_cetak?></td>
                                <td><?= $dt->harga_by." ".$dt->satuan_besar ."@".number_format($dt->harga_cetak_persatuan_terkecil) ?></td>
                                <td align="right"><?= number_format($dt->biaya_cetak + $dt->kenaikan_value )?></td>
                                <td align="right"><?= number_format($dt->biaya_design)?></td>
                                <td align="right"><?= number_format($dt->biaya_finishing)?></td>
                                <td align="right"><?= number_format($dt->sub_total)?></td>
                            </tr>
                                <?php $total+= $dt->sub_total; endforeach; ?>
                            <?php endif; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="9" align="right">
                                TOTAL:
                            </td>
                            <td class="gtotal_order" align="right">
                                <?= number_format($total) ?>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <?php
                echo anchor('order_produk', lang('order_produk_btn_cancel'));
                ?>
            </div>
        </div>
    </div>
    <?= form_close() ?>
</div>
<!-- modal view -->
<div id="lightbox-view" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="border-radius: 5px !important;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Preview Detail Order</h4>
            </div>
            <div class="modal-body form-horizontal">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group <?= form_error('nama_pekerjaan') ? ' has-error' : ''; ?>">
                            <label for="nama_pekerjaan" class="col-sm-5 control-label"><?= lang('order_produk_nama_pekerjaan') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_nama_pekerjaan"></span>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('id_kategori') ? ' has-error' : ''; ?>">
                            <label for="id_kategori" class="col-sm-5 control-label"><?= lang('order_produk_id_kategori') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_id_kategori_produk"></span>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('id_produk') ? ' has-error' : ''; ?>">
                            <label for="id_produk" class="col-sm-5 control-label"><?= lang('order_produk_id_produk') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_id_produk"></span>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('gambar') ? ' has-error' : ''; ?>">
                            <label for="gambar" class="col-sm-5 control-label"><?= lang('order_produk_gambar_cetak') ?></label>
                            <div class="col-sm-7">
                                <img width="100" height="100" src="" id="view_img" name="view_img">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('ukuran_cetak') ? ' has-error' : ''; ?>">
                            <label for="ukuran_cetak" class="col-sm-5 control-label"><?= lang('order_produk_ukuran_cetak') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_p"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group <?= form_error('jml_order') ? ' has-error' : ''; ?>">
                            <label for="jml_order" class="col-sm-5 control-label"><?= lang('order_produk_jml_order') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_jml_order"></span>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('biaya_cetak') ? ' has-error' : ''; ?>">
                            <label for="biaya_cetak" class="col-sm-5 control-label"><?= lang('order_produk_biaya_cetak') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_biaya_cetak"></span>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('biaya_desain') ? ' has-error' : ''; ?>">
                            <label for="biaya_desain" class="col-sm-5 control-label"><?= lang('order_produk_biaya_desain') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_biaya_desain"></span>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('note_produk') ? ' has-error' : ''; ?>">
                            <label for="note_produk" class="col-sm-5 control-label"><?= lang('order_produk_note_produk') ?></label>
                            <div class="col-sm-7">
                                <textarea id="view_note_produk" disabled class="form-control">
                                    
                                </textarea>
                                <!-- <span class="form-control" id="view_note_produk"></span> -->
                                <!-- <textarea class="form-control" id="view_note_produk" name="view_note_produk" disabled></textarea> -->
                            </div>
                        </div>
                        <div class="form-group <?= form_error('st_finishing') ? ' has-error' : ''; ?>">
                            <label for="st_finishing" class="col-sm-5 control-label"><?= lang('order_produk_st_finishing') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_st_finishing"></span>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-12" id="detail-hide">
                    <!-- view detail finishing -->
                    <p style="margin-top: 15px; margin-bottom: 3px;">
                        <span class="label label-warning" style="font-size: 12px"><?= lang('order_produk_list_finishing') ?></span>
                    </p>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="view_tdet_finishing" name="view_tdet_finishing">
                            <thead>
                                <tr class="success">
                                    <th style="vertical-align:middle;"><?= lang('order_produk_no') ?></th>
                                    <th style="vertical-align:middle;"><?= lang('order_produk_id_finishing') ?></th>
                                    <th style="vertical-align: middle;"><?= lang('order_produk_note_finishing')?></th>
                                    <th width="150px" style="vertical-align:middle;" class="text-center"><?= lang('order_produk_jml_pakai_finishing') ?></th>
                                    <th width="150px" style="vertical-align:middle;"><?= lang('order_produk_harga_finishing') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                
                            </tbody>
                            <tfoot>
                                <tr><!-- 
                                    <td colspan="4">
                                        Total
                                    </td>
                                    <td class="view_gtotal_finishing" align="right">
                                        
                                    </td>
                                </tr -->>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                 
            </div>  
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
