<?php $total = 0; ?>
<style type="text/css">
    hr {
        margin: 0px;
        border-width: 3px;
        margin-top: 10px; 
        margin-bottom: 10px; 
        border-top: 3px double #8c8b8b;
    }
</style>
<div class="box box-primary">
    <!--form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_order_produk','name'=>'frm_order_produk','role','class'=>'form-horizontal','enctype'=>'multipart/form-data'))?>
    <div class="box-body">
        <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group <?= form_error('nama_pekerjaan') ? ' has-error' : ''; ?>">
                        <label for="nama_pekerjaan" class="col-sm-5 control-label"><?= lang('order_produk_nama_pekerjaan') ?></label>
                        <div class="col-sm-7">
                            <span class="form-control" id="view_nama_pekerjaan"><?= $data->nama_pekerjaan ?></span>
                        </div>
                    </div>
                    <div class="form-group <?= form_error('id_kategori') ? ' has-error' : ''; ?>">
                        <label for="id_kategori" class="col-sm-5 control-label"><?= lang('order_produk_id_kategori') ?></label>
                        <div class="col-sm-7">
                            <span class="form-control" id="view_id_kategori_produk"><?= $data->nmkategori ?></span>
                        </div>
                    </div>
                    <div class="form-group <?= form_error('id_produk') ? ' has-error' : ''; ?>">
                        <label for="id_produk" class="col-sm-5 control-label"><?= lang('order_produk_id_produk') ?></label>
                        <div class="col-sm-7">
                            <span class="form-control" id="view_id_produk"><?= $data->nm_produk?></span>
                        </div>
                    </div>
                    <div class="form-group <?= form_error('gambar') ? ' has-error' : ''; ?>">
                        <label for="gambar" class="col-sm-5 control-label"><?= lang('order_produk_gambar_cetak') ?></label>
                        <div class="col-sm-7">
                            <img width="100" height="100" src="<?= base_url($data->thumbnail)?>" id="view_img" name="view_img">
                        </div>
                    </div>
                    <div class="form-group <?= form_error('ukuran_cetak') ? ' has-error' : ''; ?>">
                        <label for="ukuran_cetak" class="col-sm-5 control-label"><?= lang('order_produk_ukuran_cetak') ?></label>
                        <div class="col-sm-7">
                            <span class="form-control" id="view_p">
                                <?=
                                    $data->p."x".$data->l." ".$data->satuan_panjang;
                                ?>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group <?= form_error('jml_order') ? ' has-error' : ''; ?>">
                        <label for="jml_order" class="col-sm-5 control-label"><?= lang('order_produk_jml_order') ?></label>
                        <div class="col-sm-7">
                            <span class="form-control" id="view_jml_order">
                                <?= $data->jml_cetak." ".$data->satuan_order?>
                            </span>
                        </div>
                    </div>
                    <div class="form-group <?= form_error('biaya_cetak') ? ' has-error' : ''; ?>">
                        <label for="biaya_cetak" class="col-sm-5 control-label"><?= lang('order_produk_biaya_cetak') ?></label>
                        <div class="col-sm-7">
                            <span class="form-control" id="view_biaya_cetak"><?= number_format($data->biaya_cetak + $data->kenaikan_value) ?></span>
                        </div>
                    </div>
                    <div class="form-group <?= form_error('biaya_desain') ? ' has-error' : ''; ?>">
                        <label for="biaya_desain" class="col-sm-5 control-label"><?= lang('order_produk_biaya_desain') ?></label>
                        <div class="col-sm-7">
                            <span class="form-control" id="view_biaya_desain"><?= number_format($data->biaya_design) ?></span>
                        </div>
                    </div>
                    <div class="form-group <?= form_error('note_produk') ? ' has-error' : ''; ?>">
                        <label for="note_produk" class="col-sm-5 control-label"><?= lang('order_produk_note_produk') ?></label>
                        <div class="col-sm-7">
                            <textarea id="view_note_produk" disabled class="form-control"><?= $data->catatan?></textarea>
                        </div>
                    </div>
                    <div class="form-group <?= form_error('st_finishing') ? ' has-error' : ''; ?>">
                        <label for="st_finishing" class="col-sm-5 control-label"><?= lang('order_produk_st_finishing') ?></label>
                        <div class="col-sm-7">
                            <span class="form-control" id="view_st_finishing"><?= $data->st_finishing==0 ? 'Finishing Standart':'Finishing Custom';?></span>
                        </div>
                    </div>
                </div>
                
            </div>
            <?php if (isset($detail)):?>
            <div class="col-md-12" id="detail-hide">
                <!-- view detail finishing -->
                <p style="margin-top: 15px; margin-bottom: 3px;">
                    <span class="label label-warning" style="font-size: 12px"><?= lang('order_produk_list_finishing') ?></span>
                </p>
                <div class="table-responsive">
                    <table class="table table-bordered" id="view_tdet_finishing" name="view_tdet_finishing">
                        <thead>
                            <tr class="success">
                                <th style="vertical-align:middle;"><?= lang('order_produk_no') ?></th>
                                <th style="vertical-align:middle;"><?= lang('order_produk_id_finishing') ?></th>
                                <th style="vertical-align: middle;"><?= lang('order_produk_note_finishing')?></th>
                                <th width="150px" style="vertical-align:middle;" class="text-center"><?= lang('order_produk_jml_pakai_finishing') ?></th>
                                <th width="150px" style="vertical-align:middle;"><?= lang('order_produk_harga_finishing') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total=0; $no=0; foreach ($detail as $key => $record):?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $record->nm_produk ?></td>
                                    <td><?= $record->catatan?></td>
                                    <td><?= $record->jml ." Set"?></td>
                                    <td><?= number_format($record->harga)?></td>
                                </tr>
                            <?php $total= $total+$record->harga; endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4">
                                    Total
                                </td>
                                <td class="view_gtotal_finishing" align="right">
                                    <?= number_format($total); ?>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        <?php endif;?>
    </div>
    <?= form_close() ?>
</div>