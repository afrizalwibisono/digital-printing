<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ============================
$lang['order_produk_title_manage']			= 'Data Order';
$lang['order_produk_title_new']				= 'Data Order Baru';
$lang['order_produk_title_edit']			= 'Edit Data Order';

// from/table
$lang['order_produk_no_transaksi']			= 'No. Transaksi';
$lang['order_produk_no_po']					= 'No. PO';
$lang['order_produk_lampiran']				= 'Lampiran';
$lang['order_produk_id_konsumen']			= 'Nama';
$lang['order_produk_tgl_order']				= 'Tanggal Order';
$lang['order_produk_tgl_selesai']			= 'Tanggal Permintaan Selesai';
$lang['order_produk_jam_selesai']			= 'Jam Permintaan Selesai';
$lang['order_produk_nama_pekerjaan']		= 'Nama File';
$lang['order_produk_id_kategori']			= 'Kategori Produk';
$lang['order_produk_id_produk']				= 'Produk';
$lang['order_produk_gambar_cetak']			= 'Gambar Thumbnail';
$lang['order_produk_ukuran_cetak']			= 'Ukuran Cetak';
$lang['order_produk_jml_order']				= 'Jumlah Order';
$lang['order_produk_biaya_cetak']			= 'Biaya Cetak';
$lang['order_produk_biaya_desain']			= 'Biaya Desain';
$lang['order_produk_biaya_finishing']		= 'Biaya Finishing';
$lang['order_produk_note_produk']			= 'Note Produk';
$lang['order_produk_st_finishing']			= 'Status Finishing';
$lang['order_produk_st_biaya_cetak']		= 'Status Biaya Cetak';
$lang['order_produk_st_urgent']				= 'Status Urgensi';
$lang['order_produk_st_order']				= 'Status Order';
$lang['order_produk_st_urgent_non_urgent']	= 'Non Urgent';
$lang['order_produk_st_urgent_urgent']		= 'Urgent';
$lang['order_produk_id_finishing']			= 'Finishing';
$lang['order_produk_hanya_cetak']			= 'Hanya Biaya Cetak';
$lang['order_produk_cetak_dan_bahan']		= 'Biaya Cetak Dan Bahan';
$lang['order_produk_jml_pakai_finishing']	= 'Jumlah Pakai';
$lang['order_produk_note_finishing']		= 'Note Finishing';
$lang['order_produk_harga_finishing']		= 'Harga Finishing';
$lang['order_produk_total']					= 'Sub Total Biaya';
$lang['order_produk_total_order']			= 'Total Order';
$lang['order_produk_no_tlp']				= 'No. Telepon';
$lang['order_produk_detail_gambar']			= 'Info Gambar';
$lang['order_produk_harga_persatuan']		= 'Harga Persatuan';
$lang['order_produk_st_konsumen']			= 'ST Konsumen';
$lang['order_produk_tipe_order']            = 'Tipe Order';

$lang['order_produk_tgl_awal']				= 'Tanggal Awal';
$lang['order_produk_tgl_akhir']				= 'Tanggal Akhri';

// label
$lang['order_produk_finishing_standart']	= 'Finishing Standart';
$lang['order_produk_finishing_standart_digunakan'] = 'Finishing standart masih digunakan';
$lang['order_produk_finishing_custom']		= 'Finishing Custom';
$lang['order_produk_dimensi_susai_gambar']	= 'Custom Ukuran Cetak';
$lang['order_produk_list_finishing']		= '[ List Barang Pakai Finishing ]';
$lang['order_produk_list_order']			= '[ List Order Produk ]';

$lang['order_produk_no'] 		= 'NO';
$lang['order_produk_deskripsi']	= 'Deskripsi';
$lang['order_produk_harga']		= 'Harga';
$lang['order_produk_subtotal']	= 'Sub Total';

$lang['order_produk_label_page']	= 'Halaman';
$lang['order_produk_label_jumlah']	= 'Jml Cetak';




//button
$lang['order_produk_btn_new']				= 'Baru';
$lang['order_produk_btn_delete']			= 'Hapus';
$lang['order_produk_btn_save']				= 'Simpan Transaksi';
$lang['order_produk_btn_reset']				= 'Reset';
$lang['order_produk_btn_cancel']			= ' Kembali ke Index';
$lang['order_produk_btn_pakai_finishing']	= 'Tamabh Finishing';
$lang['order_produk_btn_tambah_order']		= 'Tambah Daftar Order';
$lang['order_produk_or']					= 'Atau';


//messages
$lang['order_produk_canceled']				= 'Transaksi Berhasil Dibatalkan';
$lang['order_produk_del_error']				= 'Anda belum memilih data order produk yang akan dihapus.';
$lang['order_produk_del_failure']			= 'Tidak dapat menghapus data order produk';
$lang['order_produk_delete_confirm']		= 'Apakah anda yakin akan menghapus data order produk terpilih?';
$lang['order_produk_cancel_confirm']		= 'Apakah anda yakin akan membatalkan Transaksi?';
$lang['order_produk_deleted']				= 'order produk berhasil dihaspus';
$lang['order_produk_no_records_found']		= 'Data tidak ditemukan.';
$lang['order_produk_no_item_order']			= 'Tidak ada barang yang diorder';
$lang['order_produk_cannot_delete'] 		= 'Order dengan nomer transaksi: %s Tidak dapat dihapus';

$lang['order_produk_create_failure']		= 'Data order produk gagal disimpan';
$lang['order_produk_create_success']		= 'Data order produk berhasil disimpan';

$lang['order_produk_edit_success']			= 'Data order produk berhasil disimpan';
$lang['order_produk_invalid_id']			= 'ID tidak Valid';

