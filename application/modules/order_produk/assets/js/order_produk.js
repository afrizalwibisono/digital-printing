var _extFile, _iSize, _fileSize, _fileName;
var _cek_min= false;

$(document).ready(function(){
	var selectedKonsumen = sessionStorage.getItem("id_konsumen");
	if(selectedKonsumen){
		var setOption = setTimeout(function(){
			setSelectedKonsumen(selectedKonsumen);
			clearTimeout(setOption);
		}, 1000);
		// setSelectedKonsumen(selectedKonsumen);
	}
	// cek_konsumen();
	setDefaultJam();
	checkAutomeasuring();
	//div finishing
	var cekdiv = $("#st_finishing").val();
	if (cekdiv==0 || cekdiv=='') {
		$("#d-hide").hide();
		$("#t-hide").hide();
	}else{
		$("#d-hide").show();
		$("#t-hide").show();
	}
	cek_urgent();
	$("#id_konsumen").select2({
		ajax: {
			url: siteurl+"order_produk/get_konsumen",
			type: "post",
			dataType: 'json',
			delay:250,
			data: function (params){
				return {
					search: params.term
				}
			},
			processResults: function (response) {
			    return {
			        results: $.map(response, function (item) {
				                return {
				                    text: item.text,
				                    tipe: item.tipe,
				                    id: item.id
				                }
						    })
				    }
			   	}
		},
		placeholder : '-Pilih Konsumen-', 
		allowClear: true,
		templateResult: formatDesign,
		width: '100%'
	});
	
	$("#id_kategori").select2({placeholder : '-Pilih Kategori Produk-', allowClear: true}); 
  	$("#id_produk").select2({placeholder : '-Pilih Produk-', allowClear: true}); 
  	$("#id_konversi").select2({placeholder : '-Pilih Konversi-', allowClear: true}); 
  	// $("#id_konversi_jml_order").select2({placeholder : '-Pilih Konversi-', allowClear: true}); 
  	$("#id_konversi_jml_finishing").select2({placeholder : '-Pilih Konversi-', allowClear: true}); 
  	$("#st_finishing").select2({placeholder : '-Pilih Jenis Finishing-', allowClear: true}); 
  	$("#st_biaya_cetak").select2({placeholder : '-Pilih Status Biaya-', allowClear: true}); 
  	// $("#st_urgent").select2({placeholder : '-Pilih -'}); 
  	$("#biaya_desain, #biaya_cetak1, #harga_finishing, #jml_order").number(true);
  	$("#p, #l").number(true,2);
  	
  	$("#st_dimensi").on("click", function(){
		  console.log('checkbox jalan bosku', $(this).prop("checked"));
		if($(this).prop("checked")){
			$("#p, #l").removeAttr('readonly');
		}else{
			$("#p,#l").attr('readonly',true);
		}
	});
	$("#id_kategori").on("change", function(){
		$("#kategori_name").val($(this).find('option:selected').text());
		load_produk($(this).val());
	});
	$("#id_produk").on("change", function(){
		reset_form();
		cek_order($(this).val());
	})
	$("#add_finishing").on("click", function(){
		add_finishing();
		clear_input_finishing();
	});
	$("#add_produk").on("click", function(e){
		e.preventDefault();
		upload_produk();
	});
	$("#add_produk_edit").on("click", function(e){
		e.preventDefault();
		upload_produk();
		// console.log("button");
	});
	$("#st_finishing").on("change", function(){
		if($(this).val()==1){
			load_produk_finishing();
			$("#d-hide").show();
			$("#t-hide").show();
		}else{
			$("#id_finishing option[value!='']").remove();
			$("#d-hide").hide();
			$("#t-hide").hide();
			clear_table_finishing();
		};
	});
	$("#gambar").change(function(){
		_iSize = ($(this)[0].files[0].size / 1024);
		_fileSize  = (Math.round((_iSize/1024)*100)/100);
		_extFile = $(this).val().split('.').pop().toLowerCase();
		_fileName =  $(this).val().split('\\').pop();

		if (_extFile == 'pdf') 
        {
        	countPdfPage(URL.createObjectURL($(this).get(0).files[0]));
        	$("#table-pdf tbody tr").remove();
        	$("#pdf-modal").modal('toggle');
        }else if(_extFile == 'jpg' || _extFile == 'jpeg' || _extFile == 'tif' || _extFile == 'tiff'){
        	// cek file image size
        	readURL(this);
        	if (_extFile == 'jpeg' || _extFile == 'jpg') {
        		if (_fileSize <= 50) {
		        	handleFiles(this);
		        	$("#preview-gambar").show();
	        	}
        	}
        }
		$("#nama_pekerjaan").val($(this).val().split('\\').pop());
    }); 
    $("#p").keyup(function(){
    	$("#hide_p").val($(this).val().replace(/[","]/g,".") * $("#id_konversi").find('option:selected').data('jml_kecil'));
    });
    $("#l").keyup(function(){
    	$("#hide_l").val($(this).val().replace(/[","]/g,".") * $("#id_konversi").find('option:selected').data('jml_kecil'));
    });
    $("#l").change(function () {
    	if ($(this).val() >0) {
    		cek_minimal_order();
    	}
    });
    $("#p").change(function () {
    	if ($(this).val() >0) {
    		cek_minimal_order();
    	}
    })
    $("#jml_order").focusout(function(){
    	if ($(this).val() > 0 ) {
    		cek_minimal_order();
	    	get_biaya_cetak();
    	}
    });
    $("#jml_finishing").change(function(){
    	if ($(this).val() >0) {
    		get_harga_finishing();
    	}
    });
    $("#st_urgent").on("click", function(){
		if($(this).prop("checked")){
			const defaultJam = parseFloat($("#default_jam").val())
			$(".d-tgl-selesai").show();
			let today = new Date();
			today.setHours(today.getHours() + defaultJam);
			$("#tgl_selesai").val(today.getDate()+'-'+(today.getMonth()+1 <10 ? '0'+(today.getMonth()+1) : today.getMonth()+1)+'-'+today.getFullYear());
			$("#jam_selesai").val( ( today.getHours() < 10 ? '0'+today.getHours() : today.getHours()) +':'+ (today.getMinutes()<10 ? '0'+today.getMinutes() : today.getMinutes()) );
			$("#jam_selesai").trigger("change");
		}else{
			$(".d-tgl-selesai").hide();
			$("#kenaikan_persen").val("");
			$("#kenaikan_value").val("");
			setDefaultJam();
		}
	});
	$("#id_konsumen").change(function(){
		if ($(this).val().length >0) {
			get_tlp();
			// cek_konsumen();
		}
	});
	$('#tgl_selesai').datepicker({
        todayBtn: "linked", 
      	format: "dd-mm-yyyy", 
      	autoclose: true, 
      	clearBtn: true, 
      	todayHighlight: true 
    });
    $("#tgl_selesai, #jam_selesai").change(function(){
	    if ($("#tgl_selesai").val().length>0 && $("#jam_selesai").val().length > 0) {

	    		fromDate = parseInt(new Date().getTime()/1000); 
	    		var tgl = $("#tgl_selesai").val().split('-');
	    		var tgl1 = tgl[2]+'-'+tgl[1]+'-'+tgl[0];
	    		toDate = new Date(tgl1);
	    		toDate.setHours($("#jam_selesai").val().substring(0,2));
	    		toDate.setMinutes($("#jam_selesai").val().substring(2,2));
			    // toDate = parseInt(new Date($("#tgl_selesai").val()+' '+ $("#jam_selesai").val()).getTime()/1000);
			    var timeDiff = ((toDate.getTime()/1000) - fromDate)/3600;
		    	$.ajax({
					url : siteurl+"order_produk/get_kenaikan",
					type : "post",
					data : {selisih : timeDiff},
					dataType : "json",
					success : function(msg){
						if (msg['type'] == 'success') {
							$("#kenaikan_persen").val(msg['persen']);
						}else{
							alertify.error(msg['msg']);
							return false;	
						}
					},
					error: function(error){
						alert(JSON.stringify(error));
					}
				});
				if ($("#tgl_selesai").val() != "" && $("#jml_order").val()>0) {
					get_biaya_cetak();
				}
	    }
    });
    $("#btn-switch").click(function(){
    	var a = $("#p").val();
    	var b = $("#l").val();
    	$("#p").val(b).trigger('keyup');
    	$("#l").val(a).trigger('keyup');

    });
    $("#gambar").click(function(){
    	var $el = $('#gambar');
		$el.wrap('<form>').closest('form').get(0).reset();
		$el.unwrap();
		reset_form();
		$("#id_produk").trigger("change");

	});
	$(".add-lampiran").on('click', '*', function(e) {  
		document.getElementById('lampiran').click();
	});
	$("#lampiran").change(function(){
		previewLampiran(this);
	})
});

function checkAutomeasuring(){
	const autoImageMeasuring = $("#auto-image-measuring-cnf").val();
	if(parseInt(autoImageMeasuring) === 0){
		$(".custom-check").hide();
		$("#st_dimensi").prop('checked', true);
		if($("#st_dimensi").prop("checked")){
			$("#p, #l").removeAttr('readonly');
		}else{
			$("#p,#l").attr('readonly',true);
		}
	}else{
		$(".custom-check").show();
		$("#st_dimensi").prop('checked', false);
		if($("#st_dimensi").prop("checked")){
			$("#p, #l").removeAttr('readonly');
		}else{
			$("#p,#l").attr('readonly',true);
		}
	}
}

function setSelectedKonsumen(idKonsumen){
	if(idKonsumen){
		$.ajax({
			url : siteurl+"order_produk/get_selected_konsument",
			type : "post",
			dataType : "json",
			data : {id_konsumen : idKonsumen},
			success : function(msg){
				let option = new Option(msg[0].text, msg[0].id, true, true)
				$("#id_konsumen").append(option).trigger("change");
			},
			error : function(err){
				console.log(err);
			}
		});
	}
}

function setDefaultJam(){
	if($("#st_urgent").prop("checked") == false){
		const defaultJam = parseFloat($("#default_jam").val())
		let today = new Date();
		today.setHours(today.getHours() + defaultJam);
		$("#tgl_selesai").val(today.getDate()+'-'+(today.getMonth()+1 <10 ? '0'+(today.getMonth()+1) : today.getMonth()+1)+'-'+today.getFullYear());
		$("#jam_selesai").val( ( today.getHours() < 10 ? '0'+today.getHours() : today.getHours()) +':'+ (today.getMinutes()<10 ? '0'+today.getMinutes() : today.getMinutes()) );
		$(".d-tgl-selesai").hide();

	}
}
function cek_konsumen(){
	if ($("#id_konsumen").val().length > 0) {
		var datakonsumen = $("#id_konsumen").find(":selected").data("tipe");
		const dataKonsumen = $("#id_konsumen").select2('data')[0];
		const tipe = dataKonsumen.tipe;
		if (tipe == 2) {
			$("#d-lampiran").show();
			// $("#lampiran").prop('required', true);
			$("#no_po").prop('required', true);
		}else{
			$("#d-lampiran").hide();
			// $("#lampiran").prop('required', false);
			$("#no_po").prop('required', false);
		}
	}
}
// load produk
function load_produk(id_kategori){
	if(id_kategori){
		$.ajax({
			url : siteurl+"order_produk/cari_produk",
			type : "post",
			data : {id_kategori : id_kategori},
			dataType : "json",
			success : function(msg){
				$("#id_produk option[value!='']").remove();
				if(msg['type'] == 'success'){
					$("#id_produk").select2({ 
						placeholder : "-- Pilih Produk --",
						allowClear : true,
						data: msg['data']
					}).val(sessionStorage.getItem("id_produk")).trigger("change");
					// console.log('Data : ', msg['data']);
				}
			},
			error: function(error){
				alert(JSON.stringify(error));
			}
		});
	}
}

// cek status2 order produk
function cek_order(id_produk){
	if(id_produk){
		$.ajax({
			url : siteurl+"order_produk/cek_order",
			type : "post",
			data : {id_produk : id_produk},
			dataType : "json",
			success : function(msg){
				if(msg['type'] == 'success'){
					$("#st_ukuran").val(msg['data']['st_ukuran']);
					$("#p_kategori").val(msg['data']['p_kategori']);
					$("#l_kategori").val(msg['data']['l_kategori']);
					$("#st_minimal_order").val(msg['data']['st_minimal_order']);
					$("#jml_minimal_order").val(msg['data']['jml_minimal_order']);
					$("#panjang_minimal").val(msg['data']['panjang_minimal']);
					$("#lebar_minimal").val(msg['data']['lebar_minimal']);
					$("#st_tipe").val(msg['data']['st_tipe']);
					$("#jml_cetak_bom").val(msg['data']['jml_cetak_bom']);

					if(msg['data']['st_tipe'] == 0){
						$(".ukuran-cetak").hide();
						$(".custom-check").hide();
						$("#id_konversi").val(11).trigger("change");
						$("#p").val(1);
						$("#l").val(1);
					}else{
						$(".ukuran-cetak").show();
						const autoImageMeasuring = $("#auto-image-measuring-cnf").val();
						if(parseInt(autoImageMeasuring) === 0){
							$(".custom-check").hide();
						}else{
							$(".custom-check").show();
						}
					}

				}
			},
			error: function(error){
				alert(JSON.stringify(error));
			}
		});
	}
}
// load produk finishing
function load_produk_finishing(){
	var id_kategori = $("#id_kategori").val()
	if (id_kategori.length>0) 
	{
		$.ajax({
		url : siteurl+"order_produk/cari_produk_finishing",
		type : "post",
		data : {id_kategori:id_kategori},
		dataType : "json",
		success : function(msg)
		{
			$("#id_finishing option[value!='']").remove();
			if(msg['type'] == 'success'){
				$("#id_finishing").select2({ 
					placeholder : "-- Pilih Produk --",
					allowClear : true,
					data: msg['data']
				}).val(sessionStorage.getItem("id_finishing")).trigger("change");
				// console.log('Data : ', msg['data']);
			}
		},
		error: function(error)
		{
			alert(JSON.stringify(error));
		}
	});
	}
}
function readURL(input) {
    if (input.files && input.files[0]) {

        var reader = new FileReader();
        var tempImage1 = new Image();
        var file = input.files[0];
        const autoImageMeasuring = $("#auto-image-measuring-cnf").val();

        if (parseInt(autoImageMeasuring) === 1 ) {
        	if (file && file.name) {
	            EXIF.getData(file, function() {
	                var exifData = EXIF.getAllTags(this);
	                if (exifData) {
	                	var XResolution = exifData['XResolution'];
	                	var YResolution = exifData['YResolution'];
	                	var imgWidth= exifData['PixelXDimension'];
	                	var imgHeight= exifData['PixelYDimension'];
	        			if (typeof XResolution == "undefined" || typeof YResolution == "undefined") {
	        				var id_produk = $("#id_produk").val();
	        				if (id_produk!="") {
	        					var target = $("#target_server").val();
	        					var alamat_server = $("#alamat_server").val();
								var dtForm = new FormData($("#frm_order_produk")[0]);
								$.ajax({
									url : (target == 0 ? alamat_server+"/index.php/upload/get_size" : baseurl+"order_produk/get_size"),
									type: 'post',
									dataType: 'json',
									data: dtForm,
									processData: false,
									contentType: false,
									async : true,
									// this part is progress bar
						            xhr: function () {
						                var xhr = new window.XMLHttpRequest();
						                xhr.upload.addEventListener("progress", function (evt) {
						                    if (evt.lengthComputable) {
						                        var percentComplete = evt.loaded / evt.total;
						                        percentComplete = parseInt(percentComplete * 100);
						                        $('#loader_percent').text(percentComplete + '%');
						                    }
						                }, false);
						                return xhr;
						            },
									success : function(msg){
										XResolution = msg[0];
										YResolution = msg[1];
										imgWidth	= msg[2];
										imgHeight = msg[3];
										$("#XResolution").val(XResolution);
					        			$("#YResolution").val(YResolution);
					        			$("#imageWidth").val(imgWidth);
										$("#imageHeight").val(imgHeight);
					        			konversi_gambar();
									},
									error:function(error){
										console.log(JSON.stringify(error));
									}
								});
							}else{
								return false;
							}
	        			}else{
	        				$("#XResolution").val(XResolution);
		        			$("#YResolution").val(YResolution);
		        			$("#imageWidth").val(imgWidth);
							$("#imageHeight").val(imgHeight);
		        			konversi_gambar();
	        			}
	                } else {
	                    alert("No EXIF data found in image '" + file.name + "'.");
	                }
	            });
	        }
	        reader.readAsDataURL(input.files[0]);
        }
    }
}

function konversi_gambar(){
	var imageWidth 	= $("#imageWidth").val();
	var imageHeight = $("#imageHeight").val();
	var XResolution = $("#XResolution").val();
	var YResolution = $("#YResolution").val();
	var realWidth 	= (imageWidth/XResolution) * 25.4;
	var realHeight 	= (imageHeight/YResolution) * 25.4;
	if (realWidth > realHeight){
		$("#hide_p").val(realWidth);
		$("#hide_l").val(realHeight);
	}else{
		$("#hide_p").val(realHeight);
		$("#hide_l").val(realWidth);
	}
	$("#id_konversi").val(11).trigger("change");
	$("#info-gambar").text('Width : '+imageWidth+"px  Height: "+imageHeight+"px ("+XResolution+","+YResolution+")DPI");
	$("#detail_gambar").val('Width : '+imageWidth+"px  Height: "+imageHeight+"px ("+XResolution+","+YResolution+")DPI")
	cek_ukuran_gambar();
}
function get_biaya_cetak(){
	var id_produk 	= $("#id_produk").val();
	var id_konsumen = $("#id_konsumen").val();
	var tgl_order	= $("#tgl_order").val();
	var st_biaya_cetak	= $("#st_biaya_cetak").val();
	var p = parseFloat($("#p").val().replace(/[","]/g,"."));
	var l =	parseFloat($("#l").val().replace(/[","]/g,"."));
	var id_konversi_gambar = $("#id_konversi").val();
	var jml_order 		= $("#jml_order").val();
	var nama_konsumen_baru = $("#nama_konsumen_baru").val();
	if(nama_konsumen_baru !== ""){
		id_konsumen = 0;
	}
	// console.log(jml_order);
	var id_konversi_jml_order = $("#id_konversi_jml_order").val();
	var cek = false;
	if (id_produk=='') {
		cek = false;
		alertify.error('Pilih Produk Dahulu!');
		return false;
	}else if (id_konsumen === "") {
		cek = false;
		alertify.error('Pilih Konsumen Dahulu!');
		return false
	}else if (tgl_order=="") {
		cek = false;
		alertify.error('Isikan Tanggal Order Dahulu!');
		return false
	}else if (p =="") {
		cek = false;
		alertify.error('Isikan Ukuran Panjang Cetak Dahulu!');
		return false
	}else if (l=="") {
		cek = false;
		alertify.error('Isikan Lebar Cetak Dahulu!');
		return false
	}else if (id_konversi_gambar =="") {
		cek = false;
		alertify.error('Pilih Konversi Satuan Panjang Dahulu!');
		return false
	}else if (jml_order=="") {
		cek = false;
		alertify.error('Isikan Jumlah Dahulu!');
		return false
	}else if (id_konversi_jml_order=="") {
		cek = false;
		alertify.error('Pilih Konversi Jumlah Order Dahulu!');
		return false
	}else{
		cek = true;
	}

	if (cek==true) {
		$.ajax({
			url : baseurl+"order_produk/get_biaya_cetak",
			type : "post",
			dataType : "json",
			data : {id_produk 				: id_produk, 
					id_konsumen				:id_konsumen,
					tgl_order				: tgl_order,
					st_biaya_cetak			: st_biaya_cetak,
					p 						: p,
					l 						: l,
					jml_order				: jml_order,
					id_konversi_gambar		: id_konversi_gambar,
					id_konversi_jml_order	: id_konversi_jml_order},
			success : function(msg){
				// console.log(msg['luas']);
				$("#biaya_cetak").val(msg['biaya_cetak']);
				$("#harga_cetak_persatuan_terkecil").val(msg['biaya_cetak_by']);
				$("#qty_harga_by").val(msg['qty_harga_by']);
				$("#harga_by").val(msg['harga_by']);
				$("#satuan_cetak_by").val(msg['id_konversi_harga_by'])
				var tambahan = parseFloat($("#kenaikan_persen").val()/100) * msg['biaya_cetak'];
				$("#kenaikan_value").val(tambahan);
				$("#biaya_cetak1").val(msg['biaya_cetak']+tambahan);
			},
			error:function(error){
				console.log(JSON.stringify(error));
			}
		});
	}else{
		alertify.error("Isikan Data Dengan Lengkap Dahulu");
		return false;
	}
}
function get_harga_finishing(){
	var id_produk 	= $("#id_finishing").val();
	var id_konsumen = $("#id_konsumen").val();
	var tgl_order	= $("#tgl_order").val();
	var p = parseFloat($("#p").val().replace(/[","]/g,"."));
	var l =	parseFloat($("#l").val().replace(/[","]/g,"."));
	var id_konversi_gambar = $("#id_konversi").val();
	var jml_order 		= parseFloat($("#jml_order").val().replace(/[","]/g,"."));
	var jml_finishing 	= parseFloat($("#jml_finishing").val().replace(/[","]/g,"."));
	var id_konversi_jml_order = $("#id_konversi_jml_order").val();
	var nama_konsumen = $("#nama_konsumen").val();
	if(nama_konsumen_baru !== ""){
		id_konsumen = 0;
	}
	var cek = false;
	if (id_produk=='') {
		cek = false;
		alertify.error('Pilih Produk Dahulu!');
		return false;
	}else if (id_konsumen=="" && nama_konsumen == "") {
		cek = false;
		alertify.error('Pilih Konsumen Dahulu!');
		return false
	}else if (tgl_order=="") {
		cek = false;
		alertify.error('Isikan Tanggal Order Dahulu!');
		return false
	}else if (p =="") {
		cek = false;
		alertify.error('Isikan Ukuran Panjang Cetak Dahulu!');
		return false
	}else if (l=="") {
		cek = false;
		alertify.error('Isikan Lebar Cetak Dahulu!');
		return false
	}else if (id_konversi_gambar =="") {
		cek = false;
		alertify.error('Pilih Konversi Satuan Panjang Dahulu!');
		return false
	}else if (jml_order=="") {
		cek = false;
		alertify.error('Isikan Jumlah Dahulu!');
		return false
	}else{
		cek = true;
	}

	if (cek==true) {
		$.ajax({
			url : baseurl+"order_produk/get_harga_finishing",
			type : "post",
			dataType : "json",
			data : {id_produk 				: id_produk, 
					id_konsumen				: id_konsumen,
					tgl_order				: tgl_order,
					p 						: p,
					l 						: l,
					id_konversi_gambar		: id_konversi_gambar,
					jml_order				: jml_order,
					jml_finishing 			: jml_finishing,
					id_konversi_jml_order	: id_konversi_jml_order},
			success : function(msg){
				$("#harga_finishing").val(msg);
			},
			error:function(error){
				console.log(JSON.stringify(error));
			}
		});
	}else{
		alertify.error("Isikan Data Dengan Lengkap Dahulu");
		return false
	}
}

function konversi_panjang(obj){
	var jml_kecil = parseFloat($(obj).find('option:selected').data('jml_kecil'));
	var jml_besar = parseFloat($(obj).find('option:selected').data('jml_besar'));
	var l 	= parseFloat($("#hide_l").val());
	var p 	= parseFloat($("#hide_p").val());
	var real_lebar_gambar = (l/jml_kecil) * jml_besar;
	var real_panjang_gambar =(p/jml_kecil) * jml_besar;

	$("#l").val(real_lebar_gambar);
	$("#p").val(real_panjang_gambar);
}
// add discon2 to list
function add_finishing(){
	var id_finishing = $("#id_finishing").val();
	var finishing_text = $("#id_finishing option:selected").text();
	var jml_finishing = $("#jml_finishing").val().replace(/,/g,"");
	var id_konversi_jml_finishing = $("#id_konversi_jml_finishing").val();
	var id_konversi_jml_finishing_text = $("#id_konversi_jml_finishing option:selected").text();
	var note_finishing = $("#note_finishing").val();
	var harga_finishing = $("#harga_finishing").val().replace(/,/g,"");


	if (harga_finishing>0) {
		var $row = $("<tr>"
	  					+"<td></td>"
	  					+"<td>"
	  						+"<input type='hidden' name='list_id_finishing[]' value='"+id_finishing+"' />"
	  						+"<input type='hidden' name='list_jml_finishing[]' value='"+jml_finishing+"' />"
	  						+"<input type='hidden' name='list_id_konversi_jml_finishing[]' value='"+id_konversi_jml_finishing+"'/>"
	  						+"<input type='hidden' name='list_note_finishing[]' value='"+note_finishing+"'/>"
	  						+"<input type='hidden' name='list_harga_finishing[]' value='"+harga_finishing+"'/>"
	  						+finishing_text
	  					+"- <a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>"
	  					+"<td>"+note_finishing+"</td>"
	  					+"<td>"
	  						+jml_finishing +"Set"
	  					+"</td>"
	  					+"<td>"+$.number(harga_finishing)+"</td>"
	  				+"</tr>");
		var pj = $("#tdet_finishing tr").length;
		var cek = false;

		if(pj > 2){
			for (var j = 0; j <= pj - 1; j++) {
				var kode = $("#tdet_finishing tr:eq("+(j+1)+")").find("input[name='list_id_finishing[]']").val();
				if(kode != "" && kode == id_finishing ){
					alertify.error("Data Finishing Sudah Ada Dalam List!");
					cek = true;
					break;
				}
			}	
		}
		if(cek == false){
			$("#tdet_finishing tbody").append($row);
			hitung_total_finishing();
			buat_no_finishing();
		}
	}else{
        alertify.error("Isikan Data Finishing Dengan Lengkap Dahulu!");
        return false;	
	}
}

// tambah daftar order ke list order
function upload_produk(){

	var id_produk 	= $("#id_produk").val();
	var id_konsumen = $("#id_konsumen").val();
	var tgl_order	= $("#tgl_order").val();
	var st_biaya_cetak	= $("#st_biaya_cetak").val();
	var p = parseFloat($("#p").val().replace(/[","]/g,"."));
	var l =	parseFloat($("#l").val().replace(/[","]/g,"."));
	var gambar = $("#gambar").val()
	var id_konversi_gambar = $("#id_konversi").val();
	var jml_order 		= parseFloat($("#jml_order").val().replace(/[","]/g,"."));
	var id_konversi_jml_order = $("#id_konversi_jml_order").val();
	var biaya_cetak = $("#biaya_cetak").val();
	var st_finishing = $("#st_finishing").val();
	var nama_konsumen = $("#nama_konsumen").val();
	// cek_minimal_order();

	var cek = false;
	if (id_produk=='') {
		cek = false;
		alertify.error('Pilih Produk Dahulu!');
		return false;
	}else if (id_konsumen=="" && nama_konsumen == "") {
		cek = false;
		alertify.error('Pilih Konsumen Dahulu!');
		return false
	}else if (tgl_order=="") {
		cek = false;
		alertify.error('Isikan Tanggal Order Dahulu!');
		return false
	}else if (p =="") {
		cek = false;
		alertify.error('Isikan Ukuran Panjang Cetak Dahulu!');
		return false
	}else if (l=="") {
		cek = false;
		alertify.error('Isikan Lebar Cetak Dahulu!');
		return false
	}else if (id_konversi_gambar =="") {
		cek = false;
		alertify.error('Pilih Konversi Satuan Panjang Dahulu!');
		return false
	}else if (jml_order=="") {
		cek = false;
		alertify.error('Isikan Jumlah Dahulu!');
		return false
	}else if (id_konversi_jml_order=="") {
		cek = false;
		alertify.error('Pilih Konversi Jumlah Order Dahulu!');
		return false
	}else if (gambar == "") {
		cek = false;
		alertify.error('Pilih Gambar Yang Akan Diupload Dahulu!');
		return false
	}else if (st_finishing=="") {
		cek = false;
		alertify.error('Pilih Status Finishing Dahulu!');
		return false
	}else{
		cek = true;
	}

	if (cek==true) {
		var target = $("#target_server").val();
		var alamat_server = $("#alamat_server").val();
		// 1: file diupload, 0 : file tidak diupload
		const uploadFile = $("#upload-file-cnf").val();
		function doUpload(){
			if (parseInt(uploadFile) === 1) {
				$.ajax({
				url : target_link,
				type: 'post',
				dataType: 'json',
				data: dtForm,
				processData: false,
				contentType: false,
				// this part is progress bar
				xhr: function () {
					var xhr = new window.XMLHttpRequest();
					xhr.upload.addEventListener("progress", function (evt) {
						if (evt.lengthComputable) {
							var percentComplete = evt.loaded / evt.total;
							percentComplete = parseInt(percentComplete * 100);
							$('#loader_percent').text(percentComplete + '%');
						}
					}, false);
					return xhr;
				},
				success : function(msg){
					if (msg['type']== 'success') {
						// cek file image size
						if ( (_extFile == 'jpg' || _extFile == 'jpeg') &&  parseFloat(_fileSize) <= 50) {
							uploadThumbnail();
						}else{
							setDefaultThumbnail();
						}
					}else{
						alertify.error(msg['msg']);
						return false;
					}
				},
				error:function(error){
					alertify.alert("Pastika server local menyala, dan dapat diakses dari komputer lain. Cek dengan memanggil alamat [ "+  alamat_server +" ] pada browser Anda.");
				}
			});

			}else{
				// cek file image size
				if ( (_extFile == 'jpg' || _extFile == 'jpeg') &&  parseFloat(_fileSize) <= 50) {
					uploadThumbnail();
				}else{
					setDefaultThumbnail();
				}	
			}
		}

		// jika file diupload
		var dtForm = new FormData($("#frm_order_produk")[0]);
		var target_link = (target == 0 ? alamat_server+"/index.php/upload" : baseurl+"order_produk/upload");
		if (_cek_min == false) {
			alertify.confirm('Warning!!!', 'Ukuran Gambar tidak sesuai dengan kategori yang dipilih! Tetap Gunakan?',function(){
				return doUpload();
			})
			// alertify.confirm('Warning!!!', 'Ukuran Gambar tidak sesuai dengan kategori yang dipilih! Tetap Gunakan?',function(){
			// 	if (uploadFile) {
			// 		$.ajax({
			// 		url : target_link,
			// 		type: 'post',
			// 		dataType: 'json',
			// 		data: dtForm,
			// 		processData: false,
			// 		contentType: false,
			// 		// this part is progress bar
			// 		xhr: function () {
			// 			var xhr = new window.XMLHttpRequest();
			// 			xhr.upload.addEventListener("progress", function (evt) {
			// 				if (evt.lengthComputable) {
			// 					var percentComplete = evt.loaded / evt.total;
			// 					percentComplete = parseInt(percentComplete * 100);
			// 					$('#loader_percent').text(percentComplete + '%');
			// 				}
			// 			}, false);
			// 			return xhr;
			// 		},
			// 		success : function(msg){
			// 			if (msg['type']== 'success') {
			// 				// cek file image size
			// 				if ( (_extFile == 'jpg' || _extFile == 'jpeg') &&  parseFloat(_fileSize) <= 50) {
			// 					uploadThumbnail();
			// 				}else{
			// 					setDefaultThumbnail();
			// 				}
			// 			}else{
			// 				alertify.error(msg['msg']);
			// 				return false;
			// 			}
			// 		},
			// 		error:function(error){
			// 			alertify.alert("Pastika server local menyala, dan dapat diakses dari komputer lain. Cek dengan memanggil alamat [ "+  alamat_server +" ] pada browser Anda.");
			// 		}
			// 	});

			// 	}else{
					
			// 	}
			// },
		}else{
			return doUpload();
			// $.ajax({
			// 	url : target_link,
			// 	type: 'post',
			// 	dataType: 'json',
			// 	data: dtForm,
			// 	processData: false,
			// 	contentType: false,
			// 	// this part is progress bar
			// 	xhr: function () {
			// 		var xhr = new window.XMLHttpRequest();
			// 		xhr.upload.addEventListener("progress", function (evt) {
			// 			if (evt.lengthComputable) {
			// 				var percentComplete = evt.loaded / evt.total;
			// 				percentComplete = parseInt(percentComplete * 100);
			// 				$('#loader_percent').text(percentComplete + '%');
			// 			}
			// 		}, false);
			// 		return xhr;
			// 	},
			// 	success : function(msg){
			// 		if (msg['type']== 'success') {
			// 			// cek file image size
			// 			if ( (_extFile == 'jpg' || _extFile == 'jpeg') &&  parseFloat(_fileSize) <= 50) {
			// 				// console.log(_fileSize);
			// 				// console.log('uploadthumbail file');
			// 				uploadThumbnail();
			// 			}else{
			// 				setDefaultThumbnail();
			// 			}
			// 		}else{
			// 			alertify.error(msg['msg']);
			// 			return false;
			// 		}
			// 	},
			// 	error:function(error){
			// 		alertify.alert("Pastika server local menyala, dan dapat diakses dari komputer lain. Cek dengan memanggil alamat [ "+  alamat_server +" ] pada browser Anda.");
			// 	}
			// });
		}
		
	}else{
		alertify.error("Isikan Data Dengan Lengkap Dahulu");
		return false
	}
}

function uploadThumbnail(){
	var formId = $("#formId").val();
	var base64 = document.getElementById("preview").toDataURL("image/jpeg");
	var nama_pekerjaan = $("#nama_pekerjaan").val();
	var no_transaksi = $("#no_transaksi").val();
	$.ajax({
			url : baseurl+"order_produk/uploadThumbnail",
			type: 'post',
			data: { img:base64, nama_pekerjaan:nama_pekerjaan, no_transaksi:no_transaksi },
			success:function(msg){
				if (formId==0) {
					save();
				}else{
					save_edit();
				}
			}
		})
}

function setDefaultThumbnail(){
	var formId = $("#formId").val();
	var nama_pekerjaan = $("#nama_pekerjaan").val();
	var no_transaksi = $("#no_transaksi").val();
	if (_extFile == 'jpeg' || _extFile == 'jpg') {
		var kode_set = 1;
	}else if (_extFile == 'pdf') {
		var kode_set = 2;
	}else if (_extFile == 'zip'){
		var kode_set = 3;
	}else{
		var kode_set = 4;
	}

	$.ajax({
			url : baseurl+"order_produk/setDefaultThumbnail",
			type: 'post',
			data: { kode_set: kode_set, nama_pekerjaan:nama_pekerjaan, no_transaksi:no_transaksi },
			success:function(msg){
				if (formId==0) {
					save();
				}else{
					save_edit();
				}
			}
		})
}

function save(){
	setDefaultJam();
	var target = $("#target_server").val();
	if ( parseInt(target) == 0) {
		var $el = $('#gambar');
		$el.wrap('<form>').closest('form').get(0).reset();
		$el.unwrap();
	}
	var dtForm = new FormData($("#frm_order_produk")[0]);
	$.ajax({
		url : baseurl+"order_produk/upload_produk",
		type: 'post',
		dataType: 'json',
		data: dtForm,
		processData: false,
		contentType: false,
		success : function(msg){
			alertify.success(msg['msg']);
			var alamat_server = $("#alamat_server").val();
			$("table#tdet_order tbody tr").remove();
			if($("#id_konsumen").val() == ""){
				setTimeout(function() {
					location.reload();
				}, 1000);
			}else{
				$.each(msg['data'], function(i,n){
					var $tr_order = $("<tr>"
									+"<td></td>"
									+"<td><input type='hidden' name='id_detail_produk_order[]' value='"+n['id_detail_produk_order']+"'><img src='"+ baseurl+n['thumbnail']  +"' width='65'></td>"
									+"<td>"

										+"<p style='font-weight: bold; margin-bottom: 0px;'>"
											+n['nama_pekerjaan'].toUpperCase()

											+"<a href='#'' onclick='remove_order(this)'><i class='fa fa-times-circle text-red'></i></a>"

											+"<a id='edit_tgl' onclick='view_order(this, event)' data-id='"+n['id_detail_produk_order']+"' href='#'' data-toggle='lightbox-view'> <i class='fa fa-folder-open'></i></a>"
										+"</p>"
										+"<p style='margin-bottom: 0px;'>"
											+n['nm_produk'].toUpperCase()
										+"</p>"
										+"<p style='margin-bottom: 0px;'>"
											+ (n['st_finishing']==0 ? 'Fiishing Standar':'Finishing Custom')
										+"</p>"
									// +n['nama_pekerjaan']+"- <a href='#' onclick='remove_order(this)'><i class='fa fa-remove text-red'></i> hapus</a>
									+"</td>"
									+"<td>"+ n['detail_gambar']
										+"<br/><i> P: "+ n['p']+ n['nm_satuan']+ " x L: "+n['l']+ n['nm_satuan']+"<i/><br/>"+ (n['st_dimensi'] == 1 ? "<span class='label label-danger'>Custom Size</span>": "<span class='label label-success'>Original Size</span>")
									+"</td>"
									+"<td>"+(n['st_urgent']==1 ? '<span class="label label-danger">Urgent</span>' : '<span class="label label-success">Non Urgent</span>')+"</td>"
									+"<td>"+$.number(n['jml_cetak'])+"</td>"
									+"<td>"+n['harga_by']+" "+n['satuan_besar']+"@"+$.number(n['harga_cetak_persatuan_terkecil']) +"</td>"
									+"<td align='right'>"+$.number( parseFloat(n['biaya_cetak']) + parseFloat(n['kenaikan_value']))+"</td>"
									+"<td align='right'>"+$.number(n['biaya_design'])+"</td>"
									+"<td align='right'>"+$.number(n['biaya_finishing'])+"</td>"
									+"<input type='hidden' name='sub_total[]' value='"+n['sub_total']+"'/>"
									+"<td align='right'>"+$.number(n['sub_total'])+"</td>"
								+"</tr>");
					$("#id_order").val(n['id_order']);
					$("#tdet_order tbody").append($tr_order);
					hitung_total_order();
				});
			}
			reset_form();
			buat_no();
		},
		error:function(error){
			console.log(JSON.stringify(error));
		}
	});
}



function save_edit(){
	setDefaultJam();
	var target = $("#target_server").val();
	if ( parseInt(target) == 0) {
		var $el = $('#gambar');
		$el.wrap('<form>').closest('form').get(0).reset();
		$el.unwrap();
	}
	var dtForm = new FormData($("#frm_order_produk")[0]);
	$.ajax({
		url : baseurl+"order_produk/upload_produk_edit",
		type: 'post',
		dataType: 'json',
		data: dtForm,
		processData: false,
		contentType: false,
		success : function(msg){
			alertify.success(msg['msg']);
			var target = $("#target_server").val();
			var alamat_server = $("#alamat_server").val();
			$("table#tdet_order tbody tr").remove();
			$.each(msg['data'], function(i,n){
					if (n['st_acc_produksi']==0) {
						var tampil_del = "<a href='#'' onclick='remove_order(this)'><i class='fa fa-times-circle text-red'></i></a>"
					}else{
						var tampil_del =""
					}
					var $tr_order = $("<tr>"
									+"<td></td>"
									+"<td><input type='hidden' name='id_detail_produk_order[]' value='"+n['id_detail_produk_order']+"'><img src='"+ baseurl+n['thumbnail'] +"' width='65'></td>"
									+"<td>"

										+"<p style='font-weight: bold; margin-bottom: 0px;'>"
											+n['nama_pekerjaan'].toUpperCase()
	                                        +"<a id='edit_tgl' onclick='view_order(this, event)' data-id='"+n['id_detail_produk_order']+"' href='#'' data-toggle='lightbox-view'> <i class='fa fa-folder-open'></i></a>"
                                		+"</p>"
	                                    +"<p style='margin-bottom: 0px;'>"
	                                        +n['nm_produk'].toUpperCase()
	                                    +tampil_del
	                                    +"</p>"
	                                    +"<p style='margin-bottom: 0px;'>"
	                                        + (n['st_finishing']==0 ? 'Fiishing Standar':'Finishing Custom')
	                                    +"</p>"
									// +n['nama_pekerjaan']+"- <a href='#' onclick='remove_order(this)'><i class='fa fa-remove text-red'></i> hapus</a>
									+"</td>"
									+"<td>"+ n['detail_gambar']
										+"<br/><i> P: "+ n['p']+ n['nm_satuan']+ " x L: "+n['l']+ n['nm_satuan']+"<i/><br/>"+ (n['st_dimensi'] == 1 ? "<span class='label label-danger'>Custom Size</span>": "<span class='label label-success'>Original Size</span>")
									+"</td>"
									+"<td>"+(n['st_urgent']==1 ? '<span class="label label-danger">Urgent</span>' : '<span class="label label-success">Non Urgent</span>')+"</td>"
									+"<td>"+$.number(n['jml_cetak'])+"</td>"
									+"<td>"+n['harga_by']+" "+n['satuan_besar']+"@"+$.number(n['harga_cetak_persatuan_terkecil']) +"</td>"
									+"<td align='right'>"+$.number( parseFloat(n['biaya_cetak']) + parseFloat(n['kenaikan_value']))+"</td>"
									+"<td align='right'>"+$.number(n['biaya_design'])+"</td>"
									+"<td align='right'>"+$.number(n['biaya_finishing'])+"</td>"
									+"<input type='hidden' name='sub_total[]' value='"+n['sub_total']+"'/>"
									+"<td align='right'>"+$.number(n['sub_total'])+"</td>"
								+"</tr>");
					$("#id_order").val(n['id_order']);
					$("#tdet_order tbody").append($tr_order);
					hitung_total_order();
				});
			reset_form();
			buat_no();
		},
		error:function(error){
			console.log(JSON.stringify(error));
		}
	});
}

//Remove Item
function remove_item(obj){
	$(obj).closest("tr").animate({backgroundColor:'red'}, 1000).fadeOut(1000,function() {
	    $(obj).closest("tr").remove();
	    //Hitung total
	    hitung_total_finishing();
	});
}

//Remove Item
function remove_order(obj){
	var formId = $("#formId").val();
	var result = confirm("Yakin, Order akan dihapus?");
	if (result) {
		var id = $(obj).closest("tr").find("input[name='id_detail_produk_order[]']").val();
		if (id) {
			$.ajax({
				url : baseurl+"order_produk/"+(formId == 0 ? "del_detail_order" : "del_detail_order_edit"),
				type: 'post',
				dataType: 'json',
				data:{id:id},
				success : function(msg){
					if (msg['type']=='success') {
						alertify.success(msg['msg']);
						$(obj).closest("tr").animate({backgroundColor:'red'}, 1000).fadeOut(1000,function() {
							    $(obj).closest("tr").remove();
						});
						$("#tdet_order tbody tr").remove();
						$.each(msg['data'], function(i,n){
							var $tr_order = $("<tr>"
											+"<td></td>"
											+"<td><input type='hidden' name='id_detail_produk_order[]' value='"+n['id_detail_produk_order']+"'><img src='"+ baseurl+n['thumbnail']  +"' width='65'></td>"
											+"<td>"

												+"<p style='font-weight: bold; margin-bottom: 0px;'>"
													+n['nama_pekerjaan'].toUpperCase()

		                                    		+"<a href='#'' onclick='remove_order(this)'><i class='fa fa-times-circle text-red'></i></a>"

			                                        +"<a id='edit_tgl' onclick='view_order(this, event)' data-id='"+n['id_detail_produk_order']+"' href='#'' data-toggle='lightbox-view'> <i class='fa fa-folder-open'></i></a>"
		                                		+"</p>"
			                                    +"<p style='margin-bottom: 0px;'>"
			                                        +n['nm_produk'].toUpperCase()
			                                    +"</p>"
			                                    +"<p style='margin-bottom: 0px;'>"
			                                        + (n['st_finishing']==0 ? 'Fiishing Standar':'Finishing Custom')
			                                    +"</p>"
											// +n['nama_pekerjaan']+"- <a href='#' onclick='remove_order(this)'><i class='fa fa-remove text-red'></i> hapus</a>
											+"</td>"
											+"<td>"+ n['detail_gambar']
												+"<br/><i> P: "+ n['p']+ n['nm_satuan']+ " x L: "+n['l']+ n['nm_satuan']+"<i/><br/>"+ (n['st_dimensi'] == 1 ? "<span class='label label-danger'>Custom Size</span>": "<span class='label label-success'>Original Size</span>")
											+"</td>"
											+"<td>"+(n['st_urgent']==1 ? '<span class="label label-danger">Urgent</span>' : '<span class="label label-success">Non Urgent</span>')+"</td>"
											+"<td>"+$.number(n['jml_cetak'])+"</td>"
											+"<td>"+n['harga_by']+" "+n['satuan_besar']+"@"+$.number(n['harga_cetak_persatuan_terkecil']) +"</td>"
											+"<td align='right'>"+$.number( parseFloat(n['biaya_cetak']) + parseFloat(n['kenaikan_value']))+"</td>"
											+"<td align='right'>"+$.number(n['biaya_design'])+"</td>"
											+"<td align='right'>"+$.number(n['biaya_finishing'])+"</td>"
											+"<input type='hidden' name='sub_total[]' value='"+n['sub_total']+"'/>"
											+"<td align='right'>"+$.number(n['sub_total'])+"</td>"
										+"</tr>");
							$("#id_order").val(n['id_order']);
							$("#tdet_order tbody").append($tr_order);
							hitung_total_order();
							buat_no();
						});
					}else{
						alertify.error(msg['msg']);
					}
				},
				error:function(error){
					console.log(JSON.stringify(error));
				}
			});
		}
	}
}
//hitung gtotal
function hitung_total_finishing(){
	var pj_table = $("#tdet_finishing tbody tr").length;

	var gtotal = 0;
	for (var i = 1; i <= pj_table; i++) {
		var sub_total=0;
		var sub = parseFloat($("#tdet_finishing tr:eq("+i+")").find("input[name='list_harga_finishing[]']").val());
		if (sub >0) {
			sub_total=parseFloat($("#tdet_finishing tr:eq("+i+")").find("input[name='list_harga_finishing[]']").val().replace(/[","]/g,""));
		}
		gtotal += sub_total;
	}
	$("td.gtotal_finishing").empty();
	$("td.gtotal_finishing").append($.number(gtotal));
}
//hitung gtotal
function hitung_total_order(){
	var pj_table = $("#tdet_order tbody tr").length;

	var gtotal = 0;
	for (var i = 1; i <= pj_table; i++) {
		var sub_total=0;
		var sub = parseFloat($("#tdet_order tr:eq("+i+")").find("input[name='sub_total[]']").val());
		if (sub >0) {
			sub_total=parseFloat($("#tdet_order tr:eq("+i+")").find("input[name='sub_total[]']").val().replace(/[","]/g,""));
		}
		gtotal += sub_total;
	}
	$("td.gtotal_order").empty();
	$("td.gtotal_order").append($.number(gtotal));
}
function view_order(obj,e){
	var formId = $("#formId").val();
    e.preventDefault();
    var id = $(obj).data('id');
    var src_img='';
    if (id !="") {
        $.ajax({
            url : baseurl+"order_produk/"+ (formId== 1? "view_detail_order_edit":"view_detail_order"),
            type : "post",
            dataType : "json",
            data : {id : id},
            success : function(msg){
            	if (msg['type']=='success') {
        			src_img = baseurl+msg['data']['thumbnail'];
            		$("#view_nama_pekerjaan").text(msg['data']['nama_pekerjaan']);
            		$("#view_id_kategori_produk").text(msg['data']['nmkategori']);
            		$("#view_id_produk").text(msg['data']['nm_produk']);
            		$("#view_img").attr('src', src_img);
            		$("#view_st_dimensi").text(msg['data']['st_dimensi']);
            		$("#view_p").text(msg['data']['p']+"x"+msg['data']['l']+" "+msg['data']['satuan_panjang']);
            		$("#view_jml_order").text( $.number(msg['data']['jml_cetak'])+" "+msg['data']['satuan_order']);
            		$("#view_biaya_cetak").text($.number( parseFloat(msg['data']['biaya_cetak']) + parseFloat(msg['data']['kenaikan_value']) ));
            		$("#view_biaya_desain").text($.number(msg['data']['biaya_design']));
            		$("#view_note_produk").text(msg['data']['catatan']);
            		$("#view_st_finishing").text((msg['data']['st_finishing']==0)?'Finishing Standart':'Finishing Custom');

            		if (msg['detail']!='') {
            			$("table#view_tdet_finishing tbody tr").remove();
            				var no=0;
            				var total =0;
							$.each(msg['detail'], function(i,n){
								var $tr = $("<tr>"
												+"<td>"+ (no+=1) +"</td>"
												+"<td>"+n['nm_produk']+"</td>"
												+"<td>"+n['catatan']+"</td>"
												+"<td>"+n['jml']+" Set </td>"
												+"<td>"+$.number(n['harga'])+"<td>"
											+"</tr>");
								$("table#view_tdet_finishing tbody").append($tr);
								total += n['harga'];
							});
						$("#view_gtotal_finishing").append($.number(total));
						$("#detail-hide").show();
            		}else{
            			$("#detail-hide").hide();
            		}
            		$("#lightbox-view").modal('show');  
            	}
                
            },
            error : function(err){
                alert(JSON.stringify(err));
            }
        });
    }
}
//buat nomor
function buat_no_finishing(){
	var pj_table = $("#tdet_finishing tbody tr").length;
	for (var i = 1; i < pj_table; i++) {
		$("#tdet_finishing tr:eq("+i+") td:first").text(i);
	}
}

function buat_no(){
	var pj_table = $("#tdet_order tbody tr").length;
	for (var i = 1; i <= pj_table; i++) {
		$("#tdet_order tr:eq("+i+") td:first").text(i);
	}
}
// buat nomer tabel pdf
function buat_no_table_pdf(){
	var pj_table = $("#table-pdf tbody tr").length;
	for (var i = 1; i <= pj_table; i++) {
		$("#table-pdf tr:eq("+i+") td:first").text(i);
	}
}

function clear_input_finishing(){
	$("#id_finishing").val("").trigger("change");
	$("#jml_finishing").val("");
	$("#note_finishing").val("");
	$("#harga_finishing").val("");
}
function clear_produk(){
	$("#nama_pekerjaan").val("");
	$("#st_dimensi").prop("checked", false);
	$("#gambar").val("");
	$("#preview-gambar").hide();
	$("#hide_p").val("");
	$("#hide_l").val("");
	$("#p").val("");
	$("#l").val("");
	$("#jml_order").val("");
	$("#biaya_cetak1").val("");
	$("#biaya_desain").val("");
	$("#biaya_finishing").val("");
	$("#note_produk").val("");
	$("#st_finishing").val("0").trigger("change");
	$("#view_detail_pdf").hide();
	if(parseInt($("#id_kategori").val()) === 1 ){
		$(".ukuran-cetak").hide();
		$(".custom-check").hide();
		$("#id_konversi").val(11).trigger("change");
		$("#p").val(42);
		$("#l").val(29.7);
	}else{
		$(".ukuran-cetak").show();
		// $(".custom-check").show();
	}
	checkAutomeasuring();
}
function clear_table_finishing(){
	$("#tdet_finishing tbody tr").remove();
}
function reset_form(){

	clear_produk();
	clear_input_finishing();
	clear_table_finishing();
}

// tgl selesai
function cek_urgent(){
	if ($("#st_urgent").prop("checked") == true){
		$(".d-tgl-selesai").show();
		if ($("#tgl_selesai").value.length>0 && $("#jml_order").val()>0) {
			get_biaya_cetak();
		}
	}else{
		// $("#tgl_selesai").val("");
		$("#kenaikan_persen").val("");
		$("#kenaikan_value").val("");
		$(".d-tgl-selesai").hide();
	}
}
// cek ukuran gambar dengan kategori
function cek_ukuran_gambar(){

	var st_ukuran 	= $("#st_ukuran").val();
	var p_kategori 	= $("#p_kategori").val();
	var l_kategori 	= $("#l_kategori").val();
	var p_gambar 	= $("#hide_p").val();
	var l_gambar	= $("#hide_l").val();
	if ($("#st_dimensi").prop('checked') == false) {
		if (st_ukuran == 1) {
			if (p_kategori != p_gambar || l_kategori != l_gambar) {
				alertify.confirm('Warning!!!', 'Ukuran Gambar tidak sesuai dengan kategori yang dipilih! Tetap Gunakan?',function(){}
                , function(){ 
                	$("#preview-gambar").hide();
                	$("#gambar").val('');
                	$("#hide_p").val('');
					$("#hide_l").val('');
					$("#id_konversi").val('').trigger("change");
					$("#l").val('');
					$("#p").val('');
                	alertify.error('Cancel')});
			}
		}
	}	
}
// cek minal order
function cek_minimal_order(){
	var st_minimal_order 	= $("#st_minimal_order").val();
	var jml_minimal_order 	= $("#jml_minimal_order").val();
	var lebar_minimal 		= $("#lebar_minimal").val();
	var panjang_minimal 	= $("#panjang_minimal").val();
	var st_tipe 			= $("#st_tipe").val();

	var p_gambar			= $("#hide_p").val();
	var l_gambar 			= $("#hide_l").val();
	var jml_order 			= $("#jml_order") * $("id_konversi").find('option:selected').data('jml_besar');
	if (st_minimal_order == 1) {
		if (st_tipe == 0) {
			if (jml_order< jml_minimal_order) {
				alertify.alert('Jumlah order kurang '+ jml_minimal_order + ' PCS');
				return false;
			}
		}else{
			if (parseFloat(p_gambar) < parseFloat(panjang_minimal) || parseFloat(l_gambar) < parseFloat(lebar_minimal)) {
				alertify.alert('Panjang minimal order'+ panjang_minimal + ' MM dan lebar minimal order ' + lebar_minimal + ' MM');	
				_cek_min = false;
				return false;
			}else{
				_cek_min = true;
			}
		}
	}else{
		_cek_min = true;
	}
}
function get_tlp(){
	var id = $("#id_konsumen").val();
	if (id.length>0) {
		$.ajax({
			url : baseurl+"order_produk/get_tlp",
			type: 'post',
			dataType: 'json',
			data: {id:id},
			success : function(msg){
				$("#no_tlp").val(msg);
			},
			error:function(error){
				console.log(JSON.stringify(error));
			}
		});
	}
}
function diff_minutes (d1, d2) {
var m1 = d1.getMinutes();
var m2 = d2.getMinutes();
return m2 - m1;

}

function selisih_jam(){
	var d = new Date();
	var timeOfCall = formatDate(d),
        timeOfResponse = $('#jam_selesai').val(),
        hours = timeOfResponse.split(':')[0] - timeOfCall.split(':')[0],
        minutes = timeOfResponse.split(':')[1] - timeOfCall.split(':')[1];
     alert(timeOfCall);
    
    minutes = minutes.toString().length<2?'0'+minutes:minutes;
    if(minutes<0){ 
        hours--;
        minutes = 60 + minutes;
    }
    hours = hours.toString().length<2?'0'+hours:hours;



    alert(hours + ':' + minutes);
    // $('#delay').val(hours + ':' + minutes);
}
function formatDate(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  // var strTime = hours + ':' + minutes + ' ' + ampm;
  var strTime = hours + ':' + minutes;
  // return date.getMonth()+1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
  return strTime;
}

// addd page pdf
function addPage(){
	var page 	= $("#page").val();
	var jumlah  = $("#jml_cetak_page").val();
	var detail_page = page.split('-');
	var isi ="";
	if (detail_page.length > 0 && detail_page.length <=2) {
		if (detail_page.length == 2) {
			for (var i = parseInt(detail_page[0]); i <= parseInt(detail_page[1]); i++) 
			{
				isi += "<input type='hidden' name='item_page[]' value='"+i+"' />"
				+"<input type='hidden' name='item_jml_cetak[]' value='"+jumlah+"' />";
			}
		}else{
			isi += "<input type='hidden' name='item_page[]' value='"+page+"' />"
				+"<input type='hidden' name='item_jml_cetak[]' value='"+jumlah+"' />";
		}
	}
		if (page !=0 && jumlah >0) {
			var $row = $("<tr>"
		  					+"<td></td>"
		  					+"<td>"
		  						+isi
		  						+ page
		  					+"- <a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>"
		  					+"<td>"
		  						+ jumlah
		  					+"</td>"
		  				+"</tr>");
			$("#table-pdf tbody").append($row);
			buat_no_table_pdf();
			$("#page").val("");
			$("#jml_cetak_page").val("");
		}else{
			toastr.clear();
	        Command: toastr['error']('Isikan halaman dan jumlah cetak dahulu', 'Error !');
	        return false;	
		}
}

function addPagePdfList(){
	var page = $("input[name='item_page[]']").map(function(){return $(this).val();}).get();
	// var page 	= $(input[name='item_page[]']).val();
	var jumlah  = $("input[name='item_jml_cetak[]']").map(function(){return $(this).val();}).get();
	var $row =""
	var total_page =0;
	if(page.length > 0){
		var jml_cetak_bom = $("#jml_cetak_bom").val();

		if (jml_cetak_bom > 1) {
			var validasi_jml = Math.round(parseInt(__TOTAL_PAGES)/jml_cetak_bom);
		}else{
			var validasi_jml = parseInt(__TOTAL_PAGES);
		}

		if (validasi_jml == $('input[name="item_page[]"]').length) {
			$("#tabel-detail-pdf tbody tr").remove();
			$.each(page, function(i,n){
				total_page += parseInt(jumlah[i]);

				$row = $("<tr>"
							+"<td>"
								+"<input type='hidden' name='item_page_pdf[]' value='"+n+"' />"
							+"</td>"
							+"<td>"
								+"<input type='hidden' name='item_jml_cetak_pdf[]' value='"+jumlah[i]+"' />"
							+"</td>"
						+"</tr>");
				$("#tabel-detail-pdf tbody").append($row);
			});
			// $("#st_dimensi").trigger("click");
			$("#id_konversi").val(11).trigger("change");
			$("#p").val(42);
			$("#l").val(29.7);
			$("#jml_order").val(total_page).trigger("focusout");
			$("#pdf-modal").modal('hide');
			$("#view_detail_pdf").show();
		}else{
			toastr.clear();
	        Command: toastr['error']('Jumlah Halaman yang diinput tidak sama dengan halaman file yang dipilih', 'Error !');
	        return false;		
		}
	}else{
		toastr.clear();
        Command: toastr['error']('isi jumlah cetak halman PDF dahulu', 'Error !');
        return false;	
	}
}
var __PDF_DOC,
	__CURRENT_PAGE,
	__TOTAL_PAGES,
	__PAGE_RENDERING_IN_PROGRESS = 0;
function countPdfPage(pdf_url) {
	$("#pdf-loader").show();
	var jml_cetak_bom = $("#jml_cetak_bom").val();

	PDFJS.getDocument({ url: pdf_url }).then(function(pdf_doc) {
		__PDF_DOC = pdf_doc;
		__TOTAL_PAGES = __PDF_DOC.numPages;
		if (jml_cetak_bom > 1) {
			$("#halaman-pdf").text(Math.round((__TOTAL_PAGES/jml_cetak_bom)) + " Halaman Cetak");	
		}else{
			$("#halaman-pdf").text(__TOTAL_PAGES + " Halaman Cetak");
		}
	});
}

function handleFiles(e) {
	var canvas = document.getElementById('preview');
    var ctx = canvas.getContext('2d');
    var reader  = new FileReader();
    var file = e.files[0];
 
    // load to image to get it's width/height
    var img = new Image();
    img.onload = function() {
        // scale canvas to image
        var scale = 100/img.height;
   
        var width = scale * img.width;
        var height = scale * img.height;
        canvas.width = width;
        canvas.height = height;

        // draw image
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        ctx.drawImage(img, 0, 0
            , width, height
        );
    }
    // this is to setup loading the image
    reader.onloadend = function () {
        img.src = reader.result;
    }

    // this is to read the file
   	reader.readAsDataURL(file);
}


// menampilkan detail pdf yang telah diinputkan
function showDetailPdf(){

	var page = $("input[name='item_page_pdf[]']").map(function(){return $(this).val();}).get();
	// var page 	= $(input[name='item_page[]']).val();
	var jumlah  = $("input[name='item_jml_cetak_pdf[]']").map(function(){return $(this).val();}).get();
	if(page.length > 0){
		$("#table-pdf tbody tr").remove();
		var $row =""
		$.each(page, function(i,n){
			var $row = $("<tr>"
		  					+"<td></td>"
		  					+"<td>"
		  						+"<input type='hidden' name='item_page[]' value='"+n+"' />"
		  						+n
		  					+"- <a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>"
		  					+"<td>"
		  						+"<input type='hidden' name='item_jml_cetak[]' value='"+jumlah[i]+"' />"
		  						+ jumlah[i]
		  					+"</td>"
		  				+"</tr>");
			$("#table-pdf tbody").append($row);
		});
		buat_no_table_pdf();
		// $("#st_dimensi").trigger("click");
		$("#pdf-modal").modal('show');
	}else{
		toastr.clear();
        Command: toastr['error']('List PDF kosong', 'Error !');
        return false;	
	}
}

function formatDesign(item) {
	var selectionText = item.text.split(",");
	var $returnString = $('<span><strong>'+selectionText[0] + '</strong></br>' + selectionText[1] + '</span>');
	return $returnString;
};

function previewLampiran(obj) {
	if (obj.files && obj.files[0]) {
		const ext = $(obj).val().split('.').pop().toLowerCase();
		console.log('extention lampiran', ext);
		if(ext === 'jpg' || ext === 'jpeg'){
			const reader = new FileReader();
			reader.onload = function(e) {
				$(".preview-lampiran").attr("src", e.target.result);
			}
	
			reader.readAsDataURL(obj.files[0]);
		}else{
			$(".preview-lampiran").attr("src", baseurl+'assets/images/default-pdf.png');
		}
	}else{
		$(".preview-lampiran").attr("src", baseurl+'assets/images/empty-image.webp');
	}
}
