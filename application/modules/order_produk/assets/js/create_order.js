$(document).ready(function(){
	cek_lampiran();
	cekUseImage();
	cekUploadImage();
	$("#lampiran").change(function(){
    	cek_lampiran();
    });
    $('#jam_selesai').mdtimepicker({theme: 'green', format: 'hh:mm'});
})
function cek_lampiran(){
	if( document.getElementById("lampiran").files.length == 0 ){
	    $("#loadLampiran").removeClass( "btn-info" ).addClass( "btn-danger" );
	}else{
		$("#loadLampiran").removeClass( "btn-danger" ).addClass( "btn-success" );
	}
}

function cekUseImage(){
	const useImage = $("#use-image-cnf").val();
	console.log('useimage', useImage);
}

function cekUploadImage(){
	const uploadIage = $("#upload-image-cnf").val();
	console.log('upload image', cekUploadImage);
}

function new_konsumen_show(){
	$("#nama_konsumen_baru").show();
	$("#d-select-konsumen").hide();
	$("#nama_konsumen_baru").focus();
}