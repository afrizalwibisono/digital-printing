<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 * 
 * This is controller for order_produk

 */

class Order_produk extends Admin_Controller
{

    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Order Produk.View";
    protected $addPermission    = "Order Produk.Add";
    protected $managePermission = "Order Produk.Manage";
    protected $deletePermission = "Order Produk.Delete";
    protected $uploadFolder = "./upload";
    protected $prefixKey        = "TRX";
    // use_image 1: menggunakan image untuk create Thumbnail 0: tidak menggunakan image ;
    // upload_file 1: file diupload 0: file tidak diupload
    // auto_image_measuring 1: auto 0: tidak menghitung image
    protected $akumulasi = true;
    protected $default_jam = 1;
    protected $use_image = 1;
    protected $upload_file = 1;
    protected $auto_image_measuring = 1;

    public function __construct()
    {
        parent::__construct();
        $this->auth->restrict($this->viewPermission);

        $this->lang->load('order_produk');
        $this->load->model(array(
            'order_produk_model',
            'produk_model',
            'konsumen/konsumen_model',
            'order_produk_detail_model',
            'order_produk_detail_finishing_model',
            'order_produk_detail_a3_model',
            'morder_produk_detail_model',
            'morder_produk_detail_finishing_model',
            'morder_produk_detail_a3_model',
            'kategori_model',
            'konversi_satuan/konversi_satuan_model',
            'setting_harga/setting_harga_model',
            'setting_harga/setting_harga_detail_model',
            'setting_promo/setting_promo_model',
            'setting_upload/setting_upload_model',
            'Setting_harga_urgent_model',
            'spk/spk_model',
        ));
        $this->template->title(lang('order_produk_title_manage'));
        $this->template->page_icon('fa fa-cart-plus');

        if (app_get_setting("app.akumulasi_harga") == 1) {
            $this->akumulasi = false;
        }
        $this->default_jam = app_get_setting("app.default_jam");
        $this->use_image = app_get_setting("app.use_image");
        $this->auto_image_measuring = app_get_setting("app.auto_image_measuring");
        $this->upload_file = app_get_setting("app.upload_file");
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if (isset($_POST['delete']) && has_permission($this->deletePermission)) {
            $checked = $this->input->post('checked');

            if (is_array($checked) && count($checked)) {
                $result = FALSE;
                $ero_msg = [];
                foreach ($checked as $pid) {

                    $data = $this->order_produk_model->find($pid);

                    $id_tran        = str_replace(["/", "-"], '_', $data->no_transaksi);
                    $tmp_folder  = explode("_", $id_tran);
                    $th_bl_hr    = "";
                    if ($tmp_folder) {
                        $th   = $tmp_folder[1]; // Tahun 4 digit
                        $blhr = $tmp_folder[2];
                        $bl   = substr($blhr, 0, 2); // 2 digit left / bulan
                        $hr   = substr($blhr, -2); // 2 digit right / hari

                        $th_bl_hr = $th . "/" . $bl . "/" . $hr;
                    }
                    $target_folder  = $this->uploadFolder;
                    $target_folder .= $th_bl_hr ? "/" . $th_bl_hr : "";
                    $target_folder .= "/" . $id_tran;
                    $dir = $target_folder;
                    // $dir = 'upload/'.str_replace(["/","-"], '_',$data->no_transaksi);
                    if ($data->st_nota == 0) {
                        if ($data->st_ctk_struk_tmp == 1) {
                            if ($data->st_acc_delete == 1) {
                                if ($this->delTree($dir)) {
                                    $result = $this->order_produk_model->delete($pid);
                                }
                            } else {
                                $ero_msg[] = sprintf(lang('order_produk_cannot_delete'), $data->no_transaksi);
                            }
                        } else {
                            if ($this->delTree($dir)) {
                                $result = $this->order_produk_model->delete($pid);
                            }
                        }
                    } else {
                        $ero_msg[] = sprintf(lang('order_produk_cannot_delete'), $data->no_transaksi);
                    }
                }

                if ($ero_msg) {
                    $keterangan = "SUKSES, hapus data setting promo dengan id : " . $pid;
                    $status     = 1;
                } else {
                    $keterangan = "GAGAL, hapus data setting promo dengan id : " . $pid;
                    $status     = 0;
                }

                $nm_hak_akses   = $this->deletePermission;
                $kode_universal = $pid;
                $jumlah         = 1;
                $sql            = $this->db->last_query();

                simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                if (count($ero_msg) == 0) {
                    $this->template->set_message(count($checked) . ' ' . lang('order_produk_deleted') . '.', 'success');
                } else {
                    $ero_msg = implode("<br>", $ero_msg);
                    $this->template->set_message($ero_msg . $this->order_produk_model->error, 'error');
                }
            } else {
                $this->template->set_message(lang('order_produk_del_error') . $this->order_produk_model->error, 'error');
            }
        } //end if

        // Pagination
        $this->load->library('pagination');

        if (isset($_POST['table_search'])) {
            $search     = isset($_POST['table_search']) ? $this->input->post('table_search') : '';
            $tgl_awal   = isset($_POST['tgl_awal']) ? $this->input->post('tgl_awal') : '';
            $tgl_akhir  = isset($_POST['tgl_awal']) ? $this->input->post('tgl_akhir') : '';
            $id_customer = isset($_POST['id_customer']) ? $this->input->post('id_customer') : '';
        } else {
            $search     = isset($_GET['search']) ? $this->input->get('search') : '';
            $tgl_awal   = isset($_GET['tgl_awal']) ? $this->input->get('tgl_awal') : '';
            $tgl_akhir  = isset($_GET['tgl_awal']) ? $this->input->get('tgl_akhir') : '';
            $id_customer = isset($_GET['id_customer']) ? $this->input->get('id_customer') : '';
        }

        $filter = "?search=" . $search;
        $addWhere = "";
        if ($tgl_awal != '' && $tgl_akhir != '') {
            $tgl_awal = date_format(date_create($tgl_awal), "Y-m-d");
            $tgl_akhir = date_format(date_create($tgl_akhir), "Y-m-d");
            $filter   .= "&tgl_awal=" . $tgl_awal . "&tgl_akhir=" . $tgl_akhir;
            $addWhere .= " AND ( date(tgl_order) >='" . $tgl_awal . "' AND date(tgl_order) <='" . $tgl_akhir . "')";
        } else {
            $tgl_awal = date('Y-m-d');
            $tgl_akhir = date('Y-m-d');

            $filter   .= "&tgl_awal=" . $tgl_awal . "&tgl_akhir=" . $tgl_akhir;
            $addWhere .= " AND ( date(tgl_order) >='" . $tgl_awal . "' AND date(tgl_order) <='" . $tgl_akhir . "')";
        }
        $search2 = $this->db->escape_str($search);

        $where = "order_produk.deleted = 0 
                AND st_simpan = 1
                $addWhere
                AND (`nama` LIKE '%$search2%' ESCAPE '!'
                OR `no_transaksi` LIKE '%$search2%' ESCAPE '!')";

        $total = $this->order_produk_model
            ->join("konsumen", "konsumen.idkonsumen=order_produk.id_konsumen", "left")
            ->where($where)
            ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url() . $filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $assets = array(
            'plugins/select2/dist/js/select2.js',
            'plugins/select2/dist/css/select2.css',
            'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
            'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
            'plugins/datetimepicker/bootstrap-datetimepicker.css',
            'plugins/datetimepicker/bootstrap-datetimepicker.js',
            'order_produk/assets/js/index_order.js',
            'plugins/number/jquery.number.js'
        );

        add_assets($assets);

        $data = $this->order_produk_model
            ->join("konsumen", "konsumen.idkonsumen=order_produk.id_konsumen", "left")
            ->where($where)
            ->order_by('tgl_order', 'DESC')
            ->order_by('nama', 'ASC')
            ->limit($limit, $offset)->find_all();

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('tgl_awal', date_format(date_create($tgl_awal), "d-m-Y"));
        $this->template->set('tgl_akhir', date_format(date_create($tgl_akhir), "d-m-Y"));
        $this->template->set("toolbar_title", lang('order_produk_title_manage'));
        $this->template->title(lang('order_produk_title_manage'));
        $this->template->set("numb", $offset + 1);
        $this->template->render('index');
    }

    public function get_konsumen()
    {
        if (!$this->input->is_ajax_request()) {
            $this->template->set_message(lang('order_produk_only_ajax'), 'error');
            redirect('order_produk');
        }
        $search = $this->input->post('search');
        if ($search !== '') {
            $data = $this->konsumen_model
                ->where("deleted", 0)
                ->like("nama", $search)
                ->or_like("wa", $search)
                ->or_like("telp", $search)
                ->order_by("nama", "asc")
                ->limit(10)
                ->find_all();
        } else {
            $data = $this->konsumen_model
                ->order_by('nama', 'ASC')
                ->limit(10)
                ->find_all_by(['deleted' => 0]);
        }

        $list = [];
        if ($data) {
            foreach ($data as $key => $value) {
                $list[$key]['id'] = $value->idkonsumen;
                $list[$key]['text'] = $value->panggilan . " " . $value->nama . ',' . $value->telp;
                $list[$key]['tipe'] = $value->st;
            }
            echo json_encode($list);
        } else {
            echo "No Result";
        }
    }

    public function delTree($dir)
    {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : @unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    public function create()
    {

        $this->auth->restrict($this->addPermission);
        if (isset($_POST['save'])) {
            if ($this->save()) {
                $this->template->set_message(lang("order_produk_create_success"), 'success');
                // redirect('order_produk');
                redirect(current_url());
            }
        }
        $data = $this->session->userdata('order_produk');
        if (!$data) {
            $setting_upload = $this->setting_upload_model->find(1);
            $data = array('order_produk' => array(
                // 'target_server'  => $setting_upload->st_target_server,
                // 'alamat_server'  => $setting_upload->url,
                'id_order'    => '',
                'no_transaksi' => $this->buat_kode("TRX"),
                'id_konsumen' => isset($_POST['id_konsumen']) ? $this->input->post('id_konsumen') : '',
                'no_tlp'    => '',
                'tgl_order' => date("Y-m-d"),
                'list_order'  => array(),
                'st_po' => 0,
            ));

            $this->session->set_userdata($data);
            $data = $this->session->userdata('order_produk');
        }

        $assets = array(
            'plugins/select2/dist/js/select2.js',
            'plugins/select2/dist/css/select2.css',
            'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
            'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
            'plugins/md-time-picker/mdtimepicker.js',
            'plugins/md-time-picker/mdtimepicker.css',
            'plugins/number/jquery.number.js',
            'plugins/toastr/toastr.js',
            'plugins/toastr/toastr.css',
            'order_produk/assets/css/order.css',
            'order_produk/assets/js/order_produk.js',
            'order_produk/assets/js/create_order.js',
        );

        add_assets($assets);
        $kategori           = $this->kategori_model
            ->join("produk", "produk.idkategori=kategori.idkategori", "left")
            ->group_by("kategori.idkategori")
            ->find_all_by(['kategori.deleted' => 0, 'st_produk' => 0]);
        $konsumen           = $this->konsumen_model->find_all_by(['deleted' => 0]);
        $konversi           = get_konversi(4, '', FALSE);
        $konversi_jml_order = get_konversi(1, '', FALSE);
        $kode_trx           = $this->buat_kode("TRX");
        $setting_upload = $this->setting_upload_model->find(1);
        $default_tgl = date('d-m-Y');

        if (isset($_POST['id_konsumen'])) {
            // echo $this->input->post('id_konsumen');
            // die();
            $data['id_konsumen'] = $this->input->post('id_konsumen');
        }

        $this->template->set('default_tgl', $default_tgl);
        $this->template->set('default_jam', $this->default_jam);
        $this->template->set('use_image', $this->use_image);
        $this->template->set('upload_file', $this->upload_file);
        $this->template->set('auto_image_measuring', $this->auto_image_measuring);
        $this->template->set('data', $data);
        $this->template->set('kategori', $kategori);
        $this->template->set('no_transaksi', $kode_trx);
        $this->template->set('setting', json_encode($setting_upload));
        $this->template->set('konsumen', $konsumen);
        $this->template->set('konversi', $konversi);
        $this->template->set('setting_upload', $setting_upload);
        $this->template->set('konversi_jml_order', $konversi_jml_order);
        $this->template->title(lang('order_produk_title_new'));
        $this->template->render('form');
    }
    public function upload()
    {

        $file_name   = $this->input->post('nama_pekerjaan');
        $id_tran        = str_replace(["/", "-"], '_', $this->input->post('no_transaksi'));

        $tmp_folder  = explode("_", $id_tran);
        $th_bl_hr    = "";
        if ($tmp_folder) {
            $th   = $tmp_folder[1]; // Tahun 4 digit
            $blhr = $tmp_folder[2];
            $bl   = substr($blhr, 0, 2); // 2 digit left / bulan
            $hr   = substr($blhr, -2); // 2 digit right / hari

            $th_bl_hr = $th . "/" . $bl . "/" . $hr;
        }
        $target_folder  = $this->uploadFolder;
        $target_folder .= $th_bl_hr ? "/" . $th_bl_hr : "";
        $target_folder .= "/" . $id_tran;
        // $target_folder .= "/".$id_tran;

        $target_thumb = $target_folder . "/thumb";

        $thumb_folder   = "thumb";

        if (!is_dir($target_thumb)) {
            mkdir($target_thumb, 0777, TRUE);
        }

        if (!is_dir($target_folder)) {
            mkdir($target_folder, 0777, TRUE);
            mkdir($target_folder . '/' . $thumb_folder, 0777, TRUE);
        }

        $this->load->library('upload');

        if ($file_name != "") {
            $config['upload_path']   = $target_folder;
            $config['allowed_types'] = 'jpeg|jpg|zip|pdf';
            $config['max_size']      = 0;
            $config['file_ext_tolower']  = TRUE;
            $config['overwrite']     = TRUE;
            $config['file_name']     = str_replace([" ", "/"], '-', $file_name);

            $this->upload->initialize($config);

            if (!$this->upload->do_upload("gambar")) {
                $return = ['type' => 'error', 'msg' => $this->upload->display_errors(), 'data' => ''];
            } else {
                $return = ['type' => 'success', 'msg' => 'Upload Sukses'];
            }
            echo json_encode($return);
        }
    }
    public function uploadThumbnail()
    {

        $file_name   = $this->input->post('nama_pekerjaan');
        $new_name = explode(".", $file_name);
        $name = $new_name[0];
        $id_tran        = str_replace(["/", "-"], '_', $this->input->post('no_transaksi'));
        $img = $_POST['img'];

        $tmp_folder  = explode("_", $id_tran);
        $th_bl_hr    = "";
        if ($tmp_folder) {
            $th   = $tmp_folder[1]; // Tahun 4 digit
            $blhr = $tmp_folder[2];
            $bl   = substr($blhr, 0, 2); // 2 digit left / bulan
            $hr   = substr($blhr, -2); // 2 digit right / hari

            $th_bl_hr = $th . "/" . $bl . "/" . $hr;
        }
        $target_folder  = $this->uploadFolder;
        $target_folder .= $th_bl_hr ? "/" . $th_bl_hr : "";
        $target_folder .= "/" . $id_tran;

        $target_thumb = $target_folder . "/thumb";

        $thumb_folder   = "thumb";

        if (!is_dir($target_thumb)) {
            mkdir($target_thumb, 0777, TRUE);
        }

        if (!is_dir($target_folder)) {
            mkdir($target_folder, 0777, TRUE);
            mkdir($target_folder . '/' . $thumb_folder, 0777, TRUE);
        }


        $img = str_replace('data:image/jpeg;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $file = $target_thumb . "/" . str_replace([" ", "/"], '-', $name . '.jpg');

        if (file_put_contents($file, $data)) {
            $return = ['type' => 'success', 'msg' => 'Upload Thumbnail Sukses'];
        } else {
            $return = ['type' => 'error', 'msg' => 'Upload Thumbnail Gagal'];
        }
        return json_encode($return);
    }

    public function setDefaultThumbnail()
    {

        $file_name   = str_replace([" ", "/"], '-', $this->input->post('nama_pekerjaan'));
        $new_name = explode(".", $file_name);
        $name = $new_name[0];
        $id_tran        = str_replace(["/", "-"], '_', $this->input->post('no_transaksi'));

        // kode_set [
        //     1: DefaultThumbnail;
        //     2: DefaultPdfThumnail;
        //     3: DefaultZipThumbnail;
        // ]
        $kode_set    = $this->input->post('kode_set');

        $tmp_folder  = explode("_", $id_tran);
        $th_bl_hr    = "";
        if ($tmp_folder) {
            $th   = $tmp_folder[1]; // Tahun 4 digit
            $blhr = $tmp_folder[2];
            $bl   = substr($blhr, 0, 2); // 2 digit left / bulan
            $hr   = substr($blhr, -2); // 2 digit right / hari

            $th_bl_hr = $th . "/" . $bl . "/" . $hr;
        }
        $target_folder  = $this->uploadFolder;
        $target_folder .= $th_bl_hr ? "/" . $th_bl_hr : "";
        $target_folder .= "/" . $id_tran;

        $target_thumb = $target_folder . "/thumb";

        if (!is_dir($target_thumb)) {
            mkdir($target_thumb, 0777, TRUE);
        }

        if (!is_dir($target_folder)) {
            mkdir($target_folder, 0777, TRUE);
            mkdir($target_folder . '/' . $thumb_folder, 0777, TRUE);
        }
        if ($kode_set == 1) {
            $copy = copy($this->uploadFolder . "/default.jpg", $target_thumb . "/" . $name . ".jpg");
        } elseif ($kode_set == 2) {
            $copy = copy($this->uploadFolder . "/defaultpdf.jpg", $target_thumb . "/" . $name . ".jpg");
        } elseif ($kode_set == 3) {
            $copy = copy($this->uploadFolder . "/defaultzip.jpg", $target_thumb . "/" . $name . ".jpg");
        } else {
            $copy = copy($this->uploadFolder . "/defaulttif.jpg", $target_thumb . "/" . $name . ".jpg");
        }
        if ($copy) {
            $return = ['type' => 'success', 'msg' => 'Upload Thumbnail Sukses'];
        } else {
            $return = ['type' => 'error', 'msg' => 'Upload Thumbnail Gagal'];
        }
        return json_encode($return);
    }
    // upload new produk
    public function upload_produk()
    {
        $this->save_upload();
    }

    // upload produk edit
    public function upload_produk_edit()
    {
        $this->save_upload(1);
    }

    // type => 0: new order 2: edit order
    public function save_upload($type = 0)
    {

        $id_order       = $this->input->post('id_order');
        $file_name      = str_replace([" ", "/"], '-', $this->input->post('nama_pekerjaan'));
        // name for thumbnail
        $new_name = explode(".", $file_name);
        $name = $new_name[0];

        $id_tran        = str_replace(["/", "-"], '_', $this->input->post('no_transaksi'));

        $tmp_folder  = explode("_", str_replace(["/", "-"], '_', $this->input->post('no_transaksi')));
        $th_bl_hr    = "";
        if ($tmp_folder) {
            $th   = $tmp_folder[1]; // Tahun 4 digit
            $blhr = $tmp_folder[2];
            $bl   = substr($blhr, 0, 2); // 2 digit left / bulan
            $hr   = substr($blhr, -2); // 2 digit right / hari

            $th_bl_hr = $th . "/" . $bl . "/" . $hr;
        }
        $target_folder = $th_bl_hr ? "/" . $th_bl_hr : "";
        $target_folder .= "/" . $id_tran;

        $target_thumb = $target_folder . "/thumb/";
        $cek_save = false;
        if ($id_order == '') {
            $cek_save = true;
            $id_order = gen_primary();
        } else {
            $cek_save = false;
        }
        $id_tran        = str_replace(["/", "-"], '_', $this->input->post('no_transaksi'));
        $no_transaksi   = $this->input->post('no_transaksi');
        $no_tlp         = $this->input->post('no_tlp');
        $id_konsumen    = $this->input->post('id_konsumen');
        if (!$id_konsumen) {

            $id_konsumen = new_customer($this->input->post('nama_konsumen_baru'), $this->input->post('no_tlp'));
            $_POST['id_konsumen'] = $id_konsumen;
        }
        $tgl_order      = $this->input->post('tgl_order');
        $tgl_selesai    = date_ymd($this->input->post('tgl_selesai')) . " " . $this->input->post('jam_selesai');
        $st_po          = $this->input->post('st_po');



        $nama_pekerjaan = $this->input->post("nama_pekerjaan");
        $id_kategori    = $this->input->post("id_kategori");
        $id_produk      = $this->input->post("id_produk");
        $st_dimensi     = isset($_POST["st_dimensi"]) ? 1 : 0;
        $p              = $this->input->post("p");
        $l              = $this->input->post("l");
        $id_konversi    = $this->input->post("id_konversi");
        $detail_gambar  = $this->input->post("detail_gambar");
        $st_biaya_cetak = $this->input->post("st_biaya_cetak");
        $harga_by       = $this->input->post("harga_by");
        $jml_order      = str_replace(",", "", $this->input->post("jml_order"));
        $qty_cetak_by   = $this->input->post("qty_cetak_by");
        $satuan_cetak_by        = $this->input->post("satuan_cetak_by");
        $harga_cetak_persatuan_terkecil = $this->input->post("harga_cetak_persatuan_terkecil");
        $id_konversi_jml_order  = $this->input->post("id_konversi_jml_order");
        $biaya_cetak            = str_replace(",", "", $this->input->post("biaya_cetak"));
        $kenaikan_persen        = $this->input->post("kenaikan_persen");
        $harga_permeter         = $this->input->post("harga_permeter");
        $kenaikan_value         = $this->input->post("kenaikan_value");
        $biaya_desain           = str_replace(",", "", $this->input->post("biaya_desain"));
        $note_produk            = $this->input->post("note_produk");
        $st_finishing           = $this->input->post("st_finishing");
        $st_finishing_standart  = isset($_POST['st_finishing_standart']) ? 0 : 1;
        $st_urgent              = isset($_POST['st_urgent']) ? 1 : 0;
        $target_server          = $this->input->post("target_server");

        $item_page_pdf          = $this->input->post('item_page_pdf');
        $item_jml_cetak_pdf     = str_replace(",", "", $this->input->post('item_jml_cetak_pdf'));

        // detail finishing custome
        $id_finishing               = $this->input->post('list_id_finishing');
        $jml_finishing              = $this->input->post('list_jml_finishing');
        $id_konversi_jml_finishing  = $this->input->post('list_id_konversi_jml_finishing');
        $note_finishing             = $this->input->post('list_note_finishing');
        $harga_finishing            = str_replace(",", "", $this->input->post('list_harga_finishing'));

        $st_spk = 0;

        if ($type == 1) {
            $cek_nota = $this->order_produk_model->find($id_order);
            if ($cek_nota->st_nota == 1) {
                $st_spk = 1;
            }
        }


        $this->db->trans_start();

        if ($cek_save == true) {
            $save = $this->order_produk_model->insert(['id_order' => $id_order, 'id_konsumen' => $id_konsumen, 'tgl_order' => $tgl_order, 'no_transaksi' => $no_transaksi, 'no_tlp' => $no_tlp, 'st_po' => $st_po]);
        }
        $sub_total = 0;
        $total = 0;
        if (isset($harga_finishing) && is_array($harga_finishing)) {
            foreach ($harga_finishing as $key => $harga_f) {
                $sub_total += str_replace(",", "", $harga_f);
            }
        }
        $total = $sub_total + floatval(str_replace(",", "", $biaya_desain)) + floatval(str_replace(",", "", $biaya_cetak)) + floatval($kenaikan_value);
        $iddetailorder = gen_primary("DETO");
        $data_detail = array(
            'id_detail_produk_order'   => $iddetailorder,
            'kode_universal'            => $iddetailorder,
            'id_order'                  => $id_order,
            'nama_pekerjaan'            => $nama_pekerjaan,
            'id_kategori'               => $id_kategori,
            'id_produk'                 => $id_produk,
            'st_urgent'                 => $st_urgent,
            'tgl_permintaan_selesai'    => $tgl_selesai,
            'gambar'                    => 'upload' . '/' . $target_folder . '/' . $file_name,
            'thumbnail'                 => 'upload' . '/' . $target_thumb . $name . "." . "jpg",
            'st_dimensi'                => $st_dimensi,
            'p'                         => str_replace(",", "", $p),
            'l'                         => str_replace(",", "", $l),
            'id_satuan'                 => $id_konversi,
            'detail_gambar'             => $detail_gambar,
            'jml_cetak'                 => $jml_order,
            'harga_by'                  => $harga_by,
            'id_konversi_jml_cetak'     => $id_konversi_jml_order,
            'qty_cetak_by'              => $qty_cetak_by,
            'satuan_cetak_by'           => $satuan_cetak_by,
            'harga_cetak_persatuan_terkecil' => $harga_cetak_persatuan_terkecil,
            'biaya_design'              => str_replace([","], "", $biaya_desain),
            'biaya_cetak'               => str_replace([","], "", $biaya_cetak),
            'kenaikan_value'            => $kenaikan_value,
            'kenaikan_persen'           => $kenaikan_persen,
            'biaya_finishing'           => $sub_total,
            'sub_total'                 => $total,
            'catatan'                   => $note_produk,
            'st_finishing'              => $st_finishing,
            'st_finishing_standart'     => $st_finishing_standart,
            'target_server'             => $target_server
        );
        $query_detail = "";
        if ($type !== 1) {
            $save_detail = $this->order_produk_detail_model->insert($data_detail);
            $query_detail = $this->db->last_query();
        }

        $save_detail_copy = $this->morder_produk_detail_model->insert($data_detail);
        $query_detail .= "\n\n" . $this->db->last_query();

        if (isset($item_page_pdf)) {
            foreach ($item_page_pdf as $key => $page) {
                $data_det_pdf[] = array(
                    'idorder_produk_detail_a3' => gen_primary("DETA3", "order_produk_detail_a3", "idorder_produk_detail_a3"),
                    'id_detail_produk_order'   => $iddetailorder,
                    'page'                     => $page,
                    'jml_print'                => $item_jml_cetak_pdf[$key]
                );
            }
            // save on main tbl
            $save_detail_a3 = $this->order_produk_detail_a3_model->insert_batch($data_det_pdf);
            $query_detail .= "\n\n" . $this->db->last_query();
            // save on mirror tbl
            $save_detail_a3_copy = $this->morder_produk_detail_a3_model->insert_batch($data_det_pdf);
            $query_detail .= "\n\n" . $this->db->last_query();
        }
        if ($save_detail_copy !== FALSE) {
            $keterangan = "SUKSES, tambah data detail order, dengan ID : " . $data_detail['id_detail_produk_order'];
            $status     = 1;
        } else {
            $keterangan = "GAGAL, tambah data detail order, dengan ID : " . $data_detail['id_detail_produk_order'];
            $status     = 0;
        }
        //Save Log
        $nm_hak_akses   = $this->addPermission;
        $kode_universal = $data_detail['id_detail_produk_order'];
        $jumlah         = 1;
        $sql            = $query_detail;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
        if (isset($id_finishing)) {
            $jml = 0;
            foreach ($id_finishing as $key => $kode) {
                $jml++;
                $data_det_finishing[] = array(
                    'id_detail_produk_finishing_order' => gen_primary(),
                    'id_detail_produk_order' => $data_detail['id_detail_produk_order'],
                    'id_produk' => $id_finishing[$key],
                    'jml' => $jml_finishing[$key],
                    'id_konversi' => $id_konversi_jml_finishing[$key],
                    'catatan' => $note_finishing[$key],
                    'harga' => str_replace([","], "", $harga_finishing[$key]),
                );
            }
            $save_detail_finishing = $this->order_produk_detail_finishing_model->insert_batch($data_det_finishing);
            $query_detail_finishing = $this->db->last_query();

            $save_detail_finishing_copy = $this->morder_produk_detail_finishing_model->insert_batch($data_det_finishing);
            $query_detail_finishing .= "\n\n" . $this->db->last_query();
            if ($save_detail_finishing !== FALSE) {
                $keterangan = "SUKSES, tambah data detail finishing order, dengan ID : " . $data_detail['id_detail_produk_order'];
                $status     = 1;
            } else {
                $keterangan = "GAGAL, tambah data detail finishing order, dengan ID : " . $data_detail['id_detail_produk_order'];
                $status     = 0;
            }
            //Save Log
            $nm_hak_akses   = $this->addPermission;
            $kode_universal = $data_detail['id_detail_produk_order'];
            $jumlah         = $jml;
            $sql            = $query_detail_finishing;

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
        }
        if ($type === 0) {
            if ($this->akumulasi) {
                update_list($id_produk, $id_order);
            } else {
                update_total($id_order);
            }
            $this->order_produk_model->Update($id_order, ['st_revisi' => 1]);
        } else {
            if ($this->akumulasi) {
                update_list($id_produk, $id_order, 1);
            } else {
                update_total($id_order);
            }
            $this->order_produk_model->Update($id_order, ['st_revisi' => 1]);
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $this->template->set_message($this->order_produk_model->error, 'error');
            $return = ['type' => 'error', 'msg' => $this->order_produk_model->error . "\n\n" . $this->order_produk_detail_finishing_model->error . "\n\n" . $this->order_produk_detail_model->error, 'data' => ""];
        } else {
            if ($st_spk == 1) {
                $data_order = $this->morder_produk_detail_model->select('order_produk.st_po')
                    ->join('order_produk', 'order_produk.id_order= m_order_produk_detail.id_order', 'left')
                    ->find($iddetailorder);
                if ($data_order->st_po == 0) {
                    simpan_spk(0, $iddetailorder);
                    set_proses_order($id_lokasi = 5, $id_ket_proses = 1, $id_detail_produk_order = $iddetailorder, $jumlah_proses = 0);
                }
            }

            if ($type === 0) {
                $_POST['id_order'] = $id_order;
                $this->save_session_upload();
            }
            $data_view = $this->morder_produk_detail_model
                ->join('produk', 'produk.idproduk = m_order_produk_detail.id_produk', 'left')
                ->join('konversi_satuan', 'konversi_satuan.id_konversi=m_order_produk_detail.satuan_cetak_by', 'left')
                ->join('(select 
                                        id_konversi, satuan_besar as nm_satuan
                                    from 
                                        konversi_satuan 
                                    where deleted=0) as satuan', 'satuan.id_konversi = m_order_produk_detail.id_satuan', 'left')
                ->find_all_by(['m_order_produk_detail.id_order' => $id_order, 'm_order_produk_detail.deleted' => 0]);
            $return = ['type' => 'success', 'msg' => 'Upload Sukses', 'data' => $data_view];
        }
        echo json_encode($return);
    }

    protected function save()
    {
        $st_po          = $this->input->post('st_po');
        $id_order       = $this->input->post('id_order');
        $id_konsumen    = $this->input->post('id_konsumen');
        $tgl_order      = $this->input->post('tgl_order');
        $tgl_selesai    = $this->input->post('tgl_selesai');
        $no_tlp         = $this->input->post('no_tlp');
        $no_transaksi   = $this->input->post('no_transaksi');
        $no_po          = $this->input->post('no_po');
        $name   = isset($_FILES["lampiran"]) && $_FILES["lampiran"]["name"] !== '' ? $_FILES["lampiran"]["name"] : '';
        $explode = explode(".", $name);
        $ext    = end($explode);
        $lampiran       = $_FILES['lampiran']['size'] == 0 ? null : gen_primary('LAMP') . '.' . $ext;

        // $st_urgent      = $this->input->post('st_urgent');

        $id_detail_produk_order = $this->input->post('id_detail_produk_order');


        if (empty($id_detail_produk_order)) {
            $this->template->set_message(lang('order_produk_no_item_order'), 'error');
            return FALSE;
        }
        $this->form_validation->set_rules('id_order', 'lang:order_produk_no_transaksi', 'required');
        $this->form_validation->set_rules('id_konsumen', 'lang:order_produk_id_konsumen', 'callback_default_select');
        $this->form_validation->set_rules('tgl_order', 'lang:order_produk_tgl_order', 'required');
        $this->form_validation->set_message('default_select', 'You should choose the "%s" field');

        if ($this->form_validation->run() === FALSE) {
            $this->save_session();
            return FALSE;
        }

        if ($no_po !== "") {
            if (file_exists($_FILES['lampiran']['tmp_name']) || is_uploaded_file($_FILES['lampiran']['tmp_name'])) {
                $this->load->library('upload');
                $config['upload_path']   = $this->uploadFolder . '/lampiran';
                $config['allowed_types'] = 'jpeg|jpg|pdf';
                $config['max_size']      = 2000;
                $config['file_ext_tolower']  = TRUE;
                $config['overwrite']     = TRUE;
                $config['file_name']     = $lampiran;

                $this->upload->initialize($config);

                if (!$this->upload->do_upload("lampiran")) {
                    $this->template->set_message($this->upload->display_errors(), 'error');
                    return false;
                }
            }
        }
        $this->db->trans_start();
        $total = 0;
        $detail_order = $this->order_produk_detail_model->find_all_by(['deleted' => 0, 'id_order' => $id_order]);
        foreach ($detail_order as $key => $record) {
            $total += $record->sub_total;
        }
        $data = array(
            'no_transaksi'              => $no_transaksi,
            'id_konsumen'               => $id_konsumen,
            'tgl_order'                 => $tgl_order,
            'no_tlp'                    => $no_tlp,
            'no_po'                     => $no_po !== "" ? $no_po : "",
            'lampiran'                  => $no_po && $lampiran ? $lampiran : null,
            'st_simpan'                 => 1,
            'total_value_order'         => $total,
            'st_po'                     => $st_po,
        );
        $update_order = $this->order_produk_model->update($id_order, $data);
        $query = $this->db->last_query();
        if ($update_order !== FALSE) {
            $keterangan = "SUKSES, tambah data order, dengan ID : " . $id_order;
            $status     = 1;
        } else {
            $keterangan = "GAGAL, tambah data order, dengan ID : " . $id_order;
            $status     = 0;
        }
        //Save Log
        $nm_hak_akses   = $this->addPermission;
        $kode_universal = $id_order;
        $jumlah         = 1;
        $sql            = $query;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $this->template->set_message($this->order_produk_model->error, 'error');
            return FALSE;
        } else {
            $this->session->unset_userdata('order_produk');
            return TRUE;
        }
    }


    // menyimpan session
    protected function save_session()
    {
        $id_order       = $this->input->post('id_order');
        $no_tlp         = $this->input->post('no_tlp');
        $id_konsumen    = $this->input->post('id_konsumen');
        $tgl_order      = $this->input->post('tgl_order');
        $tgl_selesai    = $this->input->post('tgl_selesai');
        $no_transaksi   = $this->input->post('no_transaksi');
        $no_po          = $this->input->post('no_po');
        $lampiran       = $this->input->post('lampiran');
        $st_po          = $this->input->post('st_po');

        $data_order = $this->morder_produk_detail_model
            ->join('produk', 'produk.idproduk = m_order_produk_detail.id_produk', 'left')
            ->join('konversi_satuan', 'konversi_satuan.id_konversi=m_order_produk_detail.satuan_cetak_by', 'left')
            ->join('(select 
                                        id_konversi, satuan_besar as nm_satuan
                                    from 
                                        konversi_satuan 
                                    where deleted=0) as satuan', 'satuan.id_konversi = m_order_produk_detail.id_satuan', 'left')
            ->find_all_by(['m_order_produk_detail.id_order' => $id_order, 'm_order_produk_detail.deleted' => 0]);

        $data['list_order']     = $data_order;
        $data['id_order']       = $id_order;
        $data['tgl_order']      = $tgl_order;
        $data['tgl_selesai']    = $tgl_selesai;
        $data['id_konsumen']    = $id_konsumen;
        $data['no_tlp']         = $no_tlp;
        $data['no_transaksi']   = $no_transaksi;
        $data['no_po']          = $no_po;
        $data['lampiran']       = $lampiran;
        $data['st_po']          = $st_po;
        $this->session->set_userdata('order_produk', $data);
        $this->template->set_message(validation_errors(), 'error');
        return FALSE;
    }

    protected function save_session_upload()
    {
        $id_order       = $this->input->post('id_order');
        $id_konsumen    = $this->input->post('id_konsumen');
        $tgl_order      = $this->input->post('tgl_order');
        $tgl_selesai    = $this->input->post('tgl_selesai');
        $no_transaksi   = $this->input->post('no_transaksi');
        $no_tlp         = $this->input->post('no_tlp');
        $no_po          = $this->input->post('no_po');
        $lampiran       = $this->input->post('lampiran');
        $st_po          = $this->input->post('st_po');

        $data_order = $this->morder_produk_detail_model
            ->join('produk', 'produk.idproduk = m_order_produk_detail.id_produk', 'left')
            ->join('konversi_satuan', 'konversi_satuan.id_konversi=m_order_produk_detail.satuan_cetak_by', 'left')
            ->join('(select 
                                        id_konversi, satuan_besar as nm_satuan
                                    from 
                                        konversi_satuan 
                                    where deleted=0) as satuan', 'satuan.id_konversi = m_order_produk_detail.id_satuan', 'left')
            ->find_all_by(['m_order_produk_detail.id_order' => $id_order, 'm_order_produk_detail.deleted' => 0]);

        $data['list_order']     = $data_order;
        $data['id_order']       = $id_order;
        $data['tgl_order']      = $tgl_order;
        $data['tgl_selesai']    = $tgl_selesai;
        $data['id_konsumen']    = $id_konsumen;
        $data['no_transaksi']   = $no_transaksi;
        $data['no_tlp']         = $no_tlp;
        $data['no_po']          = $no_po;
        $data['lampiran']       = $lampiran;
        $data['st_po']          = $st_po;
        $this->session->set_userdata('order_produk', $data);
    }

    public function default_select($val)
    {
        return $val == "" ? FALSE : TRUE;
    }

    // cari produk by ajax
    public function cari_produk()
    {
        $this->auth->restrict($this->addPermission);

        if (!$this->input->is_ajax_request()) {
            $this->template->set_message(lang('order_produk_only_ajax'), 'error');
            redirect('order_produk');
        }
        $id_kategori = $this->input->post('id_kategori');

        $data = $this->produk_model->select("idproduk as id,nm_produk as text, st_tipe as tipe")
            ->where("deleted=0")
            ->order_by('nm_produk')->find_all_by(['idkategori' => $id_kategori]);
        if (!$data) {
            $return = array('type' => 'error', 'msg' => lang('order_produk_no_records_found'));
        } else {
            $return = array('type' => 'success', 'msg' => '', 'data' => $data);
        }

        echo json_encode($return);
    }
    // cari produk finishing by ajax
    public function cari_produk_finishing()
    {
        $this->auth->restrict($this->addPermission);

        if (!$this->input->is_ajax_request()) {
            $this->template->set_message(lang('order_produk_only_ajax'), 'error');
            redirect('order_produk');
        }
        $id_kategori = $this->input->post('id_kategori');
        $data = $this->produk_model->select("idproduk as id,nm_produk as text, st_tipe as tipe")
            ->where("deleted=0 AND (idkategori = $id_kategori OR idkategori=5)")
            ->order_by('nm_produk')->find_all_by(['st_produk' => 1]);
        if (!$data) {
            $return = array('type' => 'error', 'msg' => lang('order_produk_no_records_found'));
        } else {
            $return = array('type' => 'success', 'msg' => '', 'data' => $data);
        }

        echo json_encode($return);
    }
    // cek status order produk (st_minimal order pnjang lebar minimal gambar dll)
    public function cek_order()
    {
        $this->auth->restrict($this->addPermission);

        if (!$this->input->is_ajax_request()) {
            $this->template->set_message(lang('order_produk_only_ajax'), 'error');
            redirect('order_produk');
        }
        $id_produk = $this->input->post('id_produk');

        $produk = $this->produk_model->find($id_produk);
        $bom = $this->produk_model
            ->join('produk_detail_penyusun', "produk.idproduk=produk_detail_penyusun.idproduk", "left")
            ->join('barang', 'barang.idbarang_bb = produk_detail_penyusun.idbarang_bb', 'left')
            // ->where("(produk_detail_penyusun.idbarang_bb =1 OR produk_detail_penyusun.idbarang_bb=2)")
            ->where('st_cetak_a3 = 1')
            ->find_all_by(['produk_detail_penyusun.idproduk' => $id_produk, 'produk_detail_penyusun.deleted' => 0]);
        $query = $this->db->last_query();
        $jml_cetak_bom = 1;
        if ($bom) {
            foreach ($bom as $key => $rc) {
                if ($rc->jml_pakai > 1) {
                    $jml_cetak_bom = $rc->jml_pakai;
                    break;
                }
            }
        }
        if ($produk) {
            // kategori produk
            $kategori           = $this->kategori_model->find($produk->idkategori);
            $st_ukuran          = $kategori->st_ukuran;
            $panjang_kategori   = $kategori->p;
            $lebar_kategori     = $kategori->l;

            $st_minimal_order = $produk->st_minimal_order;
            // jml minmal order
            $data_jml_minimal   = $this->konversi_satuan_model->hitung_ke_satuan_kecil($produk->id_konversi_jml, floatval($produk->jml_minimal));
            $jml_minimal_order  = $data_jml_minimal['qty'];
            // panjang minimal order
            $data_panjangg_minimal   = $this->konversi_satuan_model->hitung_ke_satuan_kecil($produk->id_konversi_ukuran, floatval($produk->panjang_minimal));
            $panjang_minimal  = $data_panjangg_minimal['qty'];
            // lebar minimal order
            $data_lebar_minimal   = $this->konversi_satuan_model->hitung_ke_satuan_kecil($produk->id_konversi_ukuran, floatval($produk->lebar_minimal));
            $lebar_minimal  = $data_lebar_minimal['qty'];
            $st_tipe        = $produk->st_tipe;

            $data = array(
                'st_ukuran'        => $st_ukuran,
                'p_kategori'        => $panjang_kategori,
                'l_kategori'        => $lebar_kategori,
                'st_minimal_order'  => $st_minimal_order,
                'st_tipe'           => $st_tipe,
                'jml_minimal_order' => $jml_minimal_order,
                'panjang_minimal'   => $panjang_minimal,
                'lebar_minimal'     => $lebar_minimal,
                'jml_cetak_bom'     => $jml_cetak_bom
            );
        }
        if (!$data) {
            $return = array('type' => 'error', 'msg' => lang('order_produk_no_records_found'));
        } else {
            $return = array('type' => 'success', 'msg' => '', 'data' => $data);
        }

        echo json_encode($return);
    }


    public function get_size()
    {
        if (strtolower(substr(strrchr($_FILES["gambar"]["tmp_name"], '.'), 1)) == 'jpg' || strtolower(substr(strrchr($_FILES["gambar"]["tmp_name"], '.'), 1)) == 'jpeg') {
            $return = $this->get_dpi($_FILES["gambar"]["tmp_name"], 'r');
        } else {
            $return = $this->get_size_tif($_FILES["gambar"]["tmp_name"]);
        }
        echo json_encode($return);
    }

    public function get_size_tif($filename)
    {
        // $pos_image = $_FILES["gambar"]["tmp_name"];

        $img    = new imagick($filename);


        $data_width     = $img->getImageWidth();
        $data_height    = $img->getImageHeight();
        $data_res       = $img->getImageResolution();

        // echo "width = ". $data_width." x height = ". $data_height." ; Resolusi, (X) = ".$data_res['x']." x (Y) = ".$data_res['y'];
        return array($data_res['x'], $data_res['y'], $data_width, $data_height);
    }




    public function get_biaya_cetak()
    {
        $this->auth->restrict($this->addPermission);

        if (!$this->input->is_ajax_request()) {
            $this->template->set_message(lang('order_produk_only_ajax'), 'error');
            redirect('order_produk');
        }

        $id_produk          = $this->input->post('id_produk');
        $id_konsumen        = $this->input->post('id_konsumen');
        $tgl_order          = $this->input->post('tgl_order');
        $panjang            = $this->input->post('p');
        $lebar              = $this->input->post('l');
        $id_konversi_gambar = $this->input->post('id_konversi_gambar');
        $jml_order          = $this->input->post('jml_order');


        // variabel
        $biaya_cetak = 0;
        $sub_harga  = 0;
        $harga      = 0;
        $cek = false;

        // real panjang
        $data_real_p            = $this->konversi_satuan_model->hitung_ke_satuan_kecil($id_konversi_gambar, floatval($panjang));
        $real_p                 = $data_real_p['qty'];


        // real lebar
        $data_real_l            = $this->konversi_satuan_model->hitung_ke_satuan_kecil($id_konversi_gambar, floatval($lebar));
        $real_l                 = $data_real_l['qty'];

        if ($id_konsumen != 0) {
            $status_konsumen = 2;
            // $data_konsumen = $this->konsumen_model->find_by(['idkonsumen' => $id_konsumen]);
            // $status_konsumen = $data_konsumen->st;
        } else {
            $data_konsumen = $this->konsumen_model->find_by(['idkonsumen' => $id_konsumen]);
            // statu konsumen 0: biasa 1: reseller 2:instansi

            $status_konsumen = $data_konsumen->st;
        }

        // khusus indoor
        if ($this->akumulasi !== true) {
            $produk = $this->produk_model->find($id_produk);
            if ($produk->idkategori == 3) {
                $lebar_produk = $this->konversi_satuan_model->hitung_ke_satuan_kecil($produk->id_konversi_ukuran, floatval($produk->lebar_minimal));
                $real_lebar_produk = $lebar_produk['qty'];
                die(print_r(['real_l' => $real_l, 'l produk' => $real_lebar_produk]));
                if ($real_l < $real_lebar_produk) {
                    $real_l = $real_lebar_produk;
                }
            }
        }

        $luas = ($real_l * $real_p) * $jml_order;




        // cek promo
        $promo = $this->setting_promo_model->where("tgl_awal <= '$tgl_order' AND date(tgl_akhir) >= '$tgl_order'")->find_by(['id_produk' => $id_produk, 'deleted' => 0]);
        if ($promo) {
            $data_harga = get_harga_produk($id_produk, 3, $luas, $jml_order);
            $harga = $data_harga['harga'];
        } else {
            $data_harga = get_harga_produk($id_produk, $status_konsumen, $luas, $jml_order);
            $harga = $data_harga['harga'];
        }
        if ($harga == 0) {
            $data_harga =  get_harga_produk($id_produk, 1, $luas, $jml_order);
            $harga = $data_harga['harga'];
        }



        if ($status_konsumen == 1) {
            // jika ada promo
            if ($promo) {
                $data_harga = get_harga_produk($id_produk, 3, $luas, $jml_order);
                $harga = $data_harga['harga'];
            } else {
                // jika tdak ada promo cek harga reseller
                $data_harga = get_harga_produk($id_produk, 2, $luas, $jml_order);
                $harga = $data_harga['harga'];
                if ($harga == 0) {
                    $data_harga = get_harga_produk($id_produk, 1, $luas, $jml_order);
                    $harga = $data_harga['harga'];
                }
            }
        } elseif ($status_konsumen == 0 || $status_konsumen == 2) {

            // jika ada promo
            if ($promo) {
                //harga promo
                $data_harga = get_harga_produk($id_produk, 3, $luas, $jml_order);
                $harga = $data_harga['harga'];
            } else {
                $data_harga =  get_harga_produk($id_produk, 2, $luas, $jml_order);
                $harga = $data_harga['harga'];
            }
        }

        echo json_encode(array('biaya_cetak' => $harga, 'harga_permeter' => $harga, 'luas' => $luas, 'qty_harga_by' => $data_harga['qty_harga_by'], 'id_konversi_harga_by' => $data_harga['id_konversi_harga_by'], 'biaya_cetak_by' => $data_harga['biaya_cetak_by'], 'harga_by' => $data_harga['harga_by']));
    }
    public function get_harga_finishing()
    {
        $this->auth->restrict($this->addPermission);

        if (!$this->input->is_ajax_request()) {
            $this->template->set_message(lang('order_produk_only_ajax'), 'error');
            redirect('order_produk');
        }

        $id_produk          = $this->input->post('id_produk');
        $id_konsumen        = $this->input->post('id_konsumen');
        $tgl_order          = $this->input->post('tgl_order');
        $panjang            = $this->input->post('p');
        $lebar              = $this->input->post('l');
        $id_konversi_gambar = $this->input->post('id_konversi_gambar');
        $jml_order          = $this->input->post('jml_order');
        $id_konversi_jml_order = $this->input->post('id_konversi_jml_order');
        $jml_finishing      = $this->input->post('jml_finishing');
        // variabel
        $harga_finishing = 0;
        $sub_harga  = 0;
        $harga      = 0;
        $cek = false;


        // real panjang
        $data_real_p            = $this->konversi_satuan_model->hitung_ke_satuan_kecil($id_konversi_gambar, floatval($panjang));
        $real_p                 = $data_real_p['qty'];


        // real lebar
        $data_real_l            = $this->konversi_satuan_model->hitung_ke_satuan_kecil($id_konversi_gambar, floatval($lebar));
        $real_l                 = $data_real_l['qty'];

        if ($id_konsumen == 0) {
            $status_konsumen = 1;
        } else {
            $data_konsumen = $this->konsumen_model->find_by(['idkonsumen' => $id_konsumen]);
            // statu konsumen 0: biasa 1: reseller 2:instansi
            $status_konsumen = $data_konsumen->st;
        }

        $total_jml_finishing = $jml_finishing * $jml_order;
        $luas = ($real_l * $real_p) * $total_jml_finishing;
        $promo = $this->setting_promo_model->where("tgl_awal <= '$tgl_order' AND date(tgl_akhir) >= '$tgl_order'")->find_by(['id_produk' => $id_produk, 'deleted' => 0]);
        if ($promo) {

            $data_harga = get_harga_produk($id_produk, 3, $luas, $total_jml_finishing);

            $harga = $data_harga['harga'];
        } else {
            $data_harga = get_harga_produk($id_produk, $status_konsumen, $luas, $total_jml_finishing);
            $harga = $data_harga['harga'];
        }
        if ($harga == 0) {
            $data_harga =  get_harga_produk($id_produk, 1, $luas, $total_jml_finishing);
            $harga = $data_harga['harga'];
        }

        echo json_encode($harga);
    }

    protected function buat_kode($prefix = "TRX")
    {
        $month  = date("m", strtotime(date("Y/m/d")));
        $year   = date("Y", strtotime(date("Y/m/d")));
        $day    = date("d", strtotime(date("Y/m/d")));
        $user_id = $this->auth->user_id();
        $dt_trx = $this->order_produk_model->order_by('no_transaksi', 'ASC')->find_all_by(array('MONTH(tgl_order)' => $month, 'YEAR(tgl_order)' => $year, 'DAY(tgl_order)' => $day, 'deleted' => 0, 'created_by' => $user_id));
        $last_kode = "";
        if ($dt_trx) {
            foreach ($dt_trx as $key => $dp) {
                $last_kode = $dp->no_transaksi;
            }
        }

        if ($last_kode) {
            $tkode = explode("-", $last_kode);
            if (count($tkode) == 2) {
                $user_kode = explode("D", $tkode[1]);


                $tkode = intval($user_kode[0]);
            } else {
                $tkode = 0;
            }
            $kode = strtoupper($prefix) . "/" . $year . "/" . $month . $day . "-" . str_pad($tkode + 1, 5, "0", STR_PAD_LEFT) . "D" . $user_id;
        } else {
            $kode = strtoupper($prefix) . "/" . $year . "/" . $month . $day . "-00001" . "D" . $user_id;
        }
        return $kode;
    }

    public function cancel()
    {
        $this->auth->restrict($this->addPermission);

        $this->session->unset_userdata('order_produk');
        $this->template->set_message(lang('order_produk_canceled'), 'success');

        redirect('order_produk');
    }

    public function reset()
    {
        $this->auth->restrict($this->addPermission);

        $this->session->unset_userdata('order_produk');
        $this->template->set_message(lang('order_produk_canceled'), 'success');

        redirect('order_produk/create');
    }


    public function del_detail_order()
    {
        $this->del_detail();
    }
    // type => 0: new order 1: update order
    public function del_detail($type = 0)
    {
        $this->auth->restrict($this->deletePermission);

        if (!$this->input->is_ajax_request()) {
            $this->template->set_message(lang('order_produk_only_ajax'), 'error');
            redirect('order_produk');
        }
        $id = $this->input->post('id');
        $return = [];
        if (isset($id)) {
            if ($type === 0) {
                $data = $this->order_produk_detail_model
                    ->join('order_produk', 'order_produk.id_order=order_produk_detail.id_order', 'left')
                    ->find($id);
            } else {
                $data = $this->morder_produk_detail_model
                    ->join('order_produk', 'order_produk.id_order=m_order_produk_detail.id_order', 'left')
                    ->find($id);
            }

            if ($data) {
                if ($data->st_kasir == 0) {
                    // belum dibuatkan nota
                    $cek_del = TRUE;
                    if ($data->st_ctk_struk_tmp == 1) {
                        if ($data->st_acc_delete ==  1 && $data->st_acc_delete == 0 && $data->st_kasir == 0) {
                            $cek_del = FALSE;
                        } else {
                            $cek_del = TRUE;
                        }
                    }
                    if ($cek_del == TRUE) {
                        if (@unlink($data->thumbnail)) {
                            if ($type === 0) {
                                $del = $this->order_produk_detail_model->delete($id);
                            }
                            $del = $this->morder_produk_detail_model->delete($id);
                            if ($del) {
                                if ($this->akumulasi) {
                                    update_list($data->id_produk, $data->id_order);
                                } else {
                                    update_total($data->id_order);
                                }
                                $_POST['id_order'] = $data->id_order;
                                $_POST['id_konsumen']    = $data->id_konsumen;
                                $_POST['tgl_order']      = $data->tgl_order;
                                $_POST['no_transaksi']   = $data->no_transaksi;
                                $_POST['no_tlp']         = $data->no_tlp;
                                $_POST['no_po']          = $data->no_po;
                                $_POST['lampiran']       = $data->lampiran;
                                $this->save_session_upload();
                                $data_view = $this->morder_produk_detail_model
                                    ->join('produk', 'produk.idproduk = m_order_produk_detail.id_produk', 'left')
                                    ->join('konversi_satuan', 'konversi_satuan.id_konversi=m_order_produk_detail.satuan_cetak_by', 'left')
                                    ->join('(select 
                                                                id_konversi, satuan_besar as nm_satuan
                                                            from 
                                                                konversi_satuan 
                                                            where deleted=0) as satuan', 'satuan.id_konversi = m_order_produk_detail.id_satuan', 'left')
                                    ->find_all_by(['m_order_produk_detail.id_order' => $data->id_order, 'm_order_produk_detail.deleted' => 0]);

                                $keterangan = "SUKSES, Hapus detail order, dengan ID : " . $id;
                                $status     = 1;
                                $return = ['type' => 'success', 'msg' => 'Sukses hapus data detail order.', 'data' => $data_view];
                            } else {
                                $keterangan = "GAGAL, Hapus detail order, dengan ID : " . $id;
                                $status     = 0;
                                $return = ['type' => 'error', 'msg' => 'Gagal hapus data detail order.'];
                            }
                            $nm_hak_akses   = $this->deletePermission;
                            $kode_universal = $id;
                            $jumlah         = 1;
                            $sql            = $this->db->last_query();
                            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                        }
                    } else {
                        $return = ['type' => 'error', 'msg' => 'Detail Order tidak bisa dihapus karena sudah dibuatkan nota sementara'];
                    }
                } else {
                    $return = ['type' => 'error', 'msg' => 'Data Tidak Tidak Dapat Dihapus karena sudah dibuatkan Nota.'];
                }
            } else {
                $return = ['type' => 'error', 'msg' => 'Data Tidak Ditemukan.'];
            }
        } else {
            $return = ['type' => 'error', 'msg' => 'Data Tidak Ditemukan.'];
        }
        echo json_encode($return);
    }

    // delete order saat edit
    public function del_detail_order_edit()
    {
        $this->del_detail(1);
    }
    //Edit Order
    public function edit()
    {

        $this->auth->restrict($this->managePermission);

        $id = $this->uri->segment(3);

        if (empty($id)) {
            $this->template->set_message(lang("konsumen_invalid_id"), 'error');
            redirect('order_produk');
        }


        $data   = $this->order_produk_model
            ->join('konsumen', 'konsumen.idkonsumen=order_produk.id_konsumen', 'left')
            ->find($id);
        if ($data->tgl_order != date("Y-m-d")) {
            $this->template->set_message('Order tidak dapat diedit (tgl edit melebihi tanggal order)', 'error');
            redirect('order_produk');
        }
        $setting_upload = $this->setting_upload_model->find(1);
        $detail = $this->morder_produk_detail_model
            ->join('produk', 'produk.idproduk = m_order_produk_detail.id_produk', 'left')
            ->join('konversi_satuan', 'konversi_satuan.id_konversi=m_order_produk_detail.satuan_cetak_by', 'left')
            ->join('(select 
                                                        id_konversi, satuan_besar as nm_satuan
                                                    from 
                                                        konversi_satuan 
                                                    where deleted=0) as satuan', 'satuan.id_konversi = m_order_produk_detail.id_satuan', 'left')
            ->find_all_by(['m_order_produk_detail.id_order' => $id, 'm_order_produk_detail.deleted' => 0]);

        $kategori           = $this->kategori_model
            ->join("produk", "produk.idkategori=kategori.idkategori", "left")
            ->group_by("kategori.idkategori")
            ->find_all_by(['kategori.deleted' => 0, 'st_produk' => 0]);
        $konversi           = get_konversi(4, '', FALSE);
        $default_tgl = date('d-m-Y');


        $assets = array(
            'plugins/select2/dist/js/select2.js',
            'plugins/select2/dist/css/select2.css',
            'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
            'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
            'plugins/md-time-picker/mdtimepicker.js',
            'plugins/md-time-picker/mdtimepicker.css',
            'plugins/number/jquery.number.js',
            'plugins/toastr/toastr.js',
            'plugins/toastr/toastr.css',
            'order_produk/assets/js/order_produk.js',
            'order_produk/assets/js/create_order.js',
            'order_produk/assets/css/order.css',
        );
        add_assets($assets);

        $this->template->set('default_tgl', $default_tgl);
        $this->template->set('default_jam', $this->default_jam);
        $this->template->set('use_image', $this->use_image);
        $this->template->set('upload_file', $this->upload_file);
        $this->template->set('auto_image_measuring', $this->auto_image_measuring);

        // $this->template->set('default_jam', $this->default_jam);
        $this->template->set('data', $data);
        $this->template->set('kategori', $kategori);
        $this->template->set('konversi', $konversi);
        $this->template->set('detail', $detail);
        $this->template->set('setting', json_encode($setting_upload));
        $this->template->set('setting_upload', $setting_upload);

        $this->template->title(lang('order_produk_title_manage'));
        $this->template->render('edit');
    }
    //view Order
    public function view($id_order = '')
    {
        $this->auth->restrict($this->viewPermission);

        if (!$id_order) {
            $this->template->set_message(lang("order_produk_invalid_id"), 'error');
            redirect('order_produk');
        }

        $data   = $this->order_produk_model
            ->join('konsumen', 'konsumen.idkonsumen=order_produk.id_konsumen', 'left')
            ->find($id_order);
        $detail = $this->morder_produk_detail_model
            ->join('produk', 'produk.idproduk = m_order_produk_detail.id_produk', 'left')
            ->join('konversi_satuan', 'konversi_satuan.id_konversi=m_order_produk_detail.satuan_cetak_by', 'left')
            ->join('(select 
                                                        id_konversi, satuan_besar as nm_satuan
                                                    from 
                                                        konversi_satuan 
                                                    where deleted=0) as satuan', 'satuan.id_konversi = m_order_produk_detail.id_satuan', 'left')
            ->find_all_by(['m_order_produk_detail.id_order' => $id_order, 'm_order_produk_detail.deleted' => 0]);
        $assets = array(
            'plugins/select2/dist/js/select2.js',
            'plugins/select2/dist/css/select2.css',
            'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
            'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
            'plugins/number/jquery.number.js',
            'order_produk/assets/js/order_produk.js',
        );
        add_assets($assets);

        $this->template->set('default_jam', $this->default_jam);
        $this->template->set('data', $data);
        $this->template->set('detail', $detail);

        $this->template->title(lang('order_produk_title_view'));
        $this->template->render('view');
    }
    public function view_detail_order()
    {
        $this->auth->restrict($this->addPermission);

        if (!$this->input->is_ajax_request()) {
            $this->template->set_message(lang('order_produk_only_ajax'), 'error');
            redirect('order_produk');
        }
        $id = $this->input->post('id');
        $setting_upload = $this->setting_upload_model->find(1);
        if (isset($id)) {
            $sql = "select t.*, t1.satuan_besar as satuan_panjang, t2.satuan_besar as satuan_order, nm_produk, nmkategori
                    from m_order_produk_detail as t
                    left join produk on t.id_produk = produk.idproduk
                    left join konversi_satuan t1 on t1.id_konversi= t.id_satuan
                    left join konversi_satuan t2 on t2.id_konversi= t.id_konversi_jml_cetak
                    left join kategori on kategori.idkategori= t.id_kategori
                    where t.id_detail_produk_order = '$id'";
            $data_detail = $this->db->query($sql)->row();
            if ($data_detail->st_finishing == 1) {
                $data_det_finishing = $this->morder_produk_detail_finishing_model
                    ->join('konversi_satuan', 'm_order_produk_detail_finishing.id_konversi=konversi_satuan.id_konversi', 'left')
                    ->join('produk', 'produk.idproduk=m_order_produk_detail_finishing.id_produk', 'left')
                    ->find_all_by(['id_detail_produk_order' => $data_detail->id_detail_produk_order]);
                if ($data_det_finishing) {
                    $return = ['type' => 'success', 'data' => $data_detail, 'detail' => $data_det_finishing, 'setting_upload' => $setting_upload];
                } else {
                    $return = ['type' => 'success', 'data' => $data_detail, 'detail' => '', 'setting_upload' => $setting_upload];
                }
            } else {
                $return = ['type' => 'success', 'data' => $data_detail, 'detail' => '', 'setting_upload' => $setting_upload];
            }
        } else {
            $return = ['type' => 'error', 'msg' => 'Data Tidak Ditemukan.'];
        }
        echo json_encode($return);
    }
    public function view_detail_order_edit()
    {
        $this->auth->restrict($this->addPermission);

        if (!$this->input->is_ajax_request()) {
            $this->template->set_message(lang('order_produk_only_ajax'), 'error');
            redirect('order_produk');
        }
        $id = $this->input->post('id');
        $setting_upload = $this->setting_upload_model->find(1);
        if (isset($id)) {
            $sql = "select t.*, t1.satuan_besar as satuan_panjang, t2.satuan_besar as satuan_order, nm_produk, nmkategori
                    from m_order_produk_detail as t
                    left join produk on t.id_produk = produk.idproduk
                    left join konversi_satuan t1 on t1.id_konversi= t.id_satuan
                    left join konversi_satuan t2 on t2.id_konversi= t.id_konversi_jml_cetak
                    left join kategori on kategori.idkategori= t.id_kategori
                    where t.id_detail_produk_order = '$id'";
            $data_detail = $this->db->query($sql)->row();
            if ($data_detail->st_finishing == 1) {
                $data_det_finishing = $this->morder_produk_detail_finishing_model
                    ->join('konversi_satuan', 'm_order_produk_detail_finishing.id_konversi=konversi_satuan.id_konversi', 'left')
                    ->join('produk', 'produk.idproduk=m_order_produk_detail_finishing.id_produk', 'left')
                    ->find_all_by(['id_detail_produk_order' => $data_detail->id_detail_produk_order]);
                if ($data_det_finishing) {
                    $return = ['type' => 'success', 'data' => $data_detail, 'detail' => $data_det_finishing, 'setting_upload' => $setting_upload];
                } else {
                    $return = ['type' => 'success', 'data' => $data_detail, 'detail' => '', 'setting_upload' => $setting_upload];
                }
            } else {
                $return = ['type' => 'success', 'data' => $data_detail, 'detail' => '', 'setting_upload' => $setting_upload];
            }
        } else {
            $return = ['type' => 'error', 'msg' => 'Data Tidak Ditemukan.'];
        }
        echo json_encode($return);
    }
    public function view_detail($id_detail_order = '')
    {
        $this->auth->restrict($this->viewPermission);

        if (!$id_detail_order) {
            $this->template->set_message(lang("order_produk_invalid_id"), 'error');
            redirect('order_produk');
        }
        $sql = "select t.*, t1.satuan_besar as satuan_panjang, t2.satuan_besar as satuan_order, nm_produk, nmkategori
                from m_order_produk_detail as t
                left join produk on t.id_produk = produk.idproduk
                left join konversi_satuan t1 on t1.id_konversi= t.id_satuan
                left join konversi_satuan t2 on t2.id_konversi= t.id_konversi_jml_cetak
                left join kategori on kategori.idkategori= t.id_kategori
                where t.id_detail_produk_order = '$id_detail_order'";
        $data_detail = $this->db->query($sql)->row();
        if ($data_detail->st_finishing == 1) {
            $data_det_finishing = $this->morder_produk_detail_finishing_model
                ->join('konversi_satuan', 'm_order_produk_detail_finishing.id_konversi=konversi_satuan.id_konversi', 'left')
                ->join('produk', 'produk.idproduk=m_order_produk_detail_finishing.id_produk', 'left')
                ->find_all_by(['id_detail_produk_order' => $data_detail->id_detail_produk_order]);
            if ($data_det_finishing) {
                $this->template->set('detail', $data_det_finishing);
            }
        }
        $assets = array(
            'plugins/select2/dist/js/select2.js',
            'plugins/select2/dist/css/select2.css',
            'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
            'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
            'plugins/number/jquery.number.js',
            'order_produk/assets/js/order_produk.js',
        );
        add_assets($assets);
        $this->template->set('data', $data_detail);
        $this->template->title(lang('order_produk_title_view'));
        $this->template->render('order_produk/view_detail');
    }

    function get_dpi($filename)
    {
        if ($a = fopen($filename, 'r')) {
            $string = fread($a, 20);
            ob_clean();
            flush();
            fclose($a);

            $data = bin2hex(substr($string, 14, 4));
            $x = substr($data, 0, 4);
            $y = substr($data, 0, 4);
        } else {
            $x = 0;
            $y = 0;
        }
        $data = getimagesize($filename);
        $width = $data[0];
        $height = $data[1];
        // return($filename);
        return array(hexdec($x), hexdec($y), $width, $height);
    }

    public function get_tlp()
    {
        $this->auth->restrict($this->addPermission);

        if (!$this->input->is_ajax_request()) {
            $this->template->set_message(lang('order_produk_only_ajax'), 'error');
            redirect('order_produk');
        }
        $id = $this->input->post('id');
        $konsumen = $this->konsumen_model->find($id);
        if ($konsumen) {
            $tlp = $konsumen->telp;
        } else {
            $tlp = "";
        }
        echo json_encode($tlp);
    }

    public function get_kenaikan()
    {
        $this->auth->restrict($this->addPermission);

        if (!$this->input->is_ajax_request()) {
            $this->template->set_message(lang('order_produk_only_ajax'), 'error');
            redirect('order_produk');
        }
        $selisih = $this->input->post('selisih');
        $data = $this->Setting_harga_urgent_model->where("deleted=0 AND (durasi_awal >= '$selisih' AND durasi_akhir<= '$selisih')")->find();
        if ($data) {
            $persen = $data->persen_harga;
            $return = ['type' => 'success', 'persen' => $persen];
        } else {
            $cari_max = "SELECT * FROM setting_urgent where durasi_akhir = (select max(durasi_akhir) from setting_urgent where deleted=0)";
            $max = $this->db->query($cari_max)->row();
            $cari_min = "SELECT * FROM setting_urgent where durasi_akhir = (select min(durasi_akhir) from setting_urgent where deleted=0)";
            $min = $this->db->query($cari_min)->row();
            if ($max) {
                if ($selisih > $max->durasi_akhir) {
                    $persen = $max->persen_harga;
                } else {
                    $persen = $min->persen_harga;
                }
                $return = ['type' => 'success', 'persen' => $persen];
            } else {
                $return = ['type' => 'error', 'msg' => 'Data setting kenaikan harga tidak ditemukan'];
            }
        }
        echo json_encode($return);
    }



    public function update_total($id_order)
    {
        $data_list_order = $this->morder_produk_detail_model->find_all_by(['id_order' => $id_order, 'deleted' => 0]);
        if ($data_list_order) {
            $total_order = 0;
            foreach ($data_list_order as $key => $record) {
                $total_order += $record->sub_total;
            }
            $update = $this->order_produk_model->update($id_order, ['total_value_order' => $total_order]);
        } else {
            $update = $this->order_produk_model->update($id_order, ['total_value_order' => 0]);
        }
        if ($update) {
            $keterangan = "SUKSES, Update data Total Order : " . $id_order;
            $status     = 1;
        } else {
            $keterangan = "GAGAL, Update data Total Order : " . $id_order;
            $status     = 0;
        }
        $nm_hak_akses   = $this->managePermission;
        $kode_universal = $id_order;
        $jumlah         = 1;
        $sql            = $this->db->last_query();

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
    }

    public function get_selected_konsument()
    {
        if (!$this->input->is_ajax_request()) {
            die(json_encode(array('type' => 'error', 'msg' => lang('order_produk_only_ajax'))));
        }

        $id_konsumen = $this->input->post('id_konsumen');

        $data = $this->konsumen_model
            ->order_by('nama', 'ASC')
            ->find_all_by(['idkonsumen' => $id_konsumen]);

        $list = [];
        if ($data) {
            foreach ($data as $key => $value) {
                $list[$key]['id'] = $value->idkonsumen;
                $list[$key]['text'] = $value->panggilan . " " . $value->nama . ',' . $value->telp;
                $list[$key]['tipe'] = $value->st;
            }

            echo json_encode($list);
        } else {
            echo "No Result";
        }

        // if(!$data)
        // {
        //     $return = array('type' => 'error', 'msg' => lang('barang_no_records_found'));
        // }
        // else
        // {
        //     $return = array('type' => 'success', 'msg' => '', 'data' => $data);    
        // }

        // echo json_encode($return);
    }
}
