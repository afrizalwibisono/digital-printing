<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['gudang_order_title_manage']  = 'Gudang Order';
$lang['gudang_order_title_new'] 	= 'Gudang Order Baru';
$lang['gudang_order_title_edit'] 	= 'Edit Status Gudang Order';
$lang['gudang_order_title_view'] 	= 'Detail Gudang Order';

// form/table
$lang['gudang_order_no_req'] 		= 'Kode Permintaan';
$lang['gudang_order_tanggal_kirim'] = 'Tanggal Kirim';
$lang['gudang_order_total'] 		= 'Total';

$lang['gudang_order_range_tgl'] = 'Range Tanggal';
$lang['gudang_order_tgl_awal']  = 'Tanggal awal';
$lang['gudang_order_tgl_akhir'] = 'Tanggal akhir';

$lang['gudang_order_no'] 		= '#';
$lang['gudang_order_barcode'] 	= 'Barcode';
$lang['gudang_order_nm_gudang'] = 'Gudang';
$lang['gudang_order_nm_barang'] = 'Nama Barang';
$lang['gudang_order_qty'] 		= 'Qty';
$lang['gudang_order_harga'] 	= 'Harga(Rp.)';
$lang['gudang_order_satuan'] 	= 'Satuan';
$lang['gudang_order_total'] 	= 'Total';
$lang['gudang_order_gtotal'] 	= 'Grand Total (Rp.): ';
$lang['gudang_order_dibuat_oleh'] = 'Dibuat Oleh';
$lang['gudang_order_st_req'] 	= 'Status';
$lang['gudang_order_kategory'] 	= 'Kategori';
$lang['gudang_order_jenis'] 	= 'Jenis';

$lang['gudang_order_pilih_brg'] = 'Pilih Barang';
$lang['gudang_order_scan_brg']  = 'Ketik / Scan Barcode Barang';
$lang['gudang_order_cari_brg']  = 'Cari Barang ...';
$lang['gudang_order_cari_btn']  = 'Cari';
$lang['gudang_order_pilih_btn'] = 'Pilih';

// button
$lang['gudang_order_btn_new'] 		= 'Baru';
$lang['gudang_order_btn_delete'] 	= 'Hapus';
$lang['gudang_order_btn_send'] 		= 'Kirim Permintaan';
$lang['gudang_order_btn_save'] 		= 'Simpan';
$lang['gudang_order_btn_cancel'] 	= 'Batal';
$lang['gudang_order_btn_back'] 		= 'Kembali';

// messages
$lang['gudang_order_del_error']			= 'Anda belum memilih order yang akan dihapus.';
$lang['gudang_order_del_failure']		= 'Tidak dapat menghapus order : ';
$lang['gudang_order_delete_confirm']	= 'Apakah anda yakin akan menghapus order terpilih ?';
$lang['gudang_order_cancel_confirm']	= 'Apakah anda yakin akan membatalkan order baru ?';
$lang['gudang_order_deleted']			= 'Gudang order berhasil dihapus';
$lang['gudang_order_no_records_found'] 	= 'Data tidak ditemukan.';

$lang['gudang_order_create_failure'] = 'Order baru gagal disimpan: ';
$lang['gudang_order_create_success'] = 'Order baru berhasil disimpan';
$lang['gudang_order_canceled'] 	   	 = 'Order baru telah berhasil dibatalkan';
$lang['gudang_order_edit_success'] 	 = 'Status order berhasil disimpan';
$lang['gudang_order_edit_fail'] 	 = 'Status order gagal disimpan';

$lang['gudang_order_invalid_id'] 	= 'ID Tidak Valid';

$lang['gudang_order_no_item'] 		= 'Anda belum menambahkan data barang yang akan diminta.';
$lang['gudang_order_qty_nol'] 		= 'Qty baris ke: %s tidak boleh kurang atau sama dengan 0 (nol).';
$lang['gudang_order_harga_nol'] 	= 'Harga baris ke: %s tidak boleh kurang atau sama dengan 0 (nol).';

$lang['gudang_order_barcode_found'] 	= 'Barcode : %s ditemukan.';
$lang['gudang_order_barcode_not_found'] = 'Barcode : %s tidak ditemukan.';

$lang['gudang_order_only_ajax'] 		= 'Hanya ajax request yang diijinkan.';