$(document).ready(function(){
	$("#ck_all").on("click", function(){
		if($(this).prop("checked")){
			$("input[name='item_code[]']").prop("checked", true);
		}else{
			$("input[name='item_code[]']").prop("checked", false);
		}
	});
	$("select[name='konversi[]'], #st_req").select2();

	$("#cari_btn").on("click", function(){
		cari_barang();
	});

	$("#cr_barang").on("keydown", function(e){
		if(e.keyCode == 13){
			cari_barang();
			e.preventDefault();
		}
	});

	//Date Picker
	$('#tanggal_kirim').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true,
	});

	//Date Picker
	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	//Add Items
	$("#pilih_btn").on("click", function(){
		add_items();
	});

	//Scan Barcode
	$("input[name='barcode']").on("keydown", function(e){
		if(e.keyCode == 13){
			scan_barcode();
			e.preventDefault();
		}
	});
});

//cari barang
function cari_barang(){
	var cari = $("#cr_barang").val();

	$.ajax({
		url : baseurl+"gudang_order/cari_barang",
		type : "post",
		dataType : "json",
		data : {cr_barang : cari},
		success : function(msg){
			$("table#tbl_barang tbody tr").remove();
			if(!msg){
				var $tr = $("<tr>"
							+"<td colspan='6' class='text-center'>Data tidak ditemukan</td>"
							+"</tr>");

				$("table#tbl_barang tbody").append($tr);
			}else{

				var opt_select = "";
				$.each(msg, function(i,n){
					$.each(n['konversi'], function(l,m){
						var selected = "";
						if(m['selected'] == 1)
						{
							selected = "selected";
						}

						opt_select += '<option value="'+m['id_konversi']+'" '+selected+'>'+m['tampil2']+'</option>';
					});

					var $tr = $("<tr data-konversi='"+opt_select+"'>"
								+"<td><input type='checkbox' name='item_code[]' value='"+n['idbarang_bb']+"'></td>"
								+"<td><input type='hidden' name='item_barcode[]' value='"+n['barcode']+"' />"+(i+1)+"</td>"
								+"<td><input type='hidden' name='item_id_satuan[]' value='"+n['id_satuan_terkecil']+"' />"+n['barcode']+"</td>"
								+"<td><input type='hidden' name='item_kategori[]' value='"+n['nmkategori']+"' />"+n['nm_barang']+"</td>"
								+"<td><input type='hidden' name='item_jenis[]' value='"+n['nmjenis_bb']+"' />"+n['nmkategori']+"</td>"
								+"<td><input type='hidden' name='item_nm_satuan[]' value='"+n['satuan']+"' /><input type='hidden' name='item_nm_barang[]' value='"+n['nm_barang']+"' />"+n['nmjenis_bb']+"</td>"
								+"</tr>");

					$("table#tbl_barang tbody").append($tr);

					opt_select = "";
				});
				
			}
			
		}
	});
}

//Add item to list
function add_items()
{
	var $obj_ids   = $("#tbl_barang input[name='item_code[]']:checked");

	var ids_barang = $obj_ids.map(function(){
		return $(this).val();
	}).get();

	var ids_barcodes = [];
	var nms_barang = [];
	var ids_satuan = [];
	var nms_satuan = [];
	var options 	= [];

	$.each($obj_ids, function(){
		ids_barcodes.push($(this).closest("tr").find("input[name='item_barcode[]']").val());
		nms_barang.push($(this).closest("tr").find("input[name='item_nm_barang[]']").val());
		ids_satuan.push($(this).closest("tr").find("input[name='item_id_satuan[]']").val());
		nms_satuan.push($(this).closest("tr").find("input[name='item_nm_satuan[]']").val());
		options.push($(this).closest("tr").data("konversi"));
	});

	if(ids_barang.length == 0)
	{
        alertify.error('Silahkan pilih barang yang akan ditambahkan dulu');
        return false;
	}

	var pj_ids = ids_barang.length;

	for (var i = 0; i < pj_ids; i++) {
		
 		var $row = $("<tr>"
		  					+"<td></td>"
		  					+"<td>"
		  						+ids_barcodes[i]
		  						+"<input type='hidden' name='id_barang[]' value='"+ids_barang[i]+"' />"
		  						+"<input type='hidden' name='id_satuan[]' value='"+ids_satuan[i]+"' />"
		  					+"</td>"
		  					+"<td>"+nms_barang[i]+" - <a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>"
		  					+"<td><input type='number' min='1' name='qty[]' class='form-control' value='1' /></td>"
		  					+"<td><select class='form-control' name='konversi[]' style='width: 200px;'>"+options[i]+"</select></td>"
		  				+"</tr>");

		var pj = $("#tdet_order tr").length;
		var cek = false;

		if(pj > 2){
			for (var j = 0; j <= pj - 1; j++) {
				var kode = $("#tdet_order tr:eq("+(j+1)+")").find("input[name='id_barang[]']").val();

				if(kode != "" && kode == ids_barang[i]){
					var qty = parseFloat($("#tdet_order tr:eq("+(j+1)+")").find("input[name='qty[]']").val());

					$("#tdet_order tr:eq("+(j+1)+")").find("input[name='qty[]']").val(qty+1);
					
					cek = true;
					break;
				}

			}
			
		}

		if(cek == false){
			$("#tdet_order tbody").append($row);
		}

		$("#modal-barang").modal("hide");
		$("#tbl_barang input[type='checkbox']").prop('checked', false);	
	}
	$("select[name='konversi[]']").select2();
	//Buat Nomor
	buat_no();
}

//Scan Barcoce
function scan_barcode(){
	var barcode = $("#barcode").val().trim();

	if(!barcode){
		return false;
	}

	$.ajax({
		url : baseurl+"gudang_order/scan_barcode",
		type : "post",
		dataType : "json",
		data : {barcode : barcode},
		success : function(msg){
			//clear barcode
			$("#barcode").val("");

			if(msg['type'] == 'success'){ //Sukses
				$("#barcode_input").removeClass("has-error").addClass("has-success");
				$("#barcode_msg").removeClass("has-red").addClass("text-green");
				$("#barcode_msg").text("Barcode : "+msg['barcode']+" ditemukan.");

				var addToList = function(){
					var pj = $("#tdet_order tr").length;
					var cek = false;
					//Cek jika item sudah ada pada daftar
					if(pj > 2){
						for (var j = 0; j <= pj - 1; j++) {
							var kode = $("#tdet_order tr:eq("+(j+1)+")").find("input[name='id_barang[]']").val();

							if(kode != "" && kode == msg['data'][0]['idbarang_bb']){
								var qty = parseFloat($("#tdet_order tr:eq("+(j+1)+")").find("input[name='qty[]']").val());

								$("#tdet_order tr:eq("+(j+1)+")").find("input[name='qty[]']").val(qty+1);
								//Trigger Change
								$("input[name='id_barang[]'][value='"+msg['data'][0]['idbarang_bb']+"']").closest("tr").find("input[name='qty[]']").trigger("change");
								
								cek = true;
								break;
							}

						}
						
					}

					if(cek == false){
						var opt_select = "";

						$.each(msg['data'][0]['konversi'], function(i,n){
							var selected = "";
							if(n['selected'] == 1)
							{
								selected = "selected";
							}

							opt_select += '<option value="'+n['id_konversi']+'" '+selected+'>'+n['tampil2']+'</option>';
						});

						var $row = $("<tr>"
				  					+"<td></td>"
				  					+"<td>"
				  						+msg['data'][0]['barcode']
				  						+"<input type='hidden' name='id_barang[]' value='"+msg['data'][0]['idbarang_bb']+"' />"
				  						+"<input type='hidden' name='id_satuan[]' value='"+msg['data'][0]['id_satuan_terkecil']+"' />"
				  					+"</td>"
				  					+"<td>"+msg['data'][0]['nm_barang']+" - <a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>"
				  					+"<td><input type='number' min='1' name='qty[]' class='form-control' value='1' /></td>"
				  					+"<td><select class='form-control' name='konversi[]' style='width: 200px'>"+opt_select+"</select></td>"
				  				+"</tr>");

						$("#tdet_order tbody").append($row);
					}

					$("#modal-barang").modal("hide");
					$("#tbl_barang input[type='checkbox']").prop('checked', false);
					
					//Buat Nomor
					buat_no();
				}

				addToList();

			}else{ //gagal
				$("#barcode_input").removeClass("has-success").addClass("has-error");
				$("#barcode_msg").removeClass("text-green").addClass("text-red");
				$("#barcode_msg").text("Barcode : "+msg['barcode']+" tidak ditemukan.");
			}
		}
	});
}

//Remove Item
function remove_item(obj){
	$(obj).closest("tr").animate({backgroundColor:'red'}, 1000).fadeOut(1000,function() {
	    $(obj).closest("tr").remove();
	});
}

//buat nomor
function buat_no(){
	var pj_table = $("#tdet_order tr").length;
	for (var i = 1; i <= pj_table-2; i++) {
		$("#tdet_order tr:eq("+i+") td:first").text(i);
	}
}