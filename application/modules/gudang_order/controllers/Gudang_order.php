<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for Gudang Order
 */

class Gudang_order extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Gudang Order.View";
    protected $addPermission    = "Gudang Order.Add";
    protected $managePermission = "Gudang Order.Manage";
    protected $deletePermission = "Gudang Order.Delete";

    protected $prefixKey        = "REQ";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('gudang_order/gudang_order');
        $this->load->model(array('gudang_order/gudang_order_model',
                                'gudang_order/gudang_order_detail_model',
                                'konversi_satuan/konversi_satuan_model',
                                'bahan_baku/bahan_baku_model',
                                'stok_model',
                                'gudang_order/gudang_model',
                                'gudang_order/po_model'
                                ));

        $this->template->title(lang('gudang_order_title_manage'));
		$this->template->page_icon('fa fa-envelope');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if (isset($_POST['delete']) && has_permission($this->deletePermission))
        {
            $checked = $this->input->post('checked');
            $jml_success = 0;
            if($checked)
            {
                foreach ($checked as $key => $pid) {
                    if($this->cek_hapus($pid))
                    {
                        $total      = $this->gudang_order_model->find($pid)->total;

                        $result      = $this->gudang_order_model->delete($pid);
                        $query_hapus = $this->db->last_query();
                        
                        if($result)
                        {
                            $keterangan = "SUKSES, hapus data Gudang Order dengan ID : ".$pid;
                            $status     = 1;
                            $jml_success++;
                        }
                        else
                        {
                            $keterangan = "GAGAL, hapus data Gudang Order dengan ID : ".$pid;
                            $status     = 0;
                        } 
                    }
                    else
                    {
                        $err = "karena telah dibuatkan PO";
                        $keterangan = "GAGAL, hapus data Gudang Order dengan ID : ".$pid;
                        $status     = 0;
                    }

                    $query_hapus = isset($query_hapus) ? $query_hapus : $this->db->last_query();

                    $nm_hak_akses   = $this->deletePermission; 
                    $kode_universal = $pid;
                    $jumlah         = isset($total) ? $total : 0;
                    $sql            = $query_hapus;

                    simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                }

                if ($jml_success > 0)
                {
                    $this->template->set_message($jml_success .' '. lang('gudang_order_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('gudang_order_del_failure') . (isset($err) ? $err." " : "") . $this->gudang_order_model->error, 'error');
                }
            }
            else
            { //No selected
                $this->template->set_message(lang('gudang_order_del_error'), 'error');
            }

            unset($_POST['delete']);
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search     = isset($_POST['table_search']) ? $this->input->post('table_search') :'';
            $tgl_awal   = isset($_POST['tgl_awal']) ? date_ymd($this->input->post('tgl_awal')) : '';
            $tgl_akhir  = isset($_POST['tgl_awal']) ? date_ymd($this->input->post('tgl_akhir')) : '';
        }
        else
        {
            $search     = isset($_GET['search']) ? $this->input->get('search') :'';
            $tgl_awal   = isset($_GET['tgl_awal']) ? date_ymd($this->input->get('tgl_awal')) : '';
            $tgl_akhir  = isset($_GET['tgl_awal']) ? date_ymd($this->input->get('tgl_akhir')) : '';
        }

        $filter = "?search=".$search;

        $addWhere = "";
        if($tgl_awal != '' && $tgl_akhir != '')
        {
            $filter   .= "&tgl_awal=".urlencode($tgl_awal)."&tgl_akhir=".urlencode($tgl_akhir);
            $addWhere .= " AND ( date(tanggal_kirim) >='".$tgl_awal."' AND date(tanggal_kirim) <='".$tgl_akhir."')";
        }
        else
        {
            $tgl_awal = date('Y-m-1');
            $tgl_akhir = date('Y-m-d');
            
            $filter   .= "&tgl_awal=".urlencode($tgl_awal)."&tgl_akhir=".urlencode($tgl_akhir);
            $addWhere .= " AND ( date(tanggal_kirim) >='".$tgl_awal."' AND date(tanggal_kirim) <='".$tgl_akhir."')";
        }

        $search2 = $this->db->escape_str($search);
        
        $where = "`gudang_order.deleted` = 0 $addWhere
                AND (`no_req` LIKE '%$search2%' ESCAPE '!'
                OR `tanggal_kirim` LIKE '%$search2%' ESCAPE '!'
                OR `nm_lengkap` LIKE '%$search2%' ESCAPE '!')";
        
        $total = $this->gudang_order_model
                    ->join("users","gudang_order.created_by = users.id_user","left")
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->gudang_order_model
                    ->select(array("gudang_order.*","nm_lengkap"))
                    ->join("users","gudang_order.created_by = users.id_user","left")
                    ->where($where)
                    ->order_by('tanggal_kirim','DESC')
                    ->limit($limit, $offset)->find_all();

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('tgl_awal', $tgl_awal);
        $this->template->set('tgl_akhir', $tgl_akhir);
        $this->template->set("numb", $offset+1);

        $assets = array('plugins/select2/css/select2.min.css',
                        'plugins/select2/css/select2-bootstrap.min.css',
                        'plugins/select2/js/select2.min.js',
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/number/jquery.number.js',
                        'gudang_order/assets/js/gudang_order.js'
                        );

        add_assets($assets);

        $this->template->title(lang('gudang_order_title_manage'));
        $this->template->render("index"); 
    }

    protected function cek_hapus($id_req)
    {
        if(!$id_req){
            return FALSE;
        }

        $cek = $this->po_model->join("po_permintaan","po_supplier.id_po=po_permintaan.id_po")
                            ->find_by(['id_req' => $id_req, 'deleted' => 0]);

        if(!$cek)
        {
            return TRUE;
        }
         
        return FALSE;
    }

    public function create()
    {
        $this->auth->restrict($this->addPermission);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_order())
            {
              $this->template->set_message(lang("gudang_order_create_success"), 'success');
              //Clear Session
              $this->session->unset_userdata('gudang_orders');
              redirect('gudang_order');
            }
        }

        //cek session data
        $data = $this->session->userdata('gudang_orders');
        $kode = $this->buat_kode($this->prefixKey);
        if(!$data)
        {
            $data = array('gudang_orders' => array(
                                        'no_req' => $kode,
                                        'items'     => array(),
                                        'tanggal_kirim' => ''
                                        ));

            $this->session->set_userdata($data);
            $data = $this->session->userdata('gudang_orders');
        }

        if($data['no_req'] != $kode)
        {
            $data['no_req'] = $kode;

            $this->session->set_userdata(array('gudang_orders' => $data));
            $data = $this->session->userdata('gudang_orders');
        }

        $this->template->set('data', $data);

        $assets = array('plugins/select2/css/select2.min.css',
                        'plugins/select2/css/select2-bootstrap.min.css',
                        'plugins/select2/js/select2.min.js',
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/number/jquery.number.js',
                        'gudang_order/assets/js/gudang_order.js'
                        );

        add_assets($assets);
        
        $this->template->title(lang('gudang_order_title_new'));
        $this->template->render("form");
    }

    public function edit($id = 0)
    {
        if(!$id)
        {
            $this->template->set_message(lang('gudang_order_invalid_id'),'error');
            redirect('gudang_order');
        }

        if(isset($_POST['save']))
        {
            if($this->save_status($id))
            {
                $this->template->set_message(lang('gudang_order_edit_success'), 'success');
            }
        }

        $assets = array('plugins/select2/css/select2.min.css',
                        'plugins/select2/css/select2-bootstrap.min.css',
                        'plugins/select2/js/select2.min.js',
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'gudang_order/assets/js/gudang_order.js'
                        );

        add_assets($assets);

        $data = $this->gudang_order_model->select("gudang_order.*, users.nm_lengkap")
                                        ->join("users","gudang_order.created_by = users.id_user")
                                        ->find($id);

        $this->template->set('data', $data);

        $this->template->title(lang('gudang_order_title_edit'));
        $this->template->render('edit_status');
    }

    protected function save_status($id)
    {
        $st_req = $this->input->post('st_req');

        $result = $this->gudang_order_model->update($id, array('st_req' => $st_req));

        if($result === FALSE)
        {
            $this->template->set_message(lang('gudang_order_edit_fail'),'error');

            $keterangan = "GAGAL, update status outlet order dengan ID : ".$id;
            $status     = 0;
            
            $return = FALSE;
        }
        else
        {
            $keterangan = "SUKSES, update status outlet order dengan ID : ".$id;
            $status     = 1;

            $return = TRUE;
        }

        $nm_hak_akses   = $this->managePermission; 
        $kode_universal = $id;
        $jumlah         = 1;
        $sql            = $this->db->last_query();
        //menyimpan ke Log histori
        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        return $return;
    }

    public function cancel()
    {
        $this->auth->restrict($this->addPermission);

        $this->session->unset_userdata('gudang_orders');
        $this->template->set_message(lang('gudang_order_canceled'), 'success');

        redirect('gudang_order');
    }

    protected function buat_kode($prefix = "REQ")
    {
        $dt_req     = $this->gudang_order_model
                                            ->order_by('created_on','DESC')
                                            ->limit(1)
                                            ->find_all();
        $last_kode = "";
        if($dt_req)
        {
            foreach ($dt_req as $key => $dr) {
                $last_kode = $dr->no_req;
            }
        }

        if($last_kode)
        {
            $tkode = explode("-", $last_kode);
            if(count($tkode) == 2)
            {
                $tkode = intval($tkode[1]);
            }
            else
            {
                $tkode = 0;
            }

            $kode = strtoupper($prefix)."-".($tkode+1);
        }
        else
        {
            $kode = strtoupper($prefix)."-1";
        }

        return $kode;
    }

    //View Detail
    public function view($id_req = 0)
    {
        $this->auth->restrict($this->viewPermission);

        if (!$id_req)
        {
            $this->template->set_message(lang("gudang_order_invalid_id"), 'error');
            redirect('gudang_order');
        }

        $data   = $this->gudang_order_model->find($id_req);

        $detail = $this->gudang_order_detail_model->select("gudang_order_detail.*, konversi_satuan.satuan_besar as satuan, barcode, nm_barang")
                                            ->join('barang','gudang_order_detail.id_barang = barang.idbarang_bb',"left")
                                            ->join('konversi_satuan','gudang_order_detail.id_konversi = konversi_satuan.id_konversi',"left")
                                            ->order_by('nm_barang','ASC')
                                            ->find_all_by('id_req', $id_req);

        $is_admin   = $this->auth->is_admin();

        $this->template->set('id_req', $id_req);
        $this->template->set('data', $data);
        $this->template->set('detail', $detail);
        $this->template->set('is_admin', $is_admin);

        $this->template->title(lang('gudang_order_title_view'));
        $this->template->render('view');
    }

    public function cari_barang()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('gudang_order_only_ajax'), 'error');
            redirect('gudang_order');
        }

        $cari = $this->db->escape_str($this->input->post('cr_barang'));

        $data = $this->bahan_baku_model->select(array("barang.*","satuan_terkecil.alias as satuan","nmkategori","nmjenis_bb"))
                                    ->join("satuan_terkecil","barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","left")
                                    ->join("kategori","barang.idkategori=kategori.idkategori","left")
                                    ->join("jenis_bb","barang.idjenis_bb=jenis_bb.idjenis_bb","left")
                                    ->where("`barang.deleted` = 0
                                            AND (barcode like '%$cari%' OR nm_barang like '%$cari%' OR nmkategori like '%$cari%' OR nmjenis_bb like '%$cari%')")
                                    ->order_by('nm_barang', 'ASC')
                                    ->find_all();
        if($data)
        {
            foreach ($data as $key => $dt) {
                $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
            }
        }

        echo json_encode($data);
    }

    public function scan_barcode()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('gudang_order_only_ajax'), 'error');
            redirect('gudang_order');
        }

        $barcode = $this->input->post('barcode');

        if($barcode != "")
        {
            $data = $this->bahan_baku_model->select(array("barang.*","satuan_terkecil.alias as satuan"))
                                        ->join("satuan_terkecil","barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","left")
                                        ->find_all_by(array('barang.deleted' => 0, 'barcode' => $barcode));
            if($data)
            {
                foreach ($data as $key => $dt) {
                    $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
                }

                $return = array('type' => 'success', 'barcode' => $barcode, 'data' => $data);
            }
            else
            {
                $return = array('type' => 'error', 'barcode' => $barcode, 'data' => 0);
            }
        }
        else
        {
            $return = array('type' => 'error', 'barcode' => $barcode, 'data' => 0);
        }

        echo json_encode($return);
    }

    //Simpan Gudang Order
    protected function save_order()
    {
        $no_req       = $this->input->post('no_req');
        $tanggal      = $this->input->post('tanggal_kirim');

        $id_barang  = $this->input->post('id_barang');
        $id_satuan  = $this->input->post('id_satuan');
        $konversi   = $this->input->post('konversi');
        $qty        = $this->input->post('qty');

        if(count($id_barang) == 0)
        {
            $this->template->set_message(lang('gudang_order_no_item'), 'error');
            return FALSE;
        }

        $this->form_validation->set_rules('no_req', 'lang:gudang_order_no_req', 'required');
        $this->form_validation->set_rules('tanggal_kirim', 'lang:gudang_order_tanggal_kirim', 'required');

        if ($this->form_validation->run() === FALSE) {

            //Masukkan item kedalam session
            $data = $this->session->userdata('gudang_orders');
            $items = $data['items'];

            $data_items = array();
            foreach ($id_barang as $key => $kode) {

                $dt_barang = $this->bahan_baku_model->find($kode);

                $data_items[] = array('id_barang'   => $kode,
                                    'barcode'       => $dt_barang->barcode,
                                    'nm_barang'     => $dt_barang->nm_barang,
                                    'id_konversi'   => $konversi[$key],
                                    'id_satuan_terkecil' => $id_satuan[$key],
                                    'konversi'      => get_konversi($id_satuan[$key]),
                                    'qty'           => $qty[$key],
                                );
            }

            $data['items']          = $data_items;
            $data['tanggal_kirim']  = $tanggal;

            $this->session->set_userdata('gudang_orders', $data);

            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        //Simpan Gudang Order
        $this->db->trans_start();

        //Hitung g total
        $gtotal = 0;

        foreach ($id_barang as $key => $kode) {
            $sub_total  = 0;
            $harga      = $this->get_hpp($kode);

            //Get real qty
            $dt_real_qty = $this->konversi_satuan_model->hitung_ke_satuan_kecil($konversi[$key], floatval($qty[$key]));
            $real_qty    = $dt_real_qty['qty'];
            
            if($real_qty != $qty[$key])
            {
                $sub_total = $harga * $real_qty;
            }
            else
            {
                $sub_total  = floatval($qty[$key]) * $harga;
            }

            $gtotal += $sub_total;
        }

        $data_req = array('no_req'      => $no_req,
                        'total'         => $gtotal,
                        'tanggal_kirim' => date_ymd($tanggal),
                        'st_req'        => 0
                    );

        $id_req = $this->gudang_order_model->insert($data_req);

        if ($id_req !== FALSE)
        {
            $keterangan = "SUKSES, tambah data Gudang Order, dengan ID : ".$no_req;
            $status     = 1;
        }
        else
        {
            $keterangan = "GAGAL, tambah data Gudang Order, dengan ID : ".$no_req;
            $status     = 0;
        }

        //Save Log
        $nm_hak_akses   = $this->addPermission; 
        $kode_universal = $no_req;
        $jumlah         = $gtotal;
        $sql            = $this->db->last_query();

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        $data_det_req = array();
        foreach ($id_barang as $key => $kode) {
            $sub_total  = 0;
            $harga      = $this->get_hpp($kode);

            //Get real qty
            $dt_real_qty = $this->konversi_satuan_model->hitung_ke_satuan_kecil($konversi[$key], floatval($qty[$key]));
            $real_qty    = $dt_real_qty['qty'];
            
            if($real_qty != $qty[$key])
            {
                $sub_total = $harga * $real_qty;
            }
            else
            {
                $sub_total  = floatval($qty[$key]) * $harga;
            }

            $gtotal += $sub_total;

            $data_det_req = array('id_detail_order' => gen_primary(),
                                'id_req'        => $id_req,
                                'id_barang'     => $kode,
                                'qty'           => $qty[$key],
                                'id_konversi'   => $konversi[$key],
                                'harga'         => $harga,
                                'sub_total'     => $sub_total
                            );

            $det_req = $this->gudang_order_detail_model->insert($data_det_req);
            if ($det_req !== FALSE)
            {
                $keterangan = "SUKSES, tambah data detail Gudang Order, dengan ID : ".$data_det_req['id_detail_order'];
                $status     = 1;
            }
            else
            {
                $keterangan = "GAGAL, tambah data detail Gudang Order, dengan ID : ".$data_det_req['id_detail_order'];
                $status     = 0;
            }
            //Save Log
            $nm_hak_akses   = $this->addPermission; 
            $kode_universal = $data_det_req['id_detail_order'];
            $jumlah         = $sub_total;
            $sql            = $this->db->last_query();

            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->template->set_message($this->gudang_order_model->error."\n\n".$this->gudang_order_detail_model->error, 'error');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    //Get HPP terakhir
    protected function get_hpp($id_barang)
    {
        if(!$id_barang)
        {
            return 0;
        }

        $hpp = $this->stok_model->get_hpp($id_barang, FALSE);

        return $hpp;
    }

}
?>