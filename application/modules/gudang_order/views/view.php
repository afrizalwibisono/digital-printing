<div class="box box-primary">
    <!-- form start -->
    <?= form_open("",array('id'=>'frm_detail_req','name'=>'frm_detail_req','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="form-group">
			    <div class="col-md-3">
			    	<label for="no_req" class="control-label"><?= lang('gudang_order_no_req') ?></label>
			    	<input type="text" class="form-control" id="no_req" name="no_req" value="<?= $data->no_req ?>" readonly>
			    </div>
			    <div class="col-md-3">
			    	<label for="tanggal_kirim" class="control-label"><?= lang('gudang_order_tanggal_kirim') ?></label>
			    	<input type="text" class="form-control" id="tanggal_kirim" name="tanggal_kirim" value="<?= date('d/m/Y', strtotime($data->tanggal_kirim)) ?>" readonly>
			    </div>
			     <div class="col-md-3">
			    	<label for="status" class="control-label"><?= lang('gudang_order_st_req') ?></label>
			    	<?php
			    		switch ($data->st_req) {
			    			case 0:
			    				$status = 'Belum diproses';
			    				break;
			    			case 1:
			    				$status = 'Telah diproses';
			    				break;
			    			case 2:
			    				$status = 'Dibatalkan';
			    				break;
			    			case 3:
			    				$status = 'Dibatalkan';
			    				break;
			    			default:
			    				$status = '-';
			    				break;
			    		}
			    	?>
			    	<input type="text" class="form-control" id="status" name="status" value="<?= $status ?>" readonly>
			    </div>
			    
		  	</div>
		  	<div class="table-responsive">
		  		<table class="table table-bordered" id="tdet_req">
		  			<thead>
		  				<tr class="success">
			  				<th width="50"><?= lang('gudang_order_no') ?></th>
			  				<th><?= lang('gudang_order_barcode') ?></th>
			  				<th><?= lang('gudang_order_nm_barang') ?></th>
			  				<th><?= lang('gudang_order_qty') ?></th>
			  				<th><?= lang('gudang_order_satuan') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($detail) : ?>
		  					<?php 
		  						$no = 1;
		  						foreach($detail as $key => $dt) : 
		  					?>
		  				<tr>
		  					<td><?= $no++ ?></td>
		  					<td><?= $dt->barcode ?></td>
		  					<td><?= $dt->nm_barang ?></td>
		  					<td><?= $dt->qty ?></td>
		  					<td><?= $dt->satuan ?></td>
		  				</tr>
		  					<?php endforeach; ?>
		  				<?php endif; ?>
		  			</tbody>
		  		</table>
		  	</div>
		  	<div class="form-group" style="margin-top: 15px;">
			    <div class="col-md-12">
			    	<?php
	                	echo anchor('gudang_order', lang('gudang_order_btn_back'), array('class' => 'btn btn-success'));
	                ?>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->