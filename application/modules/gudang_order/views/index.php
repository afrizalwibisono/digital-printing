<?php 
    $ENABLE_ADD     = has_permission('Gudang Order.Add');
    $ENABLE_MANAGE  = has_permission('Gudang Order.Manage');
    $ENABLE_DELETE  = has_permission('Gudang Order.Delete');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_req','name'=>'frm_req', 'class' => 'form-inline')) ?>
        <div class="box-header">
            <?php if ($ENABLE_ADD) : ?>
            <div class="pull-left">
                <a href="<?= site_url('gudang_order/create') ?>" class="btn btn-success" title="<?= lang('gudang_order_btn_new') ?>"><?= lang('gudang_order_btn_new') ?></a>
            </div>
            <?php endif; ?>
            <div class="pull-right">
                <div class="form-group">
                    <div class="input-daterange input-group">
                        <input type="text" class="form-control" name="tgl_awal" id="tgl_awal" value="<?= isset($tgl_awal) && $tgl_awal !='' ? date('d/m/Y', strtotime($tgl_awal)) : '' ?>" placeholder="<?= lang('gudang_order_tgl_awal') ?>" required />
                        <span class="input-group-addon text-black">to</span>
                        <input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?= isset($tgl_akhir) && $tgl_akhir !='' ? date('d/m/Y', strtotime($tgl_akhir)) : '' ?>" placeholder="<?= lang('gudang_order_tgl_akhir') ?>" required />
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="Search" autofocus />
                        <div class="input-group-btn">
                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
	<div class="box-body table-responsive">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
                    <th width="50">#</th>
                    <th><?= lang('gudang_order_no_req') ?></th>
                    <th><?= lang('gudang_order_dibuat_oleh') ?></th>
                    <th><?= lang('gudang_order_tanggal_kirim') ?></th>
                    <th><?= lang('gudang_order_st_req') ?></th>
                    <?php if($ENABLE_MANAGE) : ?>
                    <th></th>
                    <?php endif; ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($results as $record) : 
                    switch ($record->st_req) {
                        case 0:
                            $st_text = "<label class='label label-primary'>Belum diproses oleh Admin</label>";
                            break;
                        case 1:
                            $st_text = "<label class='label label-success'>Telah diproses oleh Admin</label>";
                            break;
                        case 2:
                            $st_text = "<label class='label label-danger'>Dibatalkan oleh Admin</label>";
                            break;
                        case 3:
                            $st_text = "<label class='label label-danger'>Dibatalkan oleh Gudang ".ucwords($record->nm_gudang)."</label>";
                            break;
                        default:
                            $st_text = "<label class='label label-warning'>Tidak diketahui</label>";
                            break;
                    }
                ?>
                <tr>
                    <td class="column-check"><input type="checkbox" name="checked[]" value="<?= $record->id_req ?>" /></td>
                    <td><?= $numb; ?></td>
                    <td><?= $record->no_req ?></td>
                    <td><?= ucwords($record->nm_lengkap) ?></td>
                    <td><?= date('d/m/Y', strtotime($record->tanggal_kirim)) ?></td>
                    <td><?= $st_text ?></td>
                    <?php if($ENABLE_MANAGE) : ?>
                    <td style="padding-right:20px">
                        <a class="text-black" href="<?= site_url('gudang_order/edit/'.$record->id_req) ?>" data-toggle="tooltip" data-placement="left" title="Edit Status"><i class="fa fa-pencil"></i></a>
                        &nbsp;|&nbsp;
                        <a class="text-black" href="<?= site_url('gudang_order/view/' . $record->id_req); ?>" data-toggle="tooltip" data-placement="left" title="View Detail"><i class="fa fa-folder"></i></a>
                    </td>
                    <?php endif; ?>
                </tr>
                <?php $numb++; endforeach; ?>
            </tbody>
	    </table>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">
		<?php if($ENABLE_DELETE) : ?>
		<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('gudang_order_btn_delete') ?>" onclick="return confirm('<?= lang('gudang_order_delete_confirm') ?>')">
		<?php endif;
		echo $this->pagination->create_links(); 
		?>
	</div>
	<?php else: ?>
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('gudang_order_no_records_found') ?></p>
    </div>
    <?php
	endif;
	echo form_close(); 
	?>
</div><!-- /.box -->