<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_order','name'=>'frm_order','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="form-group <?= form_error('no_req') ? ' has-error' : ''; ?>">
			    <div class="col-md-3">
			    	<label for="no_req" class="control-label"><?= lang('gudang_order_no_req') ?></label>
			    	<input type="text" class="form-control" id="no_req" name="no_req" maxlength="50" value="<?= $data['no_req'] ?>" readonly>
			    </div>
			    <div class="col-md-3">
			    	<label for="tanggal_kirim" class="control-label"><?= lang('gudang_order_tanggal_kirim') ?></label>
			    	<input type="text" class="form-control" id="tanggal_kirim" name="tanggal_kirim" maxlength="10" value="<?= set_value('tanggal_kirim', isset($data['tanggal_kirim']) ? $data['tanggal_kirim'] : '') ?>">
			    </div>
		  	</div>
		  	<div class="table-responsive">
		  		<table class="table table-bordered" id="tdet_order">
		  			<thead>
		  				<tr class="success">
			  				<th width="50"><?= lang('gudang_order_no') ?></th>
			  				<th><?= lang('gudang_order_barcode') ?></th>
			  				<th><?= lang('gudang_order_nm_barang') ?></th>
			  				<th><?= lang('gudang_order_qty') ?></th>
			  				<th><?= lang('gudang_order_satuan') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($data['items']) : ?>
		  					<?php 
		  						$no = 1;
		  						foreach($data['items'] as $key => $dt) : 
		  					?>
		  				<tr>
		  					<td><?= $no++ ?></td>
		  					<td>
		  						<?= $dt['barcode'] ?>
		  						<input type="hidden" name="id_barang[]" value="<?= $dt['id_barang'] ?>" />
		  						<input type="hidden" name="id_satuan[]" value="<?= $dt['id_satuan_terkecil'] ?>" />
		  					</td>
		  					<td><?= $dt['nm_barang'] ?> - <a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>
		  					<td><input type="number" min="1" name="qty[]" class="form-control" value="<?= $dt['qty'] ?>" onchange="hitung_total(this)" /></td>
		  					<td>
		  						<select class="form-control" name="konversi[]" >
		  							<?php 
		  								if($dt['konversi']) :
		  									foreach ($dt['konversi'] as $key => $kf) :
		  							?>
		  							<option value="<?= $kf->id_konversi ?>" <?= (isset($kf->id_konversi) && $kf->id_konversi == $dt['id_konversi']) ? "selected" : "" ?>><?= $kf->satuan_besar ?></option>
		  							<?php
		  									endforeach; 
		  								endif; 
		  							?>
		  						</select>
		  					</td>
		  				</tr>
		  					<?php endforeach; ?>
		  				<?php endif; ?>
		  			</tbody>
		  			<tfoot>
		  				<tr>
		  					<td colspan="6">
		  						<div class="input-group" id="barcode_input">
		  							<div class="input-group-btn">
		  								<button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-barang"><i class="fa fa-plus-square"></i></button>
		  							</div>
		  							<input type="text" class="form-control" id="barcode" name="barcode" placeholder="<?= lang('gudang_order_scan_brg') ?>" autocomplete="off" autofocus />
		  						</div>
		  						<span class="help-block text-green" id="barcode_msg"></span>
		  					</td>
		  				</tr>
		  			</tfoot>
		  		</table>
		  	</div>
		  	<div class="form-group" style="margin-top: 15px;">
			    <div class="col-md-12">
			      	<button type="submit" name="save" class="btn btn-primary"><?= lang('gudang_order_btn_send') ?></button>
			    	<?php
	                	echo ' atau ' . anchor('gudang_order/cancel', lang('gudang_order_btn_cancel'), array("onclick" => "return confirm('".lang('gudang_order_cancel_confirm')."')"));
	                ?>
			    </div>
		  	</div>
	  	</div>
	<?= form_close() ?>
</div><!-- /.box -->
<div class="modal" id="modal-barang" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
	    <div class="modal-content">
	    	<?= form_open("",array('id'=>'frm_barang','name'=>'frm_barang','role'=>'form')) ?>
		      	<div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        	<h4 class="modal-title"><?= lang('gudang_order_pilih_brg') ?></h4>
		      	</div>
		      	<div class="modal-body">
		      		<div class="form-group">
		      			<div class="input-group">
		      				<input type="text" class="form-control" id="cr_barang" name="cr_barang" placeholder="<?= lang('gudang_order_cari_brg') ?>" />
		      				<div class="input-group-btn">
		      					<button type="button" class="btn btn-primary" name="cari_btn" id="cari_btn"><i class="fa fa-search"></i> <?= lang('gudang_order_cari_btn') ?></button>
		      				</div>
		      			</div>
		      		</div>
		        	<div class="table-responsive">
		        		<table id="tbl_barang" class="table table-hover table-striped">
		        			<thead>
		        				<tr class="success">
			        				<th><input type="checkbox" id="ck_all" name="ck_all" /></th>
			        				<th><?= lang('gudang_order_no') ?></th>
			        				<th><?= lang('gudang_order_barcode') ?></th>
			        				<th><?= lang('gudang_order_nm_barang') ?></th>
			        				<th><?= lang('gudang_order_kategory') ?></th>
			        				<th><?= lang('gudang_order_jenis') ?></th>
			        			</tr>
		        			</thead>
		        			<tbody>
		        				
		        			</tbody>
		        		</table>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
		      		<button type="button" class="btn btn-primary" id="pilih_btn" name="pilih_btn"><?= lang('gudang_order_pilih_btn') ?></button>
		      	</div>
	      	<?= form_close() ?>
	    </div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" src="<?= base_url('assets/plugins/jquery-ui/jquery-ui.js') ?>"></script>