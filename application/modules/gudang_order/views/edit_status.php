<?php
	$addPembelian = "Pembelian.Add";
?>
<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_edit_status','name'=>'frm_edit_status','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="form-group">
				<label for="no_req" class="control-label col-md-2"><?= lang('gudang_order_no_req') ?></label>
			    <div class="col-md-3">
			    	<input type="text" class="form-control" id="no_req" name="no_req" value="<?= $data->no_req ?>" readonly>
			    </div>
			</div>
			<div class="form-group">
				<label for="tanggal_kirim" class="control-label col-md-2"><?= lang('gudang_order_tanggal_kirim') ?></label>
			    <div class="col-md-3">
			    	<input type="text" class="form-control" id="tanggal_kirim" name="tanggal_kirim" value="<?= date('d/m/Y', strtotime($data->tanggal_kirim)) ?>" readonly>
			    </div>
		  	</div>
			<div class="form-group">
				<label for="dibuat_oleh" class="control-label col-md-2"><?= lang('gudang_order_dibuat_oleh') ?></label>
			    <div class="col-md-3">
			    	<input type="text" class="form-control" id="dibuat_oleh" name="dibuat_oleh" value="<?= ucwords($data->nm_lengkap) ?>" readonly>
			    </div>
		  	</div>
		  	<div class="form-group">
				<label for="st_req" class="control-label col-md-2"><?= lang('gudang_order_st_req') ?></label>
			    <div class="col-md-3">
			    	<?php if(($data->st_req == 1 || $data->st_req == 2)) : ?>
			    	<input type="text" name="st_req" class="form-control" value="<?= $data->st_req == 1 ? 'Telah diproses oleh Admin' : 'Dibatalkan oleh Admin' ?>" disabled />
			    	<?php else : ?>
			    	<select class="form-control" id="st_req" name="st_req" <?= ($data->st_req == 1 || $data->st_req == 2) ? "disabled" : "" ?>>
			    		<option value="0" <?= $data->st_req == 0 ? 'selected' : '' ?>>Belum diproses</option>
			    		<?php if(has_permission($addPembelian)): ?>
			    		<option value="1" <?= $data->st_req == 1 ? 'selected' : '' ?>>Telah diproses</option>
			    		<option value="2" <?= $data->st_req == 2 ? 'selected' : '' ?>>Dibatalkan</option>
			    		<?php else : ?>
			    		<option value="3" <?= $data->st_req == 3 ? 'selected' : '' ?>>Dibatalkan</option>
			    		<?php endif; ?>
			    	</select>
			    	<?php endif; ?>	
			    </div>
		  	</div>
		  	<div class="form-group">
			    <div class="col-md-12 col-md-offset-2">
			    	<?php if(($data->st_req != 1 && $data->st_req != 2)) : ?>
			      	<button type="submit" name="save" class="btn btn-primary"><?= lang('gudang_order_btn_save') ?></button>
			    	&nbsp;atau&nbsp;
			    	<?php endif; ?>
			    	<?php
	                	echo anchor('gudang_order', lang('gudang_order_btn_back'), array('class' => 'btn btn-success'));
	                ?>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->