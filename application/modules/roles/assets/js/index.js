function update_default(obj){
	var id = $(obj).data("id");

	var conf = confirm("Apakah anda yakin akan mengubah default golongan ?");

	if(conf){
		$.ajax({
			url : siteurl+"roles/update_default",
			type: "post",
			dataType : "json",
			data : {id : id},
			success : function(data){
				if(data['type'] == 'success'){
	            	
	            	toastr.options.onHidden = function(){ 
	            		window.location.reload(); 
	            	}

	            	toastr.options.timeOut = 2000;

	            	toastr.clear();
			        Command: toastr['success'](data['msg'], 'Success');
	            	
	            }else{
	            	toastr.clear();
			        Command: toastr['error'](data['msg'], 'Error !');
			        return false;
	            }
			}
		});
	}
}