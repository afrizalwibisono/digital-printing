<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_role','name'=>'frm_role','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="form-group <?= form_error('nm_group') ? ' has-error' : ''; ?>">
			    <label for="nm_group" class="col-md-2 control-label"><?= lang('roles_group_name') ?></label>
			    <div class="col-sm-3">
			    	<input type="text" class="form-control" id="nm_group" name="nm_group" maxlength="20" value="<?= set_value('nm_group', isset($data->nm_group) ? $data->nm_group : ''); ?>" />
			    </div>
		  	</div>
		  	<div class="form-group <?= form_error('ket') ? ' has-error' : ''; ?>">
			    <label for="ket" class="col-md-2 control-label"><?= lang('roles_desc') ?></label>
			    <div class="col-md-4">
			    	<textarea type="text" class="form-control" id="ket" name="ket" maxlength="100"><?= set_value('ket', isset($data->ket) ? $data->ket : ''); ?></textarea>
			    </div>
		  	</div>
	  	</div>
	  	<div class="box-body">
	  		<div class="table-responsive">
	  			<h4><?= lang('roles_facility_title') ?></h4>
	  			<?php if($data->id_group == 1) : ?>
	  			<div class="checkbox">
		  			<label>
		  				<input type="checkbox" checked="" disabled=""> <?= lang('roles_full_access') ?>
		  			</label>
	  			</div>
	  			<?php else: ?>
	  			<table class="table table-bordered table-condended">
	  				<tr class="bg-success">
	  					<th>#</th>
	  					<th><?= lang('roles_facility_name') ?></th>
	  					<?php foreach ($header as $key => $hd) : ?>
	  					<th class="text-center"><?= $hd ?></th>
	  					<?php endforeach ?>
	  				</tr>
	  				<?php 
	  					$no = 1;
	  					foreach ($permissions as $key => $pr) : 
	  				?>
	  				<tr>
	  					<td><?= $no ?></td>
	  					<td><?= $key ?></td>
	  					<?php foreach ($pr as $key2 => $action) : ?>
	  					<td class="text-center">
	  						<?php if($action == 0) : ?>
	  						-
	  						<?php else : ?>
	  						<input type="checkbox" name="id_permissions[]" value="<?= $action['perm_id'] ?>" title="<?= $action['action_name'] ?>" <?= ($action['value'] == 1) ? "checked='checked'" : '' ?> />
	  						<?php endif ?>
	  					</td>
	  					<?php endforeach ?>	
	  				</tr>
	  				<?php $no++;endforeach ?>
	  			</table>
	  			<?php endif; ?>
	  		</div>
	  	</div>
	  	<div class="box-footer">
	  		<button type="submit" name="save" class="btn btn-primary"><i class="fa fa-save"></i> <?= lang('roles_btn_save') ?></button>
	  		<?php
            	echo ' atau ' . anchor('roles', lang('roles_btn_cancel'));
            ?>
	  	</div>
	<?= form_close() ?>
</div><!-- /.panel -->