<div class="box box-primary">
	<div class="box-body table-responsive">
		<table class="table">
			<thead>
				<tr class="success">
					<th width="50">#</th>
					<th><?= lang('roles_group_name') ?></th>
					<th><?= lang('roles_desc') ?></th>
					<th><?= lang('roles_default') ?></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php if($data) : 
					foreach ($data as $key => $dt) :?>
				<tr>
					<td><?= $key+1 ?></td>
					<td><?= $dt->nm_group ?></td>
					<td><?= $dt->ket ?></td>
					<td>
						<?php if($dt->st_default == 1) : ?>
						<span class="text-green btn-st-default" data-toggle="tooltip" data-placement="auto" title="Default group untuk user baru"><i class="fa fa-check"></i></span>
						<?php else : ?>
						<span class="text-red btn-st-default" data-id="<?= $dt->id_group ?>" onclick="update_default(this)" data-toggle="tooltip" data-placement="auto" title="Buat sebagai default group untuk user baru"><i class="fa fa-ban"></i></span>
						<?php endif; ?>
					</td>
					<td><a href="<?= site_url('roles/edit/'.$dt->id_group) ?>" data-toggle="tooltip" data-placement="auto" title="Edit"><i class="fa fa-pencil"></i></a></td>
				</tr>
			<?php endforeach;endif; ?>
			</tbody>
		</table>
	</div>
</div>