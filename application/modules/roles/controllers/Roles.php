<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Suwito
 * @copyright Copyright (c) 2017, Suwito
 * 
 * This is controller for Users Management
 */

class Roles extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */

    //Permissions
    protected $viewPermission   = "Roles.View";
    protected $managePermission = "Roles.Manage";

    public function __construct()
    {
    	parent::__construct();
        $this->lang->load('roles');
        $this->load->model(array('roles/groups_model',
                                'roles/group_permissions_model',
                                'roles/permissions_model',
                                ));

        $this->template->page_icon('fa fa-users');
        
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        $data = $this->groups_model->find_all();

        $assets = array('js/plugins/toastr/toastr.css',
                        'js/plugins/toastr/toastr.js',
                        'js/plugins/toastr/toastr_option.js',
                        'roles/assets/js/index.js'
                        );

        add_assets($assets);

        $this->template->set('data', $data);

        $this->template->title(lang('roles_title'));
        $this->template->render('index'); 
       
    }

    public function edit($id = 0)
    {
        $this->auth->restrict($this->managePermission);

        if($id == 0 || is_numeric($id) == FALSE)
        {
            $this->template->set_message(lang('users_invalid_id'), 'error');
            redirect('roles');
        }

        if(isset($_POST['save']))
        {
            if($this->save_permission($id))
            {
                $this->template->set_message(lang('roles_edit_success'),'success');
            }
        }

        $data = $this->groups_model->find($id);

        if(!$data)
        {
            $this->template->set_message(lang('users_invalid_id'), 'error');
            redirect('roles');
        }

        //All Permission
        $permissions = $this->permissions_model
                                            ->order_by("nm_permission","ASC")
                                            ->find_all();
        $auth_permissions = $this->get_auth_permission($id);
        
        $rows   = array();
        $header = array();
        $tmp    = array();
        if($permissions)
        {
            //table Header
            foreach ($permissions as $key => $pr) {
                $x = explode(".", $pr->nm_permission);
                if(!in_array($x[1], $header))
                {
                    $header[] = $x[1];
                    $tmp[$x[1]] = 0;
                }
            }
            //Temporary value
            foreach ($permissions as $key2 => $pr) {
                $x = explode(".", $pr->nm_permission);
                $rows[$x[0]] = $tmp;
            }
            //Actual value
            foreach ($permissions as $key3 => $pr) {
                $x = explode(".", $pr->nm_permission);
                //Rows
                $rows[$x[0]][$x[1]] = array('perm_id' => $pr->id_permission,'action_name' => $x[1], 'is_role_permission' => (isset($auth_permissions[$pr->id_permission]->is_role_permission) && $auth_permissions[$pr->id_permission]->is_role_permission == 1) ? 1 : '','value' => (isset($auth_permissions[$pr->id_permission]) ? 1 : 0));
            }
        }

        $this->template->set('data', $data);
        $this->template->set('header', $header);
        $this->template->set('permissions', $rows);

        $this->template->title(lang('roles_edit_title'));
        $this->template->page_icon('fa fa-users');
        $this->template->render('role_permissions');
    }

    protected function get_auth_permission($id_group = 0)
    {
        $role_permissions = $this->groups_model->select("permissions.*")
                                            ->join("group_permissions","groups.id_group = group_permissions.id_group")
                                            ->join("permissions","group_permissions.id_permission = permissions.id_permission")
                                            ->where("groups.id_group", $id_group)
                                            ->find_all();

        $merge = array();
        if($role_permissions)
        {
            foreach ($role_permissions as $key => $rp) {
                if(!isset($merge[$rp->id_permission]))
                {
                    $rp->is_role_permission = 1;
                    $merge[$rp->id_permission] = $rp;
                }
            }
        }

        return $merge;
    }

    protected function save_permission($id_group = 0)
    {
        $this->form_validation->set_rules('nm_group', 'lang:roles_group_name','required|trim');
        $this->form_validation->set_rules('ket', 'lang:roles_desc','trim');

        if($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        if($id_group == 0 || $id_group == "")
        {
            $this->template->set_message(lang('users_invalid_id'), 'error');
            return FALSE;
        }

        $nm_group   = $this->input->post('nm_group');
        $ket        = $this->input->post('ket');
        $id_permissions = $this->input->post('id_permissions');

        //Save group Data
        $group_data = array(
                    'nm_group'  => $nm_group,
                    'ket'       => $ket
                    );
        
        $result = $this->groups_model->update($id_group, $group_data);

        $insert_data = array();
        if($id_permissions)
        {
            foreach ($id_permissions as $key => $idp) {
                $insert_data[] = array(
                                'id_group' => $id_group,
                                'id_permission' => $idp
                                );
            }
        }

        //Delete Fisrt All Previous user permission
        $this->group_permissions_model->delete_where(array('id_group' => $id_group));

        //Insert New one
        if($insert_data)
        {
            $result = $this->group_permissions_model->insert_batch($insert_data);
        }

        if($result === FALSE)
        {
            $this->template->set_message(lang('roles_edit_failed'), 'error');
            return FALSE;
        }

        unset($_POST['save']);

        return $result;
    }

    public function update_default()
    {
        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('roles_only_ajax'), 'error');
            redirect('roles');
        }

        $id = $this->input->post('id');

        $this->db->query("UPDATE groups set st_default = 0 WHERE id_group != '".$id."'");

        $update = $this->groups_model->update($id, array('st_default' => 1));

        if($update)
        {
            $return = array('type' => 'success', 'msg' => lang('roles_edit_success'));
        }
        else
        {
            $return = array('type' => 'error', 'msg' => lang('roles_edit_failed'));   
        }

        echo json_encode($return);
    }
}