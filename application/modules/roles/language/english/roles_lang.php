<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['roles_title'] 		= 'User Groups';
$lang['roles_edit_title'] 	= 'Edit User Group';
$lang['roles_manage_title'] = 'Users';
$lang['roles_facility_title'] = 'Modules';
$lang['roles_full_access']  = 'Full Access';
$lang['roles_facility_name'] = 'Module Name';

$lang['roles_group_name'] 	= 'Group Name';
$lang['roles_desc'] 		= 'Description';
$lang['roles_default'] 		= 'Dafault Group';

$lang['roles_btn_save'] 	= 'Save';
$lang['roles_btn_cancel'] 	= 'Cancel';
$lang['roles_btn_delete'] 	= 'Delete';


$lang['roles_invalid_id'] 	= 'Invalid ID';
$lang['roles_edit_success'] = 'User Group saved successfully';
$lang['roles_edit_failed']  = 'User Group saving failed';
$lang['roles_only_ajax']    = 'Only ajax request allowed';