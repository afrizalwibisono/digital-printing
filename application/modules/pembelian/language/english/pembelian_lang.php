<?php defined('BASEPATH') || exit('No direct script access allowed');

// ============================
$lang['pembelian_po_title_manage'] 	= 'PO [Purchase Order]';
$lang['pembelian_title_manage'] 	= 'Pembelian';
$lang['pembelian_po_title_new'] 	= 'PO Baru';
$lang['pembelian_title_new'] 		= 'Pembelian Baru';
$lang['pembelian_po_title_view'] 	= 'PO [Purchase Order]';
$lang['pembelian_title_view'] 		= 'Detail Pembelian';
$lang['pembelian_laporan_po_title_view'] = 'Laporan PO';
$lang['pembelian_laporan_title_view'] 	 = 'Laporan Pembelian';
$lang['pembelian_laporan_hutang_title_view'] = 'Laporan Hutang';

// form/table
$lang['pembelian_no_faktur'] 	= 'No. Nota/Surat Jalan/Faktur';
$lang['pembelian_faktur_beli'] 	= 'Faktur Pembelian';
$lang['pembelian_faktur_pajak'] = 'Faktur Pajak';
$lang['pembelian_no_po'] 	= 'Kode PO';
$lang['pembelian_tanggal_beli'] = 'Tanggal Pembelian';
$lang['pembelian_tanggal'] 	= 'Tanggal Transaksi';
$lang['pembelian_tanggal_kirim'] = 'Tanggal Kirim';
$lang['pembelian_total'] 		 = 'Total';
$lang['pembelian_nm_supplier'] 	 = 'Nama Supplier';
$lang['pembelian_type'] 		 = 'Tipe Pembelian';
$lang['pembelian_req_code'] 	 = 'Kode Permintaan';

$lang['pembelian_kategori_pencarian'] = 'Kategori Pencarian';

$lang['pembelian_st_lunas']   	 = 'Status Lunas';
$lang['pembelian_tempo'] 		 = 'Tempo';
$lang['pembelian_cash'] 		 = 'Cash';
$lang['pembelian_konsinyasi'] 	 = 'Konsinyasi';
$lang['pembelian_lunas'] 	 	 = 'Lunas';
$lang['pembelian_belum_lunas'] 	 = 'Belum Lunas';
$lang['pembelian_st_pajak'] 	 = 'Status Pajak';
$lang['pembelian_berpajak'] 	 = 'Berpajak';
$lang['pembelian_tdk_berpajak']  = 'Tidak Berpajak';
$lang['pembelian_bonus']  		 = '(Bonus)';
$lang['pembelian_include_pajak'] = 'Harga sudah termasuk pajak';
$lang['pembelian_st_po'] 		 = 'Status PO';
$lang['pembelian_st_open'] 		 = 'Aktif';
$lang['pembelian_st_closed'] 	 = 'Terpenuhi';

$lang['pembelian_tempo_hari'] 	 = 'Jatuh Tempo (Hari)';
$lang['pembelian_tempo_tanggal'] = 'Jatuh Tempo (Tgl)';
$lang['pembelian_jatuh_tempo'] 	 = 'Jatuh Tempo';

$lang['pembelian_range_tgl'] = 'Range Tanggal';
$lang['pembelian_range_tgl2'] = 'Range Tanggal Tempo';
$lang['pembelian_tgl_awal']  = 'Tanggal awal';
$lang['pembelian_tgl_akhir'] = 'Tanggal akhir';

$lang['pembelian_placeholder_cari'] = 'Pencarian';
$lang['pembelian_kode_placeholder'] = 'ketik kode dan tekan Enter';
$lang['pembelian_semua_tipe'] = 'Semua Tipe';

$lang['pembelian_no'] 		= '#';
$lang['pembelian_barcode'] 	= 'Barcode';
$lang['pembelian_artikel'] 	= 'Artikel';
$lang['pembelian_nm_barang'] = 'Nama Barang';
$lang['pembelian_nm_merk']  = 'Merek';
$lang['pembelian_qty'] 		= 'Qty';
$lang['pembelian_terpenuhi']= 'Terpenuhi';
$lang['pembelian_qty_beli'] = 'Qty Beli';
$lang['pembelian_harga'] 	= 'Harga(Rp.)';
$lang['pembelian_satuan'] 	= 'Satuan';
$lang['pembelian_total'] 	= 'Total(Rp.)';
$lang['pembelian_diskon'] 	= 'Diskon';
$lang['pembelian_potongan'] = 'Potongan';
$lang['pembelian_pajak'] 	= 'Pajak';
$lang['pembelian_gtotal'] 	= 'Grand Total (Rp.): ';
$lang['pembelian_terbayar'] = 'Terbayar(Rp.)';
$lang['pembelian_sisa_hutang'] = 'Sisa(Rp.)';

$lang['pembelian_daftar_beli'] 		= 'Daftar Pembelian';
$lang['pembelian_ringkasan'] 		= 'Ringkasan';
$lang['pembelian_detail'] 			= 'Detail Transaksi';
$lang['pembelian_total_tran_qty'] 	= 'Transaksi';
$lang['pembelian_total_tran'] 		= 'Total Seluruh Transaksi';
$lang['pembelian_kali'] 			= 'kali';
$lang['pembelian_po_pengirim'] 		= 'Dibuat oleh';

$lang['pembelian_operator'] = 'Operator';
$lang['pembelian_cetak'] 	= 'Cetak';

$lang['pembelian_pilih_brg'] = 'Pilih Barang';
$lang['pembelian_scan_brg']  = 'Ketik / Scan Barcode Barang';
$lang['pembelian_cari_brg']  = 'Cari Barang ...';
$lang['pembelian_cari_btn']  = 'Cari';
$lang['pembelian_pilih_btn'] = 'Pilih';

// button
$lang['pembelian_btn_new'] 		= 'Baru';
$lang['pembelian_btn_delete'] 	= 'Hapus';
$lang['pembelian_btn_save'] 	= 'Simpan';
$lang['pembelian_btn_cancel'] 	= 'Batal';
$lang['pembelian_btn_back'] 	= 'Kembali';
$lang['pembelian_or'] 			= 'atau';

// messages
$lang['pembelian_del_po_error']		= 'Anda belum memilih PO yang akan dihapus.';
$lang['pembelian_del_error']		= 'Anda belum memilih faktur pembelian yang akan dihapus.';
$lang['pembelian_del_po_failure']	= 'Tidak dapat menghapus PO : ';
$lang['pembelian_del_failure']		= 'Tidak dapat menghapus faktur pembelian: ';
$lang['pembelian_delete_po_confirm']= 'Apakah anda yakin akan menghapus PO terpilih ?';
$lang['pembelian_delete_confirm']	= 'Apakah anda yakin akan menghapus faktur pembelian terpilih ?';
$lang['pembelian_cancel_confirm']	= 'Apakah anda yakin akan membatalkan transaksi pembelian ?';
$lang['pembelian_cancel_po_confirm']= 'Apakah anda yakin akan membatalkan PO baru ?';
$lang['pembelian_deleted_po']		= 'PO berhasil dihapus';
$lang['pembelian_deleted']			= 'Data Pembelian berhasil dihapus';
$lang['pembelian_no_records_found'] = 'Data tidak ditemukan.';

$lang['pembelian_create_failure'] 	= 'Pembelian baru gagal disimpan: ';
$lang['pembelian_create_po_failure'] = 'PO baru gagal disimpan: ';
$lang['pembelian_create_success'] 	= 'Pembelian baru berhasil disimpan';
$lang['pembelian_create_po_success'] = 'PO baru berhasil disimpan';
$lang['pembelian_canceled'] 			= 'Pembelian telah berhasil dibatalkan';
$lang['pembelian_po_canceled'] 			= 'PO baru telah berhasil dibatalkan';

$lang['pembelian_edit_success'] 		= 'Pembelian berhasil disimpan';
$lang['pembelian_invalid_id'] 		= 'ID Tidak Valid';

$lang['pembelian_no_item'] 			= 'Anda belum menambahkan data barang yang akan dimasukkan dalam PO.';
$lang['pembelian_no_item_beli'] 	= 'Anda belum menambahkan data barang yang akan dimasukkan dalam Pembelian.';
$lang['pembelian_qty_nol'] 			= 'Qty baris ke: %s tidak boleh kurang atau sama dengan 0 (nol).';
$lang['pembelian_harga_nol'] 		= 'Harga baris ke: %s tidak boleh kurang atau sama dengan 0 (nol).';

$lang['pembelian_barcode_found'] 	= 'Barcode : %s ditemukan.';
$lang['pembelian_barcode_not_found'] = 'Barcode : %s tidak ditemukan.';

$lang['pembelian_only_ajax'] 		= 'Hanya ajax request yang diijinkan.';

$lang['pembelian_date_required'] 	= 'Silahkan pilih tanggal awal dan akhir.';
$lang['pembelian_po_not_found'] 	= 'Kode PO tidak ditemukan';
$lang['pembelian_st_po_success'] 	= 'Status PO berhasil diubah';
$lang['pembelian_st_po_failed'] 	= 'Status PO gagal diubah';
$lang['pembelian_st_po_closed'] 	= 'Maaf, PO telah ditutup, mohon periksa kembali';

//Retur
$lang['pembelian_kode_retur'] 	 	= 'Kode Retur';
$lang['pembelian_tgl_retur'] 	 	= 'Tanggal Retur';
$lang['pembelian_total_retur'] 	 	= 'Total';
$lang['pembelian_deleted_retur']	= 'Retur Pembelian berhasil dihapus';
$lang['pembelian_del_error_retur']	= 'Anda belum memilih retur pembelian yang akan dihapus.';
$lang['pembelian_del_failure_retur']= 'Tidak dapat menghapus retur pembelian : ';
$lang['pembelian_title_manage_retur'] 	= 'Retur Pembelian';
$lang['pembelian_title_new_retur'] 		= 'Retur Pembelian Baru';
$lang['pembelian_title_view_retur'] 	= 'Detail Retur Pembelian';
$lang['pembelian_qty_retur'] 		= 'Qty Retur';
$lang['pembelian_retur_sebelumnya'] = 'Retur Sebelumnya';
$lang['pembelian_st_retur'] 		= 'Status Retur';
$lang['pembelian_cancel_confirm_retur']	= 'Apakah anda yakin akan membatalkan retur pembelian ?';
$lang['pembelian_ganti_barang']	= 'Ganti barang';
$lang['pembelian_potong_nota']	= 'Potong nota';
$lang['pembelian_retur_failure'] 	= 'Retur pembelian baru gagal disimpan: ';
$lang['pembelian_retur_success'] 	= 'Retur pembelian baru berhasil disimpan';
$lang['pembelian_retur_no_fill'] 	= 'Anda belum menginputkan salah satu/beberapa barang yang akan diretur.';
$lang['pembelian_qty_retur_over'] 	= 'Qty retur baris ke: %s melebihi batas maksimal yang dapat diretur.';
$lang['pembelian_delete_confirm_retur']	= 'Apakah anda yakin akan menghapus retur pembelian terpilih ?';
$lang['pembelian_retur_canceled'] 	= 'Retur baru telah dibatalkan';
$lang['pembelian_faktur_success'] 	= 'Faktur Pajak berhasil disimpan';
$lang['pembelian_faktur_failed'] 	= 'Faktur Pajak gagal disimpan';
$lang['pembelian_no_permintaan_selected'] 	= 'Maaf, Anda belum memilih Kode Permintaan';