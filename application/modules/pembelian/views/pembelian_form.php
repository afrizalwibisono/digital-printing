<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_pembelian','name'=>'frm_pembelian','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
    		<div class="form-group">
    			<div class="col-md-3">
			    	<label for="no_faktur" class="control-label"><?= lang('pembelian_no_faktur') ?></label>
			    	<input type="text" class="form-control" id="no_faktur" name="no_faktur" maxlength="50" value="<?= set_value('no_faktur', isset($data['no_faktur']) ? $data['no_faktur'] : '' ) ?>" placeholder="<?= lang('pembelian_kode_placeholder') ?>">
			    	<input type="hidden" id="id_kedatangan" name="id_kedatangan" value="<?= set_value('id_kedatangan', isset($data['id_kedatangan']) ? $data['id_kedatangan'] : '' ) ?>">
			    </div>
			    <div class="col-md-2">
			    	<label for="tgl_tran" class="control-label"><?= lang('pembelian_tanggal') ?></label>
			    	<input type="text" class="form-control" id="tgl_tran" name="tgl_tran" maxlength="10" value="<?= set_value('tgl_tran', isset($data['tgl_tran']) ? $data['tgl_tran'] : '') ?>">
			    </div>
			    <div class="col-md-2">
			    	<label for="type" class="control-label"><?= lang('pembelian_type') ?></label>
			    	<select id="type" name="type" class="form-control">
			    		<option></option>
			    		<option value="1" <?= set_select('type', 1, isset($data['type']) && $data['type'] == 1) ?>><?= lang('pembelian_cash') ?></option>
			    		<option value="0" <?= set_select('type', 0, isset($data['type']) && $data['type'] == 0) ?>><?= lang('pembelian_tempo') ?></option>
			    	</select>
			    </div>
			    <div class="col-md-2">
			    	<label for="tempo_hari" class="control-label"><?= lang('pembelian_tempo_hari') ?></label>
			    	<input type="text" class="form-control" id="tempo_hari" name="tempo_hari" maxlength="10" value="<?= set_value('tempo_hari', isset($data['tempo_hari']) ? $data['tempo_hari'] : '') ?>" <?= ($data['type'] == 0) ? '' : 'disabled'?> >
			    </div>
			    <div class="col-md-2">
			    	<label for="tempo_tanggal" class="control-label"><?= lang('pembelian_tempo_tanggal') ?></label>
			    	<input type="text" class="form-control" id="tempo_tanggal" name="tempo_tanggal" maxlength="10" value="<?= set_value('tempo_tanggal', isset($data['tempo_tanggal']) ? $data['tempo_tanggal'] : '') ?>" <?= ($data['type'] == 0) ? '' : 'disabled'?> >
			    </div>
    		</div>
			<div class="form-group">
			    <div class="col-md-2">
			    	<label for="no_po" class="control-label"><?= lang('pembelian_no_po') ?></label>
			    	<input type="text" class="form-control" id="no_po" name="no_po" maxlength="50" value="<?= set_value('no_po',isset($data['no_po']) ? $data['no_po'] : '') ?>" placeholder="Kode PO">
			    </div>
			    <div class="col-md-3">
			    	<label for="no_po" class="control-label"><?= lang('pembelian_nm_supplier') ?></label>
			    	<select id="id_supplier" name="id_supplier" class="form-control">
			    		<option></option>
			    		<?php foreach ($supplier as $key => $sp) : ?>
			    		<option value="<?= $sp->idsupplier ?>" <?= set_select('id_supplier', $sp->idsupplier, isset($data['id_supplier']) && $data['id_supplier'] == $sp->idsupplier) ?>><?= $sp->nm_supplier ?></option>
			    		<?php endforeach; ?>
			    	</select>
			    </div>
			    <div class="col-md-2">
			    	<label for="st_pajak" class="control-label"><?= lang('pembelian_st_pajak') ?></label>
			    	<select id="st_pajak" name="st_pajak" class="form-control">
			    		<option value="1" <?= set_select('st_pajak', 1, isset($data['st_pajak']) && $data['st_pajak'] == 1) ?>><?= lang('pembelian_berpajak') ?></option>
			    		<option value="0" <?= set_select('st_pajak', 0, isset($data['st_pajak']) && $data['st_pajak'] == 0) ?>><?= lang('pembelian_tdk_berpajak') ?></option>
			    	</select>
			    </div>
			    <div class="col-md-2">
			    	<label for="no_faktur_pajak" class="control-label"><?= lang('pembelian_faktur_pajak') ?></label>
			    	<input type="text" class="form-control" id="no_faktur_pajak" name="no_faktur_pajak" value="<?= set_value('no_faktur_pajak', isset($data['no_faktur_pajak']) ? $data['no_faktur_pajak'] : '') ?>" />
			    </div>
			    <div class="col-md-3">
			    	<div class="checkbox">
                        <label>
                            <input type="checkbox" id="is-include-pajak" name="is_include_pajak" value="1" checked="">
                            <?= lang('pembelian_include_pajak') ?>
                        </label>
                    </div>
			    </div>
		  	</div>
		  	<div class="table-responsive">
		  		<table class="table table-bordered" id="tdet_beli">
		  			<thead>
		  				<tr class="success">
			  				<th width="50"><?= lang('pembelian_no') ?></th>
			  				<th><?= lang('pembelian_barcode') ?></th>
			  				<th><?= lang('pembelian_nm_barang') ?></th>
			  				<th><?= lang('pembelian_qty') ?></th>
			  				<th><?= lang('pembelian_satuan') ?></th>
			  				<th><?= lang('pembelian_harga') ?></th>
			  				<th width="80"><?= lang('pembelian_diskon') ?></th>
			  				<th><?= lang('pembelian_potongan') ?></th>
			  				<th width="250"><?= lang('pembelian_total') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($data['items']) : ?>
		  					<?php 
		  						$no = 1;
		  						foreach($data['items'] as $key => $dt) : 
		  					?>
		  				<tr>
		  					<td><?= $no++ ?></td>
		  					<td>
		  						<?= $dt['barcode'] ?>
		  						<input type="hidden" name="id_barang[]" value="<?= $dt['id_barang'] ?>" />
		  					</td>
		  					<td><?= $dt['nm_barang'] ?>
		  						<input type="hidden" name="st_bonus[]" value="<?= $dt['st_bonus'] ?>">
		  					</td>
		  					<td><input type="number" min="1" name="qty[]" class="form-control" value="<?= $dt['qty'] ?>" /></td>
		  					<td>
		  						<select class="form-control" name="konversi[]" style="min-width: 150px;">
		  							<?php 
		  								if($dt['konversi']) :
		  									foreach ($dt['konversi'] as $key => $kf) :
		  							?>
		  							<option value="<?= $kf->id_konversi ?>" <?= (isset($kf->id_konversi) && $kf->id_konversi == $dt['id_konversi']) ? "selected" : "" ?>><?= $kf->tampil2 ?></option>
		  							<?php
		  									endforeach; 
		  								endif; 
		  							?>
		  						</select>
		  					</td>
		  					<td><input type="text" name="harga[]" class="form-control" data-val="<?= $dt['harga'] ?>" value="<?= number_format($dt['harga']) ?>" onchange="set_data_val(this);hitung_total(this)" /></td>
		  					<td><input type="number" name="item_diskon[]" min="0" max="100" class="form-control" value="<?= $dt['item_diskon'] ?>" onchange='hitung_total(this)' /></td>
		  					<td><input type="text" name="item_potongan[]" class="form-control" value="<?= $dt['item_potongan'] ?>" onchange='hitung_total(this)' /></td>
		  					<td align="right"><?= number_format($dt['sub_total']) ?></td>
		  				</tr>
		  					<?php endforeach; ?>
		  				<?php endif; ?>
		  			</tbody>
		  			<tfoot>
		  				<tr>
		  					<td colspan="8" align="right"><strong><?= lang('pembelian_pajak') ?></strong></td>
		  					<td align="right">
		  						<div class="form-inline" style="font-weight: bold;">
		  							<input type="text" style="width:74px;" id="pajak" name="pajak" class="form-control text-right" value="<?= set_value('pajak', isset($data['pajak'])? $data['pajak'] : '') ?>" placeholder="dalam %" maxlength="3" readonly="" />
	  								<input type="text" style="width: calc(100% - 78px)" id="nilai_pajak" name="nilai_pajak" class="form-control text-right" value="<?= set_value('nilai_pajak', isset($data['nilai_pajak'])? $data['nilai_pajak'] : '') ?>" readonly="" />
		  						</div>
		  					</td>
		  				</tr>
		  				<tr class="info">
		  					<td colspan="8" align="right"><strong><?= lang('pembelian_gtotal') ?></strong></td>
		  					<td align="right"><strong><label id="lblgtotal"><?= number_format($data['gtotal']) ?></label></strong></td>
		  				</tr>
		  			</tfoot>
		  		</table>
		  	</div>
		  	<div class="form-group" style="margin-top: 10px;">
			    <div class="col-md-12">
			      	<button type="submit" name="save" class="btn btn-primary"><?= lang('pembelian_btn_save') ?></button>
			    	<?php
	                	echo lang('pembelian_or') . ' ' . anchor('pembelian/pembelian/cancel', lang('pembelian_btn_cancel'), array("onclick" => "return confirm('".lang('pembelian_cancel_confirm')."')"));
	                ?>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->
<div class="modal" id="modal-barang" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
	    <div class="modal-content">
	    	<?= form_open("",array('id'=>'frm_barang','name'=>'frm_barang','role'=>'form')) ?>
		      	<div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        	<h4 class="modal-title"><?= lang('pembelian_pilih_brg') ?></h4>
		      	</div>
		      	<div class="modal-body">
		      		<div class="form-group">
		      			<div class="input-group">
		      				<input type="text" class="form-control" id="cr_barang" name="cr_barang" placeholder="<?= lang('pembelian_cari_brg') ?>" />
		      				<div class="input-group-btn">
		      					<button type="button" class="btn btn-primary" name="cari_btn" id="cari_btn"><i class="fa fa-search"></i> <?= lang('pembelian_cari_btn') ?></button>
		      				</div>
		      			</div>
		      		</div>
		        	<div class="table-responsive">
		        		<table id="tbl_barang" class="table table-hover table-striped">
		        			<thead>
		        				<tr class="success">
			        				<th><input type="checkbox" id="ck_all" name="ck_all" /></th>
			        				<th><?= lang('pembelian_no') ?></th>
			        				<th><?= lang('pembelian_barcode') ?></th>
			        				<th><?= lang('pembelian_nm_barang') ?></th>
			        			</tr>
		        			</thead>
		        			<tbody>
		        				
		        			</tbody>
		        		</table>
		        	</div>
		      	</div>
		      	<div class="modal-footer">
		      		<button type="button" class="btn btn-primary" id="pilih_btn" name="pilih_btn"><?= lang('pembelian_pilih_btn') ?></button>
		      	</div>
	      	<?= form_close() ?>
	    </div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->