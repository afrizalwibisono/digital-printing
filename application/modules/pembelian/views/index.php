<?php 
    $ENABLE_ADD     = has_permission('Pembelian.Add');
    $ENABLE_MANAGE  = has_permission('Pembelian.Manage');
    $ENABLE_DELETE  = has_permission('Pembelian.Delete');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_pembelian','name'=>'frm_pembelian')) ?>
        <div class="box-header">
                <?php if ($ENABLE_ADD) : ?>
	  	        <a href="<?= site_url('pembelian/pembelian/create') ?>" class="btn btn-success" title="<?= lang('pembelian_btn_new') ?>"><?= lang('pembelian_btn_new') ?></a>
                <?php endif; ?>
                <div class="pull-right form-inline">
                    <div class="form-group">
                        <select id="kategori_pencarian" name="kategori_pencarian" class="form-control" style="min-width: 190px">
                            <option></option>
                            <option value="0" <?= set_select('kategori_pencarian', 0) ?>><?= lang('pembelian_no_faktur') ?></option>
                            <option value="1" <?= set_select('kategori_pencarian', 1) ?>><?= lang('pembelian_no_po') ?></option>
                            <option value="2" <?= set_select('kategori_pencarian', 2) ?>><?= lang('pembelian_type') ?></option>
                        </select>
                    </div>
                    <div class="form-group" id="tp_beli">
                        <select id="tipe_beli" name="tipe_beli" class="form-control" style="min-width: 150px;display: none">
                            <option value="3" <?= set_select('tipe_beli', 3) ?>><?= lang('pembelian_semua_tipe') ?></option>
                            <option value="1" <?= set_select('tipe_beli', 1) ?>><?= lang('pembelian_cash') ?></option>
                            <option value="0" <?= set_select('tipe_beli', 0) ?>><?= lang('pembelian_tempo') ?></option>
                        </select>
                    </div>
                    <div class="form-group" id="st_beli_lunas">
                        <select id="st_lunas" name="st_lunas" class="form-control" style="min-width: 150px;display: none;">
                            <option value="">-- Status Lunas --</option>
                            <option value="1" <?= set_select('st_lunas', 1) ?>><?= lang('pembelian_lunas') ?></option>
                            <option value="0" <?= set_select('st_lunas', 0) ?>><?= lang('pembelian_belum_lunas') ?></option>
                        </select>
                    </div>
                    <div class="form-group" id="range_tanggal">
                        <div class="input-daterange input-group">
                            <input type="text" class="form-control" name="tgl_awal" id="tgl_awal" value="<?= isset($tgl_awal) && $tgl_awal !='' ? date('d/m/Y', strtotime($tgl_awal)) : '' ?>" placeholder="<?= lang('pembelian_tgl_awal') ?>" readonly style="width: 100px" />
                            <span class="input-group-addon text-black">to</span>
                            <input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?= isset($tgl_akhir) && $tgl_akhir !='' ? date('d/m/Y', strtotime($tgl_akhir)) : '' ?>" placeholder="<?= lang('pembelian_tgl_akhir') ?>" readonly style="width: 100px" />
                        </div>
                    </div>
                    <div class="input-group">
                        <input type="text" id="table_search" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="<?= lang('pembelian_placeholder_cari') ?>" autofocus autocomplete="off" />
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default" id="btn-cari"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
        </div>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
	<div class="box-body table-responsive no-padding">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
                        <th width="50"><?= lang('pembelian_no') ?></th>
                        <th><?= lang('pembelian_tanggal') ?></th>
                        <th><?= lang('pembelian_no_faktur') ?></th>
                        <th><?= lang('pembelian_no_po') ?></th>
                        <th><?= lang('pembelian_nm_supplier') ?></th>
                        <th><?= lang('pembelian_type') ?></th>
                        <th><?= lang('pembelian_st_lunas') ?></th>
                        <th><?= lang('pembelian_total') ?></th>
                        <?php if($ENABLE_MANAGE) : ?>
                        <th width="100"></th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($results as $record) : ?>
                    <tr <?= ($record->st_lunas == 0 && $record->type == 0) ? "class='danger'" : '' ?>>
                        <td class="column-check"><input type="checkbox" name="checked[]" value="<?= $record->id_pembelian ?>" /></td>
                        <td><?= $numb; ?></td>
                        <td><?= date('d/m/Y', strtotime($record->tgl_tran)) ?></td>
                        <td><?= strtoupper($record->no_nota) ?></td>
                        <td><?= strtoupper($record->no_po) ?></td>
                        <td><?= strtoupper($record->nm_supplier) ?></td>
                        <td><?= ($record->type == 0) ? lang('pembelian_tempo') : (($record->type == 1) ? lang('pembelian_cash') : lang('pembelian_konsinyasi')) ?></td>
                        <td><?= ($record->st_lunas == 0) ? lang('pembelian_belum_lunas') : lang('pembelian_lunas') ?></td>
                        <td><?= isset($record->gtotal) ? number_format($record->gtotal) : "-" ?></td>
                        <?php if($ENABLE_MANAGE) : ?>
                        <td style="padding-right:20px">
                            <a class="text-black" href="<?= site_url('pembelian/pembelian/view/' . $record->id_pembelian); ?>" data-toggle="tooltip" data-placement="left" title="View Detail / Edit Faktur Pajak"><i class="fa fa-folder"></i></a>
                            <?php if($record->st_lunas == 0) : ?>
                            <!-- &nbsp;|&nbsp; -->
                            <!-- <a class="text-black" href="<?= site_url('pembelian/pelunasan_pembelian/create/' . $record->id_pembelian); ?>" data-toggle="tooltip" data-placement="left" title="Pelunasan"><i class="fa fa-money"></i></a> -->
                            <?php endif ?>
                            <!-- &nbsp;|&nbsp; -->
                            <!-- <a class="text-black" href="<?= site_url('pembelian/retur/create/' . $record->id_pembelian); ?>" data-toggle="tooltip" data-placement="left" title="Retur"><i class="fa fa-reply-all"></i></a> -->
                        </td>
                        <?php endif; ?>
                    </tr>
                    <?php $numb++; endforeach; ?>
                </tbody>
	  </table>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">
		<?php if($ENABLE_DELETE) : ?>
		<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('pembelian_btn_delete') ?>" onclick="return confirm('<?= (lang('pembelian_delete_confirm')); ?>')">
		<?php endif;
		echo $this->pagination->create_links(); 
		?>
	</div>
	<?php else: ?>
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('pembelian_no_records_found') ?></p>
    </div>
    <?php
	endif;
	echo form_close(); 
	?>
</div><!-- /.box -->