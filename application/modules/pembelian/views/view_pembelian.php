<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_pembelian','name'=>'frm_pembelian','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
    		<div class="form-group">
    			<div class="col-md-3">
			    	<label for="no_faktur" class="control-label"><?= lang('pembelian_no_faktur') ?></label>
			    	<input type="text" class="form-control" id="no_faktur" name="no_faktur" maxlength="50" value="<?= $data->no_nota ?>" readonly>
			    </div>
			    <div class="col-md-2">
			    	<label for="tgl_tran" class="control-label"><?= lang('pembelian_tanggal') ?></label>
			    	<input type="text" class="form-control" id="tgl_tran" name="tgl_tran" maxlength="10" value="<?= date('d/m/Y', strtotime($data->tgl_tran)) ?>" readonly>
			    </div>
			    <div class="col-md-2">
			    	<label for="type" class="control-label"><?= lang('pembelian_type') ?></label>
			    	<select id="type" name="type" class="form-control" disabled="">
			    		<option></option>
			    		<option value="1" <?= set_select('type', 1, isset($data->type) && $data->type == 1) ?>><?= lang('pembelian_cash') ?></option>
			    		<option value="0" <?= set_select('type', 0, isset($data->type) && $data->type == 0) ?>><?= lang('pembelian_tempo') ?></option>
			    		<option value="2" <?= set_select('type', 2, isset($data->type) && $data->type == 2) ?>><?= lang('pembelian_konsinyasi') ?></option>
			    	</select>
			    </div>
			    <div class="col-md-2">
			    	<label for="tempo_hari" class="control-label"><?= lang('pembelian_tempo_hari') ?></label>
			    	<input type="text" class="form-control" id="tempo_hari" name="tempo_hari" maxlength="10" value="<?= ($data->tempo_hari) ? $data->tempo_hari : '-' ?>" readonly >
			    </div>
			    <div class="col-md-2">
			    	<label for="tempo_tanggal" class="control-label"><?= lang('pembelian_tempo_tanggal') ?></label>
			    	<input type="text" class="form-control" id="tempo_tanggal" name="tempo_tanggal" maxlength="10" value="<?= ($data->tgl_tempo) ? date('d/m/Y', strtotime($data->tgl_tempo)) : '-' ?>" readonly >
			    </div>

    		</div>
			<div class="form-group">
			    <div class="col-md-2">
			    	<label for="no_po" class="control-label"><?= lang('pembelian_no_po') ?></label>
			    	<input type="text" class="form-control" id="no_po" name="no_po" maxlength="50" value="<?= ($data->no_po) ? $data->no_po : '-' ?>" readonly>
			    </div>
			    <div class="col-md-3">
			    	<label for="no_po" class="control-label"><?= lang('pembelian_nm_supplier') ?></label>
			    	<select id="id_supplier" name="id_supplier" class="form-control" disabled>
			    		<option></option>
			    		<?php foreach ($supplier as $key => $sp) : ?>
			    		<option value="<?= $sp->idsupplier ?>" <?= set_select('id_supplier', $sp->idsupplier, isset($data->id_supplier) && $data->id_supplier == $sp->idsupplier) ?>><?= $sp->nm_supplier ?></option>
			    		<?php endforeach; ?>
			    	</select>
			    </div>
			    <div class="col-md-2">
			    	<label for="st_pajak" class="control-label"><?= lang('pembelian_st_pajak') ?></label>
			    	<select id="st_pajak" name="st_pajak" class="form-control" disabled>
			    		<option value="1" <?= set_select('st_pajak', 1, isset($data->st_pajak) && $data->st_pajak == 1) ?>><?= lang('pembelian_berpajak') ?></option>
			    		<option value="0" <?= set_select('st_pajak', 0, isset($data->st_pajak) && $data->st_pajak == 0) ?>><?= lang('pembelian_tdk_berpajak') ?></option>
			    	</select>
			    </div>
			    <div class="col-md-2">
			    	<label for="no_faktur_pajak" class="control-label"><?= lang('pembelian_faktur_pajak') ?></label>
			    	<input type="text" class="form-control" id="no_faktur_pajak" name="no_faktur_pajak" value="<?= set_value('no_faktur_pajak', isset($data->no_faktur_pajak) ? $data->no_faktur_pajak : '') ?>" autofocus="autofocus" />
			    </div>
		  	</div>
		  	<div class="table-responsive">
		  		<table class="table table-bordered" id="tdet_beli">
		  			<thead>
		  				<tr class="success">
			  				<th width="50"><?= lang('pembelian_no') ?></th>
			  				<th><?= lang('pembelian_barcode') ?></th>
			  				<th><?= lang('pembelian_nm_barang') ?></th>
			  				<th><?= lang('pembelian_qty') ?></th>
			  				<th><?= lang('pembelian_satuan') ?></th>
			  				<th><?= lang('pembelian_harga') ?></th>
			  				<th><?= lang('pembelian_diskon') ?></th>
			  				<th><?= lang('pembelian_potongan') ?></th>
			  				<th><?= lang('pembelian_total') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($detail) : ?>
		  					<?php 
		  						$no = 1;
		  						foreach($detail as $key => $dt) : 
		  					?>
		  				<tr>
		  					<td><?= $no++ ?></td>
		  					<td><?= $dt->barcode ?></td>
		  					<td><?= $dt->nm_barang ?></td>
		  					<td><?= number_format($dt->qty) ?></td>
		  					<td><?= $dt->nm_satuan ?></td>
		  					<td><?= number_format($dt->harga) ?></td>
		  					<td><?= $dt->diskon."%" ?></td>
		  					<td><?= number_format($dt->potongan) ?></td>
		  					<td align="right"><?= number_format($dt->total) ?></td>
		  				</tr>
		  					<?php endforeach; ?>
		  				<?php endif; ?>
		  			</tbody>
		  			<tfoot>
		  				<tr>
		  					<td colspan="8" align="right"><strong><?= lang('pembelian_pajak') ?></strong></td>
		  					<td align="right">
		  						<div class="form-inline" style="font-weight: bold;">
		  							<label><?= number_format($data->pajak).'%' ?></label>
		  							<label><?= ' ('.number_format($data->nilai_pajak).')' ?></label>
		  						</div>
		  					</td>
		  				</tr>
		  				<tr class="info">
		  					<td colspan="8" align="right"><strong><?= lang('pembelian_gtotal') ?></strong></td>
		  					<td align="right"><strong><label id="lblgtotal"><?= number_format($data->gtotal) ?></label></strong></td>
		  				</tr>
		  			</tfoot>
		  		</table>
		  	</div>
		  	<div class="form-group" style="margin-top: 10px">
			    <div class="col-md-12">
			    	<button type="submit" name="save" class="btn btn-primary"><?= lang('pembelian_btn_save') ?></button>
			    	<?php
	                	echo lang('pembelian_or') . ' ' . anchor('pembelian/pembelian', lang('pembelian_btn_back'),array('class' => 'btn btn-success'));
	                ?>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->