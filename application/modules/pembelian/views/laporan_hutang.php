<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_laporan','name'=>'frm_laporan','class' => 'form-inline')) ?>
        <div class="box-header hidden-print">
            <div class="pull-left">
                <button type="button" id="cetak" class="btn btn-default" onclick="cetak_halaman()"><i class="fa fa-print"></i> <?= lang('pembelian_cetak') ?></button>
            </div>
            <div class="pull-right">
                <div class="form-group">
                    <div class="input-daterange input-group" title="<?= lang('pembelian_range_tgl2') ?>" data-toggle="tooltip" data-placement="top">
                        <input type="text" style="width: 120px" class="form-control" name="tgl_awal" id="tgl_awal" value="<?= isset($period1) ? $period1 : '' ?>" placeholder="<?= lang('pembelian_tgl_awal') ?>" />
                        <span class="input-group-addon">to</span>
                        <input type="text" style="width: 120px" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?= isset($period2) ? $period2 : '' ?>" placeholder="<?= lang('pembelian_tgl_akhir') ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="<?= lang('pembelian_nm_supplier') ?>" autofocus />
                        <div class="input-group-btn">
                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?= form_close() ?>
    <?php if($idt) : ?>
    <div class="row header-laporan">
        <div class="col-md-12">
            <div class="col-md-1 laporan-logo">
                <img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo">
            </div>
            <div class="col-md-6 laporan-identitas">
                <address>
                    <h4><?= $idt->nm_perusahaan ?></h4>
                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?>
                </address>
            </div>
            <div class="col-md-5 text-right">
                <h3>Laporan Hutang</h3>
                <h5>Periode <?= $period1." - ".$period2 ?></h5>
            </div>
            <div class="clearfix"></div>
            <div class="laporan-garis"></div>
        </div>
    </div>
    <?php endif ?>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
    	<div class="box-body laporan-body">
            <?php
                $total_tran = 0;
                foreach ($results as $keySummary => $rs) {
                    $total_tran += $rs->gtotal;
                }
            ?>
            <h4><?= lang('pembelian_ringkasan') ?></h4>

            <table class="table-condensed table-summary">
                <tr>
                    <td width="200"><?= lang('pembelian_total_tran_qty') ?></td>
                    <td width="10">:</td>
                    <td><strong><?= number_format($keySummary+1) ?> <?= lang('pembelian_kali') ?></strong></td>
                </tr>
                <tr>
                    <td width="200"><?= lang('pembelian_total_tran') ?></td>
                    <td width="10">:</td>
                    <td><strong>Rp. <?= number_format($total_tran) ?></strong></td>
                </tr>
            </table>
            <div class="table-responsive no-padding">
                <table class="table table-condensed">
                    <tr>
                        <th><?= lang('pembelian_no') ?></th>
                        <th><?= lang('pembelian_faktur_beli') ?></th>
                        <th><?= lang('pembelian_nm_supplier') ?></th>
                        <th><?= lang('pembelian_tanggal') ?></th>
                        <th><?= lang('pembelian_jatuh_tempo') ?></th>
                        <th><?= lang('pembelian_total') ?></th>
                        <th><?= lang('pembelian_terbayar') ?></th>
                        <th><?= lang('pembelian_sisa_hutang') ?></th>
                    </tr>
                </table>
            </div>
    	</div><!-- /.box-body -->
        
	<?php else: ?>
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('pembelian_no_records_found') ?></p>
    </div>
    <?php endif;?>
</div><!-- /.box -->