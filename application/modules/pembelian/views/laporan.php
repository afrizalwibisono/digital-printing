<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_laporan','name'=>'frm_laporan','class' => 'form-inline')) ?>
        <div class="box-header hidden-print">
            <div class="pull-left">
                <button type="button" id="cetak" class="btn btn-default" onclick="cetak_halaman()"><i class="fa fa-print"></i> <?= lang('pembelian_cetak') ?></button>
            </div>
            <div class="pull-right">
                <div class="form-group">
                    <div class="input-daterange input-group" title="<?= lang('pembelian_range_tgl') ?>" data-toggle="tooltip" data-placement="top">
                        <input type="text" style="width: 120px" class="form-control" name="tgl_awal" id="tgl_awal" value="<?= isset($period1) ? $period1 : '' ?>" placeholder="<?= lang('pembelian_tgl_awal') ?>" />
                        <span class="input-group-addon">to</span>
                        <input type="text" style="width: 120px" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?= isset($period2) ? $period2 : '' ?>" placeholder="<?= lang('pembelian_tgl_akhir') ?>" />
                    </div>
                </div>
                <div class="form-group" title="<?= lang('pembelian_type') ?>" data-toggle="tooltip" data-placement="top">
                    <select id="tipe_beli" name="tipe_beli" class="form-control" style="min-width: 150px;">
                        <option value="3" <?= set_select('tipe_beli', 3) ?>><?= lang('pembelian_semua_tipe') ?></option>
                        <option value="1" <?= set_select('tipe_beli', 1) ?>><?= lang('pembelian_cash') ?></option>
                        <option value="0" <?= set_select('tipe_beli', 0, TRUE) ?>><?= lang('pembelian_tempo') ?></option>
                    </select>
                </div>
                <div class="form-group" title="<?= lang('pembelian_st_lunas') ?>" data-toggle="tooltip" data-placement="top">
                    <select id="st_lunas" name="st_lunas" class="form-control" style="min-width: 150px;">
                        <option value="">-- Status Lunas --</option>
                        <option value="1" <?= set_select('st_lunas', 1) ?>><?= lang('pembelian_lunas') ?></option>
                        <option value="0" <?= set_select('st_lunas', 0, TRUE) ?>><?= lang('pembelian_belum_lunas') ?></option>
                    </select>
                </div>
                <div class="form-group" title="<?= lang('pembelian_tempo_hari') ?>" data-toggle="tooltip" data-placement="top">
                    <select id="jatuh_tempo" name="jatuh_tempo" class="form-control">
                        <option value="" <?= set_select('jatuh_tempo', '') ?>>-- Pilih Jatuh Tempo--</option>
                        <option value="7" <?= set_select('jatuh_tempo', 7, TRUE) ?>>7 Hari Kedepan</option>
                        <option value="15" <?= set_select('jatuh_tempo', 15) ?>>15 Hari Kedepan</option>
                        <option value="30" <?= set_select('jatuh_tempo', 30) ?>>30 Hari Kedepan</option>
                    </select>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="<?= lang('pembelian_operator') ?>" autofocus />
                        <div class="input-group-btn">
                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?= form_close() ?>
    <?php if($idt) : ?>
    <div class="row header-laporan">
        <div class="col-md-12">
            <div class="col-md-1 laporan-logo">
                <img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo">
            </div>
            <div class="col-md-6 laporan-identitas">
                <address>
                    <h4><?= $idt->nm_perusahaan ?></h4>
                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?>
                </address>
            </div>
            <div class="col-md-5 text-right">
                <h3>Laporan Pembelian</h3>
                <h5>Periode <?= $period1." - ".$period2 ?></h5>
            </div>
            <div class="clearfix"></div>
            <div class="laporan-garis"></div>
        </div>
    </div>
    <?php endif ?>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
    	<div class="box-body laporan-body">
            <?php
                $total_tran = 0;
                foreach ($results as $keySummary => $rs) {
                    $total_tran += $rs->gtotal;
                }
            ?>
            <h4><?= lang('pembelian_ringkasan') ?></h4>

            <table class="table-condensed table-summary">
                <tr>
                    <td width="200"><?= lang('pembelian_total_tran_qty') ?></td>
                    <td width="10">:</td>
                    <td><strong><?= number_format($keySummary+1) ?> <?= lang('pembelian_kali') ?></strong></td>
                </tr>
                <tr>
                    <td width="200"><?= lang('pembelian_total_tran') ?></td>
                    <td width="10">:</td>
                    <td><strong>Rp. <?= number_format($total_tran) ?></strong></td>
                </tr>
            </table>

            <h4><?= lang('pembelian_detail') ?></h4>

            <?php foreach ($results as $key => $dt): ?>
                <div class="table-responsive no-padding">
                    <table class="table-condensed table-head">
                        <tr>
                            <td><?= lang('pembelian_faktur_beli') ?></td>
                            <td>:</td>
                            <td><strong><?= $dt->no_nota ?></strong></td>
                            <td><?= lang('pembelian_type') ?></td>
                            <td>:</td>
                            <td><?= $dt->type == 0 ? lang('pembelian_tempo') : lang('pembelian_cash') ?></td>
                            <td><?= lang('pembelian_st_pajak') ?></td>
                            <td>:</td>
                            <td><?= $dt->st_pajak == 0 ? lang('pembelian_tdk_berpajak') : lang('pembelian_berpajak') ?></td>
                        </tr>
                        <tr>
                            <td><?= lang('pembelian_tanggal') ?></td>
                            <td>:</td>
                            <td><?= date('d/m/Y', strtotime($dt->tgl_tran)) ?></td>
                            <td><?= lang('pembelian_tempo_tanggal') ?></td>
                            <td>:</td>
                            <td><?= $dt->tgl_tempo ? date('d/m/Y', strtotime($dt->tgl_tempo)) : '-' ?></td>
                            <td><?= lang('pembelian_faktur_pajak') ?></td>
                            <td>:</td>
                            <td><?= $dt->no_faktur_pajak ?></td>
                        </tr>
                        <tr>
                            <td><?= lang('pembelian_no_po') ?></td>
                            <td>:</td>
                            <td><?= $dt->no_po ?></td>
                            <td><?= lang('pembelian_nm_supplier') ?></td>
                            <td>:</td>
                            <td><?= ucwords($dt->nm_supplier) ?></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </div>
                <div class="table-responsive no-padding">
                    <table class="table table-condensed table-bordered table-detail">
                        <thead>
                            <tr class="bg-success">
                                <th width="50">#</th>
                                <th><?= lang('pembelian_barcode') ?></th>
                                <th><?= lang('pembelian_nm_barang') ?></th>
                                <th><?= lang('pembelian_qty') ?></th>
                                <th><?= lang('pembelian_satuan') ?></th>
                                <th><?= lang('pembelian_harga') ?></th>
                                <th><?= lang('pembelian_diskon') ?></th>
                                <th><?= lang('pembelian_potongan') ?></th>
                                <th><?= lang('pembelian_total') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $total = 0;
                                foreach ($dt->det_beli as $key2 => $record) : 
                            ?>
                            <tr>
                                <td><?= $key2+1 ?></td>
                                <td><?= $record->barcode ?></td>
                                <td><?= $record->nm_barang ?></td>
                                <td><?= number_format($record->qty) ?></td>
                                <td><?= $record->nm_satuan ?></td>
                                <td><?= number_format($record->harga) ?></td>
                                <td><?= $record->diskon."%" ?></td>
                                <td><?= number_format($record->potongan) ?></td>
                                <td align="right"><strong><?= number_format($record->total) ?></strong></td>
                            </tr>
                            <?php $total += $record->total; 
                                endforeach;
                            ?>
                            <tr class="border-top">
                                <td colspan="8" align="right"><strong><?= lang('pembelian_pajak') ?> (<?= $dt->pajak.'%' ?>)</strong></td>
                                <td align="right"><strong>Rp. <?= number_format($dt->nilai_pajak) ?></strong></td>
                            </tr>
                            <tr>
                                <td colspan="8" align="right"><strong><?= lang('pembelian_gtotal') ?></strong></td>
                                <td align="right"><strong>Rp. <?= number_format($total) ?></strong></td>
                            </tr>
                        </tbody>
        	       </table>
               </div>
          <?php endforeach ?>
    	</div><!-- /.box-body -->
        
	<?php else: ?>
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('pembelian_no_records_found') ?></p>
    </div>
    <?php endif;?>
</div><!-- /.box -->