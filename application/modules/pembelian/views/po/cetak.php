<div class="box box-solid">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_po','name'=>'frm_po','role'=>'form','class'=>'form-horizontal')) ?>
    	<?php if($idt) : ?>
	    <div class="row header-laporan">
	        <div class="col-md-12">
	            <div class="col-md-1 laporan-logo">
	                <img src="<?= base_url('assets/images/report/'.$idt->report) ?>" alt="logo">
	            </div>
	            <div class="col-md-6 laporan-identitas">
	                <address>
	                	<h3 class="no-margin"><?= lang('pembelian_po_title_view') ?></h3>
	                    <h4><?= $idt->nm_perusahaan ?></h4>
	                    <?= $idt->alamat ?>, <?= $idt->kota ?><br>
	                    Telp : <?= $idt->no_telp ?>, Fax : <?= $idt->fax ?><br>
	                    Email : <?= $idt->email ?>, Website : <?= prep_url($idt->website) ?><br>
	                </address>
	            </div>
	            <div class="clearfix"></div>
	            <div class="laporan-garis"></div>
	        </div>
	    </div>
	    <?php endif ?>
    	<div class="box-body">
    		<table class="table-condensed table-summary">
                <tr>
                    <td width="120"><?= lang('pembelian_no_po') ?></td>
                    <td width="10">:</td>
                    <td><strong><?= $data->no_po ?></strong></td>
                    <td width="120"><?= lang('pembelian_nm_supplier') ?></td>
                    <td width="10">:</td>
                    <td><strong><?= $data->nm_supplier ?></strong></td>
                </tr>
                <tr>
                    <td width="120"><?= lang('pembelian_tanggal_kirim') ?></td>
                    <td width="10">:</td>
                    <td><?= ($data->tanggal_kirim != "") ? date('d/m/Y', strtotime($data->tanggal_kirim)) : "-" ?></td>
                	<td width="120">Dicetak Pada</td>
                    <td width="10">:</td>
                    <td><?= date('d/m/Y H:i:s') ?></td>
                </tr>
            </table>
            <h5>Detail Barang</h5>
		  	<div class="table-responsive">
		  		<table class="table table-condensed table-bordered table-detail">
		  			<thead>
		  				<tr class="success">
			  				<th width="50"><?= lang('pembelian_no') ?></th>
			  				<th><?= lang('pembelian_barcode') ?></th>
			  				<th><?= lang('pembelian_nm_barang') ?></th>
			  				<th><?= lang('pembelian_qty') ?></th>
			  				<th><?= lang('pembelian_satuan') ?></th>
			  				<th><?= lang('pembelian_harga') ?></th>
			  				<th><?= lang('pembelian_total') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($detail) : ?>
		  					<?php 
		  						$no = 1;
		  						foreach($detail as $key => $dt) : 
		  					?>
		  				<tr>
		  					<td><?= $no++ ?></td>
		  					<td>
		  						<?= $dt->barcode ?>
		  					</td>
		  					<td><?= $dt->nm_barang ?></td>
		  					<td><?= $dt->qty ?></td>
		  					<td><?= $dt->satuan ?></td>
		  					<td><?= number_format($dt->harga) ?></td>
		  					<td align="right"><?= number_format($dt->sub_total) ?></td>
		  				</tr>
		  					<?php endforeach; ?>
		  				<?php endif; ?>
		  			</tbody>
		  			<tfoot>
		  				<tr class="info">
		  					<td colspan="6" align="right"><strong><?= lang('pembelian_gtotal') ?></strong></td>
		  					<td align="right"><strong><label id="lblgtotal"><?= number_format($data->total) ?></label></strong></td>
		  				</tr>
		  			</tfoot>
		  		</table>
		  	</div>
		  	<br>
		  	<div class="pull-right" style="margin-right: 100px">
		  		<?= lang('pembelian_po_pengirim') ?>,
		  		<br>
			  	<br>
			  	<br>
			  	<?= $pengirim->nm_karyawan ?>
		  	</div>
	  	</div>
	<?= form_close() ?>
</div><!-- /.box -->
<script type="text/javascript">
	$(document).ready(function(){
		window.print();
		window.close();
	});
</script>