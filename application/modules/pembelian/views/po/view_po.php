<div class="box box-primary">
    <!-- form start -->
    <?= form_open("",array('id'=>'frm_detail_po','name'=>'frm_detail_po','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="form-group">
			    <div class="col-md-2">
			    	<label for="no_po" class="control-label"><?= lang('pembelian_no_po') ?></label>
			    	<input type="text" class="form-control" id="no_po" name="no_po" maxlength="50" value="<?= $data->no_po ?>" readonly="">
			    </div>
			    <div class="col-md-3">
			    	<label for="no_po" class="control-label"><?= lang('pembelian_nm_supplier') ?></label>
			    	<input id="nm_supplier" name="nm_supplier" class="form-control" value="<?= $nm_supplier ?>" readonly="" />
			    </div>
			    <div class="col-md-2">
			    	<label for="tanggal_kirim" class="control-label"><?= lang('pembelian_tanggal_kirim') ?></label>
			    	<input type="text" class="form-control" id="tanggal_kirim" name="tanggal_kirim" maxlength="10" value="<?= date('d/m/Y', strtotime($data->tanggal_kirim)) ?>" readonly>
			    </div>
			    <div class="col-md-4 hidden-print">
			    	<label for="no_req" class="control-label"><?= lang('pembelian_req_code') ?></label>
			    	<input type="text" class="form-control" name="no_req" id="no_req" value="<?= $no_reqs->no_reqs ? $no_reqs->no_reqs : '-' ?>" readonly="" />
			    </div>
		  	</div>
		  	<div class="table-responsive">
		  		<table class="table table-bordered" id="tdet_po">
		  			<thead>
		  				<tr class="success">
			  				<th width="50"><?= lang('pembelian_no') ?></th>
			  				<th><?= lang('pembelian_barcode') ?></th>
			  				<th><?= lang('pembelian_nm_barang') ?></th>
			  				<th><?= lang('pembelian_qty') ?></th>
			  				<th><?= lang('pembelian_terpenuhi') ?></th>
			  				<th><?= lang('pembelian_satuan') ?></th>
			  				<th><?= lang('pembelian_harga') ?></th>
			  				<th><?= lang('pembelian_total') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($detail) : ?>
		  					<?php 
		  						$no = 1;
		  						foreach($detail as $key => $dt) : 
		  					?>
		  				<tr>
		  					<td><?= $no++ ?></td>
		  					<td>
		  						<?= $dt->barcode ?>
		  					</td>
		  					<td><?= $dt->nm_barang ?></td>
		  					<td><?= $dt->qty ?></td>
		  					<td><?= $dt->qty2 ?></td>
		  					<td><?= $dt->satuan ?></td>
		  					<td><?= number_format($dt->harga) ?></td>
		  					<td align="right"><?= number_format($dt->sub_total) ?></td>
		  				</tr>
		  					<?php endforeach; ?>
		  				<?php endif; ?>
		  			</tbody>
		  			<tfoot>
		  				<tr class="info">
		  					<td colspan="7" align="right"><strong><?= lang('pembelian_gtotal') ?></strong></td>
		  					<td align="right"><strong><label id="lblgtotal"><?= number_format($data->total) ?></label></strong></td>
		  				</tr>
		  			</tfoot>
		  		</table>
		  	</div>
		  	<br>
		  	<br>
		  	<div class="table-responsive">
		  		<h4><?= lang('pembelian_daftar_beli') ?></h4>
		  		<table class="table table-bordered" id="t_beli">
		  			<thead>
		  				<tr class="success">
			  				<th width="50"><?= lang('pembelian_no') ?></th>
			  				<th><?= lang('pembelian_faktur_beli') ?></th>
			  				<th><?= lang('pembelian_tanggal_beli') ?></th>
			  				<th><?= lang('pembelian_type') ?></th>
			  				<th><?= lang('pembelian_jatuh_tempo') ?></th>
			  			</tr>
		  			</thead>
		  			<tbody>
		  				<?php if($pembelian) : ?>
		  					<?php 
		  						$no = 1;
		  						foreach($pembelian as $key => $bl) : 
		  					?>
		  				<tr>
		  					<td><?= $no++ ?></td>
		  					<td><a href="<?= site_url('pembelian/pembelian/view/'.$bl->id_pembelian) ?>" target="_blank" title="Klik untuk melihat detail pembelian"><?= $bl->no_nota ?></a></td>
		  					<td><?= date('d/m/Y', strtotime($bl->tgl_tran)) ?></td>
		  					<td><?= $bl->type == 0 ? lang('pembelian_tempo') : lang('pembelian_cash') ?></td>
		  					<td><?= $bl->type == 1 ? '-' : date('d/m/Y', strtotime($bl->tgl_tempo)) ?></td>
		  				</tr>
		  					<?php endforeach; ?>
		  				<?php endif; ?>
		  			</tbody>
		  		</table>
		  	</div>
		  	<div class="form-group"></div>
		  	<div class="form-group hidden-print">
			    <div class="col-md-12">
			    	<?php
	                	echo anchor('pembelian/po', lang('pembelian_btn_back'), array('class' => 'btn btn-success'));
	                ?>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->