<?php 
    $ENABLE_ADD     = has_permission('PO.Add');
    $ENABLE_MANAGE  = has_permission('PO.Manage');
    $ENABLE_DELETE  = has_permission('PO.Delete');
?>
<div class="box box-primary">
	<?= form_open($this->uri->uri_string(),array('id'=>'frm_po','name'=>'frm_po', 'class' => 'form-inline')) ?>
        <div class="box-header">
            <?php if ($ENABLE_ADD) : ?>
            <div class="pull-left">
                <a href="<?= site_url('pembelian/po/create') ?>" class="btn btn-success" title="<?= lang('pembelian_btn_new') ?>"><?= lang('pembelian_btn_new') ?></a>
            </div>
            <?php endif; ?>
            <div class="pull-right">
                <div class="form-group">
                    <div class="input-daterange input-group">
                        <input type="text" class="form-control" name="tgl_awal" id="tgl_awal" value="<?= isset($tgl_awal) && $tgl_awal !='' ? date('d/m/Y', strtotime($tgl_awal)) : '' ?>" placeholder="<?= lang('pembelian_tgl_awal') ?>" required />
                        <span class="input-group-addon text-black">to</span>
                        <input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?= isset($tgl_akhir) && $tgl_akhir !='' ? date('d/m/Y', strtotime($tgl_akhir)) : '' ?>" placeholder="<?= lang('pembelian_tgl_akhir') ?>" required />
                    </div>
                </div>
                <select id="id_supplier" name="id_supplier" class="form-control" style="min-width: 150px">
                    <option></option>
                    <?php foreach ($supplier as $key => $sp) : ?>
                    <option value="<?= $sp->idsupplier ?>" <?= set_select('id_supplier', $sp->idsupplier, isset($id_supplier) && ($id_supplier === $sp->idsupplier)) ?>><?= $sp->nm_supplier ?></option>
                    <?php endforeach; ?>
                </select>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="Search" autofocus />
                        <div class="input-group-btn">
                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	<?php if (isset($results) && is_array($results) && count($results)) : ?>
	<div class="box-body table-responsive no-padding">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th class="column-check" style="width: 30px;"><input class="check-all" type="checkbox" /></th>
                        <th width="50">#</th>
                        <th><?= lang('pembelian_no_po') ?></th>
                        <th><?= lang('pembelian_nm_supplier') ?></th>
                        <th><?= lang('pembelian_tanggal_kirim') ?></th>
                        <th><?= lang('pembelian_total') ?></th>
                        <th><?= lang('pembelian_st_po') ?></th>
                        <?php if($ENABLE_MANAGE) : ?>
                        <th width="80"></th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $atts = array(
                                'width'       => 744,
                                'height'      => 600,
                                'scrollbars'  => 'yes',
                                'status'      => 'yes',
                                'resizable'   => 'yes',
                                'screenx'     => 100,
                                'screeny'     => 100,
                                'window_name' => '_blank',
                                'class'       => 'text-black',
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'left',
                                'title'       => 'Cetak PO'        
                        );

                        foreach ($results as $record) : ?>
                    <tr>
                        <td class="column-check"><input type="checkbox" name="checked[]" value="<?= $record->id_po ?>" /></td>
                        <td><?= $numb; ?></td>
                        <td><?= $record->no_po ?></td>
                        <td><?= ucwords($record->nm_supplier) ?></td>
                        <td><?= date('d/m/Y', strtotime($record->tanggal_kirim)) ?></td>
                        <td><?= isset($record->total) ? number_format($record->total) : "-" ?></td>
                        <?php if($ENABLE_MANAGE) : ?>
                        <td class="td-info">
                            <select name="st_po" class="form-control">
                                <option value="0" <?= $record->st_close == 0 ? 'selected=""': '' ?>><?= lang('pembelian_st_open') ?></option>
                                <option value="1" <?= $record->st_close == 1 ? 'selected=""': '' ?>><?= lang('pembelian_st_closed') ?></option>
                            </select>
                        </td>
                        <td style="padding-right:20px">
                            <?= anchor_popup('pembelian/po/cetak/'.$record->id_po, "<i class='fa fa-print'></i>", $atts); ?>
                            &nbsp;|&nbsp;
                            <a class="text-black" href="<?= site_url('pembelian/po/view/' . $record->id_po); ?>" data-toggle="tooltip" data-placement="left" title="View Detail"><i class="fa fa-folder"></i></a>
                        </td>
                        <?php endif; ?>
                    </tr>
                    <?php $numb++; endforeach; ?>
                </tbody>
	  </table>
	</div><!-- /.box-body -->
	<div class="box-footer clearfix">
		<?php if($ENABLE_DELETE) : ?>
		<input type="submit" name="delete" class="btn btn-danger" id="delete-me" value="<?php echo lang('pembelian_btn_delete') ?>" onclick="return confirm('<?= lang('pembelian_delete_po_confirm') ?>')">
		<?php endif;
		echo $this->pagination->create_links(); 
		?>
	</div>
	<?php else: ?>
    <div class="alert alert-info" role="alert">
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('pembelian_no_records_found') ?></p>
    </div>
    <?php
	endif;
	echo form_close(); 
	?>
</div><!-- /.box -->