$(function(){
	//Date Picker
	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	$("#tipe_beli,#st_lunas,#jatuh_tempo").select2();

	var val_tipe = $("#tipe_beli").val();
	cek_tipe(val_tipe);

	$("#tipe_beli").on("change", function(){
		var val = $(this).val();
		cek_tipe(val);
	});
});

function cek_tipe(val){
	if(val == 0){
		$("#jatuh_tempo, #st_lunas").parent().show();
		$("#tgl_awal, #tgl_akhir").removeAttr("required").val("");
	}else{
		$("#jatuh_tempo, #st_lunas").parent().hide();
		$("#tgl_awal, #tgl_akhir").attr("required","");
	}
}

function cetak_halaman(){
	window.print();
}