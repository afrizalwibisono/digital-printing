$(document).ready(function(){
	$("#ck_all").on("click", function(){
		if($(this).prop("checked")){
			$("input[name='item_code[]']").prop("checked", true);
		}else{
			$("input[name='item_code[]']").prop("checked", false);
		}
	});
	//Load Kedatangan
	$("#no_faktur").on("keydown", function(e){
		if(e.keyCode == 13){
			load_kedatangan();
			e.preventDefault();
		}
	});

	$("#id_supplier,#type,#st_pajak").select2({ placeholder : "-- Pilih --"});
	//Format Harga
	$("input[name='harga[]']").number(true, 0);
	//Date Picker
	$('#tgl_tran,#tempo_tanggal').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	//Set tgl Transaksi
	set_date_tran();

	cek_tipe();

	//Change
	$("#type").on("change", function(){
		cek_tipe();
	}) ;

	//Set Tanggal Tempo
	$("#tempo_hari").on("keyup", function(){
		validasi_angka(this);
		set_tempo();
	});

	//Set jml tempo [hari]
	$("#tempo_tanggal").on("change", function(){
		ht_tempo();
	});

	//Hitung g total
	$("#diskon").on("keyup", function(){
		validasi_angka(this);
		hitung_gtotal();
	});

	$("#potongan, #pajak").on("keyup", function(){
		hitung_gtotal();
	});

	//Format number
	$("#potongan,input[name='harga[]'],input[name='item_potongan[]']").number(true, 0);
	//Konversi
	$("select[name='konversi[]']").select2();
	//Set Enable supplier before submit
	$("#frm_pembelian").on("submit", function(){
		$("#id_supplier").prop("disabled", false);
	});

	$("#is-include-pajak").on("change", function(){
		var ck = $(this).prop('checked');
		if(!ck){
			$("#pajak").prop("readonly", false);
			$("#pajak").val(10);
			hitung_gtotal();
		}else{
			$("#pajak").prop("readonly", true);
			$("#pajak").val(0);
			hitung_gtotal();
		}
	});
}); //End Document Ready

//Cek Type
function cek_tipe(){
	if($('#type').val() == 0){
		$("#tempo_hari").prop("disabled", false);
		$("#tempo_tanggal").prop("disabled", false);
		$("#tempo_hari").focus();
	}else{
		$("#tempo_hari").prop("disabled", true);
		$("#tempo_tanggal").prop("disabled", true);
		$("#tempo_hari").val("");
		$("#tempo_tanggal").val("");
	}
}

//validasi angka
function validasi_angka(obj) {
	if (isNaN(obj.value)) {
		alert('Harus diisi angka');
		obj.value='';					
		obj.focus();
		return false;				
	}
}

//Set tgl Transaksi
function set_date_tran() {
	if($('#tgl_tran').val() == ""){
		var dNow = new Date();

		$('#tgl_tran').datepicker('update',dNow);
	}
}

//Set Tanggal Tempo
function set_tempo(){
	var tgl= $("#tgl_tran").val().split('/');
	var ctgl = new Date(tgl[2],tgl[1]-1,tgl[0]);
	var jmlhari = parseInt($("#tempo_hari").val());
	
	if (jmlhari > 0) {
		ctgl.setDate(ctgl.getDate()+jmlhari);
		$("#tempo_tanggal").datepicker('update',ctgl);
	}else {
		$("#tempo_tanggal").val("");
	}
}

//hitung jml hari
function ht_tempo(){
	var tgl_tran 	= $("#tgl_tran").datepicker("getDate");
	var tgl_tempo 	= $("#tempo_tanggal").datepicker("getDate");
	
	if(tgl_tempo <= tgl_tran ){
		alert('Tanggal tempo harus lebih dari tanggal transaksi !');
		$("#tempo_tanggal").val("");
		$("#tempo_hari").val("");
		return;
	}

	var oneday = 24*3600*1000; //Milisecond

	var diff = Math.ceil((tgl_tempo.getTime() - tgl_tran.getTime()) / oneday);

	$("#tempo_hari").val(diff);	
}

function load_kedatangan(){
	var kode = $("#no_faktur").val();

	$.ajax({
		url : baseurl+"pembelian/pembelian/load_kedatangan",
		type : "post",
		dataType : "json",
		data : {kode : kode},
		success : function(msg){
			if(msg['data'] != false){

				$("table#tdet_beli tbody tr").remove();

				$.each(msg['data']['detail'], function(i,n){
					var opt_select = "";
					$.each(n['konversi'], function(l,m){
						var selected = "";
						if(m['selected'] == 1)
						{
							selected = "selected";
						}

						opt_select += '<option value="'+m['id_konversi']+'" '+selected+'>'+m['tampil2']+'</option>';
					});

					var $row = $("<tr>"
		  					+"<td>"+(i+1)+"</td>"
		  					+"<td>"
		  						+n['barcode']
		  						+"<input type='hidden' name='id_barang[]' value='"+n['id_barang']+"' />"
		  					+"</td>"
		  					+"<td>"+n['nm_barang']+(n['st_bonus'] == 1 ? '<strong> (Bonus)</strong>' : '')+"<input type='hidden' name='st_bonus[]' value='"+n['st_bonus']+"' /></td>"
		  					+"<td><input type='number' min='1' name='qty[]' onchange='hitung_total(this)' class='form-control' value='"+n['qty']+"' /></td>"
		  					+"<td><select class='form-control' name='konversi[]' style='width: 150px;'>"+opt_select+"</select></td>"
		  					+"<td><input type='text' name='harga[]' onchange='set_data_val(this);hitung_total(this)' class='form-control' data-val='"+n['harga']+"' value='"+$.number(n['harga'])+"' /></td>"
		  					+"<td><input type='number' name='item_diskon[]' min='0' max='100' class='form-control' value='0' onchange='hitung_total(this)' /></td>"
		  					+"<td><input type='text' name='item_potongan[]' class='form-control' value='0' onchange='hitung_total(this)' /></td>"
		  					+"<td align='right'>"+$.number(n['subtotal'])+"</td>"
		  				+"</tr>");

					$("#tdet_beli tbody").append($row);

					opt_select = "";

					//Format number
					$("#potongan,input[name='harga[]'],input[name='item_potongan[]']").number(true, 0);
				});

				//Set Supplier
				$("#id_supplier").val(msg['data']['id_supplier']).trigger("change");
				$("#id_supplier").select2("enable",false);
				$("#no_faktur").prop("readonly", true);
				$("#id_kedatangan").val(msg['data']['id_kedatangan']);
				//Konversi
				$("select[name='konversi[]']").select2();

				$("#lblgtotal").text($.number(msg['data']['gtotal']));
				$(window).trigger("resize");
			}else{
        		alertify.error('No. Nota/Surat Jalan/Faktur tidak ditemukan');
			}
		}
	});
}

function set_data_val(obj){
	var val = $(obj).val().replace(/,/g,"");
	if(!val){
		val = 0;
	}

	$(obj).data('val', val);
}

//Remove Item
function remove_item(obj){
	$(obj).closest("tr").animate({backgroundColor:'red'}, 1000).fadeOut(1000,function() {
	    $(obj).closest("tr").remove();

	    hitung_gtotal();
	});
}

//hitung total
function hitung_total(obj){
	var qty 	= parseFloat($(obj).closest("tr").find("input[name='qty[]']").val());
	var harga 	= parseFloat($(obj).closest("tr").find("input[name='harga[]']").val().replace(/,/g,""));
	var diskon 	= parseFloat($(obj).closest("tr").find("input[name='item_diskon[]']").val());
	var potongan = parseFloat($(obj).closest("tr").find("input[name='item_potongan[]']").val().replace(/,/g,""));

	if(isNaN(qty)){
		qty = 0;
	}

	if(isNaN(harga)){
		harga = 0;
	}

	if(isNaN(diskon)){
		diskon = 0;
	}

	if(!potongan){
		potongan = 0;
		$(obj).closest("tr").find("input[name='item_potongan[]']").val(0)
	}

	var total = 0;
	if(qty > 0 && harga > 0)
	{
		total = (qty * harga) - ((diskon/100)*(qty * harga)) - potongan;
	}
	$(obj).closest("tr").find("td:last").text($.number(Math.round(total/10) * 10));

	hitung_gtotal();
}

//hitung gtotal
function hitung_gtotal(){
	var pj_table = $("#tdet_beli tr").length;
	var pajak 	 = parseFloat($("#pajak").val().replace(/,/g,""));

	if(!pajak){
		pajak = 0;
	}

	var gtotal = 0;
	for (var i = 1; i <= pj_table-3; i++) {
		var sub_total = parseFloat($("#tdet_beli tr:eq("+i+") td:last").text().replace(/,/g,""));
		gtotal += sub_total;
	}

	var nilai_pajak = Math.round((pajak / 100) * gtotal);
	$("#nilai_pajak").val($.number(nilai_pajak));

	gtotal += nilai_pajak;
	$("#lblgtotal").text($.number(gtotal));
}

//buat nomor
function buat_no(){
	var pj_table = $("#tdet_beli tr").length;
	for (var i = 1; i <= pj_table-5; i++) {
		$("#tdet_beli tr:eq("+i+") td:first").text(i);
	}
}