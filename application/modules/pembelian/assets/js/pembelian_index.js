$(function(){
	//Date Picker
	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	ubah_kategori_cari();
    //Tipe Jual
    $("#kategori_pencarian").select2({ placeholder : "-- Kategori Pencarian --", allowClear:true});
    
    //Kategori Pencarian
    $("#kategori_pencarian").on("change", function(){
        ubah_kategori_cari();
    });

    $("#btn-cari").on("click", function(){
        pencarian();
    });

    $("#table_search").on("keydown", function(e){
        if(e.keyCode == 13){
            pencarian();
            e.preventDefault();
        }
    });

    $("#tipe_beli").select2();
    $("#st_lunas").select2();

});

function ubah_kategori_cari(){
    var val     = $("#kategori_pencarian").val();
    var txtval  = $("#kategori_pencarian option:selected").text();

    if(val == 2){
        $("#range_tanggal").show("slow");
        $("#tp_beli").show("slow");
        $("#st_beli_lunas").show("slow");

        $("input[name='table_search']").prop("placeholder", "Nama Barang");
    }else{
        $("#range_tanggal").hide();
        $("#tp_beli").hide();
        $("#st_beli_lunas").hide();

        if(txtval != ""){
            $("input[name='table_search']").prop("placeholder", txtval);
        }else{
            $("input[name='table_search']").prop("placeholder", "Pencarian");
            $("input[name='table_search']").val("");
        }
    }
}

function pencarian(){
    var kategori = $("#kategori_pencarian").val();
    var txtcari  = $("#table_search").val();

    if(kategori ==""){
        alertify.error('Silahkan pilih kategori pencarian');
        return false;
    }

    if(kategori == 0 && txtcari == ""){
        alertify.error('Silahkan ketik nomor faktur yang akan dicari');
        return false;
    }

    if(kategori == 1 && txtcari == ""){
        alertify.error('Silahkan ketik kode PO yang akan dicari');
        return false;
    }

    if(kategori == 2){
        var tgl_awal  = $("#tgl_awal").val();
        var tgl_akhir = $("#tgl_akhir").val();
        if(tgl_awal == "" || tgl_akhir == ""){
            alertify.error('Silahkan tentukan range tanggal pencarian');
            return false;
        }
    }

    $("#frm_pembelian").submit();
}