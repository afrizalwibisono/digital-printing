$(document).ready(function(){
	$("#ck_all").on("click", function(){
		if($(this).prop("checked")){
			$("input[name='item_code[]']").prop("checked", true);
		}else{
			$("input[name='item_code[]']").prop("checked", false);
		}
	});

	$("#id_supplier").select2({ 
						placeholder : "-- Pilih Supplier --",
						allowClear : true
	});
	//No request
	$("#no_req").select2({
		tags: true,
		placeholder : '-- Pilih --'
	});
	$("select[name='st_po']").select2();

	$("#no_req").on("change", function(){
		get_detail_permintaan();
	});
	//Konversi
	$("select[name='konversi[]']").select2({
		placeholder : '-- Pilih --'
	});
	//Format Harga
	$("input[name='harga[]']").number(true, 0);

	$("#cari_btn").on("click", function(){
		cari_barang();
	});

	$("#cr_barang").on("keydown", function(e){
		if(e.keyCode == 13){
			cari_barang();
			e.preventDefault();
		}
	});

	//Date Picker
	$('#tanggal_kirim').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	//Date Picker
	$('.input-daterange').datepicker({
	    todayBtn: "linked",
	    format: "dd/mm/yyyy",
	    autoclose: true,
	    clearBtn: true,
	    todayHighlight: true
	});

	//Add Items
	$("#pilih_btn").on("click", function(){
		add_items();
	});

	//Scan Barcode
	$("input[name='barcode']").on("keydown", function(e){
		if(e.keyCode == 13){
			scan_barcode();
			e.preventDefault();
		}
	});
	//Status PO
	$("select[name='st_po']").on("change", function(){
		var id_po = $(this).closest("tr").find("input[name='checked[]']").val();
		var st  = $(this).val();
		
		update_status_po(id_po, st, this);
	})
});

function update_status_po(id_po, status, target)
{
	var conf = confirm('Apakah anda yakin akan mengubah status PO');
	if(!conf)
	{
		$(target).prop("checked", !status);
		return false;
	}

	$.ajax({
		url : baseurl+"pembelian/po/update_po",
		type : "post",
		dataType : "json",
		data : {no_po: id_po, st: status},
		success : function(data){
			var elm = $(target).closest("td").find("label.label");
			if(data['type']=='success'){
				elm.text(data['label']);
				if(status == 1){
					elm.switchClass('label-danger', 'label-success');
				}else{
					elm.switchClass('label-success', 'label-danger');
				}

		        alertify.success(data['msg']);
			}else{
				$(target).prop("checked", !status);

		        alertify.error(data['msg']);
			}
		}
	});
}

function get_detail_permintaan(){
	var ids = $("#no_req").val();

	$.ajax({
		url : baseurl+"pembelian/po/get_det_permintaan",
		type : "post",
		dataType : "json",
		data : {no_req : ids},
		success : function(msg){
			$("table#tdet_po tbody tr").remove();
			
			if(msg['type']=='success'){
				$.each(msg['data'], function(i,n){
					var opt_select = "";

					$.each(n['konversi'], function(x,y){
						var selected = "";
						if(n['id_konversi'] == y['id_konversi'])
						{
							selected = "selected";
						}

						opt_select += '<option value="'+y['id_konversi']+'" '+selected+'>'+y['satuan_besar']+'</option>';
					});

					var total = n['qty']*n['harga'];

					var $row = $("<tr>"
			  					+"<td></td>"
			  					+"<td>"
			  						+n['barcode']
			  						+"<input type='hidden' name='id_barang[]' value='"+n['id_barang']+"' />"
			  						+"<input type='hidden' name='id_satuan[]' value='"+n['id_satuan_terkecil']+"' />"
			  					+"</td>"
			  					+"<td>"+n['nm_barang']+" - <a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>"
			  					+"<td><input type='number' min='1' name='qty[]' onchange='hitung_total(this)' class='form-control' value='"+n['qty']+"' /></td>"
			  					+"<td><select class='form-control' name='konversi[]' style='width: 150px;'>"+opt_select+"</select></td>"
			  					+"<td><input type='text' name='harga[]' onchange='hitung_total(this)' class='form-control' value='"+n['harga']+"' /></td>"
			  					+"<td align='right'>"+($.number(total))+"</td>"
			  				+"</tr>");

					$("#tdet_po tbody").append($row);
				});
				
				//Format Harga
				$("input[name='harga[]']").number(true, 0);
				//Konversi
				$("select[name='konversi[]']").select2({
					placeholder : '-- Pilih --'
				});
				//Hitung Gtotal
				hitung_gtotal();
				//Buat Nomor
				buat_no();
			}
		}
	});
}

//cari barang
function cari_barang(){
	var cari = $("#cr_barang").val();

	$.ajax({
		url : baseurl+"pembelian/po/cari_barang",
		type : "post",
		dataType : "json",
		data : {cr_barang : cari},
		success : function(msg){
			$("table#tbl_barang tbody tr").remove();
			if(msg['type'] == 'error'){
				var $tr = $("<tr>"
							+"<td colspan='5' class='text-center'>"+msg['msg']+"</td>"
							+"</tr>");

				$("table#tdet_po tbody").append($tr);
			}else{

				var opt_select = "";
				$.each(msg, function(i,n){
					$.each(n['konversi'], function(l,m){
						var selected = "";
						if(m['selected'] == 1)
						{
							selected = "selected";
						}

						opt_select += '<option value="'+m['id_konversi']+'" '+selected+'>'+m['satuan_besar']+'</option>';
					});

					var $tr = $("<tr data-konversi='"+opt_select+"'>"
								+"<td><input type='checkbox' name='item_code[]' value='"+n['idbarang_bb']+"'></td>"
								+"<td><input type='hidden' name='item_barcode[]' value='"+n['barcode']+"' />"+(i+1)+"</td>"
								+"<td><input type='hidden' name='item_id_satuan[]' value='"+n['id_satuan_terkecil']+"' />"+n['barcode']+"</td>"
								+"<td><input type='hidden' name='item_nm_barang[]' value='"+n['nm_barang']+"' />"+n['nm_barang']+"</td>"
								+"<td><input type='hidden' name='item_nm_satuan[]' value='"+n['satuan']+"' />"+n['nm_merk_bb']+"</td>"
								+"</tr>");

					$("table#tbl_barang tbody").append($tr);

					opt_select = "";
				});
				
			}
			
		}
	});
}

//Add item to list
function add_items()
{
	var $obj_ids   = $("#tbl_barang input[name='item_code[]']:checked");

	var ids_barang = $("#tbl_barang input[name='item_code[]']:checked").map(function(){
		return $(this).val();
	}).get();

	var ids_barcodes = [];
	var nms_barang = [];
	var ids_satuan = [];
	var nms_satuan = [];
	var options 	= [];

	$.each($obj_ids, function(){
		ids_barcodes.push($(this).closest("tr").find("input[name='item_barcode[]']").val());
		nms_barang.push($(this).closest("tr").find("input[name='item_nm_barang[]']").val());
		ids_satuan.push($(this).closest("tr").find("input[name='item_id_satuan[]']").val());
		nms_satuan.push($(this).closest("tr").find("input[name='item_nm_satuan[]']").val());
		options.push($(this).closest("tr").data("konversi"));
	});

	if(ids_barang.length == 0)
	{
		alertify.error('Silahkan pilih barang yang akan ditambahkan dulu');
        return false;
	}

	var pj_ids = ids_barang.length;

	for (var i = 0; i < pj_ids; i++) {
		
 		var $row = $("<tr>"
		  					+"<td></td>"
		  					+"<td>"
		  						+ids_barcodes[i]
		  						+"<input type='hidden' name='id_barang[]' value='"+ids_barang[i]+"' />"
		  						+"<input type='hidden' name='id_satuan[]' value='"+ids_satuan[i]+"' />"
		  					+"</td>"
		  					+"<td>"+nms_barang[i]+" - <a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>"
		  					+"<td><input type='number' min='1' name='qty[]' onchange='hitung_total(this)' class='form-control' value='1' /></td>"
		  					+"<td><select class='form-control' name='konversi[]' style='width: 150px;'>"+options[i]+"</select></td>"
		  					+"<td><input type='text' name='harga[]' onchange='hitung_total(this)' class='form-control' value='0' /></td>"
		  					+"<td align='right'>0</td>"
		  				+"</tr>");

		var pj = $("#tdet_po tr").length;
		var cek = false;

		if(pj > 3){
			for (var j = 0; j <= pj - 2; j++) {
				var kode = $("#tdet_po tr:eq("+(j+1)+")").find("input[name='id_barang[]']").val();

				if(kode != "" && kode == ids_barang[i]){
					var qty = parseFloat($("#tdet_po tr:eq("+(j+1)+")").find("input[name='qty[]']").val());

					$("#tdet_po tr:eq("+(j+1)+")").find("input[name='qty[]']").val(qty+1);
					
					cek = true;
					break;
				}

			}
			
		}

		if(cek == false){
			$("#tdet_po tbody").append($row);
		}	
	}
	//Format Harga
	$("input[name='harga[]']").number(true, 0);

	$("#modal-barang").modal("hide");
	$("#tbl_barang input[type='checkbox']").prop('checked', false);
	//Konversi
	$("select[name='konversi[]']").select2({
		placeholder : '-- Pilih --'
	});
	//Buat Nomor
	buat_no();
}

//Scan Barcoce
function scan_barcode(){
	var barcode = $("#barcode").val().trim();

	if(!barcode){
		return false;
	}

	$.ajax({
		url : baseurl+"pembelian/po/scan_barcode",
		type : "post",
		dataType : "json",
		data : {barcode : barcode},
		success : function(msg){
			//clear barcode
			$("#barcode").val("");

			if(msg['type'] == 'success'){ //Sukses
				$("#barcode_input").removeClass("has-error").addClass("has-success");
				$("#barcode_msg").removeClass("has-red").addClass("text-green");
				$("#barcode_msg").text("Barcode : "+msg['barcode']+" ditemukan.");

				var addToList = function(){
					var pj = $("#tdet_po tr").length;
					var cek = false;
					//Cek jika item sudah ada pada daftar
					if(pj > 3){
						for (var j = 0; j <= pj - 2; j++) {
							var kode = $("#tdet_po tr:eq("+(j+1)+")").find("input[name='id_barang[]']").val();

							if(kode != "" && kode == msg['data'][0]['idbarang_bb']){
								var qty = parseFloat($("#tdet_po tr:eq("+(j+1)+")").find("input[name='qty[]']").val());

								$("#tdet_po tr:eq("+(j+1)+")").find("input[name='qty[]']").val(qty+1);
								//Trigger Change
								$("input[name='id_barang[]'][value='"+msg['data'][0]['idbarang_bb']+"']").closest("tr").find("input[name='qty[]']").trigger("change");
								
								cek = true;
								break;
							}

						}
						
					}

					if(cek == false){
						var opt_select = "";

						$.each(msg['data'][0]['konversi'], function(i,n){
							var selected = "";
							if(n['selected'] == 1)
							{
								selected = "selected";
							}

							opt_select += '<option value="'+n['id_konversi']+'" '+selected+'>'+n['satuan_besar']+'</option>';
						});

						var $row = $("<tr>"
				  					+"<td></td>"
				  					+"<td>"
				  						+msg['data'][0]['barcode']
				  						+"<input type='hidden' name='id_barang[]' value='"+msg['data'][0]['idbarang_bb']+"' />"
				  						+"<input type='hidden' name='id_satuan[]' value='"+msg['data'][0]['id_satuan_terkecil']+"' />"
				  					+"</td>"
				  					+"<td>"+msg['data'][0]['nm_barang']+" - <a href='#' onclick='remove_item(this)'><i class='fa fa-remove text-red'></i> hapus</a></td>"
				  					+"<td><input type='number' min='1' name='qty[]' onchange='hitung_total(this)' class='form-control' value='1' /></td>"
				  					+"<td><select class='form-control' name='konversi[]' style='width: 150px;'>"+opt_select+"</select></td>"
				  					+"<td><input type='text' name='harga[]' onchange='hitung_total(this)' class='form-control' value='0' /></td>"
				  					+"<td align='right'>0</td>"
				  				+"</tr>");

						$("#tdet_po tbody").append($row);
					}

					//Format Harga
					$("input[name='harga[]']").number(true, 0);

					$("#modal-barang").modal("hide");
					$("#tbl_barang input[type='checkbox']").prop('checked', false);
					//Konversi
					$("select[name='konversi[]']").select2({
						placeholder : '-- Pilih --'
					});
					
					//Buat Nomor
					buat_no();
				}

				addToList();

			}else{ //gagal
				$("#barcode_input").removeClass("has-success").addClass("has-error");
				$("#barcode_msg").removeClass("text-green").addClass("text-red");
				$("#barcode_msg").text("Barcode : "+msg['barcode']+" tidak ditemukan.");
			}
		}
	});
}

//Remove Item
function remove_item(obj){
	$(obj).closest("tr").animate({backgroundColor:'red'}, 1000).fadeOut(1000,function() {
	    $(obj).closest("tr").remove();
	    //Hitung Total
	    hitung_total(obj);
	});
}

//hitung total
function hitung_total(obj){
	var qty 	= parseFloat($(obj).closest("tr").find("input[name='qty[]']").val());
	var harga 	= parseFloat($(obj).closest("tr").find("input[name='harga[]']").val().replace(/,/g,""));

	var total = 0;
	if(qty > 0 && harga > 0)
	{
		total = qty * harga;
	}

	$(obj).closest("tr").find("td:last").text($.number(total, 0));

	hitung_gtotal();
}

//hitung gtotal
function hitung_gtotal(){
	var pj_table = $("#tdet_po tr").length;

	var gtotal = 0;
	for (var i = 1; i <= pj_table-3; i++) {
		var sub_total = parseFloat($("#tdet_po tr:eq("+i+") td:last").text().replace(/,/g,""));
		gtotal += sub_total;
	}

	$("#lblgtotal").text($.number(gtotal));
}

//buat nomor
function buat_no(){
	var pj_table = $("#tdet_po tr").length;
	for (var i = 1; i <= pj_table-3; i++) {
		$("#tdet_po tr:eq("+i+") td:first").text(i);
	}
}