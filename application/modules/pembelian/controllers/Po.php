<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2017, Cokeshome
 * 
 * This is controller for Po
 */

class Po extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "PO.View";
    protected $addPermission    = "PO.Add";
    protected $managePermission = "PO.Manage";
    protected $deletePermission = "PO.Delete";

    protected $prefixKey        = "PO";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('pembelian/pembelian');
        $this->load->model(array('pembelian/po_model',
                                'pembelian/po_detail_model',
                                'pembelian/po_permintaan_model',
                                'pembelian/supplier_model',
                                'identitas_model',
                                'pembelian/barang_model',
                                'pembelian/pembelian_model',
                                'pembelian/gudang_order_model',
                                'pembelian/gudang_order_detail_model'
                                ));

        $this->template->title(lang('pembelian_po_title_manage'));
		$this->template->page_icon('fa fa-tags');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if (isset($_POST['delete']) && has_permission($this->deletePermission))
        {
            $checked = $this->input->post('checked');
            $jml_success = 0;
            if($checked)
            {
                foreach ($checked as $key => $pid) {
                    if($this->cek_hapus($pid))
                    {
                        $result      = $this->po_model->delete($pid);
                        $query_hapus = $this->db->last_query();
                        //Update Status Request
                        $ids_request = $this->po_permintaan_model
                                                ->select("id_req")
                                                ->find_all_by('id_po', $pid);
                        if($ids_request){
                            foreach ($ids_request as $key => $val) {
                                $this->gudang_order_model->update($val->id_req, ['st_req' => 0]);
                            }
                        }
                        
                        $total      = $this->po_model->find($pid)->total;

                        if($result)
                        {
                            $keterangan = "SUKSES, hapus data PO dengan ID : ".$pid;
                            $status     = 1;
                            $jml_success++;
                        }
                        else
                        {
                            $keterangan = "GAGAL, hapus data PO dengan ID : ".$pid;
                            $status     = 0;
                        } 
                    }
                    else
                    {
                        $err        = "karena sudah ada proses pembelian dengan PO ini";
                        $keterangan = "GAGAL, hapus data PO dengan ID : ".$pid." karena sudah ada proses pembelian dengan PO ini";
                        $status     = 0;
                    }

                    $nm_hak_akses   = $this->deletePermission; 
                    $kode_universal = $pid;
                    $jumlah         = $total;
                    $sql            = $query_hapus;

                    simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                }

                if ($jml_success > 0)
                {
                    $this->template->set_message($jml_success .' '. lang('pembelian_deleted_po') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('pembelian_del_po_failure') . (isset($err) ? $err." " : "") . $this->po_model->error, 'error');
                }
            }
            else
            { //No selected
                $this->template->set_message(lang('pembelian_del_po_error'), 'error');
            }

            unset($_POST['delete']);
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search     = isset($_POST['table_search']) ? $this->input->post('table_search') :'';
            $tgl_awal   = isset($_POST['tgl_awal']) ? date_ymd($this->input->post('tgl_awal')) : '';
            $tgl_akhir  = isset($_POST['tgl_awal']) ? date_ymd($this->input->post('tgl_akhir')) : '';
            $idsupplier= isset($_POST['idsupplier']) ? $this->input->post('idsupplier') :'';
        }
        else
        {
            $search     = isset($_GET['search']) ? $this->input->get('search') :'';
            $tgl_awal   = isset($_GET['tgl_awal']) ? date_ymd($this->input->get('tgl_awal')) : '';
            $tgl_akhir  = isset($_GET['tgl_awal']) ? date_ymd($this->input->get('tgl_akhir')) : '';
            $idsupplier= isset($_GET['supplier']) ? $this->input->get('supplier') :'';
        }

        $filter = "";
        if($search!="")
        {
            $filter .= "?search=".$search;
        }

        $addWhere = "";
        if($tgl_awal != '' && $tgl_akhir != '')
        {
            $filter   .= "&tgl_awal=".$tgl_awal."&tgl_akhir=".$tgl_akhir;
            $addWhere .= " AND ( date(tanggal_kirim) >='".$tgl_awal."' AND date(tanggal_kirim) <='".$tgl_akhir."')";
        }
        else
        {
            $tgl_awal = date('Y-m-01');
            $tgl_akhir = date('Y-m-d');
            
            $filter   .= "&tgl_awal=".$tgl_awal."&tgl_akhir=".$tgl_akhir;
            $addWhere .= " AND ( date(tanggal_kirim) >='".$tgl_awal."' AND date(tanggal_kirim) <='".$tgl_akhir."')";
        }

        if($idsupplier !='')
        {
            $filter .= "&supplier=".$idsupplier;
            $addWhere .= " AND po_supplier.idsupplier='".$idsupplier."'";
        }

        $search2 = $this->db->escape_str($search);
        
        $where = "`po_supplier.deleted` = 0 $addWhere
                AND (`no_po` LIKE '%$search2%' ESCAPE '!'
                OR `supplier.nm_supplier` LIKE '%$search2%' ESCAPE '!'
                OR `tanggal_kirim` LIKE '%$search2%' ESCAPE '!')";
        
        $total = $this->po_model
                    ->join("supplier","po_supplier.id_supplier = supplier.idsupplier")
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->po_model
                    ->select(array("po_supplier.*","supplier.nm_supplier"))
                    ->join("supplier","po_supplier.id_supplier = supplier.idsupplier","left")
                    ->where($where)
                    ->order_by('tanggal_kirim','DESC')
                    ->limit($limit, $offset)->find_all();

        $supplier = $this->supplier_model->order_by('nm_supplier', 'ASC')
                                        ->find_all_by(['deleted' => 0]);

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('tgl_awal', $tgl_awal);
        $this->template->set('tgl_akhir', $tgl_akhir);
        $this->template->set('idsupplier', $idsupplier);
        $this->template->set("numb", $offset+1);

        $this->template->set('supplier', $supplier);

        $assets = array('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/dist/css/select2.min.css',
                        'plugins/select2/dist/js/select2.min.js',
                        'plugins/number/jquery.number.js',
                        'pembelian/assets/js/po.js'
                        );

        add_assets($assets);

        $this->template->title(lang('pembelian_po_title_manage'));
        $this->template->render("po/index"); 
    }

    protected function cek_hapus($id_po)
    {
        $cek = $this->pembelian_model->find_by(array('id_po' => $id_po));

        if($cek)
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    public function create()
    {
        $this->auth->restrict($this->addPermission);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_po())
            {
              $this->template->set_message(lang("pembelian_create_po_success"), 'success');
              //Clear Session
              $this->session->unset_userdata('po_supplier');
              redirect('pembelian/po');
            }
        }

        //cek session data
        $data = $this->session->userdata('po_supplier');
        $kode = $this->buat_kode($this->prefixKey);
        if(!$data)
        {
            $data = array('po_supplier' => array(
                                        'no_po' => $kode,
                                        'items'     => array(),
                                        'gtotal'    => 0,
                                        'tanggal_kirim' => '',
                                        'idsupplier' => '',
                                        'selected_no_req' => array()
                                        ));

            $this->session->set_userdata($data);
            $data = $this->session->userdata('po_supplier');
        }

        if($data['no_po'] != $kode)
        {
            $data['no_po'] = $kode;

            $this->session->set_userdata(array('po_supplier' => $data));
            $data = $this->session->userdata('po_supplier');
        }

        $supplier = $this->supplier_model->order_by('nm_supplier', 'ASC')
                                        ->find_all_by(['deleted' => 0]);

        $no_reqs = $this->gudang_order_model->order_by('created_on', 'ASC')
                                        ->find_all_by(array('deleted' => 0, 'st_req' => 0));
        $arr_req = array();
        if($no_reqs)
        {
            foreach ($no_reqs as $key => $rq) {
                $arr_req[$rq->id_req] = $rq->no_req;
            }
        }

        $this->template->set('data', $data);
        $this->template->set('supplier', $supplier);
        $this->template->set('no_reqs', $arr_req);

        $assets = array('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/dist/css/select2.min.css',
                        'plugins/select2/dist/js/select2.full.js',
                        'plugins/number/jquery.number.js',
                        'pembelian/assets/js/po.js'
                        );

        add_assets($assets);
        
        $this->template->title(lang('pembelian_po_title_new'));
        $this->template->render("po/po_form");
    }

    protected function buat_kode($prefix = "PO")
    {
        $dt_po = $this->po_model->order_by('created_on','DESC')->limit(1)->find_all();
        $last_kode = "";
        if($dt_po)
        {
            foreach ($dt_po as $key => $dp) {
                $last_kode = $dp->no_po;
            }
        }

        if($last_kode)
        {
            $tkode = explode("-", $last_kode);
            if(count($tkode) == 2)
            {
                $tkode = intval($tkode[1]);
            }
            else
            {
                $tkode = 0;
            }

            $kode = strtoupper($prefix)."-".($tkode+1);
        }
        else
        {
            $kode = strtoupper($prefix)."-1";
        }

        return $kode;
    }

    public function get_det_permintaan()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('pembelian_only_ajax'), 'error');
            redirect('pembelian/po');
        }

        $id_req = $this->input->post('no_req');
        if(!is_array($id_req) || count($id_req) == 0){
            $return = array('type' => 'error', 'msg' => lang('pembelian_no_permintaan_selected'));
            echo json_encode($return);
            die();
        }

        $detail = $this->gudang_order_detail_model->select("gudang_order_detail.*, sum(gudang_order_detail.qty) as qty, barang.id_satuan_terkecil, barcode, nm_barang")
                                                ->join("barang","gudang_order_detail.id_barang=barang.idbarang_bb","left")
                                                ->order_by("nm_barang","ASC")
                                                ->where_in("gudang_order_detail.id_req", $id_req)
                                                ->group_by(array("gudang_order_detail.id_barang","gudang_order_detail.id_konversi","gudang_order_detail.harga"))
                                                ->find_all();
        if($detail)
        {
            foreach ($detail as $key => $dt) {
                $po_detail = $this->po_detail_model->select("sum(po_detail.qty) as qty")
                                            ->join("po_supplier","po_detail.id_po=po_supplier.id_po")
                                            ->join("po_permintaan","po_supplier.id_po=po_permintaan.id_po")
                                            ->where("po_supplier.deleted", 0)
                                            ->where("po_permintaan.id_req", $dt->id_req)
                                            ->where("po_detail.id_barang", $dt->id_barang)
                                            ->where("po_detail.id_konversi", $dt->id_konversi)
                                            ->find_all();
                if($po_detail){
                    $detail[$key]->qty -= $po_detail[0]->qty;
                    if($detail[$key]->qty <= 0){
                        unset($detail[$key]);
                        continue;
                    }
                }

                $detail[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
            }
        }
        
        if($detail)
        {
            $return = array('type' => 'success', 'mesg' => '', 'data' => $detail);
        }
        else
        {
            $return = array('type' => 'error', 'msg' => lang('pembelian_no_records_found'));
        }

        echo json_encode($return);
    }

    //View Detail
    public function view($id_po = '')
    {
        $this->auth->restrict($this->viewPermission);

        if (!$id_po)
        {
            $this->template->set_message(lang("pembelian_invalid_id"), 'error');
            redirect('pembelian/po');
        }

        $data   = $this->po_model->find($id_po);
        $detail = $this->po_detail_model->select("po_detail.*, konversi_satuan.satuan_besar as satuan, barcode, nm_barang")
                                            ->join('barang','po_detail.id_barang = barang.idbarang_bb',"left")
                                            ->join('konversi_satuan','po_detail.id_konversi = konversi_satuan.id_konversi',"left")
                                            ->order_by('nm_barang', 'ASC')
                                            ->find_all_by('id_po', $id_po);
        if($detail)
        {
            foreach ($detail as $key => $dt) {
                $dt_det = hitung_ke_satuan_besar($dt->id_konversi, $dt->qty_terpenuhi);
                $detail[$key]->qty2 = $dt_det['qty'];
            }
        }

        $pembelian = $this->pembelian_model->find_all_by(['id_po' => $id_po, 'deleted' => 0]);

        $nm_supplier = $this->supplier_model->find($data->id_supplier)->nm_supplier;

        $no_reqs  = $this->po_permintaan_model->select("group_concat(no_req separator ', ') as no_reqs")
                                            ->join("gudang_order","po_permintaan.id_req=gudang_order.id_req")
                                            ->find_by('id_po', $id_po);

        $this->template->set('data', $data);
        $this->template->set('detail', $detail);
        $this->template->set('pembelian', $pembelian);
        $this->template->set('nm_supplier', $nm_supplier);
        $this->template->set('no_reqs', $no_reqs);

        $this->template->title(lang('pembelian_po_title_view'));
        $this->template->render('po/view_po');
    }

    public function cancel()
    {
        $this->auth->restrict($this->addPermission);

        $this->session->unset_userdata('po_supplier');
        $this->template->set_message(lang('pembelian_po_canceled'), 'success');

        redirect('pembelian/po');
    }

    public function cari_barang()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('pembelian_only_ajax'), 'error');
            redirect('pembelian/po');
        }

        $cari = $this->db->escape_str($this->input->post('cr_barang'));

        $data = $this->barang_model->select(array("barang.*","satuan_terkecil.alias as satuan","nm_merk_bb"))
                                    ->join("satuan_terkecil","barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","left")
                                    ->join("merk_bb","barang.idmerk_bb=merk_bb.idmerk_bb","left")
                                    ->where("`barang.deleted` = 0
                                            AND (barcode like '%$cari%' OR nm_barang like '%$cari%')")
                                    ->order_by('nm_barang', 'ASC')
                                    ->find_all();
        if($data)
        {
            foreach ($data as $key => $dt) {
                $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
            }
        }

        echo json_encode($data);
    }

    public function scan_barcode()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('pembelian_only_ajax'), 'error');
            redirect('pembelian/po');
        }

        $barcode = $this->input->post('barcode');

        if($barcode != "")
        {
            $data = $this->barang_model->select(array("barang.*","satuan_terkecil.alias as satuan"))
                                        ->join("satuan_terkecil","barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","left")
                                        ->find_all_by(array('barang.deleted' => 0, 'barcode' => $barcode));
            if($data)
            {
                foreach ($data as $key => $dt) {
                    $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
                }

                $return = array('type' => 'success', 'barcode' => $barcode, 'data' => $data);
            }
            else
            {
                $return = array('type' => 'error', 'barcode' => $barcode, 'data' => 0);
            }
        }
        else
        {
            $return = array('type' => 'error', 'barcode' => $barcode, 'data' => 0);
        }

        echo json_encode($return);
    }

    public function update_po()
    {
        $this->auth->restrict($this->managePermission);

        if(!$this->input->is_ajax_request())
        {
            die(json_encode(['type' => 'error', 'msg' => $this->template->set_message(lang('pembelian_only_ajax'), 'error')]));
        }

        $no_po  = $this->input->post('no_po');
        $status = $this->input->post('st');

        if($no_po !='' && $status !='')
        {
            $update = $this->po_model->update($no_po, ['st_close' => $status]);
            if($update)
            {
                $return = ['type' => 'success', 'msg' => lang('pembelian_st_po_success'), 'label' => $status == 0 ? lang('pembelian_st_open') : lang('pembelian_st_closed')];
                $keterangan = 'SUKSES, update status PO dari '.($status == 0 ? lang('pembelian_st_closed').' menjadi '.lang('pembelian_st_open') : lang('pembelian_st_open').' menjadi '.lang('pembelian_st_closed'));
                $status = 1;
            }
            else
            {
                $return = ['type' => 'error', 'msg' => lang('pembelian_st_po_failed'), 'label' => $status == 0 ? lang('pembelian_st_closed') : lang('pembelian_st_open')];
                $keterangan = 'GAGAL, update status PO dari '.($status == 0 ? lang('pembelian_st_closed').' menjadi '.lang('pembelian_st_open') : lang('pembelian_st_open').' menjadi '.lang('pembelian_st_closed'));
                $status = 0;
            }
        }
        else
        {
            $return = ['type' => 'error', 'msg' => lang('pembelian_st_po_failed'), 'label' => $status == 0 ? lang('pembelian_st_closed') : lang('pembelian_st_open')];
            $status = 0;
            $keterangan = 'GAGAL, update status PO, invalid input';
        }

        unset($_POST);

        //Save Log
        $nm_hak_akses   = $this->managePermission; 
        $kode_universal = $no_po;
        $jumlah         = 1;
        $sql            = $this->db->last_query();

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        echo json_encode($return);
    }

    //Simpan PO
    protected function save_po()
    {
        $no_po      = $this->input->post('no_po');
        $tanggal    = $this->input->post('tanggal_kirim');
        $id_supplier  = $this->input->post('id_supplier');
        $no_reqs    = $this->input->post('no_req');

        $id_barang  = $this->input->post('id_barang');
        $id_satuan  = $this->input->post('id_satuan');
        $konversi   = $this->input->post('konversi');
        $qty        = $this->input->post('qty');
        $harga      = $this->input->post('harga');

        if(count($id_barang) == 0)
        {
            $this->template->set_message(lang('pembelian_no_item'), 'error');
            return FALSE;
        }

        $this->form_validation->set_rules('no_po', 'lang:pembelian_no_po', 'required');
        $this->form_validation->set_rules('tanggal_kirim', 'lang:pembelian_tanggal_kirim', 'required');
        $this->form_validation->set_rules('id_supplier','lang:pembelian_nm_supplier','callback_default_select');

        $this->form_validation->set_message('default_select', 'You should choose the "%s" field');

        if ($this->form_validation->run() === FALSE) {

            //Masukkan item kedalam session
            $data = $this->session->userdata('po_supplier');
            $items = $data['items'];

            $data_items = array();
            $gtotal     = 0;        
            $sub_total  = 0;        
            foreach ($id_barang as $key => $kode) {

                $dt_barang = $this->barang_model->find($kode);

                $sub_total = intval($qty[$key]) * intval(str_replace(",", "", $harga[$key]));

                $data_items[] = array('id_barang'   => $kode,
                                    'barcode'       => $dt_barang->barcode,
                                    'nm_barang'     => $dt_barang->nm_barang,
                                    'id_konversi'   => $konversi[$key],
                                    'id_satuan_terkecil' => $id_satuan[$key],
                                    'konversi'      => get_konversi($id_satuan[$key]),
                                    'qty'           => $qty[$key],
                                    'harga'         => intval(str_replace(",", "", $harga[$key])),
                                    'total'         => $sub_total
                                );

                $gtotal += $sub_total;
            }

            $data['items']          = $data_items;
            $data['gtotal']         = $gtotal;
            $data['id_supplier']    = $id_supplier;
            $data['tanggal_kirim']  = $tanggal;
            $data['selected_no_req']= $no_reqs;

            $this->session->set_userdata('po_supplier', $data);

            $this->template->set_message(validation_errors(), 'error');
            return FALSE;
        }

        //Simpan PO
        $this->db->trans_start();

        //Hitung g total
        $gtotal = 0;
        foreach ($id_barang as $key => $kode) {

            $sub_total = intval($qty[$key]) * intval(str_replace(",", "", $harga[$key]));

            $gtotal += $sub_total;
        }

        $data_po = array('no_po'        => $no_po,
                        'id_supplier'   => $id_supplier, 
                        'total'         => $gtotal,
                        'tanggal_kirim' => date_ymd($tanggal)
                    );

        $id_po = $this->po_model->insert($data_po);
        $query = $this->db->last_query();
        $result_req = false;

        if ($id_po !== FALSE){
            //Save kode permintaan
            if(count($no_reqs)){
                $insert_no_req = array();
                foreach ($no_reqs as $key => $val) 
                {
                    $insert_no_req[] = array('id_po' => $id_po, 'id_req' => $val);
                }

                if($insert_no_req)
                {
                    $result_req = $this->po_permintaan_model->insert_batch($insert_no_req);
                    $query .= $this->db->last_query();
                }
            }
        }

        $data_det_po = array();
        foreach ($id_barang as $key => $kode) {
            $dt_real_qty = hitung_ke_satuan_kecil($konversi[$key], $qty[$key]);
            $real_qty    = $dt_real_qty['qty'];

            $sub_total = intval($qty[$key]) * intval(str_replace(",", "", $harga[$key]));

            $data_det_po = array('id_detail_po' => gen_primary(),
                                'id_po'         => $id_po,
                                'id_barang'     => $kode,
                                'qty'           => $qty[$key],
                                'id_konversi'   => $konversi[$key],
                                'harga'         => str_replace(",", "", $harga[$key]),
                                'sub_total'     => $sub_total,
                                'real_qty'      => $real_qty
                            );

            $det_po = $this->po_detail_model->insert($data_det_po);
            $query .= $this->db->last_query();
        }
        //Update Status permintaan
        if($result_req)
        {
            foreach ($insert_no_req as $key => $ir) {
                $po_detail = $this->po_detail_model->select("po_detail.id_barang, sum(po_detail.qty) as qty, po_detail.id_konversi")
                                            ->join("po_supplier","po_detail.id_po=po_supplier.id_po")
                                            ->join("po_permintaan","po_supplier.id_po=po_permintaan.id_po")
                                            ->where("po_supplier.deleted", 0)
                                            ->where("po_permintaan.id_req", $ir['id_req'])
                                            ->group_by("po_detail.id_barang")
                                            ->group_by("po_detail.id_konversi")
                                            ->find_all();
                //Cek apakah semua barang telah dibuatkan PO
                if($po_detail){
                    $jml_po  = [];
                    foreach ($po_detail as $key => $pd) {
                        $dt_real_qty = hitung_ke_satuan_kecil($pd->id_konversi, $pd->qty);
                        $real_qty_po = $dt_real_qty['qty'];
                        if(isset($jml_po[$pd->id_barang])){
                            $jml_po[$pd->id_barang] += $real_qty_po;
                        }else{
                            $jml_po[$pd->id_barang]  = $real_qty_po;
                        }
                    }

                    $req_detail = $this->gudang_order_detail_model->select("id_barang, qty, id_konversi")
                                        ->join("gudang_order","gudang_order_detail.id_req = gudang_order.id_req")
                                        ->where("gudang_order_detail.id_req", $ir['id_req'])
                                        ->where("gudang_order.deleted", 0)
                                        ->find_all();
                    // Pengecekan apakah sudah terpenuhi
                    $cek = 0;
                    foreach ($req_detail as $key2 => $rq) {
                        $dt_real_qty = hitung_ke_satuan_kecil($rq->id_konversi, $rq->qty);
                        $real_qty_ck = $dt_real_qty['qty'];

                        if($jml_po[$rq->id_barang] >= $real_qty_ck){
                            $cek++;
                        }
                    }
                    
                    if($cek == count($req_detail) && is_array($req_detail) && count($req_detail) > 0){
                        $this->gudang_order_model->update($ir['id_req'], array('st_req' => 1));    
                        $query .= $this->db->last_query();
                    }   
                }
            }
        }

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->template->set_message($this->po_model->error."\n\n".$this->po_detail_model->error, 'error');
            
            $keterangan = "GAGAL, tambah data PO dengan kode : ".$no_po;
            $status     = 0;
        }
        else
        {
            $keterangan = "SUKSES, tambah data PO dengan kode : ".$no_po;
            $status     = 1;
        }

        //Save Log
        $nm_hak_akses   = $this->addPermission; 
        $kode_universal = $no_po;
        $jumlah         = $gtotal;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $query, $status);

        return $status ? true : false;
    }

    public function default_select($val)
    {
        return $val === "" ? FALSE : TRUE;
    }

    public function cetak($id_po = ""){
        $this->auth->restrict($this->viewPermission);

        if (!$id_po)
        {
            echo lang("pembelian_invalid_id");
            return FALSE;
        }

        $data   = $this->po_model->select("po_supplier.*, supplier.nm_supplier")
                                ->join("supplier","po_supplier.id_supplier=supplier.idsupplier")
                                ->find($id_po);

        $detail = $this->po_detail_model->select("po_detail.*, konversi_satuan.satuan_besar as satuan, barcode, nm_barang")
                                            ->join('barang','po_detail.id_barang = barang.idbarang_bb',"left")
                                            ->join('konversi_satuan','po_detail.id_konversi = konversi_satuan.id_konversi',"left")
                                            ->find_all_by('id_po', $id_po);

        $no_reqs  = $this->po_permintaan_model->select("group_concat(no_req separator ', ') as no_reqs")
                                            ->join("gudang_order","po_permintaan.id_req=gudang_order.id_req")
                                            ->find_by('id_po', $id_po);

        //Identitas
        $identitas    = $this->identitas_model->find(1);
        $idt_pengirim = $this->auth->userdata();

        $this->template->set('data', $data);
        $this->template->set('detail', $detail);
        $this->template->set('no_reqs', $no_reqs);
        $this->template->set('idt', $identitas);
        $this->template->set('pengirim', $idt_pengirim);

        $this->template->set_layout('cetak');
        $this->template->title(lang('pembelian_po_title_view'));
        $this->template->render('po/cetak');
    }
}
?>