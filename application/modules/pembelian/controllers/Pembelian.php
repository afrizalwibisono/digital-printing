<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 * 
 * This is controller for Pembelian
 */

class Pembelian extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Pembelian.View";
    protected $addPermission    = "Pembelian.Add";
    protected $managePermission = "Pembelian.Manage";
    protected $deletePermission = "Pembelian.Delete";
    protected $laporanPermission = "Laporan Pembelian.View";

    protected $prefixKey        = "BL";
    protected $prefixKeyStok    = "STK";

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('pembelian/pembelian');
        $this->load->model(array(
                                'pembelian/po_model',
                                'pembelian/po_detail_model',
                                'konversi_satuan/konversi_satuan_model',
                                'supplier/supplier_model',
                                'identitas_model',
                                'pembelian/barang_model',
                                'pembelian/pembelian_model',
                                'pembelian/pembelian_detail_model',
                                'stok_model',
                                'kedatangan_barang/kedatangan_barang_model',
                                'kedatangan_barang/kedatangan_detail_model'
                            ));

        $this->template->title(lang('pembelian_title_manage'));
        $this->template->page_icon('fa fa-tags');
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);

        if (isset($_POST['delete']) && has_permission($this->deletePermission))
        {
            $checked = $this->input->post('checked');
            if($checked)
            {
                $jml_success = 0;
                foreach ($checked as $key => $pid) {
                    $dt_beli     = $this->pembelian_model->find($pid);
                    $query_hapus = "";

                    if($this->cek_hapus($dt_beli->id_kedatangan))
                    {
                        //Hapus Pembelian
                        $deleted = $this->pembelian_model->delete($pid);
                       
                        if($deleted)
                        {
                            //Simpan Alur Kas jika tunai
                            if($dt_beli->type == 1) //Kode 3 untuk pembelian
                            {
                                simpan_alurkas(3, "Pembatalan Pembayaran pembelian tunai untuk Faktur : ".$dt_beli->no_nota." karena penghapusan data pembelian", $dt_beli->gtotal , 1, $this->addPermission);    
                            }
                            //Update Status PO
                            if($dt_beli->id_po != '')
                            {
                                $det_pembelian = $this->pembelian_detail_model->find_all_by(['id_pembelian' => $pid]);
                                $det_po        = $this->po_detail_model->find_all_by(['id_po' => $dt_beli->id_po]);
                                if(is_array($det_pembelian) && count($det_pembelian) > 0 && is_array($det_po) && count($det_po) > 0)
                                {
                                    $update_po = [];
                                    foreach ($det_pembelian as $key => $dt) {
                                        $dt_real_qty = hitung_ke_satuan_kecil($dt->id_konversi, $dt->qty);
                                        $real_qty    = $dt_real_qty['qty'];

                                        foreach ($det_po as $key2 => $do) {
                                            if($dt->id_barang == $do->id_barang)
                                            {
                                                $update_po[] = ['id_detail_po' => $do->id_detail_po,
                                                                'qty_terpenuhi' => $do->qty_terpenuhi - $real_qty
                                                                ];
                                                break;
                                            }
                                        }
                                    }
                                    //Update Qty Terpenuhi
                                    if($update_po)
                                    {
                                        $this->po_detail_model->update_batch($update_po, 'id_detail_po');
                                        //Update Status Close PO
                                        $cek = $this->po_detail_model
                                                                ->where("real_qty <> qty_terpenuhi")
                                                                ->where(['id_po' => $dt_beli->id_po])
                                                                ->count_all();
                                        if($cek > 0)
                                        {
                                            $this->po_model->update($dt_beli->id_po, ['st_close' => 0]);
                                        }
                                    }
                                }
                            }

                            $keterangan = "SUKSES, hapus data Pembelian dengan ID : ".$pid;
                            $status     = 1;
                            $jml_success++;
                        }
                        else
                        {
                            $keterangan = "GAGAL, hapus data Pembelian dengan ID : ".$pid;
                            $status     = 0;
                        } 
                    }
                    else
                    {
                        $err        = "karena stok sudah berubah";
                        $keterangan = "GAGAL, hapus data Pembelian dengan ID : ".$pid." karena stok sudah berubah";
                        $status     = 0;
                    }

                    $nm_hak_akses   = $this->deletePermission; 
                    $kode_universal = $pid;
                    $jumlah         = $dt_beli->gtotal;
                    $sql            = $query_hapus;

                    simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                }

                if ($jml_success > 0)
                {
                    $this->template->set_message($jml_success .' '. lang('pembelian_deleted') .'.', 'success');
                }
                else
                {
                    $this->template->set_message(lang('pembelian_del_failure') . (isset($err) ? $err." " : "") ."\n\n".$this->pembelian_model->error."\n\n".$this->stok_model->error, 'error');
                } 
            }
            else
            {
                $this->template->set_message(lang('pembelian_del_error'), 'error');
            }

            unset($_POST);
        }//end if

        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';

            $kategory_pencarian = $this->input->post('kategori_pencarian');
            $tipe_beli          = $this->input->post('tipe_beli');
            $st_lunas           = $this->input->post('st_lunas');
            $tgl_awal   = isset($_POST['tgl_awal']) && $_POST['tgl_awal'] !='' ? date_ymd($this->input->post('tgl_awal')) : '';
            $tgl_akhir  = isset($_POST['tgl_akhir']) && $_POST['tgl_akhir'] !='' ? date_ymd($this->input->post('tgl_akhir')) : '';
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';
            $kategory_pencarian = $this->input->get('kategori_pencarian');
            $tipe_beli          = $this->input->get('tipe_beli');
            $st_lunas           = $this->input->get('st_lunas');
            $tgl_awal   = isset($_GET['tgl_awal']) && $_GET['tgl_awal'] !='' ? date_ymd($this->input->get('tgl_awal')) : '';
            $tgl_akhir  = isset($_GET['tgl_akhir']) && $_GET['tgl_akhir'] !='' ? date_ymd($this->input->get('tgl_akhir')) : '';
        }

        $search2 = $this->db->escape_str($search);

        if(is_null($kategory_pencarian))
        {
            $kategory_pencarian = 3;
        }

        $filter = "";
        if($kategory_pencarian != 3)
        {
            $filter = "?search=".urlencode($search)."&kategori_pencarian=".urlencode($kategory_pencarian)."&tipe_beli=".urlencode($tipe_beli)."&st_lunas=".urlencode($st_lunas)."&tgl_awal=".urlencode($tgl_awal)."&tgl_akhir=".urlencode($tgl_akhir);
        }

        switch ($kategory_pencarian) {
            case 0:
                $where = "`pembelian.deleted` = 0 
                        AND pembelian.no_nota='$search2'";
                break;
            case 1:
                $where = "`pembelian.deleted` = 0 
                        AND po_supplier.no_po='$search2'";
                break;
            case 2:
                $where = "`pembelian.deleted` = 0  ";
                if($tipe_beli != 3)
                {
                    $where .= "AND `pembelian`.`type`=$tipe_beli ";
                }

                if($st_lunas !="")
                {
                    $where .= "AND `pembelian`.`st_lunas`=$st_lunas ";
                }

                if($search!="")
                {
                    $where .= "AND `nm_barang` like '%$search2%' ";
                }

                if($tgl_awal != '' && $tgl_akhir != '')
                {
                    $where .= "AND (`pembelian`.`tgl_tran` >= '$tgl_awal' AND `pembelian`.`tgl_tran` <= '$tgl_akhir')";
                }

                break;
            
            default:
                $where = "`pembelian.deleted` = 0 ";
                break;
        }
        
        $total = $this->pembelian_model
                    ->select('count(distinct pembelian.no_nota) as total')
                    ->join('pembelian_detail','pembelian.id_pembelian = pembelian_detail.id_pembelian')
                    ->join('barang','pembelian_detail.id_barang = barang.idbarang_bb')
                    ->join('po_supplier','pembelian.id_po=po_supplier.id_po','left')
                    ->where($where)
                    ->find_all();
        if($total)
        {
            $total = $total[0]->total;
        }
        else
        {
            $total = 0;
        }

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $data = $this->pembelian_model
                    ->select(array("pembelian.*","supplier.nm_supplier","no_po"))
                    ->join("supplier","pembelian.id_supplier = supplier.idsupplier","left")
                    ->join('pembelian_detail','pembelian.id_pembelian = pembelian_detail.id_pembelian')
                    ->join('barang','pembelian_detail.id_barang = barang.idbarang_bb')
                    ->join('po_supplier','pembelian.id_po=po_supplier.id_po','left')
                    ->where($where)
                    ->group_by('pembelian.no_nota')
                    ->order_by('pembelian.created_on','DESC')
                    ->limit($limit, $offset)->find_all();

        $assets = array('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                        'plugins/select2/css/select2.min.css',
                        'plugins/select2/js/select2.min.js',
                        'pembelian/assets/js/pembelian_index.js'
                        );

        add_assets($assets);

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('tgl_awal', $tgl_awal);
        $this->template->set('tgl_akhir', $tgl_akhir);
        $this->template->title(lang('pembelian_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

    protected function cek_hapus($id_kedatangan = '')
    {
        if($id_kedatangan == '')
        {
            return FALSE;
        }
        
        $data = $this->stok_model->find_all_by(array('no_faktur' => $id_kedatangan));
        if(!$data)
        {
            return TRUE;
        }

        $st_bisa = TRUE;
        foreach ($data as $key => $dt) {
            if($dt->stok_datang != $dt->stok_update)
            {
                $st_bisa = FALSE;
                break;
            }
        }

        return $st_bisa;
    }

    public function create()
    {
        $this->auth->restrict($this->addPermission);
                    
        if (isset($_POST['save']))
        {
            if ($this->save_pembelian())
            {
              $this->template->set_message(lang("pembelian_create_success"), 'success');
              redirect('pembelian/pembelian/index');
            }
        }

        //cek session data
        $data = $this->session->userdata('dt_pembelian');
        if(!$data)
        {
            $data = array('dt_pembelian' => array(
                                        'no_faktur' => '',
                                        'id_kedatangan' => '',
                                        'no_po'     => '',
                                        'tgl_tran'  => '',
                                        'type'      => 1,
                                        'tempo_hari'=> '',
                                        'tempo_tanggal'=> '',
                                        'items'     => array(),
                                        'total'    => 0,
                                        'diskon'    => 0,
                                        'potongan'  => 0,
                                        'pajak'     => 0,
                                        'gtotal'    => 0,
                                        'id_supplier' => '',
                                        'st_pajak'  => 1,
                                        'no_faktur_pajak' => ''
                                        ));

            $this->session->set_userdata($data);
            $data = $this->session->userdata('dt_pembelian');
        }

        $supplier = $this->supplier_model->order_by('nm_supplier', 'ASC')
                                        ->find_all_by(['deleted' => 0]);

        $this->template->set('data', $data);
        $this->template->set('supplier', $supplier);

        $assets = array(
                    'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                    'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                    'plugins/select2/css/select2.min.css',
                    'plugins/select2/js/select2.min.js',
                    'plugins/number/jquery.number.js',
                    'pembelian/assets/js/pembelian.js'
                    );

        add_assets($assets);

        $this->template->title(lang('pembelian_title_new'));
        $this->template->render('pembelian_form');
    }

    public function load_po()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('pembelian_only_ajax'), 'error');
            redirect('pembelian/po');
        }

        $kode_po = $this->input->post('kode_po');

        $data       = $this->po_model->find($kode_po);
        if($data)
        {
            $detail_po  = $this->po_detail_model->select("detail_po.*, barang.id_satuan_terkecil, satuan_terkecil.alias as satuan, barcode, nm_barang")
                                            ->join('barang','detail_po.id_barang = barang.idbarang_bb')
                                            ->join('satuan_terkecil','barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil')
                                            ->find_all_by('no_po', $kode_po);

            if($detail_po)
            {
                foreach ($detail_po as $key => $dt) {
                    $detail_po[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
                }
            }

            $data->detail = $detail_po;
        }

        $return = array('po_no' => $kode_po, 'data' => $data);

        echo json_encode($return);

    }

    public function load_kedatangan()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('pembelian_only_ajax'), 'error');
            redirect('pembelian/pembelian/create');
        }

        $kode = $this->input->post('kode');
        $data = $this->kedatangan_barang_model->find_by('no_nota', $kode);
        if($data)
        {
            //Cek Pembelian
            $cek = $this->pembelian_model->find_by(['id_kedatangan' => $data->id_kedatangan, 'deleted' => 0]);
            if($cek)
            {
                $return = array('kode' => $kode, 'data' => FALSE);

                echo json_encode($return);
                die();
            }

            $detail  = $this->kedatangan_detail_model->select("kedatangan_detail.*, barang.id_satuan_terkecil, satuan_terkecil.alias as satuan, barcode, nm_barang")
                                            ->join('kedatangan_barang','kedatangan_detail.id_kedatangan = kedatangan_barang.id_kedatangan')
                                            ->join('barang','kedatangan_detail.id_barang = barang.idbarang_bb')
                                            ->join('satuan_terkecil','barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil')
                                            ->order_by('nm_barang', 'ASC')
                                            ->find_all_by('kedatangan_barang.no_nota', $kode);

            if($detail)
            {
                foreach ($detail as $key => $dt) {
                    $detail[$key]->konversi = get_konversi($dt->id_satuan_terkecil, $dt->id_konversi);
                }
            }

            $data->detail = $detail;
        }

        $return = array('kode' => $kode, 'data' => $data);

        echo json_encode($return);

    }

    //View Detail
    public function view($id_pembelian = '')
    {
        $this->auth->restrict($this->viewPermission);

        if (!$id_pembelian)
        {
            $this->template->set_message(lang("pembelian_invalid_id"), 'error');
            redirect('pembelian/pembelian');
        }

        if(isset($_POST['save']))
        {
            $no_faktur_pajak = $this->input->post('no_faktur_pajak');
            $update = $this->pembelian_model->update($id_pembelian, ['no_faktur_pajak' => $no_faktur_pajak]);
            if($update)
            {
                $this->template->set_message(lang('pembelian_faktur_success'), 'success');
            }
            else
            {
                $this->template->set_message(lang('pembelian_faktur_failed'), 'success');
            }
            unset($_POST['save']);
        }

        $data   = $this->pembelian_model
                                    ->select("pembelian.*, no_po")
                                    ->join('po_supplier','pembelian.id_po=po_supplier.id_po','left')
                                    ->find($id_pembelian);

        $detail = $this->pembelian_detail_model->select("pembelian_detail.*, konversi_satuan.satuan_besar as nm_satuan, barcode, nm_barang")
                                            ->join('barang','pembelian_detail.id_barang = barang.idbarang_bb')
                                            ->join('konversi_satuan','pembelian_detail.id_konversi = konversi_satuan.id_konversi')
                                            ->order_by("nm_barang", "ASC")
                                            ->find_all_by('id_pembelian', $id_pembelian);

        $supplier = $this->supplier_model->order_by('nm_supplier', 'ASC')
                                        ->find_all_by('deleted', 0);

        $this->template->set('data', $data);
        $this->template->set('detail', $detail);
        $this->template->set('supplier', $supplier);

        $this->template->title(lang('pembelian_title_view'));
        $this->template->render('view_pembelian');
    }

    public function cancel()
    {
        $this->auth->restrict($this->addPermission);

        $this->session->unset_userdata('dt_pembelian');
        $this->template->set_message(lang('pembelian_canceled'), 'success');

        redirect('pembelian/pembelian/index');
    }

    public function cari_barang()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('pembelian_only_ajax'), 'error');
            redirect('pembelian/pembelian');
        }

        $cari = $this->db->escape_str($this->input->post('cr_barang'));

        $data = $this->barang_model->select(array("barang.*","satuan_terkecil.alias as satuan"))
                                    ->join("satuan_terkecil","barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","left")
                                    ->where("`barang.deleted` = 0
                                            AND (barcode like '%$cari%' OR nm_barang like '%$cari%')")
                                    ->order_by('nm_barang', 'ASC')
                                    ->find_all();
        if($data)
        {
            foreach ($data as $key => $dt) {
                $data[$key]->konversi = get_konversi($dt->id_satuan_terkecil);
            }
        }

        echo json_encode($data);
    }

    public function scan_barcode()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('pembelian_only_ajax'), 'error');
            redirect('pembelian/pembelian');
        }

        $barcode = $this->input->post('barcode');

        if($barcode != "")
        {
            $data = $this->barang_model->select(array("barang.*","satuan_terkecil.alias as satuan"))
                                        ->join("satuan_terkecil","barang.id_satuan_terkecil = satuan_terkecil.id_satuan_terkecil","left")
                                        ->find_by(array('barang.deleted' => 0, 'barcode' => $barcode));
            if($data)
            {
                $data->konversi = get_konversi($data->id_satuan_terkecil);

                $return = array('type' => 'success', 'barcode' => $barcode, 'data' => $data);
            }
            else
            {
                $return = array('type' => 'error', 'barcode' => $barcode, 'data' => 0);
            }
        }
        else
        {
            $return = array('type' => 'error', 'barcode' => $barcode, 'data' => 0);
        }

        echo json_encode($return);
    }

    //Simpan PO
    protected function save_pembelian()
    {
        $no_faktur = $this->input->post('no_faktur');
        $id_kedatangan = $this->input->post('id_kedatangan');
        $no_po      = $this->input->post('no_po');
        $type       = $this->input->post('type');
        $tgl_tran   = $this->input->post('tgl_tran');
        $tempo_hari = $this->input->post('tempo_hari');
        $tgl_tempo  = $this->input->post('tempo_tanggal');
        $id_supplier  = $this->input->post('id_supplier');
        // $diskon     = $this->input->post('diskon');
        // $potongan   = $this->input->post('potongan');
        $diskon     = 0;
        $potongan   = 0;
        $pajak      = $this->input->post('pajak');
        $st_pajak   = $this->input->post('st_pajak');
        $no_faktur_pajak    = $this->input->post('no_faktur_pajak');
        $is_include_pajak   = $this->input->post('is_include_pajak');

        $id_barang  = $this->input->post('id_barang');
        $konversi   = $this->input->post('konversi');
        $qty        = $this->input->post('qty');
        $harga      = $this->input->post('harga');
        $item_diskon   = $this->input->post('item_diskon');
        $item_potongan = $this->input->post('item_potongan');
        $st_bonus   = $this->input->post('st_bonus');

        if(count($id_barang) == 0)
        {
            $this->template->set_message(lang('pembelian_no_item_beli'), 'error');
            return FALSE;
        }

        $this->form_validation->set_rules('no_faktur', 'lang:pembelian_no_faktur', 'required|trim|unique2[pembelian.no_nota]');
        $this->form_validation->set_rules('tgl_tran', 'lang:pembelian_tanggal', 'required');
        $this->form_validation->set_rules('no_po', 'lang:pembelian_no_po', 'trim');
        $this->form_validation->set_rules('type', 'lang:pembelian_type', 'required');

        if(isset($_POST['type']) && $_POST['type'] == 0)
        {
            $this->form_validation->set_rules('tempo_hari', 'lang:pembelian_tempo_hari', 'required|numeric|trim');
            $this->form_validation->set_rules('tempo_tanggal', 'lang:pembelian_tempo_tanggal', 'required|trim');
        }

        if($no_po == "")
        {
            $this->form_validation->set_rules('id_supplier','lang:pembelian_nm_supplier','callback_default_select');
        }

        $this->form_validation->set_rules('diskon','lang:pembelian_diskon','greater_than_equal_to[0]|less_than_equal_to[100]');
        $this->form_validation->set_rules('potongan','lang:pembelian_potongan','greater_than_equal_to[0]');
        $this->form_validation->set_rules('pajak','lang:pembelian_pajak','greater_than_equal_to[0]');
        $this->form_validation->set_rules('no_faktur_pajak','lang:pembelian_faktur_pajak','trim');

        $this->form_validation->set_message('default_select', 'You should choose the "%s" field');

        if ($this->form_validation->run() === FALSE) {
            $this->save_session();
            return FALSE;
        }

        $id_po = 0;
        if($no_po !="")
        {
            $data_po = $this->po_model->find_by(['no_po' => $no_po, 'deleted' => 0]);
            if(!$data_po)
            {
                $this->template->set_message(lang('pembelian_po_not_found'), 'error');
                return FALSE;
            }
            else
            {
                $id_po = $data_po->id_po;
                if($data_po->st_close == 1)
                {
                    $this->template->set_message(lang('pembelian_st_po_closed'), 'error');
                    return FALSE;
                }

                $id_supplier = $data_po->id_supplier;
            } 
        }

        $ero_msg = array();
        foreach ($qty as $key => $qt) {
            if(intval($qty) <= 0)
            {
                $ero_msg[] = sprintf(lang('pembelian_qty_nol'), $key+1);
            }

            if(floatval(str_replace(",", "", $harga[$key])) <= 0 && $st_bonus[$key] == 0)
            {
                $ero_msg[] = sprintf(lang('pembelian_harga_nol'), $key+1);
            }
        }

        if($ero_msg)
        {
            $this->save_session();

            $ero_msg = implode("<br>", $ero_msg);
            $this->template->set_message($ero_msg, 'error');
            return FALSE;
        }

        //Simpan Pembelian
        $this->db->trans_start();

        //Hitung g total
        $total      = 0;        
        $sub_total  = 0;
        $gtotal = 0;
        foreach ($id_barang as $key => $kode) {

            $item_diskon[$key]   = floatval($item_diskon[$key]);
            $item_potongan[$key] = floatval(str_replace(",", "", $item_potongan[$key]));

            $xtotal = floatval($qty[$key]) * floatval(str_replace(",", "", $harga[$key]));

            $sub_total = $xtotal - (($item_diskon[$key]/100) * $xtotal) - $item_potongan[$key];

            $total += $sub_total;
        }

        //Hitung gtotal
        $total_no_pajak = $total - ((floatval($diskon)/100)*$total) - floatval($potongan);
        $nilai_pajak    = round((floatval($pajak)/100) * $total_no_pajak);
        $gtotal         = $total_no_pajak + $nilai_pajak;

        if($type == 1)
        {
            $st_lunas = 1;
        }
        else
        {
            $st_lunas = 0;
        }

        $dt_supplier = $this->supplier_model->find($id_supplier);

        $id_pembelian   = gen_primary($this->prefixKey);
        $data_pembelian = array('id_pembelian' => $id_pembelian,
                                'no_nota'   => $no_faktur,
                                'id_kedatangan' => $id_kedatangan,
                                'id_po'     => $id_po,
                                'type'      => $type,
                                'tgl_tran'  => ($tgl_tran) ? date_ymd($tgl_tran) : NULL,
                                'tgl_tempo' => ($tgl_tempo) ? date_ymd($tgl_tempo) : NULL,
                                'tempo_hari' => ($tempo_hari) ? $tempo_hari : 0,
                                'total'     => $total,
                                'diskon'    => $diskon,
                                'pajak'     => $pajak,
                                'nilai_pajak' => $nilai_pajak,
                                'potongan'  => $potongan,
                                'gtotal'    => $gtotal,
                                'id_supplier' => $id_supplier,
                                'st_lunas' => $st_lunas,
                                'st_pajak' => $st_pajak,
                                'no_faktur_pajak' => $no_faktur_pajak,
                                'npwp_supplier'   => $dt_supplier->npwp_badan
                            );

        $pembelian = $this->pembelian_model->insert($data_pembelian);

        if ($pembelian !== FALSE)
        {
            $keterangan = "SUKSES, tambah data Pembelian, dengan ID : ".$no_faktur;
            $status     = 1;

            //Simpan Alur Kas jika tunai
            if($type == 1) //Kode 3 untuk pembelian
            {
                simpan_alurkas(3, "Pembayaran pembelian tunai untuk Faktur : ".$no_faktur, $gtotal , 0, $this->addPermission);    
            }
        }
        else
        {
            $keterangan = "GAGAL, tambah data Pembelian, dengan ID : ".$no_faktur;
            $status     = 0;
        }

        //Save Log
        $nm_hak_akses   = $this->addPermission; 
        $kode_universal = $no_faktur;
        $jumlah         = $gtotal;
        $sql            = $this->db->last_query();

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        $data_det_beli = array();
        $stok_data = array();
        $data_to_cek = array();
        foreach ($id_barang as $key => $kode) {

            $item_diskon[$key]   = floatval($item_diskon[$key]);
            $item_potongan[$key] = floatval(str_replace(",", "", $item_potongan[$key]));

            $sub_total = intval($qty[$key]) * intval(str_replace(",", "", $harga[$key]));
            $total     = $sub_total - (($item_diskon[$key]/100) * $sub_total) - $item_potongan[$key];

            $data_det_beli[] = array('id_detail_pembelian' => gen_primary(),
                                    'id_pembelian' => $id_pembelian,
                                    'id_barang' => $kode,
                                    'qty'       => $qty[$key],
                                    'id_konversi' => $konversi[$key],
                                    'harga'     => floatval(str_replace(",", "", $harga[$key])),
                                    'sub_total' => $sub_total,
                                    'diskon'    => $item_diskon[$key],
                                    'potongan'  => $item_potongan[$key],
                                    'total'     => $total,
                                    'st_bonus'  => $st_bonus[$key]
                            );

            //Update HPP
            $dt_real_qty = hitung_ke_satuan_kecil($konversi[$key], floatval(remove_comma($qty[$key])));
            $real_qty  = $dt_real_qty['qty'];

            $harga_no_pajak = floatval(remove_comma($harga[$key]));
            if($is_include_pajak)
            {
                $hpp = $harga_no_pajak;
            }
            else
            {
                $hpp = $harga_no_pajak + ($harga_no_pajak * $pajak / 100);    
            }
            

            if($real_qty != floatval(remove_comma($qty[$key])))
            {
                $hpp = $total / $real_qty;
            }

            $this->stok_model->update_hpp($kode, $id_kedatangan, $hpp, $st_bonus[$key]);
            
            $data_to_cek[] = [
                            'id_barang' => $kode,
                            'qty'       => $real_qty
                            ];
        }

        $det_beli = $this->pembelian_detail_model->insert_batch($data_det_beli);
        $query_det = $this->db->last_query();
        if ($det_beli !== FALSE)
        {
            $keterangan = "SUKSES, tambah data detail Pembelian, untuk ID Pembelian : ".$id_pembelian;
            $status     = 1;
        }
        else
        {
            $keterangan = "GAGAL, tambah data detail Pembelian, untuk ID Pembelian : ".$id_pembelian;
            $status     = 0;
        }
        //Save Log
        $nm_hak_akses   = $this->addPermission; 
        $kode_universal = $id_pembelian;
        $jumlah         = $gtotal;
        $sql            = $query_det;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
        //Update Status PO
        if($no_po != '')
        {
            $update_po = [];
            $dt_det_po = $this->po_detail_model->find_all_by(['id_po' => $id_po]);
            if($dt_det_po)
            {
                $cek_terpenuhi = 0;
                foreach ($dt_det_po as $key => $dp) {
                    $qty_po = $dp->real_qty;

                    foreach ($data_to_cek as $key => $val) {
                        if($val['id_barang'] == $dp->id_barang)
                        {
                            $update_po[] = ['id_detail_po'  => $dp->id_detail_po,
                                            'qty_terpenuhi' => $val['qty']+$dp->qty_terpenuhi
                                        ];
                        }
                    }
                }
            }
            //Update Detail PO
            if($update_po)
            {
                $this->po_detail_model->update_batch($update_po,'id_detail_po');
            }
            //Update Status Close PO
            $cek = $this->po_detail_model
                                    ->where("real_qty <> qty_terpenuhi")
                                    ->where(['id_po' => $id_po])
                                    ->count_all();
            if($cek == 0)
            {
                $this->po_model->update($id_po, ['st_close' => 1]);
            }
        }
        

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE)
        {
            $this->template->set_message($this->pembelian_model->error."\n\n".$this->pembelian_detail_model->error, 'error');
            return FALSE;
        }
        else
        {
            $this->session->unset_userdata('dt_pembelian');
            return TRUE;
        }
    }

    protected function save_session()
    {
        $no_faktur  = $this->input->post('no_faktur');
        $id_kedatangan = $this->input->post('id_kedatangan');
        $no_po      = $this->input->post('no_po');
        $type       = $this->input->post('type');
        $tgl_tran   = $this->input->post('tgl_tran');
        $tempo_hari = $this->input->post('tempo_hari');
        $tgl_tempo  = $this->input->post('tempo_tanggal');
        $id_supplier  = $this->input->post('id_supplier');
        // $diskon     = $this->input->post('diskon');
        // $potongan   = $this->input->post('potongan');
        $diskon     = 0;
        $potongan   = 0;
        $pajak      = $this->input->post('pajak');
        $st_pajak   = $this->input->post('st_pajak');
        $no_faktur_pajak    = $this->input->post('no_faktur_pajak');
        $is_include_pajak   = $this->input->post('is_include_pajak');

        $id_barang  = $this->input->post('id_barang');
        $konversi   = $this->input->post('konversi');
        $qty        = $this->input->post('qty');
        $harga      = $this->input->post('harga');
        $item_diskon   = $this->input->post('item_diskon');
        $item_potongan = $this->input->post('item_potongan');
        $st_bonus   = $this->input->post('st_bonus');

        //Masukkan item kedalam session
        $data = $this->session->userdata('dt_pembelian');
        $items = $data['items'];

        $data_items = array();
        $gtotal     = 0;        
        $total      = 0;        
        $sub_total  = 0;        
        foreach ($id_barang as $key => $kode) {
            $item_diskon[$key]   = floatval($item_diskon[$key]);
            $item_potongan[$key] = floatval($item_potongan[$key]);

            $xtotal = intval($qty[$key]) * intval(str_replace(",", "", $harga[$key]));

            $sub_total = $xtotal - ((floatval($item_diskon[$key])/100) * $xtotal) - floatval($item_potongan[$key]);

            $dt_barang = $this->barang_model->find($kode);

            $data_items[] = array('id_barang'   => $kode,
                                'barcode'       => $dt_barang->barcode,
                                'nm_barang'     => $dt_barang->nm_barang,
                                'id_konversi'   => $konversi[$key],
                                'qty'           => $qty[$key],
                                'harga'         => str_replace(",", "", $harga[$key]),
                                'item_diskon'   => $item_diskon[$key],
                                'item_potongan' => str_replace(",", "", $item_potongan[$key]),
                                'sub_total'     => $sub_total,
                                'konversi'      => get_konversi($dt_barang->id_satuan_terkecil),
                                'st_bonus'      => $st_bonus[$key]
                            );

            $total += $sub_total;
        }

        //Hitung gtotal
        $total_no_pajak = $total - ((floatval($diskon)/100)*$total) - floatval($potongan);
        $gtotal         = $total_no_pajak + ((floatval($pajak)/100)*$total_no_pajak);

        $data['items']      = $data_items;
        $data['no_faktur']  = $no_faktur;
        $data['id_kedatangan'] = $id_kedatangan;
        $data['no_po']     = $no_po;
        $data['tgl_tran']  = $tgl_tran;
        $data['type']      = $type;
        $data['tempo_hari']= $tempo_hari;
        $data['tempo_tanggal']= $tgl_tempo;
        $data['diskon']    = $diskon;
        $data['potongan']  = $potongan;
        $data['pajak']     = $pajak;
        $data['gtotal']    = $gtotal;
        $data['id_supplier'] = $id_supplier;
        $data['st_pajak']  = $st_pajak;
        $data['no_faktur_pajak']   = $no_faktur_pajak;
        $data['is_include_pajak']  = $is_include_pajak;

        $this->session->set_userdata('dt_pembelian', $data);

        $this->template->set_message(validation_errors(), 'error');
        return FALSE;
    }

    public function default_select($val)
    {
        return $val == "" ? FALSE : TRUE;
    }

    public function laporan()
    {
        $this->auth->restrict($this->laporanPermission);

        $this->form_validation->set_rules('tgl_awal','lang:pembelian_tgl_awal','trim');
        $this->form_validation->set_rules('tgl_akhir','lang:pembelian_tgl_akhir','trim');

        if($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
        }

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';

            $tgl_awal       = $this->input->post('tgl_awal');
            $tgl_akhir      = $this->input->post('tgl_akhir');
            $tipe_beli      = $this->input->post('tipe_beli');
            $st_lunas       = $this->input->post('st_lunas');
            $jatuh_tempo    = $this->input->post('jatuh_tempo');
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';

            $tgl_awal       = $this->input->get('tgl_awal');
            $tgl_akhir      = $this->input->get('tgl_akhir');
            $tipe_beli      = $this->input->get('tipe_beli');
            $st_lunas       = $this->input->get('st_lunas');
            $jatuh_tempo    = $this->input->get('jatuh_tempo');
        }

        if(is_null($tipe_beli))
        {
            $tipe_beli = 0;
            $jatuh_tempo = 7;
            $st_lunas = 0;
        }

        $search2 = $this->db->escape_str($search);
       
        $filter = "?search=".urlencode($search)."&tipe_beli=".urlencode($tipe_beli)."&st_lunas=".urlencode($st_lunas)."&jatuh_tempo=".urlencode($jatuh_tempo)."&tgl_awal=".urlencode($tgl_awal)."&tgl_akhir=".urlencode($tgl_akhir);

        $where = "`pembelian.deleted` = 0 ";

        if($tgl_awal !="" && $tgl_akhir !="")
        {
            $tgl1 = date_ymd($tgl_awal);
            $tgl2 = date_ymd($tgl_akhir);
            
            $where .= " AND (tgl_tran >='$tgl1' AND tgl_tran <= '$tgl2')";
        }

        if($tipe_beli != 3)
        {
            $where .= "AND `pembelian`.`type`=$tipe_beli ";
        }

        if($tipe_beli == 0 && $jatuh_tempo)
        {
            $where .= "AND DATEDIFF(tgl_tempo, date(now())) <= $jatuh_tempo ";
        }

        if($st_lunas != "" && $tipe_beli == 0)
        {
            $where .= "AND `pembelian`.`st_lunas`=$st_lunas ";
        }

        if($search!="")
        {
            $where .= "AND `nm_lengkap` like '%$search2%' ";
        }

        $data   = $this->pembelian_model->select(array('pembelian.*, nm_lengkap','nm_supplier','no_po'))
                                        ->join('supplier','pembelian.id_supplier=supplier.idsupplier','left')
                                        ->join('users','pembelian.created_by=users.id_user')
                                        ->join('po_supplier','pembelian.id_po=po_supplier.id_po','left')
                                        ->where($where)
                                        ->order_by('created_on','ASC')
                                        ->find_all();
        if($data)
        {
            foreach ($data as $key => $dt) {
                $data[$key]->det_beli = $this->pembelian_detail_model->select("pembelian_detail.*, konversi_satuan.satuan_besar as nm_satuan, barcode, nm_barang")
                                            ->join('barang','pembelian_detail.id_barang = barang.idbarang_bb')
                                            ->join('konversi_satuan','pembelian_detail.id_konversi = konversi_satuan.id_konversi')
                                            ->order_by("nm_barang", "ASC")
                                            ->find_all_by('id_pembelian', $dt->id_pembelian);
            }
        }

        //Identitas
        $identitas = $this->identitas_model->find(1);

        $assets = ['plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                    'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                    'plugins/select2/css/select2.min.css',
                    'plugins/select2/js/select2.min.js',
                    'pembelian/assets/js/laporan_beli.js'
                    ];

        add_assets($assets);

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('idt', $identitas);
        $this->template->set('period1', $tgl_awal);
        $this->template->set('period2', $tgl_akhir);
        $this->template->set('jatuh_tempo', $jatuh_tempo);
        $this->template->title(lang('pembelian_laporan_title_view'));
        $this->template->render('laporan');
    }

    public function laporan_hutang(){
        $this->auth->restrict($this->laporanPermission);

        $this->form_validation->set_rules('tgl_awal','lang:pembelian_tgl_awal','trim');
        $this->form_validation->set_rules('tgl_akhir','lang:pembelian_tgl_akhir','trim');

        if($this->form_validation->run() === FALSE)
        {
            $this->template->set_message(validation_errors(), 'error');
        }

        if(isset($_POST['table_search']))
        {
            $search = isset($_POST['table_search'])?$this->input->post('table_search'):'';

            $tgl_awal       = $this->input->post('tgl_awal');
            $tgl_akhir      = $this->input->post('tgl_akhir');
        }
        else
        {
            $search = isset($_GET['search'])?$this->input->get('search'):'';

            $tgl_awal       = $this->input->get('tgl_awal');
            $tgl_akhir      = $this->input->get('tgl_akhir');
        }

        $search2 = $this->db->escape_str($search);
       
        $filter = "?search=".urlencode($search)."&tgl_awal=".urlencode($tgl_awal)."&tgl_akhir=".urlencode($tgl_akhir);

        $where = "`pembelian.deleted`=0 AND type=0";

        if($tgl_awal !="" && $tgl_akhir !="")
        {
            $tgl1 = date_ymd($tgl_awal);
            $tgl2 = date_ymd($tgl_akhir);
            
            $where .= " AND (tgl_tempo >='$tgl1' AND tgl_tempo <= '$tgl2')";
        }

        if($search!="")
        {
            $where .= "AND `nm_supplier` like '%$search2%' ";
        }

        $data   = $this->pembelian_model->select(array('pembelian.*, nm_lengkap','nm_supplier','no_po'))
                                        ->join('supplier','pembelian.id_supplier=supplier.idsupplier','left')
                                        ->join('users','pembelian.created_by=users.id_user')
                                        ->join('po_supplier','pembelian.id_po=po_supplier.id_po','left')
                                        ->where($where)
                                        ->order_by('created_on','ASC')
                                        ->find_all();
        //Identitas
        $identitas = $this->identitas_model->find(1);

        $assets = ['plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
                    'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
                    'plugins/select2/css/select2.min.css',
                    'plugins/select2/js/select2.min.js',
                    'pembelian/assets/js/laporan_hutang.js'
                    ];

        add_assets($assets);

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('idt', $identitas);
        $this->template->set('period1', $tgl_awal);
        $this->template->set('period2', $tgl_akhir);
        $this->template->title(lang('pembelian_laporan_hutang_title_view'));
        $this->template->render('laporan_hutang');
    }
}
?>