<?php 
  $ENABLE_ADD    = has_permission('Revisi List Order.Add'); 
  $ENABLE_MANAGE  = has_permission('Revisi List Order.Manage'); 
  $ENABLE_DELETE  = has_permission('Revisi List Order.Delete'); 
?> 
<div class="box box-primary"> 
  <?= form_open($this->uri->uri_string(),array('id'=>'frm_revisi_list_order','name'=>'frm_revisi_list_order','class'=>'form-inline')) ?> 
  <div class="box-header">
    <?php if ($ENABLE_ADD): ?>
        <a href="<?= site_url('revisi_list_order/create') ?>" class="btn btn-success" title="<?= lang('revisi_list_order_btn_new') ?>"><?= lang('revisi_list_order_btn_new') ?></a>
        <?php endif;?> 
      <div class="pull-right"> 
          <div class="form-group"> 
              <div class="input-daterange input-group"> 
                  <input type="text" class="form-control" name="tgl_awal" id="tgl_awal" value="<?= isset($tgl_awal) && $tgl_awal !='' ? $tgl_awal : '' ?>" placeholder="<?= lang('revisi_list_order_tgl_awal') ?>" required /> 
                  <span class="input-group-addon text-black">to</span> 
                  <input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" value="<?= isset($tgl_akhir) && $tgl_akhir !='' ? $tgl_akhir : '' ?>" placeholder="<?= lang('revisi_list_order_tgl_akhir') ?>" required /> 
              </div> 
          </div> 
          <div class="form-group"> 
              <div class="input-group"> 
                  <input type="text" name="table_search" value="<?php echo isset($search) ? $search : ''; ?>" class="form-control pull-right" placeholder="Search" autofocus /> 
                  <div class="input-group-btn"> 
                      <button class="btn btn-default"><i class="fa fa-search"></i></button> 
                  </div> 
              </div> 
          </div> 
      </div> 
  </div> 
  <?php if (isset($results) && is_array($results) && count($results)>0) : ?> 
    <div class="box-body table-responsive"> 
        <table class="table table-hover table-striped"> 
            <thead> 
                <tr> 
                    <th width="40" class="text-center" style="vertical-align: middle;">No</th> 
                    <th class="text-center" style="vertical-align: middle;"><?= lang('revisi_list_order_no_transaksi') ?></th>
                    <th style="vertical-align: middle;"><?= lang('revisi_list_order_no_nota') ?></th>
                    <th style="vertical-align: middle;"><?= lang('revisi_list_order_id_konsumen') ?></th> 
                    <th style="vertical-align: middle;"><?= lang('revisi_list_order_id_produk') ?></th> 
                    <th width="50px" style="vertical-align: middle;"><?= lang('revisi_list_order_jml_order')?></th>
                    <th style="vertical-align: middle;"><?= lang('revisi_list_order_total_order') ?></th> 
                    <th style="vertical-align: middle;"><?= lang('revisi_list_order_tgl_revisi') ?></th> 
                    <th style="vertical-align: middle;"><?= lang('revisi_list_order_ket_revisi') ?></th>

                    <?php if($ENABLE_MANAGE) : ?> 
                        <th width="80" style="vertical-align: middle;">ACT</th> 
                    <?php endif; ?> 
                </tr> 
            </thead> 
            <tbody> 
              <?php foreach ($results as $record) : 
                    if ($record->st_revisi == 0) {
                        $style="success";
                    }else{
                        $style="danger";
                    }
                ?> 

                <tr class="<?= $style; ?>"> 
                    <td class="text-right"><?= $numb; ?></td> 
                    <td style="vertical-align: middle;"><?= $record->no_transaksi ?></td> 
                    <td style="vertical-align: middle;"><?= $record->no_faktur==null ? '-':$record->no_faktur; ?></td>
                    <td style="vertical-align: middle;"><?= $record->nama ?></td> 
                    <td style="vertical-align: middle;"><?= $record->nm_produk ?></td> 
                    <td style="vertical-align: middle;"><?= $record->jml_cetak?></td>
                    <td style="vertical-align: middle;"><?= number_format($record->sub_total) ?></td>
                    <td style="vertical-align: middle;"><?= date("d-m-Y", strtotime($record->created_on))?></td>
                    <td style="vertical-align: middle;"><?= $record->ket ?></td>
                    <?php if($ENABLE_MANAGE) : ?> 
                        <td style="padding-right:20px; vertical-align: middle;">
                            <?php 
                                if ($record->st_revisi==0):?>
                                    <a class="text-green" href="<?= base_url("order_produk/view_detail/".$record->new_id_detail_produk_order) ?>" target="_blank" title="View New Data"><i class="fa fa-eye"></i></a> | 
                                <?php endif;?>
                                <a class="text-red" href="<?= base_url("order_produk/view_detail/".$record->old_id_detail_produk_order) ?>" target="_blank" title="View Old Data"><i class="fa fa-eye"></i></a>
                        </td> 
                    <?php endif; ?> 
              </tr> 
              <?php $numb++; endforeach; ?> 
          </tbody> 
      </table> 
    </div><!-- /.box-body --> 
    <div class="box-footer clearfix"> 
      <?php 
      echo $this->pagination->create_links();  
      ?> 
    </div> 
  <?php else: ?> 
    <div class="alert alert-info" role="alert"> 
        <p><i class="fa fa-warning"></i> &nbsp; <?= lang('revisi_list_order_no_records_found') ?></p> 
    </div> 
    <?php 
  endif; 
  echo form_close();  
  ?>   
</div><!-- /.box-->

<!-- modal view -->
<div id="lightbox-view" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="border-radius: 5px !important;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Detail Data List Order</h4>
            </div>
            <div class="modal-body form-horizontal">
              <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <input type="hidden" name="id_detail_produk_order" id="id_detail_produk_order">
                        <div class="form-group <?= form_error('nama_pekerjaan') ? ' has-error' : ''; ?>">
                            <label for="nama_pekerjaan" class="col-sm-5 control-label"><?= lang('revisi_list_order_nama_pekerjaan') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_nama_pekerjaan"></span>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('id_kategori') ? ' has-error' : ''; ?>">
                            <label for="id_kategori" class="col-sm-5 control-label"><?= lang('revisi_list_order_id_kategori') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_id_kategori_produk"></span>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('id_produk') ? ' has-error' : ''; ?>">
                            <label for="id_produk" class="col-sm-5 control-label"><?= lang('revisi_list_order_id_produk') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_id_produk"></span>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('gambar') ? ' has-error' : ''; ?>">
                            <label for="gambar" class="col-sm-5 control-label"><?= lang('revisi_list_order_gambar_cetak') ?></label>
                            <div class="col-sm-7">
                                <img width="100" height="100" src="" id="view_img" name="view_img">
                            </div>
                        </div>
                        <div class="form-group <?= form_error('ukuran_cetak') ? ' has-error' : ''; ?>">
                            <label for="ukuran_cetak" class="col-sm-5 control-label"><?= lang('revisi_list_order_ukuran_cetak') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_p"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group <?= form_error('jml_order') ? ' has-error' : ''; ?>">
                            <label for="jml_order" class="col-sm-5 control-label"><?= lang('revisi_list_order_jml_order') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_jml_order"></span>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('biaya_cetak') ? ' has-error' : ''; ?>">
                            <label for="biaya_cetak" class="col-sm-5 control-label"><?= lang('revisi_list_order_biaya_cetak') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_biaya_cetak"></span>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('biaya_desain') ? ' has-error' : ''; ?>">
                            <label for="biaya_desain" class="col-sm-5 control-label"><?= lang('revisi_list_order_biaya_desain') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_biaya_desain"></span>
                            </div>
                        </div>
                        <div class="form-group <?= form_error('note_produk') ? ' has-error' : ''; ?>">
                            <label for="note_produk" class="col-sm-5 control-label"><?= lang('revisi_list_order_note_produk') ?></label>
                            <div class="col-sm-7">
                                <textarea id="view_note_produk" disabled class="form-control">
                                    
                                </textarea>
                                <!-- <span class="form-control" id="view_note_produk"></span> -->
                                <!-- <textarea class="form-control" id="view_note_produk" name="view_note_produk" disabled></textarea> -->
                            </div>
                        </div>
                        <div class="form-group <?= form_error('st_finishing') ? ' has-error' : ''; ?>">
                            <label for="st_finishing" class="col-sm-5 control-label"><?= lang('revisi_list_order_st_finishing') ?></label>
                            <div class="col-sm-7">
                                <span class="form-control" id="view_st_finishing"></span>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-12" id="detail-hide">
                    <!-- view detail finishing -->
                    <p style="margin-top: 15px; margin-bottom: 3px;">
                        <span class="label label-warning" style="font-size: 12px"><?= lang('revisi_list_order_list_finishing') ?></span>
                    </p>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="view_tdet_finishing" name="view_tdet_finishing">
                            <thead>
                                <tr class="success">
                                    <th style="vertical-align:middle;"><?= lang('revisi_list_order_no') ?></th>
                                    <th style="vertical-align:middle;"><?= lang('revisi_list_order_id_finishing') ?></th>
                                    <th style="vertical-align: middle;"><?= lang('revisi_list_order_note_finishing')?></th>
                                    <th width="150px" style="vertical-align:middle;" class="text-center"><?= lang('revisi_list_order_jml_pakai_finishing') ?></th>
                                    <th width="150px" style="vertical-align:middle;"><?= lang('revisi_list_order_harga_finishing') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                
                            </tbody>
                            <tfoot>
                                <!-- <tr>
                                    <td colspan="4">
                                        Total
                                    </td>
                                    <td class="view_gtotal_finishing" align="right">
                                        
                                    </td>
                                </tr> -->
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="col-md-12">
                  <div class="col-sm-12" style="-webkit-box-shadow: 1px 2px 4px 2px rgba(180,155,250,1);
-moz-box-shadow: 1px 2px 4px 2px rgba(180,155,250,1);
box-shadow: 1px 2px 4px 2px rgba(180,155,250,1); border-radius: 5px; padding-top: 15px !important">
                    <div id="jml_revisi_div" style="padding-left: 5px;padding-right: 5px;" class="form-group <?= form_error('revisi_jml') ? ' has-error' : ''; ?>" >
                      <label for="revisi_jml" class="control-label col-sm-2"><?= lang('revisi_list_order_jml_revisi') ?></label>
                      <div class="col-sm-5">
                        <input type="text" name="jml_revisi" id="jml_revisi" class="form-control" placeholder="" autofocus>
                      </div>
                    </div>
                    <div id="ket_revisi_div" style="padding-left: 5px;padding-right: 5px;" class="form-group <?= form_error('ket_revisi') ? ' has-error' : ''; ?>">
                      <label for="ket_revisi" class="control-label col-sm-2"><?= lang('revisi_list_order_ket_revisi') ?></label>
                      <div class="col-sm-10">
                        <textarea class="form-control" id="ket_revisi" name="ket_revisi"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info" id="btn-update" name="btn-update" onclick="update_order()">Update Jml Order</button>
                <button class="btn btn-danger" id="btn-delete" name="btn-update" onclick="delete_order()">Delete List Order</button>
            </div>  
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->