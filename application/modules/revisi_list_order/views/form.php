</style>

<div class="box box-primary">
    <!-- form start -->
    <?= form_open($this->uri->uri_string(),array('id'=>'frm_detail','name'=>'frm_detail','role'=>'form','class'=>'form-horizontal')) ?>
    	<div class="box-body">
			<div class="table-responsive">
            <table class="table table-bordered" id="tdet_penyusun" name="tdet_penyusun">
                <thead>
                    <tr class="success">
                        <th width="50">NO</th> 
                        <th style="vertical-align:middle; text-align: center;"><?= lang('spk_nm_barang') ?></th>
                        <th style="vertical-align:middle; text-align: center;"><?= lang('spk_qty') ?></th>
                        <th width="200px" style="vertical-align:middle;text-align: center;"><?= lang('spk_konversi_satuan') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($bahan_baku) : ?>
                            <?php 
                                $no = 1;
                                foreach($bahan_baku as $key => $record) :
                            ?>
                        <tr class="info"> 
                            <td><?= $no++ ?></td>
                            <td> <?= $record['nm_barang'] ?></td>
                            <td> 
                            	<input type="hidden" name="stok[]" value="<?= $record['qty'] ?>">
                            	<label class="lbl-stok"><?= number_format($record['qty']) ?></label> 
                            </td>
                            <td>
                            	<select name="konversi[]" class="form-control">
				                    <?php foreach ($record['konversi'] as $key => $record) : ?>
				                    <option value="<?= $record->id_konversi; ?>" <?= ($record->selected == 1) ? "selected='selected'" : "" ?> data-jml-kecil ="<?= $record->jml_kecil;?>" data-jml-besar="<?= $record->jml_besar;?>" data-tipe ="<?= $record->st_tipe;?>" <?= set_select('konversi', $record->id_konversi, isset($data->konversi) && $data->konversi == $record->id_konversi) ?>><?= $record->satuan_besar.' ('.$record->tampil.')'?></option> 
				                    <?php endforeach; ?>
				                </select>
                            </td>
                        </tr>
                    <?php 
                        endforeach;
                    endif;
                    ?>
                    <?php if(isset($bahan_baku_finishing)) : 
                        foreach($bahan_baku_finishing as $key => $record) :
                    ?>
                        <tr class="warning">
                            <td><?= $no++ ?></td>
                            <td> <?= $record['nm_barang'] ?></td>
                            <td> 
                                <input type="hidden" name="stok[]" value="<?= $record['qty'] ?>">
                                <label class="lbl-stok"><?= number_format($record['qty']) ?></label> 
                            </td>
                            <td>
                                <select name="konversi[]" class="form-control">
                                    <?php foreach ($record['konversi'] as $key => $record) : ?>
                                    <option value="<?= $record->id_konversi; ?>" <?= ($record->selected == 1) ? "selected='selected'" : "" ?> data-jml-kecil ="<?= $record->jml_kecil;?>" data-jml-besar="<?= $record->jml_besar;?>" data-tipe ="<?= $record->st_tipe;?>" <?= set_select('konversi', $record->id_konversi, isset($data->konversi) && $data->konversi == $record->id_konversi) ?>><?= $record->satuan_besar.' ('.$record->tampil.')'?></option> 
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>
                    <?php 
                        endforeach;
                    endif;
                    ?>
                    <?php if(isset($bahan_baku_finishing_tambahan)) :
                        foreach($bahan_baku_finishing_tambahan as $key => $record) :
                    ?>
                        <tr class="danger">
                            <td><?= $no++ ?></td>
                            <td> <?= $record['nm_barang'] ?></td>
                            <td> 
                                <input type="hidden" name="stok[]" value="<?= $record['qty'] ?>">
                                <label class="lbl-stok"><?= number_format($record['qty']) ?></label> 
                            </td>
                            <td>
                                <select name="konversi[]" class="form-control">
                                    <?php foreach ($record['konversi'] as $key => $record) : ?>
                                    <option value="<?= $record->id_konversi; ?>" <?= ($record->selected == 1) ? "selected='selected'" : "" ?> data-jml-kecil ="<?= $record->jml_kecil;?>" data-jml-besar="<?= $record->jml_besar;?>" data-tipe ="<?= $record->st_tipe;?>" <?= set_select('konversi', $record->id_konversi, isset($data->konversi) && $data->konversi == $record->id_konversi) ?>><?= $record->satuan_besar.' ('.$record->tampil.')'?></option> 
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>
                    <?php 
                        endforeach;
                    endif;
                    ?>
                </tbody>
            </table>
        </div>

		  	<div class="form-group">
			    <div class="col-sm-10">
			      	<button type="button" name="save" id="save" class="btn btn-success" onclick="goBack()"><?= lang('spk_btn_cancel') ?></button>
			    </div>
		  	</div>
	  </div>
	<?= form_close() ?>
</div><!-- /.box -->

<script>
function goBack() {
    window.history.back();
}
</script> 