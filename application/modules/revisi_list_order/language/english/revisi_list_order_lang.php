<?php defined('BASEPATH')|| exit('No direct script access allowed');

// ============================
$lang['revisi_list_order_title_manage']			= 'Data Order';
$lang['revisi_list_order_title_new']				= 'Data Order Baru';
$lang['revisi_list_order_title_edit']			= 'Edit Data Order';

// from/table
$lang['revisi_list_order_no_transaksi']			= 'No. Transaksi';
$lang['revisi_list_order_no_po']					= 'No. PO';
$lang['revisi_list_order_no_nota']				= 'Nomer Nota';
$lang['revisi_list_order_lampiran']				= 'Lampiran';
$lang['revisi_list_order_id_konsumen']			= 'Nama Konsumen';
$lang['revisi_list_order_tgl_order']			= 'Tanggal Order';
$lang['revisi_list_order_tgl_revisi']			= 'Tanggal Revisi';
$lang['revisi_list_order_tgl_selesai']			= 'Waktu Permintaan Selesai';
$lang['revisi_list_order_jam_selesai']			= 'Jam Permintaan Selesai';
$lang['revisi_list_order_nama_pekerjaan']		= 'Nama File';
$lang['revisi_list_order_id_kategori']			= 'Kategori Produk';
$lang['revisi_list_order_id_produk']				= 'Produk';
$lang['revisi_list_order_gambar_cetak']			= 'Gambar Cetak';
$lang['revisi_list_order_ukuran_cetak']			= 'Ukuran Cetak';
$lang['revisi_list_order_jml_order']				= 'Jumlah Order';
$lang['revisi_list_order_biaya_cetak']			= 'Biaya Cetak';
$lang['revisi_list_order_biaya_desain']			= 'Biaya Desain';
$lang['revisi_list_order_biaya_finishing']		= 'Biaya Finishing';
$lang['revisi_list_order_note_produk']			= 'Note Produk';
$lang['revisi_list_order_st_finishing']			= 'Status Finishing';
$lang['revisi_list_order_st_biaya_cetak']		= 'Status Biaya Cetak';
$lang['revisi_list_order_st_urgent']				= 'Status Urgensi';
$lang['revisi_list_order_st_order']				= 'Status Order';
$lang['revisi_list_order_st_urgent_non_urgent']	= 'Non Urgent';
$lang['revisi_list_order_st_urgent_urgent']		= 'Urgent';
$lang['revisi_list_order_id_finishing']			= 'Finishing';
$lang['revisi_list_order_hanya_cetak']			= 'Hanya Biaya Cetak';
$lang['revisi_list_order_cetak_dan_bahan']		= 'Biaya Cetak Dan Bahan';
$lang['revisi_list_order_jml_pakai_finishing']	= 'Jumlah Pakai';
$lang['revisi_list_order_note_finishing']		= 'Note Finishing';
$lang['revisi_list_order_harga_finishing']		= 'Harga Finishing';
$lang['revisi_list_order_total']					= 'Sub Total Biaya';
$lang['revisi_list_order_total_order']			= 'Total Order';
$lang['revisi_list_order_no_tlp']				= 'No. Telepon';
$lang['revisi_list_order_detail_gambar']			= 'Info Gambar';
$lang['revisi_list_order_harga_persatuan']		= 'Harga Persatuan';
$lang['revisi_list_order_st_konsumen']			= 'ST Konsumen';

$lang['revisi_list_order_tgl_awal']				= 'Tanggal Awal';
$lang['revisi_list_order_tgl_akhir']				= 'Tanggal Akhri';

$lang['revisi_list_order_jml_revisi']			= 'Jml Cetak Baru';
$lang['revisi_list_order_ket_revisi']			= 'Keterangan';

// label
$lang['revisi_list_order_finishing_standart']	= 'Finishing Standart';
$lang['revisi_list_order_finishing_standart_digunakan'] = 'Finishing standart masih digunakan';
$lang['revisi_list_order_finishing_custom']		= 'Finishing Custom';
$lang['revisi_list_order_dimensi_susai_gambar']	= 'Custom Ukuran Cetak';
$lang['revisi_list_order_list_finishing']		= '[ List Barang Pakai Finishing ]';
$lang['revisi_list_order_list_order']			= '[ List Order Produk ]';

$lang['revisi_list_order_no'] 		= 'NO';
$lang['revisi_list_order_deskripsi']	= 'Deskripsi';
$lang['revisi_list_order_harga']		= 'Harga';
$lang['revisi_list_order_subtotal']	= 'Sub Total';

$lang['revisi_list_order_label_page']	= 'Halaman';
$lang['revisi_list_order_label_jumlah']	= 'Jml Cetak';




//button
$lang['revisi_list_order_btn_new']				= 'Baru';
$lang['revisi_list_order_btn_delete']			= 'Hapus';
$lang['revisi_list_order_btn_save']				= 'Simpan Transaksi';
$lang['revisi_list_order_btn_cancel']			= 'Home';
$lang['revisi_list_order_btn_pakai_finishing']	= 'Tamabh Finishing';
$lang['revisi_list_order_btn_tambah_order']		= 'Tambah Daftar Order';
$lang['revisi_list_order_or']					= 'Atau';
$lang['revisi_list_order_btn_back']				= 'Kembali';

//messages
$lang['revisi_list_order_canceled']				= 'Transaksi Berhasil Dibatalkan';
$lang['revisi_list_order_del_error']				= 'Anda belum memilih data revisi_list_order yang akan dihapus.';
$lang['revisi_list_order_del_failure']			= 'Tidak dapat menghapus data revisi_list_order';
$lang['revisi_list_order_delete_confirm']		= 'Apakah anda yakin akan menghapus data revisi_list_order terpilih?';
$lang['revisi_list_order_cancel_confirm']		= 'Apakah anda yakin akan membatalkan Transaksi?';
$lang['revisi_list_order_deleted']				= 'revisi_list_order berhasil dihaspus';
$lang['revisi_list_order_no_records_found']		= 'Data tidak ditemukan.';
$lang['revisi_list_order_no_item_order']			= 'Tidak ada barang yang diorder';

$lang['revisi_list_order_create_failure']		= 'Data revisi_list_order gagal disimpan';
$lang['revisi_list_order_create_success']		= 'Data revisi_list_order berhasil disimpan';

$lang['revisi_list_order_edit_success']			= 'Data revisi_list_order berhasil disimpan';
$lang['revisi_list_order_invalid_id']			= 'ID tidak Valid';

