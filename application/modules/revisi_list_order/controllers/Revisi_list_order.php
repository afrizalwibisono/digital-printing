<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 * 
 * This is controller for order_produk

 */

class Revisi_list_order extends Admin_Controller {
    
    /**
     * Load the models, library, etc
     *
     * 
     */
    //Permission
    protected $viewPermission   = "Revisi List Order.View";
    protected $addPermission    = "Revisi List Order.Add";
    protected $managePermission = "Revisi List Order.Manage";
    protected $deletePermission = "Revisi List Order.Delete";
    protected $uploadFolder = "./upload";
    protected $akumulasi = true;

    public function __construct()
    {
        parent::__construct();

        $this->auth->restrict($this->viewPermission);

        $this->lang->load('revisi_list_order');
        $this->load->model(array(
                                'revisi_list_order/revisi_list_order_model',
                                'revisi_list_order/order_produk_model',
                                'revisi_list_order/produk_model',
                                'revisi_list_order/konsumen_model',
                                'revisi_list_order/order_produk_detail_model',
                                'revisi_list_order/order_produk_detail_finishing_model',
                                'revisi_list_order/order_produk_detail_a3_model',
                                'revisi_list_order/morder_produk_detail_model',
                                'revisi_list_order/morder_produk_detail_finishing_model',
                                'revisi_list_order/morder_produk_detail_a3_model',
                                'order_produk/kategori_model',
                                'konversi_satuan/konversi_satuan_model',
                                'setting_harga/setting_harga_model',
                                'setting_harga/setting_harga_detail_model',
                                'setting_promo/setting_promo_model',
                                'setting_upload/setting_upload_model',
                                'revisi_list_order/Setting_harga_urgent_model',
                                'produksi/produksi_model',
                                'validasi_cetak/kasir_order_po_detail_model',
                                'spk/spk_model',
                                'approve_revisi/app_revisi_model',
                            ));
        $this->template->title(lang('revisi_list_order_title_manage'));
        $this->template->page_icon('fa fa-table');
        if (app_get_setting("app.akumulasi_harga") == 1) {
            $this->akumulasi = false;
        }
    }

    public function index()
    {
        $this->auth->restrict($this->viewPermission);
        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search     = isset($_POST['table_search']) ? $this->input->post('table_search') :'';
            $tgl_awal   = isset($_POST['tgl_awal']) ? $this->input->post('tgl_awal') : '';
            $tgl_akhir  = isset($_POST['tgl_awal']) ? $this->input->post('tgl_akhir') : '';
        }
        else
        {
            $search     = isset($_GET['search']) ? $this->input->get('search') :'';
            $tgl_awal   = isset($_GET['tgl_awal']) ? $this->input->get('tgl_awal') : '';
            $tgl_akhir  = isset($_GET['tgl_awal']) ? $this->input->get('tgl_akhir') : '';
        }

        $filter = "?search=".$search;
        $addWhere = "";
        if($tgl_awal != '' && $tgl_akhir != '')
        {
            $tgl_awal = date_format(date_create($tgl_awal),"Y-m-d");
            $tgl_akhir = date_format(date_create($tgl_akhir), "Y-m-d");
            $filter   .= "&tgl_awal=".$tgl_awal."&tgl_akhir=".$tgl_akhir;
            $addWhere .= " AND ( date(revisi_list_order.created_on) >='".$tgl_awal."' AND date(revisi_list_order.created_on) <='".$tgl_akhir."')";
        }
        else
        {
            $tgl_awal = date('Y-m-d');
            $tgl_akhir = date('Y-m-d');
            
            $filter   .= "&tgl_awal=".$tgl_awal."&tgl_akhir=".$tgl_akhir;
            $addWhere .= " AND ( date(revisi_list_order.created_on) >='".$tgl_awal."' AND date(revisi_list_order.created_on) <='".$tgl_akhir."')";
        }
        $search2 = $this->db->escape_str($search);
        
        $where = "transaksi.st_history = 0
                AND transaksi.deleted = 0
                AND (`nama` LIKE '%$search2%'
                OR `nm_produk` LIKE '%$search2%' ESCAPE '!'
                OR `no_transaksi` LIKE '%$search2%' ESCAPE '!'
                OR `no_faktur` LIKE '%$search2%' ESCAPE '!')
                $addWhere";
                
        $total = $this->revisi_list_order_model
                    ->join("m_order_produk_detail","m_order_produk_detail.id_detail_produk_order = revisi_list_order.old_id_detail_produk_order","left")
                    ->join("order_produk","order_produk.id_order = m_order_produk_detail.id_order","left")
                    ->join("transaksi","order_produk.id_order= transaksi.id_order","left")
                    ->join('produk','produk.idproduk = m_order_produk_detail.id_produk','left')
                    ->join('konversi_satuan','konversi_satuan.id_konversi=m_order_produk_detail.satuan_cetak_by','left')
                    ->join("konsumen","konsumen.idkonsumen=order_produk.id_konsumen","left")
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $assets = array('plugins/select2/dist/js/select2.js', 
                        'plugins/select2/dist/css/select2.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js', 
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
                        'revisi_list_order/assets/js/index_revisi_list_order.js',
                        'plugins/number/jquery.number.js'
                        );
        
        add_assets($assets);

        $data = $this->revisi_list_order_model->select('revisi_list_order.*, m_order_produk_detail.sub_total, m_order_produk_detail.jml_cetak, m_order_produk_detail.id_detail_produk_order, transaksi.no_faktur, konsumen.nama, produk.nm_produk, order_produk.no_transaksi')
                    ->join("m_order_produk_detail","m_order_produk_detail.id_detail_produk_order = revisi_list_order.old_id_detail_produk_order","left")
                    ->join("order_produk","order_produk.id_order = m_order_produk_detail.id_order","left")
                    ->join("transaksi","order_produk.id_order= transaksi.id_order","left")
                    ->join('produk','produk.idproduk = m_order_produk_detail.id_produk','left')
                    ->join('konversi_satuan','konversi_satuan.id_konversi=m_order_produk_detail.satuan_cetak_by','left')
                    ->join("konsumen","konsumen.idkonsumen=order_produk.id_konsumen","left")
                    ->where($where)
                    ->order_by('nama','ASC')
                    ->limit($limit, $offset)->find_all();

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('tgl_awal', date_format( date_create($tgl_awal), "d-m-Y"));
        $this->template->set('tgl_akhir',date_format(date_create($tgl_akhir), "d-m-Y"));
        $this->template->set("toolbar_title", lang('revisi_list_order_title_manage'));
        $this->template->title(lang('revisi_list_order_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index'); 
    }

    public function create()
    {
        $this->auth->restrict($this->viewPermission);
        // Pagination
        $this->load->library('pagination');

        if(isset($_POST['table_search']))
        {
            $search     = isset($_POST['table_search']) ? $this->input->post('table_search') :'';
        }
        else
        {
            $search     = isset($_GET['search']) ? $this->input->get('search') :'';
        }

        $filter = "?search=".$search;
        $addWhere = "";
        $tgl_awal = date('Y-m-d');
        $tgl_akhir = date('Y-m-d');
        
        $filter   .= "&tgl_awal=".$tgl_awal."&tgl_akhir=".$tgl_akhir;
        $addWhere .= " AND ( date(tgl_order) >='".$tgl_awal."' AND date(tgl_order) <='".$tgl_akhir."')";
        $search2 = $this->db->escape_str($search);

        $data_app_revisi = $this->app_revisi_model->find_all_by(['deleted'=>0, 'st_acc' => 1, 'tgl_valid' => date('Y-m-d') ]);
        $id_approve=[];
        if ($data_app_revisi) {
            foreach ($data_app_revisi as $key => $approve) {
                $id_approve[]= $approve->id_order;
            }
        }

        // die(print_r($id_approve));
        if (count($id_approve) > 0) {
            $addWhere .= "OR (order_produk.id_order IN ('". implode(',', $id_approve) ."') AND order_produk.deleted = 0 AND m_order_produk_detail.deleted = 0 AND transaksi.st_history = 0 )";
        }
        
        $where = "order_produk.deleted = 0 AND (transaksi.st_history = 0 or st_history is null)
                AND st_simpan = 1 AND m_order_produk_detail.deleted=0
                $addWhere
                AND (`nama` LIKE '%$search2%' ESCAPE '!'
                OR `no_transaksi` LIKE '%$search2%' ESCAPE '!'
                OR `no_faktur` LIKE '%$search2%' ESCAPE '!')";
                
        $total = $this->morder_produk_detail_model
                    ->join("order_produk","m_order_produk_detail.id_order = order_produk.id_order","left")
                    ->join("transaksi","order_produk.id_order= transaksi.id_order","left")
                    ->join('produk','produk.idproduk = m_order_produk_detail.id_produk','left')
                    ->join('konversi_satuan','konversi_satuan.id_konversi=m_order_produk_detail.satuan_cetak_by','left')
                    ->join("konsumen","konsumen.idkonsumen=order_produk.id_konsumen","left")
                    ->where($where)
                    ->count_all();

        $offset = $this->input->get('per_page');

        $limit = $this->config->item('list_limit');

        $this->pager['base_url']            = current_url().$filter;
        $this->pager['total_rows']          = $total;
        $this->pager['per_page']            = $limit;
        $this->pager['page_query_string']   = TRUE;

        $this->pagination->initialize($this->pager);

        $assets = array('plugins/select2/dist/js/select2.js', 
                        'plugins/select2/dist/css/select2.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js', 
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
                        'revisi_list_order/assets/js/index_revisi_list_order.js',
                        'plugins/number/jquery.number.js'
                        );
        
        add_assets($assets);

        $data = $this->morder_produk_detail_model
                    ->join("order_produk","m_order_produk_detail.id_order = order_produk.id_order","left")
                    ->join("transaksi","order_produk.id_order= transaksi.id_order","left")
                    ->join('produk','produk.idproduk = m_order_produk_detail.id_produk','left')
                    ->join('konversi_satuan','konversi_satuan.id_konversi=m_order_produk_detail.satuan_cetak_by','left')
                    ->join("konsumen","konsumen.idkonsumen=order_produk.id_konsumen","left")
                    ->where($where)
                    ->order_by('nama','ASC')
                    ->limit($limit, $offset)->find_all();

        $this->template->set('results', $data);
        $this->template->set('search', $search);
        $this->template->set('tgl_awal', date_format( date_create($tgl_awal), "d-m-Y"));
        $this->template->set('tgl_akhir',date_format(date_create($tgl_akhir), "d-m-Y"));
        $this->template->set("toolbar_title", lang('revisi_list_order_title_manage'));
        $this->template->title(lang('revisi_list_order_title_manage'));
        $this->template->set("numb", $offset+1);
        $this->template->render('index_create'); 
    }
    public function update_order(){
        $id_detail_order = $this->input->post('id');
        $jml_revisi = $this->input->post('jml_revisi');
        $jml_tercetak = 0;
        $ket = $this->input->post('ket');
        $cek= true;
        if (isset($id_detail_order) && isset($jml_revisi) && isset($ket)) {
            $data_detail_order = $this->morder_produk_detail_model
                                ->join('order_produk','order_produk.id_order = m_order_produk_detail.id_order','left')
                                ->find($id_detail_order);
            // cek nota sementara
            $del_nota_sementara = TRUE;
            // if ($data_detail_order->st_ctk_struk_tmp == 1) {
            //     if ($data_detail_order->st_acc_delete == 1 &&  ) {
            //         $del_nota_sementara = TRUE;
            //     }else{
            //         $del_nota_sementara = FALSE;
            //     }
            // }
            if ($del_nota_sementara == TRUE) {
                $data_validasi_cetak = $this->kasir_order_po_detail_model
                                        ->join('kasir_order_po', 'kasir_order_po_detail.id_kasir_order_po = kasir_order_po.id_kasir_order_po', 'left')
                                        ->find_by(['kasir_order_po.deleted'=>0, 'kasir_order_po_detail.deleted'=>0, 'kasir_order_po_detail.id_m_order_detail'=> $id_detail_order]);
                $data_produksi = $this->produksi_model->select('sum(jml_cetak) as jml_tercetak')->find_all_by(['kode_universal'=> $data_detail_order->kode_universal, 'deleted'=>0]);
                $spk = $this->spk_model->find($data_detail_order->id_spk);
                if ($data_produksi[0]->jml_tercetak> 0) {
                    $jml_tercetak = $data_produksi[0]->jml_tercetak;
                }
                if (is_array($spk) && count($spk)>0) {
                    if ($spk->st_selesai != 0) {
                         if ($jml_revisi < $spk->jml_cetak) {
                            $return = ['type' => 'error', 'msg' => 'Jumlah revisi kurang jumlah yang susah diproduksi'];
                            $cek= false;
                         }
                     } 
                }


                if (!empty($data_validasi_cetak) && count($data_validasi_cetak)>0) {
                // jika sudah divalidasi cetak tidak bisa direvisi
                    $return = ['type' => 'error', 'msg' => 'Tidak dapat direvisi karena order sudah divalidasi cetak'];
                    $cek= false;
                }elseif($jml_revisi < $jml_tercetak){
                    // jika sudak diproduksi maka tidak bisa direvisi kurang dari yang sudah dicetak
                    $return = ['type' => 'error', 'msg' => 'Jumlah yang telah diproduksi lebih beras dari jumlah revisi'];
                    $cek = false;
                }else{
                    $data_det_finishing = $this->morder_produk_detail_finishing_model->find_all_by(['id_detail_produk_order'=>$id_detail_order]);
                    $data_det_a3 = $this->morder_produk_detail_a3_model->find_all_by(['id_detail_produk_order'=>$id_detail_order]);
                    $new_id_detail_order = gen_primary("DETO");
                    $new_data =  array(
                                        'id_detail_produk_order' => $new_id_detail_order,
                                        'kode_universal' => $data_detail_order->kode_universal,
                                        'id_order' => $data_detail_order->id_order,
                                        'nama_pekerjaan' => $data_detail_order->nama_pekerjaan,
                                        'id_kategori' => $data_detail_order->id_kategori,
                                        'id_produk'                 => $data_detail_order->id_produk,
                                        'st_urgent'                 => $data_detail_order->st_urgent,
                                        'tgl_permintaan_selesai'    => $data_detail_order->tgl_permintaan_selesai,
                                        'gambar'                    => $data_detail_order->gambar,
                                        'thumbnail'                 => $data_detail_order->thumbnail,
                                        'st_dimensi'                => $data_detail_order->st_dimensi,
                                        'p'                         => $data_detail_order->p,
                                        'l'                         => $data_detail_order->l,
                                        'id_satuan'                 => $data_detail_order->id_satuan,
                                        'detail_gambar'             => $data_detail_order->detail_gambar,
                                        'jml_cetak'                 => $jml_revisi,
                                        'harga_by'                  => $data_detail_order->harga_by,
                                        'id_konversi_jml_cetak'     => $data_detail_order->id_konversi_jml_cetak,
                                        'qty_cetak_by'              => $data_detail_order->qty_cetak_by,
                                        'satuan_cetak_by'           => $data_detail_order->satuan_cetak_by,
                                        'harga_cetak_persatuan_terkecil'=> $data_detail_order->harga_cetak_persatuan_terkecil,
                                        'biaya_design'              => $data_detail_order->biaya_design,
                                        'biaya_cetak'               => $data_detail_order->biaya_cetak,
                                        'kenaikan_value'            => $data_detail_order->kenaikan_value,
                                        'kenaikan_persen'           => $data_detail_order->kenaikan_persen,
                                        'biaya_finishing'           => $data_detail_order->biaya_finishing,
                                        'sub_total'                 => $data_detail_order->sub_total,
                                        'catatan'                   => $data_detail_order->catatan,
                                        'st_finishing'              => $data_detail_order->st_finishing,
                                        'st_finishing_standart'     => $data_detail_order->st_finishing_standart,
                                        'target_server'             => $data_detail_order->target_server,
                                        'st_acc_produksi'           => 1,
                                        'id_spk'                    => $data_detail_order->id_spk,
                                     );
                    $this->db->trans_start();
                        $query="";
                        $selisih = 0;
                        if($cek == true){
                            if ($jml_tercetak >0) {
                            $selisih = $jml_revisi - $data_produksi[0]->jml_tercetak;
                            }else{
                                // delete SPK
                                if ($data_detail_order->id_spk != null) {
                                    $cek= delete_spk($data_detail_order->id_spk);
                                    $query .= "/n/n".$this->db->last_query();
                                }
                            }
                            // save history  revisi
                            $add_data_revisi = $this->revisi_list_order_model->insert(['idrevisi_list_order'=>gen_primary("REVLO"),'old_id_detail_produk_order'=> $data_detail_order->id_detail_produk_order,'new_id_detail_produk_order'=>$new_id_detail_order,'st_revisi'=>0,'ket'=>$ket]);
                            $query .= "/n/n".$this->db->last_query();

                            // update data m_order tabel
                            $update = $this->morder_produk_detail_model->insert($new_data);
                            $query .= "/n/n".$this->db->last_query();

                            // simpan log proses
                            $diff = $jml_revisi - $data_detail_order->jml_cetak;
                            set_proses_order($id_lokasi = 4, $id_ket_proses = 2, $id_detail_produk_order = $data_detail_order->id_detail_produk_order, $jumlah_proses = $diff);

                            //delete old data m_order tabel
                            $this->morder_produk_detail_model->delete($id_detail_order);
                            $query .= "/n/n".$this->db->last_query();
                            // update detail a3
                            if(is_array($data_det_a3) && count($data_det_a3)>0){
                                foreach ($data_det_a3 as $key => $value) {
                                    $new_data_a3[] = array(
                                        'idorder_produk_detail_a3'=> gen_primary("DETA3"),
                                        'id_detail_produk_order' => $new_id_detail_order,
                                        'page' => $value->page,
                                        'jml_print' => $value->jml_print,
                                    );
                                }
                                $this->db->insert_batch('m_order_produk_detail_a3', $new_data_a3);
                                $query .= "/n/n".$this->db->last_query();
                            }
                            // update finishing
                            if ($data_det_finishing) {
                                $total_finishing = 0;
                                foreach ($data_det_finishing as $key => $finishing) {

                                    $_POST['id_produk'] = $finishing->id_produk;
                                    $_POST['id_konsumen'] = $data_detail_order->id_konsumen;
                                    $_POST['tgl_order'] = $data_detail_order->tgl_order;
                                    $_POST['p'] = $data_detail_order->p;
                                    $_POST['l'] = $data_detail_order->l;
                                    $_POST['id_konversi_gambar'] = $data_detail_order->id_satuan;
                                    $_POST['jml_order'] = $jml_revisi;
                                    $_POST['id_konversi_jml_order'] = $data_detail_order->id_konversi_jml_cetak;
                                    $_POST['jml_finishing'] = $finishing->jml;

                                    $new_harga_finishing = $this->get_harga_finishing();

                                    // add new data finishing
                                    $this->morder_produk_detail_finishing_model->insert(
                                        [ 
                                            'id_detail_produk_finishing_order' => gen_primary('DETPF'),
                                            'id_detail_produk_order' => $new_id_detail_order,
                                            'id_produk' => $this->input->post('id_produk'),
                                            'jml' => $this->input->post('jml_finishing'),
                                            'harga' => $new_harga_finishing,
                                            'catatan' => $finishing->catatan,
                                        ]
                                    );
                                    $query .= "/n/n".$this->db->last_query();
                                    $total_finishing += $new_harga_finishing;
                                }
                                // update total finishing on new data m_order_produk_detail tabel
                                $this->morder_produk_detail_model->update($new_id_detail_order,['biaya_finishing'=>$total_finishing]);
                                $query .= "/n/n". $this->db->last_query();
                            }
                            if ($this->akumulasi) {
                                $update_list = update_list($data_detail_order->id_produk, $data_detail_order->id_order, 1);
                            }else{
                                $update_list = update_total($data_detail_order->id_order);
                            }
                            $this->order_produk_model->update($data_detail_order->id_order, ['st_revisi'=>1]);
                            $query .= "/n/n". $this->db->last_query();
                            
                            // create New SPK
                            if ($data_detail_order->id_spk != null) {
                                if ($selisih > 0) {
                                    if ($spk->st_selesai != 0) {
                                        simpan_spk(0,$new_id_detail_order,8,1, $selisih);
                                    }else{
                                        // update jml spk 
                                        $this->spk_model->update($data_detail_order->id_spk, ['jml_cetak'=> $jml_revisi]);
                                    }
                                }else{
                                    $this->spk_model->delete($data_detail_order->id_spk);
                                    simpan_spk(0,$new_id_detail_order,8,1);
                                }
                            }

                            if ($update_list) {
                                $keterangan = "SUKSES, Update detail List order, dengan ID : ".$id_detail_order;
                                $status     = 1;
                            }else{
                                $keterangan = "GAGAL, Update detail List order, dengan ID : ".$id_detail_order;
                                $status     = 0;
                            }
                            $nm_hak_akses   = $this->managePermission; 
                            $kode_universal = $id_detail_order;
                            $jumlah         = 1;
                            $sql            = $query;
                            simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                        }
                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE)
                    {
                        $return = ['type' => 'error', 'msg' => $this->morder_produk_detail_model->error."\n\n".$this->morder_produk_detail_finishing_model->error. $cek];
                    }else{
                        $return = ['type' => 'success', 'msg' => 'Update Data Order Sukses',];
                    }
                }
            }else{
                $return = ['type' => 'error', 'msg' => 'Tidak dapat direvisi! Detail order sudah dibuat nota sementara.'];   
            }
            echo json_encode($return);
        }
    }
    public function delTree($dir) {
        $files = array_diff(scandir($dir), array('.','..'));
        foreach ($files as $file) {
          (is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    } 
    public function get_harga_finishing(){
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('order_produk_only_ajax'), 'error');
            redirect('order_produk');
        }

        $id_produk          = $this->input->post('id_produk');
        $id_konsumen        = $this->input->post('id_konsumen');
        $tgl_order          = $this->input->post('tgl_order');
        $panjang            = $this->input->post('p');
        $lebar              = $this->input->post('l');
        $id_konversi_gambar = $this->input->post('id_konversi_gambar');
        $jml_order          = $this->input->post('jml_order');
        $id_konversi_jml_order = $this->input->post('id_konversi_jml_order');
        $jml_finishing      = $this->input->post('jml_finishing');
        // variabel
        $harga_finishing=0;
        $sub_harga  =0;
        $harga      =0;
        $cek = false;


        // real panjang
        $data_real_p            = $this->konversi_satuan_model->hitung_ke_satuan_kecil($id_konversi_gambar, floatval($panjang));
        $real_p                 = $data_real_p['qty'];


        // real lebar
        $data_real_l            = $this->konversi_satuan_model->hitung_ke_satuan_kecil($id_konversi_gambar, floatval($lebar));
        $real_l                 = $data_real_l['qty'];

        
        $data_konsumen = $this->konsumen_model->find_by(['idkonsumen'=>$id_konsumen]);
        // statu konsumen 0: biasa 1: reseller 2:instansi
        $status_konsumen = $data_konsumen->st;
        $total_jml_finishing = $jml_finishing * $jml_order;
        $luas = ($real_l * $real_p) * $total_jml_finishing;
        
        $promo = $this->setting_promo_model->where("DATE(tgl_awal) <=  '$tgl_order' AND date(tgl_akhir) >= '$tgl_order'")->find_by(['id_produk'=>$id_produk]);
        if($promo){
            
            $data_harga = get_harga_produk($id_produk, 3, $luas, $total_jml_finishing);
            
            $harga = $data_harga['harga'];
        }else{
            $data_harga = get_harga_produk($id_produk, $status_konsumen, $luas, $total_jml_finishing);
            $harga = $data_harga['harga'];
        }
        if($harga == 0){
            $data_harga =  get_harga_produk($id_produk,1,$luas , $total_jml_finishing);
            $harga = $data_harga['harga'];
        }

        return $harga;
    }
    
    protected function buat_kode($prefix = "TRX")
    {
        $month  = date("m", strtotime(date("Y/m/d")));
        $year   = date("Y", strtotime(date("Y/m/d")));
        $day    = date("d", strtotime(date("Y/m/d")));
        $user_id = $this->auth->user_id();
        $dt_trx = $this->order_produk_model->order_by('no_transaksi','ASC')->find_all_by(array('MONTH(tgl_order)'=>$month, 'YEAR(tgl_order)'=>$year,'DAY(tgl_order)'=>$day, 'deleted'=>0, 'created_by'=> $user_id));
        $last_kode = "";
        if($dt_trx)
        {
            foreach ($dt_trx as $key => $dp) {
                $last_kode = $dp->no_transaksi;
            }
        }

        if($last_kode)
        {
            $tkode = explode("-", $last_kode);
            if(count($tkode) == 2)
            {
                $user_kode = explode("D", $tkode[1]);
                $tkode = intval($user_kode[0]);
            }
            else
            {
                $tkode = 0;
            }
            $kode = strtoupper($prefix)."/".$year."/".$month.$day."-" . str_pad($tkode+1, 5, "0", STR_PAD_LEFT)."D".$user_id;
        }
        else
        {
            $kode = strtoupper($prefix)."/".$year."/".$month.$day."-00001"."D".$user_id;
        }
        return $kode;
    }

    // delete order
    public function delete_order()
    {
        $this->auth->restrict($this->deletePermission);

        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('order_produk_only_ajax'), 'error');
            redirect('order_produk');
        }
        $id = $this->input->post('id');
        $ket = $this->input->post('ket');
        if (isset($id)) {
            $data = $this->morder_produk_detail_model
                    ->join('order_produk','order_produk.id_order=m_order_produk_detail.id_order','left')
                    ->find($id);
            // cek nota sementara
            $del_nota_sementara = TRUE;
            if ($data->st_ctk_struk_tmp == 1) {
                if ($data->st_acc_delete == 1 && $data->st_acc_delete == 0 && $data->st_kasir == 0) {
                    $del_nota_sementara = FALSE;
                }else{
                    $del_nota_sementara = TRUE;
                }
            }
            if ($del_nota_sementara == TRUE) {
                $cek_del= true;
                $data_produksi = $this->produksi_model->select('sum(jml_cetak) as jml_tercetak')->find_all_by(['kode_universal'=> $data->kode_universal, 'deleted'=>0]);
                if ($data_produksi[0]->jml_tercetak>0) {
                    $return = ['type' => 'error', 'msg' => 'Order sudah diproduksi, Tidak dapap dihapus'];
                }else{
                    if ($data->id_spk != null) {
                        $cek_del = delete_spk($data->id_spk);
                    }
                    if ($cek_del === true) {
                        $del = $this->morder_produk_detail_model->delete($id);

                        // simpan log proses
                        set_proses_order($id_lokasi = 4, $id_ket_proses = 11, $id_detail_produk_order = $data->id_detail_produk_order, $jumlah_proses = 0);

                        if (@unlink($data->thumbnail)) {
                            $this->db->trans_start();
                                $query ="";
                                // save history  revisi
                                $add_data_revisi = $this->revisi_list_order_model->insert(['idrevisi_list_order'=>gen_primary("REVLO"),'old_id_detail_produk_order'=> $id,'st_revisi'=>1,'ket'=>$ket]);
                                $query .= "\n\n". $this->db->last_query();
                                $data_del = $this->morder_produk_detail_model->find($id);
                                $del = $this->morder_produk_detail_model->delete($id);
                                $query .= "\n\n". $this->db->last_query();
                                if (is_numeric($del)) {
                                    if ($this->akumulasi) {
                                        // update akumulasi harga
                                        update_list($data_del->id_produk, $data_del->id_order,1);
                                    }else{
                                        update_total($data_del->id_order);
                                    }
                                    

                                    // update st_revisi
                                    $this->order_produk_model->update($data_del->id_order, ['st_revisi'=>1]);
                                    $query .= "\n\n". $this->db->last_query();

                                    $keterangan = "SUKSES, Hapus detail order, dengan ID : ".$id;
                                    $status     = 1;
                                    $return = ['type' => 'success', 'msg' => 'Sukses hapus data detail order.'];
                                }else{
                                    $keterangan = "GAGAL, Hapus detail order, dengan ID : ".$id;
                                    $status     = 0;
                                    $return = ['type' => 'error', 'msg' => 'Gagal hapus data detail order.'];
                                }
                                $nm_hak_akses   = $this->deletePermission; 
                                $kode_universal = $id;
                                $jumlah         = 1;
                                $sql            = $query;
                                simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
                            $this->db->trans_complete();
                            if ($this->db->trans_status() === FALSE)
                            {
                                $return = ['type' => 'error', 'msg' => $this->order_produk_model->error."\n\n".$this->morder_produk_detail_finishing_model->error."\n\n".$this->revisi_list_order_model->error];
                            }
                            else
                            {
                                $return = ['type' => 'success', 'msg' => 'Delete Data Order Sukses',];
                            }     
                        }
                    }else{
                        $return = ['type' => 'error', 'msg' => $cek_del];    
                    }
                }
            }else{
                $return = ['type' => 'error', 'msg' => 'Tidak dapat dihapus! Detail order sudah dibuat nota sementara.'];
            }
        }else{
            $return = ['type' => 'error', 'msg' => 'Data Tidak Ditemukan.'];
        }
        echo json_encode($return);
    }
    public function view_detail_order()
    {
        $this->auth->restrict($this->addPermission);

        if(!$this->input->is_ajax_request())
        {
            $this->template->set_message(lang('order_produk_only_ajax'), 'error');
            redirect('order_produk');
        }
        $id = $this->input->post('id');
        $setting_upload = $this->setting_upload_model->find(1);
        if (isset($id)) {
            $sql="select t.*, t1.satuan_besar as satuan_panjang, t2.satuan_besar as satuan_order, nm_produk, nmkategori
                    from m_order_produk_detail as t
                    left join produk on t.id_produk = produk.idproduk
                    left join konversi_satuan t1 on t1.id_konversi= t.id_satuan
                    left join konversi_satuan t2 on t2.id_konversi= t.id_konversi_jml_cetak
                    left join kategori on kategori.idkategori= t.id_kategori
                    where t.id_detail_produk_order = '$id'";
            $data_detail = $this->db->query($sql)->row();
            if ($data_detail->st_finishing ==1){
                $data_det_finishing= $this->morder_produk_detail_finishing_model
                                        ->join('konversi_satuan','m_order_produk_detail_finishing.id_konversi=konversi_satuan.id_konversi','left')
                                        ->join('produk','produk.idproduk=m_order_produk_detail_finishing.id_produk','left')
                                        ->find_all_by(['id_detail_produk_order'=>$data_detail->id_detail_produk_order]);
                if ($data_det_finishing) {
                    $return = ['type'=> 'success', 'data' => $data_detail,'detail'=>$data_det_finishing,'setting_upload'=> $setting_upload];    
                }else{
                    $return = ['type'=> 'success', 'data' => $data_detail,'detail'=>'','setting_upload'=> $setting_upload];
                }
            }else{
                $return = ['type'=> 'success', 'data' => $data_detail, 'detail'=>'','setting_upload'=> $setting_upload];
            }
        }else{
            $return = ['type' => 'error', 'msg' => 'Data Tidak Ditemukan.'];
        }
        echo json_encode($return);
    }
    public function view_detail($id_detail_order='')
    {
        $this->auth->restrict($this->viewPermission);

        if (!$id_detail_order)
        {
            $this->template->set_message(lang("order_produk_invalid_id"), 'error');
            redirect('order_produk');
        }
        $setting_upload = $this->setting_upload_model->find(1);
        $sql="select t.*, t1.satuan_besar as satuan_panjang, t2.satuan_besar as satuan_order, nm_produk, nmkategori
                from order_produk_detail as t
                left join produk on t.id_produk = produk.idproduk
                left join konversi_satuan t1 on t1.id_konversi= t.id_satuan
                left join konversi_satuan t2 on t2.id_konversi= t.id_konversi_jml_cetak
                left join kategori on kategori.idkategori= t.id_kategori
                where t.id_detail_produk_order = '$id_detail_order'";
        $data_detail = $this->db->query($sql)->row();
        if ($data_detail->st_finishing ==1){
            $data_det_finishing= $this->order_produk_detail_finishing_model
                                    ->join('konversi_satuan','order_produk_detail_finishing.id_konversi=konversi_satuan.id_konversi','left')
                                    ->join('produk','produk.idproduk=order_produk_detail_finishing.id_produk','left')
                                    ->find_all_by(['id_detail_produk_order'=>$data_detail->id_detail_produk_order]);
            if($data_det_finishing) {
                $this->template->set('detail', $data_det_finishing);
            }
        }
        $assets = array('plugins/select2/dist/js/select2.js', 
                        'plugins/select2/dist/css/select2.css',
                        'plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js', 
                        'plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
                        'plugins/number/jquery.number.js',
                        'order_produk/assets/js/order_produk.js',
                        );
        add_assets($assets);
        $this->template->set('data', $data_detail);
        $this->template->set('setting_upload', $setting_upload);
        $this->template->title(lang('order_produk_title_view'));
        $this->template->render('order_produk/view_detail');
    }

}
?>