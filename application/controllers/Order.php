<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author Cokeshome
 * @copyright Copyright (c) 2016, Cokeshome
 * 
 * This is controller for order_produk

 */

class Order extends Admin_Controller {

    protected $akumulasi = true;

    public function __construct()
    {
        parent::__construct();

        $this->load->model(array(
                                'order_produk/order_produk_model',
                                'order_produk/produk_model',
                                'order_produk/konsumen_model',
                                'order_produk/order_produk_detail_model',
                                'order_produk/morder_produk_detail_model',
                                'konversi_satuan/konversi_satuan_model',
                                'setting_harga/setting_harga_model',
                                'setting_harga/setting_harga_detail_model',
                                'setting_promo/setting_promo_model',
                            ));
        if (app_get_setting("app.akumulasi_harga") == 1) {
            $this->akumulasi = false;
        }
    }

    public function get_harga($id_produk="", $idkategori_harga=0, $luas=0 ,$qty =0)
    {
        // setting harga tidak boleh dibulatkan:
        // total quantity order / total qty setting hargai by berapa * harga setting
        $sub_harga =0;
        $total=0;
        $tipe= 0;
        $luas_harga=0;
        $harga_by =0;
        $qty_harga_by = 0;
        $id_konversi_harga_by =0;
        // $sisa_p    =$p;
        $data_harga = $this->setting_harga_model
                    ->join('produk','produk.idproduk=setting_harga.idproduk','left')
                    ->find_by(['setting_harga.idproduk'=>$id_produk,'idkategori_harga'=>$idkategori_harga,'setting_harga.deleted'=>0]);
        if ($data_harga) {
            // tipe 
            // 0= fix(pcs dll)
            // 1= custome/potongan (mm, mm dll)
            $tipe = $data_harga->st_tipe;
            if ($tipe==0) {

                if ($data_harga->st_range_harga == 1) {
                    $sub_harga = $data_harga->harga;
                    $harga_by  = $data_harga->harga_by;
                    $qty_harga_by = $data_harga->qty_harga_by;
                    $id_konversi_harga_by = $data_harga->id_konversi_harga_by;
                    $total = $data_harga->harga * ($qty/ $qty_harga_by);
                    $tipe=0;
                }else{
                    $range = $this->setting_harga_detail_model->order_by('range_b','DESC')->find_all_by(['idsettingharga'=>$data_harga->idsettingharga,'deleted'=>0]);
                    if($range){
                        foreach ($range as $key => $record) {
                            if ($qty > $record->range_b){
                                $sub_harga = $record->harga;
                                $harga_by  = $record->harga_by;
                                $qty_harga_by = $record->qty_harga_by;
                                $id_konversi_harga_by = $record->id_konversi_harga_by;
                                break;
                            }else{
                                if ($qty >= $record->range_a) {
                                    $sub_harga = $record->harga;
                                    $harga_by = $record->harga_by;
                                    $qty_harga_by = $record->qty_harga_by;
                                    $id_konversi_harga_by = $record->id_konversi_harga_by;
                                    break;
                                }
                            }
                        }
                        $total = $sub_harga * ($qty/ $qty_harga_by);
                        $tipe =0;
                    }
                }   
            }else{
                if ($data_harga->st_range_harga == 1) {
                    $sub_harga = $data_harga->harga;
                    $qty_harga_by = $data_harga->qty_harga_by;
                    $harga_by = $data_harga->harga_by;
                    $total = $data_harga->harga * ($luas/ $qty_harga_by);
                    $id_konversi_harga_by = $data_harga->id_konversi_harga_by;
                    $tipe =1;
                }else{
                    $range = $this->setting_harga_detail_model->order_by('range_b','DESC')->find_all_by(['idsettingharga'=>$data_harga->idsettingharga,'deleted'=>0]);
                    $cek = FALSE;
                    if($range){
                        foreach ($range as $key => $record) {
                            if (floatval($luas) > floatval($record->range_b)) {
                                $sub_harga = $record->harga;
                                $qty_harga_by = $record->qty_harga_by;
                                $harga_by = $record->harga_by;
                                $id_konversi_harga_by = $record->id_konversi_harga_by;
                                $cek = TRUE;
                                break;
                            }else{
                                if ($luas >= $record->range_a) {
                                    $sub_harga = $record->harga;
                                    $qty_harga_by = $record->qty_harga_by;
                                    $harga_by = $record->harga_by;
                                    $id_konversi_harga_by = $record->id_konversi_harga_by;
                                    $cek = TRUE;
                                    break;
                                }
                            }
                        }
                        if ($sub_harga == 0) {
                            foreach ($range as $key => $record) {
                                $sub_harga = $record->harga;
                                $qty_harga_by = $record->qty_harga_by;
                                $harga_by = $record->harga_by;
                                $id_konversi_harga_by = $record->id_konversi_harga_by;
                            }
                        }
                        if ($luas < $qty_harga_by) {
                            $luas= $qty_harga_by;
                        }
                        $total = $sub_harga * ($luas/$qty_harga_by);
                        $tipe =1;
                    }
                }
            }
        }
        $return = array('harga'=>$total,'tipe'=>$tipe, 'qty_harga_by' => $qty_harga_by, 'id_konversi_harga_by' => $id_konversi_harga_by, 'biaya_cetak_by' => $sub_harga, 'harga_by' => $harga_by);
        return $return;
    }
    
    // id_ produk : produk id will be updated
    // id_order: main id_order as reference to select all data detail with have same id_produk on detail order_produk
    // status_order: [0: create new order 1: update order]
    public function update_list_harga($id_produk='', $id_order='', $status_order=0)
    {
        if ($id_produk !="" && $id_order !="") {
            if ($status_order === 0) {
                $data_order = $this->order_produk_detail_model->find_all_by(['id_order'=>$id_order,'id_produk'=>$id_produk,'deleted'=>0]);
            }else{
                $data_order = $this->morder_produk_detail_model->find_all_by(['id_order'=>$id_order,'id_produk'=>$id_produk,'deleted'=>0]);   
            }
            
            if (is_array($data_order) && count($data_order)>0) {
                $main_order = $this->order_produk_model->find($id_order);
                $id_konsumen= $main_order->id_konsumen;
                $luas_total_order=0;
                $qty_total_order = 0;
                if ($data_order) {
                    foreach ($data_order as $key => $record) {
                        $data_real_p            = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_satuan, floatval($record->p));
                        $real_p                 = $data_real_p['qty'];
                        // real lebar
                        $data_real_l            = $this->konversi_satuan_model->hitung_ke_satuan_kecil($record->id_satuan, floatval($record->l));
                        $real_l                 = $data_real_l['qty'];

                        // khusus indoor
                        if ($this->akumulasi !== true) {
                            $produk = $this->produk_model->find($id_produk);
                            if ($produk->idkategori == 3) {
                                $lebar_produk= $this->konversi_satuan_model->hitung_ke_satuan_kecil($produk->id_konversi_ukuran, floatval($produk->lebar_minimal));
                                $real_lebar_produk = $lebar_produk['qty'];
                                if ($real_l < $real_lebar_produk) {
                                    $real_l = $real_lebar_produk;
                                }
                            }
                        }

                        $luas_total_order += ($real_l * $real_p) * $record->jml_cetak;
                        $qty_total_order += $record->jml_cetak;
                    }
                }
                $data_konsumen = $this->konsumen_model->find_by(['idkonsumen'=>$id_konsumen]);
                // statu konsumen 0: biasa 1: reseller 2:instansi
                $status_konsumen = $data_konsumen->st;
                $tgl_order = $main_order->tgl_order;
                // cek promo
                $promo = $this->setting_promo_model->where("tgl_awal <= '$tgl_order' AND date(tgl_akhir) >= '$tgl_order'")->find_by(['id_produk'=>$id_produk, 'deleted'=>0]);
                if($promo){
                    $data_harga = $this->get_harga($id_produk, 3, $luas_total_order, $qty_total_order);
                }else{
                    $data_harga = $this->get_harga($id_produk, $status_konsumen, $luas_total_order, $qty_total_order);
                }
                if($data_harga['harga'] == 0){
                    $data_harga =  $this->get_harga($id_produk, 1, $luas_total_order , $qty_total_order);
                }

                if ($data_harga['tipe']==1) {
                    $cek_min_biaya_cetak = false;
                    if (($luas_total_order/$data_harga['qty_harga_by'] * $data_harga['biaya_cetak_by']) < ($data_harga['biaya_cetak_by'])) {
                        $cek_min_biaya_cetak = true;
                    }
                }
                $query_update = "";
                foreach ($data_order as $key => $dtU) {
                    // tipe
                    // 0: fix(ext: pcs)
                    // 1: custom (meteran)
                    $new_biaya_cetak = 0;
                    $kenaikan_value = 0;
                    $real_biaya_cetak = 0;
                    $subtotal = 0;
                    if ($data_harga['tipe']==0) {
                        $new_biaya_cetak = ($dtU->jml_cetak/$data_harga['qty_harga_by']) * $data_harga['biaya_cetak_by'];
                        $kenaikan_value = ($dtU->kenaikan_persen/100) * $new_biaya_cetak;
                        $real_biaya_cetak = $new_biaya_cetak + $kenaikan_value;
                        $subtotal = $dtU->biaya_design + $dtU->biaya_finishing + $real_biaya_cetak;
                        $harga_cetak_persatuan_terkecil = $data_harga['biaya_cetak_by'];

                    }else{
                        $data_real_p            = $this->konversi_satuan_model->hitung_ke_satuan_kecil($dtU->id_satuan, floatval($dtU->p));
                        $real_p                 = $data_real_p['qty'];


                        // real lebar
                        $data_real_l            = $this->konversi_satuan_model->hitung_ke_satuan_kecil($dtU->id_satuan, floatval($dtU->l));
                        $real_l                 = $data_real_l['qty'];
                        if ($cek_min_biaya_cetak == true) {
                            $luas   = ($real_p * $real_l) * $dtU->jml_cetak;
                            $new_biaya_cetak = (($luas/$data_harga['qty_harga_by'])/($luas_total_order/$data_harga['qty_harga_by'])) * $data_harga['biaya_cetak_by'];
                            $kenaikan_value = ($dtU->kenaikan_persen/100) * $new_biaya_cetak;
                            $real_biaya_cetak = $new_biaya_cetak + $kenaikan_value;
                            $subtotal = $dtU->biaya_design + $dtU->biaya_finishing + $real_biaya_cetak;
                            $harga_cetak_persatuan_terkecil = ($new_biaya_cetak/$luas) * $data_harga['qty_harga_by'];
                        }else{
                            $luas   = ($real_p * $real_l) * $dtU->jml_cetak;
                            $new_biaya_cetak = ($luas/$data_harga['qty_harga_by']) * $data_harga['biaya_cetak_by'];
                            $kenaikan_value = ($dtU->kenaikan_persen/100) * $new_biaya_cetak;
                            $real_biaya_cetak = $new_biaya_cetak + $kenaikan_value;
                            $subtotal = $dtU->biaya_design + $dtU->biaya_finishing + $real_biaya_cetak;
                            $harga_cetak_persatuan_terkecil = $data_harga['biaya_cetak_by'];

                        }
                    }
                    $data_update = array('harga_cetak_persatuan_terkecil'=> $harga_cetak_persatuan_terkecil,
                                        'qty_cetak_by' => $data_harga['qty_harga_by'],
                                        'satuan_cetak_by' => $data_harga['id_konversi_harga_by'],
                                        'harga_by' => $data_harga['harga_by'],
                                        'kenaikan_value'=> $kenaikan_value,
                                        'biaya_cetak'=> $new_biaya_cetak,
                                        'sub_total'=> $subtotal);

                    if ($status_order === 0) {
                        //update before edit
                        $update= $this->order_produk_detail_model->update($dtU->id_detail_produk_order, $data_update);
                        $query_update .= "\n\n". $this->db->last_query();
                    }

                    // update after & before edit

                    $update_copy= $this->morder_produk_detail_model->update($dtU->id_detail_produk_order, $data_update);
                    $query_update .= "\n\n". $this->db->last_query();

                    if ($update_copy) {
                        $st_update = TRUE;
                    }else{
                        $st_update = FALSE;
                    }
                }
                if($st_update==TRUE){
                    $keterangan = "SUKSES, Update data list harga order dengan id_order : ".$id_order;
                    $status     = 1;
                }
                else
                {
                    $keterangan = "GAGAL, Update data list harga order dengan id_order : ".$id_order;
                    $status     = 0;
                }

                $nm_hak_akses   = "Order Produk.Manage"; 
                $kode_universal = $id_order;
                $jumlah         = 1;
                $sql            = $query_update;

                simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
            }
        }
        $this->update_total($id_order);
    }


    public function update_total($id_order){
        $data_list_order = $this->morder_produk_detail_model->find_all_by(['id_order'=>$id_order,'deleted'=>0]);
        if ($data_list_order) {
            $total_order=0;
            foreach ($data_list_order as $key => $record) {
                $total_order += $record->sub_total;
            }
            $update = $this->order_produk_model->update($id_order, ['total_value_order' => $total_order]);
        }else{
            $update = $this->order_produk_model->update($id_order, ['total_value_order' => 0]);
        }
        if ($update) {
            $keterangan = "SUKSES, Update data Total Order : ".$id_order;
            $status     = 1;
        }else{
            $keterangan = "GAGAL, Update data Total Order : ".$id_order;
            $status     = 0;
        }
        $nm_hak_akses   = "Order Produk.Manage"; 
        $kode_universal = $id_order;
        $jumlah         = 1;
        $sql            = $this->db->last_query();

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);
    }

}
?>