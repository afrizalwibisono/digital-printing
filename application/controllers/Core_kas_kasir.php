<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Core_kas_kasir extends Admin_Controller{
	
	function __construct(){

		parent::__construct();

		$this->load->model([
								'kas_kasir_model',
								'kas_kasir_detail_model',
                                'bayar_trans_model',
                                'bank_model',
                                'kasir/order_model'
							]);

	}

    public function delete_pembayaran($st_delete_by = 0,
                                        $id_primary_bayar = '',
                                        $id_transaksi_master = '',
                                        $waktu_start_delete = '',
                                        $keterangan_delete = '',
                                        $keterangan_kas_kasir = ''){

        /*
            ..:: Keterangan Parameter ::..
            
            "$st_delete_by"
                -   Status kriteria data bayar yang didelete
                -   Isi Parameter
                    -   0   = Berdasarkan ID primari pada tabel bayar_trans_kasir.
                    -   1   = Berdasarkan Id Transaksi Master / Id Transaksi saja.
                    -   2   = Berdasarkan Id Transaksi Master dan Waktu Start Bayar Transaksi.

            "$id_primary_bayar"
                -   Id Primary yang ada pada tabel bayar_trans_kasir, sebagai acuan delete.

            "$id_transaksi_master"
                -   Id transaksi yang akan didelete data pembayarannya.
                -   Untuk transaksi yang recursive maka sebagai Acuan Global.

            "$waktu_start_delete"
                -   Untuk transaksi yang dibayar berdasarkan cicilan, waktu sebagai acuan menghapus semua pembayaran untuk waktu setelahnya.
                -   Sebagai pembersih history, agar user tidak secara sembarangan menghapus cicilan.
                -   isi Parameter
                    -   Waktu dengan format Y-m-d H:i:s

            "$keterangan_delete"
                -   Keterangan untuk proses deletenya.

            "$keterangan_kas_kasir"
                -   Keterangan yang akan ditampilkan pada kas kasir keluarnya.


            ..:: Aturan / Rule ::..

            1. $keterangan_delete dan $keterangan_kas_kasir Wajib diisi.

            2. Jika 
                1.  $st_delete_by == 0
                        
                        Maka :  1. $id_primary_bayar, wajib diisi.

                2.  $st_delete_by == 1

                        Maka :  1. $id_transaksi_master, wajib diisi.

                3.  $st_delete_by == 2
                
                        Maka :  1. $id_transaksi_master, wajib diisi.
                                2. $waktu_start_delete, wajib diisi.

        */

        if(strlen($keterangan_delete) <= 0 || strlen($keterangan_kas_kasir) <= 0){

            return false;

        }

        $where  = "";

        switch ($st_delete_by) {
            case 0:
                
                if(strlen($id_primary_bayar) <= 0){

                    return false;

                }

                $where  = "idbayar_trans_kasir = '{$id_primary_bayar}'";

                break;
            case 1:
                
                if(strlen($id_transaksi_master) <= 0){

                    return false;

                }

                $where  = "id_transaksi_master = '{$id_transaksi_master}'";

                break;
            case 2:
                
                if(strlen($id_transaksi_master) <= 0 || strlen($waktu_start_delete) <= 0 ){

                    return false;

                }

                $where      = "created_on >= '{$waktu_start_delete}'
                                AND `id_transaksi_master` = '{$id_transaksi_master}'";        

                break;
            
            
        }

        // start hitung total yang akan dijadikan kas keluar

        $wh_total_uang  = $where." AND deleted = 0";
        $dt_bayar     = $this->bayar_trans_model
                                ->select("SUM(`bayar`) AS total_bayar_del")
                                ->where($wh_total_uang)
                                ->find_all();

        $total_uang = 0;

        if(is_array($dt_bayar) && count($dt_bayar)){

            $total_uang     = $dt_bayar[0]->total_bayar_del;

        }

        if($total_uang <= 0){

            return false;

        }

        // end hitung total yang akan dijadikan kas keluar

        $this->db->trans_start();

            $user       = $this->auth->userdata();
            $id_user    = $user->id_user;

            $arr_update     =   [
                                    'deleted'       => 1,
                                    'deleted_by'    => $id_user,
                                    'ket_delete'    => $keterangan_delete
                                ];    


            $this->bayar_trans_model->update_where($where,null,$arr_update);

            $ket    = $keterangan_kas_kasir;

            $this->set_alur_kas_kasir($ket, 0, 0, $total_uang);            

        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            $return     = false;

        }else{

            $return     = true;
        }


        return $return;

    }

    public function insert_bayar_trans($st_transaksi = 0,
                                        $id_transaksi = "",
                                        $st_bayar_trans = 0,
                                        $st_sumber = 0,
                                        $st_bayar = 0,
                                        $total_transaksi = 0,
                                        $tagihan = 0,
                                        $bayar  = 0,
                                        $st_dp = 0,
                                        $lokasi_file = "",
                                        $id_transaksi_master = "",
                                        $keterangan_kas_masuk = "",
                                        $order_record = 0){

        /*
            Catatan :
                
                1. Function ini hanya mencatat nilai pembayaran, untuk status Lunas disesuaikan dengan table transaksinya.
        
            ..:: Keterangan Parameter ::..

            "$st_transaksi"
                -   Status lokasi transaksi,
                -   Isi Parameter
                    -   0 = Transaksi Cetakan
                    -   1 = Transaksi Bahan Baku

            "$id_transaksi"
                -   Kode primary dari transaksi, baik itu transaksi kasir order / transaksi penjualan BB.
                -   Untuk transaksi referensi / turunan, maka tetap ID Primary transaksinya yang dimasukkan.

            "$st_bayar_trans"
                -   Karena tabel pembayaran digunakan untuk 4 jenis transaksi pembayaran Maka harus diberikan flag.
                -   Isi Parameter
                    -   0 = pembayaran via transaksi kasir, cetakan.
                    -   1 = pembayaran via pelunasan Piutang, cetakan.
                    -   2 = pembayaran via transaksi kasir, bahan baku.
                    -   3 = pembayaran via pelunasan Piutang, bahan baku.

            "$st_sumber"
                -   Sumber dana / pembayaran
                -   Isi Parameter
                    -   0 = pembayaran langsung di toko atau pembayaran selain deposit
                    -   1 = Deposit / dari simpanan

            "$st_bayar"
                -   Metode / cara pembayaran
                -   Isi Parameter
                    -   0 = cash
                    -   1 = debit
                    -   2 = transfer
                    -   3 = deposit

            "$total_transaksi"
                -   Total transaksi yang sedang diproses

            "$tagihan"
                -   Nilai yang akan dibayar pada saat itu, bukan nilai total tagihan pada nota.
                    Misalkan, Contoh.
                    Total transaksi     : 270,000
                    DP                  : 120,000
                    Bayar               : 150,000

                    Maka $tagihan == 120,000, bukan 270,000

            "$bayar"
                -   Nominal pembayaran yang dibayarkan oleh konsumen
            
            "$st_dp"
                -   Status pembayaran yang dilakukan
                -   Isi Parameter
                    -   0 = $tagihan & $bayar, merupakan pelunasan bukan DP.
                    -   1 = $tagihan & $bayar, merupakan DP dari transaksi.

            "$lokasi_file"
                -   Lokasi file attachmen

            "$id_transaksi_master"
                -   Untuk transaksi yang Notanya direferensi, maka id_referensi Nota utamanya harus diinputkan.        

            "$order_record"    
                -   Untuk mengurutkan jika dalam 1x proses memanggil function ini lebih dari 1x

        */

        // start cek bank default jika pembayaran tidak cash dan tidak dari deposit 

        $id_bank_pilih = null;
            
        if($st_sumber == 0 && $st_bayar != 0){

            $dt_bank        = $this->bank_model->select("id_bank")
                                    ->where("`st_default` = 1 and deleted = 0 ")
                                    ->find_all();

            if(is_array($dt_bank) && count($dt_bank)){

                $id_bank_pilih  = $dt_bank[0]->id_bank;

            }


        }

        // end cek bank default jika pembayaran tidak cash dan tidak dari deposit

        // kondisikan idmaster transaksi
        if(strlen($id_transaksi_master) <= 0){

            $id_transaksi_master = $id_transaksi;

        }

        // start cek total bayar yang sudah pernah dilakukan oleh konsumen terhadap transaksi yang sama

        /*$total_terbayar = 0;

        $dt_bayar   = $this->bayar_trans_model
                            ->select("sum(`bayar`) as total")
                            ->where("deleted = 0 
                                    and id_transaksi_master = '{$id_transaksi_master}'")
                            ->find_all();

        if(is_array($dt_bayar) && count($dt_bayar)){

            $total_terbayar = $dt_bayar[0]->total;
            
        }*/

        // start cek total bayar yang sudah pernah dilakukan oleh konsumen terhadap transaksi yang sama

        // start total semua pembayaran dengan metode Normal dan depositnya    

        $dt_bayar   = $this->bayar_trans_model
                            ->select("st_sumber_bayar,
                                        sum(`bayar`) as total")
                            ->where("deleted = 0 
                                    and id_transaksi_master = '{$id_transaksi_master}'")
                            ->group_by("st_sumber_bayar")
                            ->find_all();

        $total_bayar_deposit    = 0;
        $total_bayar_normal     = 0;
        $total_terbayar         = 0;

        if(is_array($dt_bayar) && count($dt_bayar)){

            foreach ($dt_bayar as $key => $isi_bayar) {
                
                if($isi_bayar->st_sumber_bayar == 1){

                    $total_bayar_deposit    += $isi_bayar->total;

                }else{

                    $total_bayar_normal    += $isi_bayar->total;

                }

                $total_terbayar            += $isi_bayar->total;

            }

        }

        // end total semua pembayaran dengan metode Normal dan depositnya    

        // start kondisikan nilai DP

        $nilai_dp           = 0;
        $nilai_dp_deposit   = 0;

        if($st_dp == 1){

            if($st_sumber == 0){

                $nilai_dp = $tagihan;
                    
            }else{

                $nilai_dp_deposit = $tagihan;

            }

        }else{

            $nilai_dp           = $total_bayar_normal;
            $nilai_dp_deposit   = $total_bayar_deposit;
        }

        // end kondisikan nilai DP

        // start cek kurang bayar / sisa piutang 

        $final_bayar            = $total_terbayar >= $total_transaksi ? $total_terbayar :  $total_terbayar + $tagihan;
        $kurang_bayar           = $total_transaksi - $final_bayar;

        // start kondisikan jika pembayaran > total transaksi
        $st_total_transaksi_menyusut    = 0;
        $st_kembalikan_ke_deposit       = 0;

        if($kurang_bayar < 0){
            
            $tagihan        = $kurang_bayar;
            $kurang_bayar   = 0;

            $st_total_transaksi_menyusut    = 1;

            // start ambil data konsumen dati transaksi
            $dt_konsumen    = $this->order_model->select('id_konsumen, transaksi.no_faktur' )
                                ->join('transaksi','order_produk.id_order = transaksi.id_order','inner')
                                ->where("transaksi.id_transaksi = '{$id_transaksi}'")
                                ->find_all();

            $id_konsumen    = $dt_konsumen[0]->id_konsumen;
            $faktur_trans   = $dt_konsumen[0]->no_faktur;

            // end ambil data konsumen dati transaksi

            // cek status deposit
            $status_deposit     = get_deposit($id_konsumen);

            if($status_deposit !== false){

                $st_kembalikan_ke_deposit   = 1;

            }

            // end cek status deposit

        }

        // start kondisikan jika pembayaran > total transaksi


        /*echo "total_transaksi : ".$total_transaksi;
        echo "total_terbayar :".$final_bayar;
        echo "Kurang_bayar :".$kurang_bayar;*/


        // end cek kurang bayar / sisa piutang 

        // start cek kembalian sesuai dengan bayar dan tagihan, kembalian merupakan kembalian uang untuk konsumen

        $kembalian  = 0;
        $sisa_balik = 0;

        if($tagihan < 0){

            $kembalian  = $bayar - $total_transaksi;
            
            if($st_kembalikan_ke_deposit == 1){

                $nilai_deposit_balik    = $total_bayar_deposit < $kembalian ? $total_bayar_deposit : $kembalian;
                $sisa_balik             = $kembalian - $nilai_deposit_balik;

            }

        }else if($bayar >= $tagihan){

            $kembalian = $bayar - $tagihan;

        }else{

            // jika bayar < tagihan maka error
            return false;

        }

        //echo "Bayar: ".$bayar."\nTotal terbayar: ".$total_terbayar."\nTagihan: ".$tagihan."\nTotal Transaksi: ".$total_transaksi."\nFinal bayar: ".$final_bayar."\nKurang Bayar: ".$kurang_bayar."\nKembalian :".$kembalian;
        //die("test");


        // end cek kembalian sesuai dengan bayar dan tagihan, kembalian merupakan kembalian uang untuk konsumen        

        $sql_all = "";

        $this->db->trans_start();

            if($st_transaksi == 0){

                $prefix = "KC";

            }else{

                $prefix = "KBB";

            }

            $id_primary = gen_primary($prefix, "bayar_trans_kasir", "idbayar_trans_kasir");

            //if($st_total_transaksi_menyusut    == 0){

                $arr_data   =   [
                                    'idbayar_trans_kasir'   => $id_primary, 
                                    'id_transaksi_master'   => $id_transaksi_master, 
                                    'id_transaksi'          => $id_transaksi, 
                                    'st_bayar_trans'        => $st_bayar_trans, 
                                    'st_sumber_bayar'       => $st_sumber, 
                                    'st_bayar'              => $st_bayar, 
                                    'id_bank'               => $id_bank_pilih, 
                                    'st_dp'                 => $st_dp,
                                    'bayar'                 => $tagihan, 
                                    'dp'                    => $nilai_dp, 
                                    'dp_deposit'            => $nilai_dp_deposit, 
                                    'pembayaran'            => $bayar, 
                                    'kurang_bayar'          => $kurang_bayar, 
                                    'kembalian'             => $kembalian, 
                                    'file'                  => $lokasi_file,
                                    'order'                 => $order_record
                                ];

                //print_r($arr_data);

                $proses = $this->bayar_trans_model->insert($arr_data);

                //var_dump($proses);

                //echo $this->db->last_query();

            //}

        $this->db->trans_complete();        

        if($this->db->trans_status() === false){

            $return = false;

        }else{

            $return = true;

            if($st_total_transaksi_menyusut == 0){            

                $status_kasir = $st_bayar >= 0 ? 1 : 0;

                $this->set_alur_kas_kasir($ket = $keterangan_kas_masuk, 
                                            $metode = $st_bayar, 
                                            $status = $status_kasir, 
                                            $jml = $tagihan);            

            }else{

                //start dapatkan nilai kas sebagai acuan pengurangan kas kasir atau pengembalian deposit

                $user           = $this->auth->userdata();
                $id_user        = $user->id_user;

                $date           = date('Y-m-d');
                /*$dt_kas_head    = $this->kas_kasir_model
                                                ->select("`idkas_kasir`")
                                                ->where("DATE(waktu_buat) = '{$date}'
                                                        AND created_by = {$id_user}
                                                        AND st_close = 0")
                                                ->find_all();
*/
                $dt_kas_head    = $this->kas_kasir_model
                                                ->select("`idkas_kasir`")
                                                ->where("created_by = {$id_user}
                                                        AND st_close = 0")
                                                ->find_all();


                $id_kasir           = $dt_kas_head[0]->idkas_kasir;



                /*$dt_kas_bergerak    = $this->kas_kasir_detail_model
                                            ->select("SUM(jumlah) AS total")
                                            ->where("st_metode = 3 and status = 1 and `idkas_kasir` = '{$id_kasir}'")
                                            ->group_by("idkas_kasir")
                                            ->find_all();   

                $nilai_pendapatan_deposit   = 0;

                if(count($dt_kas_bergerak) && is_array($dt_kas_bergerak)){

                    $nilai_pendapatan_deposit = $dt_kas_bergerak[0]->total;

                }*/

                $keterangan_pengembalian    = 'Pengembalian kelebihan bayar, dari transaksi '. $faktur_trans;

                if($st_kembalikan_ke_deposit == 1){         

                    set_deposit($id_konsumen = $id_konsumen, 
                                $nilai = $nilai_deposit_balik, 
                                $ket = $keterangan_pengembalian, 
                                $st_valid = 1, 
                                $waktu = null, 
                                $metode_bayar = 1, 
                                $rekening_pengirim = '', 
                                $input_file_resi = null);

                    $this->set_alur_kas_kasir($ket = $keterangan_pengembalian, 
                                                $metode = 3, 
                                                $status = 0, 
                                                $jml = $kembalian);  

                    // jika $sisa_balik > 0, maka sisanya kembalikan secara cash

                    if($sisa_balik > 0){

                        $this->set_alur_kas_kasir($ket = $keterangan_pengembalian, 
                                                    $metode = 0, 
                                                    $status = 0, 
                                                    $jml = $sisa_balik);
                    }

                }else{

                    $this->set_alur_kas_kasir($ket = $keterangan_pengembalian, 
                                                $metode = 0, 
                                                $status = 0, 
                                                $jml = $kembalian);  
     
                }
                
                //end dapatkan nilai kas sebagai acuan pengurangan kas kasir atau pengembalian deposit

            }

        }

        return $return;

    }


	public function cek_saldo_kasir(){

        /*  
            Hanya melakukan pengecekkan apakah User yang sedang login sudah memiliki saldo / kas terbuka.
            Jika belum, maka user tidak dapat melakukan Delete, Tambah , Edit
            
        */

        $user           = $this->auth->userdata();
        $id_user        = $user->id_user;
        $date           = date("Y-m-d");
        /*
        $dt_cek_kas     = $this->kas_kasir_model->where("DATE(waktu_buat) = '{$date}'
                                                        AND created_by = {$id_user}
                                                        AND st_close = 0")
                                                ->count_all();
        */
        $dt_cek_kas     = $this->kas_kasir_model->where("created_by = {$id_user}
                                                        AND st_close = 0")
                                                ->count_all();

        $return         = $dt_cek_kas == 0 ? false : true;
        return $return;

    }

    public function set_alur_kas_kasir($ket = "", $metode = 0, $status = 1, $jml = 0){

        /*
            Keterangan :

                $metode     
                        1.  0   = Cash.
                        2.  1   = Debit.
                        3.  2   = Transfer.
                        4.  3   = Deposit.

        */

        $user           = $this->auth->userdata();
        $id_user        = $user->id_user;
        $date           = date("Y-m-d");

        $waktu_simpan   = date("Y-m-d H:i:s");

        /*$dt_kas_pakai   = $this->kas_kasir_model->select("idkas_kasir")
                                        ->where("DATE(waktu_buat) = '{$date}'
                                                        AND created_by = {$id_user}
                                                        AND st_close = 0")
                                        ->find_all();
*/
        $dt_kas_pakai   = $this->kas_kasir_model->select("idkas_kasir")
                                        ->where("created_by = {$id_user}
                                                        AND st_close = 0")
                                        ->find_all();

        $id_primary     = $dt_kas_pakai[0]->idkas_kasir;


        $dt_saldo       = $this->kas_kasir_detail_model
                                        ->select("`kas_kasir_detail`.`saldo`")
                                        ->join("kas_kasir","kas_kasir_detail.idkas_kasir = kas_kasir.idkas_kasir")
                                        ->where("kas_kasir_detail.idkas_kasir = '{$id_primary}'")    
                                        ->order_by("`kas_kasir_detail`.`waktu`","desc")
                                        ->order_by("`kas_kasir_detail`.`order`","desc")
                                        ->limit(1,0)
                                        ->find_all();

        //kondisikan jumlah yang ditambahkan di saldo.
        //saldo hanya dijumlah jika dia kas tunai.
        if($metode == 0){

            $jml_tambahan = $jml;

        }else{

            $jml_tambahan = 0;    

        }


        if(count($dt_saldo) && is_array($dt_saldo)){

            $saldo_akhir    = $dt_saldo[0]->saldo;

        }else{

            $saldo_akhir = 0;

        }

        $saldo_sekarang = $status == 0 ? $saldo_akhir - $jml_tambahan :  $saldo_akhir + $jml_tambahan;

        //kondisikan order
        $cek_last_order     = $this->kas_kasir_detail_model
                                    ->select("order")
                                    ->where("idkas_kasir = '{$id_primary}' 
                                                and waktu = '{$waktu_simpan}'")
                                    ->order_by("order","desc")
                                    ->limit(1,0)
                                    ->find_all();

        if(is_array($cek_last_order) && count($cek_last_order)){

            $order_baru     = $cek_last_order[0]->order +1;

        }else{

            $order_baru     = 0;

        }

        $this->db->trans_start();

        $arr_detail     =   [
                                'idkas_kasir_detail'    => gen_primary("","kas_kasir_detail","idkas_kasir_detail"),
                                'idkas_kasir'           => $id_primary,
                                'st_saldo_awal'         => 0,
                                'waktu'                 => $waktu_simpan,
                                'ket'                   => $ket,
                                'st_metode'             => $metode,
                                'jumlah'                => $jml,
                                'status'                => $status,
                                'saldo'                 => $saldo_sekarang,
                                'order'                 => $order_baru
                            ];

        $this->kas_kasir_detail_model->insert($arr_detail);
        
        //echo $this->db->last_query();

        $this->db->trans_complete();

        if($this->db->trans_status() == false){

            $return = false;

        }else{

            $return = true;

        }

        return $return;

    }

	public function test(){

		$data 	= $this->bahan_baku_model->test();

		return $data;

	}
}