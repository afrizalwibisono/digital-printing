<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Get_hpp_bb_produksi extends Admin_Controller{
	
	function __construct(){

		parent::__construct();

		$this->load->model([
								'bahan_baku_model',
								'stok_model'
							]);

	}


	public function get_hpp($kode_bahan_baku = null){

		if(is_array($kode_bahan_baku)){

			$kode_all_bb = $kode_bahan_baku;

			if(count($kode_all_bb)){

				//susun where kode bahan baku
				$where 		= " barang.deleted = 0 and ";

				$jml_arr 	= count($kode_all_bb);

				for ($i = 0; $i < $jml_arr; $i++) { 

					$kode_bb = $kode_all_bb[$i];
					
					if($i == 0){

						$where 	.= " (";

					}elseif($i > 0){

						$where 	.= " or ";

					}

					$where 		.= "barang.idbarang_bb = '{$kode_bb}'";

					if($i == ($jml_arr - 1)){

						$where 	.= ") ";

					}

				}

				$data 	= $this->bahan_baku_model
								->select("`barang`.`idbarang_bb` as id,
										    `barang`.`nm_barang`,
										    `barang`.`st_potong_meteran` as st_potong,
										    stok_gudang.hpp,
										    stok_gudang.luas_datang as luas_bb,
										    IF(stok_gudang.hpp IS NULL,
										        0,
										        IF(st_potong_meteran = 0,
										            stok_gudang.hpp,
										            stok_gudang.hpp / stok_gudang.luas_datang)) AS hpp_final")
								->join("(SELECT 
										        stok.`id_barang`,
										            stok.`luas_datang`,
										            stok.`hpp`,
										            stok.`id_gudang`
										    FROM
										        stok
										    INNER JOIN (SELECT 
										        id_barang, MAX(created_on) AS created_on
										    FROM
										        stok
										    WHERE
										        id_gudang = 2
										    GROUP BY id_barang) AS last_update ON stok.id_barang = last_update.id_barang
										        AND stok.created_on = last_update.created_on
										    GROUP BY id_barang) AS stok_gudang","stok_gudang ON barang.idbarang_bb = stok_gudang.id_barang","left")
								->where($where)
								->find_all();

				if(is_array($data) && count($data)){

					return $data;

				}else{

					return false;

				}

			}else{

				return false;

			}

		}else{

			return false;

		}

	}


}

