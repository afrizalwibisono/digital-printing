<?php defined('BASEPATH') || exit('No direct script access allowed');
/**
 * @Author  : Suwito
 * @Email   : suwito.lt@gmail.com
 * @Date    : 2016-09-30 13:36:42
 * @Last Modified by    : Suwito
 * @Last Modified time  : 2018-01-10 06:34:00
 */

/**
 * A simple helper method for checking menu items against the current class/controller.
 * This function copied from cibonfire
 * <code>
 *   <a href="<?php echo site_url(SITE_AREA . '/content'); ?>" <?php echo check_class(SITE_AREA . '/content'); ?> >
 *    Admin Home
 *  </a>
 *
 * </code>
 *
 * @param string $item       The name of the class to check against.
 * @param bool   $class_only If true, will only return 'active'. If false, will
 * return 'class="active"'.
 *
 * @return string Either 'active'/'class="active"' or an empty string.
 */
if(!function_exists('check_class')){
    function check_class($item = '', $class_only = false)
    {
        if (strtolower(get_instance()->router->class) == strtolower($item)) {
            return $class_only ? 'active' : 'class="active"';
        }

        return '';
    }
}

/**
 * A simple helper method for checking menu items against the current method
 * (controller action) (as far as the Router knows).
 *
 * @param string    $item       The name of the method to check against. Can be an array of names.
 * @param bool      $class_only If true, will only return 'active'. If false, will return 'class="active"'.
 *
 * @return string Either 'active'/'class="active"' or an empty string.
 */
if(!function_exists('check_method')){
    function check_method($item, $class_only = false)
    {
        $items = is_array($item) ? $item : array($item);
        if (in_array(get_instance()->router->method, $items)) {
            return $class_only ? 'active' : 'class="active"';
        }

        return '';
    }
}

/**
 * Check if the logged user has permission or not
 * @param string $permission_name
 * @return bool True if has permission and false if not
 */
if(!function_exists('has_permission')){
    function has_permission($permission_name = "")
    {
        $ci =& get_instance();
        
    	$return = $ci->auth->has_permission($permission_name);

    	return $return;
    }
}

/**
 * Check if the logged user has permission (by id permission) or not
 * @param  int  $id_permission
 * @return boolean TRUE if has id, and FALSE if not
 */
if(!function_exists('has_id_permission')){
    function has_id_permission($id_permission = "")
    {
        $ci =& get_instance();
        
        $return = $ci->auth->has_id_permission($id_permission);

        return $return;
    }
}

/**
 * @param  string $kode_tambahan
 * @return string generated code
 */
function gen_primary($kode_tambahan = "", $nm_table = "", $primary_key_field = "")
{ 
    if(!function_exists('get_primary_code')){
        function get_primary_code($kode_tambahan = ""){
            $CI             =& get_instance();

            $tahun          = intval(date('Y'));
            $bulan          = intval(date('m'));
            $hari           = intval(date('d'));
            $jam            = intval(date('H'));
            $menit          = intval(date('i'));
            $detik          = intval(date('s'));
            $temp_ip        = ($CI->input->ip_address()) == "::1" ? "127.0.0.1" : $CI->input->ip_address();
            $temp_ip        = explode(".", $temp_ip);
            $ipval          = $temp_ip[0] + $temp_ip[1] + $temp_ip[2] + $temp_ip[3];

            $mictime        = microtime();

            $kode_rand      = mt_rand(1,999)+$ipval;
            //Format variable
            $kode_rand      = str_pad($kode_rand, 3,"0",STR_PAD_LEFT);
            $bulan          = str_pad($bulan, 2,"0",STR_PAD_LEFT);
            $hari           = str_pad($hari, 2,"0",STR_PAD_LEFT);
            $jam            = str_pad($jam, 2,"0",STR_PAD_LEFT);
            $menit          = str_pad($menit, 2,"0",STR_PAD_LEFT);
            $detik          = str_pad($detik, 2,"0",STR_PAD_LEFT);
            
            $kode_primary   = $tahun.$bulan.$hari.$jam.$menit.$detik.$kode_rand;
            $pjg_kode       = strlen($kode_primary);

            $jml_letter     = mt_rand(2,10);

            for ($i=1; $i <= $jml_letter ; $i++) { 
                
                $letter         = chr(mt_rand(65,90));
                $pos_insert     = mt_rand(0, $pjg_kode);

                $kode_primary   = substr_replace($kode_primary, $letter, $pos_insert,0);
                $pjg_kode       = strlen($kode_primary);                

            }


            //$letter1        = chr(mt_rand(65,90));

            //$kode_primary   = $tahun.$bulan.$hari.$jam.$menit.$detik.$letter1.$kode_rand;
                        
            return $kode_tambahan . $kode_primary;    
        }
    }

    $cek = FALSE;
    while ($cek == FALSE) {
        $generated_code = get_primary_code($kode_tambahan);

        if($nm_table !="" && $primary_key_field !=""){
            $CI     =& get_instance();

            $query  = $CI->db->get_where($nm_table, [$primary_key_field => $generated_code], 1, 0);
            $result = $query->row();
            if(!$result){
                $cek = TRUE;
            }
        }else{
            $cek = TRUE;
        }
    }

    return $generated_code;
}

if(! function_exists('simpan_aktifitas'))
{
    function simpan_aktifitas($nm_hak_akses = "", $kode_universal = "", $keterangan ="", $jumlah = 0, $sql = "", $status = NULL)
    {
        $CI     =& get_instance();

        $CI->load->model('aktifitas/aktifitas_model');

        $result = $CI->aktifitas_model->simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        return $result;
    }
}

/*
* $date_from is the date with format dd/mm/yyyy H:i:s / dd/mm/yyyy
*/
if (! function_exists('date_ymd')) {
    function date_ymd($date_from)
    {
        $error = false;
        if(strlen($date_from) <= 10){
            $date_from = str_replace(['-'], '/', $date_from);
            list($dd,$mm,$yyyy) = explode('/',$date_from);

            if (!checkdate(intval($mm),intval($dd),intval($yyyy)))
            {
                    $error = true;
            }
        }
        else
        {
            list($dd,$mm,$yyyy) = explode('/',$date_from);
            list($yyyy,$hhii) = explode(" ", $yyyy);

            if (!checkdate($mm,$dd,$yyyy))
            {
                    $error = true;
            }
        }

        if($error)
        {
            return false;
        }

        if(strlen($date_from) <= 10)
        {
            $date_from = DateTime::createFromFormat('d/m/Y', $date_from);   
            $date_from = $date_from->format('Y-m-d');
        }
        else
        {
            $date_from = DateTime::createFromFormat('d/m/Y H:i:s', $date_from);   
            $date_from = $date_from->format('Y-m-d H:i:s');   
        }
        
        return $date_from;
    }
}

/*
    Remove Comma from given value
 */
if(!function_exists('remove_comma'))
{
    function remove_comma($val = 0)
    {
        return str_replace(",", "", $val);
    }
}

/**
 * [Fungsi ini akan menghasilkan list konversi berdasarkan satuan terkecil]
 * @param  [integer] $id_satuan  [id satuan terkecil]
 * @param  [integer] $id_konversi [jika diisi maka akan terpilih sebagai "selected" sesuai dengan id_konversi yang dimasukkan]
 * @param  [integer] $auto_select [jika diisi maka akan terpilih sebagai "selected" sesuai dengan satuan terkecil barang]
 * @return [array]
 */
if(!function_exists('get_konversi'))
{
    function get_konversi($id_satuan_terkecil = 1, $id_konversi = "", $auto_select = TRUE)
    {
        $CI     =& get_instance();
        $CI->load->model('konversi_satuan/konversi_satuan_model');

        $konversi   = $CI->konversi_satuan_model->get_konversi($id_satuan_terkecil, $id_konversi, $auto_select);
        
        return $konversi;
    }
}

/**
 * [Fungsi ini digunakan untuk mendapatkan data konversi satuan berdasarkan tipe konversi]
 * @param  [type] $tipe        [0 = satuan barang [pcs, roll, biji box ] 
 *                              1 = panjang [ cm, m, kaki ] 
 *                              2 = berat [kg , ons, gram]]
 * @param  [type] $id_konversi [optional, jika diisi maka data yang 'selected' sama dengan 1 berarti itu yang 
 *                              terpilih (di html tag select)]
 * @return [array]             [Return dari fungsi ini berupa array]
 */
if(!function_exists('get_konversi_by_tipe')){
    function get_konversi_by_tipe($tipe = NULL, $id_konversi = NULL){
        $CI     =& get_instance();
        $CI->load->model('konversi_satuan/konversi_satuan_model');

        $konversi   = $CI->konversi_satuan_model->get_konversi_by_tipe($tipe, $id_konversi);
        
        return $konversi;
    }
}

/**
 * [Fungsi ini akan menghasilkan option untuk select element berdasarkan satuan terkecil]
 * @param  [integer] $id_satuan  [id satuan terkecil]
 * @param  [integer] $format  [1 = tidak ada satuan kecil disamping satuan, 2 = ada satuan kecil disamping satuan]
 * @return [array]
 */
if(!function_exists('get_konversi_select'))
{
    function get_konversi_select($id_satuan_terkecil = 1, $format = 1)
    {
        $CI     =& get_instance();
        $CI->load->model('konversi_satuan/konversi_satuan_model');

        $optSelect = $CI->konversi_satuan_model->get_konversi_select($id_satuan_terkecil, $format);
        
        return $optSelect;
    }
}

if(!function_exists("hitung_ke_satuan_kecil"))
{
    function hitung_ke_satuan_kecil($id_konversi = 1, $qty = 0)
    {
        $CI     =& get_instance();
        $CI->load->model('konversi_satuan/konversi_satuan_model');

        $result = $CI->konversi_satuan_model->hitung_ke_satuan_kecil($id_konversi, $qty);

        return $result;
    }
}

if(!function_exists("hitung_ke_satuan_besar"))
{
    function hitung_ke_satuan_besar($id_konversi = 1, $qty = 0)
    {
        $CI     =& get_instance();
        $CI->load->model('konversi_satuan/konversi_satuan_model');

        $result = $CI->konversi_satuan_model->hitung_ke_satuan_besar($id_konversi, $qty);

        return $result;
    }
}

/**
 * Terbilang Helper
 *
 * @package CodeIgniter
 * @subpackage  Helpers
 * @category    Helpers
 * @author  Gede Lumbung
 * @link    http://gedelumbung.com
 */

if ( ! function_exists('number_to_words'))
{
    function number_to_words($number)
    {
        $before_comma = trim(to_word($number));
        return ucwords($results = $before_comma.' Rupiah');
    }

    function to_word($number)
    {
        $words = "";
        $arr_number = array(
        "",
        "satu",
        "dua",
        "tiga",
        "empat",
        "lima",
        "enam",
        "tujuh",
        "delapan",
        "sembilan",
        "sepuluh",
        "sebelas");

        if($number<12)
        {
            $words = " ".$arr_number[$number];
        }
        else if($number<20)
        {
            $words = to_word($number-10)." belas";
        }
        else if($number<100)
        {
            $words = to_word($number/10)." puluh ".to_word($number%10);
        }
        else if($number<200)
        {
            $words = "seratus ".to_word($number-100);
        }
        else if($number<1000)
        {
            $words = to_word($number/100)." ratus ".to_word($number%100);
        }
        else if($number<2000)
        {
            $words = "seribu ".to_word($number-1000);
        }
        else if($number<1000000)
        {
            $words = to_word($number/1000)." ribu ".to_word($number%1000);
        }
        else if($number<1000000000)
        {
            $words = to_word($number/1000000)." juta ".to_word($number%1000000);
        }
        else
        {
            $words = "undefined";
        }
        return $words;
    }

    function comma($number)
    {
        $after_comma = stristr($number,',');
        $arr_number = array(
        "nol",
        "satu",
        "dua",
        "tiga",
        "empat",
        "lima",
        "enam",
        "tujuh",
        "delapan",
        "sembilan");

        $results = "";
        $length = strlen($after_comma);
        $i = 1;
        while($i<$length)
        {
            $get = substr($after_comma,$i,1);
            $results .= " ".$arr_number[$get];
            $i++;
        }
        return $results;
    }
}
/*
* Load Setting from database setting 
*/
if(! function_exists('app_get_setting'))
{
    function app_get_setting($setting_name = "")
    {
        if($setting_name == "")
        {
            return FALSE;
        }

        $CI     =& get_instance();

        $CI->load->model('settings_model');

        $result = $CI->settings_model->get_setting($setting_name);

        return $result;
    }
}


if(! function_exists('set_log_proses')){

    function set_log_proses($id_order = null, $id_detail_order = null, $st_log = 0, $id_ket_log = 0, $waktu = null){

        if($id_order == null || $id_detail_order == null || $st_log == 0){

            return false;

        }else{

            $CI     =& get_instance();
            $CI->load->model('log_status_order/log_status_model');

            $result     = $this->log_status_model->set_log($id_order, $id_detail_order, $st_log, $id_ket_log, $waktu);

            return $result;
        
        }    

    }

}

if(! function_exists('simpan_alurkas')){

    function simpan_alurkas($kode_accountKas = null, 
                            $ket = "", 
                            $total = null , 
                            $status = null, 
                            $nm_hak_akses = "", 
                            $model_transaksi = 0, 
                            $id_bank = 0, 
                            $tgl_proses = ""){

        /*
            $tgl_proses = Y-m-d H:i:s;
        */

        $CI     =& get_instance();

        $CI->load->model('kas/kas_model');

        $result = $CI->kas_model->simpan_alurKas($kode_accountKas, $ket, $total, $status, $nm_hak_akses, $model_transaksi, $id_bank, $tgl_proses);

        return $result;
    }
}

if(!function_exists('simpan_alur_stok')){
    function simpan_alur_stok($id_gudang = 0, $id_barang = '', $qty = 0, $hpp = 0, $st_alur = 1, $ket = '', $order = 0, $tgl_tran = '')
    {
        $CI =& get_instance();

        $CI->load->model('alur_stok_model');

        $result = $CI->alur_stok_model->simpan_alur_stok($id_gudang, $id_barang, $qty, $hpp, $st_alur, $ket, $order, $tgl_tran);

        return $result; 
    }
}



// *st_ref 
//     referensi tabel yang akan ditampilkan di spk
//     0: tabel m_order_produk_detail
//     1: kasir_order_po_detail
// *id_ref
//     st_ref=0 -> id_m_order_produk_detail
//     st_ref=1 -> id_kasir_order_po_detail
if(!function_exists('simpan_spk')){
    function simpan_spk($st_ref=0, $id_ref = '', $id_konversi_jml_cetak=8, $st_revisi=0, $jml_cetak=0){

        $CI =& get_instance();
        $CI->load->model('update_spk_model');

        $hasil  = $CI->update_spk_model->set_spk_baru($st_ref, $id_ref, $id_konversi_jml_cetak, $st_revisi, $jml_cetak);

        return $hasil;

    }

}

if(!function_exists('delete_spk')){
    function delete_spk($id_spk=0){

        $CI =& get_instance();
        $CI->load->model('update_spk_model');

        $delete  = $CI->update_spk_model->delete_spk($id_spk);

        return $delete;

    }
}

if(!function_exists('get_hpp_bb_by_produksi')){

    function get_hpp_bb_by_produksi($arr_idbarang){

        require_once(APPPATH.'controllers/Get_hpp_bb_produksi.php');

        $controller = new get_hpp_bb_produksi();

        return $controller->get_hpp($arr_idbarang);
    }

}

// start helper perkasiran

if(!function_exists('cek_saldo_kasir')){

    function cek_saldo_kasir(){

        require_once(APPPATH.'controllers/Core_kas_kasir.php');

        $controller = new core_kas_kasir();

        return $controller->cek_saldo_kasir();
    }

}

if(!function_exists('set_alur_kas_kasir')){

    function set_alur_kas_kasir($ket = "", $metode = 0, $status = 1, $jml = 0){

        require_once(APPPATH.'controllers/Core_kas_kasir.php');

        $controller = new core_kas_kasir();

        return $controller->set_alur_kas_kasir($ket, $metode, $status, $jml);
    }

}


if(!function_exists('set_bayar_transaksi')){

    function set_bayar_transaksi($st_transaksi = 0,
                                        $id_transaksi = "",
                                        $st_bayar_trans = 0,
                                        $st_sumber = 0,
                                        $st_bayar = 0,
                                        $total_transaksi = 0,
                                        $tagihan = 0,
                                        $bayar  = 0,
                                        $st_dp = 0,
                                        $lokasi_file = "",
                                        $id_transaksi_master = "",
                                        $keterangan_kas_masuk = "",
                                        $order_record = 0){

        require_once(APPPATH.'controllers/Core_kas_kasir.php');

        $controller = new core_kas_kasir();        

        return $controller->insert_bayar_trans($st_transaksi, $id_transaksi, $st_bayar_trans,
                                                $st_sumber, $st_bayar, $total_transaksi, $tagihan, 
                                                $bayar, $st_dp, $lokasi_file, $id_transaksi_master, 
                                                $keterangan_kas_masuk,$order_record);   


    }

}

if(!function_exists('delete_pembayaran')){

    function delete_pembayaran($st_delete_by = 0,
                                        $id_primary_bayar = '',
                                        $id_transaksi_master = '',
                                        $waktu_start_delete = '',
                                        $keterangan_delete = '',
                                        $keterangan_kas_kasir = ''){

        require_once(APPPATH.'controllers/Core_kas_kasir.php');

        $controller = new core_kas_kasir();        

        return $controller->delete_pembayaran($st_delete_by,
                                                $id_primary_bayar,
                                                $id_transaksi_master,
                                                $waktu_start_delete,
                                                $keterangan_delete,
                                                $keterangan_kas_kasir);        


    }

}

if(!function_exists('bulatkan_keatas')){
    /* $value = angka yang akan dibulatkan, 
    *  $bilangan = acuan, misal 10, 100, 1000, dst 
    */
    function bulatkan_keatas($value = 0, $bilangan = 10){ 
        $angka = intval($value);
        $acuan = intval($bilangan);

        $hasil = ceil($angka / $acuan) * $acuan;

        return $hasil;
    }
}

// end helper perkasiran

// Helper Deposit ========================================================================
// Untuk dokumentasi lebih lengkap bisa dibuka di : modules/deposit/controllers/Deposit.php
if(!function_exists('set_deposit')){
    function set_deposit($id_konsumen = '', $nilai = 0, $ket = '', $st_valid = 1, $waktu = null, $metode_bayar = 1, $rekening_pengirim = '', $input_file_resi = null){
        require_once(APPPATH.'modules/deposit/controllers/Deposit.php');
        $controller = new Deposit();

        return $controller->_simpan_deposit($id_konsumen, 1, $nilai, $ket, $st_valid, $waktu, $metode_bayar, $rekening_pengirim, $input_file_resi);
    }
}
// Untuk menggunakan deposit
if(!function_exists('use_deposit')){
    function use_deposit($id_konsumen = '', $nilai = 0, $ket = ''){
        require_once(APPPATH.'modules/deposit/controllers/Deposit.php');
        $controller = new Deposit();

        return $controller->_simpan_deposit($id_konsumen, 0, $nilai, $ket, 1);
    }
}
// Untuk mendapatkan saldo terakhir deposit
if(!function_exists('get_deposit')){
    function get_deposit($id_konsumen = ''){
        require_once(APPPATH.'modules/deposit/controllers/Deposit.php');
        $controller = new Deposit();

        return $controller->_get_saldo_deposit($id_konsumen);
    }
}
// End Helper Deposit ====================================================================

// start Helper log proses order =========================================================
if(!function_exists('set_proses_order')){
    function set_proses_order($id_lokasi = 0, $id_ket_proses = "", $id_detail_produk_order = "", $jumlah_proses = 0){
        require_once(APPPATH.'modules/log_proses_order/controllers/Log_proses_order.php');
        $controller = new Log_proses_order();

        return $controller->set_data_log($id_lokasi, $id_ket_proses, $id_detail_produk_order, $jumlah_proses);
    }
}
// end Helper log proses order ==========================================================

// start helper get harga produk
if(!function_exists('get_harga_produk')){

    function get_harga_produk($id_produk="", $idkategori_harga=0, $luas=0 ,$qty =0){

        require_once(APPPATH.'controllers/Order.php');

        $controller = new Order();

        return $controller->get_harga($id_produk, $idkategori_harga, $luas, $qty);
    }

}
// end Helper get_harga =============================================================

// start helper get harga produk
if(!function_exists('update_list')){

    function update_list($id_produk="", $id_order=0, $status=0){

        require_once(APPPATH.'controllers/Order.php');

        $controller = new Order();

        return $controller->update_list_harga($id_produk, $id_order, $status);
    }

}
// end Helper update_list_harga ==============================================

// update total order
if(!function_exists('update_total')){

    function update_total($id_order=0){

        require_once(APPPATH.'controllers/Order.php');

        $controller = new Order();

        return $controller->update_total($id_order);
    }

}
// end Helper update total ===========================

/**
 * Parameter
 * @param $str_number   ; Angka yang masih dalam bentuk format number 999,999,999.99(format english) / 999.999.999,99(format indonesia)
 * @param $jml_desimal  ; jumlah desimal dibelakang koma, untuk membedakan apakah ini pecahan atau puluhan
 *                          $jml_desimal ini akan bekerja jika hanya ditemukan 1 separator
 * Function ini berfungsi untuk memvalidasi inputan yang dikenai format, bukan menghandle semua format angka
 */

/*  Script Test
    Copy ke controller untuk testing

    $test_number[0]    = "4.300.000,222";
        $cek[0]            = get_number_pakai($test_number[0]);

        $test_number[1]    = "4,300,000.222";
        $cek[1]            = get_number_pakai($test_number[1]);

        $test_number[2]    = "4.300,343";
        $cek[2]            = get_number_pakai($test_number[2]);

        $test_number[3]    = "4,300.343";
        $cek[3]            = get_number_pakai($test_number[3]);

        $test_number[4]    = "4,30";
        $cek[4]            = get_number_pakai($test_number[4]);

        $test_number[5]    = "4.30";
        $cek[5]            = get_number_pakai($test_number[5]);

        $test_number[6]    = "430.89";
        $cek[6]            = get_number_pakai($test_number[6]);

        $test_number[7]    = "430,89";
        $cek[7]            = get_number_pakai($test_number[7]);

        for ($i=0; $i < 8; $i++) { 
            
            echo "angka asli : {$test_number[$i]} =======> final_angka : {$cek[$i]}<br>";    

        }
 */


if(!function_exists('get_number_pakai')){
    function get_number_pakai($str_number = "", $jml_desimal = 2){

        // ..:: Start cek kondisi parameter ::..

        if(strlen($str_number) == 0){

            return false;

        }

        // ..:: end cek kondisi parameter ::..

        $jml_str    = strlen($str_number);

        // ..:: start cari separator ribuan dan desimal ::..
        
        //count jml ,(koma)
        $jml_koma   = substr_count($str_number,",");

        //count jml .(titik)
        $jml_titik  = substr_count($str_number,".");

        // status jika hanya ditemukan separator desimal
        $st_1_separator = false;

        // cek jika salah satu separator hanya berjumlah 1 dan lainnya 0, cari apakah ini desimal atau ribuan
        if(($jml_koma == 1 && $jml_titik == 0) || ($jml_koma == 0 && $jml_titik == 1)){

            $separator  = $jml_koma == 0 ? "." : ",";

            $pecah      = explode($separator,$str_number);

            if(strlen($pecah[1]) < 3 || strlen($pecah[1]) > 3 ){
                
                $para_desimal       = $separator;
                $para_ribuan        = "";

            }else{

                $para_ribuan        = $separator;
                $para_desimal       = "";

            }

            $st_1_separator = true;

        }

        if($st_1_separator == false){

            if ($jml_titik != $jml_koma){

                $para_desimal           = $jml_titik < $jml_koma ? "." : ",";
                $para_ribuan            = $jml_koma > $jml_titik ? "," : ".";
    
            }else{
    
                //cari posisi terakhir dari karakter "," koma
    
                $n          = 0;
                $pos_cek    = 0;
    
                while ($n < $jml_koma) {
                    
                    if (strpos($str_number,",",($pos_cek > 0 ? ++$pos_cek : $pos_cek) ) != false){
    
                        ++$n;
                        $pos_cek = strpos($str_number,",",($pos_cek > 0 ? ++$pos_cek : $pos_cek) );
    
                    }
    
                }
    
                $last_post_koma     = $pos_cek;
    
                //cari posisi terakhir dari karakter "." titik
    
                $n          = 0;
                $pos_cek    = 0;
    
                while ($n < $jml_titik) {
                    
                    if (strpos($str_number,".",($pos_cek > 0 ? ++$pos_cek : $pos_cek) ) != false){
    
                        ++$n;
                        $pos_cek = strpos($str_number,".",($pos_cek > 0 ? ++$pos_cek : $pos_cek) );
    
                    }
    
                }
    
                $last_post_titik    = $pos_cek;
    
                // tentukan separator ribuan dan separator desimal
    
                $para_desimal       = $last_post_titik > $last_post_koma ? "." : ",";
                $para_ribuan        = $last_post_koma < $last_post_titik ? "," : ".";
    
            }

        }

        //echo "para desimal = {$para_desimal} ===> para ribuan = {$para_ribuan} ";

        //exit();

        // ..:: end cari separator ribuan dan desimal ::..

        // ..:: Start gabungkan nilai ribuan dan desimal ::..

        $get_nilai_desimal  = "";
        $length_cek         = false;

        $get_nilai_ribuan   = 0;

        //get komponen desimal        

        if(strlen($para_desimal) > 0){

            $length_cek             = strpos($str_number, $para_desimal);
            $length_desimal         = $length_cek == false ? 0 : $jml_str - $length_cek;
            $length_desimal         = $length_desimal == 0 ? 0 : $length_desimal - 1; //agar posisi separator nggak ikut kehitung.        
            
            $get_nilai_desimal      = $length_cek == false ? "" : substr($str_number, $length_cek+1, $jml_str - $length_cek);
            //echo "; Nilai Desimal : ".$get_nilai_desimal;
        
        }

        //get komponen ribuan
        $get_nilai_ribuan       = $length_cek == false ? substr($str_number, 0, $jml_str) : substr($str_number, 0, $length_cek);
        $get_nilai_ribuan       = str_replace($para_ribuan, "", $get_nilai_ribuan);
        //echo "; Nilai Ribuan : ".$get_nilai_ribuan;

        //gabungkan nilai 
        $nilai                  = strlen($get_nilai_desimal) > 0 ? $get_nilai_ribuan.".".$get_nilai_desimal : $get_nilai_ribuan;
        $nilai                  = doubleval($nilai);

        //echo "; Nilai Final :".$nilai;

        // ..:: Start gabungkan nilai ribuan dan desimal ::..

        return $nilai;

    }
}

if(! function_exists('new_konsumen'))
{
    function new_customer($nama_konsumen = "", $telp = "")
    {
        $CI     =& get_instance();

        $CI->load->model('customer_model');

        $result = $CI->customer_model->new_customer($nama_konsumen, $telp);

        return $result;
    }
}
