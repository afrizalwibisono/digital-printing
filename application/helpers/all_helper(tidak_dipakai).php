<?php defined('BASEPATH') || exit('No direct script access allowed');

if(!function_exists('cek_saldo_kasir')){

	function cek_saldo_kasir(){

		require_once(APPPATH.'controllers/Core_kas_kasir.php');

		$controller = new core_kas_kasir();

		return $controller->cek_saldo_kasir();
	}

}

if(!function_exists('set_alur_kas_kasir')){

	function set_alur_kas_kasir($ket = "", $metode = 0, $status = 1, $jml = 0){

		require_once(APPPATH.'controllers/Core_kas_kasir.php');

		$controller = new core_kas_kasir();

		return $controller->set_alur_kas_kasir($ket, $metode, $status, $jml);
	}

}