<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * @author CokesHome
 * @copyright Copyright (c) 2017, CokesHome
 * 
 * This is model class for table "alur_stok"
 */

class Alur_stok_model extends BF_Model
{
    /**
     * @var string  User Table Name
     */
    protected $table_name = 'alur_stok';
    protected $key        = 'id_alur_stok';

    /**
     * @var string Field name to use for the created time column in the DB table
     * if $set_created is enabled.
     */
    protected $created_field = 'created_on';

    /**
     * @var string Field name to use for the modified time column in the DB
     * table if $set_modified is enabled.
     */
    protected $modified_field = 'modified_on';

    /**
     * @var bool Set the created time automatically on a new record (if true)
     */
    protected $set_created = TRUE;

    /**
     * @var bool Set the modified time automatically on editing a record (if true)
     */
    protected $set_modified = FALSE;
    /**
     * @var bool Enable/Disable soft deletes.
     * If false, the delete() method will perform a delete of that row.
     * If true, the value in $deleted_field will be set to 1.
     */
    protected $soft_deletes = TRUE;

    /**
     * @var string The type of date/time field used for $created_field and $modified_field.
     * Valid values are 'int', 'datetime', 'date'.
     */
    protected $date_format = 'datetime';
    //--------------------------------------------------------------------

    /**
     * @var bool If true, will log user id in $created_by_field, $modified_by_field,
     * and $deleted_by_field.
     */
    protected $log_user = TRUE;

    /**
     * Function construct used to load some library, do some actions, etc.
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Fungsi untuk menyimpan log alur stok barang
     * @param  int     $id_gudang [ID Gudang dimana barang dikeluarkan]
     * @param  string  $id_barang [kode barang]
     * @param  integer $qty       [jumlah barang yang keluar / masuk]
     * @param  integer $hpp       [harga barang]
     * @param  integer $st_alur   [status alur barang, 0 = Keluar, 1 = Masuk]
     * @param  string  $ket       [keterangan / catatan]
     * @param  string  $tgl_tran  [tanggal transaksi barang]
     * @return boolean            [TRUE = sukses, FALSE = gagal]
     */
    public function simpan_alur_stok($id_gudang = 0, $id_barang = '', $qty = 0, $hpp = 0, $st_alur = 1, $ket = '', $order = 0, $tgl_tran = '')
    {
        if($id_barang == '' || $qty < 1 || intval($id_gudang) == 0)
        {
            return FALSE;
        }

        //Hitung Saldo
        $dt_alur = $this->select("saldo")
                    ->order_by(['created_on' => 'DESC', 'order' => 'DESC'])
                    ->limit(1)
                    ->find_all_by(['id_gudang' => $id_gudang,'id_barang' => $id_barang, 'deleted' => 0]);
        if($dt_alur)
        {
            $saldo_awal = $dt_alur[0]->saldo;
        }
        else
        {
            $saldo_awal = 0;
        }

        if($st_alur == 0)
        {
            $saldo_akhir = $saldo_awal - $qty;
        }
        else
        {
            $saldo_akhir = $saldo_awal + $qty;
        }

        $data = ['id_alur_stok' => gen_primary('AS'),
                 'id_gudang' => $id_gudang,
                 'id_barang' => $id_barang,
                 'qty'       => $qty,
                 'hpp'       => $hpp,
                 'st_alur'   => $st_alur,
                 'saldo'     => $saldo_akhir, 
                 'ket'       => $ket,
                 'tgl_tran'  => $tgl_tran != '' ? $tgl_tran : date('Y-m-d'),
                 'order'     => $order
                ];

        $this->insert($data);
        $cek = $this->find($data['id_alur_stok']);

        return $cek != FALSE ? TRUE : FALSE;
    }
}