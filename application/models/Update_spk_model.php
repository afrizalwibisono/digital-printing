<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * @author CokesHome
 * @copyright Copyright (c) 2017, CokesHome
 * 
 * This is model class for table "alur_stok"
 */

class Update_spk_model extends BF_Model
{
    /**
     * @var string  User Table Name
     */
    protected $table_name = 'spk';
    protected $key        = 'idspk';

    /**
     * @var string Field name to use for the created time column in the DB table
     * if $set_created is enabled.
     */
    protected $created_field = 'created_on';

    /**
     * @var string Field name to use for the modified time column in the DB
     * table if $set_modified is enabled.
     */
    protected $modified_field = 'modified_on';

    /**
     * @var bool Set the created time automatically on a new record (if true)
     */
    protected $set_created = TRUE;

    /**
     * @var bool Set the modified time automatically on editing a record (if true)
     */
    protected $set_modified = FALSE;
    /**
     * @var bool Enable/Disable soft deletes.
     * If false, the delete() method will perform a delete of that row.
     * If true, the value in $deleted_field will be set to 1.
     */
    protected $soft_deletes = TRUE;

    /**
     * @var string The type of date/time field used for $created_field and $modified_field.
     * Valid values are 'int', 'datetime', 'date'.
     */
    protected $date_format = 'datetime';
    //--------------------------------------------------------------------

    /**
     * @var bool If true, will log user id in $created_by_field, $modified_by_field,
     * and $deleted_by_field.
     */
    protected $log_user = TRUE;

    /**
     * Function construct used to load some library, do some actions, etc.
     */
    public function __construct()
    {
        parent::__construct();
    }

    // *st_ref 
    //     referensi tabel yang akan ditampilkan di spk
    //     0: tabel m_order_produk_detail
    //     1: kasir_order_po_detail
    // *id_ref
    //     st_ref=0 -> id_m_order_produk_detail
    //     st_ref=1 -> id_kasir_order_po_detail

    public function set_spk_baru($st_ref=0, $id_ref = "", $id_konversi_jml_cetak=8, $st_revisi=0, $jml_cetak){

        $this->db->trans_start();

        $id_user_login      = $this->auth->user_id();
        $sql_all            = "";

        $this->load->model([
                                "order_produk/morder_produk_detail_model",
                                "order_produk/morder_produk_detail_a3_model",
                                "kasir/spk_model",
                                "kasir/spk_page_model",
                                'validasi_cetak/kasir_order_po_detail_model'
                            ]);
        // data detail
        if($st_ref == 0){
            $dt_detail_order    = $this->morder_produk_detail_model
                                ->select("
                                            `produk`.`id_worksheet`,
                                            `m_order_produk_detail`.`id_order`,
                                            `m_order_produk_detail`.`id_detail_produk_order`,
                                            `m_order_produk_detail`.`jml_cetak`,
                                            `m_order_produk_detail`.`id_konversi_jml_cetak`,
                                            `m_order_produk_detail`.`kode_universal`
                                        ")
                                ->join("produk","m_order_produk_detail.id_produk = produk.idproduk","inner")
                                ->where("
                                            m_order_produk_detail.deleted = 0 and 
                                            m_order_produk_detail.id_detail_produk_order = '{$id_ref}'  
                                        ")
                                ->find_all();
        }else{
            $dt_detail_order    = $this->kasir_order_po_detail_model
                                ->select("
                                            `produk`.`id_worksheet`,
                                            `order_produk_id` as `id_order`,
                                            `kasir_order_po_detail`.`id_m_order_detail` as `id_detail_produk_order`,
                                            `kasir_order_po_detail`.`jml_cetak`,
                                            `m_order_produk_detail`.`kode_universal`
                                        ")
                                ->join("kasir_order_po","kasir_order_po.id_kasir_order_po=kasir_order_po_detail.id_kasir_order_po","left")
                                ->join("m_order_produk_detail", "m_order_produk_detail.id_detail_produk_order=kasir_order_po_detail.id_m_order_detail")
                                ->join("produk","m_order_produk_detail.id_produk = produk.idproduk","inner")
                                ->where("
                                            m_order_produk_detail.deleted = 0 and 
                                            kasir_order_po_detail.id_kasir_order_po_detail = '{$id_ref}'  
                                        ")
                                ->find_all();
        }


        if(is_array($dt_detail_order) && count($dt_detail_order)){

            $arr_spk        = [];
            $arr_update     = [];            
            $id_spk         = gen_primary("spk","spk","idspk");

                            
            $arr_spk    =   [
                                'idspk'                     => $id_spk,
                                'id_worksheet'              => $dt_detail_order[0]->id_worksheet,
                                'id_order'                  => $dt_detail_order[0]->id_order,
                                'id_detail_produk_order'    => $dt_detail_order[0]->id_detail_produk_order,
                                'jml_cetak'                 => $jml_cetak>0? $jml_cetak : $dt_detail_order[0]->jml_cetak,
                                'id_konversi_jml_cetak'     => $st_ref==1 ? $id_konversi_jml_cetak : $dt_detail_order[0]->id_konversi_jml_cetak,
                                'st_revisi'                 => $st_revisi,
                                'st_ref' => $st_ref,
                                'kode_universal' => $dt_detail_order[0]->kode_universal
                                
                            ];

            $arr_update =   [
                                'st_acc_produksi'       => 1,
                                'id_spk'                => $id_spk
                            ];


            /* simpan data spk */
            $this->spk_model->insert($arr_spk);
            $sql_all    = $this->db->last_query();

            /* update status acc spk */
            if($st_ref==0){
                $this->morder_produk_detail_model->update($id_ref, $arr_update);
                $sql_all    .= "\n\n". $this->db->last_query();
            }else{
                $this->kasir_order_po_detail_model->update($id_ref, $arr_update);
                $sql_all    .= "\n\n". $this->db->last_query();
            }

        }

        // dimatikan sementara karena dijoinkan dengan m_order_produk_detail_a3

        // $dt_page    = $this->morder_produk_detail_a3_model
        //                 ->select("
        //                             `page`,
        //                             `jml_print`            
        //                         ")
        //                 ->where("m_order_produk_detail_a3.id_detail_produk_order = '{$id_ref}'")
        //                 ->find_all();

        // if(is_array($dt_page) && count($dt_page)){

        //     $arr_page = [];

        //     foreach ($dt_page as $key => $data) {
                
        //         $arr_page[] =   [
        //                             "id_spk_detail_a3"  => gen_primary("DETA3","spk_detail_a3","id_spk_detail_a3"), 
        //                             "idspk"             => $id_spk, 
        //                             "page"              => $data->page, 
        //                             "jml_print"         => $data->jml_print
        //                         ];

        //     }

        //     /* insert data */
        //     $this->spk_page_model->insert_batch($arr_page);
        //     $sql_all    .= "\n\n". $this->db->last_query();

        // }

        $this->db->trans_complete();

        if($this->db->trans_status() === false){

            $keterangan     = "Gagal, simpan spk id = " . $id_spk;
            $total          = 0;
            $status         = 0;
            $return         = false;

        }else{

            $keterangan     = "Sukses, simpan spk id = ". $id_spk;
            $total          = 0;           
            $status         = 1;
            $return         = true;

        }

        $nm_hak_akses   = "SPK.Add"; 
        $kode_universal = $id_spk;
        $jumlah         = 0;
        $sql            = $sql_all;

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $total, $sql, $status);

        return $return;
    }

    public function delete_spk($id_spk){

        $this->db->trans_start();


        $this->load->model([
                                "order_produk/morder_produk_detail_model",
                                "order_produk/morder_produk_detail_a3_model",
                                "kasir/spk_model",
                                "kasir/spk_page_model",
                                'validasi_cetak/kasir_order_po_detail_model'
                            ]);
        // data spk
        $data_spk = $this->spk_model->find($id_spk);
        if($data_spk->st_ambil == 1){
            $return = "Order sudah diproduksi, tidak dapat dihapus";
        }else{
            $this->spk_model->delete($id_spk);
            $return = true;
        }
       

        $this->db->trans_complete();

        if($this->db->trans_status() == true && $return == true ){

            $keterangan     = "Sukses, Delete spk id = ". $id_spk;
            $status         = 1;
            $return         = $return;

        }else{

            $keterangan     = "Gagal, Delete spk id = ". $id_spk;
            $status         = 1;
            $return         = $return;

        }

        $nm_hak_akses   = "SPK.Delete"; 
        $kode_universal = $id_spk;
        $jumlah         = 1;
        $sql            = $this->db->last_query();

        simpan_aktifitas($nm_hak_akses, $kode_universal, $keterangan, $jumlah, $sql, $status);

        return $return;
    }
}