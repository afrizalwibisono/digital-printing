<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @Author 				: CokesHome
 * @Email				: dekkyp@gmai.com
 * @Date 				: 2018-05-09 09:42
 * @Last Modified by 	: Dekky
 * @Last Modified time 	: 2018-04-26 02:27:08
 * @Documentation 		: https://github.com/picqer/php-barcode-generator
 */

require(APPPATH."third_party/GenerateBarcode/BarcodeGenerator.php");
require(APPPATH."third_party/GenerateBarcode/BarcodeGeneratorHTML.php");
require(APPPATH."third_party/GenerateBarcode/BarcodeGeneratorPNG.php");

class Barcode{
	
	function __construct(){

	}

	/*
		Parameter
		1.	$kode => Kode barcode yang akan digenerate.
		2. 	$tipe => Tipe barcode yang akan dicetak, hanya ada 2 tipe yang disediakan
						$tipe == 0 => ean13
						$tipe == 1 => 128
		3. 	$bentuk => 	0. HTML
						1. PNG

	*/

	public function gen_barcode($kode = "", $tipe = 0, $bentuk = 0){

		switch ($bentuk) {
			
			case 0:
				$generator 	= new Picqer\Barcode\BarcodeGeneratorHTML();					
				break;
			
			case 1:
				$generator 	= new Picqer\Barcode\BarcodeGeneratorPNG();					
				break;
			
		}


		switch ($tipe) {

			case 0:
				$model 	= $generator::TYPE_EAN_13;				
				break;
			
			case 1:
				$model 	= $generator::TYPE_CODE_128;				
				break;
			
		}

		$hasil 	= $generator->getBarcode($kode, $model);

		return $hasil;		

	}


}