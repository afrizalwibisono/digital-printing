<?php defined('BASEPATH') || exit('No direct script access allowed');

class Menu_generator
{
	protected $ci;
	protected $x; //Db Prefix
	protected $uri; //Current uri string
	protected $viewSPK    	= "SPK.View";
    protected $viewProduksi = "Produksi.View";
    protected $viewRekapBB 	= "Penggunaan Bahan Baku.View";

	public function __construct(){
		$this->ci =& get_instance();
		
		$this->x = $this->ci->db->dbprefix;
		$this->uri = '/'.$this->ci->uri->uri_string().'/';
	}

	public function build_menus($type=1)
	{
		$user_id 	= $this->ci->auth->user_id();
		$is_admin 	= $this->ci->auth->is_admin();

		if($type==1)
		{
			$menu = $this->ci->db->select("t1.*")
							->from("{$this->x}menus as t1")
							->join("{$this->x}menus as t2","t1.id = t2.parent_id","left")
							->where("t1.parent_id",0)
							->where("t1.group_menu",$type)
							->where("t1.status",1)
							->group_by("t1.id")
							->order_by("t1.order","ASC")
							->get()
							->result();

			$html = "<ul class='sidebar-menu' data-widget='tree'>
							<li class='header'></li>
	                        <li class='".check_class('dashboard', TRUE)."'>
	                            <a href='".site_url()."'>
	                                <i class='fa fa-dashboard'></i> <span>Dashboard</span>
	                            </a>
	                        </li>";
	        /** Menu Worksheet **/
	        $userdata = $this->ci->auth->userdata();
	        $idkaryawan = $userdata->idkaryawan;
	        if($idkaryawan){
	        	//Get Worksheet
	        	$worksheet = $this->ci->db->select("w.id_worksheet, w.nm_worksheet")
	        					->from("{$this->x}worksheet as w")
	        					->join("{$this->x}karyawan_detail_worksheet as kdw","w.id_worksheet = kdw.id_worksheet", $is_admin ? "left" : "inner");
	        	//Jika Bukan Admin Maka cari berdasarkan id karyawan
	        	if(!$is_admin){
	        		$worksheet = $worksheet->where("kdw.idkaryawan", $idkaryawan);	
	        	}

	        	$worksheet = $worksheet
	        					->where("w.deleted", 0)
	        					->where("kdw.deleted", 0)
	        					->group_by('w.id_worksheet')
	        					->order_by("w.id_worksheet", "ASC")
	        					->get()
	        					->result();
	        	if($worksheet){
	        		// Menu Worksheet
	        		if(has_permission($this->viewSPK)){
	        			$active = "";

		        		$spk_li  = "";
		        		$spk_uri = "/spk/index/";
						foreach ($worksheet as $ws) {
							//Cek active menu sub menu worksheet
							if(strpos($this->uri, $spk_uri.$ws->id_worksheet.'/')!==FALSE)
							{							
								$active = "active";
							}else{
								$active = "";
							}
							$spk_li .= "
		                				<li class='".$active."'><a href='".site_url($spk_uri.$ws->id_worksheet)."'><i class='fa fa-angle-right'></i>".strtoupper($ws->nm_worksheet)." <span class='pull-right-container'><small class='label pull-right bg-red' id='mn-lbl-spk-".$ws->id_worksheet."'></small></span></a></li>";
						}
						//Cek active menu parent menu worksheet
						$active = "";
						foreach ($worksheet as $ws) {
							if(strpos($this->uri, $spk_uri.$ws->id_worksheet.'/')!==FALSE)
							{							
								$active = "active";
								break;
							}
						}

		        		$html .= "
	            			    <li class='treeview {$active}'>
	                                <a href='#'>
	                                    <i class='fa fa-clipboard'></i>
	                                    <span>SPK</span>
	                                    <span class='pull-right-container'>
							              	<i class='fa fa-angle-left pull-right'></i>
							              	<small class='label pull-right bg-red' id='mn-lbl-total-spk'></small>
							            </span>
	                                </a>
	                                <ul class='treeview-menu'>
	                                	".$spk_li."
	                                </ul>";
	        		}
                    // End Menu Worksheet

                    // Menu Produksi [Disable on 26-10-2018 06:33]
                    if(has_permission($this->viewProduksi)){
	                    $active = "";

		        		$produksi_li  = "";
		        		$produksi_uri = "/produksi/index/";
						foreach ($worksheet as $ws) {
							//Cek active menu sub menu worksheet
							if(strpos($this->uri, $produksi_uri.$ws->id_worksheet.'/')!==FALSE)
							{							
								$active = "active";
							}else{
								$active = "";
							}
							$produksi_li .= "
		                				<li class='".$active."'><a href='".site_url($produksi_uri.$ws->id_worksheet)."'><i class='fa fa-angle-right'></i>".strtoupper($ws->nm_worksheet)." <span class='pull-right-container'><small class='label pull-right bg-red' id='mn-lbl-produksi-".$ws->id_worksheet."'></small></span></a></li>";
						}
						//Cek active menu parent menu worksheet
						$active = "";
						foreach ($worksheet as $ws) {
							if(strpos($this->uri, $produksi_uri.$ws->id_worksheet.'/')!==FALSE)
							{							
								$active = "active";
								break;
							}
						}

		        		$html .= "
            			    <li class='treeview {$active}'>
                                <a href='#'>
                                    <i class='fa fa-life-ring'></i>
                                    <span>Produksi</span>
                                    <span class='pull-right-container'>
                                    	<i class='fa fa-angle-left pull-right'></i>
							            <small class='label pull-right bg-red' id='mn-lbl-total-produksi'></small>
						            </span>
                                </a>
                                <ul class='treeview-menu'>
                                	".$produksi_li."
                                </ul>";
                    }
                    // End Menu Produksi
                     
                    // Menu Rekap Penggunaan BB
	        		if(has_permission($this->viewRekapBB)){
	        			$active = "";

		        		$pengbb_li  = "";
		        		$pengbb_uri = "/penggunaan_bb/index/";
						foreach ($worksheet as $ws) {
							//Cek active menu sub menu rekap
							if(strpos($this->uri, $pengbb_uri.$ws->id_worksheet.'/')!==FALSE)
							{							
								$active = "active";
							}else{
								$active = "";
							}
							$pengbb_li .= "
		                				<li class='".$active."'><a href='".site_url($pengbb_uri.$ws->id_worksheet)."'><i class='fa fa-angle-right'></i>".strtoupper($ws->nm_worksheet)."</a></li>";
						}
						//Cek active menu parent menu rekap
						$active = "";
						foreach ($worksheet as $ws) {
							if(strpos($this->uri, $pengbb_uri.$ws->id_worksheet.'/')!==FALSE)
							{							
								$active = "active";
								break;
							}
						}

		        		$html .= "
	            			    <li class='treeview {$active}'>
	                                <a href='#'>
	                                    <i class='fa fa-calendar'></i>
	                                    <span>Rekap Harian BB</span>
	                                    <span class='pull-right-container'>
							              	<i class='fa fa-angle-left pull-right'></i>
							            </span>
	                                </a>
	                                <ul class='treeview-menu'>
	                                	".$pengbb_li."
	                                </ul>";
	        		}
                    // End Rekap Penggunaan BB
	        	}
	        }
	        /** End Menu Worksheet **/

			if(is_array($menu) && count($menu))
			{
	            foreach ($menu as $rw) {
	            	$id 			= $rw->id;
	            	$title 			= $rw->title;
	            	$link 			= $rw->link;
	            	$icon 			= $rw->icon;
	            	$target 		= $rw->target;
	            	$permission_id 	= $rw->permission_id;

	            	if($is_admin)
	            	{ // If Admin, User can access every menu
	            		$submenu = $this->ci->db->select("t1.*")
												->from("{$this->x}menus as t1")
												->where("t1.parent_id",$id)
												->where("t1.group_menu",$type)
												->where("t1.status",1)
												->group_by("t1.id")
												->order_by("t1.order","ASC")
												->get()
												->result();
	            	}
	            	else
	            	{
	            		$sql = "SELECT 
								    *
								FROM
								    (SELECT 
								        `menus`.*
								    FROM
								        `menus`
								    JOIN `permissions` ON `menus`.`permission_id` = `permissions`.`id_permission`
								    JOIN `group_permissions` ON `permissions`.`id_permission` = `group_permissions`.`id_permission`
								    JOIN `groups` ON `group_permissions`.`id_group` = `groups`.`id_group`
								    JOIN `user_groups` ON `groups`.`id_group` = `user_groups`.`id_group`
								    WHERE
								        `user_groups`.`id_user` = ?
								            AND `menus`.`parent_id` = ?
								            AND `menus`.`group_menu` = ?
								            AND `menus`.`status` = 1 
									
								    UNION ALL 
								    
								    SELECT 
								        `menus`.*
								    FROM
								        `menus`
								    JOIN `permissions` ON `menus`.`permission_id` = `permissions`.`id_permission`
								    JOIN `user_permissions` ON `permissions`.`id_permission` = `user_permissions`.`id_permission`
								    JOIN `users` ON `user_permissions`.`id_user` = `users`.`id_user`
								    WHERE
								        `users`.`id_user` = ?
								            AND `menus`.`parent_id` = ?
								            AND `menus`.`group_menu` = ?
								            AND `menus`.`status` = 1) AS `tbl`
								GROUP BY `tbl`.`id`
								ORDER BY `tbl`.`order` ASC";

						$submenu = $this->ci->db->query($sql, array($user_id, $id, $type, $user_id, $id, $type))->result();
	            	}

					//Jump to end_for point
					if(count($submenu) == 0)
					{
						if($link !="#" && has_id_permission($permission_id))
						{
							$active = "";

							if(strpos($this->uri, '/'.$link.'/') !== FALSE)
							{							
								$active = "active";
							}

							$html .= "<li class='{$active}'><a href='".($link == '#' ? '#' : site_url($link))."' ".($target == '_blank' ? "target='_blank'" : "")."><i class='{$icon}'></i> <span>".ucwords($title)."</span></a></li>";
						}

						goto end_for;
					}

					$active = "";

					foreach ($submenu as $sub) {
						
						if(strpos($this->uri, '/'.$sub->link.'/')!==FALSE)
						{							
							$active = "active";
							break;
						}
					}
					
	            	$html .= "
            			    <li class='treeview {$active}'>
                                <a href='#'>
                                    <i class='".$icon."'></i>
                                    <span>".ucwords($title)."</span>
                                    <span class='pull-right-container'>
						              <i class='fa fa-angle-left pull-right'></i>
						            </span>
                                </a>
                                <ul class='treeview-menu'>";

	                //Make Sub Menu
	                foreach ($submenu as $sub) {

	                	$subid 		= $sub->id;
		            	$subtitle 	= $sub->title;
		            	$sublink 	= $sub->link;
		            	$subicon 	= $sub->icon;
		            	$subtarget 	= $sub->target;

		            	$subtarget = "";

		            	if($subtarget == '_blank')
		            	{
		            		$subtarget = "target='_blank'";
		            	}

		            	//Check current link
						if(strpos($this->uri, '/'.$sublink.'/')!==FALSE)
						{
							$active = "active";
						}
						else
						{
							$active="";
						}

	                	$html .= "
	                				<li class='".$active."'><a href='".($sublink == '#' ? '#' : site_url($sublink))."'"." ".$subtarget."><i class='".$subicon."'></i>".ucwords($subtitle)."</a></li>";
	                }

	                $html .="		
                				</ul>
                			</li>";

                	//Jump Point
                	end_for:
	            }

	            $html .="
	            	</ul>";
			}
		}else{
			//other menu
		}
		
		return $html;
	}

}