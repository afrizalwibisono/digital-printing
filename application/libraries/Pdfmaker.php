<?php defined('BASEPATH') OR exit('No direct script access allowed');

define('DOMPDF_ENABLE_AUTOLOAD', false);
require_once(APPPATH.'third_party/Dompdf/autoload.inc.php');

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

class Pdfmaker {

  public function generate($html, $filename='', $stream=TRUE, $paper = 'A4', $orientation = "portrait", $forcedownload = 0)
  {
    $options = new Options();
    $options->set('isRemoteEnabled', true);

    $dompdf = new DOMPDF($options);
    $dompdf->load_html($html);
    $dompdf->set_paper($paper, $orientation);
    $dompdf->render();
    if ($stream) {
        $dompdf->stream($filename.".pdf", array("Attachment" => $forcedownload));
    } else {
        return $dompdf->output();
    }
  }
}