<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @Author 		: CokesHome
 * @Email		: suwito.lt@gmail.com
 * @Date 		: 2018-04-26 00:29:42
 * @Last Modified by 	: Suwito
 * @Last Modified time 	: 2018-04-26 02:27:08
 * @Documentation : https://github.com/PHPMailer/PHPMailer
 */

require(APPPATH."third_party/PHPMailer/src/Exception.php");
require(APPPATH."third_party/PHPMailer/src/PHPMailer.php");
require(APPPATH."third_party/PHPMailer/src/SMTP.php");

class Mailer{
	private $ci;

	public function __construct(){
		$this->ci =& get_instance();
		$this->ci->config->load('email', FALSE, TRUE);
	}

	public function load(){
		$config = $this->ci->config->config;
		
		$sender  	 = app_get_setting('email.sender');
		$sender_name = app_get_setting('email.sender.name');

		$mail = new PHPMailer\PHPMailer\PHPMailer();
		$mail->IsSMTP(); // enable SMTP
	    $mail->Timeout 		= $config['mail_timeout'];
	    $mail->SMTPDebug 	= $config['debug_level']; // `0` No output, `1` Commands, `2` Data and commands, `3` As 2 plus connection status, `4` Low-level data output.
	    $mail->SMTPAuth 	= $config['_smtp_auth']; // authentication enabled
	    $mail->SMTPSecure 	= $config['smtp_crypto']; // secure transfer enabled REQUIRED for Gmail
	    $mail->Host 	= $config['smtp_host'];
	    $mail->Port 	= $config['smtp_port']; // or 587
	    $mail->IsHTML($config['mailtype'] == 'html' ? TRUE : FALSE);
	    $mail->Username = $config['smtp_user'];
	    $mail->Password = $config['smtp_pass'];
	    $mail->SetFrom($sender ? $sender : $config['smtp_user'], $sender_name ? $sender_name : $config['sender_name']);

	    return $mail;
	}

}