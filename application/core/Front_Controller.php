<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Front_Controller extends Base_Controller
{

    //--------------------------------------------------------------------

    /**
     * Class constructor
     *
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library(array('form_validation','template'));

        $this->load->model('identitas_model');

        $this->form_validation->set_error_delimiters('', '</br>');

        $idt = $this->identitas_model->find(1);

        $this->template->set('idt', $idt);
        $this->template->set_theme('default');
        $this->template->set_layout('index');
    }//end __construct()

    //--------------------------------------------------------------------

}
/*End of Front Controller*/